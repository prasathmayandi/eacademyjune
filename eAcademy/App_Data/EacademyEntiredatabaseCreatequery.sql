USE [master]
GO
/****** Object:  Database [Eacademy1]    Script Date: 01/05/2016 10:49:13 ******/
CREATE DATABASE [Eacademy1] ON  PRIMARY 
( NAME = N'Eacademy1', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Eacademy1.mdf' , SIZE = 7424KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Eacademy1_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Eacademy1_1.LDF' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Eacademy1] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Eacademy1].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Eacademy1] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [Eacademy1] SET ANSI_NULLS OFF
GO
ALTER DATABASE [Eacademy1] SET ANSI_PADDING OFF
GO
ALTER DATABASE [Eacademy1] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [Eacademy1] SET ARITHABORT OFF
GO
ALTER DATABASE [Eacademy1] SET AUTO_CLOSE ON
GO
ALTER DATABASE [Eacademy1] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [Eacademy1] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [Eacademy1] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [Eacademy1] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [Eacademy1] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [Eacademy1] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [Eacademy1] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [Eacademy1] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [Eacademy1] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [Eacademy1] SET  DISABLE_BROKER
GO
ALTER DATABASE [Eacademy1] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [Eacademy1] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [Eacademy1] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [Eacademy1] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [Eacademy1] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [Eacademy1] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [Eacademy1] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [Eacademy1] SET  READ_WRITE
GO
ALTER DATABASE [Eacademy1] SET RECOVERY SIMPLE
GO
ALTER DATABASE [Eacademy1] SET  MULTI_USER
GO
ALTER DATABASE [Eacademy1] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [Eacademy1] SET DB_CHAINING OFF
GO
USE [Eacademy1]
GO
/****** Object:  Table [dbo].[PreAdmissionOnLineRegister]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PreAdmissionOnLineRegister](
	[OnlineRegisterId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](150) NULL,
	[Passwords] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[OnlineRegisterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PreAdmissionMotherRegister]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[PreAdmissionMotherRegister](
	[MotherAdmissionId] [int] IDENTITY(1,1) NOT NULL,
	[MotherFirstname] [varchar](50) NULL,
	[MotherMiddleName] [varchar](50) NULL,
	[MotherLastName] [varchar](50) NULL,
	[MotherDOB] [datetime] NULL,
	[MotherQualification] [varchar](50) NULL,
	[MotherOccupation] [varchar](100) NULL,
	[MotherMobileNo] [bigint] NULL,
	[MotherEmail] [varchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[MotherAdmissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PreAdmissionGuardianRegister]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[PreAdmissionGuardianRegister](
	[GuardianAdmissionId] [int] IDENTITY(1,1) NOT NULL,
	[GuardianFirstname] [varchar](50) NULL,
	[GuardianMiddleName] [varchar](50) NULL,
	[GuardianLastName] [varchar](50) NULL,
	[GuardianDOB] [datetime] NULL,
	[GuardianQualification] [varchar](50) NULL,
	[GuardianOccupation] [varchar](100) NULL,
	[GuardianMobileNo] [bigint] NULL,
	[GuardianEmail] [varchar](150) NULL,
	[GuardianCompanyName] [varchar](200) NULL,
	[GuardianIncome] [bigint] NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[PreAdmissionGuardianRegister] ADD [GuRelationshipToChild] [varchar](50) NULL
ALTER TABLE [dbo].[PreAdmissionGuardianRegister] ADD [GuardianGender] [varchar](10) NULL
ALTER TABLE [dbo].[PreAdmissionGuardianRegister] ADD [GuardianRequried] [varchar](50) NULL
ALTER TABLE [dbo].[PreAdmissionGuardianRegister] ADD PRIMARY KEY CLUSTERED 
(
	[GuardianAdmissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PreAdmissionFatherRegister]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[PreAdmissionFatherRegister](
	[FatherAdmissionId] [int] IDENTITY(1,1) NOT NULL,
	[FatherFirstName] [varchar](50) NULL,
	[FatherMiddleName] [varchar](50) NULL,
	[FatherLastName] [varchar](50) NULL,
	[FatherDOB] [datetime] NULL,
	[FatherQualification] [varchar](50) NULL,
	[FatherOccupation] [varchar](100) NULL,
	[FatherMobileNo] [bigint] NULL,
	[FatherEmail] [varchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[FatherAdmissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FuelType]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FuelType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FuelType] [varchar](30) NULL,
	[status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HostelBlock]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HostelBlock](
	[BlockId] [int] IDENTITY(1,1) NOT NULL,
	[BlockName] [varchar](50) NULL,
	[HostelType] [varchar](15) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[Status] [varchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[BlockId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[BlockName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GradeType]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GradeType](
	[GradeTypeId] [int] IDENTITY(1,1) NOT NULL,
	[GradeTypeName] [varchar](25) NULL,
	[GradeTypeStatus] [bit] NULL,
 CONSTRAINT [PkGradeType_GradeId] PRIMARY KEY CLUSTERED 
(
	[GradeTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FeeCategory]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FeeCategory](
	[FeeCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[FeeCategoryName] [varchar](150) NOT NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PkFeeCategory_FeeCategoryId] PRIMARY KEY CLUSTERED 
(
	[FeeCategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UkFeeCategory_FeeName] UNIQUE NONCLUSTERED 
(
	[FeeCategoryName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Feature]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Feature](
	[FeatureId] [int] IDENTITY(1,1) NOT NULL,
	[FaetureType] [varchar](200) NOT NULL,
	[FeatureLink] [varchar](500) NOT NULL,
	[FeatureStatus] [bit] NOT NULL,
	[FeatureDetails] [varchar](300) NULL,
	[ControllerName] [varchar](100) NULL,
	[GroupName] [varchar](100) NULL,
	[Priority] [int] NULL,
	[GroupPriority] [int] NULL,
 CONSTRAINT [OtherFeature_FeatureId] PRIMARY KEY CLUSTERED 
(
	[FeatureId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExamTimeSchedule]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExamTimeSchedule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScheduleName] [varchar](30) NULL,
	[StartTime] [time](7) NULL,
	[EndTime] [time](7) NULL,
	[Status] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Facilities]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Facilities](
	[FacilityId] [int] IDENTITY(1,1) NOT NULL,
	[FacilityName] [varchar](70) NOT NULL,
	[FacilityDescription] [varchar](300) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PkFacilities_FacilityId] PRIMARY KEY CLUSTERED 
(
	[FacilityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExtraActivities]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExtraActivities](
	[ActivityId] [int] IDENTITY(1,1) NOT NULL,
	[Activity] [varchar](100) NOT NULL,
	[ActivityDescription] [varchar](300) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PkExtraActivities_ActivityId] PRIMARY KEY CLUSTERED 
(
	[ActivityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EnrollmentUserConfirmation]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EnrollmentUserConfirmation](
	[UserConfirmationId] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationNumber] [varchar](50) NULL,
	[PrimaryUserEmail] [varchar](150) NULL,
	[RequestSend] [varchar](50) NULL,
	[ContactNumber] [bigint] NULL,
	[StatusFlag] [varchar](50) NULL,
	[Descriptions] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserConfirmationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeType]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeType](
	[EmployeeTypeId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeTypes] [varchar](150) NOT NULL,
 CONSTRAINT [PkEmployeeType_EmployeeTypeId] PRIMARY KEY CLUSTERED 
(
	[EmployeeTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Exam]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Exam](
	[ExamId] [int] IDENTITY(1,1) NOT NULL,
	[ExamName] [varchar](100) NULL,
 CONSTRAINT [PkExam_ExamId] PRIMARY KEY CLUSTERED 
(
	[ExamId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AcademicYear]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AcademicYear](
	[AcademicYearId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYear] [varchar](10) NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[StudentOnRole] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PkAcademicYear_AcademicYearId] PRIMARY KEY CLUSTERED 
(
	[AcademicYearId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UkAcademicYear_AcademicYear] UNIQUE NONCLUSTERED 
(
	[AcademicYear] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ClassRooms]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClassRooms](
	[ClassRoomId] [int] IDENTITY(1,1) NOT NULL,
	[RoomNumber] [varchar](20) NULL,
	[Capacity] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [Pk_ClassRooms_ClassRoomId] PRIMARY KEY CLUSTERED 
(
	[ClassRoomId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DayOrder]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DayOrder](
	[dayorderId] [int] IDENTITY(1,1) NOT NULL,
	[value] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[dayorderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[currency]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[currency](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[country] [nvarchar](100) NULL,
	[currency] [nvarchar](100) NULL,
	[code] [nvarchar](100) NULL,
	[symbol] [nvarchar](100) NULL,
 CONSTRAINT [Pk_Currencies_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Subject]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Subject](
	[SubjectId] [int] IDENTITY(1,1) NOT NULL,
	[SubjectName] [varchar](50) NOT NULL,
	[Status] [bit] NULL,
	[NonAcademicSubject] [varchar](10) NULL,
 CONSTRAINT [PkSubject_SubjectId] PRIMARY KEY CLUSTERED 
(
	[SubjectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VehicleType]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VehicleType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VehicleType] [varchar](50) NULL,
	[status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserLog]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserLog](
	[UserLogId] [int] IDENTITY(1,1) NOT NULL,
	[SessionId] [varchar](200) NULL,
	[UserId] [varchar](100) NULL,
	[UserType] [varchar](100) NULL,
	[IpAddress] [varchar](100) NULL,
	[LoginTime] [datetime] NULL,
	[LogoutTime] [datetime] NULL,
	[UserRegId] [int] NULL,
 CONSTRAINT [PkUserLog_UserLogId] PRIMARY KEY CLUSTERED 
(
	[UserLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblTransferStatus]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTransferStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TransferReason] [varchar](30) NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TimeSchedule]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TimeSchedule](
	[TimeScheduleId] [int] IDENTITY(1,1) NOT NULL,
	[Schedule] [varchar](100) NULL,
	[NoPeriods] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [Pk_TimeSchedules_TimeScheduleId] PRIMARY KEY CLUSTERED 
(
	[TimeScheduleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblMotherResetPassword]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMotherResetPassword](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [int] NULL,
	[UserName] [varchar](70) NULL,
	[ResetRequestDateTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblGuardianResetPassword]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblGuardianResetPassword](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [int] NULL,
	[UserName] [varchar](70) NULL,
	[ResetRequestDateTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblFatherResetPassword]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFatherResetPassword](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [int] NULL,
	[UserName] [varchar](70) NULL,
	[ResetRequestDateTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblCountry]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCountry](
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](255) NULL,
	[ISO3166_2LetterCode] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblConductStatus]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblConductStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Conduct] [varchar](30) NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblCommunity]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCommunity](
	[CommunityId] [int] IDENTITY(1,1) NOT NULL,
	[CommunityName] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[CommunityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblBloodGroup]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBloodGroup](
	[BloodId] [int] IDENTITY(1,1) NOT NULL,
	[BloodGroupName] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[BloodId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblAttendanceStatus]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAttendanceStatus](
	[AttendanceStatusId] [int] IDENTITY(1,1) NOT NULL,
	[AttendanceStatusName] [varchar](250) NULL,
	[AttendanceStatusValue] [varchar](250) NULL,
	[AttendanceStatus] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[AttendanceStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleType] [varchar](200) NOT NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PkRoles_RoleId] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UkRoles_RoleType] UNIQUE NONCLUSTERED 
(
	[RoleType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PrimaryUserRegister]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrimaryUserRegister](
	[PrimaryUserRegisterId] [int] IDENTITY(1,1) NOT NULL,
	[PrimaryUser] [varchar](50) NULL,
	[PrimaryUserName] [varchar](50) NULL,
	[PrimaryUserEmail] [varchar](50) NULL,
	[CompanyAddress1] [varchar](250) NULL,
	[CompanyAddress2] [varchar](250) NULL,
	[CompanyCity] [varchar](50) NULL,
	[CompanyState] [varchar](50) NULL,
	[CompanyCountry] [int] NULL,
	[CompanyPostelcode] [bigint] NULL,
	[CompanyContact] [bigint] NULL,
	[EmergencyContactPersonName] [varchar](200) NULL,
	[EmergencyContactNumber] [bigint] NULL,
	[ContactPersonRelationship] [varchar](100) NULL,
	[PrimaryUserId] [varchar](50) NULL,
	[PrimaryUserPwd] [varchar](50) NULL,
	[UserCompanyname] [varchar](150) NULL,
	[WorkSameSchool] [varchar](10) NULL,
	[TotalIncome] [bigint] NULL,
	[PrimaryUserContactNo] [bigint] NULL,
	[Salt] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[PrimaryUserRegisterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PreAdmissionPrimaryUserResetPassword]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PreAdmissionPrimaryUserResetPassword](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [int] NULL,
	[ResetRequestDateTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Months]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Months](
	[MonthsId] [int] NOT NULL,
	[MonthsName] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[MonthsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsTrends]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsTrends](
	[NewsTrendsId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[NewsDate] [datetime] NULL,
	[Heading] [varchar](100) NULL,
	[Description] [varchar](300) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_NewsTrends_NewsTrendsId] PRIMARY KEY CLUSTERED 
(
	[NewsTrendsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PreAdmissionPrimaryUserRegister]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[PreAdmissionPrimaryUserRegister](
	[PrimaryUserAdmissionId] [int] IDENTITY(1,1) NOT NULL,
	[OnlineRegisterId] [int] NULL,
	[PrimaryUser] [varchar](50) NULL,
	[PrimaryUserName] [varchar](50) NULL,
	[PrimaryUserEmail] [varchar](50) NULL,
	[CompanyAddress1] [varchar](250) NULL,
	[CompanyAddress2] [varchar](250) NULL,
	[CompanyCity] [varchar](50) NULL,
	[CompanyState] [varchar](50) NULL,
	[CompanyCountry] [int] NULL,
	[CompanyPostelcode] [bigint] NULL,
	[CompanyContact] [bigint] NULL,
	[EmergencyContactPersonName] [varchar](200) NULL,
	[EmergencyContactNumber] [bigint] NULL,
	[ContactPersonRelationship] [varchar](100) NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[PreAdmissionPrimaryUserRegister] ADD [PrimaryUserPwd] [varchar](50) NULL
ALTER TABLE [dbo].[PreAdmissionPrimaryUserRegister] ADD [UserCompanyname] [varchar](150) NULL
ALTER TABLE [dbo].[PreAdmissionPrimaryUserRegister] ADD [WorkSameSchool] [varchar](10) NULL
ALTER TABLE [dbo].[PreAdmissionPrimaryUserRegister] ADD [TotalIncome] [bigint] NULL
ALTER TABLE [dbo].[PreAdmissionPrimaryUserRegister] ADD [PrimaryUserContactNo] [bigint] NULL
ALTER TABLE [dbo].[PreAdmissionPrimaryUserRegister] ADD PRIMARY KEY CLUSTERED 
(
	[PrimaryUserAdmissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SchoolSettings]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SchoolSettings](
	[SettingsId] [int] IDENTITY(1,1) NOT NULL,
	[SchoolName] [varchar](200) NULL,
	[SchoolLogo] [varchar](50) NULL,
	[AddressLine1] [varchar](100) NULL,
	[AddressLine2] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](100) NULL,
	[ZIP] [varchar](20) NULL,
	[Country] [varchar](100) NULL,
	[Phone1] [varchar](20) NULL,
	[Phone2] [varchar](20) NULL,
	[Mobile1] [bigint] NULL,
	[Mobile2] [bigint] NULL,
	[Email] [varchar](200) NULL,
	[Fax] [varchar](20) NULL,
	[Website] [varchar](200) NULL,
	[CurrencyType] [int] NULL,
	[StudentIdFormat] [varchar](100) NULL,
	[StaffIDFormat] [varchar](100) NULL,
	[Non_StaffIDFormat] [varchar](100) NULL,
	[DayOrder] [int] NULL,
	[MarkFormat] [varchar](25) NULL,
	[ServicesTax] [decimal](19, 4) NULL,
	[VAT] [decimal](19, 4) NULL,
	[FormFormat] [varchar](25) NULL,
	[ParentIDFormat] [varchar](50) NULL,
	[SchoolCode] [varchar](10) NULL,
 CONSTRAINT [Pk_SchoolSettings_SettingsId] PRIMARY KEY CLUSTERED 
(
	[SettingsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SchoolAchievements]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SchoolAchievements](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[DateOfAchievement] [datetime] NULL,
	[Achievement] [varchar](250) NULL,
	[Descriptions] [varchar](250) NULL,
	[PeopleInvolved] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Grade]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Grade](
	[GradeId] [int] IDENTITY(1,1) NOT NULL,
	[GradeTypeId] [int] NULL,
	[GradeName] [varchar](20) NULL,
	[MinRange] [int] NULL,
	[MaxRange] [int] NULL,
	[GradeDescribtion] [varchar](250) NULL,
 CONSTRAINT [PkGrade_GradeId] PRIMARY KEY CLUSTERED 
(
	[GradeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Term]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Term](
	[TermId] [int] IDENTITY(1,1) NOT NULL,
	[TermName] [varchar](25) NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[AcademicYearId] [int] NULL,
 CONSTRAINT [PkTerm_TermId] PRIMARY KEY CLUSTERED 
(
	[TermId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StudentUserActivity]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentUserActivity](
	[StudentActivityId] [int] IDENTITY(1,1) NOT NULL,
	[UserLogId] [int] NULL,
	[Activity] [varchar](50) NULL,
	[Features] [varchar](150) NULL,
	[FeaturesName] [varchar](50) NULL,
	[Time] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[StudentActivityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Class]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Class](
	[ClassId] [int] IDENTITY(1,1) NOT NULL,
	[ClassType] [varchar](50) NULL,
	[Status] [bit] NULL,
	[TimeScheduleId] [int] NULL,
 CONSTRAINT [Pk_Class_ClassId] PRIMARY KEY CLUSTERED 
(
	[ClassId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ClassTimeSchedule]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClassTimeSchedule](
	[ClassTimeSchedule1] [int] IDENTITY(1,1) NOT NULL,
	[TimeScheduleId] [int] NULL,
	[ClassId] [int] NULL,
 CONSTRAINT [PK_ClassTimeSchedule] PRIMARY KEY CLUSTERED 
(
	[ClassTimeSchedule1] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Fk_ClassTimeSchedule_ClassId] ON [dbo].[ClassTimeSchedule] 
(
	[ClassId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FK_fkClassTimeSchedule_TimeScheduleId] ON [dbo].[ClassTimeSchedule] 
(
	[TimeScheduleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeDesignation]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeDesignation](
	[EmployeeDesignationId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeTypeId] [int] NULL,
	[Designation] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[EmployeeDesignationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Announcement]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Announcement](
	[AnnouncementId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[AnnouncementDate] [datetime] NOT NULL,
	[AnnouncementName] [varchar](70) NOT NULL,
	[AnnouncementDescription] [varchar](300) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PkAnnouncement_AnnouncementId] PRIMARY KEY CLUSTERED 
(
	[AnnouncementId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Event]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Event](
	[EventId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[EventDate] [datetime] NOT NULL,
	[EventName] [varchar](70) NOT NULL,
	[EventDecribtion] [varchar](300) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PkEvent_EventId] PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeUserActivity]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeUserActivity](
	[EmployeeActivityId] [int] IDENTITY(1,1) NOT NULL,
	[UserLogId] [int] NULL,
	[Activity] [varchar](50) NULL,
	[Features] [varchar](150) NULL,
	[FeaturesName] [varchar](50) NULL,
	[Time] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EmployeeActivityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FacilityIncharge]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FacilityIncharge](
	[FacilityInchargeId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[FacilityId] [int] NULL,
	[EmployeeRegisterId] [int] NULL,
	[Note] [varchar](200) NULL,
	[EmployeeId] [varchar](100) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [Pk_FacilityIncharge_FacilityInchargeId] PRIMARY KEY CLUSTERED 
(
	[FacilityInchargeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HostelFeesCategory]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HostelFeesCategory](
	[categoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](50) NULL,
	[FeesAmount] [money] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[Status] [varchar](15) NULL,
	[Tax] [money] NULL,
	[AcademicYear] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[categoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HostelRoom]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HostelRoom](
	[RoomId] [int] IDENTITY(1,1) NOT NULL,
	[BlockId] [int] NULL,
	[RoomName] [varchar](50) NULL,
	[RoomCapacity] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[Status] [varchar](15) NULL,
	[RoomAvailablity] [int] NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[HostelRoom] ADD [RoomType] [varchar](50) NULL
ALTER TABLE [dbo].[HostelRoom] ADD PRIMARY KEY CLUSTERED 
(
	[RoomId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Menu]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Menu](
	[MenuId] [int] IDENTITY(1,1) NOT NULL,
	[FeatureId] [int] NULL,
	[MenuType] [varchar](100) NULL,
	[MenuLink] [varchar](300) NULL,
	[MenuStatus] [bit] NULL,
 CONSTRAINT [Pk_Menu_MenuId] PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MapRoleFeature]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MapRoleFeature](
	[MapId] [int] IDENTITY(1,1) NOT NULL,
	[FeatureId] [int] NULL,
	[RoleId] [int] NULL,
	[status] [bit] NULL,
 CONSTRAINT [Pk_MapRoleFeature_MapId] PRIMARY KEY CLUSTERED 
(
	[MapId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PeriodsSchedule]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PeriodsSchedule](
	[PeriodScheduleId] [int] IDENTITY(1,1) NOT NULL,
	[TimeScheduleId] [int] NULL,
	[Periods] [varchar](50) NULL,
	[Type] [varchar](10) NULL,
	[StartTime] [time](7) NULL,
	[EndTime] [time](7) NULL,
 CONSTRAINT [Pk_PeriodsSchedule_PeriodsScheduleId] PRIMARY KEY CLUSTERED 
(
	[PeriodScheduleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ParentUserActivity]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ParentUserActivity](
	[ParentActivityId] [int] IDENTITY(1,1) NOT NULL,
	[UserLogId] [int] NULL,
	[Activity] [varchar](50) NULL,
	[Features] [varchar](150) NULL,
	[FeaturesName] [varchar](50) NULL,
	[Time] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ParentActivityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Section]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Section](
	[SectionId] [int] IDENTITY(1,1) NOT NULL,
	[ClassId] [int] NULL,
	[SectionName] [varchar](10) NOT NULL,
 CONSTRAINT [PkSection_SectionId] PRIMARY KEY CLUSTERED 
(
	[SectionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OtherEmployee]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OtherEmployee](
	[EmployeeRegisterId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [varchar](100) NULL,
	[EmployeeName] [varchar](150) NULL,
	[UserName] [varchar](250) NULL,
	[Password] [varchar](250) NULL,
	[Gender] [varchar](10) NULL,
	[DOB] [datetime] NULL,
	[BloodGroup] [varchar](10) NULL,
	[Email] [varchar](150) NULL,
	[Community] [varchar](100) NULL,
	[Religion] [varchar](20) NULL,
	[Nationality] [varchar](20) NULL,
	[Contact] [bigint] NULL,
	[EmergencyContactPerson] [varchar](150) NULL,
	[EmergencyContactNumber] [bigint] NULL,
	[Address] [varchar](250) NULL,
	[City] [varchar](30) NULL,
	[State] [varchar](30) NULL,
	[Expertise] [varchar](200) NULL,
	[Qualification] [varchar](150) NULL,
	[Experience] [varchar](50) NULL,
	[DOR] [datetime] NULL,
	[DOJ] [datetime] NULL,
	[DOL] [datetime] NULL,
	[EmployeeStatus] [bit] NULL,
	[ImgFile] [varchar](200) NULL,
	[Country] [varchar](100) NULL,
	[Middlename] [varchar](50) NULL,
	[Lastname] [varchar](50) NULL,
	[MaritalStatus] [varchar](50) NULL,
	[Mobile] [bigint] NULL,
	[Phone] [varchar](30) NULL,
	[Address2] [varchar](200) NULL,
	[ZipCode] [varchar](15) NULL,
	[PermanentAddress1] [varchar](200) NULL,
	[PermanentAddress2] [varchar](200) NULL,
	[PermanentCity] [varchar](100) NULL,
	[PermanentState] [varchar](100) NULL,
	[PermanentCountry] [varchar](200) NULL,
	[PermanentZipCode] [varchar](15) NULL,
	[EmergencyContactpersonRelationship] [varchar](150) NULL,
	[Salary] [money] NULL,
	[PAN] [varchar](15) NULL,
	[AccountNo] [varchar](100) NULL,
	[AccountName] [varchar](100) NULL,
	[BankName] [varchar](100) NULL,
	[ResumeFile] [varchar](50) NULL,
	[DegreeCertificateFile] [varchar](100) NULL,
	[RationCard] [varchar](150) NULL,
	[VoterId] [varchar](150) NULL,
	[EmployeeTypeId] [int] NULL,
	[EmployeeDesignationId] [int] NULL,
	[LicenseNo] [varchar](50) NULL,
	[LicenseExpireDate] [datetime] NULL,
 CONSTRAINT [PkOtherEmployee_EmployeeRegisterId] PRIMARY KEY CLUSTERED 
(
	[EmployeeRegisterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UkOtherEmployee_EmployeeId] UNIQUE NONCLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UkOtherEmployee_UserName] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Fee]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Fee](
	[FeeId] [int] IDENTITY(1,1) NOT NULL,
	[ClassId] [int] NULL,
	[PaymentTypeId] [int] NULL,
	[Amount] [money] NULL,
	[AcademicYearId] [int] NULL,
	[LastDate] [datetime] NULL,
	[Tax] [money] NULL,
	[FeeCategoryId] [int] NULL,
	[Total] [money] NULL,
	[ReFundAmount] [money] NULL,
	[NetAmount] [money] NULL,
 CONSTRAINT [PkFee_FeeId] PRIMARY KEY CLUSTERED 
(
	[FeeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AsignClassVacancy]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AsignClassVacancy](
	[VacancyId] [int] IDENTITY(1,1) NOT NULL,
	[ClassId] [int] NULL,
	[Vacancy] [int] NULL,
	[AcademicYearId] [int] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[VacancyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssignGradeTypeToClass]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignGradeTypeToClass](
	[GradeClassId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[GradeTypeId] [int] NULL,
 CONSTRAINT [Pk_AssignGradeTypeToClass_GradeClassId] PRIMARY KEY CLUSTERED 
(
	[GradeClassId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TechEmployee]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TechEmployee](
	[EmployeeRegisterId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [varchar](100) NULL,
	[EmployeeName] [varchar](150) NOT NULL,
	[UserName] [varchar](250) NULL,
	[Password] [varchar](250) NULL,
	[Gender] [varchar](10) NULL,
	[DOB] [datetime] NULL,
	[BloodGroup] [varchar](10) NULL,
	[Email] [varchar](150) NULL,
	[Community] [varchar](100) NULL,
	[Religion] [varchar](20) NULL,
	[Nationality] [varchar](20) NULL,
	[Mobile] [bigint] NULL,
	[EmergencyContactPerson] [varchar](150) NULL,
	[EmergencyContactNumber] [bigint] NULL,
	[AddressLine1] [varchar](250) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](100) NULL,
	[Expertise] [varchar](200) NULL,
	[Qualification] [varchar](150) NULL,
	[Experience] [varchar](50) NULL,
	[DOR] [datetime] NULL,
	[DOJ] [datetime] NULL,
	[DOL] [datetime] NULL,
	[EmployeeStatus] [bit] NULL,
	[ImgFile] [varchar](200) NULL,
	[Country] [varchar](100) NULL,
	[MaritalStatus] [varchar](20) NULL,
	[Phone] [varchar](30) NULL,
	[AddressLine2] [varchar](150) NULL,
	[ResumeFile] [varchar](50) NULL,
	[Salary] [money] NULL,
	[DegreeCertificateFile] [varchar](100) NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Contact] [varchar](50) NULL,
	[AccountNo] [varchar](100) NULL,
	[AccountName] [varchar](100) NULL,
	[BankName] [varchar](100) NULL,
	[ZipCode] [varchar](15) NULL,
	[PAN] [varchar](15) NULL,
	[PermanentAddress1] [varchar](200) NULL,
	[PermanentAddress2] [varchar](200) NULL,
	[PermanentCity] [varchar](100) NULL,
	[PermanentState] [varchar](100) NULL,
	[PermanentCountry] [varchar](200) NULL,
	[PermanentZipCode] [varchar](15) NULL,
	[EmergencyContactpersonRelationship] [varchar](150) NULL,
	[RationCard] [varchar](150) NULL,
	[VoterId] [varchar](150) NULL,
	[EmployeeTypeId] [int] NULL,
	[EmployeeDesignationId] [int] NULL,
 CONSTRAINT [TechEmployee_EmployeeRegisterId] PRIMARY KEY CLUSTERED 
(
	[EmployeeRegisterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UkTechEmployee_EmployeeId] UNIQUE NONCLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UkTechEmployee_UserName] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransportDestination]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransportDestination](
	[DestinationId] [int] IDENTITY(1,1) NOT NULL,
	[DestinationName] [varchar](100) NULL,
	[status] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[DestinationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TermFee]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TermFee](
	[TermFeeID] [int] IDENTITY(1,1) NOT NULL,
	[TermId] [int] NULL,
	[FeeCategoryId] [int] NULL,
	[ClassId] [int] NULL,
	[PaymentTypeId] [int] NULL,
	[Amount] [money] NULL,
	[LastDate] [datetime] NULL,
	[AcademicYearId] [int] NULL,
	[FeeId] [int] NULL,
	[Tax] [money] NULL,
	[Total] [money] NULL,
 CONSTRAINT [PkTermFee_TermFeeID] PRIMARY KEY CLUSTERED 
(
	[TermFeeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TechEmployeeProfessional]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TechEmployeeProfessional](
	[TechEmployeeProfessionalId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[Cours] [varchar](100) NULL,
	[CertificateName] [varchar](150) NULL,
	[CertificateFile] [varchar](100) NULL,
 CONSTRAINT [Pk_TechEmployeeProfessional_TechEmployeeProfessionalId] PRIMARY KEY CLUSTERED 
(
	[TechEmployeeProfessionalId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TechEmployeeEmployment]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TechEmployeeEmployment](
	[TechEmployeeEmploymentId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[Institute] [varchar](100) NULL,
	[Designation] [varchar](50) NULL,
	[Expertise] [varchar](150) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[TotalYears] [int] NULL,
	[OtherEmployeeRegisterId] [int] NULL,
 CONSTRAINT [Pk_TechEmployeeEmployment_TechEmployeeEmploymentId] PRIMARY KEY CLUSTERED 
(
	[TechEmployeeEmploymentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TechEmployeeEducation]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TechEmployeeEducation](
	[TechEmployeeEducationId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[Cours] [varchar](100) NULL,
	[Institute] [varchar](150) NULL,
	[University] [varchar](150) NULL,
	[YearOfCompletion] [varchar](10) NULL,
	[Mark] [float] NULL,
	[OtherEmployeeRegisterId] [int] NULL,
 CONSTRAINT [Pk_TechEmployeeEducation_TechEmployeeEducationId] PRIMARY KEY CLUSTERED 
(
	[TechEmployeeEducationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TechEmployeeClassSubject]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TechEmployeeClassSubject](
	[TechEmployeeClassSubjectId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[ClassId] [int] NULL,
	[SubjectId] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [Pk_TechEmployeeClassSubject_TechEmployeeClassSubjectId] PRIMARY KEY CLUSTERED 
(
	[TechEmployeeClassSubjectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SingleStaffCommunication]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SingleStaffCommunication](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[EmployeeTypeId] [int] NULL,
	[EmployeeRegisterId] [int] NULL,
	[DateOfAnnouncement] [datetime] NULL,
	[Title] [varchar](100) NULL,
	[Description] [varchar](800) NULL,
	[FileName] [varchar](50) NULL,
	[MarkedBy] [int] NULL,
	[MarkedDate] [datetime] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SectionStrength]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SectionStrength](
	[SectionStrengthId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[Strength] [int] NULL,
	[Status] [bit] NULL,
	[ClassRoomId] [int] NULL,
 CONSTRAINT [Pk_SectionStrength_SectionStrengthId] PRIMARY KEY CLUSTERED 
(
	[SectionStrengthId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTechEmployeeResetPassword]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTechEmployeeResetPassword](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [int] NULL,
	[ResetRequestDateTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubjectNotes]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubjectNotes](
	[NotesId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[SubjectId] [int] NULL,
	[DateOfNotes] [datetime] NULL,
	[Topic] [varchar](250) NULL,
	[Descriptions] [varchar](250) NULL,
	[NotesFileName] [varchar](250) NULL,
	[NotesStatus] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[NotesId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PreAdmissionStudentRegister]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PreAdmissionStudentRegister](
	[StudentAdmissionId] [int] IDENTITY(1,1) NOT NULL,
	[OnlineRegisterId] [int] NULL,
	[OfflineApplicationID] [varchar](50) NULL,
	[StuFirstName] [varchar](50) NULL,
	[StuMiddlename] [varchar](50) NULL,
	[StuLastname] [varchar](50) NULL,
	[Gender] [varchar](10) NULL,
	[DOB] [datetime] NULL,
	[PlaceOfBirth] [varchar](20) NULL,
	[Community] [int] NULL,
	[Religion] [varchar](20) NULL,
	[Nationality] [varchar](20) NULL,
	[FirstLanguage] [varchar](50) NULL,
	[BloodGroup] [int] NULL,
	[Height] [varchar](20) NULL,
	[Weights] [varchar](20) NULL,
	[IdentificationMark] [varchar](50) NULL,
	[LocAddress1] [varchar](250) NULL,
	[LocAddress2] [varchar](250) NULL,
	[LocCity] [varchar](50) NULL,
	[LocState] [varchar](30) NULL,
	[LocCountry] [int] NULL,
	[LocPostelcode] [bigint] NULL,
	[Distance] [varchar](10) NULL,
	[Email] [varchar](150) NULL,
	[Contact] [bigint] NULL,
	[PerAddress1] [varchar](250) NULL,
	[PerAddress2] [varchar](250) NULL,
	[PerCity] [varchar](50) NULL,
	[PerState] [varchar](50) NULL,
	[PerCountry] [int] NULL,
	[PerPostelcode] [bigint] NULL,
	[PreSchholName] [varchar](250) NULL,
	[PreSchoolMedium] [varchar](50) NULL,
	[PreSchoolClass] [varchar](50) NULL,
	[PreSchoolMark] [varchar](10) NULL,
	[PreSchoolFromdate] [datetime] NULL,
	[PreSchoolToDate] [datetime] NULL,
	[StudentPhoto] [varchar](200) NULL,
	[IncomeCertificate] [varchar](200) NULL,
	[BirthCertificate] [varchar](200) NULL,
	[CommunityCertificate] [varchar](200) NULL,
	[AddressCertificate] [varchar](200) NULL,
	[TransferCertificate] [varchar](200) NULL,
	[MarkSheet] [varchar](200) NULL,
	[AttestedCertificate] [varchar](200) NULL,
	[NoOfSibling] [int] NULL,
	[AcademicyearId] [int] NULL,
	[AdmissionClass] [int] NULL,
	[Statusflag] [varchar](30) NULL,
	[SiblingStatus] [varchar](10) NULL,
	[ApplicationSource] [varchar](50) NULL,
	[EnrolledBy] [int] NULL,
	[DateOfApply] [datetime] NULL,
	[GuardianRequried] [varchar](10) NULL,
	[SendEmailStatus] [varchar](30) NULL,
	[EmergencyContactPersonName] [varchar](200) NULL,
	[EmergencyContactNumber] [bigint] NULL,
	[ContactPersonRelationship] [varchar](100) NULL,
 CONSTRAINT [PK__StudentR__7FE0AEC405F8DC4F] PRIMARY KEY CLUSTERED 
(
	[StudentAdmissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RollNumberFormat]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RollNumberFormat](
	[RollNumberFormatId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[RollNumber] [varchar](20) NULL,
	[Status] [bit] NULL,
	[Assigned] [bit] NULL,
 CONSTRAINT [PK_RollNumberFormat_RollNumberId] PRIMARY KEY CLUSTERED 
(
	[RollNumberFormatId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [Fk_RollNumberFormat_RollNumber] UNIQUE NONCLUSTERED 
(
	[RollNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VehicleDetails]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VehicleDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RegistrationNumber] [varchar](50) NULL,
	[RegistrationDate] [datetime] NULL,
	[OwnerFirstName] [varchar](50) NULL,
	[OwnerLastName] [varchar](50) NULL,
	[Address1] [varchar](200) NULL,
	[Address2] [varchar](200) NULL,
	[City] [varchar](50) NULL,
	[Country] [int] NULL,
	[Pincode] [varchar](30) NULL,
	[BoughtOn] [datetime] NULL,
	[VehicleType] [int] NULL,
	[Manufacture] [varchar](50) NULL,
	[Color] [varchar](30) NULL,
	[EngineNumber] [varchar](50) NULL,
	[txt_ChaseNum] [varchar](50) NULL,
	[FuelType] [int] NULL,
	[FuelCapacity] [varchar](30) NULL,
	[SeatCapacity] [int] NULL,
	[RegisterUptoDate] [datetime] NULL,
	[TaxValidUpto] [varchar](50) NULL,
	[InsuranceNumber] [varchar](50) NULL,
	[InsuranceValidFromDate] [datetime] NULL,
	[InsuranceValidUpto] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[status] [bit] NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[VehicleDetails] ADD [State] [varchar](30) NULL
ALTER TABLE [dbo].[VehicleDetails] ADD PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblOtherEmployeeResetPassword]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblOtherEmployeeResetPassword](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [int] NULL,
	[ResetRequestDateTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Student](
	[StudentRegisterId] [int] IDENTITY(1,1) NOT NULL,
	[AdmissionId] [int] NULL,
	[StudentId] [varchar](100) NULL,
	[Salt] [varchar](10) NULL,
	[Password] [varchar](100) NULL,
	[FirstName] [varchar](100) NULL,
	[MiddleName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[Gender] [varchar](10) NULL,
	[DOB] [datetime] NULL,
	[PlaceOfBirth] [varchar](100) NULL,
	[StudentBloodGroup] [int] NULL,
	[StudentCommunity] [int] NULL,
	[Religion] [varchar](20) NULL,
	[Nationality] [varchar](20) NULL,
	[Height] [varchar](50) NULL,
	[Weights] [varchar](50) NULL,
	[IdentificationMark] [varchar](250) NULL,
	[LocalAddress1] [varchar](250) NULL,
	[LocalAddress2] [varchar](250) NULL,
	[LocalCity] [varchar](100) NULL,
	[LocalState] [varchar](100) NULL,
	[LocalCountry] [int] NULL,
	[LocalPostalCode] [bigint] NULL,
	[Distance] [varchar](100) NULL,
	[PermanentAddress1] [varchar](250) NULL,
	[PermanentAddress2] [varchar](250) NULL,
	[PermanentCity] [varchar](100) NULL,
	[PermanentState] [varchar](100) NULL,
	[PermanentCountry] [int] NULL,
	[PermanentPostalCode] [bigint] NULL,
	[Email] [varchar](150) NULL,
	[Contact] [bigint] NULL,
	[EmergencyContactPerson] [varchar](150) NULL,
	[EmergencyContactNumber] [bigint] NULL,
	[ContactPersonRelationship] [varchar](100) NULL,
	[PreviousSchoolName] [varchar](250) NULL,
	[BoardOfPreviousSchool] [varchar](200) NULL,
	[MediumOfStudy] [varchar](200) NULL,
	[PreviousSchoolClass] [varchar](200) NULL,
	[PreviousSchoolMark] [varchar](50) NULL,
	[PreviousSchoolFromDate] [datetime] NULL,
	[PreviousSchoolToDate] [datetime] NULL,
	[IncomeCertificate] [varchar](100) NULL,
	[BirthCertificate] [varchar](100) NULL,
	[CommunityCertificate] [varchar](100) NULL,
	[AddressCertificate] [varchar](100) NULL,
	[TransferCertificate] [varchar](100) NULL,
	[DOR] [datetime] NULL,
	[DOJ] [datetime] NULL,
	[DOL] [datetime] NULL,
	[StudentStatus] [bit] NULL,
	[Photo] [varchar](200) NULL,
	[FirstLanguage] [varchar](100) NULL,
	[AcademicyearId] [int] NULL,
	[AdmissionClass] [int] NULL,
	[HaveGuardian] [varchar](50) NULL,
	[HaveSiblings] [varchar](50) NULL,
	[NoOfSibling] [int] NULL,
	[Statusflag] [varchar](100) NULL,
	[ApplicationSource] [varchar](100) NULL,
	[DateOfApply] [datetime] NULL,
	[EnrolledBy] [int] NULL,
 CONSTRAINT [PkStudent_StudentId] PRIMARY KEY CLUSTERED 
(
	[StudentRegisterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StaffCategoryCommunication]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StaffCategoryCommunication](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[EmployeeTypeId] [int] NULL,
	[DateOfAnnouncement] [datetime] NULL,
	[Title] [varchar](100) NULL,
	[Description] [varchar](800) NULL,
	[FileName] [varchar](50) NULL,
	[MarkedBy] [int] NULL,
	[MarkedDate] [datetime] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AssignFacultyToSubject]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignFacultyToSubject](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[SubjectId] [int] NULL,
	[EmployeeRegisterId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssignFacultyToExamRoom]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssignFacultyToExamRoom](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ExamId] [int] NULL,
	[ExamDate] [datetime] NULL,
	[TimeScheduleId] [int] NULL,
	[ClassRoomId] [int] NULL,
	[EmployeeRegisterId] [int] NULL,
	[Status] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AssignSubstitudeFaculty]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignSubstitudeFaculty](
	[SubstitudeId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[DateOfSubstitude] [datetime] NULL,
	[DayOrder] [int] NULL,
	[Period] [int] NULL,
	[SubjectId] [int] NULL,
	[ActualEmployeeRegisterId] [int] NULL,
	[SubstitudeEmployeeRegisterId] [int] NULL,
	[AssignedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[SubstitudeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssignSubjectToSection]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignSubjectToSection](
	[AssignSubjectToSectionId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[SubjectId] [int] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[AssignSubjectToSectionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssignSubjectToPeriod]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignSubjectToPeriod](
	[AssignSubjectToPeriodId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[Day] [int] NULL,
	[Period] [int] NULL,
	[SubjectId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AssignSubjectToPeriodId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Assignments]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Assignments](
	[AssignmentId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[SubjectId] [int] NULL,
	[AssignmentPostedDate] [datetime] NULL,
	[Assignment] [varchar](250) NULL,
	[Descriptions] [varchar](250) NULL,
	[AssignmentFileName] [varchar](250) NULL,
	[DateOfSubmission] [datetime] NULL,
	[AssignmentStatus] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[AssignmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AssignClassTeacher]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssignClassTeacher](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[EmployeeRegisterId] [int] NULL,
	[IsClassTeacher] [varchar](50) NULL,
	[SubjectId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ActivityIncharge]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActivityIncharge](
	[ActivityInchargeId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ActivityId] [int] NULL,
	[EmployeeRegisterId] [int] NULL,
	[EmployeeId] [varchar](100) NULL,
	[Note] [varchar](200) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [Pk_ActivityIncharge_ActivityInchargeId] PRIMARY KEY CLUSTERED 
(
	[ActivityInchargeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeSalaryConfiguration]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeSalaryConfiguration](
	[EmployeeSalaryConfigurationId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[OtherEmployeeregisterId] [int] NULL,
	[BasicSalary] [money] NULL,
	[DearnessAllowance] [money] NULL,
	[HouseRentAllowance] [money] NULL,
	[OtherAllowance] [money] NULL,
	[MedicalAllowance] [money] NULL,
	[GrossEarning] [money] NULL,
	[ProvidentFund] [money] NULL,
	[Esi] [money] NULL,
	[GrossDeduction] [money] NULL,
	[NetSalary] [money] NULL,
	[Increment] [money] NULL,
	[EnteredBy] [int] NULL,
	[EntryDate] [datetime] NULL,
	[Status] [bit] NULL,
	[IncrementId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[EmployeeSalaryConfigurationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeSalary]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeSalary](
	[EmployeeSalaryConfigurationId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[OtherEmployeeregisterId] [int] NULL,
	[ActualSalary] [money] NULL,
	[TotalNumberOfWorkingDays] [int] NULL,
	[NoOfDaysLeave] [int] NULL,
	[SalaryDeduction] [money] NULL,
	[NetSalary] [money] NULL,
	[EnteredBy] [int] NULL,
	[EntryDate] [datetime] NULL,
	[Status] [bit] NULL,
	[EmployeeTypeId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[EmployeeSalaryConfigurationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ELearning]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ELearning](
	[ELearningId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[SubjectId] [int] NULL,
	[DateOfPosted] [datetime] NULL,
	[Title] [varchar](250) NULL,
	[Descriptions] [varchar](500) NULL,
	[FileName] [varchar](250) NULL,
	[DateOfSubmission] [datetime] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ELearningId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeDailyAttendance]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeDailyAttendance](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[DateOfAttendance] [datetime] NULL,
	[AttendanceStatus] [varchar](20) NULL,
	[LeaveReason] [varchar](250) NULL,
	[AttendanceMarkedDate] [datetime] NULL,
	[SessionPeriod] [varchar](30) NULL,
	[AttendanceUpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeAchievements]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeAchievements](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[EmployeeRegisterId] [int] NULL,
	[DateOfAchievement] [datetime] NULL,
	[Achievement] [varchar](250) NULL,
	[Descriptions] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Circular]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Circular](
	[CricularId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[IssuedOn] [datetime] NULL,
	[Reason] [varchar](300) NULL,
	[IssuedBy] [int] NULL,
	[Heading] [varchar](200) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [Pk_Circular_CricularId] PRIMARY KEY CLUSTERED 
(
	[CricularId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BookEntry]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BookEntry](
	[BookEntryRegisterId] [int] IDENTITY(1,1) NOT NULL,
	[BookTitle] [varchar](50) NULL,
	[PublicationName] [varchar](50) NULL,
	[BookLanaguage] [varchar](50) NULL,
	[Edition] [varchar](50) NULL,
	[BookType] [varchar](50) NULL,
	[BookPrice] [money] NULL,
	[NoOfCopies] [int] NULL,
	[BookSection] [varchar](50) NULL,
	[RackNo] [varchar](50) NULL,
	[DateOfEntry] [datetime] NULL,
	[EmployeeRegisterId] [int] NULL,
	[NoOfBookReturn] [int] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[BookEntryRegisterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FeesRefund]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FeesRefund](
	[FeeRefundid] [int] IDENTITY(1,1) NOT NULL,
	[FeeId] [int] NULL,
	[LastAmount] [money] NULL,
	[RefundAmount] [money] NULL,
	[NetAmount] [money] NULL,
	[Reason] [varchar](200) NULL,
	[DateOfModified] [datetime] NULL,
	[EmployeeRegisterId] [int] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[FeeRefundid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HomeWork]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HomeWork](
	[HomeWorkId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[SubjectId] [int] NULL,
	[DateOfWorkPosted] [datetime] NULL,
	[HomeWork] [varchar](250) NULL,
	[Descriptions] [varchar](250) NULL,
	[HomeWorkFileName] [varchar](250) NULL,
	[DateToCompleteWork] [datetime] NULL,
	[HomeWorkStatus] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[HomeWorkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FacultyLeaveRequest]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FacultyLeaveRequest](
	[RequestId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[NumberOfDays] [int] NULL,
	[LeaveReason] [varchar](200) NULL,
	[Status] [varchar](100) NULL,
	[ApproverId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RequestId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FacultyDailyAttendance]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FacultyDailyAttendance](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[CurrentDate] [datetime] NULL,
	[AttendanceStatus] [varchar](20) NULL,
	[LeaveReason] [varchar](250) NULL,
	[SessionPeriod] [varchar](30) NULL,
	[AttendanceMarkedDate] [datetime] NULL,
	[AttendanceUpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ClassCommunication]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClassCommunication](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[DateOfAnnouncement] [datetime] NULL,
	[Title] [varchar](100) NULL,
	[Description] [varchar](800) NULL,
	[FileName] [varchar](50) NULL,
	[MarkedBy] [int] NULL,
	[MarkedDate] [datetime] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExamClassRoom]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExamClassRoom](
	[ExamClassRoomId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[ExamDate] [datetime] NULL,
	[ExamId] [int] NULL,
	[SubjectId] [int] NULL,
	[StartRollNumber] [varchar](100) NULL,
	[EndRollNumber] [varchar](100) NULL,
	[ClassRoomNumber] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[ExamClassRoomId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExamTimetable]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamTimetable](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[ExamId] [int] NULL,
	[SubjectId] [int] NULL,
	[ExamDate] [datetime] NULL,
	[StartTime] [time](7) NULL,
	[EndTime] [time](7) NULL,
	[TimeScheduleId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HostelWarden]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HostelWarden](
	[WardenId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[EmployeeRegisterId] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [Pk_HostelWarden_WardenId] PRIMARY KEY CLUSTERED 
(
	[WardenId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mark]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mark](
	[MarkId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[SubjectId] [int] NULL,
	[MaxMark] [int] NULL,
	[MinMark] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
 CONSTRAINT [PkMark_MarkId] PRIMARY KEY CLUSTERED 
(
	[MarkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MapRoleUser]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MapRoleUser](
	[MapId] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NULL,
	[EmployeeRegisterId] [int] NULL,
	[status] [bit] NULL,
 CONSTRAINT [Pk_MapRoleUser_MapId] PRIMARY KEY CLUSTERED 
(
	[MapId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ParentRegister]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ParentRegister](
	[ParentAdmissionId] [int] IDENTITY(1,1) NOT NULL,
	[StudentAdmissionId] [int] NULL,
	[OnlineRegisterId] [int] NULL,
	[FatherFirstName] [varchar](50) NULL,
	[FatherLastName] [varchar](50) NULL,
	[FatherDOB] [datetime] NULL,
	[FatherQualification] [varchar](50) NULL,
	[FatherOccupation] [varchar](100) NULL,
	[FatherMobileNo] [bigint] NULL,
	[FatherEmail] [varchar](150) NULL,
	[MotherFirstname] [varchar](50) NULL,
	[MotherLastName] [varchar](50) NULL,
	[MotherDOB] [datetime] NULL,
	[MotherQualification] [varchar](50) NULL,
	[MotherOccupation] [varchar](100) NULL,
	[MotherMobileNo] [bigint] NULL,
	[MotherEmail] [varchar](150) NULL,
	[TotalIncome] [bigint] NULL,
	[CompanyAddress1] [varchar](250) NULL,
	[CompanyAddress2] [varchar](250) NULL,
	[CompanyCity] [varchar](50) NULL,
	[CompanyState] [varchar](50) NULL,
	[CompanyCountry] [int] NULL,
	[CompanyPostelcode] [bigint] NULL,
	[CompanyContact] [bigint] NULL,
	[GuardianFirstname] [varchar](50) NULL,
	[GuardianLastName] [varchar](50) NULL,
	[GuardianDOB] [datetime] NULL,
	[GuardianQualification] [varchar](50) NULL,
	[GuardianOccupation] [varchar](100) NULL,
	[GuardianMobileNo] [bigint] NULL,
	[GuardianEmail] [varchar](150) NULL,
	[GuardianIncome] [bigint] NULL,
	[GuardianCompanyName] [varchar](200) NULL,
	[EmergencyContactPersonName] [varchar](200) NULL,
	[EmergencyContactNumber] [bigint] NULL,
	[ContactPersonRelationship] [varchar](100) NULL,
	[GuardianGender] [varchar](10) NULL,
	[GuRelationshipToChild] [varchar](50) NULL,
	[UserCompanyname] [varchar](150) NULL,
	[UserName] [varchar](30) NULL,
	[UserPassword] [varchar](50) NULL,
	[PrimaryUser] [varchar](50) NULL,
	[GuardianRequried] [varchar](10) NULL,
	[PrimaryUserEmail] [varchar](50) NULL,
	[PrimaryUserContact] [bigint] NULL,
	[WorkSameSchool] [varchar](10) NULL,
	[PrimaryUserName] [varchar](50) NULL,
	[SendEmailStatus] [varchar](50) NULL,
	[FatherMiddleName] [varchar](50) NULL,
	[MotherMiddleName] [varchar](50) NULL,
	[GuardianMiddleName] [varchar](50) NULL,
	[OfflineApplicationID] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ParentAdmissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ParentLeaveRequest]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ParentLeaveRequest](
	[RequestId] [int] IDENTITY(1,1) NOT NULL,
	[ParentRegisterId] [int] NULL,
	[ParentUserId] [varchar](100) NULL,
	[ParentName] [varchar](100) NULL,
	[StudentRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[NumberOfDays] [int] NULL,
	[LeaveReason] [varchar](200) NULL,
	[Status] [varchar](100) NULL,
	[EmployeeRegisterId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RequestId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ParentFacultyFeedback]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ParentFacultyFeedback](
	[FeedbackId] [int] IDENTITY(1,1) NOT NULL,
	[ParentRegisterId] [int] NULL,
	[EmployeeRegisterId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[SubjectId] [int] NULL,
	[DateOfFeedback] [datetime] NULL,
	[Feedback] [varchar](250) NULL,
	[ParentUserName] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[FeedbackId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MedicalInfo]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalInfo](
	[MedicalInfoId] [int] IDENTITY(1,1) NOT NULL,
	[StudentRegisterId] [int] NULL,
	[Date] [datetime] NULL,
	[BloodGrp] [nvarchar](20) NULL,
	[Weight] [int] NULL,
	[Height] [int] NULL,
	[Concerns_Status] [nvarchar](20) NULL,
	[Concerns_Desc] [nvarchar](250) NULL,
	[Allergies_Status] [nvarchar](20) NULL,
	[Allegries_Desc] [nvarchar](250) NULL,
	[Vision_Status] [nvarchar](20) NULL,
	[Vision_Desc] [nvarchar](250) NULL,
	[Glasses_Status] [nvarchar](20) NULL,
	[Glasses_Desc] [nvarchar](250) NULL,
	[Hearing_Status] [nvarchar](20) NULL,
	[Hearing_Desc] [nvarchar](250) NULL,
	[Speech_Status] [nvarchar](20) NULL,
	[Speech_Desc] [nvarchar](250) NULL,
	[Emergency_Status] [nvarchar](20) NULL,
	[Emergency_Desc] [nvarchar](250) NULL,
	[Broken_Status] [nvarchar](20) NULL,
	[Broken_Desc] [nvarchar](250) NULL,
	[Injuries_Status] [nvarchar](20) NULL,
	[Injuries_Desc] [nvarchar](250) NULL,
	[Problems_Status] [nvarchar](20) NULL,
	[Problems_Desc] [nvarchar](250) NULL,
	[Dental_Status] [nvarchar](20) NULL,
	[Dental_Desc] [nvarchar](250) NULL,
	[Chest_Status] [nvarchar](20) NULL,
	[Chest_Desc] [nvarchar](250) NULL,
	[Heart_Status] [nvarchar](20) NULL,
	[Heart_Desc] [nvarchar](250) NULL,
	[Highbp_Status] [nvarchar](20) NULL,
	[Highbp_Desc] [nvarchar](250) NULL,
	[Lowbp_Status] [nvarchar](20) NULL,
	[Lowbp_Desc] [nvarchar](250) NULL,
	[Bleeding_Status] [nvarchar](20) NULL,
	[Bleeding_Desc] [nvarchar](250) NULL,
	[Breathing_Status] [nvarchar](20) NULL,
	[Breathing_Desc] [nvarchar](250) NULL,
	[Asthma_Status] [nvarchar](20) NULL,
	[Asthma_Desc] [nvarchar](250) NULL,
	[Seizure_Status] [nvarchar](20) NULL,
	[Seizure_Desc] [nvarchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[MedicalInfoId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MarkTotal]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MarkTotal](
	[MarkTotalId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[ExamId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[TotalMark] [int] NULL,
	[StudentRank] [int] NULL,
	[Result] [varchar](100) NULL,
	[Comment] [varchar](100) NULL,
	[EmployeeRegisterId] [int] NULL,
	[MarkedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MarkTotalId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LibraryFeeCollection]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LibraryFeeCollection](
	[CollectionId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[FineAmount] [money] NULL,
	[TotalAmount] [money] NULL,
	[EmployeeRegisterId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CollectionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HostelStudent]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HostelStudent](
	[HostelStudentId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[RoomNumber] [varchar](50) NULL,
	[status] [bit] NULL,
 CONSTRAINT [Pk_HostelStudent_HostelStudentId] PRIMARY KEY CLUSTERED 
(
	[HostelStudentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HostelFeesCollection]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HostelFeesCollection](
	[FeeCollectionId] [int] IDENTITY(1,1) NOT NULL,
	[CollectionDate] [datetime] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[FeeAmount] [money] NULL,
	[AmountCollected] [money] NULL,
	[PaymentMode] [varchar](50) NULL,
	[BankName] [varchar](50) NULL,
	[ChequeNumber] [varchar](100) NULL,
	[DDNumber] [varchar](100) NULL,
	[Remark] [varchar](500) NULL,
	[Discount] [money] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[Status] [varchar](15) NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[FeeCollectionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeSalaryIncrement]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeSalaryIncrement](
	[EmployeeSalaryIncrementId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeSalaryConfigurationId] [int] NULL,
	[EmployeeRegisterid] [int] NULL,
	[OtherEmployeeRegisterid] [int] NULL,
	[LastbasicSalary] [money] NULL,
	[LastGrossEarning] [money] NULL,
	[LastGrossDeduction] [money] NULL,
	[LastNetSalary] [money] NULL,
	[IncrementAmount] [money] NULL,
	[Entryby] [int] NULL,
	[Entrydate] [datetime] NULL,
 CONSTRAINT [PK__Employee__4CD9EB1C25077354] PRIMARY KEY CLUSTERED 
(
	[EmployeeSalaryIncrementId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExamRemarks]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExamRemarks](
	[ExamRemarkId] [int] IDENTITY(1,1) NOT NULL,
	[StudentRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[ExamDate] [datetime] NULL,
	[ExamId] [int] NULL,
	[SubjectId] [int] NULL,
	[ExamRemark] [varchar](250) NULL,
	[EmployeeRegisterId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ExamRemarkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EntryBookId]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EntryBookId](
	[BookRegisterId] [int] IDENTITY(1,1) NOT NULL,
	[BookEntryRegisterId] [int] NULL,
	[BookId] [varchar](50) NULL,
	[StatusFlag] [varchar](50) NULL,
	[status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[BookRegisterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FatherRegister]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FatherRegister](
	[FatherRegisterId] [int] IDENTITY(1,1) NOT NULL,
	[FatherFirstName] [varchar](50) NULL,
	[FatherMiddleName] [varchar](50) NULL,
	[FatherLastName] [varchar](50) NULL,
	[FatherDOB] [datetime] NULL,
	[FatherQualification] [varchar](50) NULL,
	[FatherOccupation] [varchar](100) NULL,
	[FatherMobileNo] [bigint] NULL,
	[FatherEmail] [varchar](150) NULL,
	[FatherStatusFlag] [varchar](50) NULL,
	[StudentRegisterId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[FatherRegisterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HomeworkReplyFromStudent]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HomeworkReplyFromStudent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HomeWorkId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[Description] [varchar](800) NULL,
	[AnswerFileName] [varchar](50) NULL,
	[AnswerDate] [datetime] NULL,
	[Remark] [varchar](200) NULL,
	[RemarkGivenBy] [int] NULL,
	[RemarkDate] [datetime] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GuardianRegister]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GuardianRegister](
	[GuardianRegisterId] [int] IDENTITY(1,1) NOT NULL,
	[GuardianFirstname] [varchar](50) NULL,
	[GuardianMiddleName] [varchar](50) NULL,
	[GuardianLastName] [varchar](50) NULL,
	[GuardianDOB] [datetime] NULL,
	[GuardianQualification] [varchar](50) NULL,
	[GuardianOccupation] [varchar](100) NULL,
	[GuardianMobileNo] [bigint] NULL,
	[GuardianEmail] [varchar](150) NULL,
	[GuardianCompanyName] [varchar](200) NULL,
	[GuardianIncome] [bigint] NULL,
	[GuRelationshipToChild] [varchar](50) NULL,
	[GuardianGender] [varchar](10) NULL,
	[GuardianRequried] [varchar](50) NULL,
	[GuardianStatusFlag] [varchar](50) NULL,
	[StudentRegisterId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[GuardianRegisterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GradeComment]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GradeComment](
	[GradeCommentId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[ExamId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[Comment] [varchar](100) NULL,
	[EmployeeRegisterId] [int] NULL,
	[CommentMarkedDate] [datetime] NULL,
	[CommentUpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[GradeCommentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FeeCollection]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FeeCollection](
	[FeeCollectionId] [int] IDENTITY(1,1) NOT NULL,
	[CollectionDate] [datetime] NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[FeeAmount] [money] NULL,
	[AmountCollected] [money] NULL,
	[Fine] [money] NULL,
	[PaymentMode] [varchar](20) NULL,
	[BankName] [varchar](100) NULL,
	[ChequeNumber] [varchar](20) NULL,
	[DDNumber] [varchar](20) NULL,
	[Remark] [varchar](200) NULL,
	[VAT] [money] NULL,
	[Discount] [money] NULL,
	[ApplicationId] [int] NULL,
	[EmployeeRegisterId] [int] NULL,
 CONSTRAINT [Pk_FeeCollection_FeeCollectionId] PRIMARY KEY CLUSTERED 
(
	[FeeCollectionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BookAuthors]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BookAuthors](
	[BookAuthorId] [int] IDENTITY(1,1) NOT NULL,
	[BookEntryRegisterId] [int] NULL,
	[AuthorName] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[BookAuthorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ELearningReplyFromStudent]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ELearningReplyFromStudent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ELearningId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[Description] [varchar](800) NULL,
	[AnswerFileName] [varchar](50) NULL,
	[AnswerDate] [datetime] NULL,
	[Remark] [varchar](500) NULL,
	[RemarkGivenBy] [int] NULL,
	[RemarkDate] [datetime] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ClassInchargeStudentRemarks]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClassInchargeStudentRemarks](
	[RemarkId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[DateOfRemarks] [datetime] NULL,
	[Remark] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[RemarkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AssignClassToStudent]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssignClassToStudent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[StudentRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[TransportPreference] [varchar](20) NULL,
	[HostelPreference] [varchar](20) NULL,
	[HaveRollNumber] [bit] NULL,
	[Status] [bit] NULL,
	[Result] [varchar](20) NULL,
	[ConductStatus] [varchar](30) NULL,
	[TransferStatus] [varchar](30) NULL,
	[SeparationStatus] [varchar](30) NULL,
	[ConductCertificate] [varchar](30) NULL,
	[TransferCertificate] [varchar](30) NULL,
	[DateOfClassAssigning] [datetime] NULL,
	[SeparatedBy] [int] NULL,
	[ClassAssignedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AssignActivityToStudent]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssignActivityToStudent](
	[StudentActivityId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[ActivityId] [int] NULL,
	[Note] [varchar](200) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [Pk_AssignActivityToStudent_StudentActivityId] PRIMARY KEY CLUSTERED 
(
	[StudentActivityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ApplySeparation]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ApplySeparation](
	[SeparationApplyId] [int] IDENTITY(1,1) NOT NULL,
	[ParentRegisterId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[StudentName] [varchar](100) NULL,
	[AcademicYearId] [int] NULL,
	[Class] [varchar](50) NULL,
	[Section] [varchar](50) NULL,
	[DateOfApply] [datetime] NULL,
	[FeesDue] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[SeparationApplyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AssignmentReplyFromStudent]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssignmentReplyFromStudent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AssignmentId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[Description] [varchar](800) NULL,
	[AnswerFileName] [varchar](50) NULL,
	[AnswerDate] [datetime] NULL,
	[Remark] [varchar](200) NULL,
	[RemarkGivenBy] [int] NULL,
	[RemarkDate] [datetime] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StudentRollNumber]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentRollNumber](
	[RollNumberId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[RollNumber] [varchar](20) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_StudentRollNumber_RollNumberId] PRIMARY KEY CLUSTERED 
(
	[RollNumberId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [Fk_StudentRollNumber_RollNumber] UNIQUE NONCLUSTERED 
(
	[RollNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StudentMarks]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentMarks](
	[StudentMarkId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[SubjectId] [int] NULL,
	[ExamId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[Mark] [int] NULL,
	[EmployeeRegisterId] [int] NULL,
	[Remark] [varchar](250) NULL,
	[RemarkUpdatedBy] [int] NULL,
	[CommentUpdatedDate] [datetime] NULL,
	[RemarkDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[StudentMarkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StudentGrade]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentGrade](
	[StudentGradeId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[SubjectId] [int] NULL,
	[ExamId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[Mark] [int] NULL,
	[ConvertedMark] [int] NULL,
	[Grade] [varchar](50) NULL,
	[EmployeeRegisterId] [int] NULL,
	[Comment] [varchar](500) NULL,
	[CommentDate] [datetime] NULL,
	[CommentUpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[StudentGradeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StudentFacultyQueries]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentFacultyQueries](
	[QueryId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[SubjectId] [int] NULL,
	[DateOfQuery] [datetime] NULL,
	[Query] [varchar](250) NULL,
	[QueryFile] [varchar](50) NULL,
	[ReplyEmployeeRegId] [int] NULL,
	[Reply] [varchar](250) NULL,
	[ReplyFile] [varchar](50) NULL,
	[QueryFrom] [varchar](30) NULL,
	[PrimaryUserRegId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[QueryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StudentFacultyFeedback]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentFacultyFeedback](
	[FeedbackId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[SubjectId] [int] NULL,
	[DateOfFeedback] [datetime] NULL,
	[Feedback] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[FeedbackId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StudentDailyAttendance]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentDailyAttendance](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[CurrentDate] [datetime] NULL,
	[AttendanceStatus] [varchar](20) NULL,
	[LeaveReason] [varchar](250) NULL,
	[SessionPeriod] [varchar](30) NULL,
	[AttendanceUpdatedDate] [datetime] NULL,
	[AttendanceMarkedDate] [datetime] NULL,
	[EmployeeRegisterId] [int] NULL,
	[AttendanceUpdatedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StudentCommunication]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentCommunication](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[DateOfAnnouncement] [datetime] NULL,
	[Title] [varchar](100) NULL,
	[Description] [varchar](800) NULL,
	[FileName] [varchar](50) NULL,
	[MarkedBy] [int] NULL,
	[MarkedDate] [datetime] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransportDetails]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransportDetails](
	[TransportId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[BusNO] [varchar](50) NULL,
	[Location] [varchar](300) NULL,
	[BusDriver] [varchar](200) NULL,
	[driverContact] [bigint] NULL,
	[routeNo] [varchar](50) NULL,
	[StudentRegisterId] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [Pk_TransportDetails_TransportId] PRIMARY KEY CLUSTERED 
(
	[TransportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RoomAllotment]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoomAllotment](
	[RoomAllotmentId] [int] IDENTITY(1,1) NOT NULL,
	[StudentRegisterId] [int] NULL,
	[EmployeeregisterId] [int] NULL,
	[OtherEmpoyeeRegisterId] [int] NULL,
	[BlockId] [int] NULL,
	[RoomId] [int] NULL,
	[status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[RoomAllotmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PreAdmissionRegisterStatusFlag]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PreAdmissionRegisterStatusFlag](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusFlag] [varchar](30) NULL,
	[Descriptions] [varchar](500) NULL,
	[OnlineRegisterId] [int] NULL,
	[StudentAdmissionId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PreAdmissionTransaction]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PreAdmissionTransaction](
	[PreAdmissionTransactionId] [int] IDENTITY(1,1) NOT NULL,
	[PrimaryUserAdmissionId] [int] NULL,
	[StudentAdmissionId] [int] NULL,
	[FatherAdmissionId] [int] NULL,
	[MotherAdmissionId] [int] NULL,
	[GuardianAdmissionId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PreAdmissionTransactionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PreAdmissionParentSibling]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PreAdmissionParentSibling](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentAdmissionId] [int] NULL,
	[OnlineRegisterId] [int] NULL,
	[SiblingAcademicYear] [varchar](20) NULL,
	[SiblingRollNumbers] [varchar](20) NULL,
	[SiblingClass] [varchar](10) NULL,
	[SiblingSection] [varchar](10) NULL,
	[PersonName] [varchar](50) NULL,
	[PersonDepartment] [varchar](50) NULL,
	[OfflineApplicationID] [varchar](50) NULL,
	[SblingStudentRegId] [int] NULL,
	[SiblingStudentId] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MotherRegister]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MotherRegister](
	[MotherRegisterId] [int] IDENTITY(1,1) NOT NULL,
	[MotherFirstname] [varchar](50) NULL,
	[MotherMiddleName] [varchar](50) NULL,
	[MotherLastName] [varchar](50) NULL,
	[MotherDOB] [datetime] NULL,
	[MotherQualification] [varchar](50) NULL,
	[MotherOccupation] [varchar](100) NULL,
	[MotherMobileNo] [bigint] NULL,
	[MotherEmail] [varchar](150) NULL,
	[MotherStatusFlag] [varchar](50) NULL,
	[StudentRegisterId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MotherRegisterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ModuleInfoStudentRemarks]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ModuleInfoStudentRemarks](
	[RemarkId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[SubjectId] [int] NULL,
	[DateOfRemarks] [datetime] NULL,
	[Remark] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[RemarkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblStudentResetPassword]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblStudentResetPassword](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [int] NULL,
	[ResetRequestDateTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSibling]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSibling](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentRegisterId] [int] NULL,
	[SiblingAcademicYear] [varchar](30) NULL,
	[SiblingRollNumbers] [varchar](30) NULL,
	[SiblingClass] [varchar](20) NULL,
	[SiblingSection] [varchar](20) NULL,
	[PersonName] [varchar](50) NULL,
	[PersonDepartment] [varchar](50) NULL,
	[SiblingStudentRegId] [int] NULL,
	[StatusFlag] [varchar](50) NULL,
	[PrimaryUserRegId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StudentAchievements]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentAchievements](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[DateOfAchievement] [datetime] NULL,
	[Achievement] [varchar](250) NULL,
	[Descriptions] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SeparateStudent]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SeparateStudent](
	[SeparationId] [int] IDENTITY(1,1) NOT NULL,
	[StudentRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[DateOfIssue] [datetime] NULL,
	[Reason] [varchar](100) NULL,
	[Description] [varchar](250) NULL,
	[TC] [varchar](50) NULL,
	[CC] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[SeparationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransportPickPoint]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransportPickPoint](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DestinationId] [int] NULL,
	[PickPointName] [varchar](100) NULL,
	[Distance] [varchar](10) NULL,
	[Amount] [money] NULL,
	[status] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransportMapping]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransportMapping](
	[RouteId] [int] IDENTITY(1,1) NOT NULL,
	[RouteNumber] [varchar](50) NULL,
	[Destination] [int] NULL,
	[RegistrationNumberId] [int] NULL,
	[StartTimeFromParking] [time](7) NULL,
	[SchoolArrivalTime] [time](7) NULL,
	[SchoolOutTime] [time](7) NULL,
	[ParkingReachTime] [time](7) NULL,
	[DriverId] [int] NULL,
	[HelperId] [int] NULL,
	[ParkingArea] [varchar](200) NULL,
	[status] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[RouteId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransportTiming]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransportTiming](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RouteId] [int] NULL,
	[PickPointId] [int] NULL,
	[DepartureTime] [time](7) NULL,
	[ArrivalTime] [time](7) NULL,
	[status] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TransportAllotment]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransportAllotment](
	[TransportAllotmentId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[DestinationId] [int] NULL,
	[PickPointId] [int] NULL,
	[RouteMapId] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[TransportAllotmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TransportFeesCollection]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransportFeesCollection](
	[FeeCollectionId] [int] IDENTITY(1,1) NOT NULL,
	[CollectionDate] [datetime] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[DestinationId] [int] NULL,
	[PickPointId] [int] NULL,
	[FeeAmount] [money] NULL,
	[AmountCollected] [money] NULL,
	[Fine] [money] NULL,
	[PaymentMode] [varchar](50) NULL,
	[BankName] [varchar](50) NULL,
	[ChequeNumber] [varchar](100) NULL,
	[DDNumber] [varchar](100) NULL,
	[Remark] [varchar](500) NULL,
	[VAT] [money] NULL,
	[Discount] [money] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[FeeCollectionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StudentBookTakenReturn]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentBookTakenReturn](
	[StudentTakenId] [int] IDENTITY(1,1) NOT NULL,
	[StudentRegisterId] [int] NULL,
	[StudentRollNumber] [varchar](50) NULL,
	[BookRegisterId] [int] NULL,
	[DateOfTaken] [datetime] NULL,
	[DateOfReturn] [datetime] NULL,
	[StatusFlag] [varchar](50) NULL,
	[status] [bit] NULL,
	[GivenBy] [int] NULL,
	[FineAmount] [money] NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[StudentBookTakenReturn] ADD [FineStatusFlag] [varchar](10) NULL
ALTER TABLE [dbo].[StudentBookTakenReturn] ADD [FinePaid] [varchar](10) NULL
ALTER TABLE [dbo].[StudentBookTakenReturn] ADD PRIMARY KEY CLUSTERED 
(
	[StudentTakenId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StaffBookTakenReturn]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StaffBookTakenReturn](
	[StaffTakenId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeRegisterId] [int] NULL,
	[EmployeeId] [varchar](50) NULL,
	[BookRegisterId] [int] NULL,
	[DateOfTaken] [datetime] NULL,
	[DateOfReturn] [datetime] NULL,
	[StatusFlag] [varchar](50) NULL,
	[status] [bit] NULL,
	[GivenBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[StaffTakenId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AssignExamClassRoom]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssignExamClassRoom](
	[AssignExamClassRoomId] [int] IDENTITY(1,1) NOT NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[SubjectId] [int] NULL,
	[ExamId] [int] NULL,
	[ExamDate] [datetime] NULL,
	[StartRollNumber] [varchar](50) NULL,
	[EndRollNumber] [varchar](50) NULL,
	[StartRegisterId] [int] NULL,
	[EndRegisterId] [int] NULL,
	[RoomNumber] [varchar](50) NULL,
	[status] [bit] NULL,
	[StartRollNumberId] [int] NULL,
	[EndRollNumberId] [int] NULL,
	[NumberOfStudents] [int] NULL,
	[ClassRoomId] [int] NULL,
	[TimeScheduleId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AssignExamClassRoomId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdmissionTransaction]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdmissionTransaction](
	[AdmissionTransactionId] [int] IDENTITY(1,1) NOT NULL,
	[PrimaryUserRegisterId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[FatherRegisterId] [int] NULL,
	[MotherRegisterId] [int] NULL,
	[GuardianRegisterId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AdmissionTransactionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FeeCollectionCategory]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FeeCollectionCategory](
	[CollectionId] [int] IDENTITY(1,1) NOT NULL,
	[FeeCollectionId] [int] NULL,
	[FeeCategoryId] [int] NULL,
	[PaymentTypeId] [int] NULL,
	[Amount] [money] NULL,
	[ServicesTax] [money] NULL,
	[TotalAmount] [money] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[StudentRegId] [int] NULL,
	[ApplicationId] [int] NULL,
	[ReFundAmount] [money] NULL,
	[NetAmount] [money] NULL,
	[EmployeeRegisterId] [int] NULL,
	[ReceivedBy] [varchar](50) NULL,
	[Reason] [varchar](250) NULL,
	[RefundDate] [datetime] NULL,
	[FeeCollection] [datetime] NULL,
 CONSTRAINT [Pk_FeeCollectionCategory_CollectionId] PRIMARY KEY CLUSTERED 
(
	[CollectionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExamAttendance]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExamAttendance](
	[AttendanceId] [int] IDENTITY(1,1) NOT NULL,
	[StudentRegisterId] [int] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[ExamDate] [datetime] NULL,
	[ExamId] [int] NULL,
	[SubjectId] [int] NULL,
	[AttendanceStatus] [varchar](20) NULL,
	[LeaveReason] [varchar](250) NULL,
	[TimeScheduleId] [int] NULL,
	[RollNumberId] [int] NULL,
	[MarkedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AttendanceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EntryBookReturn]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EntryBookReturn](
	[EnterBookReturnId] [int] IDENTITY(1,1) NOT NULL,
	[BookEntryRegisterId] [int] NULL,
	[BookRegisterId] [int] NULL,
	[status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[EnterBookReturnId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HostelFeesCollectioncategory]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HostelFeesCollectioncategory](
	[CollectionCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[FeeCollectionId] [int] NULL,
	[FeeCategoryId] [int] NULL,
	[Amount] [money] NULL,
	[ServiceTax] [money] NULL,
	[TotalAmount] [money] NULL,
	[AcademicYearId] [int] NULL,
	[ClassId] [int] NULL,
	[SectionId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[Status] [varchar](15) NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CollectionCategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FeesRefundToStudent]    Script Date: 01/05/2016 10:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FeesRefundToStudent](
	[StudentFeeRefundId] [int] IDENTITY(1,1) NOT NULL,
	[CollectionId] [int] NULL,
	[StudentRegisterId] [int] NULL,
	[LastAmount] [money] NULL,
	[RefundAmount] [money] NULL,
	[NetAmount] [money] NULL,
	[Reason] [varchar](200) NULL,
	[IssuedBy] [varchar](50) NULL,
	[DateOfRefund] [datetime] NULL,
	[EmployeeRegisterId] [int] NULL,
	[Status] [bit] NULL,
	[FeeId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[StudentFeeRefundId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [Fk_NewsTrends_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[NewsTrends]  WITH CHECK ADD  CONSTRAINT [Fk_NewsTrends_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[NewsTrends] CHECK CONSTRAINT [Fk_NewsTrends_AcademicYearId]
GO
/****** Object:  ForeignKey [FK__PreAdmiss__Onlin__0B27A5C0]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[PreAdmissionPrimaryUserRegister]  WITH CHECK ADD  CONSTRAINT [FK__PreAdmiss__Onlin__0B27A5C0] FOREIGN KEY([OnlineRegisterId])
REFERENCES [dbo].[PreAdmissionOnLineRegister] ([OnlineRegisterId])
GO
ALTER TABLE [dbo].[PreAdmissionPrimaryUserRegister] CHECK CONSTRAINT [FK__PreAdmiss__Onlin__0B27A5C0]
GO
/****** Object:  ForeignKey [Fk_SchoolSettings_CurrencyType]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SchoolSettings]  WITH CHECK ADD  CONSTRAINT [Fk_SchoolSettings_CurrencyType] FOREIGN KEY([CurrencyType])
REFERENCES [dbo].[currency] ([id])
GO
ALTER TABLE [dbo].[SchoolSettings] CHECK CONSTRAINT [Fk_SchoolSettings_CurrencyType]
GO
/****** Object:  ForeignKey [FK__SchoolAch__Acade__02925FBF]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SchoolAchievements]  WITH CHECK ADD  CONSTRAINT [FK__SchoolAch__Acade__02925FBF] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[SchoolAchievements] CHECK CONSTRAINT [FK__SchoolAch__Acade__02925FBF]
GO
/****** Object:  ForeignKey [FkGrade_GradeTypeId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Grade]  WITH CHECK ADD  CONSTRAINT [FkGrade_GradeTypeId] FOREIGN KEY([GradeTypeId])
REFERENCES [dbo].[GradeType] ([GradeTypeId])
GO
ALTER TABLE [dbo].[Grade] CHECK CONSTRAINT [FkGrade_GradeTypeId]
GO
/****** Object:  ForeignKey [FkTerm_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Term]  WITH CHECK ADD  CONSTRAINT [FkTerm_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[Term] CHECK CONSTRAINT [FkTerm_AcademicYearId]
GO
/****** Object:  ForeignKey [FK__StudentUs__UserL__4AF81212]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentUserActivity]  WITH CHECK ADD  CONSTRAINT [FK__StudentUs__UserL__4AF81212] FOREIGN KEY([UserLogId])
REFERENCES [dbo].[UserLog] ([UserLogId])
GO
ALTER TABLE [dbo].[StudentUserActivity] CHECK CONSTRAINT [FK__StudentUs__UserL__4AF81212]
GO
/****** Object:  ForeignKey [Fk_Class_TimeScheduleId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Class]  WITH CHECK ADD  CONSTRAINT [Fk_Class_TimeScheduleId] FOREIGN KEY([TimeScheduleId])
REFERENCES [dbo].[TimeSchedule] ([TimeScheduleId])
GO
ALTER TABLE [dbo].[Class] CHECK CONSTRAINT [Fk_Class_TimeScheduleId]
GO
/****** Object:  ForeignKey [FK_fkClassTimeSchedule_TimeScheduleId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ClassTimeSchedule]  WITH CHECK ADD  CONSTRAINT [FK_fkClassTimeSchedule_TimeScheduleId] FOREIGN KEY([TimeScheduleId])
REFERENCES [dbo].[TimeSchedule] ([TimeScheduleId])
GO
ALTER TABLE [dbo].[ClassTimeSchedule] CHECK CONSTRAINT [FK_fkClassTimeSchedule_TimeScheduleId]
GO
/****** Object:  ForeignKey [FK__EmployeeD__Emplo__45DE573A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeDesignation]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeD__Emplo__45DE573A] FOREIGN KEY([EmployeeTypeId])
REFERENCES [dbo].[EmployeeType] ([EmployeeTypeId])
GO
ALTER TABLE [dbo].[EmployeeDesignation] CHECK CONSTRAINT [FK__EmployeeD__Emplo__45DE573A]
GO
/****** Object:  ForeignKey [FkAnnouncement_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Announcement]  WITH CHECK ADD  CONSTRAINT [FkAnnouncement_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[Announcement] CHECK CONSTRAINT [FkAnnouncement_AcademicYearId]
GO
/****** Object:  ForeignKey [FkEvent_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Event]  WITH CHECK ADD  CONSTRAINT [FkEvent_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[Event] CHECK CONSTRAINT [FkEvent_AcademicYearId]
GO
/****** Object:  ForeignKey [FK__EmployeeU__UserL__46335CF5]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeUserActivity]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeU__UserL__46335CF5] FOREIGN KEY([UserLogId])
REFERENCES [dbo].[UserLog] ([UserLogId])
GO
ALTER TABLE [dbo].[EmployeeUserActivity] CHECK CONSTRAINT [FK__EmployeeU__UserL__46335CF5]
GO
/****** Object:  ForeignKey [Fk_FacilityIncharge_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FacilityIncharge]  WITH CHECK ADD  CONSTRAINT [Fk_FacilityIncharge_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[FacilityIncharge] CHECK CONSTRAINT [Fk_FacilityIncharge_AcademicYearId]
GO
/****** Object:  ForeignKey [Fk_FacilityIncharge_FacilityId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FacilityIncharge]  WITH CHECK ADD  CONSTRAINT [Fk_FacilityIncharge_FacilityId] FOREIGN KEY([FacilityId])
REFERENCES [dbo].[Facilities] ([FacilityId])
GO
ALTER TABLE [dbo].[FacilityIncharge] CHECK CONSTRAINT [Fk_FacilityIncharge_FacilityId]
GO
/****** Object:  ForeignKey [FK__HostelFee__Acade__30441BD6]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCategory]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__Acade__30441BD6] FOREIGN KEY([AcademicYear])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[HostelFeesCategory] CHECK CONSTRAINT [FK__HostelFee__Acade__30441BD6]
GO
/****** Object:  ForeignKey [FK__HostelRoo__Block__25C68D63]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelRoom]  WITH CHECK ADD  CONSTRAINT [FK__HostelRoo__Block__25C68D63] FOREIGN KEY([BlockId])
REFERENCES [dbo].[HostelBlock] ([BlockId])
GO
ALTER TABLE [dbo].[HostelRoom] CHECK CONSTRAINT [FK__HostelRoo__Block__25C68D63]
GO
/****** Object:  ForeignKey [Fk_Menu_FeatureId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Menu]  WITH CHECK ADD  CONSTRAINT [Fk_Menu_FeatureId] FOREIGN KEY([FeatureId])
REFERENCES [dbo].[Feature] ([FeatureId])
GO
ALTER TABLE [dbo].[Menu] CHECK CONSTRAINT [Fk_Menu_FeatureId]
GO
/****** Object:  ForeignKey [Fk_MapRoleFeature_FeatureId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[MapRoleFeature]  WITH CHECK ADD  CONSTRAINT [Fk_MapRoleFeature_FeatureId] FOREIGN KEY([FeatureId])
REFERENCES [dbo].[Feature] ([FeatureId])
GO
ALTER TABLE [dbo].[MapRoleFeature] CHECK CONSTRAINT [Fk_MapRoleFeature_FeatureId]
GO
/****** Object:  ForeignKey [Fk_MapRoleFeature_RoleId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[MapRoleFeature]  WITH CHECK ADD  CONSTRAINT [Fk_MapRoleFeature_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[MapRoleFeature] CHECK CONSTRAINT [Fk_MapRoleFeature_RoleId]
GO
/****** Object:  ForeignKey [Pk_PeriodsSchedule_TimeScheduleId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[PeriodsSchedule]  WITH CHECK ADD  CONSTRAINT [Pk_PeriodsSchedule_TimeScheduleId] FOREIGN KEY([TimeScheduleId])
REFERENCES [dbo].[TimeSchedule] ([TimeScheduleId])
GO
ALTER TABLE [dbo].[PeriodsSchedule] CHECK CONSTRAINT [Pk_PeriodsSchedule_TimeScheduleId]
GO
/****** Object:  ForeignKey [FK__ParentUse__UserL__4FBCC72F]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentUserActivity]  WITH CHECK ADD  CONSTRAINT [FK__ParentUse__UserL__4FBCC72F] FOREIGN KEY([UserLogId])
REFERENCES [dbo].[UserLog] ([UserLogId])
GO
ALTER TABLE [dbo].[ParentUserActivity] CHECK CONSTRAINT [FK__ParentUse__UserL__4FBCC72F]
GO
/****** Object:  ForeignKey [Fk_Section_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Section]  WITH CHECK ADD  CONSTRAINT [Fk_Section_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[Section] CHECK CONSTRAINT [Fk_Section_ClassId]
GO
/****** Object:  ForeignKey [FK__OtherEmpl__Emplo__48BAC3E5]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[OtherEmployee]  WITH CHECK ADD  CONSTRAINT [FK__OtherEmpl__Emplo__48BAC3E5] FOREIGN KEY([EmployeeTypeId])
REFERENCES [dbo].[EmployeeType] ([EmployeeTypeId])
GO
ALTER TABLE [dbo].[OtherEmployee] CHECK CONSTRAINT [FK__OtherEmpl__Emplo__48BAC3E5]
GO
/****** Object:  ForeignKey [FK__OtherEmpl__Emplo__49AEE81E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[OtherEmployee]  WITH CHECK ADD  CONSTRAINT [FK__OtherEmpl__Emplo__49AEE81E] FOREIGN KEY([EmployeeDesignationId])
REFERENCES [dbo].[EmployeeDesignation] ([EmployeeDesignationId])
GO
ALTER TABLE [dbo].[OtherEmployee] CHECK CONSTRAINT [FK__OtherEmpl__Emplo__49AEE81E]
GO
/****** Object:  ForeignKey [Fk_Fee__ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Fee]  WITH CHECK ADD  CONSTRAINT [Fk_Fee__ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[Fee] CHECK CONSTRAINT [Fk_Fee__ClassId]
GO
/****** Object:  ForeignKey [Fk_feesAcid]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Fee]  WITH CHECK ADD  CONSTRAINT [Fk_feesAcid] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[Fee] CHECK CONSTRAINT [Fk_feesAcid]
GO
/****** Object:  ForeignKey [FkFee_FeeCategoryId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Fee]  WITH CHECK ADD  CONSTRAINT [FkFee_FeeCategoryId] FOREIGN KEY([FeeCategoryId])
REFERENCES [dbo].[FeeCategory] ([FeeCategoryId])
GO
ALTER TABLE [dbo].[Fee] CHECK CONSTRAINT [FkFee_FeeCategoryId]
GO
/****** Object:  ForeignKey [FkFee_PaymentTypeId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Fee]  WITH CHECK ADD  CONSTRAINT [FkFee_PaymentTypeId] FOREIGN KEY([PaymentTypeId])
REFERENCES [dbo].[Fee] ([FeeId])
GO
ALTER TABLE [dbo].[Fee] CHECK CONSTRAINT [FkFee_PaymentTypeId]
GO
/****** Object:  ForeignKey [FK__AsignClas__Class__39E294A9]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AsignClassVacancy]  WITH CHECK ADD  CONSTRAINT [FK__AsignClas__Class__39E294A9] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[AsignClassVacancy] CHECK CONSTRAINT [FK__AsignClas__Class__39E294A9]
GO
/****** Object:  ForeignKey [Fk_AssignGradeTypeToClass_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignGradeTypeToClass]  WITH CHECK ADD  CONSTRAINT [Fk_AssignGradeTypeToClass_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[AssignGradeTypeToClass] CHECK CONSTRAINT [Fk_AssignGradeTypeToClass_AcademicYearId]
GO
/****** Object:  ForeignKey [Fk_AssignGradeTypeToClass_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignGradeTypeToClass]  WITH CHECK ADD  CONSTRAINT [Fk_AssignGradeTypeToClass_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[AssignGradeTypeToClass] CHECK CONSTRAINT [Fk_AssignGradeTypeToClass_ClassId]
GO
/****** Object:  ForeignKey [Fk_AssignGradeTypeToClass_GradeTypeId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignGradeTypeToClass]  WITH CHECK ADD  CONSTRAINT [Fk_AssignGradeTypeToClass_GradeTypeId] FOREIGN KEY([GradeTypeId])
REFERENCES [dbo].[GradeType] ([GradeTypeId])
GO
ALTER TABLE [dbo].[AssignGradeTypeToClass] CHECK CONSTRAINT [Fk_AssignGradeTypeToClass_GradeTypeId]
GO
/****** Object:  ForeignKey [FK__TechEmplo__Emplo__46D27B73]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TechEmployee]  WITH CHECK ADD  CONSTRAINT [FK__TechEmplo__Emplo__46D27B73] FOREIGN KEY([EmployeeTypeId])
REFERENCES [dbo].[EmployeeType] ([EmployeeTypeId])
GO
ALTER TABLE [dbo].[TechEmployee] CHECK CONSTRAINT [FK__TechEmplo__Emplo__46D27B73]
GO
/****** Object:  ForeignKey [FK__TechEmplo__Emplo__47C69FAC]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TechEmployee]  WITH CHECK ADD  CONSTRAINT [FK__TechEmplo__Emplo__47C69FAC] FOREIGN KEY([EmployeeDesignationId])
REFERENCES [dbo].[EmployeeDesignation] ([EmployeeDesignationId])
GO
ALTER TABLE [dbo].[TechEmployee] CHECK CONSTRAINT [FK__TechEmplo__Emplo__47C69FAC]
GO
/****** Object:  ForeignKey [FK__Transport__Creat__5C8CB268]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportDestination]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Creat__5C8CB268] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TransportDestination] CHECK CONSTRAINT [FK__Transport__Creat__5C8CB268]
GO
/****** Object:  ForeignKey [FK__Transport__Updat__5D80D6A1]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportDestination]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Updat__5D80D6A1] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TransportDestination] CHECK CONSTRAINT [FK__Transport__Updat__5D80D6A1]
GO
/****** Object:  ForeignKey [Fk_TermFee_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TermFee]  WITH CHECK ADD  CONSTRAINT [Fk_TermFee_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[TermFee] CHECK CONSTRAINT [Fk_TermFee_AcademicYearId]
GO
/****** Object:  ForeignKey [Fk_TermFee_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TermFee]  WITH CHECK ADD  CONSTRAINT [Fk_TermFee_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[TermFee] CHECK CONSTRAINT [Fk_TermFee_ClassId]
GO
/****** Object:  ForeignKey [Fk_TermFee_FeeId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TermFee]  WITH CHECK ADD  CONSTRAINT [Fk_TermFee_FeeId] FOREIGN KEY([FeeId])
REFERENCES [dbo].[Fee] ([FeeId])
GO
ALTER TABLE [dbo].[TermFee] CHECK CONSTRAINT [Fk_TermFee_FeeId]
GO
/****** Object:  ForeignKey [FkTermFee_FeeCategoryId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TermFee]  WITH CHECK ADD  CONSTRAINT [FkTermFee_FeeCategoryId] FOREIGN KEY([FeeCategoryId])
REFERENCES [dbo].[FeeCategory] ([FeeCategoryId])
GO
ALTER TABLE [dbo].[TermFee] CHECK CONSTRAINT [FkTermFee_FeeCategoryId]
GO
/****** Object:  ForeignKey [FkTermFee_PaymentTypeId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TermFee]  WITH CHECK ADD  CONSTRAINT [FkTermFee_PaymentTypeId] FOREIGN KEY([PaymentTypeId])
REFERENCES [dbo].[Fee] ([FeeId])
GO
ALTER TABLE [dbo].[TermFee] CHECK CONSTRAINT [FkTermFee_PaymentTypeId]
GO
/****** Object:  ForeignKey [FkTermFee_TermId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TermFee]  WITH CHECK ADD  CONSTRAINT [FkTermFee_TermId] FOREIGN KEY([TermId])
REFERENCES [dbo].[Term] ([TermId])
GO
ALTER TABLE [dbo].[TermFee] CHECK CONSTRAINT [FkTermFee_TermId]
GO
/****** Object:  ForeignKey [Fk_TechEmployeeProfessional_EmployeeRegisterId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TechEmployeeProfessional]  WITH CHECK ADD  CONSTRAINT [Fk_TechEmployeeProfessional_EmployeeRegisterId] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TechEmployeeProfessional] CHECK CONSTRAINT [Fk_TechEmployeeProfessional_EmployeeRegisterId]
GO
/****** Object:  ForeignKey [FK__TechEmplo__Other__4B973090]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TechEmployeeEmployment]  WITH CHECK ADD  CONSTRAINT [FK__TechEmplo__Other__4B973090] FOREIGN KEY([OtherEmployeeRegisterId])
REFERENCES [dbo].[OtherEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TechEmployeeEmployment] CHECK CONSTRAINT [FK__TechEmplo__Other__4B973090]
GO
/****** Object:  ForeignKey [Fk_TechEmployeeEmployment_EmployeeRegisterId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TechEmployeeEmployment]  WITH CHECK ADD  CONSTRAINT [Fk_TechEmployeeEmployment_EmployeeRegisterId] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TechEmployeeEmployment] CHECK CONSTRAINT [Fk_TechEmployeeEmployment_EmployeeRegisterId]
GO
/****** Object:  ForeignKey [FK__TechEmplo__Other__4AA30C57]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TechEmployeeEducation]  WITH CHECK ADD  CONSTRAINT [FK__TechEmplo__Other__4AA30C57] FOREIGN KEY([OtherEmployeeRegisterId])
REFERENCES [dbo].[OtherEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TechEmployeeEducation] CHECK CONSTRAINT [FK__TechEmplo__Other__4AA30C57]
GO
/****** Object:  ForeignKey [Fk_TechEmployeeEducation_EmployeeRegisterId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TechEmployeeEducation]  WITH CHECK ADD  CONSTRAINT [Fk_TechEmployeeEducation_EmployeeRegisterId] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TechEmployeeEducation] CHECK CONSTRAINT [Fk_TechEmployeeEducation_EmployeeRegisterId]
GO
/****** Object:  ForeignKey [Fk_TechEmployeeClassSubject_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TechEmployeeClassSubject]  WITH CHECK ADD  CONSTRAINT [Fk_TechEmployeeClassSubject_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[TechEmployeeClassSubject] CHECK CONSTRAINT [Fk_TechEmployeeClassSubject_ClassId]
GO
/****** Object:  ForeignKey [Fk_TechEmployeeClassSubject_EmployeeRegisterId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TechEmployeeClassSubject]  WITH CHECK ADD  CONSTRAINT [Fk_TechEmployeeClassSubject_EmployeeRegisterId] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TechEmployeeClassSubject] CHECK CONSTRAINT [Fk_TechEmployeeClassSubject_EmployeeRegisterId]
GO
/****** Object:  ForeignKey [FK__SingleSta__Acade__3296789C]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SingleStaffCommunication]  WITH CHECK ADD  CONSTRAINT [FK__SingleSta__Acade__3296789C] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[SingleStaffCommunication] CHECK CONSTRAINT [FK__SingleSta__Acade__3296789C]
GO
/****** Object:  ForeignKey [FK__SingleSta__Emplo__338A9CD5]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SingleStaffCommunication]  WITH CHECK ADD  CONSTRAINT [FK__SingleSta__Emplo__338A9CD5] FOREIGN KEY([EmployeeTypeId])
REFERENCES [dbo].[EmployeeType] ([EmployeeTypeId])
GO
ALTER TABLE [dbo].[SingleStaffCommunication] CHECK CONSTRAINT [FK__SingleSta__Emplo__338A9CD5]
GO
/****** Object:  ForeignKey [FK__SingleSta__Emplo__347EC10E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SingleStaffCommunication]  WITH CHECK ADD  CONSTRAINT [FK__SingleSta__Emplo__347EC10E] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[SingleStaffCommunication] CHECK CONSTRAINT [FK__SingleSta__Emplo__347EC10E]
GO
/****** Object:  ForeignKey [FK__SingleSta__Marke__3572E547]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SingleStaffCommunication]  WITH CHECK ADD  CONSTRAINT [FK__SingleSta__Marke__3572E547] FOREIGN KEY([MarkedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[SingleStaffCommunication] CHECK CONSTRAINT [FK__SingleSta__Marke__3572E547]
GO
/****** Object:  ForeignKey [Fk_SectionStrength_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SectionStrength]  WITH CHECK ADD  CONSTRAINT [Fk_SectionStrength_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[SectionStrength] CHECK CONSTRAINT [Fk_SectionStrength_AcademicYearId]
GO
/****** Object:  ForeignKey [Fk_SectionStrength_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SectionStrength]  WITH CHECK ADD  CONSTRAINT [Fk_SectionStrength_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[SectionStrength] CHECK CONSTRAINT [Fk_SectionStrength_ClassId]
GO
/****** Object:  ForeignKey [Fk_SectionStrength_ClassRoomId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SectionStrength]  WITH CHECK ADD  CONSTRAINT [Fk_SectionStrength_ClassRoomId] FOREIGN KEY([ClassRoomId])
REFERENCES [dbo].[ClassRooms] ([ClassRoomId])
GO
ALTER TABLE [dbo].[SectionStrength] CHECK CONSTRAINT [Fk_SectionStrength_ClassRoomId]
GO
/****** Object:  ForeignKey [Fk_SectionStrength_SectionId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SectionStrength]  WITH CHECK ADD  CONSTRAINT [Fk_SectionStrength_SectionId] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[SectionStrength] CHECK CONSTRAINT [Fk_SectionStrength_SectionId]
GO
/****** Object:  ForeignKey [FK__tblTechEm__UserI__00AA174D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[tblTechEmployeeResetPassword]  WITH CHECK ADD  CONSTRAINT [FK__tblTechEm__UserI__00AA174D] FOREIGN KEY([UserId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[tblTechEmployeeResetPassword] CHECK CONSTRAINT [FK__tblTechEm__UserI__00AA174D]
GO
/****** Object:  ForeignKey [FK__SubjectNo__Acade__53A266AC]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SubjectNotes]  WITH CHECK ADD  CONSTRAINT [FK__SubjectNo__Acade__53A266AC] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[SubjectNotes] CHECK CONSTRAINT [FK__SubjectNo__Acade__53A266AC]
GO
/****** Object:  ForeignKey [FK__SubjectNo__Acade__54968AE5]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SubjectNotes]  WITH CHECK ADD  CONSTRAINT [FK__SubjectNo__Acade__54968AE5] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[SubjectNotes] CHECK CONSTRAINT [FK__SubjectNo__Acade__54968AE5]
GO
/****** Object:  ForeignKey [FK__SubjectNo__Emplo__558AAF1E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SubjectNotes]  WITH CHECK ADD  CONSTRAINT [FK__SubjectNo__Emplo__558AAF1E] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[SubjectNotes] CHECK CONSTRAINT [FK__SubjectNo__Emplo__558AAF1E]
GO
/****** Object:  ForeignKey [FK__SubjectNo__Emplo__567ED357]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SubjectNotes]  WITH CHECK ADD  CONSTRAINT [FK__SubjectNo__Emplo__567ED357] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[SubjectNotes] CHECK CONSTRAINT [FK__SubjectNo__Emplo__567ED357]
GO
/****** Object:  ForeignKey [FK__SubjectNo__Secti__5772F790]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SubjectNotes]  WITH CHECK ADD  CONSTRAINT [FK__SubjectNo__Secti__5772F790] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[SubjectNotes] CHECK CONSTRAINT [FK__SubjectNo__Secti__5772F790]
GO
/****** Object:  ForeignKey [FK__SubjectNo__Secti__58671BC9]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SubjectNotes]  WITH CHECK ADD  CONSTRAINT [FK__SubjectNo__Secti__58671BC9] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[SubjectNotes] CHECK CONSTRAINT [FK__SubjectNo__Secti__58671BC9]
GO
/****** Object:  ForeignKey [FK__SubjectNo__Subje__595B4002]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SubjectNotes]  WITH CHECK ADD  CONSTRAINT [FK__SubjectNo__Subje__595B4002] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[SubjectNotes] CHECK CONSTRAINT [FK__SubjectNo__Subje__595B4002]
GO
/****** Object:  ForeignKey [FK__SubjectNo__Subje__5A4F643B]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SubjectNotes]  WITH CHECK ADD  CONSTRAINT [FK__SubjectNo__Subje__5A4F643B] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[SubjectNotes] CHECK CONSTRAINT [FK__SubjectNo__Subje__5A4F643B]
GO
/****** Object:  ForeignKey [FK__SubjectNotes_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SubjectNotes]  WITH CHECK ADD  CONSTRAINT [FK__SubjectNotes_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[SubjectNotes] CHECK CONSTRAINT [FK__SubjectNotes_ClassId]
GO
/****** Object:  ForeignKey [FK__StudentRe__Enrol__17236851]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[PreAdmissionStudentRegister]  WITH CHECK ADD  CONSTRAINT [FK__StudentRe__Enrol__17236851] FOREIGN KEY([EnrolledBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[PreAdmissionStudentRegister] CHECK CONSTRAINT [FK__StudentRe__Enrol__17236851]
GO
/****** Object:  ForeignKey [Fk_RollNumberFormat_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[RollNumberFormat]  WITH CHECK ADD  CONSTRAINT [Fk_RollNumberFormat_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[RollNumberFormat] CHECK CONSTRAINT [Fk_RollNumberFormat_AcademicYearId]
GO
/****** Object:  ForeignKey [Fk_RollNumberFormat_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[RollNumberFormat]  WITH CHECK ADD  CONSTRAINT [Fk_RollNumberFormat_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[RollNumberFormat] CHECK CONSTRAINT [Fk_RollNumberFormat_ClassId]
GO
/****** Object:  ForeignKey [Fk_RollNumberFormat_SectionId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[RollNumberFormat]  WITH CHECK ADD  CONSTRAINT [Fk_RollNumberFormat_SectionId] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[RollNumberFormat] CHECK CONSTRAINT [Fk_RollNumberFormat_SectionId]
GO
/****** Object:  ForeignKey [FK__VehicleDe__Count__14D10B8B]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[VehicleDetails]  WITH CHECK ADD  CONSTRAINT [FK__VehicleDe__Count__14D10B8B] FOREIGN KEY([Country])
REFERENCES [dbo].[tblCountry] ([CountryId])
GO
ALTER TABLE [dbo].[VehicleDetails] CHECK CONSTRAINT [FK__VehicleDe__Count__14D10B8B]
GO
/****** Object:  ForeignKey [FK__VehicleDe__Creat__17AD7836]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[VehicleDetails]  WITH CHECK ADD  CONSTRAINT [FK__VehicleDe__Creat__17AD7836] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[VehicleDetails] CHECK CONSTRAINT [FK__VehicleDe__Creat__17AD7836]
GO
/****** Object:  ForeignKey [FK__VehicleDe__FuelT__16B953FD]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[VehicleDetails]  WITH CHECK ADD  CONSTRAINT [FK__VehicleDe__FuelT__16B953FD] FOREIGN KEY([FuelType])
REFERENCES [dbo].[FuelType] ([Id])
GO
ALTER TABLE [dbo].[VehicleDetails] CHECK CONSTRAINT [FK__VehicleDe__FuelT__16B953FD]
GO
/****** Object:  ForeignKey [FK__VehicleDe__Updat__18A19C6F]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[VehicleDetails]  WITH CHECK ADD  CONSTRAINT [FK__VehicleDe__Updat__18A19C6F] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[VehicleDetails] CHECK CONSTRAINT [FK__VehicleDe__Updat__18A19C6F]
GO
/****** Object:  ForeignKey [FK__VehicleDe__Vehic__15C52FC4]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[VehicleDetails]  WITH CHECK ADD  CONSTRAINT [FK__VehicleDe__Vehic__15C52FC4] FOREIGN KEY([VehicleType])
REFERENCES [dbo].[VehicleType] ([Id])
GO
ALTER TABLE [dbo].[VehicleDetails] CHECK CONSTRAINT [FK__VehicleDe__Vehic__15C52FC4]
GO
/****** Object:  ForeignKey [FK__tblOtherE__UserI__038683F8]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[tblOtherEmployeeResetPassword]  WITH CHECK ADD  CONSTRAINT [FK__tblOtherE__UserI__038683F8] FOREIGN KEY([UserId])
REFERENCES [dbo].[OtherEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[tblOtherEmployeeResetPassword] CHECK CONSTRAINT [FK__tblOtherE__UserI__038683F8]
GO
/****** Object:  ForeignKey [FK__Student__Academi__73DA2C14]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [FK__Student__Academi__73DA2C14] FOREIGN KEY([AcademicyearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [FK__Student__Academi__73DA2C14]
GO
/****** Object:  ForeignKey [FK__Student__Admissi__74CE504D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [FK__Student__Admissi__74CE504D] FOREIGN KEY([AdmissionClass])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [FK__Student__Admissi__74CE504D]
GO
/****** Object:  ForeignKey [FK__Student__Enrolle__75C27486]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [FK__Student__Enrolle__75C27486] FOREIGN KEY([EnrolledBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [FK__Student__Enrolle__75C27486]
GO
/****** Object:  ForeignKey [FK__StaffCate__Acade__2BE97B0D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StaffCategoryCommunication]  WITH CHECK ADD  CONSTRAINT [FK__StaffCate__Acade__2BE97B0D] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[StaffCategoryCommunication] CHECK CONSTRAINT [FK__StaffCate__Acade__2BE97B0D]
GO
/****** Object:  ForeignKey [FK__StaffCate__Emplo__2CDD9F46]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StaffCategoryCommunication]  WITH CHECK ADD  CONSTRAINT [FK__StaffCate__Emplo__2CDD9F46] FOREIGN KEY([EmployeeTypeId])
REFERENCES [dbo].[EmployeeType] ([EmployeeTypeId])
GO
ALTER TABLE [dbo].[StaffCategoryCommunication] CHECK CONSTRAINT [FK__StaffCate__Emplo__2CDD9F46]
GO
/****** Object:  ForeignKey [FK__StaffCate__Marke__2DD1C37F]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StaffCategoryCommunication]  WITH CHECK ADD  CONSTRAINT [FK__StaffCate__Marke__2DD1C37F] FOREIGN KEY([MarkedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[StaffCategoryCommunication] CHECK CONSTRAINT [FK__StaffCate__Marke__2DD1C37F]
GO
/****** Object:  ForeignKey [FK__AssignFac__Acade__7F80E8EA]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignFacultyToSubject]  WITH CHECK ADD  CONSTRAINT [FK__AssignFac__Acade__7F80E8EA] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[AssignFacultyToSubject] CHECK CONSTRAINT [FK__AssignFac__Acade__7F80E8EA]
GO
/****** Object:  ForeignKey [FK__AssignFac__Emplo__00750D23]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignFacultyToSubject]  WITH CHECK ADD  CONSTRAINT [FK__AssignFac__Emplo__00750D23] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[AssignFacultyToSubject] CHECK CONSTRAINT [FK__AssignFac__Emplo__00750D23]
GO
/****** Object:  ForeignKey [FK__AssignFac__Secti__0169315C]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignFacultyToSubject]  WITH CHECK ADD  CONSTRAINT [FK__AssignFac__Secti__0169315C] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[AssignFacultyToSubject] CHECK CONSTRAINT [FK__AssignFac__Secti__0169315C]
GO
/****** Object:  ForeignKey [FK__AssignFac__Subje__025D5595]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignFacultyToSubject]  WITH CHECK ADD  CONSTRAINT [FK__AssignFac__Subje__025D5595] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[AssignFacultyToSubject] CHECK CONSTRAINT [FK__AssignFac__Subje__025D5595]
GO
/****** Object:  ForeignKey [Fk_AssignFacultyToSubject_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignFacultyToSubject]  WITH CHECK ADD  CONSTRAINT [Fk_AssignFacultyToSubject_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[AssignFacultyToSubject] CHECK CONSTRAINT [Fk_AssignFacultyToSubject_ClassId]
GO
/****** Object:  ForeignKey [FK__AssignFac__Acade__1A69E950]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignFacultyToExamRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignFac__Acade__1A69E950] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[AssignFacultyToExamRoom] CHECK CONSTRAINT [FK__AssignFac__Acade__1A69E950]
GO
/****** Object:  ForeignKey [FK__AssignFac__Class__1B5E0D89]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignFacultyToExamRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignFac__Class__1B5E0D89] FOREIGN KEY([ClassRoomId])
REFERENCES [dbo].[ClassRooms] ([ClassRoomId])
GO
ALTER TABLE [dbo].[AssignFacultyToExamRoom] CHECK CONSTRAINT [FK__AssignFac__Class__1B5E0D89]
GO
/****** Object:  ForeignKey [FK__AssignFac__Emplo__1C5231C2]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignFacultyToExamRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignFac__Emplo__1C5231C2] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[AssignFacultyToExamRoom] CHECK CONSTRAINT [FK__AssignFac__Emplo__1C5231C2]
GO
/****** Object:  ForeignKey [FK__AssignFac__ExamI__1D4655FB]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignFacultyToExamRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignFac__ExamI__1D4655FB] FOREIGN KEY([ExamId])
REFERENCES [dbo].[Exam] ([ExamId])
GO
ALTER TABLE [dbo].[AssignFacultyToExamRoom] CHECK CONSTRAINT [FK__AssignFac__ExamI__1D4655FB]
GO
/****** Object:  ForeignKey [FK__AssignFac__TimeS__1E3A7A34]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignFacultyToExamRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignFac__TimeS__1E3A7A34] FOREIGN KEY([TimeScheduleId])
REFERENCES [dbo].[ExamTimeSchedule] ([Id])
GO
ALTER TABLE [dbo].[AssignFacultyToExamRoom] CHECK CONSTRAINT [FK__AssignFac__TimeS__1E3A7A34]
GO
/****** Object:  ForeignKey [FK__AssignSub__Acade__689D8392]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubstitudeFaculty]  WITH CHECK ADD  CONSTRAINT [FK__AssignSub__Acade__689D8392] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[AssignSubstitudeFaculty] CHECK CONSTRAINT [FK__AssignSub__Acade__689D8392]
GO
/****** Object:  ForeignKey [FK__AssignSub__Actua__6991A7CB]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubstitudeFaculty]  WITH CHECK ADD  CONSTRAINT [FK__AssignSub__Actua__6991A7CB] FOREIGN KEY([ActualEmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[AssignSubstitudeFaculty] CHECK CONSTRAINT [FK__AssignSub__Actua__6991A7CB]
GO
/****** Object:  ForeignKey [FK__AssignSub__Assig__6A85CC04]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubstitudeFaculty]  WITH CHECK ADD  CONSTRAINT [FK__AssignSub__Assig__6A85CC04] FOREIGN KEY([AssignedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[AssignSubstitudeFaculty] CHECK CONSTRAINT [FK__AssignSub__Assig__6A85CC04]
GO
/****** Object:  ForeignKey [FK__AssignSub__Class__6B79F03D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubstitudeFaculty]  WITH CHECK ADD  CONSTRAINT [FK__AssignSub__Class__6B79F03D] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[AssignSubstitudeFaculty] CHECK CONSTRAINT [FK__AssignSub__Class__6B79F03D]
GO
/****** Object:  ForeignKey [FK__AssignSub__Secti__6C6E1476]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubstitudeFaculty]  WITH CHECK ADD  CONSTRAINT [FK__AssignSub__Secti__6C6E1476] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[AssignSubstitudeFaculty] CHECK CONSTRAINT [FK__AssignSub__Secti__6C6E1476]
GO
/****** Object:  ForeignKey [FK__AssignSub__Subje__6D6238AF]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubstitudeFaculty]  WITH CHECK ADD  CONSTRAINT [FK__AssignSub__Subje__6D6238AF] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[AssignSubstitudeFaculty] CHECK CONSTRAINT [FK__AssignSub__Subje__6D6238AF]
GO
/****** Object:  ForeignKey [FK__AssignSub__Subst__6E565CE8]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubstitudeFaculty]  WITH CHECK ADD  CONSTRAINT [FK__AssignSub__Subst__6E565CE8] FOREIGN KEY([SubstitudeEmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[AssignSubstitudeFaculty] CHECK CONSTRAINT [FK__AssignSub__Subst__6E565CE8]
GO
/****** Object:  ForeignKey [FK__AssignSub__Acade__6F4A8121]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubjectToSection]  WITH CHECK ADD  CONSTRAINT [FK__AssignSub__Acade__6F4A8121] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[AssignSubjectToSection] CHECK CONSTRAINT [FK__AssignSub__Acade__6F4A8121]
GO
/****** Object:  ForeignKey [FK__AssignSub__Secti__703EA55A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubjectToSection]  WITH CHECK ADD  CONSTRAINT [FK__AssignSub__Secti__703EA55A] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[AssignSubjectToSection] CHECK CONSTRAINT [FK__AssignSub__Secti__703EA55A]
GO
/****** Object:  ForeignKey [FK__AssignSub__Subje__7132C993]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubjectToSection]  WITH CHECK ADD  CONSTRAINT [FK__AssignSub__Subje__7132C993] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[AssignSubjectToSection] CHECK CONSTRAINT [FK__AssignSub__Subje__7132C993]
GO
/****** Object:  ForeignKey [Fk_AssignSubjectToSection_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubjectToSection]  WITH CHECK ADD  CONSTRAINT [Fk_AssignSubjectToSection_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[AssignSubjectToSection] CHECK CONSTRAINT [Fk_AssignSubjectToSection_ClassId]
GO
/****** Object:  ForeignKey [FK__AssignSub__Acade__731B1205]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubjectToPeriod]  WITH CHECK ADD  CONSTRAINT [FK__AssignSub__Acade__731B1205] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[AssignSubjectToPeriod] CHECK CONSTRAINT [FK__AssignSub__Acade__731B1205]
GO
/****** Object:  ForeignKey [FK__AssignSub__Secti__740F363E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubjectToPeriod]  WITH CHECK ADD  CONSTRAINT [FK__AssignSub__Secti__740F363E] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[AssignSubjectToPeriod] CHECK CONSTRAINT [FK__AssignSub__Secti__740F363E]
GO
/****** Object:  ForeignKey [FK__AssignSub__Subje__75035A77]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubjectToPeriod]  WITH CHECK ADD  CONSTRAINT [FK__AssignSub__Subje__75035A77] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[AssignSubjectToPeriod] CHECK CONSTRAINT [FK__AssignSub__Subje__75035A77]
GO
/****** Object:  ForeignKey [Fk_AssignSubjectToPeriod_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignSubjectToPeriod]  WITH CHECK ADD  CONSTRAINT [Fk_AssignSubjectToPeriod_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[AssignSubjectToPeriod] CHECK CONSTRAINT [Fk_AssignSubjectToPeriod_ClassId]
GO
/****** Object:  ForeignKey [FK__Assignmen__Acade__76EBA2E9]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD  CONSTRAINT [FK__Assignmen__Acade__76EBA2E9] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[Assignments] CHECK CONSTRAINT [FK__Assignmen__Acade__76EBA2E9]
GO
/****** Object:  ForeignKey [FK__Assignmen__ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD  CONSTRAINT [FK__Assignmen__ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[Assignments] CHECK CONSTRAINT [FK__Assignmen__ClassId]
GO
/****** Object:  ForeignKey [FK__Assignmen__Emplo__78D3EB5B]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD  CONSTRAINT [FK__Assignmen__Emplo__78D3EB5B] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[Assignments] CHECK CONSTRAINT [FK__Assignmen__Emplo__78D3EB5B]
GO
/****** Object:  ForeignKey [FK__Assignmen__Secti__79C80F94]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD  CONSTRAINT [FK__Assignmen__Secti__79C80F94] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[Assignments] CHECK CONSTRAINT [FK__Assignmen__Secti__79C80F94]
GO
/****** Object:  ForeignKey [FK__Assignmen__Subje__7ABC33CD]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD  CONSTRAINT [FK__Assignmen__Subje__7ABC33CD] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[Assignments] CHECK CONSTRAINT [FK__Assignmen__Subje__7ABC33CD]
GO
/****** Object:  ForeignKey [FK__AssignCla__Acade__0EC32C7A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignClassTeacher]  WITH CHECK ADD  CONSTRAINT [FK__AssignCla__Acade__0EC32C7A] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[AssignClassTeacher] CHECK CONSTRAINT [FK__AssignCla__Acade__0EC32C7A]
GO
/****** Object:  ForeignKey [FK__AssignCla__Emplo__0FB750B3]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignClassTeacher]  WITH CHECK ADD  CONSTRAINT [FK__AssignCla__Emplo__0FB750B3] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[AssignClassTeacher] CHECK CONSTRAINT [FK__AssignCla__Emplo__0FB750B3]
GO
/****** Object:  ForeignKey [FK__AssignCla__Secti__10AB74EC]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignClassTeacher]  WITH CHECK ADD  CONSTRAINT [FK__AssignCla__Secti__10AB74EC] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[AssignClassTeacher] CHECK CONSTRAINT [FK__AssignCla__Secti__10AB74EC]
GO
/****** Object:  ForeignKey [FK__AssignCla__Subje__119F9925]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignClassTeacher]  WITH CHECK ADD  CONSTRAINT [FK__AssignCla__Subje__119F9925] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[AssignClassTeacher] CHECK CONSTRAINT [FK__AssignCla__Subje__119F9925]
GO
/****** Object:  ForeignKey [Fk_AssignClassTeacher_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignClassTeacher]  WITH CHECK ADD  CONSTRAINT [Fk_AssignClassTeacher_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[AssignClassTeacher] CHECK CONSTRAINT [Fk_AssignClassTeacher_ClassId]
GO
/****** Object:  ForeignKey [Fk_ActivityIncharge_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ActivityIncharge]  WITH CHECK ADD  CONSTRAINT [Fk_ActivityIncharge_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[ActivityIncharge] CHECK CONSTRAINT [Fk_ActivityIncharge_AcademicYearId]
GO
/****** Object:  ForeignKey [Fk_ActivityIncharge_ActivityId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ActivityIncharge]  WITH CHECK ADD  CONSTRAINT [Fk_ActivityIncharge_ActivityId] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[ExtraActivities] ([ActivityId])
GO
ALTER TABLE [dbo].[ActivityIncharge] CHECK CONSTRAINT [Fk_ActivityIncharge_ActivityId]
GO
/****** Object:  ForeignKey [Fk_ActivityIncharge_EmployeeRegisterId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ActivityIncharge]  WITH CHECK ADD  CONSTRAINT [Fk_ActivityIncharge_EmployeeRegisterId] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[ActivityIncharge] CHECK CONSTRAINT [Fk_ActivityIncharge_EmployeeRegisterId]
GO
/****** Object:  ForeignKey [FK__EmployeeS__Emplo__54817C4C]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeSalaryConfiguration]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeS__Emplo__54817C4C] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[EmployeeSalaryConfiguration] CHECK CONSTRAINT [FK__EmployeeS__Emplo__54817C4C]
GO
/****** Object:  ForeignKey [FK__EmployeeS__Enter__5575A085]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeSalaryConfiguration]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeS__Enter__5575A085] FOREIGN KEY([EnteredBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[EmployeeSalaryConfiguration] CHECK CONSTRAINT [FK__EmployeeS__Enter__5575A085]
GO
/****** Object:  ForeignKey [FK__EmployeeS__Other__5669C4BE]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeSalaryConfiguration]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeS__Other__5669C4BE] FOREIGN KEY([OtherEmployeeregisterId])
REFERENCES [dbo].[OtherEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[EmployeeSalaryConfiguration] CHECK CONSTRAINT [FK__EmployeeS__Other__5669C4BE]
GO
/****** Object:  ForeignKey [FK__EmployeeS__Emplo__3E923B2D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeSalary]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeS__Emplo__3E923B2D] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[EmployeeSalary] CHECK CONSTRAINT [FK__EmployeeS__Emplo__3E923B2D]
GO
/****** Object:  ForeignKey [FK__EmployeeS__Emplo__3F865F66]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeSalary]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeS__Emplo__3F865F66] FOREIGN KEY([EmployeeTypeId])
REFERENCES [dbo].[EmployeeType] ([EmployeeTypeId])
GO
ALTER TABLE [dbo].[EmployeeSalary] CHECK CONSTRAINT [FK__EmployeeS__Emplo__3F865F66]
GO
/****** Object:  ForeignKey [FK__EmployeeS__Enter__407A839F]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeSalary]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeS__Enter__407A839F] FOREIGN KEY([EnteredBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[EmployeeSalary] CHECK CONSTRAINT [FK__EmployeeS__Enter__407A839F]
GO
/****** Object:  ForeignKey [FK__EmployeeS__Other__416EA7D8]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeSalary]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeS__Other__416EA7D8] FOREIGN KEY([OtherEmployeeregisterId])
REFERENCES [dbo].[OtherEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[EmployeeSalary] CHECK CONSTRAINT [FK__EmployeeS__Other__416EA7D8]
GO
/****** Object:  ForeignKey [FK__ELearning__Acade__7775B2CE]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ELearning]  WITH CHECK ADD  CONSTRAINT [FK__ELearning__Acade__7775B2CE] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[ELearning] CHECK CONSTRAINT [FK__ELearning__Acade__7775B2CE]
GO
/****** Object:  ForeignKey [FK__ELearning__Class__7869D707]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ELearning]  WITH CHECK ADD  CONSTRAINT [FK__ELearning__Class__7869D707] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[ELearning] CHECK CONSTRAINT [FK__ELearning__Class__7869D707]
GO
/****** Object:  ForeignKey [FK__ELearning__Emplo__76818E95]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ELearning]  WITH CHECK ADD  CONSTRAINT [FK__ELearning__Emplo__76818E95] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[ELearning] CHECK CONSTRAINT [FK__ELearning__Emplo__76818E95]
GO
/****** Object:  ForeignKey [FK__ELearning__Secti__795DFB40]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ELearning]  WITH CHECK ADD  CONSTRAINT [FK__ELearning__Secti__795DFB40] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[ELearning] CHECK CONSTRAINT [FK__ELearning__Secti__795DFB40]
GO
/****** Object:  ForeignKey [FK__ELearning__Subje__7A521F79]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ELearning]  WITH CHECK ADD  CONSTRAINT [FK__ELearning__Subje__7A521F79] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[ELearning] CHECK CONSTRAINT [FK__ELearning__Subje__7A521F79]
GO
/****** Object:  ForeignKey [FK__EmployeeD__Acade__12C8C788]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeDailyAttendance]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeD__Acade__12C8C788] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[EmployeeDailyAttendance] CHECK CONSTRAINT [FK__EmployeeD__Acade__12C8C788]
GO
/****** Object:  ForeignKey [FK__EmployeeD__Emplo__13BCEBC1]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeDailyAttendance]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeD__Emplo__13BCEBC1] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[EmployeeDailyAttendance] CHECK CONSTRAINT [FK__EmployeeD__Emplo__13BCEBC1]
GO
/****** Object:  ForeignKey [FK__EmployeeA__Acade__14B10FFA]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeAchievements]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeA__Acade__14B10FFA] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[EmployeeAchievements] CHECK CONSTRAINT [FK__EmployeeA__Acade__14B10FFA]
GO
/****** Object:  ForeignKey [FK__EmployeeA__Emplo__15A53433]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeAchievements]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeA__Emplo__15A53433] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[EmployeeAchievements] CHECK CONSTRAINT [FK__EmployeeA__Emplo__15A53433]
GO
/****** Object:  ForeignKey [Fk_Circular_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Circular]  WITH CHECK ADD  CONSTRAINT [Fk_Circular_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[Circular] CHECK CONSTRAINT [Fk_Circular_AcademicYearId]
GO
/****** Object:  ForeignKey [Fk_Circular_IssuedBy]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Circular]  WITH CHECK ADD  CONSTRAINT [Fk_Circular_IssuedBy] FOREIGN KEY([IssuedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[Circular] CHECK CONSTRAINT [Fk_Circular_IssuedBy]
GO
/****** Object:  ForeignKey [FK__BookEntry__Emplo__3B2BBE9D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[BookEntry]  WITH CHECK ADD  CONSTRAINT [FK__BookEntry__Emplo__3B2BBE9D] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[BookEntry] CHECK CONSTRAINT [FK__BookEntry__Emplo__3B2BBE9D]
GO
/****** Object:  ForeignKey [FK__FeesRefun__Emplo__0D64F3ED]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeesRefund]  WITH CHECK ADD  CONSTRAINT [FK__FeesRefun__Emplo__0D64F3ED] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[FeesRefund] CHECK CONSTRAINT [FK__FeesRefun__Emplo__0D64F3ED]
GO
/****** Object:  ForeignKey [FK__FeesRefun__FeeId__0C70CFB4]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeesRefund]  WITH CHECK ADD  CONSTRAINT [FK__FeesRefun__FeeId__0C70CFB4] FOREIGN KEY([FeeId])
REFERENCES [dbo].[Fee] ([FeeId])
GO
ALTER TABLE [dbo].[FeesRefund] CHECK CONSTRAINT [FK__FeesRefun__FeeId__0C70CFB4]
GO
/****** Object:  ForeignKey [FK__HomeWork__Academ__7A8729A3]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HomeWork]  WITH CHECK ADD  CONSTRAINT [FK__HomeWork__Academ__7A8729A3] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[HomeWork] CHECK CONSTRAINT [FK__HomeWork__Academ__7A8729A3]
GO
/****** Object:  ForeignKey [FK__HomeWork__ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HomeWork]  WITH CHECK ADD  CONSTRAINT [FK__HomeWork__ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[HomeWork] CHECK CONSTRAINT [FK__HomeWork__ClassId]
GO
/****** Object:  ForeignKey [FK__HomeWork__Employ__7C6F7215]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HomeWork]  WITH CHECK ADD  CONSTRAINT [FK__HomeWork__Employ__7C6F7215] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[HomeWork] CHECK CONSTRAINT [FK__HomeWork__Employ__7C6F7215]
GO
/****** Object:  ForeignKey [FK__HomeWork__Sectio__7D63964E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HomeWork]  WITH CHECK ADD  CONSTRAINT [FK__HomeWork__Sectio__7D63964E] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[HomeWork] CHECK CONSTRAINT [FK__HomeWork__Sectio__7D63964E]
GO
/****** Object:  ForeignKey [FK__HomeWork__Subjec__7E57BA87]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HomeWork]  WITH CHECK ADD  CONSTRAINT [FK__HomeWork__Subjec__7E57BA87] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[HomeWork] CHECK CONSTRAINT [FK__HomeWork__Subjec__7E57BA87]
GO
/****** Object:  ForeignKey [FK__FacultyLe__Acade__2116E6DF]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FacultyLeaveRequest]  WITH CHECK ADD  CONSTRAINT [FK__FacultyLe__Acade__2116E6DF] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[FacultyLeaveRequest] CHECK CONSTRAINT [FK__FacultyLe__Acade__2116E6DF]
GO
/****** Object:  ForeignKey [FK__FacultyLe__Emplo__220B0B18]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FacultyLeaveRequest]  WITH CHECK ADD  CONSTRAINT [FK__FacultyLe__Emplo__220B0B18] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[FacultyLeaveRequest] CHECK CONSTRAINT [FK__FacultyLe__Emplo__220B0B18]
GO
/****** Object:  ForeignKey [FK__FacultyDa__Acade__22FF2F51]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FacultyDailyAttendance]  WITH CHECK ADD  CONSTRAINT [FK__FacultyDa__Acade__22FF2F51] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[FacultyDailyAttendance] CHECK CONSTRAINT [FK__FacultyDa__Acade__22FF2F51]
GO
/****** Object:  ForeignKey [FK__FacultyDa__Emplo__23F3538A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FacultyDailyAttendance]  WITH CHECK ADD  CONSTRAINT [FK__FacultyDa__Emplo__23F3538A] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[FacultyDailyAttendance] CHECK CONSTRAINT [FK__FacultyDa__Emplo__23F3538A]
GO
/****** Object:  ForeignKey [FK__ClassComm__Acade__1CA7377D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ClassCommunication]  WITH CHECK ADD  CONSTRAINT [FK__ClassComm__Acade__1CA7377D] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[ClassCommunication] CHECK CONSTRAINT [FK__ClassComm__Acade__1CA7377D]
GO
/****** Object:  ForeignKey [FK__ClassComm__Class__1D9B5BB6]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ClassCommunication]  WITH CHECK ADD  CONSTRAINT [FK__ClassComm__Class__1D9B5BB6] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[ClassCommunication] CHECK CONSTRAINT [FK__ClassComm__Class__1D9B5BB6]
GO
/****** Object:  ForeignKey [FK__ClassComm__Marke__1E8F7FEF]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ClassCommunication]  WITH CHECK ADD  CONSTRAINT [FK__ClassComm__Marke__1E8F7FEF] FOREIGN KEY([MarkedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[ClassCommunication] CHECK CONSTRAINT [FK__ClassComm__Marke__1E8F7FEF]
GO
/****** Object:  ForeignKey [FK__ExamClass__Acade__297722B6]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__ExamClass__Acade__297722B6] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[ExamClassRoom] CHECK CONSTRAINT [FK__ExamClass__Acade__297722B6]
GO
/****** Object:  ForeignKey [FK__ExamClass__Class__2A6B46EF]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__ExamClass__Class__2A6B46EF] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[ExamClassRoom] CHECK CONSTRAINT [FK__ExamClass__Class__2A6B46EF]
GO
/****** Object:  ForeignKey [FK__ExamClass__ExamI__2B5F6B28]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__ExamClass__ExamI__2B5F6B28] FOREIGN KEY([ExamId])
REFERENCES [dbo].[Exam] ([ExamId])
GO
ALTER TABLE [dbo].[ExamClassRoom] CHECK CONSTRAINT [FK__ExamClass__ExamI__2B5F6B28]
GO
/****** Object:  ForeignKey [FK__ExamClass__Secti__2C538F61]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__ExamClass__Secti__2C538F61] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[ExamClassRoom] CHECK CONSTRAINT [FK__ExamClass__Secti__2C538F61]
GO
/****** Object:  ForeignKey [FK__ExamClass__Subje__2D47B39A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__ExamClass__Subje__2D47B39A] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[ExamClassRoom] CHECK CONSTRAINT [FK__ExamClass__Subje__2D47B39A]
GO
/****** Object:  ForeignKey [FK__ExamTimet__Acade__1D114BD1]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamTimetable]  WITH CHECK ADD  CONSTRAINT [FK__ExamTimet__Acade__1D114BD1] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[ExamTimetable] CHECK CONSTRAINT [FK__ExamTimet__Acade__1D114BD1]
GO
/****** Object:  ForeignKey [FK__ExamTimet__ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamTimetable]  WITH CHECK ADD  CONSTRAINT [FK__ExamTimet__ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[ExamTimetable] CHECK CONSTRAINT [FK__ExamTimet__ClassId]
GO
/****** Object:  ForeignKey [FK__ExamTimet__ExamI__1EF99443]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamTimetable]  WITH CHECK ADD  CONSTRAINT [FK__ExamTimet__ExamI__1EF99443] FOREIGN KEY([ExamId])
REFERENCES [dbo].[Exam] ([ExamId])
GO
ALTER TABLE [dbo].[ExamTimetable] CHECK CONSTRAINT [FK__ExamTimet__ExamI__1EF99443]
GO
/****** Object:  ForeignKey [FK__ExamTimet__Secti__1FEDB87C]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamTimetable]  WITH CHECK ADD  CONSTRAINT [FK__ExamTimet__Secti__1FEDB87C] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[ExamTimetable] CHECK CONSTRAINT [FK__ExamTimet__Secti__1FEDB87C]
GO
/****** Object:  ForeignKey [FK__ExamTimet__Subje__20E1DCB5]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamTimetable]  WITH CHECK ADD  CONSTRAINT [FK__ExamTimet__Subje__20E1DCB5] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[ExamTimetable] CHECK CONSTRAINT [FK__ExamTimet__Subje__20E1DCB5]
GO
/****** Object:  ForeignKey [FK__ExamTimet__TimeS__21D600EE]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamTimetable]  WITH CHECK ADD  CONSTRAINT [FK__ExamTimet__TimeS__21D600EE] FOREIGN KEY([TimeScheduleId])
REFERENCES [dbo].[ExamTimeSchedule] ([Id])
GO
ALTER TABLE [dbo].[ExamTimetable] CHECK CONSTRAINT [FK__ExamTimet__TimeS__21D600EE]
GO
/****** Object:  ForeignKey [Fk_HostelWarden_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelWarden]  WITH CHECK ADD  CONSTRAINT [Fk_HostelWarden_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[HostelWarden] CHECK CONSTRAINT [Fk_HostelWarden_AcademicYearId]
GO
/****** Object:  ForeignKey [Fk_HostelWarden_EmployeeRegisterId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelWarden]  WITH CHECK ADD  CONSTRAINT [Fk_HostelWarden_EmployeeRegisterId] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[HostelWarden] CHECK CONSTRAINT [Fk_HostelWarden_EmployeeRegisterId]
GO
/****** Object:  ForeignKey [Fk_Mark_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Mark]  WITH CHECK ADD  CONSTRAINT [Fk_Mark_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[Mark] CHECK CONSTRAINT [Fk_Mark_ClassId]
GO
/****** Object:  ForeignKey [Fk_Mark_SectionId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Mark]  WITH CHECK ADD  CONSTRAINT [Fk_Mark_SectionId] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[Mark] CHECK CONSTRAINT [Fk_Mark_SectionId]
GO
/****** Object:  ForeignKey [FkMark_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Mark]  WITH CHECK ADD  CONSTRAINT [FkMark_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[Mark] CHECK CONSTRAINT [FkMark_AcademicYearId]
GO
/****** Object:  ForeignKey [FkMark_SubjectId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[Mark]  WITH CHECK ADD  CONSTRAINT [FkMark_SubjectId] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[Mark] CHECK CONSTRAINT [FkMark_SubjectId]
GO
/****** Object:  ForeignKey [Fk_MapRoleUser_EmployeeRegisterId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[MapRoleUser]  WITH CHECK ADD  CONSTRAINT [Fk_MapRoleUser_EmployeeRegisterId] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[MapRoleUser] CHECK CONSTRAINT [Fk_MapRoleUser_EmployeeRegisterId]
GO
/****** Object:  ForeignKey [Fk_MapRoleUser_RoleId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[MapRoleUser]  WITH CHECK ADD  CONSTRAINT [Fk_MapRoleUser_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[MapRoleUser] CHECK CONSTRAINT [Fk_MapRoleUser_RoleId]
GO
/****** Object:  ForeignKey [FK__ParentReg__Onlin__314D4EA8]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentRegister]  WITH CHECK ADD  CONSTRAINT [FK__ParentReg__Onlin__314D4EA8] FOREIGN KEY([OnlineRegisterId])
REFERENCES [dbo].[PreAdmissionOnLineRegister] ([OnlineRegisterId])
GO
ALTER TABLE [dbo].[ParentRegister] CHECK CONSTRAINT [FK__ParentReg__Onlin__314D4EA8]
GO
/****** Object:  ForeignKey [FK__ParentReg__Stude__6462DE5A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentRegister]  WITH CHECK ADD  CONSTRAINT [FK__ParentReg__Stude__6462DE5A] FOREIGN KEY([StudentAdmissionId])
REFERENCES [dbo].[PreAdmissionStudentRegister] ([StudentAdmissionId])
GO
ALTER TABLE [dbo].[ParentRegister] CHECK CONSTRAINT [FK__ParentReg__Stude__6462DE5A]
GO
/****** Object:  ForeignKey [FK__ParentLea__Acade__5B0E7E4A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentLeaveRequest]  WITH CHECK ADD  CONSTRAINT [FK__ParentLea__Acade__5B0E7E4A] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[ParentLeaveRequest] CHECK CONSTRAINT [FK__ParentLea__Acade__5B0E7E4A]
GO
/****** Object:  ForeignKey [FK__ParentLea__Emplo__5C02A283]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentLeaveRequest]  WITH CHECK ADD  CONSTRAINT [FK__ParentLea__Emplo__5C02A283] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[ParentLeaveRequest] CHECK CONSTRAINT [FK__ParentLea__Emplo__5C02A283]
GO
/****** Object:  ForeignKey [FK__ParentLea__Secti__5DEAEAF5]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentLeaveRequest]  WITH CHECK ADD  CONSTRAINT [FK__ParentLea__Secti__5DEAEAF5] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[ParentLeaveRequest] CHECK CONSTRAINT [FK__ParentLea__Secti__5DEAEAF5]
GO
/****** Object:  ForeignKey [FK__ParentLea__Stude__0D0FEE32]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentLeaveRequest]  WITH CHECK ADD  CONSTRAINT [FK__ParentLea__Stude__0D0FEE32] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[ParentLeaveRequest] CHECK CONSTRAINT [FK__ParentLea__Stude__0D0FEE32]
GO
/****** Object:  ForeignKey [FK__ParentLea_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentLeaveRequest]  WITH CHECK ADD  CONSTRAINT [FK__ParentLea_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[ParentLeaveRequest] CHECK CONSTRAINT [FK__ParentLea_ClassId]
GO
/****** Object:  ForeignKey [FK__ParentFac__Acade__68687968]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__ParentFac__Acade__68687968] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[ParentFacultyFeedback] CHECK CONSTRAINT [FK__ParentFac__Acade__68687968]
GO
/****** Object:  ForeignKey [FK__ParentFac__Acade__695C9DA1]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__ParentFac__Acade__695C9DA1] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[ParentFacultyFeedback] CHECK CONSTRAINT [FK__ParentFac__Acade__695C9DA1]
GO
/****** Object:  ForeignKey [FK__ParentFac__Emplo__6A50C1DA]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__ParentFac__Emplo__6A50C1DA] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[ParentFacultyFeedback] CHECK CONSTRAINT [FK__ParentFac__Emplo__6A50C1DA]
GO
/****** Object:  ForeignKey [FK__ParentFac__Emplo__6B44E613]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__ParentFac__Emplo__6B44E613] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[ParentFacultyFeedback] CHECK CONSTRAINT [FK__ParentFac__Emplo__6B44E613]
GO
/****** Object:  ForeignKey [FK__ParentFac__Secti__6E2152BE]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__ParentFac__Secti__6E2152BE] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[ParentFacultyFeedback] CHECK CONSTRAINT [FK__ParentFac__Secti__6E2152BE]
GO
/****** Object:  ForeignKey [FK__ParentFac__Secti__6F1576F7]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__ParentFac__Secti__6F1576F7] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[ParentFacultyFeedback] CHECK CONSTRAINT [FK__ParentFac__Secti__6F1576F7]
GO
/****** Object:  ForeignKey [FK__ParentFac__Stude__7DCDAAA2]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__ParentFac__Stude__7DCDAAA2] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[ParentFacultyFeedback] CHECK CONSTRAINT [FK__ParentFac__Stude__7DCDAAA2]
GO
/****** Object:  ForeignKey [FK__ParentFac__Stude__7EC1CEDB]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__ParentFac__Stude__7EC1CEDB] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[ParentFacultyFeedback] CHECK CONSTRAINT [FK__ParentFac__Stude__7EC1CEDB]
GO
/****** Object:  ForeignKey [FK__ParentFac__Subje__71F1E3A2]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__ParentFac__Subje__71F1E3A2] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[ParentFacultyFeedback] CHECK CONSTRAINT [FK__ParentFac__Subje__71F1E3A2]
GO
/****** Object:  ForeignKey [FK__ParentFac__Subje__72E607DB]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__ParentFac__Subje__72E607DB] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[ParentFacultyFeedback] CHECK CONSTRAINT [FK__ParentFac__Subje__72E607DB]
GO
/****** Object:  ForeignKey [FK_ParentFacultyFeedback_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ParentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK_ParentFacultyFeedback_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[ParentFacultyFeedback] CHECK CONSTRAINT [FK_ParentFacultyFeedback_ClassId]
GO
/****** Object:  ForeignKey [FK__MedicalIn__Stude__74EE4BDE]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[MedicalInfo]  WITH CHECK ADD  CONSTRAINT [FK__MedicalIn__Stude__74EE4BDE] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[MedicalInfo] CHECK CONSTRAINT [FK__MedicalIn__Stude__74EE4BDE]
GO
/****** Object:  ForeignKey [FK__MarkTotal__Acade__004002F9]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[MarkTotal]  WITH CHECK ADD  CONSTRAINT [FK__MarkTotal__Acade__004002F9] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[MarkTotal] CHECK CONSTRAINT [FK__MarkTotal__Acade__004002F9]
GO
/****** Object:  ForeignKey [FK__MarkTotal__ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[MarkTotal]  WITH CHECK ADD  CONSTRAINT [FK__MarkTotal__ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[MarkTotal] CHECK CONSTRAINT [FK__MarkTotal__ClassId]
GO
/****** Object:  ForeignKey [FK__MarkTotal__Emplo__02284B6B]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[MarkTotal]  WITH CHECK ADD  CONSTRAINT [FK__MarkTotal__Emplo__02284B6B] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[MarkTotal] CHECK CONSTRAINT [FK__MarkTotal__Emplo__02284B6B]
GO
/****** Object:  ForeignKey [FK__MarkTotal__ExamI__031C6FA4]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[MarkTotal]  WITH CHECK ADD  CONSTRAINT [FK__MarkTotal__ExamI__031C6FA4] FOREIGN KEY([ExamId])
REFERENCES [dbo].[Exam] ([ExamId])
GO
ALTER TABLE [dbo].[MarkTotal] CHECK CONSTRAINT [FK__MarkTotal__ExamI__031C6FA4]
GO
/****** Object:  ForeignKey [FK__MarkTotal__Secti__041093DD]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[MarkTotal]  WITH CHECK ADD  CONSTRAINT [FK__MarkTotal__Secti__041093DD] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[MarkTotal] CHECK CONSTRAINT [FK__MarkTotal__Secti__041093DD]
GO
/****** Object:  ForeignKey [FK__MarkTotal__Stude__6ABAD62E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[MarkTotal]  WITH CHECK ADD  CONSTRAINT [FK__MarkTotal__Stude__6ABAD62E] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[MarkTotal] CHECK CONSTRAINT [FK__MarkTotal__Stude__6ABAD62E]
GO
/****** Object:  ForeignKey [FK__LibraryFe__Acade__60E75331]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[LibraryFeeCollection]  WITH CHECK ADD  CONSTRAINT [FK__LibraryFe__Acade__60E75331] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[LibraryFeeCollection] CHECK CONSTRAINT [FK__LibraryFe__Acade__60E75331]
GO
/****** Object:  ForeignKey [FK__LibraryFe__Class__61DB776A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[LibraryFeeCollection]  WITH CHECK ADD  CONSTRAINT [FK__LibraryFe__Class__61DB776A] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[LibraryFeeCollection] CHECK CONSTRAINT [FK__LibraryFe__Class__61DB776A]
GO
/****** Object:  ForeignKey [FK__LibraryFe__Emplo__62CF9BA3]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[LibraryFeeCollection]  WITH CHECK ADD  CONSTRAINT [FK__LibraryFe__Emplo__62CF9BA3] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[LibraryFeeCollection] CHECK CONSTRAINT [FK__LibraryFe__Emplo__62CF9BA3]
GO
/****** Object:  ForeignKey [FK__LibraryFe__Secti__63C3BFDC]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[LibraryFeeCollection]  WITH CHECK ADD  CONSTRAINT [FK__LibraryFe__Secti__63C3BFDC] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[LibraryFeeCollection] CHECK CONSTRAINT [FK__LibraryFe__Secti__63C3BFDC]
GO
/****** Object:  ForeignKey [FK__LibraryFe__Stude__64B7E415]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[LibraryFeeCollection]  WITH CHECK ADD  CONSTRAINT [FK__LibraryFe__Stude__64B7E415] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[LibraryFeeCollection] CHECK CONSTRAINT [FK__LibraryFe__Stude__64B7E415]
GO
/****** Object:  ForeignKey [Fk_HostelStudent_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelStudent]  WITH CHECK ADD  CONSTRAINT [Fk_HostelStudent_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[HostelStudent] CHECK CONSTRAINT [Fk_HostelStudent_AcademicYearId]
GO
/****** Object:  ForeignKey [Fk_HostelStudent_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelStudent]  WITH CHECK ADD  CONSTRAINT [Fk_HostelStudent_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[HostelStudent] CHECK CONSTRAINT [Fk_HostelStudent_ClassId]
GO
/****** Object:  ForeignKey [Fk_HostelStudent_SectionId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelStudent]  WITH CHECK ADD  CONSTRAINT [Fk_HostelStudent_SectionId] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[HostelStudent] CHECK CONSTRAINT [Fk_HostelStudent_SectionId]
GO
/****** Object:  ForeignKey [Fk_HostelStudent_StudentRegisterId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelStudent]  WITH CHECK ADD  CONSTRAINT [Fk_HostelStudent_StudentRegisterId] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[HostelStudent] CHECK CONSTRAINT [Fk_HostelStudent_StudentRegisterId]
GO
/****** Object:  ForeignKey [FK__HostelFee__Acade__3508D0F3]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCollection]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__Acade__3508D0F3] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[HostelFeesCollection] CHECK CONSTRAINT [FK__HostelFee__Acade__3508D0F3]
GO
/****** Object:  ForeignKey [FK__HostelFee__Class__35FCF52C]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCollection]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__Class__35FCF52C] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[HostelFeesCollection] CHECK CONSTRAINT [FK__HostelFee__Class__35FCF52C]
GO
/****** Object:  ForeignKey [FK__HostelFee__Creat__36F11965]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCollection]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__Creat__36F11965] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[HostelFeesCollection] CHECK CONSTRAINT [FK__HostelFee__Creat__36F11965]
GO
/****** Object:  ForeignKey [FK__HostelFee__Secti__37E53D9E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCollection]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__Secti__37E53D9E] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[HostelFeesCollection] CHECK CONSTRAINT [FK__HostelFee__Secti__37E53D9E]
GO
/****** Object:  ForeignKey [FK__HostelFee__Stude__38D961D7]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCollection]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__Stude__38D961D7] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[HostelFeesCollection] CHECK CONSTRAINT [FK__HostelFee__Stude__38D961D7]
GO
/****** Object:  ForeignKey [FK__HostelFee__Updat__39CD8610]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCollection]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__Updat__39CD8610] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[HostelFeesCollection] CHECK CONSTRAINT [FK__HostelFee__Updat__39CD8610]
GO
/****** Object:  ForeignKey [FK__EmployeeS__Emplo__27E3DFFF]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeSalaryIncrement]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeS__Emplo__27E3DFFF] FOREIGN KEY([EmployeeRegisterid])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[EmployeeSalaryIncrement] CHECK CONSTRAINT [FK__EmployeeS__Emplo__27E3DFFF]
GO
/****** Object:  ForeignKey [FK__EmployeeS__Emplo__2E90DD8E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeSalaryIncrement]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeS__Emplo__2E90DD8E] FOREIGN KEY([EmployeeSalaryConfigurationId])
REFERENCES [dbo].[EmployeeSalaryConfiguration] ([EmployeeSalaryConfigurationId])
GO
ALTER TABLE [dbo].[EmployeeSalaryIncrement] CHECK CONSTRAINT [FK__EmployeeS__Emplo__2E90DD8E]
GO
/****** Object:  ForeignKey [FK__EmployeeS__Entry__26EFBBC6]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeSalaryIncrement]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeS__Entry__26EFBBC6] FOREIGN KEY([Entryby])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[EmployeeSalaryIncrement] CHECK CONSTRAINT [FK__EmployeeS__Entry__26EFBBC6]
GO
/****** Object:  ForeignKey [FK__EmployeeS__Other__28D80438]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EmployeeSalaryIncrement]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeS__Other__28D80438] FOREIGN KEY([OtherEmployeeRegisterid])
REFERENCES [dbo].[OtherEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[EmployeeSalaryIncrement] CHECK CONSTRAINT [FK__EmployeeS__Other__28D80438]
GO
/****** Object:  ForeignKey [FK__ExamRemar__Acade__22CA2527]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ExamRemar__Acade__22CA2527] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[ExamRemarks] CHECK CONSTRAINT [FK__ExamRemar__Acade__22CA2527]
GO
/****** Object:  ForeignKey [FK__ExamRemar__ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ExamRemar__ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[ExamRemarks] CHECK CONSTRAINT [FK__ExamRemar__ClassId]
GO
/****** Object:  ForeignKey [FK__ExamRemar__Emplo__24B26D99]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ExamRemar__Emplo__24B26D99] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[ExamRemarks] CHECK CONSTRAINT [FK__ExamRemar__Emplo__24B26D99]
GO
/****** Object:  ForeignKey [FK__ExamRemar__ExamI__25A691D2]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ExamRemar__ExamI__25A691D2] FOREIGN KEY([ExamId])
REFERENCES [dbo].[Exam] ([ExamId])
GO
ALTER TABLE [dbo].[ExamRemarks] CHECK CONSTRAINT [FK__ExamRemar__ExamI__25A691D2]
GO
/****** Object:  ForeignKey [FK__ExamRemar__Secti__269AB60B]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ExamRemar__Secti__269AB60B] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[ExamRemarks] CHECK CONSTRAINT [FK__ExamRemar__Secti__269AB60B]
GO
/****** Object:  ForeignKey [FK__ExamRemar__Stude__3CF40B7E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ExamRemar__Stude__3CF40B7E] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[ExamRemarks] CHECK CONSTRAINT [FK__ExamRemar__Stude__3CF40B7E]
GO
/****** Object:  ForeignKey [FK__ExamRemar__Subje__2882FE7D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ExamRemar__Subje__2882FE7D] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[ExamRemarks] CHECK CONSTRAINT [FK__ExamRemar__Subje__2882FE7D]
GO
/****** Object:  ForeignKey [FK__EntryBook__BookE__44B528D7]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EntryBookId]  WITH CHECK ADD  CONSTRAINT [FK__EntryBook__BookE__44B528D7] FOREIGN KEY([BookEntryRegisterId])
REFERENCES [dbo].[BookEntry] ([BookEntryRegisterId])
GO
ALTER TABLE [dbo].[EntryBookId] CHECK CONSTRAINT [FK__EntryBook__BookE__44B528D7]
GO
/****** Object:  ForeignKey [FK__FatherReg__Stude__2077C861]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FatherRegister]  WITH CHECK ADD  CONSTRAINT [FK__FatherReg__Stude__2077C861] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[FatherRegister] CHECK CONSTRAINT [FK__FatherReg__Stude__2077C861]
GO
/****** Object:  ForeignKey [FK__HomeworkR__HomeW__69279377]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HomeworkReplyFromStudent]  WITH CHECK ADD  CONSTRAINT [FK__HomeworkR__HomeW__69279377] FOREIGN KEY([HomeWorkId])
REFERENCES [dbo].[HomeWork] ([HomeWorkId])
GO
ALTER TABLE [dbo].[HomeworkReplyFromStudent] CHECK CONSTRAINT [FK__HomeworkR__HomeW__69279377]
GO
/****** Object:  ForeignKey [FK__HomeworkR__Remar__6B0FDBE9]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HomeworkReplyFromStudent]  WITH CHECK ADD  CONSTRAINT [FK__HomeworkR__Remar__6B0FDBE9] FOREIGN KEY([RemarkGivenBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[HomeworkReplyFromStudent] CHECK CONSTRAINT [FK__HomeworkR__Remar__6B0FDBE9]
GO
/****** Object:  ForeignKey [FK__HomeworkR__Stude__6A1BB7B0]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HomeworkReplyFromStudent]  WITH CHECK ADD  CONSTRAINT [FK__HomeworkR__Stude__6A1BB7B0] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[HomeworkReplyFromStudent] CHECK CONSTRAINT [FK__HomeworkR__Stude__6A1BB7B0]
GO
/****** Object:  ForeignKey [FK__GuardianR__Stude__226010D3]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[GuardianRegister]  WITH CHECK ADD  CONSTRAINT [FK__GuardianR__Stude__226010D3] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[GuardianRegister] CHECK CONSTRAINT [FK__GuardianR__Stude__226010D3]
GO
/****** Object:  ForeignKey [FK__GradeComm__Acade__3B60C8C7]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[GradeComment]  WITH CHECK ADD  CONSTRAINT [FK__GradeComm__Acade__3B60C8C7] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[GradeComment] CHECK CONSTRAINT [FK__GradeComm__Acade__3B60C8C7]
GO
/****** Object:  ForeignKey [FK__GradeComm__Class__3C54ED00]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[GradeComment]  WITH CHECK ADD  CONSTRAINT [FK__GradeComm__Class__3C54ED00] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[GradeComment] CHECK CONSTRAINT [FK__GradeComm__Class__3C54ED00]
GO
/****** Object:  ForeignKey [FK__GradeComm__Emplo__40257DE4]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[GradeComment]  WITH CHECK ADD  CONSTRAINT [FK__GradeComm__Emplo__40257DE4] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[GradeComment] CHECK CONSTRAINT [FK__GradeComm__Emplo__40257DE4]
GO
/****** Object:  ForeignKey [FK__GradeComm__ExamI__3E3D3572]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[GradeComment]  WITH CHECK ADD  CONSTRAINT [FK__GradeComm__ExamI__3E3D3572] FOREIGN KEY([ExamId])
REFERENCES [dbo].[Exam] ([ExamId])
GO
ALTER TABLE [dbo].[GradeComment] CHECK CONSTRAINT [FK__GradeComm__ExamI__3E3D3572]
GO
/****** Object:  ForeignKey [FK__GradeComm__Secti__3D491139]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[GradeComment]  WITH CHECK ADD  CONSTRAINT [FK__GradeComm__Secti__3D491139] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[GradeComment] CHECK CONSTRAINT [FK__GradeComm__Secti__3D491139]
GO
/****** Object:  ForeignKey [FK__GradeComm__Stude__3F3159AB]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[GradeComment]  WITH CHECK ADD  CONSTRAINT [FK__GradeComm__Stude__3F3159AB] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[GradeComment] CHECK CONSTRAINT [FK__GradeComm__Stude__3F3159AB]
GO
/****** Object:  ForeignKey [FK__GradeComm__Updat__4119A21D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[GradeComment]  WITH CHECK ADD  CONSTRAINT [FK__GradeComm__Updat__4119A21D] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[GradeComment] CHECK CONSTRAINT [FK__GradeComm__Updat__4119A21D]
GO
/****** Object:  ForeignKey [FK__FeeCollec__Emplo__150615B5]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeeCollection]  WITH CHECK ADD  CONSTRAINT [FK__FeeCollec__Emplo__150615B5] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[FeeCollection] CHECK CONSTRAINT [FK__FeeCollec__Emplo__150615B5]
GO
/****** Object:  ForeignKey [Fk_AcademicYearId_FeeCollection]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeeCollection]  WITH CHECK ADD  CONSTRAINT [Fk_AcademicYearId_FeeCollection] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[FeeCollection] CHECK CONSTRAINT [Fk_AcademicYearId_FeeCollection]
GO
/****** Object:  ForeignKey [Fk_FeeCollection_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeeCollection]  WITH CHECK ADD  CONSTRAINT [Fk_FeeCollection_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[FeeCollection] CHECK CONSTRAINT [Fk_FeeCollection_ClassId]
GO
/****** Object:  ForeignKey [Fk_SectionId_FeeCollection]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeeCollection]  WITH CHECK ADD  CONSTRAINT [Fk_SectionId_FeeCollection] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[FeeCollection] CHECK CONSTRAINT [Fk_SectionId_FeeCollection]
GO
/****** Object:  ForeignKey [Fk_StudentRegisterId_FeeCollection]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeeCollection]  WITH CHECK ADD  CONSTRAINT [Fk_StudentRegisterId_FeeCollection] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[FeeCollection] CHECK CONSTRAINT [Fk_StudentRegisterId_FeeCollection]
GO
/****** Object:  ForeignKey [FK__BookAutho__BookE__3FF073BA]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[BookAuthors]  WITH CHECK ADD  CONSTRAINT [FK__BookAutho__BookE__3FF073BA] FOREIGN KEY([BookEntryRegisterId])
REFERENCES [dbo].[BookEntry] ([BookEntryRegisterId])
GO
ALTER TABLE [dbo].[BookAuthors] CHECK CONSTRAINT [FK__BookAutho__BookE__3FF073BA]
GO
/****** Object:  ForeignKey [FK__ELearning__ELear__04CFADEC]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ELearningReplyFromStudent]  WITH CHECK ADD  CONSTRAINT [FK__ELearning__ELear__04CFADEC] FOREIGN KEY([ELearningId])
REFERENCES [dbo].[ELearning] ([ELearningId])
GO
ALTER TABLE [dbo].[ELearningReplyFromStudent] CHECK CONSTRAINT [FK__ELearning__ELear__04CFADEC]
GO
/****** Object:  ForeignKey [FK__ELearning__Remar__06B7F65E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ELearningReplyFromStudent]  WITH CHECK ADD  CONSTRAINT [FK__ELearning__Remar__06B7F65E] FOREIGN KEY([RemarkGivenBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[ELearningReplyFromStudent] CHECK CONSTRAINT [FK__ELearning__Remar__06B7F65E]
GO
/****** Object:  ForeignKey [FK__ELearning__Stude__05C3D225]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ELearningReplyFromStudent]  WITH CHECK ADD  CONSTRAINT [FK__ELearning__Stude__05C3D225] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[ELearningReplyFromStudent] CHECK CONSTRAINT [FK__ELearning__Stude__05C3D225]
GO
/****** Object:  ForeignKey [FK__ClassInch__Acade__04459E07]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ClassInchargeStudentRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ClassInch__Acade__04459E07] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[ClassInchargeStudentRemarks] CHECK CONSTRAINT [FK__ClassInch__Acade__04459E07]
GO
/****** Object:  ForeignKey [FK__ClassInch__Emplo__0539C240]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ClassInchargeStudentRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ClassInch__Emplo__0539C240] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[ClassInchargeStudentRemarks] CHECK CONSTRAINT [FK__ClassInch__Emplo__0539C240]
GO
/****** Object:  ForeignKey [FK__ClassInch__Secti__062DE679]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ClassInchargeStudentRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ClassInch__Secti__062DE679] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[ClassInchargeStudentRemarks] CHECK CONSTRAINT [FK__ClassInch__Secti__062DE679]
GO
/****** Object:  ForeignKey [FK__ClassInch__Stude__29E1370A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ClassInchargeStudentRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ClassInch__Stude__29E1370A] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[ClassInchargeStudentRemarks] CHECK CONSTRAINT [FK__ClassInch__Stude__29E1370A]
GO
/****** Object:  ForeignKey [Fk_ClassInchargeStudentRemarks_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ClassInchargeStudentRemarks]  WITH CHECK ADD  CONSTRAINT [Fk_ClassInchargeStudentRemarks_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[ClassInchargeStudentRemarks] CHECK CONSTRAINT [Fk_ClassInchargeStudentRemarks_ClassId]
GO
/****** Object:  ForeignKey [FK__AssignCla__Class__090A5324]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignClassToStudent]  WITH CHECK ADD  CONSTRAINT [FK__AssignCla__Class__090A5324] FOREIGN KEY([ClassAssignedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[AssignClassToStudent] CHECK CONSTRAINT [FK__AssignCla__Class__090A5324]
GO
/****** Object:  ForeignKey [FK__AssignCla__Separ__09FE775D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignClassToStudent]  WITH CHECK ADD  CONSTRAINT [FK__AssignCla__Separ__09FE775D] FOREIGN KEY([SeparatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[AssignClassToStudent] CHECK CONSTRAINT [FK__AssignCla__Separ__09FE775D]
GO
/****** Object:  ForeignKey [FK__AssignCla__Stude__0880433F]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignClassToStudent]  WITH CHECK ADD  CONSTRAINT [FK__AssignCla__Stude__0880433F] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[AssignClassToStudent] CHECK CONSTRAINT [FK__AssignCla__Stude__0880433F]
GO
/****** Object:  ForeignKey [Fk_AssignClassToStudent_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignClassToStudent]  WITH CHECK ADD  CONSTRAINT [Fk_AssignClassToStudent_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[AssignClassToStudent] CHECK CONSTRAINT [Fk_AssignClassToStudent_AcademicYearId]
GO
/****** Object:  ForeignKey [Fk_AssignClassToStudent_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignClassToStudent]  WITH CHECK ADD  CONSTRAINT [Fk_AssignClassToStudent_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[AssignClassToStudent] CHECK CONSTRAINT [Fk_AssignClassToStudent_ClassId]
GO
/****** Object:  ForeignKey [Fk_AssignClassToStudent_SectionId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignClassToStudent]  WITH CHECK ADD  CONSTRAINT [Fk_AssignClassToStudent_SectionId] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[AssignClassToStudent] CHECK CONSTRAINT [Fk_AssignClassToStudent_SectionId]
GO
/****** Object:  ForeignKey [Fk_AssignActivityToStudent_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignActivityToStudent]  WITH CHECK ADD  CONSTRAINT [Fk_AssignActivityToStudent_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[AssignActivityToStudent] CHECK CONSTRAINT [Fk_AssignActivityToStudent_AcademicYearId]
GO
/****** Object:  ForeignKey [Fk_AssignActivityToStudent_ActivityId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignActivityToStudent]  WITH CHECK ADD  CONSTRAINT [Fk_AssignActivityToStudent_ActivityId] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[ExtraActivities] ([ActivityId])
GO
ALTER TABLE [dbo].[AssignActivityToStudent] CHECK CONSTRAINT [Fk_AssignActivityToStudent_ActivityId]
GO
/****** Object:  ForeignKey [Fk_AssignActivityToStudent_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignActivityToStudent]  WITH CHECK ADD  CONSTRAINT [Fk_AssignActivityToStudent_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[AssignActivityToStudent] CHECK CONSTRAINT [Fk_AssignActivityToStudent_ClassId]
GO
/****** Object:  ForeignKey [Fk_AssignActivityToStudent_SectionId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignActivityToStudent]  WITH CHECK ADD  CONSTRAINT [Fk_AssignActivityToStudent_SectionId] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[AssignActivityToStudent] CHECK CONSTRAINT [Fk_AssignActivityToStudent_SectionId]
GO
/****** Object:  ForeignKey [Fk_AssignActivityToStudent_StudentRegisterId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignActivityToStudent]  WITH CHECK ADD  CONSTRAINT [Fk_AssignActivityToStudent_StudentRegisterId] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[AssignActivityToStudent] CHECK CONSTRAINT [Fk_AssignActivityToStudent_StudentRegisterId]
GO
/****** Object:  ForeignKey [FK__ApplySepa__Stude__7EF6D905]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ApplySeparation]  WITH CHECK ADD  CONSTRAINT [FK__ApplySepa__Stude__7EF6D905] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[ApplySeparation] CHECK CONSTRAINT [FK__ApplySepa__Stude__7EF6D905]
GO
/****** Object:  ForeignKey [FK__Assignmen__Assig__6FD49106]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignmentReplyFromStudent]  WITH CHECK ADD  CONSTRAINT [FK__Assignmen__Assig__6FD49106] FOREIGN KEY([AssignmentId])
REFERENCES [dbo].[Assignments] ([AssignmentId])
GO
ALTER TABLE [dbo].[AssignmentReplyFromStudent] CHECK CONSTRAINT [FK__Assignmen__Assig__6FD49106]
GO
/****** Object:  ForeignKey [FK__Assignmen__Remar__71BCD978]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignmentReplyFromStudent]  WITH CHECK ADD  CONSTRAINT [FK__Assignmen__Remar__71BCD978] FOREIGN KEY([RemarkGivenBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[AssignmentReplyFromStudent] CHECK CONSTRAINT [FK__Assignmen__Remar__71BCD978]
GO
/****** Object:  ForeignKey [FK__Assignmen__Stude__70C8B53F]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignmentReplyFromStudent]  WITH CHECK ADD  CONSTRAINT [FK__Assignmen__Stude__70C8B53F] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[AssignmentReplyFromStudent] CHECK CONSTRAINT [FK__Assignmen__Stude__70C8B53F]
GO
/****** Object:  ForeignKey [Fk_StudentRollNumber_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentRollNumber]  WITH CHECK ADD  CONSTRAINT [Fk_StudentRollNumber_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[StudentRollNumber] CHECK CONSTRAINT [Fk_StudentRollNumber_AcademicYearId]
GO
/****** Object:  ForeignKey [Fk_StudentRollNumber_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentRollNumber]  WITH CHECK ADD  CONSTRAINT [Fk_StudentRollNumber_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[StudentRollNumber] CHECK CONSTRAINT [Fk_StudentRollNumber_ClassId]
GO
/****** Object:  ForeignKey [Fk_StudentRollNumber_SectionId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentRollNumber]  WITH CHECK ADD  CONSTRAINT [Fk_StudentRollNumber_SectionId] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[StudentRollNumber] CHECK CONSTRAINT [Fk_StudentRollNumber_SectionId]
GO
/****** Object:  ForeignKey [Fk_StudentRollNumber_StudentRegisterId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentRollNumber]  WITH CHECK ADD  CONSTRAINT [Fk_StudentRollNumber_StudentRegisterId] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[StudentRollNumber] CHECK CONSTRAINT [Fk_StudentRollNumber_StudentRegisterId]
GO
/****** Object:  ForeignKey [FK__StudentMa__Acade__320C68B7]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentMarks]  WITH CHECK ADD  CONSTRAINT [FK__StudentMa__Acade__320C68B7] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[StudentMarks] CHECK CONSTRAINT [FK__StudentMa__Acade__320C68B7]
GO
/****** Object:  ForeignKey [FK__StudentMa__Emplo__33008CF0]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentMarks]  WITH CHECK ADD  CONSTRAINT [FK__StudentMa__Emplo__33008CF0] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[StudentMarks] CHECK CONSTRAINT [FK__StudentMa__Emplo__33008CF0]
GO
/****** Object:  ForeignKey [FK__StudentMa__ExamI__33F4B129]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentMarks]  WITH CHECK ADD  CONSTRAINT [FK__StudentMa__ExamI__33F4B129] FOREIGN KEY([ExamId])
REFERENCES [dbo].[Exam] ([ExamId])
GO
ALTER TABLE [dbo].[StudentMarks] CHECK CONSTRAINT [FK__StudentMa__ExamI__33F4B129]
GO
/****** Object:  ForeignKey [FK__StudentMa__Remar__07AC1A97]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentMarks]  WITH CHECK ADD  CONSTRAINT [FK__StudentMa__Remar__07AC1A97] FOREIGN KEY([RemarkUpdatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[StudentMarks] CHECK CONSTRAINT [FK__StudentMa__Remar__07AC1A97]
GO
/****** Object:  ForeignKey [FK__StudentMa__Secti__34E8D562]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentMarks]  WITH CHECK ADD  CONSTRAINT [FK__StudentMa__Secti__34E8D562] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[StudentMarks] CHECK CONSTRAINT [FK__StudentMa__Secti__34E8D562]
GO
/****** Object:  ForeignKey [FK__StudentMa__Stude__3429BB53]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentMarks]  WITH CHECK ADD  CONSTRAINT [FK__StudentMa__Stude__3429BB53] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[StudentMarks] CHECK CONSTRAINT [FK__StudentMa__Stude__3429BB53]
GO
/****** Object:  ForeignKey [FK__StudentMa__Subje__36D11DD4]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentMarks]  WITH CHECK ADD  CONSTRAINT [FK__StudentMa__Subje__36D11DD4] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[StudentMarks] CHECK CONSTRAINT [FK__StudentMa__Subje__36D11DD4]
GO
/****** Object:  ForeignKey [FK__StudentMark_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentMarks]  WITH CHECK ADD  CONSTRAINT [FK__StudentMark_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[StudentMarks] CHECK CONSTRAINT [FK__StudentMark_ClassId]
GO
/****** Object:  ForeignKey [FK__StudentGr__Acade__38B96646]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentGrade]  WITH CHECK ADD  CONSTRAINT [FK__StudentGr__Acade__38B96646] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[StudentGrade] CHECK CONSTRAINT [FK__StudentGr__Acade__38B96646]
GO
/****** Object:  ForeignKey [FK__StudentGr__Emplo__39AD8A7F]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentGrade]  WITH CHECK ADD  CONSTRAINT [FK__StudentGr__Emplo__39AD8A7F] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[StudentGrade] CHECK CONSTRAINT [FK__StudentGr__Emplo__39AD8A7F]
GO
/****** Object:  ForeignKey [FK__StudentGr__ExamI__3AA1AEB8]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentGrade]  WITH CHECK ADD  CONSTRAINT [FK__StudentGr__ExamI__3AA1AEB8] FOREIGN KEY([ExamId])
REFERENCES [dbo].[Exam] ([ExamId])
GO
ALTER TABLE [dbo].[StudentGrade] CHECK CONSTRAINT [FK__StudentGr__ExamI__3AA1AEB8]
GO
/****** Object:  ForeignKey [FK__StudentGr__Secti__3B95D2F1]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentGrade]  WITH CHECK ADD  CONSTRAINT [FK__StudentGr__Secti__3B95D2F1] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[StudentGrade] CHECK CONSTRAINT [FK__StudentGr__Secti__3B95D2F1]
GO
/****** Object:  ForeignKey [FK__StudentGr__Stude__2D7CBDC4]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentGrade]  WITH CHECK ADD  CONSTRAINT [FK__StudentGr__Stude__2D7CBDC4] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[StudentGrade] CHECK CONSTRAINT [FK__StudentGr__Stude__2D7CBDC4]
GO
/****** Object:  ForeignKey [FK__StudentGr__Subje__3D7E1B63]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentGrade]  WITH CHECK ADD  CONSTRAINT [FK__StudentGr__Subje__3D7E1B63] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[StudentGrade] CHECK CONSTRAINT [FK__StudentGr__Subje__3D7E1B63]
GO
/****** Object:  ForeignKey [FK__StudentGrade_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentGrade]  WITH CHECK ADD  CONSTRAINT [FK__StudentGrade_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[StudentGrade] CHECK CONSTRAINT [FK__StudentGrade_ClassId]
GO
/****** Object:  ForeignKey [FK__StudentFa__Acade__3F6663D5]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentFacultyQueries]  WITH CHECK ADD  CONSTRAINT [FK__StudentFa__Acade__3F6663D5] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[StudentFacultyQueries] CHECK CONSTRAINT [FK__StudentFa__Acade__3F6663D5]
GO
/****** Object:  ForeignKey [FK__StudentFa__ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentFacultyQueries]  WITH CHECK ADD  CONSTRAINT [FK__StudentFa__ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[StudentFacultyQueries] CHECK CONSTRAINT [FK__StudentFa__ClassId]
GO
/****** Object:  ForeignKey [FK__StudentFa__Emplo__414EAC47]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentFacultyQueries]  WITH CHECK ADD  CONSTRAINT [FK__StudentFa__Emplo__414EAC47] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[StudentFacultyQueries] CHECK CONSTRAINT [FK__StudentFa__Emplo__414EAC47]
GO
/****** Object:  ForeignKey [FK__StudentFa__Prima__4242D080]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentFacultyQueries]  WITH CHECK ADD  CONSTRAINT [FK__StudentFa__Prima__4242D080] FOREIGN KEY([PrimaryUserRegId])
REFERENCES [dbo].[PrimaryUserRegister] ([PrimaryUserRegisterId])
GO
ALTER TABLE [dbo].[StudentFacultyQueries] CHECK CONSTRAINT [FK__StudentFa__Prima__4242D080]
GO
/****** Object:  ForeignKey [FK__StudentFa__Reply__4336F4B9]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentFacultyQueries]  WITH CHECK ADD  CONSTRAINT [FK__StudentFa__Reply__4336F4B9] FOREIGN KEY([ReplyEmployeeRegId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[StudentFacultyQueries] CHECK CONSTRAINT [FK__StudentFa__Reply__4336F4B9]
GO
/****** Object:  ForeignKey [FK__StudentFa__Secti__442B18F2]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentFacultyQueries]  WITH CHECK ADD  CONSTRAINT [FK__StudentFa__Secti__442B18F2] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[StudentFacultyQueries] CHECK CONSTRAINT [FK__StudentFa__Secti__442B18F2]
GO
/****** Object:  ForeignKey [FK__StudentFa__Stude__26CFC035]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentFacultyQueries]  WITH CHECK ADD  CONSTRAINT [FK__StudentFa__Stude__26CFC035] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[StudentFacultyQueries] CHECK CONSTRAINT [FK__StudentFa__Stude__26CFC035]
GO
/****** Object:  ForeignKey [FK__StudentFa__Subje__46136164]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentFacultyQueries]  WITH CHECK ADD  CONSTRAINT [FK__StudentFa__Subje__46136164] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[StudentFacultyQueries] CHECK CONSTRAINT [FK__StudentFa__Subje__46136164]
GO
/****** Object:  ForeignKey [FK__StudentFa__Acade__4707859D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__StudentFa__Acade__4707859D] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[StudentFacultyFeedback] CHECK CONSTRAINT [FK__StudentFa__Acade__4707859D]
GO
/****** Object:  ForeignKey [FK__StudentFa__Emplo__47FBA9D6]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__StudentFa__Emplo__47FBA9D6] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[StudentFacultyFeedback] CHECK CONSTRAINT [FK__StudentFa__Emplo__47FBA9D6]
GO
/****** Object:  ForeignKey [FK__StudentFa__Secti__48EFCE0F]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__StudentFa__Secti__48EFCE0F] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[StudentFacultyFeedback] CHECK CONSTRAINT [FK__StudentFa__Secti__48EFCE0F]
GO
/****** Object:  ForeignKey [FK__StudentFa__Stude__2116E6DF]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__StudentFa__Stude__2116E6DF] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[StudentFacultyFeedback] CHECK CONSTRAINT [FK__StudentFa__Stude__2116E6DF]
GO
/****** Object:  ForeignKey [FK__StudentFa__Subje__4AD81681]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__StudentFa__Subje__4AD81681] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[StudentFacultyFeedback] CHECK CONSTRAINT [FK__StudentFa__Subje__4AD81681]
GO
/****** Object:  ForeignKey [FK__StudentFa_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentFacultyFeedback]  WITH CHECK ADD  CONSTRAINT [FK__StudentFa_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[StudentFacultyFeedback] CHECK CONSTRAINT [FK__StudentFa_ClassId]
GO
/****** Object:  ForeignKey [FK__StudentDa__Acade__4CC05EF3]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentDailyAttendance]  WITH CHECK ADD  CONSTRAINT [FK__StudentDa__Acade__4CC05EF3] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[StudentDailyAttendance] CHECK CONSTRAINT [FK__StudentDa__Acade__4CC05EF3]
GO
/****** Object:  ForeignKey [FK__StudentDa__Atten__2A363CC5]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentDailyAttendance]  WITH CHECK ADD  CONSTRAINT [FK__StudentDa__Atten__2A363CC5] FOREIGN KEY([AttendanceUpdatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[StudentDailyAttendance] CHECK CONSTRAINT [FK__StudentDa__Atten__2A363CC5]
GO
/****** Object:  ForeignKey [FK__StudentDa__ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentDailyAttendance]  WITH CHECK ADD  CONSTRAINT [FK__StudentDa__ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[StudentDailyAttendance] CHECK CONSTRAINT [FK__StudentDa__ClassId]
GO
/****** Object:  ForeignKey [FK__StudentDa__Emplo__4EA8A765]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentDailyAttendance]  WITH CHECK ADD  CONSTRAINT [FK__StudentDa__Emplo__4EA8A765] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[StudentDailyAttendance] CHECK CONSTRAINT [FK__StudentDa__Emplo__4EA8A765]
GO
/****** Object:  ForeignKey [FK__StudentDa__Secti__4F9CCB9E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentDailyAttendance]  WITH CHECK ADD  CONSTRAINT [FK__StudentDa__Secti__4F9CCB9E] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[StudentDailyAttendance] CHECK CONSTRAINT [FK__StudentDa__Secti__4F9CCB9E]
GO
/****** Object:  ForeignKey [FK__StudentDa__Stude__1C5231C2]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentDailyAttendance]  WITH CHECK ADD  CONSTRAINT [FK__StudentDa__Stude__1C5231C2] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[StudentDailyAttendance] CHECK CONSTRAINT [FK__StudentDa__Stude__1C5231C2]
GO
/****** Object:  ForeignKey [FK__StudentCo__Acade__2354350C]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentCommunication]  WITH CHECK ADD  CONSTRAINT [FK__StudentCo__Acade__2354350C] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[StudentCommunication] CHECK CONSTRAINT [FK__StudentCo__Acade__2354350C]
GO
/****** Object:  ForeignKey [FK__StudentCo__Class__24485945]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentCommunication]  WITH CHECK ADD  CONSTRAINT [FK__StudentCo__Class__24485945] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[StudentCommunication] CHECK CONSTRAINT [FK__StudentCo__Class__24485945]
GO
/****** Object:  ForeignKey [FK__StudentCo__Marke__2724C5F0]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentCommunication]  WITH CHECK ADD  CONSTRAINT [FK__StudentCo__Marke__2724C5F0] FOREIGN KEY([MarkedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[StudentCommunication] CHECK CONSTRAINT [FK__StudentCo__Marke__2724C5F0]
GO
/****** Object:  ForeignKey [FK__StudentCo__Secti__253C7D7E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentCommunication]  WITH CHECK ADD  CONSTRAINT [FK__StudentCo__Secti__253C7D7E] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[StudentCommunication] CHECK CONSTRAINT [FK__StudentCo__Secti__253C7D7E]
GO
/****** Object:  ForeignKey [FK__StudentCo__Stude__2630A1B7]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentCommunication]  WITH CHECK ADD  CONSTRAINT [FK__StudentCo__Stude__2630A1B7] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[StudentCommunication] CHECK CONSTRAINT [FK__StudentCo__Stude__2630A1B7]
GO
/****** Object:  ForeignKey [Fk_TransportDetails_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportDetails]  WITH CHECK ADD  CONSTRAINT [Fk_TransportDetails_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[TransportDetails] CHECK CONSTRAINT [Fk_TransportDetails_AcademicYearId]
GO
/****** Object:  ForeignKey [Fk_TransportDetails_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportDetails]  WITH CHECK ADD  CONSTRAINT [Fk_TransportDetails_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[TransportDetails] CHECK CONSTRAINT [Fk_TransportDetails_ClassId]
GO
/****** Object:  ForeignKey [Fk_TransportDetails_SectionId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportDetails]  WITH CHECK ADD  CONSTRAINT [Fk_TransportDetails_SectionId] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[TransportDetails] CHECK CONSTRAINT [Fk_TransportDetails_SectionId]
GO
/****** Object:  ForeignKey [Fk_TransportDetails_StudentRegisterId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportDetails]  WITH CHECK ADD  CONSTRAINT [Fk_TransportDetails_StudentRegisterId] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[TransportDetails] CHECK CONSTRAINT [Fk_TransportDetails_StudentRegisterId]
GO
/****** Object:  ForeignKey [FK__RoomAllot__Block__27AED5D5]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[RoomAllotment]  WITH CHECK ADD  CONSTRAINT [FK__RoomAllot__Block__27AED5D5] FOREIGN KEY([BlockId])
REFERENCES [dbo].[HostelBlock] ([BlockId])
GO
ALTER TABLE [dbo].[RoomAllotment] CHECK CONSTRAINT [FK__RoomAllot__Block__27AED5D5]
GO
/****** Object:  ForeignKey [FK__RoomAllot__Emplo__17786E0C]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[RoomAllotment]  WITH CHECK ADD  CONSTRAINT [FK__RoomAllot__Emplo__17786E0C] FOREIGN KEY([EmployeeregisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[RoomAllotment] CHECK CONSTRAINT [FK__RoomAllot__Emplo__17786E0C]
GO
/****** Object:  ForeignKey [FK__RoomAllot__Emplo__28A2FA0E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[RoomAllotment]  WITH CHECK ADD  CONSTRAINT [FK__RoomAllot__Emplo__28A2FA0E] FOREIGN KEY([EmployeeregisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[RoomAllotment] CHECK CONSTRAINT [FK__RoomAllot__Emplo__28A2FA0E]
GO
/****** Object:  ForeignKey [FK__RoomAllot__Other__186C9245]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[RoomAllotment]  WITH CHECK ADD  CONSTRAINT [FK__RoomAllot__Other__186C9245] FOREIGN KEY([OtherEmpoyeeRegisterId])
REFERENCES [dbo].[OtherEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[RoomAllotment] CHECK CONSTRAINT [FK__RoomAllot__Other__186C9245]
GO
/****** Object:  ForeignKey [FK__RoomAllot__Other__29971E47]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[RoomAllotment]  WITH CHECK ADD  CONSTRAINT [FK__RoomAllot__Other__29971E47] FOREIGN KEY([OtherEmpoyeeRegisterId])
REFERENCES [dbo].[OtherEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[RoomAllotment] CHECK CONSTRAINT [FK__RoomAllot__Other__29971E47]
GO
/****** Object:  ForeignKey [FK__RoomAllot__RoomI__2A8B4280]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[RoomAllotment]  WITH CHECK ADD  CONSTRAINT [FK__RoomAllot__RoomI__2A8B4280] FOREIGN KEY([RoomId])
REFERENCES [dbo].[HostelRoom] ([RoomId])
GO
ALTER TABLE [dbo].[RoomAllotment] CHECK CONSTRAINT [FK__RoomAllot__RoomI__2A8B4280]
GO
/****** Object:  ForeignKey [FK__RoomAllot__Stude__1A54DAB7]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[RoomAllotment]  WITH CHECK ADD  CONSTRAINT [FK__RoomAllot__Stude__1A54DAB7] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[RoomAllotment] CHECK CONSTRAINT [FK__RoomAllot__Stude__1A54DAB7]
GO
/****** Object:  ForeignKey [FK__RoomAllot__Stude__2B7F66B9]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[RoomAllotment]  WITH CHECK ADD  CONSTRAINT [FK__RoomAllot__Stude__2B7F66B9] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[RoomAllotment] CHECK CONSTRAINT [FK__RoomAllot__Stude__2B7F66B9]
GO
/****** Object:  ForeignKey [FK__RegisterS__Stude__379037E3]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[PreAdmissionRegisterStatusFlag]  WITH CHECK ADD  CONSTRAINT [FK__RegisterS__Stude__379037E3] FOREIGN KEY([StudentAdmissionId])
REFERENCES [dbo].[PreAdmissionStudentRegister] ([StudentAdmissionId])
GO
ALTER TABLE [dbo].[PreAdmissionRegisterStatusFlag] CHECK CONSTRAINT [FK__RegisterS__Stude__379037E3]
GO
/****** Object:  ForeignKey [FK__PreAdmiss__Fathe__3DB3258D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[PreAdmissionTransaction]  WITH CHECK ADD  CONSTRAINT [FK__PreAdmiss__Fathe__3DB3258D] FOREIGN KEY([FatherAdmissionId])
REFERENCES [dbo].[PreAdmissionFatherRegister] ([FatherAdmissionId])
GO
ALTER TABLE [dbo].[PreAdmissionTransaction] CHECK CONSTRAINT [FK__PreAdmiss__Fathe__3DB3258D]
GO
/****** Object:  ForeignKey [FK__PreAdmiss__Guard__3EA749C6]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[PreAdmissionTransaction]  WITH CHECK ADD  CONSTRAINT [FK__PreAdmiss__Guard__3EA749C6] FOREIGN KEY([GuardianAdmissionId])
REFERENCES [dbo].[PreAdmissionGuardianRegister] ([GuardianAdmissionId])
GO
ALTER TABLE [dbo].[PreAdmissionTransaction] CHECK CONSTRAINT [FK__PreAdmiss__Guard__3EA749C6]
GO
/****** Object:  ForeignKey [FK__PreAdmiss__Mothe__3F9B6DFF]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[PreAdmissionTransaction]  WITH CHECK ADD  CONSTRAINT [FK__PreAdmiss__Mothe__3F9B6DFF] FOREIGN KEY([MotherAdmissionId])
REFERENCES [dbo].[PreAdmissionMotherRegister] ([MotherAdmissionId])
GO
ALTER TABLE [dbo].[PreAdmissionTransaction] CHECK CONSTRAINT [FK__PreAdmiss__Mothe__3F9B6DFF]
GO
/****** Object:  ForeignKey [FK__PreAdmiss__Prima__408F9238]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[PreAdmissionTransaction]  WITH CHECK ADD  CONSTRAINT [FK__PreAdmiss__Prima__408F9238] FOREIGN KEY([PrimaryUserAdmissionId])
REFERENCES [dbo].[PreAdmissionPrimaryUserRegister] ([PrimaryUserAdmissionId])
GO
ALTER TABLE [dbo].[PreAdmissionTransaction] CHECK CONSTRAINT [FK__PreAdmiss__Prima__408F9238]
GO
/****** Object:  ForeignKey [FK__PreAdmiss__Stude__7B4643B2]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[PreAdmissionTransaction]  WITH CHECK ADD  CONSTRAINT [FK__PreAdmiss__Stude__7B4643B2] FOREIGN KEY([StudentAdmissionId])
REFERENCES [dbo].[PreAdmissionStudentRegister] ([StudentAdmissionId])
GO
ALTER TABLE [dbo].[PreAdmissionTransaction] CHECK CONSTRAINT [FK__PreAdmiss__Stude__7B4643B2]
GO
/****** Object:  ForeignKey [FK__ParentSib__Stude__1AF3F935]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[PreAdmissionParentSibling]  WITH CHECK ADD  CONSTRAINT [FK__ParentSib__Stude__1AF3F935] FOREIGN KEY([StudentAdmissionId])
REFERENCES [dbo].[PreAdmissionStudentRegister] ([StudentAdmissionId])
GO
ALTER TABLE [dbo].[PreAdmissionParentSibling] CHECK CONSTRAINT [FK__ParentSib__Stude__1AF3F935]
GO
/****** Object:  ForeignKey [FK__MotherReg__Stude__216BEC9A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[MotherRegister]  WITH CHECK ADD  CONSTRAINT [FK__MotherReg__Stude__216BEC9A] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[MotherRegister] CHECK CONSTRAINT [FK__MotherReg__Stude__216BEC9A]
GO
/****** Object:  ForeignKey [FK__ModuleInf__Acade__5F141958]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ModuleInfoStudentRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ModuleInf__Acade__5F141958] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[ModuleInfoStudentRemarks] CHECK CONSTRAINT [FK__ModuleInf__Acade__5F141958]
GO
/****** Object:  ForeignKey [FK__ModuleInf__ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ModuleInfoStudentRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ModuleInf__ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[ModuleInfoStudentRemarks] CHECK CONSTRAINT [FK__ModuleInf__ClassId]
GO
/****** Object:  ForeignKey [FK__ModuleInf__Emplo__60FC61CA]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ModuleInfoStudentRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ModuleInf__Emplo__60FC61CA] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[ModuleInfoStudentRemarks] CHECK CONSTRAINT [FK__ModuleInf__Emplo__60FC61CA]
GO
/****** Object:  ForeignKey [FK__ModuleInf__Secti__61F08603]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ModuleInfoStudentRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ModuleInf__Secti__61F08603] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[ModuleInfoStudentRemarks] CHECK CONSTRAINT [FK__ModuleInf__Secti__61F08603]
GO
/****** Object:  ForeignKey [FK__ModuleInf__Stude__7073AF84]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ModuleInfoStudentRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ModuleInf__Stude__7073AF84] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[ModuleInfoStudentRemarks] CHECK CONSTRAINT [FK__ModuleInf__Stude__7073AF84]
GO
/****** Object:  ForeignKey [FK__ModuleInf__Subje__63D8CE75]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ModuleInfoStudentRemarks]  WITH CHECK ADD  CONSTRAINT [FK__ModuleInf__Subje__63D8CE75] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[ModuleInfoStudentRemarks] CHECK CONSTRAINT [FK__ModuleInf__Subje__63D8CE75]
GO
/****** Object:  ForeignKey [FK__tblStuden__UserI__473C8FC7]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[tblStudentResetPassword]  WITH CHECK ADD  CONSTRAINT [FK__tblStuden__UserI__473C8FC7] FOREIGN KEY([UserId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[tblStudentResetPassword] CHECK CONSTRAINT [FK__tblStuden__UserI__473C8FC7]
GO
/****** Object:  ForeignKey [FK__tblSiblin__Prima__65C116E7]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[tblSibling]  WITH CHECK ADD  CONSTRAINT [FK__tblSiblin__Prima__65C116E7] FOREIGN KEY([PrimaryUserRegId])
REFERENCES [dbo].[PrimaryUserRegister] ([PrimaryUserRegisterId])
GO
ALTER TABLE [dbo].[tblSibling] CHECK CONSTRAINT [FK__tblSiblin__Prima__65C116E7]
GO
/****** Object:  ForeignKey [FK__tblSiblin__Sibli__1D9B5BB6]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[tblSibling]  WITH CHECK ADD  CONSTRAINT [FK__tblSiblin__Sibli__1D9B5BB6] FOREIGN KEY([SiblingStudentRegId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[tblSibling] CHECK CONSTRAINT [FK__tblSiblin__Sibli__1D9B5BB6]
GO
/****** Object:  ForeignKey [FK__tblSiblin__Stude__1F83A428]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[tblSibling]  WITH CHECK ADD  CONSTRAINT [FK__tblSiblin__Stude__1F83A428] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[tblSibling] CHECK CONSTRAINT [FK__tblSiblin__Stude__1F83A428]
GO
/****** Object:  ForeignKey [FK__StudentAc__Acade__51851410]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentAchievements]  WITH CHECK ADD  CONSTRAINT [FK__StudentAc__Acade__51851410] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[StudentAchievements] CHECK CONSTRAINT [FK__StudentAc__Acade__51851410]
GO
/****** Object:  ForeignKey [FK__StudentAc__ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentAchievements]  WITH CHECK ADD  CONSTRAINT [FK__StudentAc__ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[StudentAchievements] CHECK CONSTRAINT [FK__StudentAc__ClassId]
GO
/****** Object:  ForeignKey [FK__StudentAc__Secti__536D5C82]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentAchievements]  WITH CHECK ADD  CONSTRAINT [FK__StudentAc__Secti__536D5C82] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[StudentAchievements] CHECK CONSTRAINT [FK__StudentAc__Secti__536D5C82]
GO
/****** Object:  ForeignKey [FK__StudentAc__Stude__1881A0DE]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentAchievements]  WITH CHECK ADD  CONSTRAINT [FK__StudentAc__Stude__1881A0DE] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[StudentAchievements] CHECK CONSTRAINT [FK__StudentAc__Stude__1881A0DE]
GO
/****** Object:  ForeignKey [FK__SeparateS__Stude__14B10FFA]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SeparateStudent]  WITH CHECK ADD  CONSTRAINT [FK__SeparateS__Stude__14B10FFA] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[SeparateStudent] CHECK CONSTRAINT [FK__SeparateS__Stude__14B10FFA]
GO
/****** Object:  ForeignKey [Fk_SeparateStudent_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[SeparateStudent]  WITH CHECK ADD  CONSTRAINT [Fk_SeparateStudent_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[SeparateStudent] CHECK CONSTRAINT [Fk_SeparateStudent_ClassId]
GO
/****** Object:  ForeignKey [FK__Transport__Creat__7093AB15]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportPickPoint]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Creat__7093AB15] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TransportPickPoint] CHECK CONSTRAINT [FK__Transport__Creat__7093AB15]
GO
/****** Object:  ForeignKey [FK__Transport__Desti__6F9F86DC]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportPickPoint]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Desti__6F9F86DC] FOREIGN KEY([DestinationId])
REFERENCES [dbo].[TransportDestination] ([DestinationId])
GO
ALTER TABLE [dbo].[TransportPickPoint] CHECK CONSTRAINT [FK__Transport__Desti__6F9F86DC]
GO
/****** Object:  ForeignKey [FK__Transport__Updat__7187CF4E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportPickPoint]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Updat__7187CF4E] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TransportPickPoint] CHECK CONSTRAINT [FK__Transport__Updat__7187CF4E]
GO
/****** Object:  ForeignKey [FK__Transport__Creat__3DD3211E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportMapping]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Creat__3DD3211E] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TransportMapping] CHECK CONSTRAINT [FK__Transport__Creat__3DD3211E]
GO
/****** Object:  ForeignKey [FK__Transport__Desti__3A02903A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportMapping]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Desti__3A02903A] FOREIGN KEY([Destination])
REFERENCES [dbo].[TransportDestination] ([DestinationId])
GO
ALTER TABLE [dbo].[TransportMapping] CHECK CONSTRAINT [FK__Transport__Desti__3A02903A]
GO
/****** Object:  ForeignKey [FK__Transport__Drive__3BEAD8AC]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportMapping]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Drive__3BEAD8AC] FOREIGN KEY([DriverId])
REFERENCES [dbo].[OtherEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TransportMapping] CHECK CONSTRAINT [FK__Transport__Drive__3BEAD8AC]
GO
/****** Object:  ForeignKey [FK__Transport__Helpe__3CDEFCE5]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportMapping]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Helpe__3CDEFCE5] FOREIGN KEY([HelperId])
REFERENCES [dbo].[OtherEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TransportMapping] CHECK CONSTRAINT [FK__Transport__Helpe__3CDEFCE5]
GO
/****** Object:  ForeignKey [FK__Transport__Regis__3AF6B473]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportMapping]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Regis__3AF6B473] FOREIGN KEY([RegistrationNumberId])
REFERENCES [dbo].[VehicleDetails] ([Id])
GO
ALTER TABLE [dbo].[TransportMapping] CHECK CONSTRAINT [FK__Transport__Regis__3AF6B473]
GO
/****** Object:  ForeignKey [FK__Transport__Updat__3EC74557]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportMapping]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Updat__3EC74557] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TransportMapping] CHECK CONSTRAINT [FK__Transport__Updat__3EC74557]
GO
/****** Object:  ForeignKey [FK__Transport__Creat__457442E6]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportTiming]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Creat__457442E6] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TransportTiming] CHECK CONSTRAINT [FK__Transport__Creat__457442E6]
GO
/****** Object:  ForeignKey [FK__Transport__PickP__44801EAD]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportTiming]  WITH CHECK ADD  CONSTRAINT [FK__Transport__PickP__44801EAD] FOREIGN KEY([PickPointId])
REFERENCES [dbo].[TransportPickPoint] ([Id])
GO
ALTER TABLE [dbo].[TransportTiming] CHECK CONSTRAINT [FK__Transport__PickP__44801EAD]
GO
/****** Object:  ForeignKey [FK__Transport__Route__438BFA74]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportTiming]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Route__438BFA74] FOREIGN KEY([RouteId])
REFERENCES [dbo].[TransportMapping] ([RouteId])
GO
ALTER TABLE [dbo].[TransportTiming] CHECK CONSTRAINT [FK__Transport__Route__438BFA74]
GO
/****** Object:  ForeignKey [FK__Transport__Updat__4668671F]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportTiming]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Updat__4668671F] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TransportTiming] CHECK CONSTRAINT [FK__Transport__Updat__4668671F]
GO
/****** Object:  ForeignKey [FK__Transport__Acade__0A1E72EE]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportAllotment]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Acade__0A1E72EE] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[TransportAllotment] CHECK CONSTRAINT [FK__Transport__Acade__0A1E72EE]
GO
/****** Object:  ForeignKey [FK__Transport__Class__0B129727]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportAllotment]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Class__0B129727] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[TransportAllotment] CHECK CONSTRAINT [FK__Transport__Class__0B129727]
GO
/****** Object:  ForeignKey [FK__Transport__Creat__10CB707D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportAllotment]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Creat__10CB707D] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TransportAllotment] CHECK CONSTRAINT [FK__Transport__Creat__10CB707D]
GO
/****** Object:  ForeignKey [FK__Transport__Desti__0DEF03D2]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportAllotment]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Desti__0DEF03D2] FOREIGN KEY([DestinationId])
REFERENCES [dbo].[TransportDestination] ([DestinationId])
GO
ALTER TABLE [dbo].[TransportAllotment] CHECK CONSTRAINT [FK__Transport__Desti__0DEF03D2]
GO
/****** Object:  ForeignKey [FK__Transport__PickP__0EE3280B]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportAllotment]  WITH CHECK ADD  CONSTRAINT [FK__Transport__PickP__0EE3280B] FOREIGN KEY([PickPointId])
REFERENCES [dbo].[TransportPickPoint] ([Id])
GO
ALTER TABLE [dbo].[TransportAllotment] CHECK CONSTRAINT [FK__Transport__PickP__0EE3280B]
GO
/****** Object:  ForeignKey [FK__Transport__Route__0FD74C44]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportAllotment]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Route__0FD74C44] FOREIGN KEY([RouteMapId])
REFERENCES [dbo].[TransportMapping] ([RouteId])
GO
ALTER TABLE [dbo].[TransportAllotment] CHECK CONSTRAINT [FK__Transport__Route__0FD74C44]
GO
/****** Object:  ForeignKey [FK__Transport__Secti__0C06BB60]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportAllotment]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Secti__0C06BB60] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[TransportAllotment] CHECK CONSTRAINT [FK__Transport__Secti__0C06BB60]
GO
/****** Object:  ForeignKey [FK__Transport__Stude__0CFADF99]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportAllotment]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Stude__0CFADF99] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[TransportAllotment] CHECK CONSTRAINT [FK__Transport__Stude__0CFADF99]
GO
/****** Object:  ForeignKey [FK__Transport__Updat__11BF94B6]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportAllotment]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Updat__11BF94B6] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TransportAllotment] CHECK CONSTRAINT [FK__Transport__Updat__11BF94B6]
GO
/****** Object:  ForeignKey [FK__Transport__Acade__7246E95D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportFeesCollection]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Acade__7246E95D] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[TransportFeesCollection] CHECK CONSTRAINT [FK__Transport__Acade__7246E95D]
GO
/****** Object:  ForeignKey [FK__Transport__Class__733B0D96]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportFeesCollection]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Class__733B0D96] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[TransportFeesCollection] CHECK CONSTRAINT [FK__Transport__Class__733B0D96]
GO
/****** Object:  ForeignKey [FK__Transport__Creat__77FFC2B3]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportFeesCollection]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Creat__77FFC2B3] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TransportFeesCollection] CHECK CONSTRAINT [FK__Transport__Creat__77FFC2B3]
GO
/****** Object:  ForeignKey [FK__Transport__Desti__76177A41]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportFeesCollection]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Desti__76177A41] FOREIGN KEY([DestinationId])
REFERENCES [dbo].[TransportDestination] ([DestinationId])
GO
ALTER TABLE [dbo].[TransportFeesCollection] CHECK CONSTRAINT [FK__Transport__Desti__76177A41]
GO
/****** Object:  ForeignKey [FK__Transport__PickP__770B9E7A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportFeesCollection]  WITH CHECK ADD  CONSTRAINT [FK__Transport__PickP__770B9E7A] FOREIGN KEY([PickPointId])
REFERENCES [dbo].[TransportPickPoint] ([Id])
GO
ALTER TABLE [dbo].[TransportFeesCollection] CHECK CONSTRAINT [FK__Transport__PickP__770B9E7A]
GO
/****** Object:  ForeignKey [FK__Transport__Secti__742F31CF]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportFeesCollection]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Secti__742F31CF] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[TransportFeesCollection] CHECK CONSTRAINT [FK__Transport__Secti__742F31CF]
GO
/****** Object:  ForeignKey [FK__Transport__Stude__75235608]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportFeesCollection]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Stude__75235608] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[TransportFeesCollection] CHECK CONSTRAINT [FK__Transport__Stude__75235608]
GO
/****** Object:  ForeignKey [FK__Transport__Updat__78F3E6EC]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[TransportFeesCollection]  WITH CHECK ADD  CONSTRAINT [FK__Transport__Updat__78F3E6EC] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[TransportFeesCollection] CHECK CONSTRAINT [FK__Transport__Updat__78F3E6EC]
GO
/****** Object:  ForeignKey [FK__StudentBo__BookR__4F32B74A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentBookTakenReturn]  WITH CHECK ADD  CONSTRAINT [FK__StudentBo__BookR__4F32B74A] FOREIGN KEY([BookRegisterId])
REFERENCES [dbo].[EntryBookId] ([BookRegisterId])
GO
ALTER TABLE [dbo].[StudentBookTakenReturn] CHECK CONSTRAINT [FK__StudentBo__BookR__4F32B74A]
GO
/****** Object:  ForeignKey [FK__StudentBo__Given__57C7FD4B]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentBookTakenReturn]  WITH CHECK ADD  CONSTRAINT [FK__StudentBo__Given__57C7FD4B] FOREIGN KEY([GivenBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[StudentBookTakenReturn] CHECK CONSTRAINT [FK__StudentBo__Given__57C7FD4B]
GO
/****** Object:  ForeignKey [FK__StudentBo__Stude__5026DB83]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StudentBookTakenReturn]  WITH CHECK ADD  CONSTRAINT [FK__StudentBo__Stude__5026DB83] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[StudentBookTakenReturn] CHECK CONSTRAINT [FK__StudentBo__Stude__5026DB83]
GO
/****** Object:  ForeignKey [FK__StaffBook__BookR__54EB90A0]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StaffBookTakenReturn]  WITH CHECK ADD  CONSTRAINT [FK__StaffBook__BookR__54EB90A0] FOREIGN KEY([BookRegisterId])
REFERENCES [dbo].[EntryBookId] ([BookRegisterId])
GO
ALTER TABLE [dbo].[StaffBookTakenReturn] CHECK CONSTRAINT [FK__StaffBook__BookR__54EB90A0]
GO
/****** Object:  ForeignKey [FK__StaffBook__Emplo__55DFB4D9]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StaffBookTakenReturn]  WITH CHECK ADD  CONSTRAINT [FK__StaffBook__Emplo__55DFB4D9] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[StaffBookTakenReturn] CHECK CONSTRAINT [FK__StaffBook__Emplo__55DFB4D9]
GO
/****** Object:  ForeignKey [FK__StaffBook__Given__56D3D912]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[StaffBookTakenReturn]  WITH CHECK ADD  CONSTRAINT [FK__StaffBook__Given__56D3D912] FOREIGN KEY([GivenBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[StaffBookTakenReturn] CHECK CONSTRAINT [FK__StaffBook__Given__56D3D912]
GO
/****** Object:  ForeignKey [FK__AssignExa__Acade__190BB0C3]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignExa__Acade__190BB0C3] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[AssignExamClassRoom] CHECK CONSTRAINT [FK__AssignExa__Acade__190BB0C3]
GO
/****** Object:  ForeignKey [FK__AssignExa__Class__19FFD4FC]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignExa__Class__19FFD4FC] FOREIGN KEY([ClassRoomId])
REFERENCES [dbo].[ClassRooms] ([ClassRoomId])
GO
ALTER TABLE [dbo].[AssignExamClassRoom] CHECK CONSTRAINT [FK__AssignExa__Class__19FFD4FC]
GO
/****** Object:  ForeignKey [FK__AssignExa__EndRe__0B5CAFEA]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignExa__EndRe__0B5CAFEA] FOREIGN KEY([EndRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[AssignExamClassRoom] CHECK CONSTRAINT [FK__AssignExa__EndRe__0B5CAFEA]
GO
/****** Object:  ForeignKey [FK__AssignExa__EndRo__1BE81D6E]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignExa__EndRo__1BE81D6E] FOREIGN KEY([EndRollNumberId])
REFERENCES [dbo].[StudentRollNumber] ([RollNumberId])
GO
ALTER TABLE [dbo].[AssignExamClassRoom] CHECK CONSTRAINT [FK__AssignExa__EndRo__1BE81D6E]
GO
/****** Object:  ForeignKey [FK__AssignExa__ExamI__1CDC41A7]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignExa__ExamI__1CDC41A7] FOREIGN KEY([ExamId])
REFERENCES [dbo].[Exam] ([ExamId])
GO
ALTER TABLE [dbo].[AssignExamClassRoom] CHECK CONSTRAINT [FK__AssignExa__ExamI__1CDC41A7]
GO
/****** Object:  ForeignKey [FK__AssignExa__Secti__1DD065E0]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignExa__Secti__1DD065E0] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[AssignExamClassRoom] CHECK CONSTRAINT [FK__AssignExa__Secti__1DD065E0]
GO
/****** Object:  ForeignKey [FK__AssignExa__Start__0E391C95]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignExa__Start__0E391C95] FOREIGN KEY([StartRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[AssignExamClassRoom] CHECK CONSTRAINT [FK__AssignExa__Start__0E391C95]
GO
/****** Object:  ForeignKey [FK__AssignExa__Start__1FB8AE52]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignExa__Start__1FB8AE52] FOREIGN KEY([StartRollNumberId])
REFERENCES [dbo].[StudentRollNumber] ([RollNumberId])
GO
ALTER TABLE [dbo].[AssignExamClassRoom] CHECK CONSTRAINT [FK__AssignExa__Start__1FB8AE52]
GO
/****** Object:  ForeignKey [FK__AssignExa__Subje__20ACD28B]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignExa__Subje__20ACD28B] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[AssignExamClassRoom] CHECK CONSTRAINT [FK__AssignExa__Subje__20ACD28B]
GO
/****** Object:  ForeignKey [FK__AssignExa__TimeS__21A0F6C4]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignExamClassRoom]  WITH CHECK ADD  CONSTRAINT [FK__AssignExa__TimeS__21A0F6C4] FOREIGN KEY([TimeScheduleId])
REFERENCES [dbo].[ExamTimeSchedule] ([Id])
GO
ALTER TABLE [dbo].[AssignExamClassRoom] CHECK CONSTRAINT [FK__AssignExa__TimeS__21A0F6C4]
GO
/****** Object:  ForeignKey [Fk_AssignExamClassRoom_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AssignExamClassRoom]  WITH CHECK ADD  CONSTRAINT [Fk_AssignExamClassRoom_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[AssignExamClassRoom] CHECK CONSTRAINT [Fk_AssignExamClassRoom_ClassId]
GO
/****** Object:  ForeignKey [FK__Admission__Fathe__257187A8]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AdmissionTransaction]  WITH CHECK ADD  CONSTRAINT [FK__Admission__Fathe__257187A8] FOREIGN KEY([FatherRegisterId])
REFERENCES [dbo].[FatherRegister] ([FatherRegisterId])
GO
ALTER TABLE [dbo].[AdmissionTransaction] CHECK CONSTRAINT [FK__Admission__Fathe__257187A8]
GO
/****** Object:  ForeignKey [FK__Admission__Guard__2665ABE1]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AdmissionTransaction]  WITH CHECK ADD  CONSTRAINT [FK__Admission__Guard__2665ABE1] FOREIGN KEY([GuardianRegisterId])
REFERENCES [dbo].[GuardianRegister] ([GuardianRegisterId])
GO
ALTER TABLE [dbo].[AdmissionTransaction] CHECK CONSTRAINT [FK__Admission__Guard__2665ABE1]
GO
/****** Object:  ForeignKey [FK__Admission__Mothe__2759D01A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AdmissionTransaction]  WITH CHECK ADD  CONSTRAINT [FK__Admission__Mothe__2759D01A] FOREIGN KEY([MotherRegisterId])
REFERENCES [dbo].[MotherRegister] ([MotherRegisterId])
GO
ALTER TABLE [dbo].[AdmissionTransaction] CHECK CONSTRAINT [FK__Admission__Mothe__2759D01A]
GO
/****** Object:  ForeignKey [FK__Admission__Prima__284DF453]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AdmissionTransaction]  WITH CHECK ADD  CONSTRAINT [FK__Admission__Prima__284DF453] FOREIGN KEY([PrimaryUserRegisterId])
REFERENCES [dbo].[PrimaryUserRegister] ([PrimaryUserRegisterId])
GO
ALTER TABLE [dbo].[AdmissionTransaction] CHECK CONSTRAINT [FK__Admission__Prima__284DF453]
GO
/****** Object:  ForeignKey [FK__Admission__Stude__131DCD43]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[AdmissionTransaction]  WITH CHECK ADD  CONSTRAINT [FK__Admission__Stude__131DCD43] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[AdmissionTransaction] CHECK CONSTRAINT [FK__Admission__Stude__131DCD43]
GO
/****** Object:  ForeignKey [FK__FeeCollec__Emplo__15FA39EE]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeeCollectionCategory]  WITH CHECK ADD  CONSTRAINT [FK__FeeCollec__Emplo__15FA39EE] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[FeeCollectionCategory] CHECK CONSTRAINT [FK__FeeCollec__Emplo__15FA39EE]
GO
/****** Object:  ForeignKey [Fk_FeeCollectionCategory_AcademicYearId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeeCollectionCategory]  WITH CHECK ADD  CONSTRAINT [Fk_FeeCollectionCategory_AcademicYearId] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[FeeCollectionCategory] CHECK CONSTRAINT [Fk_FeeCollectionCategory_AcademicYearId]
GO
/****** Object:  ForeignKey [Fk_FeeCollectionCategory_ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeeCollectionCategory]  WITH CHECK ADD  CONSTRAINT [Fk_FeeCollectionCategory_ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[FeeCollectionCategory] CHECK CONSTRAINT [Fk_FeeCollectionCategory_ClassId]
GO
/****** Object:  ForeignKey [Fk_FeeCollectionCategory_FeeCategoryId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeeCollectionCategory]  WITH CHECK ADD  CONSTRAINT [Fk_FeeCollectionCategory_FeeCategoryId] FOREIGN KEY([FeeCategoryId])
REFERENCES [dbo].[FeeCategory] ([FeeCategoryId])
GO
ALTER TABLE [dbo].[FeeCollectionCategory] CHECK CONSTRAINT [Fk_FeeCollectionCategory_FeeCategoryId]
GO
/****** Object:  ForeignKey [Fk_FeeCollectionCategory_FeeCollectionId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeeCollectionCategory]  WITH CHECK ADD  CONSTRAINT [Fk_FeeCollectionCategory_FeeCollectionId] FOREIGN KEY([FeeCollectionId])
REFERENCES [dbo].[FeeCollection] ([FeeCollectionId])
GO
ALTER TABLE [dbo].[FeeCollectionCategory] CHECK CONSTRAINT [Fk_FeeCollectionCategory_FeeCollectionId]
GO
/****** Object:  ForeignKey [Fk_FeeCollectionCategory_SectionId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeeCollectionCategory]  WITH CHECK ADD  CONSTRAINT [Fk_FeeCollectionCategory_SectionId] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[FeeCollectionCategory] CHECK CONSTRAINT [Fk_FeeCollectionCategory_SectionId]
GO
/****** Object:  ForeignKey [Fk_FeeCollectionCategory_StudentRegId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeeCollectionCategory]  WITH CHECK ADD  CONSTRAINT [Fk_FeeCollectionCategory_StudentRegId] FOREIGN KEY([StudentRegId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[FeeCollectionCategory] CHECK CONSTRAINT [Fk_FeeCollectionCategory_StudentRegId]
GO
/****** Object:  ForeignKey [FK__ExamAtten__Acade__0F824689]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamAttendance]  WITH CHECK ADD  CONSTRAINT [FK__ExamAtten__Acade__0F824689] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[ExamAttendance] CHECK CONSTRAINT [FK__ExamAtten__Acade__0F824689]
GO
/****** Object:  ForeignKey [FK__ExamAtten__ClassId]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamAttendance]  WITH CHECK ADD  CONSTRAINT [FK__ExamAtten__ClassId] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[ExamAttendance] CHECK CONSTRAINT [FK__ExamAtten__ClassId]
GO
/****** Object:  ForeignKey [FK__ExamAtten__ExamI__116A8EFB]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamAttendance]  WITH CHECK ADD  CONSTRAINT [FK__ExamAtten__ExamI__116A8EFB] FOREIGN KEY([ExamId])
REFERENCES [dbo].[Exam] ([ExamId])
GO
ALTER TABLE [dbo].[ExamAttendance] CHECK CONSTRAINT [FK__ExamAtten__ExamI__116A8EFB]
GO
/****** Object:  ForeignKey [FK__ExamAtten__Marke__125EB334]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamAttendance]  WITH CHECK ADD  CONSTRAINT [FK__ExamAtten__Marke__125EB334] FOREIGN KEY([MarkedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[ExamAttendance] CHECK CONSTRAINT [FK__ExamAtten__Marke__125EB334]
GO
/****** Object:  ForeignKey [FK__ExamAtten__RollN__1352D76D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamAttendance]  WITH CHECK ADD  CONSTRAINT [FK__ExamAtten__RollN__1352D76D] FOREIGN KEY([RollNumberId])
REFERENCES [dbo].[StudentRollNumber] ([RollNumberId])
GO
ALTER TABLE [dbo].[ExamAttendance] CHECK CONSTRAINT [FK__ExamAtten__RollN__1352D76D]
GO
/****** Object:  ForeignKey [FK__ExamAtten__Secti__1446FBA6]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamAttendance]  WITH CHECK ADD  CONSTRAINT [FK__ExamAtten__Secti__1446FBA6] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[ExamAttendance] CHECK CONSTRAINT [FK__ExamAtten__Secti__1446FBA6]
GO
/****** Object:  ForeignKey [FK__ExamAtten__Stude__373B3228]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamAttendance]  WITH CHECK ADD  CONSTRAINT [FK__ExamAtten__Stude__373B3228] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[ExamAttendance] CHECK CONSTRAINT [FK__ExamAtten__Stude__373B3228]
GO
/****** Object:  ForeignKey [FK__ExamAtten__Subje__162F4418]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamAttendance]  WITH CHECK ADD  CONSTRAINT [FK__ExamAtten__Subje__162F4418] FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO
ALTER TABLE [dbo].[ExamAttendance] CHECK CONSTRAINT [FK__ExamAtten__Subje__162F4418]
GO
/****** Object:  ForeignKey [FK__ExamAtten__TimeS__17236851]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamAttendance]  WITH CHECK ADD  CONSTRAINT [FK__ExamAtten__TimeS__17236851] FOREIGN KEY([TimeScheduleId])
REFERENCES [dbo].[ExamTimeSchedule] ([Id])
GO
ALTER TABLE [dbo].[ExamAttendance] CHECK CONSTRAINT [FK__ExamAtten__TimeS__17236851]
GO
/****** Object:  ForeignKey [FK__ExamAtten__Updat__18178C8A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[ExamAttendance]  WITH CHECK ADD  CONSTRAINT [FK__ExamAtten__Updat__18178C8A] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[ExamAttendance] CHECK CONSTRAINT [FK__ExamAtten__Updat__18178C8A]
GO
/****** Object:  ForeignKey [FK__EntryBook__BookE__4979DDF4]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EntryBookReturn]  WITH CHECK ADD  CONSTRAINT [FK__EntryBook__BookE__4979DDF4] FOREIGN KEY([BookEntryRegisterId])
REFERENCES [dbo].[BookEntry] ([BookEntryRegisterId])
GO
ALTER TABLE [dbo].[EntryBookReturn] CHECK CONSTRAINT [FK__EntryBook__BookE__4979DDF4]
GO
/****** Object:  ForeignKey [FK__EntryBook__BookR__4A6E022D]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[EntryBookReturn]  WITH CHECK ADD  CONSTRAINT [FK__EntryBook__BookR__4A6E022D] FOREIGN KEY([BookRegisterId])
REFERENCES [dbo].[EntryBookId] ([BookRegisterId])
GO
ALTER TABLE [dbo].[EntryBookReturn] CHECK CONSTRAINT [FK__EntryBook__BookR__4A6E022D]
GO
/****** Object:  ForeignKey [FK__HostelFee__Acade__697C9932]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCollectioncategory]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__Acade__697C9932] FOREIGN KEY([AcademicYearId])
REFERENCES [dbo].[AcademicYear] ([AcademicYearId])
GO
ALTER TABLE [dbo].[HostelFeesCollectioncategory] CHECK CONSTRAINT [FK__HostelFee__Acade__697C9932]
GO
/****** Object:  ForeignKey [FK__HostelFee__Class__6A70BD6B]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCollectioncategory]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__Class__6A70BD6B] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([ClassId])
GO
ALTER TABLE [dbo].[HostelFeesCollectioncategory] CHECK CONSTRAINT [FK__HostelFee__Class__6A70BD6B]
GO
/****** Object:  ForeignKey [FK__HostelFee__Creat__6B64E1A4]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCollectioncategory]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__Creat__6B64E1A4] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[HostelFeesCollectioncategory] CHECK CONSTRAINT [FK__HostelFee__Creat__6B64E1A4]
GO
/****** Object:  ForeignKey [FK__HostelFee__FeeCa__6C5905DD]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCollectioncategory]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__FeeCa__6C5905DD] FOREIGN KEY([FeeCategoryId])
REFERENCES [dbo].[HostelFeesCategory] ([categoryId])
GO
ALTER TABLE [dbo].[HostelFeesCollectioncategory] CHECK CONSTRAINT [FK__HostelFee__FeeCa__6C5905DD]
GO
/****** Object:  ForeignKey [FK__HostelFee__FeeCo__6D4D2A16]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCollectioncategory]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__FeeCo__6D4D2A16] FOREIGN KEY([FeeCollectionId])
REFERENCES [dbo].[HostelFeesCollection] ([FeeCollectionId])
GO
ALTER TABLE [dbo].[HostelFeesCollectioncategory] CHECK CONSTRAINT [FK__HostelFee__FeeCo__6D4D2A16]
GO
/****** Object:  ForeignKey [FK__HostelFee__Secti__6E414E4F]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCollectioncategory]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__Secti__6E414E4F] FOREIGN KEY([SectionId])
REFERENCES [dbo].[Section] ([SectionId])
GO
ALTER TABLE [dbo].[HostelFeesCollectioncategory] CHECK CONSTRAINT [FK__HostelFee__Secti__6E414E4F]
GO
/****** Object:  ForeignKey [FK__HostelFee__Stude__6F357288]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCollectioncategory]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__Stude__6F357288] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[HostelFeesCollectioncategory] CHECK CONSTRAINT [FK__HostelFee__Stude__6F357288]
GO
/****** Object:  ForeignKey [FK__HostelFee__Updat__702996C1]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[HostelFeesCollectioncategory]  WITH CHECK ADD  CONSTRAINT [FK__HostelFee__Updat__702996C1] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[HostelFeesCollectioncategory] CHECK CONSTRAINT [FK__HostelFee__Updat__702996C1]
GO
/****** Object:  ForeignKey [FK__FeesRefun__Colle__1229A90A]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeesRefundToStudent]  WITH CHECK ADD  CONSTRAINT [FK__FeesRefun__Colle__1229A90A] FOREIGN KEY([CollectionId])
REFERENCES [dbo].[FeeCollectionCategory] ([CollectionId])
GO
ALTER TABLE [dbo].[FeesRefundToStudent] CHECK CONSTRAINT [FK__FeesRefun__Colle__1229A90A]
GO
/****** Object:  ForeignKey [FK__FeesRefun__Emplo__1411F17C]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeesRefundToStudent]  WITH CHECK ADD  CONSTRAINT [FK__FeesRefun__Emplo__1411F17C] FOREIGN KEY([EmployeeRegisterId])
REFERENCES [dbo].[TechEmployee] ([EmployeeRegisterId])
GO
ALTER TABLE [dbo].[FeesRefundToStudent] CHECK CONSTRAINT [FK__FeesRefun__Emplo__1411F17C]
GO
/****** Object:  ForeignKey [FK__FeesRefun__FeeId__36670980]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeesRefundToStudent]  WITH CHECK ADD  CONSTRAINT [FK__FeesRefun__FeeId__36670980] FOREIGN KEY([FeeId])
REFERENCES [dbo].[Fee] ([FeeId])
GO
ALTER TABLE [dbo].[FeesRefundToStudent] CHECK CONSTRAINT [FK__FeesRefun__FeeId__36670980]
GO
/****** Object:  ForeignKey [FK__FeesRefun__Stude__131DCD43]    Script Date: 01/05/2016 10:49:15 ******/
ALTER TABLE [dbo].[FeesRefundToStudent]  WITH CHECK ADD  CONSTRAINT [FK__FeesRefun__Stude__131DCD43] FOREIGN KEY([StudentRegisterId])
REFERENCES [dbo].[Student] ([StudentRegisterId])
GO
ALTER TABLE [dbo].[FeesRefundToStudent] CHECK CONSTRAINT [FK__FeesRefun__Stude__131DCD43]
GO


