--1.TechEmployee
--2.class
--3.EmployeeType
--4.EmployeeDesignation
--5.Feature
--6.Menu
--7.Months
--8.tblBloodGroup
--9.tblCommunity
--10.FuelType
--11.VechileType
--12.tblCountry
--13.Roles
--14.MapRoleFeature
--15.MapRoleUser



---1.Tech Employee--------------------------->


INSERT INTO TechEmployee VALUES ('Admin','Jaikumar','Jai','40BD001563085FC35165329EA1FF5C5ECBDBBEEF','Male','2014-10-01 00:00:00.000','2','cjegadeesh@acestranetworks.com','2','Hindu','Indian','9003206099','Vineeth','9940325892','Temple View Appartment','Mylapore','Tamilnadu','100000','B.E','1 year','1900-01-01 00:00:00.000','2015-08-28 00:00:00.000','1900-01-01 00:00:00.000','True','AKIlash523.png','Algeria','Married','NULL','NULL','NULL','5656.0000','NULL','NULL','k','NULL','638052','NULL','Temple View Appartment','NULL','Mylapore','Tamilnadu','Algeria','638052','juhgju','NULL','NULL','3','6');
--===============================================================

---2.Class--------------------------->
INSERT INTO [Class] VALUES('PreKG','NULL','NULL');
INSERT INTO [Class] VALUES('LKG','NULL','NULL');
INSERT INTO [Class] VALUES('UKG','NULL','NULL');
INSERT INTO [Class] VALUES('I','NULL','NULL');
INSERT INTO [Class] VALUES('II','NULL','NULL');
INSERT INTO [Class] VALUES('III','NULL','NULL');
INSERT INTO [Class] VALUES('IV','NULL','NULL');
INSERT INTO [Class] VALUES('V','NULL','NULL');
INSERT INTO [Class] VALUES('VI','NULL','NULL');
INSERT INTO [Class] VALUES('VII','NULL','NULL');
INSERT INTO [Class] VALUES('VIII','NULL','NULL');
INSERT INTO [Class] VALUES('IX','NULL','NULL');
INSERT INTO [Class] VALUES('X','NULL','NULL');
INSERT INTO [Class] VALUES('XI','NULL','NULL');
INSERT INTO [Class] VALUES('XII','NULL','NULL');
--=================================================

--3.Employee Type ---------->

insert into EmployeeType values ('Teaching Staff')
insert into EmployeeType values ('Non_Teaching Staff')
insert into EmployeeType values ('Admin Staff')
insert into EmployeeType values ('Helpers&Maintenance Staff')
insert into EmployeeType values ('Driver&Cleaner Staff')

--======================================================

--4.Employee Designation ---------->

insert into EmployeeDesignation values (1,'Faculty')
insert into EmployeeDesignation values (2,'Librarian')
insert into EmployeeDesignation values (2,'Hostel Warden')
insert into EmployeeDesignation values (2,'Sports Staff')
insert into EmployeeDesignation values (2,'laboratory staff')
insert into EmployeeDesignation values (3,'Principal')
insert into EmployeeDesignation values (3,'Accountant')
insert into EmployeeDesignation values (3,'Casier')
insert into EmployeeDesignation values (3,'Office Staff')
insert into EmployeeDesignation values (4,'Incharge')
insert into EmployeeDesignation values (4,'Staff')
insert into EmployeeDesignation values (5,'Dirver')
insert into EmployeeDesignation values (5,'Cleaner')
insert into EmployeeDesignation values (5,'Incharge')

--===========================================================

--5. Feature table insert values---------->
INSERT INTO Feature VALUES('School Configuration','/SchoolConfiguration/Settings','True',null,'SchoolConfiguration','Configuration','1','1') --8 ----1
INSERT INTO Feature VALUES('User Configuration','/UserConfiguration/CreateRole','True',null,'UserConfiguration','Configuration','2','1') --7 ----2
INSERT INTO Feature VALUES('Academics Configuration','/AcademicsConfiguration/AcademicYear','True',null,'AcademicsConfiguration','Configuration','3','1') --1 ----3
INSERT INTO Feature VALUES('Employee Configuration','/EmployeeConfiguration/EmployeeEnrollment','True',null,'EmployeeConfiguration','Configuration','4','1') --2 ----4
INSERT INTO Feature VALUES('Fee Configuration','/FeeConfiguration/FeeCategory','True',null,'FeeConfiguration','Configuration','5','1') --6 ----5
INSERT INTO Feature VALUES('Infra Management','/InfraManagement/ClassRooms','True',null,'InfraManagement','Configuration','6','1') --36 ----6
INSERT INTO Feature VALUES('Exam Configuration','/ExamConfiguration/ExamConfig','True',null,'ExamConfiguration','Configuration','7','1') --3 ----7
INSERT INTO Feature VALUES('Mark Configuration','/MarkConfiguration/MarkConfig','True',null,'MarkConfiguration','Configuration','8','1') --4 ----8
INSERT INTO Feature VALUES('Grade Configuration','/GradeConfiguration/GradeType','True',null,'GradeConfiguration','Configuration','9','1') --5 ----9
INSERT INTO Feature VALUES('Pre Admission Management','/Admission/Configuration','True',null,'Admission','Enrollment','10','2') --35 ----10
INSERT INTO Feature VALUES('Admission and Separation','/StudentEnrollment/SingleFormEnrollment','True',null,'StudentEnrollment','Enrollment','11','2') --21 ----11
INSERT INTO Feature VALUES('Class Management','/ClassManagement/AssignSubjectWithFaculty','True',null,'ClassManagement','Academics Administration','12','3') --11 ----12
INSERT INTO Feature VALUES('Timetable Management','/TimeTableManagement/ClassTimetable','True',null,'TimeTableManagement','Academics Administration','13','3') --12 ----13
INSERT INTO Feature VALUES('RollNumber Management','/RollNumberManagement/FormatRollNumber','True',null,'RollNumberManagement','Academics Administration','14','3') --9 ----14
INSERT INTO Feature VALUES('Attendance Management','/AttendanceManagement/FacultyAttendance','True',null,'AttendanceManagement','Academics Administration','15','3') --13 ----15
INSERT INTO Feature VALUES('Exam Management','/ExamManagement/ExamClassRoom','True',null,'ExamManagement','Academics Administration','16','3') --10 ----16
INSERT INTO Feature VALUES('Achievement Management','/Achievement/StudentAchievement','True',null,'Achievement','Academics Administration','17','3') --34 ----17
INSERT INTO Feature VALUES('Student Management','/StudentManagement/StudentList','True',null,'StudentManagement','School Administration','18','4') --14 ----18
INSERT INTO Feature VALUES('Fee Collection','/FeeCollection/CollectFee','True',null,'FeeCollection','School Administration','19','4') --17 ----19
INSERT INTO Feature VALUES('Fee Management','/FeeManagement/FeeDetails','True',null,'FeeManagement','School Administration','20','4') --16 ----20
INSERT INTO Feature VALUES('Human Resource','/EmployeeManagement/LeaveRequest','True',null,'EmployeeManagement','School Administration','21','4') --22 ----21
INSERT INTO Feature VALUES('School Management','/SchoolManagement/Facilities','True',null,'SchoolManagement','School Administration','22','4') --15 ----22
-- INSERT INTO Feature VALUES('Hostel Management','/HostelManagement/Warden','True',null,'HostelManagement','School Administration','23','4') --18    Hostel management
--INSERT INTO Feature VALUES('Transport Management','/TransportManagement/TransportDetails','True',null,'TransportManagement','School Administration','24','4') --19  Transport management
INSERT INTO Feature VALUES('Achievement','/Achievements/StudentAchievement','True',null,'Achievements','School Administration','23','4') --20  ----23
INSERT INTO Feature VALUES('Communication','/Communication/Message/Index','True',null,'Message','School Administration','24','4') --38 ----24
INSERT INTO Feature VALUES('Financial Accounting','/FinancialAccounting/EmployeeSalary/EmployeeSalaryConfiguration','True',null,'EmployeeSalary','School Administration','25','4') --54 ----25
INSERT INTO Feature VALUES('Medical Checkup','/Medical/MedicalDetails/MedicalForm','True',null,'MedicalDetails','School Administration','26','4') --70 ---26
INSERT INTO Feature VALUES('Exam Attendance','/ExamAttendance/Attendance','True',null,'ExamAttendance','Faculty Management','27','5') --37 ----27
INSERT INTO Feature VALUES('Faculty Info','/FacultyInfo/ClassAssigned','True',null,'FacultyInfo','Faculty Management','28','5') --23  ----28
INSERT INTO Feature VALUES('Class Incharge','/ClassIncharge/StudentList','True',null,'ClassIncharge','Faculty Management','29','5') --24 ----29
INSERT INTO Feature VALUES('Modules Info','/ModulesInfo/StudentList','True',null,'ModulesInfo','Faculty Management','30','5') --25 ----30
INSERT INTO Feature VALUES('Mark Info','/MarksInfo/Mark','True',null,'MarksInfo','Faculty Management','31','5') --27 ----31
INSERT INTO Feature VALUES('Grade Info','/GradeInfo/Grade','True',null,'GradeInfo','Faculty Management','32','5') --26 ----32
INSERT INTO Feature VALUES('Faculty Leave Request','/FacultyLeaveRequest/LeaveRequest','True',null,'FacultyLeaveRequest','Faculty Management','33','5') --28 ----33
INSERT INTO Feature VALUES('Library Management','/LibraryManagement/Library/BookRegister','True',null,'Library','Library','34','6') --39 ----34
INSERT INTO Feature VALUES('Hostel Infrastructure','/HostelManagement/HostelInfrastructure/AddBlockRoom','True',null,'HostelInfrastructure','Hostel','35','7') --63 ----35
INSERT INTO Feature VALUES('Hostel Fee Config','/HostelManagement/HostelFeesCategory/AddHostelFeescategory','True',null,'HostelFeesCategory','Hostel','36','7') --66 ----36
INSERT INTO Feature VALUES('Hostel Fee Collection','/HostelManagement/HostelFeesCollection/CollectHostelFee_Hostel','True',null,'HostelFeesCollection','Hostel','37','7') --67 ----37
INSERT INTO Feature VALUES('Transport Fee Config','/Transport/FeeConfig/Index','True',null,'FeeConfig','Transport','38','8') --56 ----38
INSERT INTO Feature VALUES('Transport Vehicle Config','/Transport/VehicleConfiguration/Index','True',null,'VehicleConfiguration','Transport','39','8') --56 ----39
INSERT INTO Feature VALUES('Transport Mapping','/Transport/Mapping/Index','True',null,'Mapping','Transport','40','8') --57 ----40
INSERT INTO Feature VALUES('Transport Fee Collection','/Transport/TransportFeesCollection/Index','True',null,'TransportFeesCollection','Transport','41','8') --59 ----41
INSERT INTO Feature VALUES('Transport Allotment','/Transport/TransportAllotment/Index','True',null,'TransportAllotment','Transport','42','8') --60 ----42
INSERT INTO Feature VALUES('Student Report','/StudentReport/Studentprofile','True',null,'StudentReport','Report Centre','43','9') --29 ----43
INSERT INTO Feature VALUES('Employee Report','/EmployeeReport/EmployeeProfile','True',null,'EmployeeReport','Report Centre','44','9') --30 ----44
INSERT INTO Feature VALUES('Class Report','/ClassReport/ClassList','True',null,'ClassReport','Report Centre','45','9') --31 ----45
INSERT INTO Feature VALUES('Academic Report','/AcademicReport/FunctionTimeTable','True',null,'AcademicReport','Report Centre','46','9') --32 ----46
INSERT INTO Feature VALUES('Exam Report','/ExamReport/ExamTimeTable','True',null,'ExamReport','Report Centre','47','9') --33 ----47












--==========================================================================================================================================================================================
---6.Menu--------------------------->
INSERT INTO Menu     VALUES (1,'Settings','/SchoolConfiguration/Settings','True')--9
INSERT INTO Menu     VALUES (1,'Time Settings','/SchoolConfiguration/TimeSettings','True')--118
INSERT INTO Menu     VALUES (1,'Class Settings','/SchoolConfiguration/ClassSettings','True')--119
INSERT INTO Menu     VALUES (2,'Create Role','/UserConfiguration/CreateRole','True')--10
INSERT INTO Menu     VALUES (2,'Map Role','/UserConfiguration/MapRole','True')--12
INSERT INTO Menu     VALUES (2,'Map User','/UserConfiguration/MapUser','True')--13
INSERT INTO Menu     VALUES (2,'User Log','/UserConfiguration/UserLog','True')--11
INSERT INTO Menu     VALUES (3,'Academic Year','/AcademicsConfiguration/AcademicYear','True')--1
INSERT INTO Menu     VALUES (3,'Term','/AcademicsConfiguration/Term','True')--2
INSERT INTO Menu     VALUES (3,'Class','/AcademicsConfiguration/Class','True')--3
INSERT INTO Menu     VALUES (3,'Class Strength','/AcademicsConfiguration/ClassStrength','True')--130
INSERT INTO Menu     VALUES (3,'Subjects','/AcademicsConfiguration/Subject','True')--4
INSERT INTO Menu     VALUES (3,'Holiday','/AcademicsConfiguration/Calender','True')--5
INSERT INTO Menu     VALUES (3,'Announcements','/AcademicsConfiguration/Announcement','True')--6
INSERT INTO Menu     VALUES (3,'Facility','/AcademicsConfiguration/Facility','True')--7
INSERT INTO Menu     VALUES (3,'Extra Curricular','/AcademicsConfiguration/ExtraAcitivity','True')--8
INSERT INTO Menu     VALUES (4,'Employee Enrollment','/EmployeeConfiguration/EmployeeEnrollment','True')--14
INSERT INTO Menu     VALUES (4,'Tech Employee Report','/EmployeeConfiguration/TechEmployeeList','True')--15
INSERT INTO Menu     VALUES (4,'Non-Tech Employee Report','/EmployeeConfiguration/Non_TechEmployeeList','True')--16
INSERT INTO Menu     VALUES (5,'Fee Category','/FeeConfiguration/FeeCategory','True')--21
INSERT INTO Menu     VALUES (5,'Fee Config','/FeeConfiguration/FeeConfig','True')--22
INSERT INTO Menu     VALUES (5,'Fee Refund Report','/FeeConfiguration/FeeRefundConfigReport','True')--140
INSERT INTO Menu     VALUES (6,'Class Rooms','/InfraManagement/ClassRooms','True')--128
INSERT INTO Menu     VALUES (6,'Allocate Class Room','/InfraManagement/AllocateClassRooms','True')--129
INSERT INTO Menu     VALUES (7,'Exam Config','/ExamConfiguration/ExamConfig','True')--17
INSERT INTO Menu     VALUES (8,'Mark Settings','/MarkConfiguration/MarkConfig','True')--20
INSERT INTO Menu     VALUES (9,'Grade Type','/GradeConfiguration/GradeType','True')--18
INSERT INTO Menu     VALUES (9,'Grade Planner','/GradeConfiguration/GradePlanner','True')--19
INSERT INTO Menu     VALUES (10,'Admission Announcement','/Admission/Configuration','True')--120
INSERT INTO Menu     VALUES (10,'Student Selection','/Admission/SelectionProcess','True')--121
INSERT INTO Menu     VALUES (10,'Admission Fees Collection','/Admission/AdmissionFeesCollection','True')--122
INSERT INTO Menu     VALUES (10,'Admission Acceptance','/Admission/Confirmation','True')--123
INSERT INTO Menu     VALUES (11,'Single Form Enrollment','/StudentEnrollment/SingleFormEnrollment','False')--58
INSERT INTO Menu     VALUES (11,'Step Form Enrollment','/StudentEnrollment/StepFormEnrollment','True')--59
INSERT INTO Menu     VALUES (11,'Assign Class','/StudentEnrollment/AssignClass','True')--60
INSERT INTO Menu     VALUES (11,'Change Section','/StudentEnrollment/ChangeSection','True')--61
INSERT INTO Menu     VALUES (11,'Student Profile','/StudentEnrollment/EditStudentBioData','True')--124
INSERT INTO Menu     VALUES (11,'Student Separation','/StudentEnrollment/SeparateStudents','True')--125
INSERT INTO Menu     VALUES (11,'Old Students','/StudentEnrollment/OldStudents','True')--126
INSERT INTO Menu     VALUES (11,'Applied Separation','/StudentEnrollment/AppliedSeparationForms','False')--131 --Separation
INSERT INTO Menu     VALUES (11,'Student bio-data','/StudentEnrollment/EditStudentBioData','False')--135 --Bio-data
INSERT INTO Menu     VALUES (12,'Subject With Faculty','/ClassManagement/AssignSubjectWithFaculty','True')--41
INSERT INTO Menu     VALUES (12,'Subject Faculty','/ClassManagement/AssignFacultyToSubject','False')--42
INSERT INTO Menu     VALUES (12,'Class Subject','/ClassManagement/AssignSubjectToSection','False')--43
INSERT INTO Menu     VALUES (13,'Day Order','/TimeTableManagement/DayOrder','False')--47
INSERT INTO Menu     VALUES (13,'Class Timetable','/TimeTableManagement/ClassTimetable','True')--48
INSERT INTO Menu     VALUES (13,'Faculty Timetable','/TimeTableManagement/StaffTimetable','True')--49
INSERT INTO Menu     VALUES (13,'Exam TimeSchedule','/TimeTableManagement/ExamTimeSchedule','True')--50
INSERT INTO Menu     VALUES (13,'Create ExamTimetable','/TimeTableManagement/CreateExamTimetable','True')--51
INSERT INTO Menu     VALUES (14,'Format RollNumber','/RollNumberManagement/FormatRollNumber','True')--23
INSERT INTO Menu     VALUES (15,'Faculty Attendance','/AttendanceManagement/FacultyAttendance','True')--52
INSERT INTO Menu     VALUES (15,'Employee Attendance','/AttendanceManagement/EmployeeAttendance','True')--53
INSERT INTO Menu     VALUES (15,'Attendance Report','/AttendanceManagement/FacultyAttendanceReport','True')--54
INSERT INTO Menu     VALUES (16,'Exam Attendance','/ExamManagement/ExamAttendance','False')--44
INSERT INTO Menu     VALUES (16,'Exam Room','/ExamManagement/ExamClassRoom','True')--45
INSERT INTO Menu     VALUES (16,'Exam Remarks','/ExamManagement/ExamRemarks','False')--46
INSERT INTO Menu     VALUES (16,'Assign Invigilator','/ExamManagement/AssignInvigilator','True')--133
INSERT INTO Menu     VALUES (17,'Student Achievement','/Achievement/StudentAchievement','True')--112
INSERT INTO Menu     VALUES (17,'Faculty Achievement','/Achievement/FacultyAchievement','True')--113
INSERT INTO Menu     VALUES (17,'School Achievement','/Achievement/SchoolAchievement','True')--114
INSERT INTO Menu     VALUES (18,'Student List','/StudentManagement/StudentList','True')--24
INSERT INTO Menu     VALUES (18,'Student Profile','/StudentManagement/StudentProfiles','True')--25
INSERT INTO Menu     VALUES (18,'Performance Report','/StudentManagement/StudentPerformance','True')--26
INSERT INTO Menu     VALUES (18,'RollNumber Management','/StudentManagement/StudentRollNumber','True')--27
INSERT INTO Menu     VALUES (18,'Academic Session Result','/StudentManagement/AcademicResult','True')--28
INSERT INTO Menu     VALUES (19,'Fee Collection','/FeeCollection/CollectFee','True')--37
INSERT INTO Menu     VALUES (19,'Admission Fee Refund','/FeeCollection/AdmissionFeeRefund','True')--141
INSERT INTO Menu     VALUES (19,'Fee Refund Report','/FeeCollection/FeeRefundReports','True')--142
INSERT INTO Menu     VALUES (20,'Fee Details','/FeeManagement/FeeDetails','True')--34
INSERT INTO Menu     VALUES (20,'Fee Paid List','/FeeManagement/FeePaidList','True')--35
INSERT INTO Menu     VALUES (20,'Student Due List','/FeeManagement/FeeDueList','True')--36
INSERT INTO Menu     VALUES (21,'Leave Request','/EmployeeManagement/LeaveRequest','True')--62
INSERT INTO Menu     VALUES (21,'Faculty Feedback','/EmployeeManagement/FacultyFeedback','True')--63
INSERT INTO Menu     VALUES (21,'Substitude Faculty','/EmployeeManagement/AssignSubstitudeFaculty','True')--127
INSERT INTO Menu     VALUES (22,'Facility','/SchoolManagement/Facilities','True')--29
INSERT INTO Menu     VALUES (22,'Activity','/SchoolManagement/ExtraActivities','True')--30
INSERT INTO Menu     VALUES (22,'News & Trends','/SchoolManagement/NewsTrends','False')--31
INSERT INTO Menu     VALUES (22,'Announcements','/SchoolManagement/Announcement','false')--32 --Announcement
INSERT INTO Menu     VALUES (22,'Circular','/SchoolManagement/Circular','false')--33 --circular
INSERT INTO Menu     VALUES (23,'Student Achievement','/Achievements/StudentAchievement','True')--55
INSERT INTO Menu     VALUES (23,'Faculty Achievement','/Achievements/FacultyAchievement','True')--56
INSERT INTO Menu     VALUES (23,'School Achievement','/Achievements/SchoolAchievement','True')--57
INSERT INTO Menu     VALUES (24,'Email / SMS','/Communication/Message/Index','True')--143
INSERT INTO Menu     VALUES (24,'Email / SMS','/Communication/Index','False')--138
INSERT INTO Menu     VALUES (25,'Employee Salary Configuration','/FinancialAccounting/EmployeeSalary/EmployeeSalaryConfiguration','True')--149
INSERT INTO Menu     VALUES (25,'Employee Monthly Salary','/FinancialAccounting/EmployeeSalary/EmployeeMonthlySalary','True')--150
INSERT INTO Menu     VALUES (26,'Medical Checkup','/Medical/MedicalDetails/MedicalForm','True')--166
INSERT INTO Menu     VALUES (27,'Exam Attendance','/ExamAttendance/Attendance','True')--136
INSERT INTO Menu     VALUES (28,'Class Assigned','/FacultyInfo/ClassAssigned','True')--64
INSERT INTO Menu     VALUES (28,'Modules Assigned','/FacultyInfo/ModulesAssigned','True')--65
INSERT INTO Menu     VALUES (28,'Class Timetable','/FacultyInfo/ClassTimetable','True')--66
INSERT INTO Menu     VALUES (28,'Student Remarks','/FacultyInfo/StudentRemark','True')--134
INSERT INTO Menu     VALUES (29,'Student List','/ClassIncharge/StudentList','True')--67
INSERT INTO Menu     VALUES (29,'Class Attendance','/ClassIncharge/ClassAttendance','True')--68
INSERT INTO Menu     VALUES (29,'Attendance Report','/ClassIncharge/AttendanceReport','True')--69
INSERT INTO Menu     VALUES (29,'Leave Request','/ClassIncharge/LeaveRequest','True')--70
INSERT INTO Menu     VALUES (29,'Generate ProgressCard','/ClassIncharge/CreateProgressCard','False')--71
INSERT INTO Menu     VALUES (29,'Student Remarks','/ClassIncharge/StudentRemarks','False')--72
INSERT INTO Menu     VALUES (29,'Generate Progress Card','/ClassIncharge/GradeProgressCard','True')--73
INSERT INTO Menu     VALUES (29,'Graph','/ClassIncharge/Graph','True')--132
INSERT INTO Menu     VALUES (30,'Student List','/ModulesInfo/StudentList','True')--74
INSERT INTO Menu     VALUES (30,'Subject Notes','/ModulesInfo/SubjectNotes','True')--75
INSERT INTO Menu     VALUES (30,'Homework','/ModulesInfo/Homework','True')--76
INSERT INTO Menu     VALUES (30,'Assignment','/ModulesInfo/Assignment','True')--77
INSERT INTO Menu     VALUES (30,'Student Remarks','/ModulesInfo/StudentRemarks','False')--78
INSERT INTO Menu     VALUES (30,'Queries','/ModulesInfo/queries','True')--137
INSERT INTO Menu     VALUES (30,'E-Learning','/ModulesInfo/ELearning','True')--139
INSERT INTO Menu     VALUES (31,'Mark','/MarksInfo/Mark','True')--81
INSERT INTO Menu     VALUES (31,'Progress Card','/MarksInfo/ProgressCard','True')--82
INSERT INTO Menu     VALUES (32,'Grade','/GradeInfo/Grade','True')--79
INSERT INTO Menu     VALUES (32,'Progress Card','/GradeInfo/ProgressCard','True')--80
INSERT INTO Menu     VALUES (33,'Faculty Leave Request','/FacultyLeaveRequest/LeaveRequest','True')--83
INSERT INTO Menu     VALUES (34,'New Book Entry','/LibraryManagement/Library/BookRegister','True')--144
INSERT INTO Menu     VALUES (34,'Book Taken/Return','/LibraryManagement/Library/BookTaken','True')--145
INSERT INTO Menu     VALUES (34,'SeachBook Reports','/LibraryManagement/Library/BookSearchReport','True')--146
INSERT INTO Menu     VALUES (34,'Library Fine Collection','/LibraryManagement/Library/FineCollection','True')--148
INSERT INTO Menu     VALUES (35,'Block Config','/HostelManagement/HostelInfrastructure/AddBlockRoom','True')--161
INSERT INTO Menu     VALUES (35,'Student Allotment','/HostelManagement/HostelInfrastructure/StudentRoomAllotment','True')--164
INSERT INTO Menu     VALUES (35,'Faculty Allotment','/HostelManagement/HostelInfrastructure/StaffRoomAllotment','True')--165
INSERT INTO Menu     VALUES (36,'Fee Config','/HostelManagement/HostelFeesCategory/AddHostelFeescategory','True')--162
INSERT INTO Menu     VALUES (37,'Fee Collection','/HostelManagement/HostelFeesCollection/CollectHostelFee_Hostel','True')--163
INSERT INTO Menu     VALUES (38,'Fee Config','/Transport/FeeConfig/Index','True')--151
INSERT INTO Menu     VALUES (39,'Vehicle Config','/Transport/VehicleConfiguration/Index','True')--152
INSERT INTO Menu     VALUES (40,'Mapping','/Transport/Mapping/Index','True')--153
INSERT INTO Menu     VALUES (41,'Fee Collection','/Transport/TransportFeesCollection/Index','True')--159
INSERT INTO Menu     VALUES (42,'Transport Allotment','/Transport/TransportAllotment/Index','True')--160
INSERT INTO Menu     VALUES (43,'Student Profile','/StudentReport/Studentprofile','True')--84
INSERT INTO Menu     VALUES (43,'Parent Profile','/StudentReport/Parentprofile','True')--85
INSERT INTO Menu     VALUES (43,'Progress Card','/StudentReport/StudentProgressCard','False')--86
INSERT INTO Menu     VALUES (43,'Student FeeDetails','/StudentReport/StudentFeeDetails','True')--87
INSERT INTO Menu     VALUES (43,'Student Attendance','/StudentReport/IndividualStudentAttendance','True')--88
INSERT INTO Menu     VALUES (43,'Progress Card','/StudentReport/StudentGradeProgressCard','True')--89
INSERT INTO Menu     VALUES (43,'Student Remarks','/StudentReport/StudentRemark','True')--115
INSERT INTO Menu     VALUES (44,'Employee Profile','/EmployeeReport/EmployeeProfile','True')--90
INSERT INTO Menu     VALUES (44,'Teacher TimeTable','/EmployeeReport/TeacherTimeTable','True')--91
INSERT INTO Menu     VALUES (44,'Employee Attendance','/EmployeeReport/EmployeeAttendance','True')--92
INSERT INTO Menu     VALUES (44,'Employee LeaveDetails','/EmployeeReport/EmployeeLeaves','True')--93
INSERT INTO Menu     VALUES (44,'Faculty Feedback','/EmployeeReport/FacultyFeedback','True')--116
INSERT INTO Menu     VALUES (45,'Class List','/ClassReport/ClassList','True')--94
INSERT INTO Menu     VALUES (45,'Class Incharge','/ClassReport/ClassIncharge','True')--95
INSERT INTO Menu     VALUES (45,'Class Faculty','/ClassReport/ClassFaculty','True')--96
INSERT INTO Menu     VALUES (45,'Class Subject','/ClassReport/ClassSubject','True')--97
INSERT INTO Menu     VALUES (45,'Student Name List','/ClassReport/StudentNameList','True')--98
INSERT INTO Menu     VALUES (45,'Class Rank List','/ClassReport/ClassRankList','True')--99
INSERT INTO Menu     VALUES (45,'Class Attendance','/ClassReport/StudentAttendance','True')--100
INSERT INTO Menu     VALUES (45,'Class Timetable','/ClassReport/ClassTimeTable','True')--101
INSERT INTO Menu     VALUES (45,'Class Mark List','/ClassReport/MarkList','True')--102
INSERT INTO Menu     VALUES (46,'Function Timetable','/AcademicReport/FunctionTimeTable','True')--103
INSERT INTO Menu     VALUES (46,'Fee Details','/AcademicReport/Fee','True')--104
INSERT INTO Menu     VALUES (46,'Hostal Details','/AcademicReport/HostalStudent','True')--105
INSERT INTO Menu     VALUES (46,'Transport Details','/AcademicReport/TransportStudent','True')--106
INSERT INTO Menu     VALUES (46,'Extra-Circular Incharge','/AcademicReport/ExtraCurricular','True')--107
INSERT INTO Menu     VALUES (46,'Faclities Report','/AcademicReport/FacilitiesReport','True')--108
INSERT INTO Menu     VALUES (46,'Circular Details','/AcademicReport/Circular','False')--109
INSERT INTO Menu     VALUES (47,'Exam Timetable','/ExamReport/ExamTimeTable','True')--110
INSERT INTO Menu     VALUES (47,'Exam Attendance','/ExamReport/ExamAttendance','True')--111
INSERT INTO Menu     VALUES (47,'Exam Result','/ExamReport/ExamResult','True')--117






--==========================================================================================================================================================================================




---7.Months--------------------------->

INSERT INTO Months   VALUES('January');
INSERT INTO Months   VALUES('February');
INSERT INTO Months   VALUES('March');
INSERT INTO Months   VALUES('April');
INSERT INTO Months   VALUES('May');
INSERT INTO Months   VALUES('June');
INSERT INTO Months   VALUES('July');
INSERT INTO Months   VALUES('August');
INSERT INTO Months   VALUES('September');
INSERT INTO Months   VALUES('October');
INSERT INTO Months   VALUES('November');
INSERT INTO Months   VALUES('December');

--=========================================

---8.Blood Group--------------------------->
INSERT INTO tblBloodGroup  VALUES  ('O-(Negative)');
INSERT INTO tblBloodGroup  VALUES  ('O+(Positive)');
INSERT INTO tblBloodGroup  VALUES  ('A-(Negative)');
INSERT INTO tblBloodGroup  VALUES  ('A+(Positive)');
INSERT INTO tblBloodGroup  VALUES  ('B-(Negative)');
INSERT INTO tblBloodGroup  VALUES  ('B+(Positive)');
INSERT INTO tblBloodGroup  VALUES  ('AB-(Negative)');
INSERT INTO tblBloodGroup  VALUES  ('AB+(Positive)');
--====================================================

---9.Community--------------------------->
INSERT INTO tblCommunity  VALUES ('BC');
INSERT INTO tblCommunity  VALUES ('MBC');
INSERT INTO tblCommunity  VALUES ('SC/ST');
 --===================================================
 
 


---10.FuelType--------------------------->


insert into FuelType values('Petrol',1)
insert into FuelType values('Diesel',1)

--==========================================

---11.VechileType--------------------------->
insert into VehicleType values('Van',1)
insert into VehicleType values('Bus',1)

--=============================================


---12.Country--------------------------->


--create table tblCountry(CountryId int identity primary key, CountryName nvarchar(255), ISO3166_2LetterCode nvarchar(255))


insert into tblCountry values('Afghanistan','AF')
insert into tblCountry values('Albania','AL')
insert into tblCountry values('Algeria','DZ')
insert into tblCountry values('Andorra','AD')
insert into tblCountry values('Angola','AO')
insert into tblCountry values('Antigua and Barbuda','AG')
insert into tblCountry values('Argentina','AR')
insert into tblCountry values('Armenia','AM')
insert into tblCountry values('Australia','AU')
insert into tblCountry values('Austria','AT')
insert into tblCountry values('Azerbaijan','AZ')
insert into tblCountry values('Bahamas, The','BS')
insert into tblCountry values('Bahrain','BH')
insert into tblCountry values('Bangladesh','BD')
insert into tblCountry values('Barbados','BB')
insert into tblCountry values('Belarus','BY')
insert into tblCountry values('Belgium','BE')
insert into tblCountry values('Belize','BZ')
insert into tblCountry values('Benin','BJ')
insert into tblCountry values('Bhutan','BT')
insert into tblCountry values('Bolivia','BO')
insert into tblCountry values('Bosnia and Herzegovina','BA')
insert into tblCountry values('Botswana','BW')
insert into tblCountry values('Brazil','BR')
insert into tblCountry values('Brunei','BN')
insert into tblCountry values('Bulgaria','BG')
insert into tblCountry values('Burkina Faso','BF')
insert into tblCountry values('Burundi','BI')
insert into tblCountry values('Cambodia','KH')
insert into tblCountry values('Cameroon','CM')
insert into tblCountry values('Canada','CA')
insert into tblCountry values('Cape Verde','CV')
insert into tblCountry values('Central African Republic','CF')
insert into tblCountry values('Chad','TD')
insert into tblCountry values('Chile','CL')
insert into tblCountry values('China, Peoples Republic of','CN')
insert into tblCountry values('Colombia','CO')
insert into tblCountry values('Comoros','KM')
insert into tblCountry values('Congo, (Congo � Kinshasa)','CD')
insert into tblCountry values('Congo, (Congo � Brazzaville)','CG')
insert into tblCountry values('Costa Rica','CR')
insert into tblCountry values('Cote dIvoire (Ivory Coast)','CI')
insert into tblCountry values('Croatia','HR')
insert into tblCountry values('Cuba','CU')
insert into tblCountry values('Cyprus','CY')
insert into tblCountry values('Czech Republic','CZ')
insert into tblCountry values('Denmark','DK')
insert into tblCountry values('Djibouti','DJ')
insert into tblCountry values('Dominica','DM')
insert into tblCountry values('Dominican Republic','DO')
insert into tblCountry values('Ecuador','EC')
insert into tblCountry values('Egypt','EG')
insert into tblCountry values('El Salvador','SV')
insert into tblCountry values('Equatorial Guinea','GQ')
insert into tblCountry values('Eritrea','ER')
insert into tblCountry values('Estonia','EE')
insert into tblCountry values('Ethiopia','ET')
insert into tblCountry values('Fiji','FJ')
insert into tblCountry values('Finland','FI')
insert into tblCountry values('France','FR')
insert into tblCountry values('Gabon','GA')
insert into tblCountry values('Gambia, The','GM
')
insert into tblCountry values('Georgia','GE
')
insert into tblCountry values('Germany','DE
')
insert into tblCountry values('Ghana','GH
')
insert into tblCountry values('Greece','GR
')
insert into tblCountry values('Grenada','GD
')
insert into tblCountry values('Guatemala','GT
')
insert into tblCountry values('Guinea','GN
')
insert into tblCountry values('Guinea-Bissau','GW
')
insert into tblCountry values('Guyana','GY
')
insert into tblCountry values('Haiti','HT
')
insert into tblCountry values('Honduras','HN
')
insert into tblCountry values('Hungary','HU
')
insert into tblCountry values('Iceland','IS
')
insert into tblCountry values('India','IN
')
insert into tblCountry values('Indonesia','ID
')
insert into tblCountry values('Iran','IR
')
insert into tblCountry values('Iraq','IQ
')
insert into tblCountry values('Ireland','IE
')
insert into tblCountry values('Israel','IL
')
insert into tblCountry values('Italy','IT
')
insert into tblCountry values('Jamaica','JM
')
insert into tblCountry values('Japan','JP
')
insert into tblCountry values('Jordan','JO
')
insert into tblCountry values('Kazakhstan','KZ
')
insert into tblCountry values('Kenya','KE
')
insert into tblCountry values('Kiribati','KI
')
insert into tblCountry values('Korea, North','KP
')
insert into tblCountry values('Korea, South','KR
')
insert into tblCountry values('Kuwait','KW
')
insert into tblCountry values('Kyrgyzstan','KG
')
insert into tblCountry values('Laos','LA
')
insert into tblCountry values('Latvia','LV
')
insert into tblCountry values('Lebanon','LB
')
insert into tblCountry values('Lesotho','LS
')
insert into tblCountry values('Liberia','LR
')
insert into tblCountry values('Libya','LY
')
insert into tblCountry values('Liechtenstein','LI
')
insert into tblCountry values('Lithuania','LT
')
insert into tblCountry values('Luxembourg','LU
')
insert into tblCountry values('Macedonia','MK
')
insert into tblCountry values('Madagascar','MG
')
insert into tblCountry values('Malawi','MW
')
insert into tblCountry values('Malaysia','MY
')
insert into tblCountry values('Maldives','MV
')
insert into tblCountry values('Mali','ML
')
insert into tblCountry values('Malta','MT
')
insert into tblCountry values('Marshall Islands','MH
')
insert into tblCountry values('Mauritania','MR
')
insert into tblCountry values('Mauritius','MU
')
insert into tblCountry values('Mexico','MX
')
insert into tblCountry values('Micronesia','FM
')
insert into tblCountry values('Moldova','MD
')
insert into tblCountry values('Monaco','MC
')
insert into tblCountry values('Mongolia','MN
')
insert into tblCountry values('Montenegro','ME
')
insert into tblCountry values('Morocco','MA
')
insert into tblCountry values('Mozambique','MZ
')
insert into tblCountry values('Myanmar (Burma)','MM
')
insert into tblCountry values('Namibia','NA
')
insert into tblCountry values('Nauru','NR
')
insert into tblCountry values('Nepal','NP
')
insert into tblCountry values('Netherlands','NL
')
insert into tblCountry values('New Zealand','NZ
')
insert into tblCountry values('Nicaragua','NI
')
insert into tblCountry values('Niger','NE
')
insert into tblCountry values('Nigeria','NG
')
insert into tblCountry values('Norway','NO
')
insert into tblCountry values('Oman','OM
')
insert into tblCountry values('Pakistan','PK
')
insert into tblCountry values('Palau','PW
')
insert into tblCountry values('Panama','PA
')
insert into tblCountry values('Papua New Guinea','PG
')
insert into tblCountry values('Paraguay','PY
')
insert into tblCountry values('Peru','PE
')
insert into tblCountry values('Philippines','PH
')
insert into tblCountry values('Poland','PL
')
insert into tblCountry values('Portugal','PT
')
insert into tblCountry values('Qatar','QA
')
insert into tblCountry values('Romania','RO
')
insert into tblCountry values('Russia','RU
')
insert into tblCountry values('Rwanda','RW
')
insert into tblCountry values('Saint Kitts and Nevis','KN
')
insert into tblCountry values('Saint Lucia','LC
')
insert into tblCountry values('Saint Vincent and the Grenadines','VC
')
insert into tblCountry values('Samoa','WS
')
insert into tblCountry values('San Marino','SM
')
insert into tblCountry values('Sao Tome and Principe','ST
')
insert into tblCountry values('Saudi Arabia','SA
')
insert into tblCountry values('Senegal','SN
')
insert into tblCountry values('Serbia','RS
')
insert into tblCountry values('Seychelles','SC
')
insert into tblCountry values('Sierra Leone','SL
')
insert into tblCountry values('Singapore','SG
')
insert into tblCountry values('Slovakia','SK
')
insert into tblCountry values('Slovenia','SI
')
insert into tblCountry values('Solomon Islands','SB
')
insert into tblCountry values('Somalia','SO
')
insert into tblCountry values('South Africa','ZA
')
insert into tblCountry values('Spain','ES
')
insert into tblCountry values('Sri Lanka','LK
')
insert into tblCountry values('Sudan','SD
')
insert into tblCountry values('Suriname','SR
')
insert into tblCountry values('Swaziland','SZ
')
insert into tblCountry values('Sweden','SE
')
insert into tblCountry values('Switzerland','CH
')
insert into tblCountry values('Syria','SY
')
insert into tblCountry values('Tajikistan','TJ
')
insert into tblCountry values('Tanzania','TZ
')
insert into tblCountry values('Thailand','TH
')
insert into tblCountry values('Timor-Leste (East Timor)','TL
')
insert into tblCountry values('Togo','TG
')
insert into tblCountry values('Tonga','TO
')
insert into tblCountry values('Trinidad and Tobago','TT
')
insert into tblCountry values('Tunisia','TN
')
insert into tblCountry values('Turkey','TR
')
insert into tblCountry values('Turkmenistan','TM
')
insert into tblCountry values('Tuvalu','TV
')
insert into tblCountry values('Uganda','UG
')
insert into tblCountry values('Ukraine','UA
')
insert into tblCountry values('United Arab Emirates','AE
')
insert into tblCountry values('United Kingdom','GB
')
insert into tblCountry values('United States','US
')
insert into tblCountry values('Uruguay','UY
')
insert into tblCountry values('Uzbekistan','UZ
')
insert into tblCountry values('Vanuatu','VU
')
insert into tblCountry values('Vatican City','VA
')
insert into tblCountry values('Venezuela','VE
')
insert into tblCountry values('Vietnam','VN
')
insert into tblCountry values('Yemen','YE
')
insert into tblCountry values('Zambia','ZM
')
insert into tblCountry values('Zimbabwe','ZW
')
insert into tblCountry values('Abkhazia','GE
')
insert into tblCountry values('China, Republic of (Taiwan)','TW
')
insert into tblCountry values('Nagorno-Karabakh','AZ
')
insert into tblCountry values('Northern Cyprus','CY
')
insert into tblCountry values('Pridnestrovie (Transnistria)','MD
')
insert into tblCountry values('Somaliland','SO
')
insert into tblCountry values('South Ossetia','GE
')
insert into tblCountry values('Ashmore and Cartier Islands','AU
')
insert into tblCountry values('Christmas Island','CX
')
insert into tblCountry values('Cocos (Keeling) Islands','CC
')
insert into tblCountry values('Coral Sea Islands','AU
')
insert into tblCountry values('Heard Island and McDonald Islands','HM
')
insert into tblCountry values('Norfolk Island','NF
')
insert into tblCountry values('New Caledonia','NC
')
insert into tblCountry values('French Polynesia','PF
')
insert into tblCountry values('Mayotte','YT
')
insert into tblCountry values('Saint Barthelemy','GP
')
insert into tblCountry values('Saint Martin','GP
')
insert into tblCountry values('Saint Pierre and Miquelon','PM
')
insert into tblCountry values('Wallis and Futuna','WF
')
insert into tblCountry values('French Southern and Antarctic Lands','TF
')
insert into tblCountry values('Clipperton Island','PF
')
insert into tblCountry values('Bouvet Island','BV
')
insert into tblCountry values('Cook Islands','CK
')
insert into tblCountry values('Niue','NU
')
insert into tblCountry values('Tokelau','TK
')
insert into tblCountry values('Guernsey','GG
')
insert into tblCountry values('Isle of Man','IM
')
insert into tblCountry values('Jersey','JE
')
insert into tblCountry values('Anguilla','AI
')
insert into tblCountry values('Bermuda','BM
')
insert into tblCountry values('British Indian Ocean Territory','IO
')
insert into tblCountry values('British Sovereign Base Areas',' ')
insert into tblCountry values('British Virgin Islands','VG
')
insert into tblCountry values('Cayman Islands','KY
')
insert into tblCountry values('Falkland Islands (Islas Malvinas)','FK
')
insert into tblCountry values('Gibraltar','GI
')
insert into tblCountry values('Montserrat','MS
')
insert into tblCountry values('Pitcairn Islands','PN
')
insert into tblCountry values('Saint Helena','SH
')
insert into tblCountry values('South Georgia & South Sandwich Islands','GS
')
insert into tblCountry values('Turks and Caicos Islands','TC
')
insert into tblCountry values('Northern Mariana Islands','MP
')
insert into tblCountry values('Puerto Rico','PR
')
insert into tblCountry values('American Samoa','AS
')
insert into tblCountry values('Baker Island','UM
')
insert into tblCountry values('Guam','GU
')
insert into tblCountry values('Howland Island','UM
')
insert into tblCountry values('Jarvis Island','UM
')
insert into tblCountry values('Johnston Atoll','UM
')
insert into tblCountry values('Kingman Reef','UM
')
insert into tblCountry values('Midway Islands','UM
')
insert into tblCountry values('Navassa Island','UM
')
insert into tblCountry values('Palmyra Atoll','UM
')
insert into tblCountry values('U.S. Virgin Islands','VI
')
insert into tblCountry values('Wake Island','UM
')
insert into tblCountry values('Hong Kong','HK
')
insert into tblCountry values('Macau','MO
')
insert into tblCountry values('Faroe Islands','FO
')
insert into tblCountry values('Greenland','GL
')
insert into tblCountry values('French Guiana','GF
')
insert into tblCountry values('Guadeloupe','GP
')
insert into tblCountry values('Martinique','MQ
')
insert into tblCountry values('Reunion','RE
')
insert into tblCountry values('Aland','AX
')
insert into tblCountry values('Aruba','AW
')
insert into tblCountry values('Netherlands Antilles','AN
')
insert into tblCountry values('Svalbard','SJ
')
insert into tblCountry values('Ascension','AC
')
insert into tblCountry values('Tristan da Cunha','TA
')
insert into tblCountry values('Australian Antarctic Territory','AQ
')
insert into tblCountry values('Ross Dependency','AQ
')
insert into tblCountry values('Peter I Island','AQ
')
insert into tblCountry values('Queen Maud Land','AQ
')
insert into tblCountry values('British Antarctic Territory','AQ
')
--====================================================================

---13.Roles--------------------------->
insert into Roles values('Super Admin','true')


--=============================================
---14.MapRoleFeature--------------------------->
insert into MapRoleFeature values(2,1,'true')


--=============================================
---15.MapRoleUser--------------------------->
insert into MapRoleUser values(1,1,'true')


--=============================================