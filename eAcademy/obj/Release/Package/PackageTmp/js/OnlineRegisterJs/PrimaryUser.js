﻿$(document).ready(function () {
    $("input[name=PrimaryUser]:radio").change(function () {
        var prop_for = $("input[name=PrimaryUser]:checked").val();
        if (prop_for == 'Father') {
            $(".father-input").css("border-color", "crimson");
            $(".mother-input").css("border-color", "#CCCCCC");
            $(".guardian-input").css("border-color", "#CCCCCC");
            var email = $('#PrimaryEmail').val();            
            if (email != "") {
                $('#FatherEmail').val(email);
                $('#FatherEmail').addClass('father-input');
                $('#FatherMobileNo').removeClass('father-input');
                $('#FatherEmail').attr('readonly', true);                
                //$('#MotherEmail').val("");
                $('#MotherEmail').attr('readonly', false);
                //$('#GuardianEmail').val("");
                $('#GuardianEmail').attr('readonly', false);
            }
            var contact = $('#PrimaryContact').val();            
            if(contact!="")
            {
                $('#FatherMobileNo').val(contact);
                $('#FatherMobileNo').addClass('father-input');
                $('#FatherEmail').removeClass('father-input');
                $('#FatherMobileNo').attr('readonly', true);
                //$('#MotherEmail').val("");
                $('#MotherMobileNo').attr('readonly', false);
                //$('#GuardianEmail').val("");
                $('#GuardianMobileNo').attr('readonly', false);
            }
        } else if (prop_for == 'Mother') {
            $(".mother-input").css("border-color", "crimson");
            $(".father-input").css("border-color", "#CCCCCC");
            $(".guardian-input").css("border-color", "#CCCCCC");
            var email = $('#PrimaryEmail').val();
            if (email != "") {
                $('#MotherEmail').val(email);
                $('#MotherEmail').addClass('mother-input');
                $('#MotherMobileNo').removeClass('mother-input');
                $('#MotherEmail').attr('readonly', true);
                //$('#FatherEmail').val("");
                $('#FatherEmail').attr('readonly', false);
                //$('#GuardianEmail').val("");
                $('#GuardianEmail').attr('readonly', false);
            }
            var contact = $('#PrimaryContact').val();
            if (contact != "")
            {
                $('#MotherMobileNo').val(contact);
                $('#MotherMobileNo').addClass('mother-input');
                $('#MotherEmail').removeClass('mother-input');
                $('#MotherMobileNo').attr('readonly', true);
                //$('#FatherEmail').val("");
                $('#FatherMobileNo').attr('readonly', false);
                //$('#GuardianEmail').val("");
                $('#GuardianMobileNo').attr('readonly', false);
            }
        }
        else if (prop_for == 'Guardian') {
            $(".mother-input").css("border-color", "#CCCCCC");
            $(".father-input").css("border-color", "#CCCCCC");
            $(".guardian-input").css("border-color", "crimson");
            $("input").attr("href", "#GuardianName");
            var email = $('#PrimaryEmail').val();
            if (email != "") {
                $('#GuardianEmail').val(email);
                $('#GuardianEmail').addClass('guardian-input');
                $('#GuardianMobileNo').removeClass('guardian-input');
                $('#GuardianEmail').attr('readonly', true);
                //$('#MotherEmail').val("");
                $('#MotherEmail').attr('readonly', false);
                //$('#FatherEmail').val("");
                $('#FatherEmail').attr('readonly', false);
            }
            var contact = $('#PrimaryContact').val();
            if (contact != "") {
                $('#GuardianMobileNo').val(contact);
                $('#GuardianMobileNo').addClass('guardian-input');
                $('#GuardianEmail').removeClass('guardian-input');
                $('#GuardianMobileNo').attr('readonly', true);
                //$('#MotherEmail').val("");
                $('#MotherMobileNo').attr('readonly', false);
                //$('#FatherEmail').val("");
                $('#FatherMobileNo').attr('readonly', false);
            }
        }
    });
    $("input[name=PrimaryUser1]:radio").change(function () {
        var prop_for = $("input[name=PrimaryUser1]:checked").val();
        if (prop_for == 'Father') {
            $(".father-input").css("border-color", "crimson");
            $(".mother-input").css("border-color", "#CCCCCC");
            $(".guardian-input").css("border-color", "#CCCCCC");
            var email = $('#PrimaryEmail').val();
            if (email != "") {
                $('#FatherEmail').val(email);
            }
            var contact = $('#PrimaryContact').val();
            if (contact != "") {
                $('#FatherMobileNo').val(contact);
            }
        } else if (prop_for == 'Mother') {
            $(".mother-input").css("border-color", "crimson");
            $(".father-input").css("border-color", "#CCCCCC");
            $(".guardian-input").css("border-color", "#CCCCCC");
            var email = $('#PrimaryEmail').val();
            if (email != "") {
                $('#MotherEmail').val(email);
            }
            var contact = $('#PrimaryContact').val();
            if (contact != "") {
                $('#MotherMobileNo').val(contact);
            }
        }
        else if (prop_for == 'Guardian') {
            $(".mother-input").css("border-color", "#CCCCCC");
            $(".father-input").css("border-color", "#CCCCCC");
            $(".guardian-input").css("border-color", "crimson");
            $("input").attr("href", "#GuardianName");
            var email = $('#PrimaryEmail').val();
            if (email != "") {
                $('#GuardianEmail').val(email);
            }
            var contact = $('#PrimaryContact').val();
            if (contact != "") {
                $('#GuardianMobileNo').val(contact);
            }
        }
    });
    //$('.graduate').click(function () {
    //    $(this).addClass('active visited').siblings().removeClass('active visited');
    //});

});