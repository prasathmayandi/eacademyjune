﻿$(document).ready(function () {

   


    
  

    //Jqgrid for classInchage
    jQuery("#classinchargegrid").jqGrid({
        
        url: "/ClassReport/JQgridclassincharge",
        datatype: 'json',
        mtype: 'Get',
        colNames: ['Class Name', 'Section Name', 'Class Inchage'],
        colModel: [


                { key: false, name: 'ClassName', index: 'ClassName', editable: true },
                { key: false, name: 'SectionName', index: 'SectionName', editable: true },
                { key: false, name: 'ClassTeacher', index: 'ClassTeacher', editable: true },
              

        ],
        pager: jQuery("#pager"),
        rowNum: 10,
        rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        caption: 'ClassInchage List',
        loadonce: true,
        emptyrecords: "No records to display",
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            id: "0"
        },
        autowidth: true,
        multiselect: false,
    }).navGrid('#pager', { edit: false, add: false, del: false, search: false, refresh: true }
       ).jqGrid('navButtonAdd', '#pager',
       {
           caption: " Excel ",
           buttonicon: "ui-icon-bookmark",
           position: "last",
           onClickButton: function () {
               classinchargeExportDataToExcel("#classinchargegrid");
           }
       }).jqGrid('navButtonAdd', '#pager',
       {
           caption: " Pdf ",
           buttonicon: "ui-icon-bookmark",
           position: "last",
           onClickButton: function () {
               classinchargeExportDataToPdf();
           }
       });


    function classinchargeExportDataToExcel(tableCtrl) {
        //  Export the data from our jqGrid into a "real" Excel 2007 file
        ExportJQGridDataToExcel(tableCtrl, "ClassIncharge.xlsx");
    }
    function classinchargeExportDataToPdf() {
        window.location.href = "../AcademicReport/ClassIncharge?pdf=true";
        //  Export the data from our jqGrid into a "real" Excel 2007 file
        ExportJQGridDataToPdf("ClassInchageList.pdf");
    }




    $('#classinchargefilter').click(function (event) {

        event.preventDefault();

        classinchargefilterGrid();

    });


    function classinchargefilterGrid() {

        var postDataValues = $("#classinchargegrid").jqGrid('getGridParam', 'postData');
        $('.filterItem').each(function (index, item) {
            postDataValues[$(item).attr('id')] = $(item).val();
        });
        $('#classinchargegrid').jqGrid().setGridParam({
            postData: postDataValues, page: 1
        }).trigger('reloadGrid');
    }
});