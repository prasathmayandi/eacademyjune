﻿using eAcademy.ApiModels;
using eAcademy.DataModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;

namespace eAcademy.ApiServices
{
    public class NotificationServices
    {
        EacademyEntities db = new EacademyEntities();
        AdmissionTransactionServices admissionTrans = new AdmissionTransactionServices();
        AssignClassToStudentServices stu = new AssignClassToStudentServices();
        public bool addpushtoken(int ParentRegId,string ntoken) 
        {
            var getRow = (from a in db.PrimaryUserRegisters where a.PrimaryUserRegisterId == ParentRegId select a).FirstOrDefault();
            if (getRow!=null)
            {
                getRow.AppToken = ntoken;
                db.SaveChanges();
            }           
            return true;
        }

        public ArrayList getUnreadNotificationList(int parentRegId)
        {
            var getStudentList = admissionTrans.GetStudentList(parentRegId).ToList();

            ArrayList List = new ArrayList();

            for (int i = 0; i < getStudentList.Count; i++)
            {
                int studentRegId = getStudentList[i].StudentRegId;
                var row = stu.GetClassDetails(studentRegId);
                int academicYearId = row.AcademicYearId.Value;
                int classId = row.ClassId.Value;
                int secId = row.SectionId.Value;


                var ListByStudent = ((from a in db.ParentLeaveRequests
                                      from b in db.Students
                                      where a.ParentRegisterId == parentRegId &&
                                      a.StudentRegisterId == b.StudentRegisterId &&
                                      a.StudentRegisterId == studentRegId &&
                                      a.Status != "Pending" &&
                                      a.NotificationStatus == "UnRead"
                                      select new Notification
                                      {
                                          FromDate = SqlFunctions.DateName("day", a.FromDate) + "/" + SqlFunctions.StringConvert((double)a.FromDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.FromDate).Trim(),
                                          ToDate = SqlFunctions.DateName("day", a.ToDate) + "/" + SqlFunctions.StringConvert((double)a.ToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.ToDate).Trim(),
                                          LeaveRequestStatus = a.Status,
                                          FileName = "",                                          
                                          PostedDate = SqlFunctions.DateName("day", a.ApprovalDate).Trim() + " " + SqlFunctions.DateName("month", a.ApprovalDate).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.ApprovalDate)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.ApprovalDate)).Trim(),
                                          studentRegId = studentRegId,
                                          StudentName = b.FirstName + " " + b.LastName,
                                          StudentId=b.StudentId,
                                          StudentPhoto = b.Photo,
                                          Reason=a.LeaveReason,
                                          NotificationFrom = "LEAVE REQUEST",
                                          RowId = a.RequestId
                                      }).ToList()

                .Union(from a in db.StudentCommunications
                       from c in db.Students
                       where a.StudentRegisterId == c.StudentRegisterId &&
                       a.AcademicYearId == academicYearId &&
                       a.ClassId == classId &&
                       a.SectionId == secId &&
                       c.StudentRegisterId == studentRegId &&
                       a.NotificationStatus == "UnRead" &&
                       a.Status == true
                       select new Notification
                       {
                           Title = a.Title,
                           Description = a.Description,
                           EventDate = a.DateOfAnnouncement,
                           FileName = a.FileName,
                           PostedDate = SqlFunctions.DateName("day", a.MarkedDate).Trim() + " " + SqlFunctions.DateName("month", a.MarkedDate).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.MarkedDate)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.MarkedDate)).Trim(),
                           studentRegId = studentRegId,
                           StudentName = c.FirstName+" "+c.LastName,
                           StudentId = c.StudentId,
                           StudentPhoto = c.Photo,
                           NotificationFrom = "COMMUNICATION",
                           RowId = a.Id
                       }).ToList()

                                //.Union(from a in db.MomentOfTheDays
                                //       from c in db.Students
                                //       where a.SentMoment == "Parent" &&
                                //       c.StudentRegisterId == studentRegId
                                //       && !(from e in db.MomentOfTheDayNotifications
                                //            where e.MomentOfTheDayId == a.MomentsId &&
                                //            e.ParentRegisterId == parentRegId
                                //            select e.MomentOfTheDayId).Contains(a.MomentsId)
                                //       select new Notification
                                //       {
                                //           Title = a.Moment_Title,
                                //           Description = a.Moment_Description,
                                //           FileName = a.Moment_Image,
                                //           PostedDate = SqlFunctions.DateName("day", a.Updated_date).Trim() + " " + SqlFunctions.DateName("month", a.Updated_date).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.Updated_date)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.Updated_date)).Trim(),
                                //           studentRegId = c.StudentRegisterId,
                                //           StudentPhoto = c.Photo,
                                //           NotificationFrom = "MOMENT OF THE DAY",
                                //           RowId = a.MomentsId
                                //       }).ToList()

                                 .Union(from a in db.Albums
                                        from c in db.Students
                                        where a.SentMoment == "Parent" &&
                                        a.Status == true &&
                                        c.StudentRegisterId == studentRegId
                                        && !(from e in db.AlbumNotifications
                                             where e.AlbumId == a.AlbumId &&
                                             e.ParentRegisterId == parentRegId
                                             select e.AlbumId).Contains(a.AlbumId)
                                        select new Notification
                                        {
                                            AlbumId = a.AlbumId,
                                            Title = a.Album_Title,
                                            Description = a.Album_Description,
                                            PostedDate = SqlFunctions.DateName("day", a.Updated_date).Trim() + " " + SqlFunctions.DateName("month", a.Updated_date).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.Updated_date)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.Updated_date)).Trim(),
                                            studentRegId = c.StudentRegisterId,
                                            StudentId = c.StudentId,
                                            StudentName = c.FirstName + " " + c.LastName,
                                            StudentPhoto = c.Photo,
                                            NotificationFrom = "ALBUM",
                                            RowId = a.AlbumId
                                        }).ToList()
                                        //.Select(a => new Notification
                                        //{
                                        //    Title = a.Title,
                                        //    Description = a.Description,
                                        //    AlbumImg = getAlbumImage(a.RowId), // b.ImageFile,
                                        //    PostedDate = SqlFunctions.DateName("day", a.PostedDate).Trim() + " " + SqlFunctions.DateName("month", a.PostedDate).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.PostedDate)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.PostedDate)).Trim(),
                                        //    studentRegId = a.studentRegId,
                                        //    StudentPhoto = a.StudentPhoto,
                                        //    NotificationFrom = "Album",
                                        //    RowId = a.RowId
                                        //}).ToList()

                                      .Union(from a in db.ClassCommunications
                                             from c in db.Students
                                        where a.Status == true &&
                                        a.AcademicYearId == academicYearId &&
                                        a.ClassId == classId &&
                                         c.StudentRegisterId == studentRegId &&
                                        !(from e in db.ClassCommunicationNotifications
                                          where e.ClassCommunicationId == a.Id
                                          && e.ParentRegisterId == parentRegId
                                          select e.ClassCommunicationId).Contains(a.Id)
                                        select new Notification
                                        {
                                            Title = a.Title,
                                            Description = a.Description,
                                            EventDate = a.DateOfAnnouncement,
                                            FileName = a.FileName,
                                            PostedDate = SqlFunctions.DateName("day", a.MarkedDate).Trim() + " " + SqlFunctions.DateName("month", a.MarkedDate).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.MarkedDate)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.MarkedDate)).Trim(),
                                            studentRegId = c.StudentRegisterId,
                                            StudentId = c.StudentId,
                                            StudentName = c.FirstName + " " + c.LastName,
                                            StudentPhoto = c.Photo,
                                            NotificationFrom = "COMMUNICATION",
                                            RowId = a.Id                                           
                                        }).ToList()
                                       ).OrderBy(x => x.PostedDate).ToList();

                List.AddRange(ListByStudent);
            }

            var GeneralList = (from a in db.Albums
                                      where  a.SentMoment == "All" &&
                                      a.Status == true &&
                                      !(from e in db.AlbumNotifications
                                        where e.AlbumId == a.AlbumId &&                                      
                                        e.ParentRegisterId == parentRegId
                                        select e.AlbumId).Contains(a.AlbumId)
                                      select new Notification
                                      {
                                          AlbumId=a.AlbumId,
                                          Title = a.Album_Title,
                                          Description = a.Album_Description,
                                          PostedDate = SqlFunctions.DateName("day", a.Updated_date).Trim() + " " + SqlFunctions.DateName("month", a.Updated_date).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.Updated_date)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.Updated_date)).Trim(),
                                          NotificationFrom = "ALBUM",
                                          RowId = a.AlbumId
                                      }).ToList().OrderBy(x => x.PostedDate).ToList();


                //var GeneralList1 =     (from a in db.Albums
                //                        where a.SentMoment == "All" &&
                //                         !(from e in db.AlbumNotifications
                //                             where e.AlbumId == a.AlbumId &&
                //                             e.ParentRegisterId == parentRegId
                //                             select e.AlbumId).Contains(a.AlbumId)
                //                        select new Notification
                //                        {
                //                            Title = a.Album_Title,
                //                            Description = a.Album_Description,
                //                            // b.ImageFile,
                //                            PostedDate = SqlFunctions.DateName("day", a.Updated_date).Trim() + " " + SqlFunctions.DateName("month", a.Updated_date).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.Updated_date)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.Updated_date)).Trim(),
                //                            studentRegId = c.StudentRegisterId,
                //                            StudentPhoto = c.Photo,
                //                            NotificationFrom = "ALBUM",
                //                            RowId = a.AlbumId
                //                        }).ToList()
                                      
                                      //.Select(a => new Notification
                                      //{
                                      //    Title = a.Title,
                                      //    Description = a.Description,
                                      //    AlbumImg = getAlbumImage(a.RowId), // b.ImageFile,
                                      //    PostedDate = SqlFunctions.DateName("day", a.PostedDate).Trim() + " " + SqlFunctions.DateName("month", a.PostedDate).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.PostedDate)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.PostedDate)).Trim(),
                                      //    studentRegId = a.studentRegId,
                                      //    StudentPhoto = a.StudentPhoto,
                                      //    NotificationFrom = "Album",
                                      //    RowId = a.RowId
                                      //}).ToList()

                                       //.Union(from a in db.ArticleOfTheWeeks
                                       //       where
                                       //       !(from e in db.ArticleOfTheWeekNotifications
                                       //         where e.ArticleOfTheWeekId == a.ArticleId &&
                                       //         e.ParentRegisterId == parentRegId
                                       //         select e.ArticleOfTheWeekId).Contains(a.ArticleId)
                                       //       select new Notification
                                       //       {
                                       //           Title = a.Article_Title,
                                       //           Description = a.Article_Description,
                                       //           PostedDate = SqlFunctions.DateName("day", a.Updated_date).Trim() + " " + SqlFunctions.DateName("month", a.Updated_date).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.Updated_date)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.Updated_date)).Trim(),
                                       //           NotificationFrom = "ARTICLE OF THE WEEK",
                                       //           RowId = a.ArticleId                                                  
                                       //       })).ToList().OrderBy(x => x.PostedDate).ToList();

            List.AddRange(GeneralList);
            return List;
        }



        public List<AlbumImage> getAlbumImage(int albumId)
        {
            var list = (from a in db.AlbumImages where a.AlbumId == albumId select a).ToList();
            return list;
        }


        public void UpdateNotificationListRead(int parentRegId)
        {
            

            var getStudentList = admissionTrans.GetStudentList(parentRegId).ToList();

            for (int i = 0; i < getStudentList.Count; i++)
            {
                int studentRegId = getStudentList[i].StudentRegId;
                var row = stu.GetClassDetails(studentRegId);
                int academicYearId = row.AcademicYearId.Value;
                int classId = row.ClassId.Value;
                int secId = row.SectionId.Value;

                var LeaveReqestLst = (from a in db.ParentLeaveRequests
                                      from b in db.Students
                                      where a.ParentRegisterId == parentRegId &&
                                      a.StudentRegisterId == b.StudentRegisterId &&
                                      a.StudentRegisterId == studentRegId &&
                                      a.Status != "Pending" &&
                                      a.NotificationStatus == "UnRead"
                                      select new
                                      {
                                          LeavReqId = a.RequestId
                                      }).ToList();

                for (int y = 0; y < LeaveReqestLst.ToList().Count; y++)
                {
                    int reqId = LeaveReqestLst[y].LeavReqId;
                    var row_Leave = (from a in db.ParentLeaveRequests where a.RequestId == reqId select a).FirstOrDefault();
                    row_Leave.NotificationStatus = "Read";
                    db.SaveChanges();
                }

                var studentCommunicationList = (from a in db.StudentCommunications
                                                from c in db.Students
                                                where a.StudentRegisterId == c.StudentRegisterId &&
                                                a.AcademicYearId == academicYearId &&
                                                a.ClassId == classId &&
                                                a.SectionId == secId &&
                                                c.StudentRegisterId == studentRegId &&
                                                a.NotificationStatus == "UnRead" &&
                                                a.Status == true
                                                select new
                                                {
                                                    RowId = a.Id,
                                                }).ToList();

                for (int x = 0; x < studentCommunicationList.ToList().Count; x++)
                {
                    int reqId = studentCommunicationList[x].RowId;
                    var row1 = (from a in db.StudentCommunications where a.Id == reqId select a).FirstOrDefault();
                    row1.NotificationStatus = "Read";
                    db.SaveChanges();
                }


                var ClassCommunicationList = (from a in db.ClassCommunications
                                              from c in db.Students
                                              where a.Status == true &&
                                              a.AcademicYearId == academicYearId &&
                                              a.ClassId == classId &&
                                               c.StudentRegisterId == studentRegId &&
                                              !(from e in db.ClassCommunicationNotifications
                                                where e.ClassCommunicationId == a.Id
                                                && e.ParentRegisterId == parentRegId
                                                select e.ClassCommunicationId).Contains(a.Id)
                                              select new Notification
                                              {
                                                  RowId = a.Id
                                              }).ToList();



                foreach (var a in ClassCommunicationList)
                {
                    ClassCommunicationNotification c = new ClassCommunicationNotification();
                    c.ClassCommunicationId = a.RowId;
                    c.ParentRegisterId = parentRegId;
                    c.NotificationStatus = "Read";
                    db.ClassCommunicationNotifications.Add(c);
                    db.SaveChanges();
                }
                


                //var MomentOfTheDay_Parent  = (from a in db.MomentOfTheDays
                //                          from c in db.Students
                //                          where a.SentMoment == "Parent" &&
                //                          c.StudentRegisterId == studentRegId
                //                          && !(from e in db.MomentOfTheDayNotifications
                //                               where e.MomentOfTheDayId == a.MomentsId &&
                //                               e.ParentRegisterId == parentRegId
                //                               select e.MomentOfTheDayId).Contains(a.MomentsId)
                //                          select new Notification
                //                          {
                //                              RowId = a.MomentsId,
                //                          }).ToList();
                //foreach (var a in MomentOfTheDay_Parent)
                //{
                //    MomentOfTheDayNotification m = new MomentOfTheDayNotification();
                //    m.MomentOfTheDayId = a.RowId;
                //    m.ParentRegisterId = parentRegId;
                //    m.NotificationStatus = "Read";
                //    db.MomentOfTheDayNotifications.Add(m);
                //    db.SaveChanges();
                //}
            }

            //var MomentOfTheDay_All = (from a in db.MomentOfTheDays
            //                          where a.SentMoment == "All" &&
            //                          !(from e in db.MomentOfTheDayNotifications
            //                            where e.MomentOfTheDayId == a.MomentsId &&
            //                            e.ParentRegisterId == parentRegId
            //                            select e.MomentOfTheDayId).Contains(a.MomentsId)
            //                             select new Notification
            //                             {
            //                                 RowId = a.MomentsId,
            //                             }).ToList();

            //foreach (var a in MomentOfTheDay_All)
            //{
            //    MomentOfTheDayNotification mp = new MomentOfTheDayNotification();
            //    mp.MomentOfTheDayId = a.RowId;
            //    mp.ParentRegisterId = parentRegId;
            //    mp.NotificationStatus = "Read";
            //    db.MomentOfTheDayNotifications.Add(mp);
            //    db.SaveChanges();
            //}
            

            //var ArticleOfTheWeek = (from a in db.ArticleOfTheWeeks
            //                        where
            //                        !(from e in db.ArticleOfTheWeekNotifications
            //                          where e.ArticleOfTheWeekId == a.ArticleId &&
            //                          e.ParentRegisterId == parentRegId
            //                          select e.ArticleOfTheWeekId).Contains(a.ArticleId)
            //                        select new Notification
            //                        {
            //                            RowId = a.ArticleId,
            //                        }).ToList();

            //foreach (var a in ArticleOfTheWeek)
            //{
            //    ArticleOfTheWeekNotification art = new ArticleOfTheWeekNotification();
            //    art.ArticleOfTheWeekId = a.RowId;
            //    art.ParentRegisterId = parentRegId;
            //    art.NotificationStatus = "Read";
            //    db.ArticleOfTheWeekNotifications.Add(art);
            //    db.SaveChanges();
            //}
           
        }
    }
}
