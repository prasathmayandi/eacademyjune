﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.ApiServices
{
    public class TokenAuthSservices
    {
        EacademyEntities db = new EacademyEntities();

        public bool AddToken(string token, int userId)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            TokenAuth add = new TokenAuth()
            {
                Token = token,
                Login = date,
                UserId = userId
            };
            db.TokenAuths.Add(add);
            db.SaveChanges();

            return true;
        }


        public TokenAuth IsValidUser(string token)
        {
            var row = (from a in db.TokenAuths where a.Token == token select a).FirstOrDefault();
            if(row != null)
            {
                DateTime? logout = row.Logout;
                if (logout == null)
                {
                    return row;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }


        public void Logout(string token)
        {
            var row = (from a in db.TokenAuths where a.Token == token select a).FirstOrDefault();
            row.Logout = DateTimeByZone.getCurrentDateTime();
            db.SaveChanges();
        }

    }
}