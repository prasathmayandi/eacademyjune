﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;

namespace eAcademy.ApiServices
{
    public class CalendarServices
    {
        EacademyEntities db = new EacademyEntities();

        public IEnumerable<object> GetEventsList()
        {

            var list = (from a in db.Events
                        where a.Status == true 
                        //a.EventDate >= DateTime.Today
                        select new
                        {
                            MomentId = a.EventId,
                            PostedDate = SqlFunctions.DateName("day", a.Updated_date).Trim() + "/" + SqlFunctions.StringConvert((double)a.Updated_date.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.Updated_date),
                            //EventDate = SqlFunctions.DateName("year", a.EventDate) + "," + SqlFunctions.StringConvert((double)a.EventDate.Month).TrimStart() + "," + SqlFunctions.DateName("day", a.EventDate).Trim(),
                            EventDate =a.EventDate,
                            Title = a.EventName,
                            Description = a.EventDecribtion
                        }).ToList();
            return list;
        }
    }
}