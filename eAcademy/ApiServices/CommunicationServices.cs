﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;

namespace eAcademy.ApiServices
{
    public class CommunicationServices
    {
        EacademyEntities db = new EacademyEntities();

        //public IEnumerable<object> GetTodayCommunicationList(int yearId, int classId, int secId, int studentRegId)
        //{

        //    var list = ((from a in db.ClassCommunications
        //                       where a.AcademicYearId == yearId &&
        //                       a.ClassId == classId &&
        //                       a.Status == true
        //                       select new
        //                       {
        //                           CommunicationId = a.Id,
        //                           PostedDate = SqlFunctions.DateName("day", a.MarkedDate).Trim() + "/" + SqlFunctions.StringConvert((double)a.MarkedDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.MarkedDate),
        //                           Title = a.Title,
        //                           Description = a.Description,
        //                           AnnouncementDate = SqlFunctions.DateName("day", a.DateOfAnnouncement).Trim() + "/" + SqlFunctions.StringConvert((double)a.DateOfAnnouncement.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DateOfAnnouncement),
        //                           File = a.FileName
        //                       }).ToList().Union((from a in db.StudentCommunications
        //                where a.AcademicYearId == yearId &&
        //                a.ClassId == classId &&
        //                a.SectionId == secId &&
        //                a.StudentRegisterId == studentRegId &&
        //                a.Status == true
        //                select new
        //                {
        //                    CommunicationId = a.Id,
        //                    PostedDate = SqlFunctions.DateName("day", a.MarkedDate).Trim() + "/" + SqlFunctions.StringConvert((double)a.MarkedDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.MarkedDate),
        //                    Title = a.Title,
        //                    Description = a.Description,
        //                    AnnouncementDate = SqlFunctions.DateName("day", a.DateOfAnnouncement).Trim() + "/" + SqlFunctions.StringConvert((double)a.DateOfAnnouncement.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DateOfAnnouncement),
        //                    File = a.FileName
        //                }).ToList())).ToList();

        //    return list;
        //}


        public IEnumerable<object> GetCommunicationListByDate(int yearId, int classId, int secId, int studentRegId, DateTime? fdate, DateTime? ToDate)
        {

            var list = ((from a in db.ClassCommunications
                         where a.AcademicYearId == yearId &&
                         a.ClassId == classId &&
                         a.MarkedDate >= fdate &&
                         a.MarkedDate < ToDate &&
                         a.Status == true
                         select new
                         {
                             CommunicationId = a.Id,
                             PostedDate = SqlFunctions.DateName("day", a.MarkedDate).Trim() + " " + SqlFunctions.DateName("month", a.MarkedDate).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.MarkedDate)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.MarkedDate)).Trim(),
                             Title = a.Title,
                             Description = a.Description,
                             AnnouncementDate = SqlFunctions.DateName("day", a.DateOfAnnouncement).Trim() + "/" + SqlFunctions.StringConvert((double)a.DateOfAnnouncement.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DateOfAnnouncement),
                             File = a.FileName
                         }).ToList().Union((from a in db.StudentCommunications
                                            where a.AcademicYearId == yearId &&
                                            a.ClassId == classId &&
                                            a.SectionId == secId &&
                                            a.StudentRegisterId == studentRegId &&
                                            a.MarkedDate >= fdate &&
                                            a.MarkedDate < ToDate &&
                                            a.Status == true
                                            select new
                                            {
                                                CommunicationId = a.Id,
                                                PostedDate = SqlFunctions.DateName("day", a.MarkedDate).Trim() + " " + SqlFunctions.DateName("month", a.MarkedDate).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.MarkedDate)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.MarkedDate)).Trim(),
                                                Title = a.Title,
                                                Description = a.Description,
                                                AnnouncementDate = SqlFunctions.DateName("day", a.DateOfAnnouncement).Trim() + "/" + SqlFunctions.StringConvert((double)a.DateOfAnnouncement.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DateOfAnnouncement),
                                                File = a.FileName
                                            }).ToList())).ToList();

            return list;
        }
    }
}