﻿using eAcademy.ApiModels;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;

namespace eAcademy.ApiServices
{
    public class ParentLeaveRequestServices
    {
        EacademyEntities db = new EacademyEntities();


        public bool AddParentLeaveRequest(int ParentRegId, int StudentRegId, int yearId, int classId, int secId, DateTime fdate, DateTime toDate, string reason, int? EmployeeRegId)
        {
            Double NoOfDays = (toDate - fdate).TotalDays;
            int NoOfDays1 = Convert.ToInt32(NoOfDays) + 1;

            ParentLeaveRequest add = new ParentLeaveRequest()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                ParentRegisterId = ParentRegId,
                StudentRegisterId = StudentRegId,
                FromDate = fdate,
                ToDate = toDate,
                NumberOfDays = NoOfDays1,
                LeaveReason = reason,
                Status = "Pending",
                NotificationStatus ="UnRead",
                EmployeeRegisterId = EmployeeRegId
            };
            db.ParentLeaveRequests.Add(add);
            db.SaveChanges();
            return true;
        }

        public IEnumerable<LeaveRequest> getLeaveReqDetail(int StudentRegId)
        {
            var LeaveRequestStatus1 = (from a in db.ParentLeaveRequests
                                       where a.StudentRegisterId == StudentRegId
                                       select new LeaveRequest
                                       {
                                           fromdate = a.FromDate.Value,
                                           fdate = SqlFunctions.DateName("day", a.FromDate) + "/" + SqlFunctions.StringConvert((double)a.FromDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.FromDate).Trim(),
                                           tdate = SqlFunctions.DateName("day", a.ToDate) + "/" + SqlFunctions.StringConvert((double)a.ToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.ToDate).Trim(),
                                           reason = a.LeaveReason,
                                           status = a.Status
                                       }).OrderByDescending(x => x.fromdate).Take(3).ToList();
            return LeaveRequestStatus1;
        }


        public IEnumerable<object> getLeaveReqStatusList(int parentRegId)
        {
            var list = (from a in db.ParentLeaveRequests
                        from b in db.Students
                        where a.ParentRegisterId == parentRegId &&
                        a.StudentRegisterId == b.StudentRegisterId &&
                        a.Status != "Pending" &&
                        a.NotificationStatus == "UnRead"
                        select new
                        {
                            StudentRegId = a.StudentRegisterId,
                            StudentPhoto = b.Photo,
                            FromDate = SqlFunctions.DateName("day", a.FromDate) + "/" + SqlFunctions.StringConvert((double)a.FromDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.FromDate).Trim(),
                            ToDate = SqlFunctions.DateName("day", a.ToDate) + "/" + SqlFunctions.StringConvert((double)a.ToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.ToDate).Trim(),
                            Status = a.Status
                        }).ToList();
            return list;
        }


        public void UpdateLeaveReqListRead(int parentRegId)
        {
            var list = (from a in db.ParentLeaveRequests
                        where a.ParentRegisterId == parentRegId &&
                        a.Status != "Pending" &&
                        a.NotificationStatus == "UnRead"
                        select new 
                        {
                          LeavReqId = a.RequestId
                        }).ToList();

            for(int i=0; i<list.ToList().Count; i++)
            {
                int reqId = list[i].LeavReqId;
                var row = (from a in db.ParentLeaveRequests where a.RequestId == reqId select a).FirstOrDefault();
                row.NotificationStatus = "Read";
                db.SaveChanges();
            }
    
        }
    }
}