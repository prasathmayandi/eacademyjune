﻿using eAcademy.DataModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace eAcademy.ApiServices
{
    public class StudentDailyAttendanceServices
    {
        EacademyEntities db = new EacademyEntities();

        public object getAttendanceMonth(int yearId, int classId, int secId,int studentRegId)
        {
            var getAttendanceMonth = (from a in db.StudentDailyAttendances
                                      where a.AcademicYearId == yearId &&
                                      a.ClassId == classId &&
                                      a.SectionId == secId
                                      select new
                                      {
                                          month = a.CurrentDate.Value.Month,
                                          year = a.CurrentDate.Value.Year,
                                      }).OrderBy(m => m.month).Distinct().ToList().OrderBy(x => x.year).ToList(); 

            ArrayList month1 = new ArrayList();
            ArrayList monthName = new ArrayList();
            ArrayList presentDaysCount = new ArrayList();
            ArrayList NumberOfWorkingDays = new ArrayList();

            var totalMonthCount = getAttendanceMonth.Count;

            for (int i = 0; i < getAttendanceMonth.Count; i++)
            {
                var month = getAttendanceMonth[i].month;
                var year = getAttendanceMonth[i].year;
                //var month_Name = getAttendanceMonth[i].monthname.Value.ToString("MMM");
                //string month_Name="";

                //if(month == 1)
                //{
                //    month_Name = "Jan";
                //}
                string month_Name = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month).Substring(0, 3) + "/" + year;
                StudentDailyAttendanceServices a = new StudentDailyAttendanceServices();
                var getPresentDays = a.getPresentDays(yearId, classId, secId, month, studentRegId);
                var getWorkingDays = a.getWorkingDays(yearId, classId, secId, month);
                int TotalWorkingDays = getWorkingDays;
                presentDaysCount.Add(getPresentDays.Count);
                month1.Add(month);
                monthName.Add(month_Name);
                NumberOfWorkingDays.Add(TotalWorkingDays);
            }

            return new { monthName, presentDaysCount, NumberOfWorkingDays, totalMonthCount };
        }

        public List<StudentDailyAttendance> getPresentDays(int yearId, int classId, int secId, int month, int studentRegId)
        {
            var getPresentDays = (from a in db.StudentDailyAttendances
                                  where a.AcademicYearId == yearId &&
                                  a.ClassId == classId &&
                                  a.SectionId == secId &&
                                  a.StudentRegisterId == studentRegId &&
                                  a.CurrentDate.Value.Month == month &&
                                  a.AttendanceStatus == "P"
                                  select a).ToList();
            return getPresentDays;
        }

        public int getWorkingDays(int? yearId, int? classId, int? secId, int month)
        {
            var getWorkingDays = (from a in db.StudentDailyAttendances
                                  from b in db.Students
                                  where a.StudentRegisterId == b.StudentRegisterId &&
                                      a.AcademicYearId == yearId &&
                                      a.ClassId == classId &&
                                      a.SectionId == secId &&
                                      a.CurrentDate.Value.Month == month &&
                                      b.StudentStatus == true
                                  select new 
                                  {
                                      date = a.CurrentDate
                                  }).Distinct().ToList().Count;
            return getWorkingDays;
        }


    }
}