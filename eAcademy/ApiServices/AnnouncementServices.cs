﻿using eAcademy.ApiModels;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;

namespace eAcademy.ApiServices
{
    public class AnnouncementServices
    {
        EacademyEntities dc = new EacademyEntities();

        public IEnumerable<AnnouncementList> GetAnnouncementList()
        {
            var currentDate = DateTimeByZone.getCurrentDate();
            var list = (from a in dc.Announcements
                        where a.AnnouncementDate.Year == currentDate.Year &&
                        //a.AnnouncementDate.Month == currentDate.Month &&
                        a.AnnouncementDate >= DateTimeByZone.getCurrentDate() &&
                        a.Status == true 
                        orderby a.AnnouncementId descending
                        select new AnnouncementList
                        {
                            announcementname = a.AnnouncementName,
                            announcementdescription =a.AnnouncementDescription,
                            announcementdate = SqlFunctions.DateName("day", a.AnnouncementDate).Trim() + " " + SqlFunctions.DateName("month", a.AnnouncementDate).Remove(3) + " " + SqlFunctions.DateName("year", a.AnnouncementDate).Trim(),
                            PostedDate = SqlFunctions.DateName("day", a.Updated_date).Trim() + " " + SqlFunctions.DateName("month", a.Updated_date).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.Updated_date)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.Updated_date)).Trim(),
                        }).ToList(); //db.AcademicYears.Where(q => q.AcademicYearId == id).FirstOrDefault();      
            return list;
        }

        public int getPresentDays(int yearId, int classId, int secId, int month, int studentRegId)
        {
            int presentDaysCount = 5;
            return presentDaysCount;
        }

        public Announcement PushNotifi()
        {
            var row = (from a in dc.Announcements where a.PushNotification == true && a.PushNotificationDeliveryStatus == "Pending" select a).FirstOrDefault();
            row.PushNotificationDeliveryStatus = "Sent";
            dc.SaveChanges();
            return row;
        }

    }
}