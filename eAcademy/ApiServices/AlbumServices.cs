﻿using eAcademy.ApiModels;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;

namespace eAcademy.ApiServices
{
    public class AlbumServices
    {
        EacademyEntities db = new EacademyEntities();
       
        public IEnumerable<object> GetAlbumListByMonth(int yearId, int classId, int secId, int studentRegId,DateTime? date)
        {
            var list = (((from a in db.Albums
                        where  a.Updated_date.Value.Month == date.Value.Month &&
                        a.Updated_date.Value.Year == date.Value.Year &&
                        a.Status == true &&
                        a.SentMoment == "All"
                        select new
                        {
                            AlbumId = a.AlbumId,
                            PostedDate = SqlFunctions.DateName("day", a.Updated_date).Trim() + " " + SqlFunctions.DateName("month", a.Updated_date).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.Updated_date)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.Updated_date)).Trim(),
                            Title = a.Album_Title,
                            Description = a.Album_Description,
                        }).ToList().Select(a => new //Notification
            {
                Title = a.Title,
                Description = a.Description,
                AlbumImg = getAlbumImage(a.AlbumId), 
                PostedDate =a.PostedDate
                //PostedDate = SqlFunctions.DateName("day", a.PostedDate).Trim() + " " + SqlFunctions.DateName("month", a.PostedDate).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.PostedDate)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.PostedDate)).Trim(),
            })).ToList()
            .Union((from a in db.Albums
                        where a.Updated_date.Value.Month == date.Value.Month &&
                        a.Updated_date.Value.Year == date.Value.Year &&
                        a.Status == true &&
                        a.SentMoment == "Parent" && 
                        a.AcademicYear == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.StudentId == studentRegId
                        select new
                        {
                            AlbumId = a.AlbumId,
                            PostedDate = SqlFunctions.DateName("day", a.Updated_date).Trim() + " " + SqlFunctions.DateName("month", a.Updated_date).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.Updated_date)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.Updated_date)).Trim(),
                            Title = a.Album_Title,
                            Description = a.Album_Description,
                        }).ToList().Select(a => new //Notification
                        {
                            Title = a.Title,
                            Description = a.Description,
                            AlbumImg = getAlbumImage(a.AlbumId),
                            PostedDate = a.PostedDate
                            //PostedDate = SqlFunctions.DateName("day", a.PostedDate).Trim() + " " + SqlFunctions.DateName("month", a.PostedDate).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.PostedDate)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.PostedDate)).Trim(),
                        })).ToList());

            return list;

        }

        public List<AlbumImage> getAlbumImage(int albumId)
        {
            var list = (from a in db.AlbumImages
                        where a.AlbumId == albumId
                        select new 
                        {
                            ImageFile = a.ImageFile
                        }).ToList().Select(a=> new AlbumImage
                        {
                            ImageFile = a.ImageFile
                        }).ToList();
            return list;
        }
    }
}