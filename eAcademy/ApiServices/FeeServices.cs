﻿using eAcademy.ApiModels;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;

namespace eAcademy.ApiServices
{
    public class FeeServices
    {
        EacademyEntities db = new EacademyEntities();

        public IEnumerable<FeeList>getFeeDetail(int yearId,int classId,int secId,int StudentRegId)
        {
            FeeServices fee = new FeeServices();

            var list = ((from a in db.Fees
                        from b in db.FeeCategories
                        where a.FeeCategoryId == b.FeeCategoryId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId
                        select new 
                        {
                            FeeCategoryName = b.FeeCategoryName,
                            Amount = a.Amount,
                            Tax = a.Tax,
                            LastDate =  a.LastDate,
                            FeeCategoryId = a.FeeCategoryId,
                        }).ToList().Select(a => new FeeList
                        {
                            FeeCategoryName = a.FeeCategoryName,
                            Amount = a.Amount,
                            Tax = a.Tax,
                            LastDate = fee.status(yearId, classId, secId, StudentRegId, a.FeeCategoryId, a.LastDate),
                            category = "Annual"
                        }).ToList()).Union((from a in db.TermFees
                         from b in db.FeeCategories
                         where a.FeeCategoryId == b.FeeCategoryId &&
                         a.AcademicYearId == yearId &&
                         a.ClassId == classId
                         select new 
                         {
                             FeeCategoryName = b.FeeCategoryName,
                             Amount = a.Amount,
                             Tax = a.Tax,
                             LastDate =a.LastDate,
                             FeeCategoryId = a.FeeCategoryId,
                             TermId = a.TermId
                         }).ToList().Select(a => new FeeList
                         {
                             FeeCategoryName = a.FeeCategoryName + " / " + fee.getTermName(a.TermId),
                             Amount = a.Amount,
                             Tax = a.Tax,
                             LastDate = fee.TermFeeStatus(yearId, classId, secId, StudentRegId, a.FeeCategoryId, a.LastDate, a.TermId),
                             category = "Term"
                         }).ToList()).ToList();
            return list;
        }


        public string status(int yearId, int classId, int secId, int StudentRegId, int? feeCategoryId, DateTime? LastDate)
        {
            var row = (from b in db.FeeCollectionCategories
                       where b.AcademicYearId == yearId &&
                       b.ClassId == classId &&
                       b.SectionId == secId &&
                       b.StudentRegId == StudentRegId &&
                       b.FeeCategoryId == feeCategoryId &&
                       b.PaymentTypeId == 0
                       select b
                        ).FirstOrDefault();
             string stat;
            if(row != null)
            {
                stat = "Paid";
                return stat;
            }
            else
            {
                stat = LastDate.Value.ToString("dd/MM/yyyy");
                return stat;
            }
        }


        public string getTermName(int? termId)
        {
            string termName = (from a in db.Terms
                            where a.TermId == termId
                            select a).FirstOrDefault().TermName;
            return termName;
        }

        public string TermFeeStatus(int yearId, int classId, int secId, int StudentRegId, int? feeCategoryId, DateTime? LastDate,int? Termid)
        {
            var row = (from b in db.FeeCollectionCategories
                       where b.AcademicYearId == yearId &&
                       b.ClassId == classId &&
                       b.SectionId == secId &&
                       b.StudentRegId == StudentRegId &&
                       b.FeeCategoryId == feeCategoryId &&
                       b.PaymentTypeId == Termid
                       select b
                        ).FirstOrDefault();
            string stat;
            if (row != null)
            {
                stat = "Paid";
                return stat;
            }
            else
            {
                stat = LastDate.Value.ToString("dd/MM/yyyy");
                return stat;
            }
        }

    }
}