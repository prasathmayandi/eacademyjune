﻿using eAcademy.ApiModels;
using eAcademy.DataModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;

namespace eAcademy.ApiServices
{
    public class HomeworkServices
    {
        EacademyEntities db = new EacademyEntities();
        public IEnumerable<object> GetDateHomeworkList(int yearId, int classId, int secId,DateTime? hwdate)
        {
            var cc = hwdate.Value.Date;
            var list = (from a in db.HomeWork
                        from b in db.Subjects
                        where a.SubjectId == b.SubjectId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.DateOfWorkPosted == cc
                        select new
                        {
                            HomeWorkId = a.HomeWorkId,
                            DateOfWorkPosted = SqlFunctions.DateName("day", a.DateOfWorkPosted).Trim() + " " + SqlFunctions.DateName("month", a.DateOfWorkPosted).Remove(3) + " , " + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("hh", a.DateOfWorkPosted)).Trim() + ":" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mi", a.DateOfWorkPosted)).Trim(),
                            Title = a.HomeWork1,
                            Description = a.Descriptions,
                            DateToCompleteWork = SqlFunctions.DateName("day", a.DateToCompleteWork).Trim() + " " + SqlFunctions.DateName("month", a.DateToCompleteWork).Remove(3),
                            Subject = b.SubjectName,
                            File =a.HomeWorkFileName
                        }).ToList();
            return list;
        }      
    }
}