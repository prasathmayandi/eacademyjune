﻿using eAcademy.ApiModels;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace eAcademy.ApiServices
{
    public class PrimaryUserRegisterServices
    {
        EacademyEntities dc = new EacademyEntities();

        public string CheckValidUserId(string userId, string password)
        {
            var ValidUser = dc.PrimaryUserRegisters.Where(a => a.PrimaryUserId.Equals(userId)).FirstOrDefault();
            if (ValidUser != null)
            {
                var salt = ValidUser.Salt;
                var PasswordWithSalt = salt + password;
                string ConvertedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(PasswordWithSalt, "SHA1");
                var CheckValidPrimaryUser = (from a in dc.PrimaryUserRegisters where a.PrimaryUserId == userId && a.PrimaryUserPwd == ConvertedPassword select a).FirstOrDefault();
                if (CheckValidPrimaryUser != null)               
                {
                    int primaryUserRegId = CheckValidPrimaryUser.PrimaryUserRegisterId;
                    string token = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
                    TokenAuthSservices tok = new TokenAuthSservices();
                    tok.AddToken(token, primaryUserRegId);
                    return token;
                }
                else
                {
                    return "Incorrect password";
                }           
            }
            else
            {
                return "Incorrect username and password";
            }
        }
        public Int32 totalstudentstrength()
        {
            var ans = dc.Students.Where(q => q.StudentStatus == true).ToList().Count;
            return ans;
        }
        public Int32 totalwebLoginCount()
        {
            var ans = dc.UserLogs.Where(q => q.UserType == "Parent").Select(q => q.UserId).Distinct().ToList().Count;
            return ans;
        }
        public Int32 totalAppLoginCount()
        {
            var ans = dc.TokenAuths.Select(q => q.UserId).Distinct().ToList().Count;
            return ans;
        }
    }
}