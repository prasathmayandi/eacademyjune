﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.ApiServices
{
    public class AssignClassTeacherServices
    {
        EacademyEntities db = new EacademyEntities();

        public AssignClassTeacher getClassTeacher(int yearId, int classId, int secId)
        {
            var getClassIncharge = (from a in db.AssignClassTeachers where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.IsClassTeacher == "Yes" select a).FirstOrDefault();
            return getClassIncharge;
        }

        public IEnumerable<object> getClassWithSection(int passYearId)
        {
            var getList =  (from a in db.Classes
                           from b in db.Sections
                           where a.ClassId == b.ClassId
                             && a.Status==true
                           select new
                           {
                               ClassName = a.ClassType,
                               SecName = b.SectionName,
                               ClassId = a.ClassId,
                               SecId = b.SectionId
                           }).ToList().Select(c => new
                           {
                               ClassNameSecName = c.ClassName + " " + c.SecName,
                               ClassIdSecId = c.ClassId + " " + c.SecId
                           }).ToList();
            return getList;
        }



    }
}