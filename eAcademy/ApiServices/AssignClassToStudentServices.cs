﻿using eAcademy.ApiModels;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;

namespace eAcademy.ApiServices
{
    public class AssignClassToStudentServices
    {
        EacademyEntities db = new EacademyEntities();

        public AssignClassToStudent GetClassDetails(int StudentRegId) 
        { 
            var row =(from a in db.AssignClassToStudents
                      where a.StudentRegisterId ==StudentRegId && a.Status==true
                      select a).FirstOrDefault();
            return row;
        }
        public StudentDetails GetStudentDetails(int studentRegId)
        {
            var row = (from a in db.Students
                       from b in db.AssignClassToStudents
                       from c in db.PrimaryUserRegisters                       
                       from d in db.AdmissionTransactions
                       from e in db.Classes
                       from f in db.Sections
                       where a.StudentRegisterId == studentRegId && a.StudentRegisterId == b.StudentRegisterId && a.StudentRegisterId == d.StudentRegisterId && b.ClassId == e.ClassId && b.SectionId == f.SectionId && b.Status == true
                       select new StudentDetails {
                            studentRegId =a.StudentRegisterId,
                            firstname = a.FirstName,
                            lastname = a.LastName,
                            photo = a.Photo,
                            Class = e.ClassType,
                            section = f.SectionName,
                            dob = SqlFunctions.DateName("day", a.DOB) + "/" + SqlFunctions.StringConvert((double)a.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOB).Trim(),                      
                            primaryusername=c.PrimaryUserName,
                            addressline1=a.LocalAddress1,
                            addressline2=a.LocalAddress2,
                            city=a.LocalCity,
                            state=a.LocalState,
                            pincode = a.LocalPostalCode,
                            email = c.PrimaryUserEmail,
                            phone = a.Contact
                       }).FirstOrDefault();
            return row;
        }

    }
}