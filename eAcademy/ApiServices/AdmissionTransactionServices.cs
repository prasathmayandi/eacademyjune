﻿using eAcademy.ApiModels;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.ApiServices
{
    public class AdmissionTransactionServices
    {
        EacademyEntities dc = new EacademyEntities();

        public IEnumerable<StudentList> GetStudentList(int userId)
        {
           var list = (from a in dc.PrimaryUserRegisters
                        from b in dc.AdmissionTransactions
                        from c in dc.Students
                        from d in dc.AssignClassToStudents
                        from g in dc.AcademicYears
                        from e in dc.Classes
                        from f in dc.Sections
                        where a.PrimaryUserRegisterId == b.PrimaryUserRegisterId && b.StudentRegisterId == c.StudentRegisterId && b.StudentRegisterId == d.StudentRegisterId && a.PrimaryUserRegisterId == userId && d.Status == true && c.StudentStatus == true && d.ClassId == e.ClassId && d.SectionId == f.SectionId && c.AcademicyearId == g.AcademicYearId
                        select new StudentList
                        {
                            PrimaryUserFirstName = a.PrimaryUserName,
                            StudentFirstName = c.FirstName,
                            StudentLastName = c.LastName,
                            Class = e.ClassType,
                            Section = f.SectionName,
                            Photo = c.Photo,
                            StudentRegId = c.StudentRegisterId,
                            StudentId = c.StudentId
                        }).ToList();
          return list;
        }

    }
}