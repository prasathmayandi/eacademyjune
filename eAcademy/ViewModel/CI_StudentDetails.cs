﻿using System;
namespace eAcademy.ViewModel
{
    public class CI_StudentDetails
    {
        
        public string StudentId { get; set; }
        public string StudentRollNum { get; set; }
        public string StudentName { get; set; }
        public string StudentGender { get; set; }
        public DateTime? StudentDOB { get; set; }
        public string StudentBlood { get; set; }
        public string StudentEmail { get; set; }
        public string StudentCommunity { get; set; }
        public string StudentReligion { get; set; }
        public string StudentNationality { get; set; }
        public long? StudentContact { get; set; }
        public string StudentEmergencyContactPerson { get; set; }
        public long? StudentEmergencyContact { get; set; }
        public string StudentAddress { get; set; }
        public string StudentCity { get; set; }
        public string StudentState { get; set; }
        public string StudentCountry { get; set; }
        public string StudentHostel { get; set; }
        public string StudentTransport { get; set; }
        public DateTime? StudentDOR { get; set; }
        public string StudentPhoto { get; set; }
        public string StudentAchievement { get; set; }
        public string FatherName { get; set; }
        public DateTime? FatherDOB { get; set; }
        public string FatherEmail { get; set; }
        public string FatherQualification { get; set; }
        public string FatherOccupation { get; set; }
        public long? FatherContact { get; set; }
        public Nullable<long> FatherWorkNumber { get; set; }
        public string FatherPhoto { get; set; }
        public string MotherName { get; set; }
        public DateTime? MotherDOB { get; set; }
        public string MotherEmail { get; set; }
        public string MotherQualification { get; set; }
        public string MotherOccupation { get; set; }
        public long? MotherContact { get; set; }
        public Nullable<long> MotherWorkNumber { get; set; }
        public string MotherPhoto { get; set; }
        public string GuardianName { get; set; }
        public DateTime? GuardianDOB { get; set; }
        public string GuardianGender { get; set; }
        public string GuardianRelation { get; set; }
        public string GuardianEmail { get; set; }
        public string GuardianQualification { get; set; }
        public string GuardianOccupation { get; set; }
        public long? GuardianContact { get; set; }
        public Nullable<long> GuardianWorkNumber { get; set; }
        public string GuardianPhoto { get; set; }
        public string Community { get; set; }
        public string Religion { get; set; }
        public string Nationality { get; set; }
        public Nullable<long> Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public int? YeralyIncome { get; set; }
        public DateTime? DOR { get; set; }
        public long? Income { get; set; }
        
    }
}