﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class VM_SendEmailSingle
    {
        [Required]
        public string Descriptions { get; set; }
        [Required]
        public string StatusFlag { get; set; }
        [Required]
        public int StudentAdmissionId { get; set; }
        [Required]
        public string PrimaryuserEmail { get; set; }
        [Required]
        public string PrimaryUserName { get; set; }
        [Required]
        public string StudentName { get; set; }
        [Required]
        public int ApplyClass { get; set; }
        [Required]
        public string txt_ApplyClass { get; set; }
        [Required]
        public string txt_Academicyear { get; set; }
        [Required]
        public int AcyearId { get; set; }
        public string feepaylastdate { get; set; }
    }
}