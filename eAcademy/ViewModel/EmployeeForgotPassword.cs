﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class EmployeeForgotPassword
    {
         [Required]
        public string Employee_frpsw_UserName { get; set; }
        [Required]
        public string Captcha { get; set; } 
    }
}