﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class VM_EmployeeAttendanceValidation
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int AcademiYear { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectEmployeeCategory")]
        public int EmployeeType { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectEmployeeDesignation")]
        public int EmployeeDesignantion { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectReportType")]
        public int ReportType { get; set; }
        public DateTime? Dates { get; set; }
        public DateTime? FromDates { get; set; }
        public DateTime? Todates { get; set; }
        public int? month { get; set; }
    }
}