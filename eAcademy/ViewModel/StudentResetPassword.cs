﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class StudentResetPassword
    {
        [Required]
        public string student_reset_newpsw { get; set; }
        [Required]
        public string student_reset_confirmpsw { get; set; }
        [Required]
        public string Captcha { get; set; }
        [Required]
        public string uid { get; set; }
    }
}