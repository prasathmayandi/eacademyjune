﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class AchievementList
    {
        public int? Ach_allAcademicYearsA1 { get; set; }
        public Nullable<System.DateTime> txt_doa_search { get; set; }
        public Nullable<System.DateTime> DateOfAchievement { get; set; }
        public string Date { get; set; }
        public string Achievement { get; set; }
        public string Descriptions { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        public int Id { get; set; }
        public int AchievementId { get; set; }
        public string AchievementId1 { get; set; }
        
        public string EmployeeId { get; set; }
        
        [Required]
        public int AcademicYear { get; set; }
        
        public int AcademicYearId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
      
    }
    public class AddStudentAchievement
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int Ach_allAcademicYears { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectClass")]
        public int Ach_allClass { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectSection")]
        public int Ach_Section { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectStudent")]
        public int Ach_SectionStudent { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectDate")]
        public Nullable<System.DateTime> txt_doa { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterAchievement")]
        public string txt_achievement { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterDescription")]
        public string txt_description { get; set; }
    }
    public class EditStudentAchievement
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectDate")]
        public string txt_EditDoa { get; set; }
       [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterAchievement")]
        public string txt_EditAchievement { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterDescription")]
        public string txt_EditDescription { get; set; }
        [Required]
        public string AchievementEId { get; set; }      
    }
    
    public class AddFacultyAchievement
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int Ach_allAcademicYears { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectEmployee")]
        public int allEmployees { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectDate")]
        public Nullable<System.DateTime> txt_doa { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterAchievement")]
        public string txt_achievement { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterDescription")]
        public string txt_description { get; set; }
    }
    public class EditFacultyAchievement
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectDate")]
        public string txt_EditDoa { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterAchievement")]
        public string txt_EditAchievement { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterDescription")]
        public string txt_EditDescription { get; set; }
        [Required]
        public string AchievementEId { get; set; }
    }
    
    public class AddSchoolAchievement
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int Ach_allAcademicYears { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectDate")]
        public Nullable<System.DateTime> txt_doa { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterAchievement")]
        public string txt_achievement { get; set; }
         [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterDescription")]
        public string txt_description { get; set; }
         [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterPeopleInvolved")]
        public string txt_people { get; set; }
    }
    public class EditSchoolAchievement
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public string txt_EditDoa { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterAchievement")]
        public string txt_EditAchievement { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterDescription")]
        public string txt_EditDescription { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterPeopleInvolved")]
        public string txt_EditPeople { get; set; }
        [Required]
        public string AchievementEId { get; set; }
    }
}