﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class MI_HomeworkList
    {
        [Required]
        public int MI_HW_List_allAcademicYears { get; set; }
        [Required]
        public int MI_HW_List_allClass { get; set; }
        [Required]
        public string MI_HW_List_Section { get; set; }
        [Required]
        public string MI_HW_List_allSubjects { get; set; }
        [Required]
        public DateTime MI_HW_List_txt_fromDate { get; set; }
        [Required]
        public DateTime MI_HW_List_txt_toDate { get; set; }
    }
}