﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class TM_ClassTimetale
    {
        [Required]
        public int allAcademicYears { get; set; }
        [Required]
        public int allClass { get; set; }
        [Required]
        public int Section { get; set; }
        [Required]
        public int dayorder { get; set; }
    }
}