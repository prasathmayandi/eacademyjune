﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class VM_Biodata
    {
        [Required]
        public string StudentId {get;set;}
        [Required]
        public string UserName { get; set; }
    }
}