﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class ParentResetPassword
    {
        [Required]
        public string parent_reset_newpsw { get; set; }
        [Required]
        public string parent_reset_confirmpsw { get; set; }
        [Required]
        public string Captcha { get; set; }
        [Required]
        public string uid { get; set; }
    }
}