﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class Rpt_Circular
    {
         [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
         public int AcademicYear { get; set; }
         [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectReportType")]
         public int ReportType { get; set; }
    }
}