﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class R_StudentRemark
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int R_Remarks_allAcademicYears { get; set; }
       [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectClass")]
        public int R_Remarks_allClass { get; set; }
      [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectSection")]
        public string R_Remarks_Section { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectStudentName")]
        public string R_Remarks_students { get; set; }
         [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectFromdate")]
        public DateTime From_date { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectTodate")]
        public DateTime To_date { get; set; }
        public string UserName { get; set; }
        public DateTime? date { get; set; }
        public string Remark { get; set; }
        public string UserType { get; set; }
        public string Dates { get; set; }
        public string RemarkType { get; set; }
     
    }
}