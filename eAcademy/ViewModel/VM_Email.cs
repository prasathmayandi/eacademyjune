﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eAcademy.ViewModel
{
    public class VM_Email
    {
        [Required]
        public string[] RegId { get; set; }
        [Required]
        public int SelectCount { get; set; }
        public string Acyear { get; set; }
        public string Class { get; set; }
    }
}