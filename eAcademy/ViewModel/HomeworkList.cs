﻿using System;

namespace eAcademy.ViewModel
{
    public class HomeworkList
    {
        public int HomeWorkId { get; set; }
        public string HomeWork_Id { get; set; }
        public string PostedtDate { get; set; }
        public string LastDate { get; set; }
        public Nullable<int> EmployeeRegisterId { get; set; }
        public Nullable<int> AcademicYearId { get; set; }
        public Nullable<int> ClassId { get; set; }
        public Nullable<int> SectionId { get; set; }
        public Nullable<int> SubjectId { get; set; }
        public Nullable<System.DateTime> DateOfWorkPosted { get; set; }
        public string HomeWork1 { get; set; }
        public string Descriptions { get; set; }
        public string HomeWorkFileName { get; set; }
        public Nullable<System.DateTime> DateToCompleteWork { get; set; }
        public Nullable<bool> HomeWorkStatus { get; set; }
        public DateTime? MI_HW_List_txt_fromDate { get; set; }
        public DateTime? MI_HW_List_txt_toDate { get; set; }
        public string HomeWorkWithFileName { get; set; }
    }

    public class AssignmentList
    {
        public int AssignmentId { get; set; }
        public string Assignment_Id { get; set; }
        public string PostedtDate { get; set; }
        public string LastDate { get; set; }
        public Nullable<int> EmployeeRegisterId { get; set; }
        public Nullable<int> AcademicYearId { get; set; }
        public Nullable<int> ClassId { get; set; }
        public Nullable<int> SectionId { get; set; }
        public Nullable<int> SubjectId { get; set; }
        public Nullable<System.DateTime> DateOfWorkPosted { get; set; }
        public string Assignment { get; set; }
        public string Descriptions { get; set; }
        public string AssignmentFileName { get; set; }
        public Nullable<System.DateTime> DateToCompleteWork { get; set; }
        public Nullable<bool> Status { get; set; }
        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; }
        public string AssignmentAttachFileName { get; set; }
    }
}