﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class R_ExamResult
    {
        [Required]
        public int? R_ExamResult_allAcademicYears { get; set; }
        [Required]
        public int? R_ExamResult_allClass { get; set; }
        [Required]
        public string R_ExamResult_Section { get; set; }
        [Required]
        public int R_ExamResult_allExams { get; set; }
        [Required]
        public string sort { get; set; }
        public string RollNumber { get; set; }
        public string StudentName { get; set; }
        public int? Total { get; set; }
        public string Result { get; set; }
        public int? Ranks { get; set; }
        public string Comment { get; set; }
        public string StudentId { get; set; }
        public string Rank { get; set; }
    }

}