﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class VM_UpdateApplicationStatus
    {
        [Required(ErrorMessage = "Please Enter Description")]
        public string Descriptions { get; set; }
        [Required(ErrorMessage = "Please Slecte Statusflag")]
        public string StatusFlag { get; set; }
        [Required]
        public int Stuadmissionid { get; set; }
        [Required]
        public int AcademicYearId { get; set; }
        [Required]
        public int Classid { get; set; }
        public string FeePaidLastDate { get; set; }
    }
}