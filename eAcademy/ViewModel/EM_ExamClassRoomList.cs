﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class EM_ExamClassRoomList
    {
        [Required]
        public int? EM_allAcademicYears_List { get; set; }
        [Required]
        public int? EM_allClass_List { get; set; }
        [Required]
        public string EM_Section_List { get; set; }
        [Required]
        public int EM_allExams_List { get; set; }
        [Required]
        public int EM_allSubjects_List { get; set; }
        public string StartRollNumber { get; set; }
        public string EndRollNumber { get; set; }
        public string RoomNumber { get; set; }
        public string ClassRoomId { get; set; }
        public string ExamDate { get; set; }
    }
}