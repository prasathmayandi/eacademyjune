﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class AM_StudentAttendanceReport
    {
        [Required]
        public int Att_allAcademicYears1 { get; set; }
        [Required]
        public int Terms1 { get; set; }
        [Required]
        public int Att_allClass1 { get; set; }
        [Required]
        public int Att_Section1 { get; set; }
        [Required]
        public int Att_SectionStudent { get; set; }
    }
}