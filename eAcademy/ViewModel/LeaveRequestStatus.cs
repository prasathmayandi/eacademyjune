﻿using System;
namespace eAcademy.ViewModel
{
    public class LeaveRequestStatus
    {
        public Nullable<System.DateTime> FromDate { get; set; }
        public Nullable<System.DateTime> ToDate { get; set; }
        public Nullable<int> NumberOfDays { get; set; }
        public string LeaveReason { get; set; }
        public string Status { get; set; }
        public string ClassIncharge { get; set; }
        public string Request_Id { get; set; }
        public string From_Date { get; set; }
        public string To_Date { get; set; }
        public int? RequestId { get; set; }
    }
}