﻿namespace eAcademy.ViewModel
{
    public class StaffTimetableList
    {
        public int? Period { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        public string Subject { get; set; }
        public int? ClassId { get; set; }
        public int? SectionId { get; set; }
        public int? SubjectId { get; set; }
        public string ClassId1 { get; set; }
        public string SectionId1 { get; set; }
        public string SubjectId1 { get; set; }
        public string Period1 { get; set; }
        public int? Day { get; set; }
    }
}