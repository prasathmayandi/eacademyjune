﻿namespace eAcademy.ViewModel
{
    public class ProgressCard
    {
        public string Subject { get; set; }
        
        public int? Mark { get; set; }
        public int? MinMark { get; set; }
        public int? MaxMark { get; set; }
        public int? Subject_Id { get; set; }
        public int markTotalId { get; set; }
        public int Totalmark { get; set; }
        public string Grade { get; set; }
        public int? sid { get; set; }
        public string name { get; set; }
        public int? mark { get; set; }
        public int? StudentRegId { get; set; }
        public string Student_RegId { get; set; }
        public string RollNumber { get; set; }
        public string StudentName { get; set; }
    }
}