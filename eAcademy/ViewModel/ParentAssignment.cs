﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class ParentAssignment
    {
        [Required]
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public DateTime SM_FromDate { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public DateTime SM_ToDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> AssignmentPostedDate { get; set; }
        public string Subject { get; set; }
        public string Assignment1 { get; set; }
        public string Descriptions { get; set; }
        public string AssignmentFileName { get; set; }
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateOfSubmission { get; set; }
        public int AssignmentId { get; set; }
    }
}