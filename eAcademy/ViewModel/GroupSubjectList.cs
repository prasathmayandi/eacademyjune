﻿using System;
using eAcademy.Models;
using eAcademy.HelperClass;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eAcademy.ViewModel
{
    public class GroupSubjectList
    {
        public int  SubjectId { get; set; }
        public string Subject { get; set; }

        //public int SubjectId { get; set; }
        //public string Subject { get; set; }
        //public string Non_Subject { get; set; }
        //public Boolean? Status { get; set; }
        //public Boolean? GroupSubjectStatus { get; set; }
        //public string SubId { get; set; }

        
        
        
    }
    public class SaveGroupSubject
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "GroupSubjectName_is_required")]
        [StringLength(30, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "GroupSubjectName_should_contain_2_characters_can_not_exceed_30_characters", MinimumLength = 2)]      
        public string GroupName { get; set; }
        public int  GroupId { get; set; }
        public int?[] GroupConfigId { get; set; }
       
        public string[] GroupSubjectId { get; set; }
        public string[] GroupSubjectStatus { get; set; }
        public string GroupStatus { get; set; }
        

    }
    public class EditGroupSubject
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "GroupSubjectName_is_required")]
        [StringLength(30, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "GroupSubjectName_should_contain_2_characters_can_not_exceed_30_characters", MinimumLength = 2)]      
        public string GroupName { get; set; }
        public int GroupId { get; set; }
        public string[] GroupConfigId { get; set; }
        public int[] GroupSubjectId { get; set; }
        public string[] GroupSubjectStatus { get; set; }
        public string GroupStatus { get; set; }


    }

    public class GroupList
    {
        public string GroupName { get; set; }
        public int GroupSubjectId { get; set; }
        public Boolean? Status { get; set; }
        public int SubjectId { get; set; }
        public int? SubId { get; set; }
        public string SubjectName{ get; set; }
        public int SubjectConfigId { get; set; }
        
    }
}