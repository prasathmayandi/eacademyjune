﻿ using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class CI_AttendanceReport
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]  
        public int CI_allAcademicYears { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectClass")] 
        public string CI_ClassWithSection { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectStudent")] 
        public string CI_allStudents { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectFromdate")] 
        public DateTime txt_FromDate { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectTodate")] 
        public DateTime txt_ToDate { get; set; }
    }
}