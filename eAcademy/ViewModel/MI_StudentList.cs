﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class MI_StudentList
    {
        [Required]
        public int MI_allAcademicYears { get; set; }
        [Required]
        public int MI_allClass { get; set; }
        [Required]
        public string MI_Section { get; set; }
    }
}