﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class AssignSubjectToSection1
    {
        [Required]
        public int CM_SS_allAcademicYears { get; set; }
        [Required]
        public int CM_SS_allClass { get; set; }
        [Required]
        public int CM_SS_Section { get; set; }
        [Required]
        public int CM_SS_Subjects { get; set; }
    }
}