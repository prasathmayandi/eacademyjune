﻿using System.ComponentModel.DataAnnotations;
using eAcademy.Resources.AcademicReport;
namespace eAcademy.ViewModel
{
    public class Rpt_FunctionTimeTable
    {
       // [Required(ErrorMessageResourceType = typeof(FuntionTimetable), ErrorMessageResourceName = "AcademicYear")]
          [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
          public int AcademicYear { get; set; }
    }
}