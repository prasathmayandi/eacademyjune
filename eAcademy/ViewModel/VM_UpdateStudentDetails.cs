﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class VM_UpdateStudentDetails
    {
        public int? Stuadmissionid { get; set; }
        [Required(ErrorMessage = "Student FirstName is Required")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "First name cannot exceed 30 characters")]
        public string StuFirstName { get; set; }
        [StringLength(30, ErrorMessage = "Middle name cannot exceed 30 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string StuMiddlename { get; set; }
        [Required(ErrorMessage = "Student LastName is Required")]
        [StringLength(30, ErrorMessage = "Student last name cannot exceed 30 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string StuLastname { get; set; }
        [Required(ErrorMessage = "Gender is Required")]
        public string Gender { get; set; }
        [Required(ErrorMessage = "DateofBirth is Required")]
        public DateTime? DOB { get; set; }
        [StringLength(30, ErrorMessage = "Student placeofbirth cannot exceed 30 characters")]
        public string PlaceOfBirth { get; set; }
        [Required(ErrorMessage = "Comunity is Required")]
        public int? Community { get; set; }
        [Required(ErrorMessage = "Religion is Required")]
        [StringLength(30, ErrorMessage = "Student religion cannot exceed 30 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string Religion { get; set; }
        [Required(ErrorMessage = "Nationality is Required")]
        [StringLength(30, ErrorMessage = "Student nationality cannot exceed 30 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string Nationality { get; set; }
        public int? AcademicyearId { get; set; }
        public int? AdmissionClass { get; set; }
        [Required]
        public string PrimaryUser { get; set; }
        [StringLength(30, ErrorMessage = "Father first name cannot exceed 30 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string FatherFirstName { get; set; }
        [StringLength(50, ErrorMessage = "Father occupation cannot exceed 50 charracters")]
        public string FatherOccupation { get; set; }
        public long? FatherMobileNo { get; set; }
        [StringLength(70, ErrorMessage = "Father email cannot exceed 70 charracters")]
        public string FatherEmail { get; set; }
        [StringLength(30, ErrorMessage = "Mother first name cannot exceed 30 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string MotherFirstname { get; set; }
        [StringLength(50, ErrorMessage = "Father occupation cannot exceed 50 charracters")]
        public string MotherOccupation { get; set; }
        public long? MotherMobileNo { get; set; }
        [StringLength(70, ErrorMessage = "Father enail cannot exceed 70 charracters")]
        public string MotherEmail { get; set; }
        [StringLength(30, ErrorMessage = "Father last name cannot exceed 30 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string FatherLastName { get; set; }
        [StringLength(50, ErrorMessage = "Father qualification cannot exceed 50 charracters")]
        public string FatherQualification { get; set; }
        public DateTime? FatherDOB { get; set; }
        [StringLength(30, ErrorMessage = "Mother last name cannot exceed 30 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string MotherLastName { get; set; }
        public DateTime? MotherDOB { get; set; }
        [StringLength(50, ErrorMessage = "Mother qualification cannot exceed 50 charracters")]
        public string MotherQualification { get; set; }
        [Required(ErrorMessage = "GuardianRequired is Required")]
        public string GuardianRequried { get; set; }
        public long? TotalIncome { get; set; }
        [StringLength(30, ErrorMessage = "Father last name cannot exceed 30 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string GuardianLastName { get; set; }
        [StringLength(30, ErrorMessage = "Guardian first name cannot exceed 30 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string GuardianFirstname { get; set; }
        public string GuardianGender { get; set; }
        public DateTime? GuardianDOB { get; set; }
        [StringLength(50, ErrorMessage = "Guardian relationship to child cannot exceed 50 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string GuRelationshipToChild { get; set; }
        [StringLength(50, ErrorMessage = "Guardian first namequalification cannot exceed 50 charracters")]
        public string GuardianQualification { get; set; }
        [StringLength(50, ErrorMessage = "Guardian occupation cannot exceed 50 charracters")]
        public string GuardianOccupation { get; set; }
        public long? GuardianMobileNo { get; set; }
        [StringLength(70, ErrorMessage = "Guardian email cannot exceed 70 charracters")]
        public string GuardianEmail { get; set; }
        public int? GuardianIncome { get; set; }
        public int? NoOfSbling { get; set; }
        public string primaryUserEmail { get; set; }
        [Required(ErrorMessage = "Bloodgroup is Required")]
        public int? BloodGroup { get; set; }
        [Required(ErrorMessage = "Local AddressLine1 is Required")]
        [StringLength(200, ErrorMessage = "Address1 cannot exceed 200 characters")]
        public string LocAddress1 { get; set; }
        [StringLength(200, ErrorMessage = "Address2 cannot exceed 200 characters")]
        public string LocAddress2 { get; set; }
        [Required(ErrorMessage = "LocalCity is Required")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(50, ErrorMessage = "Local city cannot exceed 50 characters")]
        public string LocCity { get; set; }
        [Required(ErrorMessage = "LocalState is Required")]
        [StringLength(50, ErrorMessage = "State cannot exceed 50 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string LocState { get; set; }
        [Required(ErrorMessage = "LocalCountry is Required")]
        public string LocCountry { get; set; }
        [Required(ErrorMessage = "LocalPostelcode is Required")]
        public long? LocPostelcode { get; set; }
        [Required(ErrorMessage = "Distance is Required")]
        public int? Distance { get; set; }
        [Required(ErrorMessage = "Permenant address1 is Required")]
        [StringLength(200, ErrorMessage = "Permenant address1 cannot exceed 200 characters")]
        public string PerAddress1 { get; set; }
        [StringLength(200, ErrorMessage = "Permenant address2 cannot exceed 200 characters")]
        public string PerAddress2 { get; set; }
        [Required(ErrorMessage = "Permenant city is Required")]
        [StringLength(50, ErrorMessage = "Permenant city cannot exceed 50 characters")]
        public string PerCity { get; set; }
        [Required(ErrorMessage = "Permenant state  is Required")]
        [StringLength(50, ErrorMessage = "Permenant state cannot exceed 50 characters")]
        public string PerState { get; set; }
        [Required(ErrorMessage = "Permenant country is Required")]
        public string PerCountry { get; set; }
        [Required(ErrorMessage = "Permenant postelcode is Required")]
        public long? PerPostelcode { get; set; }
        //[Required(ErrorMessage = "Companyname is Required")]
        [StringLength(150, ErrorMessage = "Company name cannot exceed 150 charracters")]
        public string UserCompanyname { get; set; }
        //[Required(ErrorMessage = "Companyaddress1 is Required")]
        [StringLength(200, ErrorMessage = "Company Address1 cannot exceed 200 charracters")]
        public string CompanyAddress1 { get; set; }
        [StringLength(200, ErrorMessage = "Company Address2 cannot exceed 200 charracters")]
        public string CompanyAddress2 { get; set; }
        //[Required(ErrorMessage = "Companycity is Required")]
        [StringLength(50, ErrorMessage = "Company city cannot exceed 50 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string CompanyCity { get; set; }
        //[Required(ErrorMessage = "Companystate is Required")]
        [StringLength(50, ErrorMessage = "Company state cannot exceed 50 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string CompanyState { get; set; }
        //[Required(ErrorMessage = "Companycountry is Required")]
        public string CompanyCountry { get; set; }
        //[Required(ErrorMessage = "Companypostelcode is Required")]
        public long? CompanyPostelcode { get; set; }
        //[Required(ErrorMessage = "Companycontactnumber is Required")]
        public long? CompanyContact { get; set; }
        [StringLength(70, ErrorMessage = "Student email cannot exceed 70 characters")]
        public string Email { get; set; }
        public long? Contact { get; set; }
        [StringLength(20, ErrorMessage = "Hight cannot exceed 20 characters")]
        public string Height { get; set; }
        [StringLength(20, ErrorMessage = "Weight cannot exceed 20 characters")]
        public string Weights { get; set; }
        [StringLength(50, ErrorMessage = "Identification Mark cannot exceed 50 characters")]
        public string IdentificationMark { get; set; }
        [Required(ErrorMessage = "Emergency contact prson name is Required")]
        [StringLength(30, ErrorMessage = "Emergency contact person name cannot exceed 30 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string EmergencyContactPersonName { get; set; }
        [Required(ErrorMessage = "Emergency contact person contact is Required")]
        public long? EmergencyContactNumber { get; set; }
        [Required(ErrorMessage = "Emergency contact person relationship is Required")]
        [StringLength(50, ErrorMessage = "Emergency contact person relationship cannot exceed 50 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string ContactPersonRelationship { get; set; }
        [StringLength(150, ErrorMessage = "Previous school name cannot exceed 150 characters")]
        public string PreSchool { get; set; }
        //[StringLength(50, ErrorMessage = "Previous school medium cannot exceed 50 characters")]
        public int? PreMedium { get; set; }
        //[StringLength(50, ErrorMessage = "Previous school class cannot exceed 50 characters")]
        public string PreClass { get; set; }
        [StringLength(10, ErrorMessage = "Previous school mark cannot exceed 10 characters")]
        public string PreMarks { get; set; }
        public DateTime? PreFromDate1 { get; set; }
        public DateTime? PreToDate { get; set; }
        [Required]
        public string WorkSameSchool { get; set; }
        public string OldPrimaryEmail { get; set; }
        public int userconfirmation { get; set; }
        public int? EmployeeDesignationId { get; set; }
        public string EmployeeId { get; set; }
        public bool? hostel_required { get; set; }
        public bool? transport_required { get; set; } 
    }
}