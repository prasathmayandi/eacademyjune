﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class VM_PayFees
    {
       [Required]
        public int NoOfFees { get; set; }
        [Required]
        public string[] feecategoryid { get; set; }
        [Required]
        public string[] paymenttypeid { get; set; }
        [Required]
        public string[] amount { get; set; }
        [Required]
        public string[] servicetax { get; set; }
        [Required]
        public string[] total { get; set; }
        [Required]
        public decimal TotalAmount { get; set; }
        [Required]
        public int ApplicationId { get; set; }
        [Required]
        public int AcademicYearId { get; set; }
        [Required]
        public int ClassId { get; set; }
        public int DestinationId { get; set; }

        public int PickPointId { get; set; }
        public int AccommodationCategoryId { get; set; }
        public int AccommodationSubCategoryId { get; set; }
        public int FoodCategoryId { get; set; }
        public int FoodSubCategoryId { get; set; }
        
      
    }
}