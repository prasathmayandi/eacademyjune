﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class TM_ExamTimetable
    {
        [Required]
        public int allAcademicYears { get; set; }
        [Required]
        public int allExams { get; set; }
        [Required]
        public int allClass { get; set; }
        [Required]
        public int Section { get; set; }
    }
}