﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class VM_AdmissionFeeRefund
    {
        [Required(ErrorMessage="Enter admission id")]
        public int AdmissionId { get; set; }
        [Required(ErrorMessage="Enter user email id")]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$",
        ErrorMessage = "Please Enter Correct Email Address")]
        public string EmailId { get; set; }
    }
}