﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eAcademy.ViewModel
{
    public class AddResourceLanguage
    {
        public int ResourceLanguageId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Language_Display_Name_is_Required")]     
        public string LanguageDisplayname { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Culture_Name_is_Required")]
        public string LanguageCultureName { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Culture_Code_is_Required")]
        public string LanguageCultureCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Country_Code_is_Required")]      
        public string LanguageCountryCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Status_is_Required")]    
        public bool? status { get; set; }
    }
}