﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class TM_CreateExamTimetable
    {
        [Required]
        public int allAcademicYears { get; set; }
        [Required]
        public int allClass { get; set; }
        [Required]
        public int Section { get; set; }
        [Required]
        public int allSubjects { get; set; }
        [Required]
        public int allExams { get; set; }
        [Required]
        public TimeSpan txt_StartTime { get; set; }
        [Required]
        public TimeSpan txt_EndTime { get; set; }
        [Required]
        public DateTime txt_ExamDate { get; set; }
    }
}