﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class Stu_ProgressCard
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]  
        public int stu_PC_allAcademicYears { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectExam")] 
        public int stu_PC_allExams { get; set; }
    }
}