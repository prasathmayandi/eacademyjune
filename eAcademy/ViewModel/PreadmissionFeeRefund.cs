﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.ViewModel
{
    public class PreadmissionFeeRefund
    {
        public int AcademicYearId { get; set; }
        public int ClassId { get; set; }
        public int ApplicationId { get; set; }
        public Decimal TotalAmount { get; set; }
        public Decimal AmountRefunded { get; set; }
    }
}