﻿using System;
namespace eAcademy.ViewModel
{
    public class MI_Queries
    {
        public int MI_Queries_allAcademicYears { get; set; }
        public string MI_Queries_ClassWithSection { get; set; }
        public int MI_Queries_allSubjects1 { get; set; }
        public DateTime R_Query_FromDate { get; set; }
        public DateTime R_Query_ToDate { get; set; }
        public string Date { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string RollNumber { get; set; }
        public string Query { get; set; }
        public string ParentName { get; set; }
        public string SentBy { get; set; }
        public string classs { get; set; }
        public string Query_Id { get; set; }
        public string Subject { get; set; }
        public string Faculty { get; set; }
        public string QueryFrom { get; set; }
        public string QueryFile { get; set; }
        public string Reply { get; set; }
        public string ReplyFile { get; set; }
        public string subject { get; set; }
        public string FacultyName { get; set; }
        public int FacultyId { get; set; }
        public string LastName { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Sec { get; set; }
        public string staffqueryfilename { get; set; }
        public string staffqueryreplyfilename { get; set; }
        public string parentqueryattachementfilename { get; set; }
        public string parentreplyqueryattachementfilename { get; set; }
        public string Studentqueryattachementfilename { get; set; }
        public string Studentreplyqueryattachementfilename { get; set; }
        public int?  academicYearId { get; set; }
        

    }
}