﻿using eAcademy.Resources.OnlineRegister;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class VM_ResetPassword
    {
        [Required(ErrorMessageResourceType = typeof(RegisterResource), ErrorMessageResourceName = "passwordsRequired")]
        [RegularExpression("(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{3,20})$", ErrorMessage = "Password should contains atleast one charracter ,one numeric digit ,")]
        [MinLength(3, ErrorMessage = "should enter 3 characters")]
        [MaxLength(20, ErrorMessage = "should not exceed 20 characters")]
        public string passwords { get; set; }
        [Required(ErrorMessageResourceType = typeof(RegisterResource), ErrorMessageResourceName = "repasswordsRequired")]
        [RegularExpression("(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{3,20})$", ErrorMessage = "Re-password should contains atleast one charracter ,one numeric digit ,")]
        [MinLength(3, ErrorMessage = "should enter 3 characters")]
        [MaxLength(20, ErrorMessage = "should not exceed 20 characters")]
        public string repasswords { get; set; }

        [Required]
        public string Captcha { get; set; }

        public long? otp { get; set; }
        public string uid { get; set; }
    }
}