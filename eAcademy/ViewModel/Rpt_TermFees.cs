﻿using eAcademy.Resources.AcademicReport;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class Rpt_TermFees
    {
        //[Required(ErrorMessageResourceType = typeof(Fee), ErrorMessageResourceName = "AcademicYear")]
        //public int AcademicYear { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Fee), ErrorMessageResourceName = "Class")]
        //public int Class { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Fee), ErrorMessageResourceName = "Rpt_Section")]
        //public int Rpt_Section { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Fee), ErrorMessageResourceName = "PaymentType")]
        //public int PaymentType { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Fee), ErrorMessageResourceName = "Feecategory")]
        //public int Feecategory { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Fee), ErrorMessageResourceName = "FeeParticular")]
        //public int FeeParticular { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Fee), ErrorMessageResourceName = "Termtype")]
        //public int Termtype { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int AcademicYear { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectClass")]
        public int Class { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectSection")]
        public int Rpt_Section { get; set; }
         [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectPaymentType")]
        public int PaymentType { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectFeeCategory")]
        public int Feecategory { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_select_fee_particular")]
        public int FeeParticular { get; set; }       
        public int Termtype { get; set; }
    }
}