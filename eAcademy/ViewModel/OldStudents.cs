﻿namespace eAcademy.ViewModel
{
    public class OldStudents
    {
        public int StudentRegisterId { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public long? Contact { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        public string Reason { get; set; }
        public string TC { get; set; }
        public string CC { get; set; }
    }
}