﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class VM_UserConfirmation
    {
        public string ExitUserEmail{get;set;}
        [Required(ErrorMessage="Statusflag is required")]
        public string StatusFlag{get;set;}
        [Required(ErrorMessage = "Requestsend is required")]
        public string RequestSend { get; set; }
        public string Descriptions { get; set; }
        public string OfflineApplicationId { get; set; }
        public long? ExitUserMobileNo { get; set; }
        public string UserName { get; set; }
    }
}