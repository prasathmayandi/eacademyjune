﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class VM_SendEmailFlag
    {
        [Required]
        public string[] RegId { get; set; }
        [Required]
        public int SelectCount { get; set; }
        [Required]
        public int AcademicYearId { get; set; }
        [Required]
        public int Classid { get; set; }
        
        public string description { get; set; }
        public string FeePaidLastDate { get; set; }
    }
}