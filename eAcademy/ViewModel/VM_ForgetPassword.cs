﻿using eAcademy.Resources.OnlineRegister;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class VM_ForgetPassword
    {
        [Required(ErrorMessageResourceType = typeof(RegisterResource), ErrorMessageResourceName = "UsernameRequired")]
       // [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[-+_!@#$%^&*.,?])[A-Za-z0-9_@./#&+-]{0,7}$", ErrorMessage = "enter correct")]
        public string Username { get; set; }
        [Required(ErrorMessageResourceType = typeof(RegisterResource), ErrorMessageResourceName = "CaptchaRequired")]
        public string Captcha { get; set; }
        public string UsernameType { get; set; }
    }
}