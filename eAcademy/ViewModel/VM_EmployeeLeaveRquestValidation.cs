﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class VM_EmployeeLeaveRquestValidation
    {
        [Required]
        public DateTime Fromdate { get; set; }
        [Required]
        public DateTime Todate { get; set; }
    }
}