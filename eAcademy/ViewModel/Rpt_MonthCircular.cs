﻿using System.ComponentModel.DataAnnotations;
using eAcademy.Resources.AcademicReport;

namespace eAcademy.ViewModel
{
    public class Rpt_MonthCircular
    {
        [Required(ErrorMessageResourceType = typeof(Circular), ErrorMessageResourceName = "AcademicYearRequired")]
        public int AcademicYear { get; set; }
        [Required(ErrorMessageResourceType = typeof(Circular), ErrorMessageResourceName = "ReportTypeRequired")]
        public int ReportType { get; set; }
          [Required(ErrorMessageResourceType = typeof(Circular), ErrorMessageResourceName = "MonthRequired")]
        public int month { get; set; }
    }
}