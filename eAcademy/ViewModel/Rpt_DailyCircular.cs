﻿using System;
using System.ComponentModel.DataAnnotations;
using eAcademy.Resources.AcademicReport;
namespace eAcademy.Models
{
    public class Rpt_DailyCircular
    {
        [Required(ErrorMessageResourceType = typeof(Circular), ErrorMessageResourceName = "AcademicYearRequired")]
        public int AcademicYear { get; set; }
        [Required(ErrorMessageResourceType = typeof(Circular), ErrorMessageResourceName = "ReportTypeRequired")]
        public int ReportType { get; set; }
        [Required(ErrorMessageResourceType = typeof(Circular), ErrorMessageResourceName = "DailyDateRequired")]
        public DateTime Dates { get; set; }
        
    }
}