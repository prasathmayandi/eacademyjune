﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class ListSubjectToSection
    {
        [Required]
        public int SS_allAcademicYears { get; set; }
        [Required]
        public int SS_allClass { get; set; }
        [Required]
        public string SS_Section { get; set; }
        public string Subject { get; set; }
        public int AssignSubjectToSectionId { get; set; }
    }
}