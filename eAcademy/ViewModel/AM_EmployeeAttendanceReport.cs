﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class AM_EmployeeAttendanceReport
    {
        [Required]
        public int allEmployees { get; set; }
        [Required]
        public DateTime txt_FromDate { get; set; }
        [Required]
        public DateTime txt_ToDate { get; set; }
    }
}