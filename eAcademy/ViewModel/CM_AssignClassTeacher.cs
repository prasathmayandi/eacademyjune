﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class CM_AssignClassTeacher
    {
        [Required(ErrorMessage = "Please select year")]
        public string allAcademicYears { get; set; }
        [Required(ErrorMessage = "Please select class")]
        public int allClass { get; set; }
        [Required(ErrorMessage = "Please select section")]
        public string Section { get; set; }
        [Required(ErrorMessage = "Please select Faculty")]
        public int allFaculties { get; set; }
    }
}