﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class ParentHomework
    {
        [Required]
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public DateTime SM_HomeworkDate { get; set; }
        public int HomeWorkId { get; set; }
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateOfWorkPosted { get; set; }
        public string Subject { get; set; }
        public string HomeWork1 { get; set; }
        public string Descriptions { get; set; }
        public string HomeWorkFileName { get; set; }
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateToCompleteWork { get; set; }
    }
}