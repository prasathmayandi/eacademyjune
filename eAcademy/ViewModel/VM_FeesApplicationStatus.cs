﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class VM_FeesApplicationStatus
    {
        [Required(ErrorMessage="Please enter application id")]
        public int ApplicationID { get; set; }
        [Required(ErrorMessage = "Please enter academic year")]
        public int AcademicYearId { get; set; }
        [Required(ErrorMessage = "Please enter class")]
        public int AdmissionClassId { get; set; }
    }
}