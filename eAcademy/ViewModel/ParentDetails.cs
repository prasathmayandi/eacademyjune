﻿using System;
namespace eAcademy.ViewModel
{
    public class ParentDetails
    {
        public string ParentName { get; set; }
        public string Email { get; set; }
        public string Community { get; set; }
        public string Religion { get; set; }
        public string Nationality { get; set; }
        public Nullable<long> Phone { get; set; }
        public long Contact { get; set; }
        public Nullable<long> WorkNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Qualification { get; set; }
        public string Occupation { get; set; }
        public int YeralyIncome { get; set; }
        public System.DateTime DOR { get; set; }
        public string Gender { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
    }
}