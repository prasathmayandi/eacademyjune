﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class P_ParentProfile
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public long? Mobile { get; set; }
        public long? WorkNumber { get; set; }
        [Required]
        public string Qualification { get; set; }
        public string Occupation { get; set; }
        [Required]
        public int? YearlyIncome { get; set; }
        public string Photo { get; set; }
        public string Religion { get; set; }
        public string Nationality { get; set; }
        public long? Phone { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string State { get; set; }
        [Required]
        public string Parent_allCountries { get; set; }
        public string UserId { get; set; }
    }
}