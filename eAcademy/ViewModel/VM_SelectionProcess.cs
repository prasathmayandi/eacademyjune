﻿using eAcademy.Resources.Admission;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class VM_SelectionProcess
    {
        [Required(ErrorMessageResourceType = typeof(SelectionProcess), ErrorMessageResourceName = "AcademicYear")]
        public int AcademicYear { get; set; }
        [Required(ErrorMessageResourceType = typeof(SelectionProcess), ErrorMessageResourceName = "Class")]
        public int Class { get; set; }
    }
}