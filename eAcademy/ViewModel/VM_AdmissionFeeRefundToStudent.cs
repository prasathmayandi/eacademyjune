﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class VM_AdmissionFeeRefundToStudent
    {
        //[Required]
        public int NoOfFees { get; set; }
        //[Required]
        public string[] CollectionCategoryId { get; set; }
        //[Required]
        public string ReceivedBy { get; set; }
        //[Required]
        public string reason { get; set; }
        public Decimal TotalAmount { get; set; }
        public Decimal AmountRefunded { get; set; }
        public int FeeCollectionidstudent { get; set; }
        public int StudentApplicationid { get; set; }
    }
}