﻿using System;

namespace eAcademy.ViewModel
{
    public class RankList
    {
        public string MarkTotalId { get; set; }
        public Nullable<int> StudentRegisterId { get; set; }
        public int? TotalMark { get; set; }
        public Nullable<int> StudentRank { get; set; }
        public string Result { get; set; }
        public string Comment { get; set; }
        public Nullable<int> EmployeeRegisterId { get; set; }
        public string StudentName { get; set; }
        public string MarkTotal_Id { get; set; }
    }
}