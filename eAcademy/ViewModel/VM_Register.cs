﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class VM_Register
    {
        public string Username { get; set; }
       
        public string passwords { get; set; }
        
        public string repasswords { get; set; }

        public string uid { get; set; }

        [Required]
        public string Captcha { get; set; }
       

        public string StudentId { get; set;}
        public string ParentId { get; set; }
        public string UsernameType { get; set; }
    }
}