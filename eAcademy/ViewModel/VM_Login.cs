﻿using eAcademy.Resources.OnlineRegister;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class VM_Login
    {
        [Required(ErrorMessageResourceType = typeof(LoginResource), ErrorMessageResourceName = "UsernameRequired")]
        public string Username { get; set; }
        [Required(ErrorMessageResourceType = typeof(LoginResource), ErrorMessageResourceName = "passwordsRequired")]
        [RegularExpression("(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{3,20})$", ErrorMessage = "password should contains atleast one charracter ,one numeric digit ,")]
        [MinLength(3, ErrorMessage = "should enter 3 characters")]
        [MaxLength(20, ErrorMessage = "should not exceed 20 characters")]
        public string passwords { get; set; }
        [Required(ErrorMessageResourceType = typeof(LoginResource), ErrorMessageResourceName = "CaptchaRequired")]
        public string Captcha { get; set; }
        public string UsernameType { get; set; }
    }
}