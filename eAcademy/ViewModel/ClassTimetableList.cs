﻿namespace eAcademy.ViewModel
{
    public class ClassTimetableList
    {
        public int? Period1 { get; set; }
        public string SubjectName { get; set; }
        public int? SubjectId { get; set; }
        public int id { get; set; }
    }
}