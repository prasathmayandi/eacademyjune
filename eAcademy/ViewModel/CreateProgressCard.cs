﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class CreateProgressCard
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]  
        public int PC_allAcademicYears { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectExam")]  
        public int allExams { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectClass")]
        public string CI_ClassWithSection { get; set; }
    }
    public class CI_RankComments   
    {
        public int Att_allAcademicYears { get; set; }
        public int Att_class { get; set; }
        public int Att_sec { get; set; }
        public string[] Id { get; set; }
        public string[] rank_comment { get; set; }
        public int TotalCount { get; set; }
        public int Exam { get; set; }
        public string Att_classWithSection { get; set; }
    }
}