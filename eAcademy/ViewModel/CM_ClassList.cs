﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class CM_ClassList
    {
        [Required(ErrorMessage = "Please select year")]
        public int CM_allAcademicYears_List { get; set; }
    }
}