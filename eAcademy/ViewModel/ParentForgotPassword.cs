﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class ParentForgotPassword
    {
        [Required]
        public string parent_frpsw_username { get; set; }
        [Required]
        public string Captcha { get; set; }
    }
}