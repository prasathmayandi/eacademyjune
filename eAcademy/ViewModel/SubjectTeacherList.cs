﻿
namespace eAcademy.ViewModel
{
    public class SubjectTeacherList
    {
        public int Id { get; set; }
        public string SubjectName { get; set; }
        public string FacultyName { get; set; }
    }
}