﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class StudentLogin
    {
        [Required]
        public string std_userName { get; set; }
        [Required]
        public string std_password { get; set; }
        [Required]
        public string Captcha { get; set; }
    }
}