﻿using System;
namespace eAcademy.ViewModel
{
    public class ParentProfile
    {
        public string Name { get; set; }
        public DateTime? Dob { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public long? Mobile { get; set; }
        public long? WorkNumber { get; set; }
        public string Qualification { get; set; }
        public string Occupation { get; set; }
        public int? YearlyIncome { get; set; }
        public string Photo { get; set; }
        public string Religion { get; set; }
        public string Nationality { get; set; }
        public long? Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string UserId { get; set; }
    }
}