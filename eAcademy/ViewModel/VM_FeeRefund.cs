﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class VM_FeeRefund
    {
        public int? CollectionId { get; set; }
        public Decimal ExistCollectedamt { get; set; }
        [Required (ErrorMessage="Enter Refund Amount")]
        public Decimal refundamt { get; set; }
        public Decimal netamount { get; set; }
         [Required(ErrorMessage = "Enter Reason")]
        public string Reason { get; set; }
         [Required(ErrorMessage = "Enter Receivedby")]
        public string ReceivedBy { get; set; }
         public int? FeeId { get; set; }
    }
}