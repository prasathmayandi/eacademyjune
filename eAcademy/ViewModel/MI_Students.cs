﻿
namespace eAcademy.ViewModel
{
    public class MI_Students
    {
        public string RollNumber { get; set; }
        public string StudentName { get; set; }
        public string Gender { get; set; }
        public string EmergencyContactPerson { get; set; }
        public long? EmergencyContactNumber { get; set; }
        public string FatherName { get; set; }
        public long? FatherContactNumber { get; set; }
        public string MotherName { get; set; }
        public long? MotherContactNumber { get; set; }
        public string GuardianName { get; set; }
        public long? GuardianContactNumber { get; set; }
        public string HostelPreference { get; set; }
        public int? MI_allAcademicYears { get; set; }
        public int? MI_allClass { get; set; }
        public int? MI_Section { get; set; }

    }
}