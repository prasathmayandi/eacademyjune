﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class CM_ClassListEdit
    {
        [Required(ErrorMessage = "Please select Faculty")]
        public int ACT_Edit_allFaculties1 { get; set; }
        public int AssignClassTeacherId { get; set; }
    }
}