﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class StudentProfile
    {
     
        public int StudentRegisterId { get; set; }
        public string BloodGroup { get; set; }
        public string Email { get; set; }
        public string Religion { get; set; }
        public string Community { get; set; }
        public string Nationality { get; set; }
        public Nullable<long> Contact { get; set; }
        [Required]
        public string EmergencyContactPerson { get; set; }
        [Required]
        public Nullable<long> EmergencyContactNumber { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string State { get; set; }
        [Required]
        public string Student_allCountries { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string UserName { get; set; }
        public string Salt { get; set; }
        public string Password { get; set; }
        public string Gender { get; set; }
        public System.DateTime DOB { get; set; }
        public System.DateTime DOR { get; set; }
        public System.DateTime DOJ { get; set; }
        public Nullable<System.DateTime> DOL { get; set; }
        public bool StudentStatus { get; set; }
        public string Photo { get; set; }
        public string HaveGuardian { get; set; }
        public string Country { get; set; }
        public string HaveSiblings { get; set; }
        public Nullable<int> SiblingAcademicYearId { get; set; }
        public Nullable<int> SiblingClassId { get; set; }
        public Nullable<int> SiblingSectionId { get; set; }
        public Nullable<int> SiblingRegisterId { get; set; }
        public string Date_birth { get; set; }
        public string Date_join { get; set; }
        public string Date_Register { get; set; }
    }
}