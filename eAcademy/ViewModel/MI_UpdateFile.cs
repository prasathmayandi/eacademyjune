﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class MI_UpdateFile
    {
        [Required]
        public string CI_HW_File_Update { get; set; }
        public int HomeworkId { get; set; }
    }
}