﻿namespace eAcademy.ViewModel
{
    public class ClassList
    {
        public string SectionName { get; set; }
        public string ClassName { get; set; }
        public string ClassTeacher { get; set; }
        public int id { get; set; }
    }
}