﻿using eAcademy.HelperClass;
using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class AM_FacultyAttendanceReport
    {
        [Required]
        public int Att_allAcademicYears { get; set; }
        [Required]
        public int Terms { get; set; }
        [Required]
        public int allFaculties { get; set; }
    }
    public class AM_AddFacultyAttendance
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int Att_allAcademicYears { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectDate")]
        public DateTime txt_AttendanceDate { get; set; }
        public string[] Id { get; set; }       
        public string[] atn_status { get; set; }
        public string[] atn_comment { get; set; }
        public int TotalCount { get; set; }
    }
    public class AttendanceExistList
    {
        public string EmployeeRegId { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string status { get; set; }
        public string comment { get; set; }
        public string AttendanceStatusName { get; set; }
        public string AttendanceId { get; set; } 
    }
    public class StudentAttendanceExistList
    {
        public string StudentRegId { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string status { get; set; }
        public string comment { get; set; }
        public string AttendanceStatusName { get; set; }
        public string AttendanceId { get; set; }
    }
    public class AM_AddStudentAttendance
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]  
        public int Att_allAcademicYears { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectClass")]  
        public int Att_class { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectDate")]
        public DateTime DateVal { get; set; }
        public int Att_sec { get; set; }
        public string[] Id { get; set; }
        public string[] atn_status { get; set; }
        public string[] atn_comment { get; set; }
        public int TotalCount { get; set; }
        public string txt_AttendanceDate { get; set; }
    }
    
}