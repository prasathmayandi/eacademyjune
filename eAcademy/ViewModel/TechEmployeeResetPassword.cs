﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class TechEmployeeResetPassword
    {
        [Required]
        public string Employee_reset_newpsw { get; set; }
        [Required]
        public string Employee_reset_confirmpsw { get; set; }
        [Required]
        public string Captcha { get; set; }
        [Required]
        public string uid { get; set; }   
    }
}