﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class VM_ReFundFeesConfig
    {
        public string feeid { get; set; }
     
        public int acYear_Fees { get; set; }
        public int class_Fees { get; set; }
        public int feeCategory { get; set; }
      
        public Decimal txt_fees { get; set; }
        public DateTime txt_ldate { get; set; }
        public Decimal ServicesTax { get; set; }
        public Decimal Total { get; set; }
        public DateTime LastDate { get; set; }
         [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Refund_amount_is_required")]
        public Decimal RefundAmount { get; set; }
          [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Net_amount_is_required")]
        public Decimal NetAmount { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Descriptionisrequired")]
          public string Description { get; set; }
    }
}