﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class LeaveRequestList
    {
        [Required]
        public DateTime SM_txt_FromDate_List { get; set; }
        [Required]
        public DateTime SM_txt_ToDate_List { get; set; }
    }
}