﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class VM_ClassVacancy
    {
        [Required]
        public int AcademicYearId { get; set; }
        [Required]
        public int count { get; set; }
        [Required]
        public string[] clssid { get; set; }
        [Required]
        public string[] vacancy { get; set; }
        [Required]
        public string[] Status { get; set; }
        public string[] AgeLimitDate { get; set; }
    }
}