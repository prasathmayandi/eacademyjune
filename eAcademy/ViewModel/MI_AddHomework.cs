﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class MI_AddHomework
    {
        [Required]
        public int MI_HW_allAcademicYears { get; set; }
        [Required]
        public int MI_HW_allClass { get; set; }
        [Required]
        public string MI_HW_Section1 { get; set; }
        [Required]
        public string MI_HW_allSubjects1 { get; set; }
        [Required]
        public string MI_HW_txt_Homework { get; set; }
        [Required]
        public string MI_HW_txt_description { get; set; }
        [Required]
        public DateTime MI_HW_txt_DateToComplete { get; set; }
        public string MI_HW_File { get; set; }
    }
}