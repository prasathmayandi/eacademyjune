﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class StudentForgotPassword
    {
        [Required]
        public string student_frpsw_UserName { get; set; }
        [Required]
        public string Captcha { get; set; }
    }
}