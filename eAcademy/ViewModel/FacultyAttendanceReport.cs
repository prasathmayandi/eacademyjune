﻿using System;
namespace eAcademy.ViewModel
{
    public class FacultyAttendanceReport
    {
        public DateTime? Date { get; set; }
        public string Date1 { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public int? Att_SectionStudent { get; set; }
        public DateTime? txt_FromDate { get; set; }
        public DateTime? txt_ToDate { get; set; }
    }
}