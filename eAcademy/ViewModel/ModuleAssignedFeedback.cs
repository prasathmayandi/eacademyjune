﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class ModuleAssignedFeedback
    {
        [Required]
        public string rowId { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterFeedback")]  
        public string Feedback { get; set; }
    }
    public class ModuleAssignedQuery
    {
        [Required]
        public string rowId1 { get; set; }
        [Required]
        public string Query { get; set; }
    }
    public class getSubject
    {
        public string Subject { get; set; }
        public string Faculty { get; set; }
    }
}