﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class CreateProgressCard1
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int PC_allAcademicYears { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectExam")]  
        public int PC_allExams { get; set; }
    }
}