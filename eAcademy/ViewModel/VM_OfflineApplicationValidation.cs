﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class VM_OfflineApplicationValidation
    {
        public string OfflineApplicationId { get; set; }
        public int? OnlineRegid { get; set; }
        public int? Stuadmissionid { get; set; }
        [Required(ErrorMessage = "Enter Student First Name")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "First name cannot exceed 30 characters")]      
        public string StuFirstName { get; set; }
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "First name cannot exceed 30 characters")]
        public string StuMiddlename { get; set; }
        [Required(ErrorMessage = "Enter Student Last Name")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "Last name should not allow the special characters like ':', '.' ';', '*', '/' and '\' ")]
        [StringLength(30, ErrorMessage = "Last name cannot exceed 30 characters")]
        public string StuLastname { get; set; }
        [Required(ErrorMessage = "Select Student Gender")]
        public string Gender { get; set; }
        [Required(ErrorMessage = "Enter Student Date Of Birth")]
        public DateTime? DOB { get; set; }
        public string PlaceOfBirth { get; set; }
        [Required(ErrorMessage = "Enter Student Community")]
        public string Community { get; set; }
        [Required(ErrorMessage = "Enter Student Reigion")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(20, ErrorMessage = "Religion cannot exceed 20 characters")]        
        public string Religion { get; set; }
        [Required(ErrorMessage = "Enter Student Nationality")]
        [StringLength(20, ErrorMessage = "Nationality cannot exceed 20 characters")]        
        public string Nationality { get; set; }
        [Required(ErrorMessage = "Select AcademicYear")]
        public int? AcademicyearId { get; set; }
        [Required(ErrorMessage = "Enter Admission Class")]
        public string AdmissionClass { get; set; }
        [Required(ErrorMessage = "Enter Student Bloodgroup")]
        public string BloodGroup { get; set; }
        [Required(ErrorMessage = "Select Primary User")]
        public string PrimaryUser { get; set; }
        [Required(ErrorMessage = "Enter Father First Name")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "Father Fizst name cannot exceed 30 characters")]       
        public string FatherFirstName { get; set; }
        [StringLength(100, ErrorMessage = "Father occupation cannot exceed 100 characters")]       
        public string FatherOccupation { get; set; }
        public long? FatherMobileNo { get; set; }
        [StringLength(70, ErrorMessage = "Father Email cannot exceed 70 characters")]       
        public string FatherEmail { get; set; }
        [Required(ErrorMessage = "Enter Mother First Name")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "Mother First name cannot exceed 30 characters")]        
        public string MotherFirstname { get; set; }
        [StringLength(100, ErrorMessage = "Mother occupation cannot exceed 100 characters")]       
        public string MotherOccupation { get; set; }
        public long? MotherMobileNo { get; set; }
        [StringLength(70, ErrorMessage = "Mother Email cannot exceed 70 characters")]       
        public string MotherEmail { get; set; }
        [Required(ErrorMessage = "Enter User Company Name")]
        [StringLength(100, ErrorMessage = "Company name cannot exceed 100 characters")]        
        public string UserCompanyname { get; set; }
        [Required(ErrorMessage = "Enter User Company Address")]
        [StringLength(200, ErrorMessage = "Company Address1 cannot exceed 200 characters")]        
        public string CompanyAddress1 { get; set; }
        [StringLength(200, ErrorMessage = "Company Address2 cannot exceed 200 characters")]
        public string CompanyAddress2 { get; set; }
        [Required(ErrorMessage = "Enter User Company City")]
        [StringLength(50, ErrorMessage = "Company city cannot exceed 50 characters")]      
        public string CompanyCity { get; set; }
        [Required(ErrorMessage = "Enter User Company State")]
        [StringLength(50, ErrorMessage = "Company state cannot exceed 50 characters")]      
        public string CompanyState { get; set; }
        [Required(ErrorMessage = "Enter User Company Country")]       
        public string CompanyCountry { get; set; }
        [Required(ErrorMessage = "Enter User Company Postelcode")]
        public long? CompanyPostelcode { get; set; }
        [Required(ErrorMessage = "Enter User Company Contact No")]
        public long? CompanyContact { get; set; }
        [StringLength(30, ErrorMessage = "Guardian first name cannot exceed 30 characters")]      
        public string GuardianFirstname { get; set; }
        public string GuardianLastname { get; set; }
        public DateTime? GuardianDOB { get; set; }
        public string GuardianGender { get; set; }
        [StringLength(30, ErrorMessage = "Guardian relationship to child cannot exceed 30 characters")]      
        public string GuRelationshipToChild { get; set; }
        [StringLength(50, ErrorMessage = "Guardian qualification cannot exceed 50 characters")]      
        public string GuardianQualification { get; set; }
        [StringLength(70, ErrorMessage = "Guardian first name cannot exceed 70 characters")]      
        public string GuardianOccupation { get; set; }
        public long? GuardianMobileNo { get; set; }
        [StringLength(70, ErrorMessage = "Guardian Email cannot exceed 70 characters")]       
        public string GuardianEmail { get; set; }
        public int? GuardianIncome { get; set; }
        [Required(ErrorMessage = "Enter Address Line1")]
        [StringLength(200, ErrorMessage = "Address1 cannot exceed 200 characters")]       
        public string LocAddress1 { get; set; }
        [StringLength(200, ErrorMessage = "Address2 cannot exceed 200 characters")]       
        public string LocAddress2 { get; set; }
        [Required(ErrorMessage = "Enter City")]
        [StringLength(50, ErrorMessage = "City cannot exceed 50 characters")]       
        public string LocCity { get; set; }
        [Required(ErrorMessage = "Enter State")]
        [StringLength(30, ErrorMessage = "State cannot exceed 30 characters")]       
        public string LocState { get; set; }
        [Required(ErrorMessage = "Enter Country")]
        [StringLength(50, ErrorMessage = "Company Address cannot exceed 50 characters")]
        public string LocCountry { get; set; }
        [Required(ErrorMessage = "Enter Postelcode")]
        public long? LocPostelcode { get; set; }
        [Required(ErrorMessage = "Select Distance")]
        public string Distance { get; set; }
        [Required(ErrorMessage = "Enter Emergency Contact Person Name")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "Emergency Contact Person name cannot exceed 30 characters")]        
        public string EmergencyContactPersonName { get; set; }
        [Required(ErrorMessage = "Enter Emergency Contact Person Contact")]        
        public long? EmergencyContactNumber { get; set; }
        [Required(ErrorMessage = "Enter Emergency Contact Person Relationship")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "Emergency Contact Person Relationship cannot exceed 30 characters")]        
        public string ContactPersonRelationship { get; set; }
        [Required(ErrorMessage = "Select Parents Work Same School")]
        public string WorkSameSchool { get; set; }
        public string StudentPhoto { get; set; }
        public string BirthCertificate { get; set; }
        public string CommunityCertificate { get; set; }
        public string TransferCertificate { get; set; }
        public string IncomeCertificate { get; set; }
        public string StudentPhoto1 { get; set; }
        public string BirthCertificate1 { get; set; }
        public string CommunityCertificate1 { get; set; }
        public string TransferCertificate1 { get; set; }
        public string IncomeCertificate1 { get; set; }
        public int? NoOfSbling { get; set; }
        public string[] Ayear { get; set; }
        public string[] Aroll { get; set; }
        public string[] Aclass { get; set; }
        public string[] Asec { get; set; }
        [StringLength(150, ErrorMessage = "Previous school name cannot exceed 150 characters")]       
        public string PreSchool { get; set; }
        [StringLength(50, ErrorMessage = "Previous school medium cannot exceed 50 characters")]       
        public string PreMedium { get; set; }
        [StringLength(50, ErrorMessage = "Previous school class cannot exceed 50 charactideners")]       
        public string PreClass { get; set; }
        public string PreMarks { get; set; }
        public DateTime? PreFromDate { get; set; }
        public DateTime? PreToDate { get; set; }
        [Required(ErrorMessage = "Choose Term and Condition")]
        public string termscondition { get; set; }
        public string Height { get; set; }
        public string Weights { get; set; }
        public string IdentificationMark { get; set; }
        [StringLength(200, ErrorMessage = "Permentant Address1 cannot exceed 200 characters")]       
        public string PerAddress1 { get; set; }
        [StringLength(200, ErrorMessage = "Permentant Address2 cannot exceed 200 characters")]       
        public string PerAddress2 { get; set; }
        [StringLength(50, ErrorMessage = "Permentant city cannot exceed 50 characters")]       
        public string PerCity { get; set; }
        [StringLength(50, ErrorMessage = "Permentant state cannot exceed 50 characters")]       
        public string PerState { get; set; }
        public string PerCountry { get; set; }
        public long? PerPostelcode { get; set; }
        [StringLength(30, ErrorMessage = "Father last name cannot exceed 30 characters")]       
        public string FatherLastName { get; set; }
        [StringLength(50, ErrorMessage = "Father qualification cannot exceed 50 characters")]       
        public string FatherQualification { get; set; }
        public DateTime? FatherDOB { get; set; }
        [StringLength(30, ErrorMessage = "Mother last name cannot exceed 30 characters")]       
        public string MotherLastName { get; set; }
        public DateTime? MotherDOB { get; set; }
        [StringLength(50, ErrorMessage = "Father qualification cannot exceed 50 characters")]       
        public string MotherQualification { get; set; }
        public string GuardianRequried { get; set; }
        public long? TotalIncome { get; set; }
        public string Edit { get; set; }
    }
}