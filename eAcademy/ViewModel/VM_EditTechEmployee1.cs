﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class VM_EditTechEmployee1
    {
        [Required(ErrorMessage = "Enter employee first name")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "First name cannot exceed 30 characters")]
        public string txt_empname { get; set; }
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "Middle name cannot exceed 30 characters")]
        public string txt_empMname { get; set; }
        [Required(ErrorMessage = "Enter employee last name")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "Last name cannot exceed 30 characters")]
        public string txt_emplastname { get; set; }
        [Required(ErrorMessage = "Select dob")]
        public DateTime txt_dob { get; set; }
        [Required(ErrorMessage = "Select gender")]
        public string txt_gender { get; set; }
        [Required(ErrorMessage = "Select marital status")]
        public string txt_MaritalStatus { get; set; }
        [Required(ErrorMessage = "Enter blood group")]
        public string txt_bgrp { get; set; }
        [Required(ErrorMessage = "Enter pan number")]
        public string txt_PAN { get; set; }
        [Required(ErrorMessage = "Enter nationality")]
        public string txt_nationality { get; set; }
        public string txt_EmpPhoto { get; set; }
        [Required(ErrorMessage = "Enter mobile number")]
        public long txt_Mobile { get; set; }
        [Required(ErrorMessage = "Enter email address")]
        public string txt_email { get; set; }
        [Required(ErrorMessage = "Enter address line1")]
        public string txt_addressLine1 { get; set; }
        [Required(ErrorMessage = "Enter city")]
        public string txt_city { get; set; }
        [Required(ErrorMessage = "Enter state")]
        public string txt_state { get; set; }
        [Required(ErrorMessage = "Enter country")]
        public string Country { get; set; }
        [Required(ErrorMessage = "Enter zip code")]
        public string txt_pin { get; set; }
        [Required(ErrorMessage = "Enter emergency contact person name")]
        public string txt_emergencyconper { get; set; }
        [Required(ErrorMessage = "Enter emergency person conduct number")]
        public long txt_econno { get; set; }
        public string Community { get; set; }
        public string Religion { get; set; }
        public string Contact { get; set; }
        public string txt_addressLine2 { get; set; }
        public string EmpId { get; set; }
    }
}