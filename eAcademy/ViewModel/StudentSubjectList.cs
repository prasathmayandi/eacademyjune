﻿
namespace eAcademy.ViewModel
{
    public class StudentSubjectList
    {
        public string Subject { get; set; }
        public string Faculty { get; set; }
        public int FacultyId { get; set; }
        public string RowId { get; set; }
        public string classTeacher { get; set; }
    }
}