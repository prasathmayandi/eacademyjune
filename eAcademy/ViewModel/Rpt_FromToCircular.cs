﻿using System;
using System.ComponentModel.DataAnnotations;
using eAcademy.Resources.AcademicReport;
namespace eAcademy.ViewModel
{
    public class Rpt_FromToCircular
    {
        [Required(ErrorMessageResourceType = typeof(Circular), ErrorMessageResourceName = "AcademicYearRequired")]
        public int AcademicYear { get; set; }
        [Required(ErrorMessageResourceType = typeof(Circular), ErrorMessageResourceName = "ReportTypeRequired")]
        public int ReportType { get; set; }
         [Required(ErrorMessageResourceType = typeof(Circular), ErrorMessageResourceName = "FromDateRequired")]
        public DateTime Fromdate { get; set; }
         [Required(ErrorMessageResourceType = typeof(Circular), ErrorMessageResourceName = "ToDateRequired")]
        public DateTime Todate { get; set; }
    }
}