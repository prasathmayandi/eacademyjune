﻿using System;
namespace eAcademy.ViewModel
{
    public class FeeDetails
    {
        public string FeeName { get; set; }
        public Decimal? FeeAmount { get; set; }
        public DateTime? DateToPay { get; set; }
        public DateTime PaidDate { get; set; }
        public string PaidDate1 { get; set; }
        public int AcademicYearId { get; set; }
        public int FeeCategoryId { get; set; }
        public int? TermId { get; set; }
        public string TermName { get; set; }
        public string TermFeeName { get; set; }
        public Decimal? TermFeeAmount { get; set; }
        public DateTime? TermFeeLastDate { get; set; }
    }
}