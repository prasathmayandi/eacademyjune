﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class VM_ClassTimeTableValidation
    {
         [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int AcademicYear { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectClass")]
        public int Class { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectSection")]
        public int Rpt_Section { get; set; }
    }
}