﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class ExamTimetableList
    {
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> ExamDate { get; set; }
        public string SubjectName { get; set; }
        [DisplayFormat(DataFormatString = @"{0:hh\:mm}", ApplyFormatInEditMode = true)]
        public Nullable<System.TimeSpan> StartTime { get; set; }
        [DisplayFormat(DataFormatString = @"{0:hh\:mm}", ApplyFormatInEditMode = true)]
        public Nullable<System.TimeSpan> EndTime { get; set; }
        public int Id { get; set; }
    }
}