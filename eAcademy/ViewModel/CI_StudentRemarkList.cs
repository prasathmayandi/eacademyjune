﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class CI_StudentRemarkList
    {
        [Required]
        public int CI_Remark_List_allAcademicYears { get; set; }
        [Required]
        public int CI_Remark_List_allClass { get; set; }
        [Required]
        public string CI_Remark_List_Section { get; set; }
        [Required]
        public int CI_Remark_List_students { get; set; }
        [Required]
        public DateTime CI_Remark_List_txt_fromDate { get; set; }
        [Required]
        public DateTime CI_Remark_List_txt_toDate { get; set; }
        public DateTime date { get; set; }
    }
}