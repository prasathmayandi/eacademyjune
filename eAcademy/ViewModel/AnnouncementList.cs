﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class AnnouncementList
    {
        public int id { get; set; }
        public string Announcement { get; set; }
        [DisplayFormat(DataFormatString = "{0: dd, yyyy}", ApplyFormatInEditMode = true)]
        public DateTime date { get; set; }
    }
}