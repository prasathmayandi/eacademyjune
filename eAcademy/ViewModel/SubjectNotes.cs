﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class SubjectNotes
    {
        [Required]
        public int SM_AllSubjects { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public DateTime SM_FromDate { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public DateTime SM_ToDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> NotesPostedDate { get; set; }
        public string Topic { get; set; }
        public string Descriptions { get; set; }
        public string NotesFileName { get; set; }
        public int NotesId { get; set; }
        public string Notes_Id { get; set; }
        public string NotesPosted_Date { get; set; }
        public string Subject { get; set; }
        public string studentattachedfilename { get; set; }
            }
}