﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class VM_StepStudent
    {
       
        public int? OnlineRegid { get; set; }
        public string OfflineApplicationId { get; set; }
        public int? Stuadmissionid { get; set; }

        [Required(ErrorMessage = "Student FirstName is Required")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "First name cannot exceed 30 characters")]
        public string StuFirstName { get; set; }

        [StringLength(30, ErrorMessage = "Middle name cannot exceed 30 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string StuMiddlename { get; set; }

        [Required(ErrorMessage="Student LastName is Required")]
        [StringLength(30, ErrorMessage = "Student last name cannot exceed 30 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string StuLastname { get; set; }

        [Required(ErrorMessage="Gender is Required")]
        public string Gender { get; set; }

        [Required(ErrorMessage="DateofBirth is Required")]
        public DateTime? DOB { get; set; }

        [StringLength(30, ErrorMessage = "Student placeofbirth cannot exceed 30 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string PlaceOfBirth { get; set; }

        [Required(ErrorMessage="Comunity is Required")]
        public int? Community { get; set; }

        [Required(ErrorMessage="Religion is Required")]
        [StringLength(30, ErrorMessage = "Student religion cannot exceed 30 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string Religion { get; set; }

        [Required(ErrorMessage="Nationality is Required" )]
        [StringLength(30, ErrorMessage = "Student nationality cannot exceed 30 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string Nationality { get; set; }

        [Required(ErrorMessage="Academicyear is Required")]
        public int? AcademicyearId { get; set; }

        [Required(ErrorMessage="Admissionclass is Required")]      
        public int? AdmissionClass { get; set; }
        public string Edit { get; set; }

        
        [Required]
        public string PrimaryUser { get; set; }
       
        [StringLength(30, ErrorMessage = "Father first name cannot exceed 30 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string FatherFirstName { get; set; }

        [StringLength(50, ErrorMessage = "Father occupation cannot exceed 50 charracters")]
        public string FatherOccupation { get; set; }

        public long? FatherMobileNo { get; set; }

        [StringLength(70, ErrorMessage = "Father email cannot exceed 70 charracters")]
        public string FatherEmail { get; set; }
       
        [StringLength(30, ErrorMessage = "Mother first name cannot exceed 30 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string MotherFirstname { get; set; }

        [StringLength(50, ErrorMessage = "Father occupation cannot exceed 50 charracters")]
        public string MotherOccupation { get; set; }

        public long? MotherMobileNo { get; set; }

        [StringLength(70, ErrorMessage = "Father enail cannot exceed 70 charracters")]
        public string MotherEmail { get; set; }

        
        [StringLength(30, ErrorMessage = "Father last name cannot exceed 30 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string FatherLastName { get; set; }

        [StringLength(50, ErrorMessage = "Father qualification cannot exceed 50 charracters")]
        public string FatherQualification { get; set; }

        public DateTime? FatherDOB { get; set; }

        [StringLength(30, ErrorMessage = "Mother last name cannot exceed 30 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string MotherLastName { get; set; }

        public DateTime? MotherDOB { get; set; }

        [StringLength(50, ErrorMessage = "Mother qualification cannot exceed 50 charracters")]
        public string MotherQualification { get; set; }

        [Required(ErrorMessage = "GuardianRequired is Required")]
        public string GuardianRequried { get; set; }

        public long? TotalIncome { get; set; }

        [StringLength(30, ErrorMessage = "Father last name cannot exceed 30 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string GuardianLastName { get; set; }
      
       
        [StringLength(30, ErrorMessage = "Guardian first name cannot exceed 30 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string GuardianFirstname { get; set; }

        public string GuardianGender { get; set; }

        public DateTime? GuardianDOB { get; set; }

        [StringLength(50, ErrorMessage = "Guardian relationship to child cannot exceed 50 charracters")]
        public string GuRelationshipToChild { get; set; }

        [StringLength(50, ErrorMessage = "Guardian first namequalification cannot exceed 50 charracters")]
        public string GuardianQualification { get; set; }

        [StringLength(50, ErrorMessage = "Guardian occupation cannot exceed 50 charracters")]
        public string GuardianOccupation { get; set; }

        public long? GuardianMobileNo { get; set; }

        [StringLength(70, ErrorMessage = "Guardian email cannot exceed 70 charracters")]
        public string GuardianEmail { get; set; }

        public int? GuardianIncome { get; set; }

        public int? NoOfSbling { get; set; }

        public string primaryUserEmail { get; set; }
        public int EmployeeDesignationId { get; set; }
        public string EmployeeId { get; set; }
        [Required]
        public string WorkSameSchool { get; set; }
        [StringLength(150, ErrorMessage = "Previous school name cannot exceed 150 characters")]
        public string PreSchool { get; set; }
        public int? PreBoardOfSchool { get; set; }
        public string PreClass { get; set; }
        //[StringLength(10, ErrorMessage = "Previous school mark cannot exceed 10 characters")]
        public string PreMarks { get; set; }
        public DateTime? PreFromDate1 { get; set; }
        public DateTime? PreToDate { get; set; }

        public long? primaryUserContact { get; set; }
        public bool? EmailRequired { get; set; }
        public bool? SmsRequired { get; set; }
        public string UserName { get; set; }
    }
}