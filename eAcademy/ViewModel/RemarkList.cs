﻿using System;

namespace eAcademy.ViewModel
{
    public class RemarkList
    {
        public Nullable<System.DateTime> DateOfRemarks { get; set; }
        public string Remark { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
    }
}