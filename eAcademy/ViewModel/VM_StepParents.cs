﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class VM_StepParents
    {
        public int? OnlineRegid { get; set; }
        public int? Stuadmissionid { get; set; }
        public string OfflineApplicationId { get; set; }
       
        [Required(ErrorMessage = "Bloodgroup is Required")]
        public int? BloodGroup { get; set; }


        
        [Required(ErrorMessage = "Local AddressLine1 is Required")]
        [StringLength(200, ErrorMessage = "Address1 cannot exceed 200 characters")]
        public string LocAddress1 { get; set; }

        [StringLength(200, ErrorMessage = "Address2 cannot exceed 200 characters")]
        public string LocAddress2 { get; set; }

        [Required(ErrorMessage = "LocalCity is Required")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(50, ErrorMessage = "Local city cannot exceed 50 characters")]
        public string LocCity { get; set; }

        [Required(ErrorMessage = "LocalState is Required")]
        [StringLength(50, ErrorMessage = "State cannot exceed 50 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string LocState { get; set; }

        [Required(ErrorMessage = "LocalCountry is Required")]        
        public string LocCountry { get; set; }

        [Required(ErrorMessage = "LocalPostelcode is Required")]
        public long? LocPostelcode { get; set; }

        [Required(ErrorMessage = "Distance is Required")]
        public int? Distance { get; set; }

        
        [Required(ErrorMessage = "Permenant address1 is Required")]
        [StringLength(200, ErrorMessage = "Permenant address1 cannot exceed 200 characters")]
        public string PerAddress1 { get; set; }

        [StringLength(200, ErrorMessage = "Permenant address2 cannot exceed 200 characters")]
        public string PerAddress2 { get; set; }

        [Required(ErrorMessage = "Permenant city is Required")]
        [StringLength(50, ErrorMessage = "Permenant city cannot exceed 50 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string PerCity { get; set; }

        [Required(ErrorMessage = "Permenant state  is Required")]
        [StringLength(50, ErrorMessage = "Permenant state cannot exceed 50 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string PerState { get; set; }

        [Required(ErrorMessage = "Permenant country is Required")]
        public string PerCountry { get; set; }

        [Required(ErrorMessage = "Permenant postelcode is Required")]
        public long? PerPostelcode { get; set; }

      


        public int? AdmissionClass { get; set; }



       
        //[Required(ErrorMessage="Companyname is Required")]
        [StringLength(150, ErrorMessage = "Company name cannot exceed 150 charracters")]
        public string UserCompanyname { get; set; }

        //[Required(ErrorMessage = "Companyaddress1 is Required")]
        [StringLength(200, ErrorMessage = "Company Address1 cannot exceed 200 charracters")]
        public string CompanyAddress1 { get; set; }

        [StringLength(200, ErrorMessage = "Company Address2 cannot exceed 200 charracters")]
        public string CompanyAddress2 { get; set; }

        //[Required(ErrorMessage = "Companycity is Required")]
        [StringLength(50, ErrorMessage = "Company city cannot exceed 50 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string CompanyCity { get; set; }

        //[Required(ErrorMessage = "Companystate is Required")]
        [StringLength(50, ErrorMessage = "Company state cannot exceed 50 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string CompanyState { get; set; }

        //[Required(ErrorMessage = "Companycountry is Required")]
        public string CompanyCountry { get; set; }

        //[Required(ErrorMessage = "Companypostelcode is Required")]
        public long? CompanyPostelcode { get; set; }

        //[Required(ErrorMessage = "Companycontactnumber is Required")]
        public long? CompanyContact { get; set; }

        
        [StringLength(70, ErrorMessage = "Student email cannot exceed 70 characters")]
        public string Email { get; set; }

        public long? Contact { get; set; }
        
        [StringLength(20, ErrorMessage = "Hight cannot exceed 20 characters")]
        public string Height { get; set; }

        [StringLength(20, ErrorMessage = "Weight cannot exceed 20 characters")]
        public string Weights { get; set; }

        [StringLength(50, ErrorMessage = "Identification Mark cannot exceed 50 characters")]
        public string IdentificationMark { get; set; }

       
        [Required(ErrorMessage = "Emergency contact prson name is Required")]
        [StringLength(30, ErrorMessage = "Emergency contact person name cannot exceed 30 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string EmergencyContactPersonName { get; set; }

        [Required(ErrorMessage = "Emergency contact person contact is Required")]
        public long? EmergencyContactNumber { get; set; }

        [Required(ErrorMessage = "Emergency contact person relationship is Required")]
        [StringLength(50, ErrorMessage = "Emergency contact person relationship cannot exceed 50 charracters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string ContactPersonRelationship { get; set; }
        public int? NoOfSbling { get; set; }
        public string[] Ayear { get; set; }
        public string[] Aroll { get; set; }
        public string[] Aclass { get; set; }
        public string[] Asec { get; set; }
        public string PrimaryUser { get; set; }
        public string primaryUserEmail { get; set; }
        public long? primaryUserContact { get; set; }
        public bool? TransportRequired { get; set; }
        public int? TransportDestination { get; set; }
        public int? TransportPickpoint { get; set; }
        public Decimal? TransportFeeAmount { get; set; }
        public bool? HostelRequired { get; set; }
        public int? AccommodationFeeCategoryId { get; set; }
        public int? AccommodationSubFeeCategoryId { get; set; }
        public int? FoodFeeCategoryId { get; set; }
        public int? FoodSubFeeCategoryId { get; set; }
    }
}