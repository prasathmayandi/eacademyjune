﻿
namespace eAcademy.ViewModel
{
    public class FacultyClassStudentsList
    {
        public int StudentRegisterId { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
    }
}