﻿
namespace eAcademy.ViewModel
{
    public class FacultyPeriodSubject
    {
        public int? day { get; set; }
        public int? period { get; set; }
        public string subject { get; set; }
        public string className { get; set; }
        public string sectionName { get; set; }
        public int? TimeScheduleId { get; set; }
        public int? PeriodNum { get; set; }
        public string PeriodTime { get; set; }
        
    }
}