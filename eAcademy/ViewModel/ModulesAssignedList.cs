﻿
namespace eAcademy.ViewModel
{
    public class ModulesAssignedList
    {
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public string SubjectName { get; set; }
    }
}