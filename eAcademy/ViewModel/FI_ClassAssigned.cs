﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class FI_ClassAssigned
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]  
        public int Fac_allAcademicYears { get; set; }
        public string className { get; set; }
        public string sectionName { get; set; }
        public string subjectName { get; set; }
    }
}