﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class ParentLogin
    {
        [Required]
        public string parent_userName { get; set; }
        [Required]
        public string parent_password { get; set; }
        [Required]
        public string Captcha { get; set; }
    }
}