﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class StudentAssignment
    {
        [Required]
        public int SM_AllSubjects { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public DateTime SM_FromDate { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public DateTime SM_ToDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> AssignmentPostedDate { get; set; }
        public string Assignment { get; set; }
        public string Description { get; set; }
        public string AssignmentFileName { get; set; }
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateOfSubmission { get; set; }
        public int AssignmentId { get; set; }
        public string Assignment_Id { get; set; }
        public string Subject { get; set; }
        public string PostedDate { get; set; }
        public string SubmissionDate { get; set; }
        public string parentAssignmentfilename { get; set; }
    }
}