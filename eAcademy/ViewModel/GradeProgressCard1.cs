﻿using System;

namespace eAcademy.ViewModel
{
    public class GradeProgressCard1
    {
        public String Subject { get; set; }
        public int? stuRegId { get; set; }
        public String stuRollNum { get; set; }
        public String stuName { get; set; }
        public String stu_RegId { get; set; }
        public string Comment { get; set; }
    }
}