﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class StudentHomework
    {
        [Required]
        public int SM_AllSubjects { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public DateTime SM_HomeworkDate { get; set; }
        public int HomeWorkId { get; set; }
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateOfWorkPosted { get; set; }
        public string Homework { get; set; }
        public string Description { get; set; }
        public string HomeWorkFileName { get; set; }
        [DisplayFormat(DataFormatString = "{0:MMMM dd, yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateToCompleteWork { get; set; }
        public string Subject { get; set; }
        public string HomeWork_Id { get; set; }
        public string PostedDate { get; set; }
        public string DateOfSubmission { get; set; }
        public string HomeWorkWithFileName { get; set; }
    }
}