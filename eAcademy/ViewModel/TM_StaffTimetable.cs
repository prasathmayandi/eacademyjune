﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class TM_StaffTimetable
    {
        [Required]
        public int allAcademicYears { get; set; }
        [Required]
        public int allFaculties { get; set; }
        [Required]
        public int dayorder { get; set; }
    }
}