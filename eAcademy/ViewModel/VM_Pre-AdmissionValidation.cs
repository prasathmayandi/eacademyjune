﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class VM_Pre_AdmissionValidation
    {
        public string OfflineApplicationId { get; set; }
        public int? OnlineRegid { get; set; }
        public int? Stuadmissionid { get; set; }
        [Required(ErrorMessage = "StudentFirstName is Required")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "Student first name cannot exceed 30 characters")]      
        public string StuFirstName { get; set; }
       [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "Middle name should not allow the special characters like ':', '.' ';', '*', '/' and '\' ")]
        [StringLength(30, ErrorMessage = "Student middle name cannot exceed 30 characters")]
        public string StuMiddlename { get; set; }
        [Required(ErrorMessage = "StudentLastName is Required")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "Last name should not allow the special characters like ':', '.' ';', '*', '/' and '\' ")]
        [StringLength(30, ErrorMessage = "Last name cannot exceed 30 characters")]
        public string StuLastname { get; set; }
        [Required(ErrorMessage = "StudentGender is Required")]
        public string Gender { get; set; }
        [Required(ErrorMessage = "Student DateOfBirth is Required")]      
        public DateTime? DOB { get; set; }
        public string PlaceOfBirth { get; set; }
        [Required(ErrorMessage = "Student Community is Required")]
        public string Community { get; set; }
        [Required(ErrorMessage = "Student Reigion is Required")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(20, ErrorMessage = "Religion cannot exceed 20 characters")]        
        public string Religion { get; set; }
        [Required(ErrorMessage = "Enter Student Nationality")]       
        [StringLength(20, ErrorMessage = "Nationality cannot exceed 20 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string Nationality { get; set; }
        [Required(ErrorMessage = "AcademicYear is Required")]
        public int? AcademicyearId { get; set; }
        [Required(ErrorMessage = "Admission Class is Required")]
        public string AdmissionClass { get; set; }
        [Required(ErrorMessage = "Student Bloodgroup is Required")]
        public string BloodGroup { get; set; }
        [Required(ErrorMessage = "PrimaryUser is Required")]
        public string PrimaryUser { get; set; }
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "Father Fizst name cannot exceed 30 characters")]       
        public string FatherFirstName { get; set; }
        [StringLength(30, ErrorMessage = "Father last name cannot exceed 30 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string FatherLastName { get; set; }
        [StringLength(100, ErrorMessage = "Father occupation cannot exceed 100 characters")]       
        public string FatherOccupation { get; set; }
        [StringLength(70, ErrorMessage = "Father Email cannot exceed 70 characters")]       
        public string FatherEmail { get; set; }
        [StringLength(50, ErrorMessage = "Father qualification cannot exceed 50 characters")]
        public string FatherQualification { get; set; }
        public DateTime? FatherDOB { get; set; }
      [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "Mother First name cannot exceed 30 characters")]        
        public string MotherFirstname { get; set; }
        [StringLength(30, ErrorMessage = "Mother last name cannot exceed 30 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string MotherLastName { get; set; }
        [StringLength(100, ErrorMessage = "Mother occupation cannot exceed 100 characters")]       
        public string MotherOccupation { get; set; }
        [StringLength(70, ErrorMessage = "Mother Email cannot exceed 70 characters")]       
        public string MotherEmail { get; set; }
        public DateTime? MotherDOB { get; set; }
        [StringLength(50, ErrorMessage = "Mother qualification cannot exceed 50 characters")]
        public string MotherQualification { get; set; }
        //[Required(ErrorMessage = "User CompanyName is Required")]
        [StringLength(100, ErrorMessage = "Company name cannot exceed 100 characters")]        
        public string UserCompanyname { get; set; }
        //[Required(ErrorMessage = "CompanyAddress1 is Required" )]
        [StringLength(200, ErrorMessage = "Company Address1 cannot exceed 200 characters")]        
        public string CompanyAddress1 { get; set; }
        [StringLength(200, ErrorMessage = "Company Address2 cannot exceed 200 characters")]
        public string CompanyAddress2 { get; set; }
        //[Required(ErrorMessage = "CompanyCity is Required")]
        [StringLength(50, ErrorMessage = "Company city cannot exceed 50 characters")]
        //[RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string CompanyCity { get; set; }
        //[Required(ErrorMessage = "CompanyState is Required")]
        [StringLength(50, ErrorMessage = "Company state cannot exceed 50 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string CompanyState { get; set; }
        //[Required(ErrorMessage = "CompanyCountry is Required")]       
        public string CompanyCountry { get; set; }
        //[Required(ErrorMessage = "CompanyPostelcode is Required")]
        public long? CompanyPostelcode { get; set; }
        [StringLength(30, ErrorMessage = "Guardian First name cannot exceed 30 characters")]          
        public string GuardianFirstname { get; set; }
        [StringLength(30, ErrorMessage = "Guardian First name cannot exceed 30 characters")]          
        public string GuardianLastname { get; set; }
        public string GuardianGender { get; set; }
        public DateTime? GuardianDOB { get; set; }
        [StringLength(30, ErrorMessage = "Guardian relationship to child cannot exceed 30 characters")]
        public string GuRelationshipToChild { get; set; }
        [StringLength(50, ErrorMessage = "Guardian qualification cannot exceed 50 characters")]      
        public string GuardianQualification { get; set; }
        [StringLength(70, ErrorMessage = "Guardian first name cannot exceed 70 characters")]      
        public string GuardianOccupation { get; set; }
        [StringLength(70, ErrorMessage = "Guardian Email cannot exceed 70 characters")]       
        public string GuardianEmail { get; set; }
        public int? GuardianIncome { get; set; }
        [Required(ErrorMessage = "Local AddressLine1 is Required")]
        [StringLength(200, ErrorMessage = "Address1 cannot exceed 200 characters")]       
        public string LocAddress1 { get; set; }
        [StringLength(200, ErrorMessage = "Address2 cannot exceed 200 characters")]       
        public string LocAddress2 { get; set; }
        [Required(ErrorMessage = "LocalCity is Required")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(50, ErrorMessage = "Local city cannot exceed 50 characters")]       
        public string LocCity { get; set; }
        [Required(ErrorMessage = "LocalState is Required")]
        [StringLength(50, ErrorMessage = "State cannot exceed 50 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string LocState { get; set; }
        [Required(ErrorMessage = "LocalCountry is Required")]
        [StringLength(50, ErrorMessage = "Company Address cannot exceed 50 characters")]
        public string LocCountry { get; set; }
        [Required(ErrorMessage = "LocalPostelcode is Required")]
        public long? LocPostelcode { get; set; }
        [Required(ErrorMessage = "Distance is Required")]
        public string Distance { get; set; }
        [Required(ErrorMessage = " EmergencyContactPersonName is Required")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "Emergency Contact Person name cannot exceed 30 characters")]        
        public string EmergencyContactPersonName { get; set; }
        [Required(ErrorMessage = "EmergencyContactPersonContact is Required")]        
        public long? EmergencyContactNumber { get; set; }
        [Required(ErrorMessage = "EmergencyContactPersonRelationship is Required")]
        [StringLength(30, ErrorMessage = "Emergency Contact Person Relationship cannot exceed 30 characters")]        
        public string ContactPersonRelationship { get; set; }
        [Required(ErrorMessage = "ParentsWorkSameSchool is Required")]
        public string WorkSameSchool { get; set; }
        public string StudentPhoto { get; set; }
        public string BirthCertificate { get; set; }
        public string CommunityCertificate { get; set; }
        public string TransferCertificate { get; set; }
        public string IncomeCertificate { get; set; }
        public string StudentPhoto1 { get; set; }
        public string BirthCertificate1 { get; set; }
        public string CommunityCertificate1 { get; set; }
        public string TransferCertificate1 { get; set; }
        public string IncomeCertificate1 { get; set; }
        public int? NoOfSbling { get; set; }
        public string[] Ayear { get; set; }
        public string[] Aroll { get; set; }
        public string[] Aclass { get; set; }
        public string[] Asec { get; set; }
        public string txt_Fatherdob { get; set; }
        public string txt_Motherdob { get; set; }
        public string txt_Guardiandob { get; set; }
        [StringLength(150, ErrorMessage = "Previous school name cannot exceed 150 characters")]       
        public string PreSchool { get; set; }
        //[StringLength(50, ErrorMessage = "Previous school medium cannot exceed 50 characters")]
        public int? PreBoardOfSchool { get; set; }
        //[StringLength(50, ErrorMessage = "Previous school class cannot exceed 50 characters")]       
        public int? PreClass { get; set; }
        public string PreMarks { get; set; }
        public DateTime? PreFromDate { get; set; }
        public DateTime? PreToDate { get; set; }
        [Required(ErrorMessage = " Term and Condition is Required")]               
        public string termscondition { get; set; }
        public string Height { get; set; }
        public string Weights { get; set; }
        public string IdentificationMark { get; set; }
        [StringLength(200, ErrorMessage = "Permentant Address1 cannot exceed 200 characters")]       
        public string PerAddress1 { get; set; }
        [StringLength(200, ErrorMessage = "Permentant Address2 cannot exceed 200 characters")]       
        public string PerAddress2 { get; set; }
        [StringLength(50, ErrorMessage = "Permanent city cannot exceed 50 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string PerCity { get; set; }
        [StringLength(50, ErrorMessage = "Permanent state cannot exceed 50 characters")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        public string PerState { get; set; }
        public string PerCountry { get; set; }
        public long? PerPostelcode { get; set; }
        [Required(ErrorMessage="GuardianRequired is Required")]
        public string GuardianRequried { get; set; }
        public long? TotalIncome { get; set; }
        public string Edit { get; set; }
        //[Required(ErrorMessage = "CompanyContactNo is Required")]
        [StringLength(14, ErrorMessage = "Company contact number can not exceed 14 digit.")]
        public string CompanyContact { get; set; }
        [MinLength(10, ErrorMessage = "Guardian mobile number should contain 10 digit, can not exceed 10 digit.")]
        public string GuardianMobileNo { get; set; }
        [MinLength(10, ErrorMessage = "Father mobile number should contain 10 digit, can not exceed 10 digit.")]
        public string FatherMobileNo { get; set; }
        [MinLength(10, ErrorMessage = "Mother mobile number should contain 10 digit, can not exceed 10 digit.")]
        public string MotherMobileNo { get; set; }
        public int? EmployeeDesignationId { get; set; }
        public string EmployeeId { get; set; }
        public string EnrollmentType { get; set; }
        public long? PrimaryContact { get; set; }
        public long? StudentContact { get; set; }
        public string StudentEmail { get; set; }
    }
}