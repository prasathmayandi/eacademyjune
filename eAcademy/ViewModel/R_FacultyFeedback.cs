﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class R_FacultyFeedback
    {
        [Required]
        public int FacultyId { get; set; }
        [Required]
        public DateTime R_Feedback_FromDate { get; set; }
        [Required]
        public DateTime R_Feedback_ToDate { get; set; }
        public string UserName { get; set; }
        public DateTime? date { get; set; }
        public string Feedback { get; set; }
        public string UserType { get; set; }
        public string Date { get; set; }
        public string SubjectName { get; set; }
        public DateTime? DateOfFeedback { get; set; }
    }
    public class R_FacultyFeedback123
    {
        [Required(ErrorMessage="Please select faculty")]
        public int FacultyId { get; set; }
        [Required(ErrorMessage = "Please select from date")]
        public DateTime R_Feedback_FromDate { get; set; }
        [Required(ErrorMessage = "Please select to date")]
        public DateTime R_Feedback_ToDate { get; set; }
    }
}