﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class FI_ClassTimetable
    {
        [Required(ErrorMessage="Please select academic year")]
        public int Fac_allAcademicYears { get; set; }
        [Required(ErrorMessage="Please select day")]
        public int dayorder { get; set; }
    }
} 