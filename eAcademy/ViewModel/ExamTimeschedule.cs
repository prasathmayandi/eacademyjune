﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class ExamTimeschedule
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_schedule_name")]
        [StringLength(20, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Schedule_name_should_not_exceed_20_characters")]        
        public string txt_ScheduleName { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Start_date_is_required")]        
        public TimeSpan txt_stime { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "End_date_is_required")] 
        public TimeSpan txt_etime { get; set; }
    }
    public class EditExamTimeschedule
    {
        
        public string scheduleId { get; set; }        
        public string Status { get; set; }
    }
}