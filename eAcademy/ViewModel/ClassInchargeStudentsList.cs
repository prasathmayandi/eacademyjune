﻿using System;

namespace eAcademy.ViewModel
{
    public class ClassInchargeStudentsList
    {
        public string RollNumber { get; set; }
        public string StudentName { get; set; }
        public string Gender { get; set; }
        public string EmergencyContactPerson { get; set; }
        public Nullable<long> EmergencyContactNumber { get; set; }
        public string Hostel { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public int StudentRegId { get; set; }
        public string Student_RegId { get; set; }
    }
}