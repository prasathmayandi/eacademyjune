﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class TM_AssignSubjectToPeriod
    {
        [Required]
        public int TM_AssignSubToPeriod_allAcademicYears { get; set; }
        [Required]
        public int TM_AssignSubToPeriod_allClass { get; set; }
        [Required]
        public int TM_AssignSubToPeriod_Section { get; set; }
        [Required]
        public int dayorder { get; set; }
    }
}