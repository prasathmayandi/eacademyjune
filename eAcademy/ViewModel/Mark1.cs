﻿using eAcademy.HelperClass;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class Mark1
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")] 
        public int Mark_allAcademicYears { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectClass")] 
        public int Mark_allClass { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectSection")] 
        public int Mark_Section { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectSubject")] 
        public int Mark_allSubjects { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectExam")] 
        public int Mark_Exam { get; set; }
        [StringArrayRequiredAttribute(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_mark_to_all_student")] 
        public string[] Mark { get; set; }
        [Required]
        public string[] Id { get; set; }
        [Required]
        public int TotalCount { get; set; }
        public string[] Comment { get; set; }
        public string[] grade_Id { get; set; }
    }
}