﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class LeaveRequest
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectFromdate")] 
        public DateTime SM_txt_FromDate { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectTodate")] 
        public DateTime SM_txt_ToDate { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterReason")] 
        public string txt_ParentLeaveReason { get; set; }
    }
    public class EditLeaveRequest
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectFromdate")] 
        public DateTime FL_Edit_txt_FromDate { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectTodate")] 
        public DateTime FL_Edit_txt_ToDate { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterReason")] 
        public string FL_Edit_txt_ParentLeaveReason { get; set; }
        public string RequestId { get; set; }
    }
}