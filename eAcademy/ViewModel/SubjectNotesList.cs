﻿using System;
namespace eAcademy.ViewModel
{
    public class SubjectNotesList
    {
        public int NotesId { get; set; }
        public string Notes_Id { get; set; }
        public Nullable<System.DateTime> DateOfNotes { get; set; }
        public string Topic { get; set; }
        public string Descriptions { get; set; }
        public string NotesFileName { get; set; }
        public Nullable<bool> NotesStatus { get; set; }
        public string DateOf_Notes { get; set; }
        public DateTime? MI_notes_List_txt_fromDate { get; set; }
        public DateTime? MI_notes_List_txt_toDate { get; set; }
        public string subjectnotesfilename { get; set; }
    }
}