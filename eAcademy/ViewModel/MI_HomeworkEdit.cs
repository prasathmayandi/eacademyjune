﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.ViewModel
{
    public class MI_HomeworkEdit
    {
        [Required]
        public DateTime MI_HW_Edit_txt_submissionDate { get; set; }
        [Required]
        public string MI_HW_Edit_txt_Homework { get; set; }
        [Required]
        public string MI_HW_Edit_txt_description { get; set; }
        [Required]
        public bool status { get; set; }
        public int HomeworkId { get; set; }
    }
}