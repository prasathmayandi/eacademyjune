﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class CM_SubjectTeacher
    {
        [Required(ErrorMessage = "Please select year")]
        public int allAcademicYears_List { get; set; }
        [Required(ErrorMessage = "Please select class")]
        public int allClass_List { get; set; }
        [Required(ErrorMessage = "Please select section")]
        public int allSection_List { get; set; }
    }
}