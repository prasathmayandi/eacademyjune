﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class AddSubjectWithFaculty
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int allAcademicYears { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectClass")]
        public int AssignFacultyToSubject_allClass { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectSection")]
        public int AssignFacultyToSubject_Section { get; set; }

         [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectSubjects")]
        public int AssignFacultyToSubjects { get; set; }

         [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectFaculty")]
        public int allFaculties { get; set; }

        public string CI_SubjectWithFaculty { get; set; }
    }
    public class ListSubjectWithFaculty
    {
        public int AsubId { get; set; }
        public int AFacultyId { get; set; }
        public int ACI_Id { get; set; }
        public string Subject { get; set; }
        public string Faculty { get; set; }
        public string ClassIncharge { get; set; }
        public Boolean? SubjectStatus { get; set; }
    }
    public class EditSubjectWithFaculty
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectFaculty")]
        public int Edit_AssignFaculty { get; set; }

        public string Edit_CI_SubjectWithFaculty { get; set; }
        
        public Boolean status { get; set; }
        [Required]
        public int SubjectWithFacultyId { get; set; }
    }
}