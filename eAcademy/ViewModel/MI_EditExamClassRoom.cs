﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.ViewModel
{
    public class MI_EditExamClassRoom
    {
        [Required]
        public int Edit_CR_Years { get; set; }
        [Required]
        public int Edit_CR_Class { get; set; }
        [Required]
        public string Edit_CR_Sec { get; set; }
        [Required]
        public DateTime Edit_CR_txt_ExamDate { get; set; }
        [Required]
        public int Edit_CR_Exam { get; set; }
        [Required]
        public int Edit_CR_Subject { get; set; }
        [Required]
        public int Edit_CR_start_RollNumber { get; set; }
        [Required]
        public int Edit_CR_end_RollNumber { get; set; }
        [Required]
        public string Edit_CR_txt_RoomNumber { get; set; }
    }
    public class EditExamClassRoom
    {
        public string year { get; set; }
        public string clas { get; set; }
        public string sec { get; set; }
        public string exam { get; set; }
        public string sub { get; set; }
        public int yearId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int StartRollNumId { get; set; }
        public int EndRollNumId { get; set; }
        public int ClassRoomId { get; set; }
        public string RowId { get; set; }
    }
    public class EditExamClassRoomPost
    {
        [Required]
        public string RowId { get; set; }
        [Required]
        public int start_RollNumber { get; set; }
        [Required]
        public int end_RollNumber { get; set; }
        [Required]
        public int RoomNumberId { get; set; }
    }
    public class CheckRollNumberExist
    {
        public int StartRollNumberId { get; set; }
        public int EndRollNumberId { get; set; }
    }
    public class CheckExamExist
    {
        public int? StartRollNumberId { get; set; }
        
    }
      
}