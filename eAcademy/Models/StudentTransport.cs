﻿namespace eAcademy.Models
{
    public class StudentTransport
    {
        public string StudentName { get; set; }
        public long ParentContact { get; set; }
        public string RoutetNo { get; set; }
        public string BusNo { get; set; }
        public string Location { get; set; }
        public string Driver { get; set; }
        public long? Contact { get; set; }
        public int TransportId { get; set; }
        public int? StudentRegId { get; set; }
        public int? AcYearId { get; set; }
        public int? ClassId { get; set; }
        public int? SectionId { get; set; }
        public string AcademicYr { get; set; }
        public string ClassType { get; set; }
        public string Section { get; set; }
        public string ClassSection { get; set; }
        public string StdTransportId { get; set; }
        public long? stdContact { get; set; }
        public string DriverContact { get; set; }
        public bool? Status { get; set; }
    }
}