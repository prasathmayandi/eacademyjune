﻿using eAcademy.HelperClass;
using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class SaveExamTimetable
    {
        
        public int AcademicYears { get; set; }
        
        public int Exam { get; set; }
        
        public int Class { get; set; }
        
        public int[] subjectId { get; set; }

        public DateTime[] Examdate { get; set; }

        [StringArrayRequiredAttribute(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_select_schedule_to_all_fields")]
        public string[] ScheduleVal { get; set; }
        public int TotalCount { get; set; }
        public string[] ID { get; set; }
    }

    public class ExistExamTimetableList
    {
        public string id { get; set; }
        public int? SubjectId { get; set; }
        public string date { get; set; }
        public string stime { get; set; }
        public string endTime { get; set; }
        public string SubjectName { get; set; }
        public int? Schedule { get; set; } 
    }
}