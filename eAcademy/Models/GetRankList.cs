﻿
namespace eAcademy.Models
{
    public class GetRankList
    {
        public int MarkTotalId { get; set; }
        public int? TotalMark { get; set; }
        public int Rank { get; set; }
    }
}