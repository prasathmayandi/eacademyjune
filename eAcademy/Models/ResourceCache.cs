﻿using eAcademy.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;

namespace eAcademy.Models
{
    public class ResourceCache
    {
        private static bool CultureEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["CultureEnabled"]);
        private static bool OrientationEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["OrientationEnabled"]);
        private static bool CountryEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["CountryEnabled"]);
        public ResourceCache()
        {
            HttpSessionStateBase session = new HttpSessionStateWrapper(HttpContext.Current.Session);
            var CultureName =Convert.ToString(session["Language"]);
            var db = new ResourceValueService();
            if(CultureName=="")
            {
                CultureName=CultureInfo.CurrentUICulture.TextInfo.CultureName;
                session["Language"] = CultureName;
            }
            else
            {
                CultureName = CultureInfo.CurrentUICulture.TextInfo.CultureName;
                var list1 = db.GetResourceValues(ResourceType.UI, CultureEnabled ? CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);
                foreach (var item in list1) if (HttpRuntime.Cache.Get(item.Key) != null) HttpRuntime.Cache.Remove(item.Key);
                CultureName = Convert.ToString(session["Language"]);
            }
            // preload all localized text resources into cache
            var list = db.GetResourceValues(ResourceType.UI, CultureEnabled ? CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);
            foreach (var item in list) if (HttpRuntime.Cache.Get(item.Key) == null) HttpRuntime.Cache.Insert(item.Key, item.Value);
        }
        public static string Localize(string key)
        {
            if (HttpRuntime.Cache.Get(key) == null)
            {
                string value = ResourceValueService.GetResourceValue(key, CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);
                if (!string.IsNullOrEmpty(value))
                {
                    HttpRuntime.Cache.Insert(key, value);
                }
            }
            var val = HttpRuntime.Cache.Get(key);
            return val != null ? val.ToString() : null;
        }
    }
}