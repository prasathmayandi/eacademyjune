﻿using System;

namespace eAcademy.Models
{
    public class ParentStudentList
    {
        public String StudentName { get; set; }
        public int? StudentRegisterId { get; set; }
    }
}