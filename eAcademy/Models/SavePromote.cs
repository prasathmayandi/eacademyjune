﻿namespace eAcademy.Models
{
    public class SavePromote
    {
        public int AcademicYearId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int[] StudentRegId { get; set; }
        public int Old_AcademicYearId { get; set; }
        public int Old_ClassId { get; set; }
        public int Old_SectionId { get; set; }
    }
}