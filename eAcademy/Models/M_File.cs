﻿
namespace eAcademy.Models
{
    public class M_File
    {
        public string filename { get; set; }
        public int FileSizeId { get; set; }
        public int FileTypeId { get; set; }
    }
}