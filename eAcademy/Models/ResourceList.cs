﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Models
{
    public class ResourceList
    {
        public long Id { get; set; }
        public string RType { get; set; }
        public string Name { get; set; }
        public string Culture { get; set; }
        public string Value { get; set; }
    }
}