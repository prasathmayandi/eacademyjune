﻿
namespace eAcademy.Models
{
    public class M_PrimaryUser
    {
          public string PrimaryUser {get;set;}
          public string PrimaryUserName {get;set;}
          public string PrimaryUserEmail {get;set;}
          public long? PrimaryUserContactNo { get; set; }
          public int PrimaryUserAdmissionId { get; set; }
    }
}