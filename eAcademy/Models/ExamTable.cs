﻿using System;

namespace eAcademy.Models
{
    public class ExamTable
    {
        public DateTime? dates { get; set; }
        public int? subjectid { get; set; }
        public string SubjectName { get; set; }
        public TimeSpan? sttime { get; set; }
        public TimeSpan? endtime { get; set; }
        public string RollNumber { get; set; }
        public string studentname { get; set; }
        public string AttendanceStatus { get; set; }
        public string ExamType { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string ExamDates { get; set; }
    }
}