﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eAcademy.Models
{
    public class ResourceAdd
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Resource_Type_is_Required")]
        public long TypeId { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Culture_Code_is_Required")]
        public int ResourcelanquageId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Resource_Name_is_Required")]
        public string Resource_Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Resource_Value_is_Required")]
        public string Resource_Value { get; set; }

        
        public long Id { get; set; }

    }
}