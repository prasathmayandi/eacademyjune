﻿using System;

namespace eAcademy.Models
{
    public class FeesRefundList
    {
        public int AcademicYearId { get; set; }
        public string AcademicYear { get; set; }
        public int ClassId { get; set; }
        public string Class { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public int FeeCategoryId { get; set; }
        public string FeeCategory { get; set; }
        public Decimal? Refund { get; set; }
        public Decimal? NetAmount { get; set; }
        public DateTime Date1 { get; set; }
        public string Date { get; set; }
        public string Reason { get; set; }
        public DateTime dd { get; set; }
    }
}