﻿
namespace eAcademy.Models
{
    public class extracurricular
    {
        public string acyear { get; set; }
        public int? FacultyId { get; set; }
        public string FacultyName { get; set; }
        public string FacultyInchargeName { get; set; }
        public string FacultyInchargeId { get; set; }
        public string usertype { get; set; }
        public string AcademicYear { get; set; }
        public int? ActivityId { get; set; }
        public string ActivityName { get; set; }
        public string ActivityInchargeName { get; set; }
        public string InchargeId { get; set; }
        public long? ContactNumber { get; set; }
        public string FacilityName { get; set; }
    }
}