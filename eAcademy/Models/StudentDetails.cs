﻿using System;

namespace eAcademy.Models
{
    public class StudentDetails
    {
        public int? StudentRegId { get; set; }
        public string StudentName { get; set; }
        public string FatherName { get; set; }
        public string FatherQualification { get; set; }
        public string FatherOccupation { get; set; }
        public string MotherName { get; set; }
        public string MotherQualification { get; set; }
        public string MotherOccupation { get; set; }
        public string GuardianName { get; set; }
        public string GuardianQualification { get; set; }
        public string GuardianOccupation { get; set; }
        public long? FatherContact { get; set; }
        public long? MotherContact { get; set; }
        public long? GuardiaContact { get; set; }
        public int? ClassId { get; set; }
        public int? SectionId { get; set; }
        public string Hostel { get; set; }
        public string Emg_Person { get; set; }
        public long? Emg_Contact { get; set; }
        public long ParentContact { get; set; }
        public string Transport { get; set; }
        public string Bloodgroup { get; set; }
        public string Gender { get; set; }
        public string Religion { get; set; }
        public string Community { get; set; }
        public string Nationality { get; set; }
        public DateTime DOR { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public long? Contact { get; set; }
        public DateTime DOB { get; set; }
        public string studentId { get; set; }
        public string Stu_DateOfBirth { get; set; }
        public string Student_Email { get; set; }
        public string DateOfJoin { get; set; }
        public string DateOfRegister { get; set; }
        public string DateOfLeave { get; set; }
        public string FatherEmail { get; set; }
        public string MotherEmail { get; set; }
        public string GuardianEmail { get; set; }
        public string FatherDob { get; set; }
        public string MotherDob { get; set; }
        public string GuardianDob { get; set; }
        public string student_photo { get; set; }

        
    }
}