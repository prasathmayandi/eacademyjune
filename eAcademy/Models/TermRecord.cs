﻿using System;
namespace eAcademy.Models
{
    public class TermRecord
    {
        public int TermID { get; set; }
        public string TermName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int AcademicYearId { get; set; }
        public string AcademicYear { get; set; }
        public int? acYear1 { get; set; }
        public string SDate { get; set; }
        public string EDate { get; set; }
        public string Term_ID { get; set; }
        public string TermStartDate { get; set; }
        public string TermEndDate { get; set; }
    }
}