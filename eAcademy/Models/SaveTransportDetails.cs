﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SaveTransportDetails
    {
        [Required(ErrorMessage = "Select Academic Year.")]
        public int AcademicYearId { get; set; }
        [Required(ErrorMessage = "Select Class.")]
        public int ClassId { get; set; }
        [Required(ErrorMessage = "Select Section.")]
        public int SectionId { get; set; }
        [Required(ErrorMessage = "Select Student.")]
        public int StudentId { get; set; }
        [Required(ErrorMessage = "Bus number is required.")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\)\(]+$", ErrorMessage = "Bus number should not allow the special characters like ':', ';', '*', '/' and '\' ")]
        [StringLength(5, ErrorMessage = "Bus number should not exceed 5 characters.")]
        public string BusNumber { get; set; }
        [Required(ErrorMessage = "Route number is required.")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\)\(]+$", ErrorMessage = "Route number should not allow the special characters like ':', ';', '*', '/' and '\' ")]
        [StringLength(10, ErrorMessage = "Route number should not exceed 10 characters.")]
        public string RouteNumber { get; set; }
        [Required(ErrorMessage = "Location is required.")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\)\(]+$", ErrorMessage = "Location should not allow the special characters like ':', ';', '*', '/' and '\' ")]
        [StringLength(30, ErrorMessage = "Location should not exceed 30 characters.")]
        public string Location { get; set; }
         [Required(ErrorMessage = "Driver name is required")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\)\(]+$", ErrorMessage = "Driver name should not allow the special characters like ':', ';', '*', '/' and '\' ")]
        [StringLength(30, ErrorMessage = "Driver name should not exceed 30 characters.")]
        public string Driver { get; set; }
         [Required(ErrorMessage = "Contact number is required.")]
         public long Contact { get; set; }
    }
}