﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SaveGradeType
    {
        [Required]
        public string txt_GradeType { get; set; }
    }
}