﻿using System;
namespace eAcademy.Models
{
    public class StudentRemark
    {
        public string date { get; set; }
        public string CommentFrom { get; set; }
        public string Remark { get; set; }
        public string Name { get; set; }
        public DateTime? date1 { get; set; }
        public string Date { get; set; }
    }
}