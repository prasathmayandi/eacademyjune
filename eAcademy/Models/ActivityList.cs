﻿namespace eAcademy.Models
{
    public class ActivityList
    {
        public int ActivityInchargeId { get; set; }
        public int? AcYearId { get; set; }
        public string AcYear { get; set; }
        public int ActivityId { get; set; }
        public string Activity { get; set; }
        public int EmpRegId { get; set; }
        public string Fac_Incharge { get; set; }
        public string EmpId { get; set; }
        public long EmpContact { get; set; }
        public string Note { get; set; }
        public int StudentActivityId { get; set; }
        public int ClassId { get; set; }
        public string Class { get; set; }
        public int SectionId { get; set; }
        public string Section { get; set; }
        public int StdRegId { get; set; }
        public string Student { get; set; }
        public string Aid { get; set; }
        public string Desc { get; set; }
        public bool? Status { get; set; }
        public string Description { get; set; }
    }
}