﻿
namespace eAcademy.Models
{
    public class TimeTable
    {
        public int? day { get; set; }
        public int? period { get; set; }
        public string subject { get; set; }
        public int? classid { get; set; }
        public int? subjectID { get; set; }
    }
}