﻿using System;
namespace eAcademy.Models
{
    public class TC_Pdf
    {
        public int? StudentRegId { get; set; }
        public int? AdmissionId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimaryUserFirstName { get; set; }
        public string FromYear { get; set; }
        public string toYear { get; set; }
        public string FromClass { get; set; }
        public string toClass { get; set; }
        public string ConductStatus { get; set; }
        public string Nationality { get; set; }
        public string Religion { get; set; }
        public string caste { get; set; }
        public DateTime? DOB { get; set; }
        public DateTime? DateOfAdmission { get; set; }
        public int? MediumOfStudy { get; set; }
        public string JoinedClass { get; set; }
        public string LeavingClass { get; set; }
        public DateTime? DateOfIssue { get; set; }
        
    }
}