﻿
using eAcademy.HelperClass;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class GradeClass
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Grade_type_is_required")]
        public string txt_GradeType { get; set; }
        public int GradeClassId { get; set; }
        public int AcYaerId { get; set; }
        public string AcYear { get; set; }
        public int ClassId { get; set; }
        public string Class { get; set; }
        public int GradeTypeId { get; set; }      
        public string GradeType { get; set; }
        public int count { get; set; }
        public int grdesystemid { get; set; }
        public string gradesystemname { get; set; }
        public string[] gradename { get; set; }
        public int[] min { get; set; }
        public int[] max { get; set; }
        [StringArrayRequiredAttribute(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterDescription")]
        public string[] des { get; set; }
        public int?[] gid { get; set; }
        public string gradesysid { get; set; }
        public string AcademicYear { get; set; }
        public bool Status { get; set; }
    }
    public class EditGradeClass
    {
        public string txt_GradeType { get; set; }
        public int GradeClassId { get; set; }
        public int AcYaerId { get; set; }
        public string AcYear { get; set; }
        public int ClassId { get; set; }
        public string Class { get; set; }
        public int GradeTypeId { get; set; }      
        public string GradeType { get; set; }
        public int count { get; set; }
        public int grdesystemid { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Grade_type_is_required")]
        public string gradesystemname { get; set; }
        public string[] gradename { get; set; }
        public int[] min { get; set; }
        public int[] max { get; set; }
        [StringArrayRequiredAttribute(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseEnterDescription")]
        public string[] des { get; set; }
        public int?[] gid { get; set; }
        public string gradesysid { get; set; }
        public string AcademicYear { get; set; }
        public bool Status { get; set; }
    }
}