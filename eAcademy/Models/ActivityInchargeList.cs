﻿namespace eAcademy.Models
{
    public class ActivityInchargeList
    {
        public int ActivityInchargeId { get; set; }
        public int? AcYearId { get; set; }
        public string AcYear { get; set; }
        public int ActivityId { get; set; }
        public string Activity { get; set; }
        public int EmpRegId { get; set; }
        public string Emp { get; set; }
        public string EmpId { get; set; }
        public long EmpContact { get; set; }
        public string Note { get; set; }
        public bool? status { get; set; }
    }
}