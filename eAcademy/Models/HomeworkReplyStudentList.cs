﻿
namespace eAcademy.Models
{
    public class HomeworkReplyStudentList
    {
        public string StudentRollNumber { get; set; }
        public string Name { get; set; }
        public string Answer { get; set; }
        public string HomeworkAnswer_Id { get; set; }
        public string StudentReg_Id { get; set; }
        public string Remark { get; set; }
    }
}