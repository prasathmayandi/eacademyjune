﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class SaveExam
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Exam_name_is_required")]
        [StringLength(30, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Facility_should_contain_3_characters_can_not_exceed_30_characters", MinimumLength = 3)]
        public string txt_exam { get; set; }
        public string examid { get; set; }
        public string status { get; set; }
    }
}