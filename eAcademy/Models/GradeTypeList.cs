﻿
namespace eAcademy.Models
{
    public class GradeTypeList
    {

        public string GradeType { get; set; }
        public int GradeTypeId { get; set; }
        public string IdGradeType { get; set; }
        public bool? Status { get; set; }
        public string addview { get; set; }
    }
}