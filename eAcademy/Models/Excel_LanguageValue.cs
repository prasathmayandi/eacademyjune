﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Datatable.Models
{
    public class Excel_LanguageValue
    {
        public string ResourceName { get; set; }
        public string ResourceValue { get; set; }
        public string ResourceTypeId { get; set; }
        public string ResourceCulture { get; set; }
        public string ResourceOrientation { get; set; }
    }
}