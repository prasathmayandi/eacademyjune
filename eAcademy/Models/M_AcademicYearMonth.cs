﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Models
{
    public class M_AcademicYearMonth
    {
        public int MonthId { get; set; }
        public string MonthName { get; set; }
        public string order { get; set; }
    }
}