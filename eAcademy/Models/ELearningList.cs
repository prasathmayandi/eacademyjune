﻿
namespace eAcademy.Models
{
    public class ELearningList
    {
        public string ELearn_Id { get; set; }
        public string PostedtDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string LastDate { get; set; }
        public bool? Status { get; set; }
        public string File { get; set; }
        public string ElearningAttachfilename { get; set; }
    }
}