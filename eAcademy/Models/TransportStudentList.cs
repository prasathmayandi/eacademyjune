﻿namespace eAcademy.Models
{
    public class TransportStudentList
    {
        public int hsid { get; set; }
        public int? acid { get; set; }
        public int? cid { get; set; }
        public int? sec_id { get; set; }
        public int? std_id { get; set; }
        public string acYear { get; set; }
        public string classType { get; set; }
        public string section { get; set; }
        public string stdName { get; set; }
        public long? stdContact { get; set; }
        public string stdAddress { get; set; }
        public long? emgyContact { get; set; }
        public string emgyPerson { get; set; }
        public string roomNumber { get; set; }
        public bool? status { get; set; }
        public string busno { get; set; }
        public string ClassSection { get; set; }
        public string NameContact { get; set; }
        public string HStdID { get; set; }
    }
}