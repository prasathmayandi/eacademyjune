﻿using System;
namespace eAcademy.Models
{
    public class UserLogList
    {
        public int UserLogId { get; set; }
        public string IpAddress { get; set; }
        public string LoginTime { get; set; }
        public string LogoutTime { get; set; }
        public string SessionId { get; set; }
        public string UserRegId { get; set; }
        public string UserType { get; set; }
        public string Link { get; set; }
        public string userid { get; set; }
        public string Activity { get; set; }
        public DateTime? Time { get; set; }
        public string LoginSource { get; set; }
        public DateTime? Login { get; set; }
        public DateTime? logout { get; set; }
    }
 }