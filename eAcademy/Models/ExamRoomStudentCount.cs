﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Models
{
    public class ExamRoomStudentCount
    {
        public int AssignExamClassRoomId { get; set; }
        public int? NumberOfStudents { get; set; }
    }
    public class ExamRollNo
    {
        public int? StartRoolNo { get; set; }
        public int? EndRoolNo { get; set; }
        public int? SubjectId { get; set; }
        public int? StartRegisterId { get; set; }
        
            
    }
}