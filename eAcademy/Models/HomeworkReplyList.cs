﻿namespace eAcademy.Models
{
    public class HomeworkReplyList
    {
        public string Description { get; set; }
        public string Subject { get; set; }
        public string Title { get; set; }
        public string SubmissionDate { get; set; }
        public string[] Id { get; set; }
        public string[] Homework_comment { get; set; }
        public int TotalCount { get; set; }
       
    }
}