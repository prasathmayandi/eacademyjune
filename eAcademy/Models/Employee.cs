﻿using System;

namespace eAcademy.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string EmpRegId { get; set; }
        public string EmployeeName { get; set; }
        public string DOB { get; set; }
        public string BloodGroup { get; set; }
        public long? Contact { get; set; }
        public string Address { get; set; }
        public string DOJ { get; set; }
        public string DOR { get; set; }
        public string PersonContact { get; set; }
        public string DOResigned { get; set; }
        public string EmpId { get; set; }
        public string EmployeeTypes { get; set; }
        public string EmployeeDesianations { get; set; }
        public string Edit { get; set; }
        public DateTime? dob { get; set; }
        public DateTime? doj { get; set; }
        public DateTime? dor { get; set; }
        public DateTime? doresign { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string File { get; set; }
        public bool? Status { get; set; }
    }
}