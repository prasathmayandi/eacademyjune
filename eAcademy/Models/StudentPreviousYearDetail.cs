﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Models
{
    public class StudentPreviousYearDetail
    {
        public string PreviousAcYear { get; set; }
        public string PreviousAcClass { get; set; }
    }
}