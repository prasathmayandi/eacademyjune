﻿
namespace eAcademy.Models
{
    public class M_GetFlagDetails
    {
        public string PrimaryUserName { get; set; }
        public string SchoolName { get; set; }
        public string SchoolLogo { get; set; }
        public string StudentName { get; set; }
        public int ApplicationNo { get; set; }
        public string Txt_Academicyear { get; set; }
        public string Txt_class { get; set; }
        public int? OnlineregisterId { get; set; }
    }
}