﻿using eAcademy.HelperClass;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SaveMapFeatureRole
    {
        
        public string FeatureId { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Select_Role")]
        public int RoleId { get; set; }

    }
}