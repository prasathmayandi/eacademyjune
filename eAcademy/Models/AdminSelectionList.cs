﻿
namespace eAcademy.Models
{
    public class AdminSelectionList
    {
        public int? OnlineRegid { get; set; }
        public int? StudentAdmissionId { get; set; }
        public string ApplicationId { get; set; }
        public string PrimaryUserName { get; set; }
        public string StuFirstName { get; set; }
        public string txt_AdmissionClass { get; set; }
        public string Distance { get; set; }
        public string SiblingStatus { get; set; }
        public string WorkSameSchool { get; set; }
        public int? NoOfSbling { get; set; }
        public string statusFlag { get; set; }
        public string PrimaryUserEmail { get; set; }
        public string txt_AcademicYear { get; set; }
        public int? AcademicyearId { get; set; }
        public int? ClassId { get; set; }
        public string description { get; set; }
        public string ApplicationSource { get; set; }
        public string Photo { get; set; }
        public string StudentName { get; set; }
        public string ApplyClass { get; set; }
    }
}