﻿
namespace eAcademy.Models
{
    public class GetGrade
    {
        public string grade { get; set; }
        public int? minVal { get; set; }
        public int? maxVal { get; set; }
    }
}