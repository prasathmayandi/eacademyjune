﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class EditCalender
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Holiday_name_is_required")]
        [StringLength(50, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Event_should_contain_3_characters_can_not_exceed_50_characters", MinimumLength = 3)]       
        public string txt_event { get; set; }

        [StringLength(300, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Event_description_should_not_exceed_250_characters")]  
        public string txt_event_desc { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "EventDate_is_required")]  
        public DateTime txt_event_date { get; set; }

        public string EventId { get; set; }
        public string Status { get; set; }
        public bool? Editannouncementstatus { get; set; }
    }
}