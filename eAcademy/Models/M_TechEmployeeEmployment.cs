﻿
namespace eAcademy.Models
{
    public class M_TechEmployeeEmployment
    {
        public string Institute { get; set; }
        public string Designation { get; set; }
        public string Expertise { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Tootalyears { get; set; }
        public int EmployeeExperienceId { get; set; }
    }
}