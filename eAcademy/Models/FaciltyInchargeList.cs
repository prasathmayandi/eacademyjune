﻿namespace eAcademy.Models
{
    public class FaciltyInchargeList
    {
        public int FacilityInchargeId { get; set; }
        public int? AcYearId { get; set; }
        public string AcYear { get; set; }
        public int FacId { get; set; }
        public string Fac { get; set; }
        public int EmpRegId { get; set; }
        public string Emp { get; set; }
        public string EmpId { get; set; }
        public long EmpContact { get; set; }
        public string Note { get; set; }
        public bool? status { get; set; }
        public string EmpType { get; set; }
    }
}