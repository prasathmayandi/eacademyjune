﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class EditFeeConfig
    {
        public string feeid { get; set; }
        public int feeid1 { get; set; }
        public int acYear_Fees { get; set; }
        public int class_Fees { get; set; }
        public int feeCategory { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Feeisrequired")]
        public Decimal txt_fees { get; set; }
        public DateTime txt_ldate { get; set; }
        public Decimal ServicesTax { get; set; }
        public Decimal Total { get; set; }
        public DateTime LastDate { get; set; }
        public Decimal RefundAmount { get; set; }
        public Decimal NetAmount { get; set; }
    }
}