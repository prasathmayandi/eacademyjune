﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SaveSubject
    {

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Subject_is_required")]
        [StringLength(30, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Subject_should_contain_2_characters_can_not_exceed_30_characters", MinimumLength = 2)]         
        public string subject { get; set; }
        public string status { get; set; }
        
        
        public string GroupSubjectStatus { get; set; }
        
        public string non_Academic { get; set; }
        public string subject_id { get; set; }
    }
}