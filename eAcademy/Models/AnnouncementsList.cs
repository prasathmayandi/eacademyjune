﻿using System;
namespace eAcademy.Models
{
    public class AnnouncementsList
    {
        public int AnnouncementId { get; set; }
        public string AnnouncementName { get; set; }
        public DateTime AnnouncementDate { get; set; }
        public string AnnouncementDescription { get; set; }
    }
}