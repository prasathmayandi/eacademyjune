﻿using System;
namespace eAcademy.Models
{
    public class Eprofile
    {
        public int EmployeeRegisterId { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Gender { get; set; }
        public System.DateTime DOB { get; set; }
        public string BloodGroup { get; set; }
        public string Email { get; set; }
        public string Community { get; set; }
        public string Religion { get; set; }
        public string Nationality { get; set; }
        public int? EmployeeTypeId { get; set; }
        public int? EmployeeDesignationId { get; set; }
        public string LicenseNo { get; set; }
        public System.DateTime? Expiredate { get; set; }
        public long? Contact { get; set; }
        public string EmergencyContactPerson { get; set; }
        public Nullable<long> EmergencyContactNumber { get; set; }
        public string EmergencyContactPersonRelationShip { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Expertise { get; set; }
        public string Qualification { get; set; }
        public string Experience { get; set; }
        public System.DateTime DOR { get; set; }
        public System.DateTime DOJ { get; set; }
        public DateTime DOL { get; set; }
        public bool? EmployeeStatus { get; set; }
        public string ImgFile { get; set; }
        public string LandlineNo { get; set; }
        public string Addressline1 { get; set; }
        public string Addressline2 { get; set; }
        public string DateOfBirth { get; set; }
        public string DateOfJoin { get; set; }
        public string DateOfRegister { get; set; }
        public string DateOfLeaving { get; set; }
        public string Fname { get; set; }
        public string Mname { get; set; }
        public string Lname { get; set; }
        public string Country { get; set; }
        public string Pincode { get; set; }
        public string PAN { get; set; }
        public string Marital { get; set; }
        public int TotalExp { get; set; }
        public Decimal? Salary { get; set; }
        public string BankAcNo { get; set; }
        public string BankAcName { get; set; }
        public string BankName { get; set; }
        public string PermanentAddressline1 { get; set; }
        public string PermanentAddressline2 { get; set; }
        public string PermanentCity { get; set; }
        public string PermanentState { get; set; }
        public string PermanentCountry { get; set; }
        public string PermanentPincode { get; set; }
        public string ResumeFile { get; set; }
        public string RationCardFile { get; set; }
        public string VoterIdFile { get; set; }
        public string DegreeFile { get; set; }
        public string emptype { get; set; }
        public string empdesign { get; set; }
        public string txt_Expiredate { get; set; }
        public string  EmployeeType { get; set; }
        public string EmployeeDesignation { get; set; }
        public string Etype { get; set; }
        public bool? EmpStatus { get; set; }
    }
}