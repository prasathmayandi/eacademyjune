﻿
namespace eAcademy.Models
{
    public class StudentParentProfile
    {
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string StudentDob { get; set; }
        public string StudentGender { get; set; }
        public string StudentEmail { get; set; }
        public long? StudentMobile { get; set; }
        public string StudentPlaceOfBirth { get; set; }
        public string StudentHeight { get; set; }
        public string StudentWeight { get; set; }
        public string StudentAddress { get; set; }
        public string StudentCity { get; set; }
        public string StudentState { get; set; }
        public string StudentCountry { get; set; }
        public long? StudentPostalCode { get; set; }
        public string StudentEmergencyContactPersonName { get; set; }
        public long? StudentEmergencyContactPersonPhone { get; set; }
        public string FatherName { get; set; }
        public string FatherDob { get; set; }
        public string FatherEmail { get; set; }
        public long? FatherMobile { get; set; }
        public string FatherQualification { get; set; }
        public string FatherOccupation { get; set; }
        public string MotherName { get; set; }
        public string MotherDob { get; set; }
        public string MotherEmail { get; set; }
        public long? MotherMobile { get; set; }
        public string MotherQualification { get; set; }
        public string MotherOccupation { get; set; }
        public string GuardianName { get; set; }
        public string GuardianGender { get; set; }
        public string GuardianRelationship { get; set; }
        public string GuardianDob { get; set; }
        public string GuardianEmail { get; set; }
        public long? GuardianMobile { get; set; }
        public string GuardianQualification { get; set; }
        public string GuardianOccupation { get; set; }
        public long? GuardianIncome { get; set; }
        public string PrimaryUser { get; set; }
        public long? PrimaryUserWorkNumber { get; set; }
        public long? PrimaryUserYearlyIncome { get; set; }
        public string PrimaryUserCompanyAddress { get; set; }
        public string PrimaryUserCompanyCity { get; set; }
        public string PrimaryUserCompanyState { get; set; }
        public string PrimaryUserCompanyCountry { get; set; }
        public long? PrimaryUserCompanyContactNumber { get; set; }
    }
}