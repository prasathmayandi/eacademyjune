﻿namespace eAcademy.Models
{
    public class M_ClassMarkList
    {
        public int? StudentRegid { get; set; }
        public string StudentId { get; set; }
        public string RollNumber1 { get; set; }
        public string StudentName { get; set; }
        public int? TotalMark { get; set; }
        public int? Ranks { get; set; }
        public string Rank { get; set; }
    }
}