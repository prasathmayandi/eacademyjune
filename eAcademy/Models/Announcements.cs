﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class Announcements
    {
        [Required(ErrorMessage = "Select Academic Year")]
        public int acYear { get; set; }
        public string AcademicYear { get; set; }
        public int AnnoumcementId { get; set; }
        [Required(ErrorMessage = "Date is required.")]
        public DateTime? tacYSD { get; set; }
        [Required(ErrorMessage = "Heading is required.")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\)\(]+$", ErrorMessage = "Heading should not allow the special characters like ':', ';', '*', '/' and '\' ")]
        [StringLength(30, ErrorMessage = "Heading should contain 3 characters, can not exceed 30 characters.", MinimumLength = 3)]
        public string Heading { get; set; }
         [StringLength(250, ErrorMessage = "Description should  not exceed 250 characters.")]
        public string Desc { get; set; }
        public bool? Status { get; set; }
        public string Aid { get; set; }
        public string Date { get; set; }
        public string AnnouncementName { get; set; }
        public string Description { get; set; }
        public string AnnouncementDate { get; set; }
        public bool? Pushnotification { get; set; }
        public int? eventid { get; set; }
        public string Source { get; set; }
    }
}