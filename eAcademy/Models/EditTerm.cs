﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class EditTerm
    {
        [Required]
        public int acYear { get; set; }
        [Required]
        public string txt_termname { get; set; }
        [Required]
        public DateTime tacYSD { get; set; }
        [Required]
        public DateTime tacYED { get; set; }
    }
}