﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Models
{
    public class GetFeeStructureTransportHostel
    {
        public int? CategoryId { get; set; }
        public int? SubCategoryId { get; set; }
        public int? DestinationId { get; set; }
        public int? Id { get; set; }
        public decimal? TransportFee { get; set; }
        public string DestinationName { get; set; }
        public string Pickpoint { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public decimal? hostelfee { get; set; }
    }
}