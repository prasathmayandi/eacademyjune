﻿using System;

namespace eAcademy.Models
{
    public class Date
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string SDate { get; set; }
        public string EDate { get; set; }
        public bool? academicstatus { get; set; }
    }
}