﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Models
{
    public class Resource_Language_Excelsheet
    {

        public string LanquageDisplayName { get; set; }
        public string LanguageCultureName { get; set; }
        public string LanguageCultureCode { get; set; }
        public string LanguageCountryCode { get; set; }
    }
}