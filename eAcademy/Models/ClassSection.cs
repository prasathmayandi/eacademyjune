﻿using System;
namespace eAcademy.Models
{
    public class ClassSection
    {
        public int ClassId { get; set; }
        public string ClassType { get; set; }
        public int SecClassId { get; set; }
        public int SectionId { get; set; }
        public string Section { get; set; }
        public Boolean? Status { get; set; }
        public string ClassTimeSchedule { get; set; }
        public int? ScheduleID { get; set; }
        public int? Strength { get; set; }
        public int? ClassRoomId { get; set; }
        public int SectionStrengthId { get; set; }
    }
}