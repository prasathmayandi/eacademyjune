﻿
namespace eAcademy.Models
{
    public class FacultyListForAttendance
    {
        public string employeeId { get; set; }
        public string employeeName { get; set; }
        public string empRegId { get; set; }
    }
}