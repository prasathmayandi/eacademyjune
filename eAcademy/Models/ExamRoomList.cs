﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class ExamRoomList
    {
        public string RoomNumber { get; set; }
        public int? ClassRoomId { get; set; }
        public int? FacultyId { get; set; }
        public string id { get; set; }
        public string RoomId { get; set; }
    }
    public class SaveInvigilator
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int AcademicYears { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectExam")]
        public int Exam { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectDate")]
        public DateTime Date { get; set; }
        [Required]
        public int TotalCount { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectSchedule")]
        public int Schedule { get; set; }
        [Required]
        public string[] ClassRoomId { get; set; }
        [Required]
        public int[] EmployeeRegId { get; set; }
        [Required]
        public string[] ID { get; set; }
    }
}