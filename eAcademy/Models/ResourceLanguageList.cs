﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Models
{
    public class ResourceLanguageList
    {
        public int ResourceLanguageId { get; set; }
        public string LanguageDisplayname { get; set; }
        public string LanguageCultureName { get; set; }
        public string LanguageCultureCode { get; set; }
        public string LanguageCountryCode { get; set; }
        public bool? status { get; set; }
    }
}