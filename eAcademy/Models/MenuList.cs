﻿
namespace eAcademy.Models
{
    public class MenuList
    {
        public int FeatureId { get; set; }
        public string SubMenu { get; set; }
        public string SubMenu_Link { get; set; }
    }
}