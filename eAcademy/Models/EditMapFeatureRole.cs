﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class EditMapFeatureRole
    {
        public string MRid { get; set; }
              
        public int RoleId { get; set; }
        
        public int FeatureId { get; set; }
        public string status { get; set; }
     
    }
}