﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class SaveTechEmployee1
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_employee_first_name")]
        [StringLength(30, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "employee_first_name_should_contain_3_characters_can_not_exceed_200_characters", MinimumLength = 3)]
        public string txt_empname { get; set; }

        [StringLength(30, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "employee_Middle_name_should_contain_3_characters_can_not_exceed_200_characters", MinimumLength = 3)]       
        public string txt_empMname { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_employee_last_name")]
        [StringLength(30, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "employee_last_name_name_should_contain_1_characters_can_not_exceed_200_characters")]
        public string txt_emplastname { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_choose_dateofbirth")]
        public DateTime txt_dob { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_select_gender")]        
        public string txt_gender { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_select_blood_group")]
        public string txt_bgrp { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_religion")]
        public string txt_religion { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_nationality")]
        public string txt_nationality { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_select_community")]
        public string txt_community { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_select_martial_status")]
        public string txt_MaritalStatus { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_select_employee_type")]
        public int txt_EmployeeType { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectEmployeeDesignation")]
         public int txt_EmployeeDesignation { get; set; }

        public string[] txt_ClassList1 { get; set; }
        public string[] txt_SubjectList1 { get; set; }
        public string txt_LicenseNo { get; set; }
        public DateTime? txt_ExpireDate { get; set; }
        public string txt_contact { get; set; }

       [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_mobile_number")]
        public long txt_Mobile { get; set; }

        public string txt_email { get; set; }

         [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_local_addressline1")]
        public string txt_addressLine1 { get; set; }

        public string txt_addressLine2 { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_local_city")]
        public string txt_city { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_local_state")]
        public string txt_state { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_select_local_country")]
        public string Country { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_local_zipcode")]
        public string txt_pin { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_permanent_addressline1")]
        public string txt_Permanentaddress { get; set; }

        public string txt_PermanentaddressLine { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_permanent_city")]
        public string txt_Permanentcity { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_permanent_state")]
        public string txt_Permanentstate { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_select_permanent_country")]
        public string PermanentCountry { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_permanent_zipcode")]
        public string txt_Permanentpin { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_emergency_contact_person_name")]
        public string txt_emergencyconper { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_emergency_contact_person_contact")]
        public long txt_econno { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_emergency_contact_person_relationship")]
        public string txt_emergencyconperrelationship { get; set; }

        public string txt_Status { get; set; }
        public string EmpId { get; set; }
    }
}