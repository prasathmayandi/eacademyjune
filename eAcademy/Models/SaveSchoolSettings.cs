﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SaveSchoolSettings
    {
        
        public int sid { get; set; }

       // [EmailAddress(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "School_code_should_contain_3_characters_can_not_exceed_10_characters")]
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "School_name_is_required")]
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "School_name_should_contain_3_characters_can_not_exceed_200_characters", MinimumLength = 3)]        
        public string txt_schoolName { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "School_code_is_required")]
        [StringLength(10, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "School_code_should_contain_3_characters_can_not_exceed_10_characters")]
        public string txt_schoolcode { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "AddressLine1_name_is_required")]
        [StringLength(100, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Adress_line1_can_not_exceed_100_characters")]
        public string txt_Address1 { get; set; }

        [StringLength(100, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Adress_line2_can_not_exceed_100_characters")]
        public string txt_Address2 { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "City_is_required")]
        [StringLength(50, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "City_should_contain_3_characters_can_not_exceed_50_characters", MinimumLength = 3)]
        public string txt_City { get; set; }

        //[EmailAddress(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "State should not allow the special characters like ':', '.' ';', '*', '/' and '\' ")]

        

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "State_is_required")]
        [StringLength(50, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "State_should_contain_2_characters_can_not_exceed_50_characters", MinimumLength = 2)]
        public string txt_State { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Zip_code_is_required")]
        [StringLength(6, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Zip_code_should_contain_6_digit_can_not_exceed_6_digit", MinimumLength = 6)]
        public string txt_ZIP { get; set; }

        //[Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Country_is_required")]      
        public string txt_Country { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Phone_Number1_is_required")]
        [StringLength(14, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Phone_number1_can_not_exceed_14_digit")]
        public string txt_Phone1 { get; set; }

        [StringLength(14, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Phone_number2_can_not_exceed_14_digit")]
        public string txt_Phone2 { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Mobile_Number1_is_required")]
        public long txt_Mobile1 { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Mobile_Number2_is_required")]
        public long txt_Mobile2 { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Fax_is_required")]
        [StringLength(14, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "FAX_can_not_exceed_14_digit")]
        public string txt_Fax { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Email_is_required")]
        [StringLength(70, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Email_can_not_exceed_70_characters")]
        [EmailAddress(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Invalid_email_address")]
        public string txt_Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Website_URL_is_required")]
        [StringLength(50, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Website_URL_can_not_exceed_50_characters")]
        public string txt_Website { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Student_ID_format_is_required")]
        [StringLength(10, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Student_ID_format_can_not_exceed_10_digit")]
        //[RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "Student register ID format should not allow the special characters like ':', '.' ';', '*', '/' and '\' ")]       
        public string txt_stdIdFormat { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Parent_ID_format_is_required")]
        [StringLength(10, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Parent_ID_format_can_not_exceed_10_digit")]
        //[RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "Parent register ID format should not allow the special characters like ':', '.' ';', '*', '/' and '\' ")]
        public string txt_parentIdFormat { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Staff_ID_format_is_required")]
        [StringLength(10, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Staff_ID_format_can_not_exceed_10_digit")]
        //[RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "Staff ID format should not allow the special characters like ':', '.' ';', '*', '/' and '\' ")]
        public string txt_staffIdFormat { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Non_Staff_ID_format_is_required")]
        [StringLength(10, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Non_Staff_ID_format_can_not_exceed_10_digit")]
        //[RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "Non-Staff ID format should not allow the special characters like ':', '.' ';', '*', '/' and '\' ")]        
        public string txt_nonStaffIdFormat { get; set; }

        //[Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Currency_type_is_required")]
        public int txt_Currency { get; set; }

        //[Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Day_order_is_required")]
        public int txt_dayOrder { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Marking_scheme_format_is_required")]
        public string txt_markFormat { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Services_tax_is_required")]
        public Decimal? txt_Tax { get; set; }


       public Decimal? txt_VAT { get; set; }
       public bool TransportRequired { get; set; }
       public bool HostelRequired { get; set; }
       public bool EmailRequired { get; set; }
       public bool SmsRequired { get; set; }
       public bool AppRequired { get; set; }
    }
}