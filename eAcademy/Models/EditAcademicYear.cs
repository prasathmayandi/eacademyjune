﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class EditAcademicYear
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Start_date_is_required")]        
        public DateTime EditacYSD { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "End_date_is_required")]        
        public DateTime EditacYED { get; set; }

        public int AcademicYearId { get; set; }
        public bool AcademicStatus { get; set; }
    }
}