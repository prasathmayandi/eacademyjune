﻿using System;
namespace eAcademy.Models
{
    public class TermFeeList
    {
        public int TermFeeId { get; set; }
        public int FeeId { get; set; }
        public int AcYearId { get; set; }
        public string AcYear { get; set; }
        public int TermId { get; set; }
        public string Term { get; set; }
        public int ClassId { get; set; }
        public string Class { get; set; }
        public int FeeCategoryId { get; set; }
        public string FeeCategory { get; set; }
        public Decimal? Amount { get; set; }
        public Decimal? Tax { get; set; }
        public Decimal? Total { get; set; }
        public DateTime LastDate { get; set; }
    }
}