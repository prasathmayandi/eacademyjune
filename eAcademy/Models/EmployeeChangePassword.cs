﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class EmployeeChangePassword
    {
        [Required]
        public string Emp_NewPsw { get; set; }
        [Required]
        public string Emp_RePsw { get; set; }
    }
}