﻿using System;
using System.ComponentModel.DataAnnotations;
using eAcademy.Resources.AcademicReport;
namespace eAcademy.Models
{
    public class Circulars
    {
        [Required(ErrorMessage = "Select Academic Year.")]
        public int acYear { get; set; }
        public string AcademicYear { get; set; }
        public int CircularId { get; set; }
        [Required(ErrorMessage = "Date is required.")]
        public DateTime? tacYSD { get; set; }
        [Required(ErrorMessage = "Heading is required.")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\)\(]+$", ErrorMessage = "Heading should not allow the special characters like ':', ';', '*', '/' and '\' ")]
        [StringLength(30, ErrorMessage = "Heading should contain 3 characters, can not exceed 30 characters.", MinimumLength = 3)]
        public string Heading { get; set; }
       [StringLength(250, ErrorMessage = "Description should  not exceed 250 characters.")]
        public string Reason { get; set; }
        public int EmpRegId { get; set; }
        public string Employee { get; set; }
        public bool? Status { get; set; }
        public string employeeid { get; set; }
        public string Circular_ID { get; set; }
        [Display(Name = "ReportType",ResourceType=typeof(Circular))]
        public int ReportType { get; set; }
        [Display(Name = "FromDate", ResourceType = typeof(Circular))]
        public DateTime Fromdate { get; set; }
        [Display(Name = "ToDate", ResourceType = typeof(Circular))]
        public DateTime Todate { get; set; }
        [Display(Name = "Dates", ResourceType = typeof(Circular))]
        public DateTime Dates { get; set; }
        [Display(Name = "Month", ResourceType = typeof(Circular))]
        public int month { get; set; }
        public string ddates { get; set; }
        public string Date { get; set; }
    }
}