﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class QuickE_Mail
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_to_mail")]  
        public string emailto { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_subject")]  
        public string subject { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_message")]  
        public string message { get; set; }
    }
}