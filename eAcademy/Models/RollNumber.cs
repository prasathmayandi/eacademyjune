﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class RollNumberList
    {
        public int RollNumberFormatId { get; set; }
        public string RollNumberFormat{get;set;}
        public int AcademicYearId { get; set; }
        public string AcademicYear { get; set; }
        public int ClassId { get; set; }
        public string Class { get; set; }
        public int SectionId { get; set; }
        public string Section { get; set; }
        public bool Status { get; set; }
        public string FormatId { get; set; }
        public string ClassSection { get; set; }
    
    }
    public class RollNumber
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int acYear_Roll { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectClass")]
        public int class_Roll { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectSection")]
        public int Section_Roll { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Roll_number_format_is_required")]
        [StringLength(10, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Roll_number_should_not_exceed_10_characters")]
        public string txt_rollNumber { get; set; }
    }
    public class EditRollNumber
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Roll_number_format_is_required")]
        [StringLength(10, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Roll_number_should_not_exceed_10_characters")]
        public string txt_rollNumber { get; set; }
        public string FormatId { get; set; }
        public int AcademicYearId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public string Status { get; set; }
    }
}