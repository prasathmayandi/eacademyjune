﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SaveHostelStudents
    {
        [Required(ErrorMessage = "Select Academic Year.")]
        public int AcademicYearId { get; set; }
        [Required(ErrorMessage = "Select Class.")]
        public int ClassId { get; set; }
        [Required(ErrorMessage = "Select Section.")]
        public int SectionId { get; set; }
        [Required(ErrorMessage = "Select Student.")]
        public int StudentId { get; set; }
        [Required(ErrorMessage = "Room number is required.")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\)\(]+$", ErrorMessage = "Room number should not allow the special characters like ':', ';', '*', '/' and '\' ")]
        [StringLength(10, ErrorMessage = "Room number should not exceed 10 characters.")]
        public string RoomNumber { get; set; }
    }
}