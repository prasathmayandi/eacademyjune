﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class Mandatory
    {
        [Required]
        public int AcademicYear { get; set; }
        [Required]
        public int Class { get; set; }
        [Required]
        public int Rpt_Section { get; set; }
        [Required]
        public int StudentId { get; set; }
        [Required]
        public int EmployeeName { get; set; }
        [Required]
        public int EmployeeId { get; set; }
        [Required]
        public int EmployeeType { get; set; }
        [Required]
        public int ExamTypeId { get; set; }
        [Required]
        public int ReportType { get; set; }
        [Required]
        public int Academicyear1 { get; set; }
        [Required]
        public int EmpName1 { get; set; }
        [Required]
        public int PaymentType { get; set; }
        [Required]
        public int Feecategory { get; set; }
        [Required]
        public int FeeParticular { get; set; }
       [Required]
        public int Examsubject { get; set; }
        [Required]
        public DateTime Selectdate { get; set; }
       
    }
}