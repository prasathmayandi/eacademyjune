﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class SaveTechEmployee3
    {
        [Required(ErrorMessage = "Select date of joining")]
        public DateTime txt_doj { get; set; }
        [Required(ErrorMessage = "Select class for staff")]
        public string[] Class { get; set; }
        [Required(ErrorMessage = "Select subject for staff")]
        public string[] Subject { get; set; }
        public string txt_AcNo { get; set; }
        public string txt_AcName { get; set; }
        public string txt_BName { get; set; }
        public string EmpRegId { get; set; }
        [Required(ErrorMessage = "Enter employee salary")]
        public int txt_Salary { get; set; }

    }
}