﻿
namespace eAcademy.Models
{
    public class M_TechEmployeeClassSubject
    {
        public int? Classid { get; set; }
        public string ClassName { get; set; }
        public int? Subjectid { get; set; }
        public string SubjectName { get; set; }
    }
}