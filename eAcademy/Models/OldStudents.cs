﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class OldStudents1
    {
        [Required(ErrorMessage="Please select year")]
        public int allAcademicYears { get; set; }
        [Required(ErrorMessage = "Please select class")]
        public int allClass { get; set; }
        [Required(ErrorMessage = "Please select section")]
        public int Section { get; set; }
    }
}