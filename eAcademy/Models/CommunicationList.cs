﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Models
{
    public class CommunicationList
    {
        public string Date { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Filename { get; set; }
        public string To { get; set; }
        public string EventDate { get; set; }
        public string PostedDate { get; set; }
        public string Cid { get; set; }
        public int id { get; set; }
        public string Usertype { get; set; }
        public DateTime? Pdate { get; set; }
        public string Titles { get; set; }
    }
}