﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class FaciltyIncharge
    {
        public int FacilityInchargeId { get; set; }
        [Required(ErrorMessage = "Select Academic Year.")]       
        public int Fac_AcYearId { get; set; }
        public string Fac_AcYear { get; set;}
        [Required(ErrorMessage = "Select Facility.")]
        public int FacId { get; set; }
        public string Fac { get; set; }
        public int EmpRegId { get; set; }
        public string Fac_Incharge { get; set; }
        [Required(ErrorMessage = "Select Employee.")]
        public string EmpId { get; set; }
        public long? EmpContact { get; set; }
        public string Note { get; set; }
        public string FacilityId { get; set; }
        public bool? Status { get; set; }
        public string FacilityIncharge { get; set; }
        public string Facility { get; set; }
        public string EmployeeId { get; set; }
        public string AcademicYear { get; set; }
        public long? InchargeContact { get; set; }
        public string EmpType { get; set; }
    }
}