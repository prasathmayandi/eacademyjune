﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class EditNews
    {
      [Required(ErrorMessage = "Date is required.")]
        public DateTime? tacYSD { get; set; }
        [Required(ErrorMessage = "Heading is required.")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\)\(]+$", ErrorMessage = "Heading should not allow the special characters like ':', ';', '*', '/' and '\' ")]
        [StringLength(30, ErrorMessage = "Heading should contain 3 characters, can not exceed 30 characters.", MinimumLength = 3)]
        public string Heading { get; set; }
        [StringLength(250, ErrorMessage = "Description should  not exceed 250 characters.")]
        public string Desc { get; set; }
        public string NewsId { get; set; }
        public string Status { get; set; } 
    }
}