﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SaveGrade
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int GradeacYear { get; set; }
        public string class_Fees { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Grade_type_is_required")]
        public int GradeType { get; set; }
        public string CheckClass { get; set; }
    }
}