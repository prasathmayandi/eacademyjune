﻿
namespace eAcademy.Models
{
    public class PrimaryUserMailList
    {
        public string PrimaryUserEmail { get; set; }
        public int StudentRegId { get; set; }
        public long? PrimaryUserMobileNum { get; set; }
    }
}