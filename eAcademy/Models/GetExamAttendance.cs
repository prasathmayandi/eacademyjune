﻿using System;

namespace eAcademy.Models
{
    public class GetExamAttendance
    {
        public string year { get; set; }
        public string classs { get; set; }
        public string section { get; set; }
        public string subject { get; set; }
        public string exam { get; set; }
        public DateTime? date { get; set; }
        public int? sRegId { get; set; }
        public int? eRegId { get; set; }
        public string room { get; set; }
    }
}