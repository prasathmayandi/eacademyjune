﻿using System;

namespace eAcademy.Models
{
    public class FeeDetails
    {
        public int FeeId { get; set; }
        public Decimal? Fee { get; set; }
        public int AcYearId { get; set; }
        public string AcYear { get; set; }
        public int ClassId { get; set; }
        public string Class { get; set; }
        public int FeeCatagoryId { get; set; }
        public string FeeCatagory { get; set; }
        public int TermId { get; set; }
        public string Term { get; set; }
        public int TermFeeId { get; set; }
        public Decimal? TermFee { get; set; }
        public DateTime Ldate { get; set; }
        public DateTime Term_Ldate { get; set; }
        public int PaymentTypeId { get; set; }
        public string Remark { get; set; }
        public Decimal? Fine { get; set; }
        public Decimal? AmountCollected { get; set; }
        
    }
    
}