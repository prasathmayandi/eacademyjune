﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class EditActivityIncharge
    {
        public string ActivityInchargeId { get; set; }
        public int Fac_AcYearId { get; set; }
        public string Fac_AcYear { get; set; }
        [Required(ErrorMessage = "Select Facility.")]
        public int FacId { get; set; }
        public string Fac { get; set; }
        public int EmpRegId { get; set; }
        public string Fac_Incharge { get; set; }
        [Required(ErrorMessage = "Select Employee.")]
        public string EmpId { get; set; }
        public long EmpContact { get; set; }
        [StringLength(250, ErrorMessage = "Description should not exceed 250 characters.")]
        public string Note { get; set; }
        public string Status { get; set; }
    }
}