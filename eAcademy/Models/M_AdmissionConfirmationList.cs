﻿
namespace eAcademy.Models
{
    public class M_AdmissionConfirmationList

    {
        public int? OnlineRegid { get; set; }
        public string ApplicationId { get; set; }
        public string PrimaryUserName { get; set; }
        public string StuFirstName { get; set; }
        public string txt_AdmissionClass { get; set; }
        public string Applicationstatus { get; set; }
        public string PrimaryUserEmail { get; set; }
        public string txt_AcademicYear { get; set; }
        public int? AcademicyearId { get; set; }
        public int? ClassId { get; set; }
        public int? StudentAdmissionId { get; set; }
        public string ApplicationSource { get; set; }
        public string StudentName { get; set; }
        public string ApplyClass { get; set; }
        public string Source { get; set; }
        public int? AdmissionId { get; set; }
    }
}