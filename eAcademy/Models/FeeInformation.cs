﻿using System;

namespace eAcademy.Models
{
    public class FeeInformation
    {
        public int? stuRid { get; set; }
        public string FeeCategory { get; set; }
        public Decimal? FeeAmount { get; set; }
        public int? Fine { get; set; }
        public string PaymentMode { get; set; }
        public int? PayAmount { get; set; }
        public string Status { get; set; }
        public string term { get; set; }
    }
}