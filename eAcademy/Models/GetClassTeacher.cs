﻿using System;
namespace eAcademy.Models
{
    public class GetClassTeacher
    {
        public string year { get; set; }
        public string clas { get; set; }
        public string sec { get; set; }
        public int? empId { get; set; }
        public int? stuRegId { get; set; }
        public DateTime? date { get; set; }
        public int month { get; set; }
        public DateTime? monthname { get; set; }
    }
}