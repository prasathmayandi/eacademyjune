﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SaveFeeCategory
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Fee_Category_is_required")]
        [StringLength(30, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Fee_type_should_not_exceed_30_characters﻿")]        
        public string txt_feetype { get; set; }
        public int feeid { get; set; }
        public string Status { get; set; }
    }
}