﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SaveFeeConfig
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "SelectAcademicYear")]
        public int acYear_Fees { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectClass")]       
        public string class_Fees { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "SelectFeeCategory")]       
        public int feeCategory { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Feeisrequired")]       
        public Decimal txt_fees { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Last_date_is_required")]       
        public DateTime txt_ldate { get; set; }

        public Decimal ServicesTax { get; set; }
        public Decimal Total { get; set; }
        public string txt_checkclass { get; set; }
        public string txt_checkterm { get; set; }
        public int[] Termid { get; set; }
        public Decimal[] Fees { get; set; }
        public Decimal[] Tax { get; set; }
        public Decimal[] Term_Total { get; set; }
        public DateTime[] LastDate { get; set; }
        public int Count { get; set; }
    }
}