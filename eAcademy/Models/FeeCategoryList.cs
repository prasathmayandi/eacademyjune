﻿
namespace eAcademy.Models
{
    public class FeeCategoryList
    {
        public int FeeCatId { get; set; }
        public string FeeType { get; set; }
        public bool? Status { get; set; }
        public string FeeCategory { get; set; }
    }
}