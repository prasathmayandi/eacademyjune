﻿using eAcademy.HelperClass;
using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class TimePeriodSchedule
    {
        public int ScheduleID { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_Enter_Schedule_Name")]
        public string txt_Schedule { get; set; }
        public int NumberPeriod { get; set; }
        [StringArrayRequired(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources),
                        ErrorMessageResourceName = "Fill_Periods_in_all_Added_Rows")]
        public string[] Periods { get; set; }
        [StringArrayRequired(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources),
                                ErrorMessageResourceName = "Select_Type_in_all_Added_Rows")]
         public string[] Type { get; set; }
        [TimeSpanValidationAttribute(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Select_StartTime_in_all_Added_Rows")]
        public TimeSpan[] StartTime { get; set; }
        [TimeSpanValidationAttribute(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Select_EndTime_in_all_Added_Rows")]
        public TimeSpan[] EndTime { get; set; }
        public int?[] period_No { get; set; }
        

        public int TotalCount { get; set; }
        public Boolean? Status { get; set; }
        public int TimeScheduleId { get; set; }
        public int[] period_ScheduleId { get; set; }
        public int PeriodS_Id { get; set; }
        public string txt_PeriodTime { get; set; }       
        public string PeriodType { get; set; }       
        public string STime { get; set; }        
        public string ETime { get; set; }      
        public string PeriodName { get; set; }

        
        
    }
    
}