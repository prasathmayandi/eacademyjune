﻿
namespace eAcademy.Models
{
    public class M_PreviousApplicationStatus
    {

        public int ApplicationRegisterNumber { get; set; }
        public string PreStudentName { get; set; }
        public string PreAcademicYear { get; set; }
        public string PreApplyClass { get; set; }
        public string PreApplicationStatus { get; set; }
        public string ApplicationFormat { get; set; }
        public string OnlnlineId { get; set; }
        public int? StudentRegId { get; set; }
        public int? Oid { get; set; }
        public string stuid { get; set; }
        public string OfflineApplicationNumber { get; set; }

        public int? PrimaryuserId { get; set; }
        public string PrimaryUserAdmissionId { get; set; }

        public string priAdmissionid { get; set; }
        public int Priadmission_id { get; set; }
    }
}