﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class EditHostelStudent
    {
        public string HostelStdID { get; set; }
        [Required(ErrorMessage = "Room number is required.")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\)\(]+$", ErrorMessage = "Room number should not allow the special characters like ':', ';', '*', '/' and '\' ")]
        [StringLength(10, ErrorMessage = "Room number should not exceed 10 characters.")]
        public string RoomNumber { get; set; }
        public string Status { get; set; }
        public int StudentId { get; set; }
    }
}