﻿namespace eAcademy.Models
{
    public class StrengthList
    {
        
        public int StrengthId { get; set; }
        public string Strength_Id { get; set; }
        public string AcademicYear { get; set; }
        public string ClassSection { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        public int? Strength { get; set; }
        public bool? Status { get; set; }
        public int Classid { get; set; }
        public int Acid { get; set; }
        public int Secid { get; set; }
        public string Status1 { get; set; }
    }
}