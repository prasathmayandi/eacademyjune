﻿namespace eAcademy.Models
{
    public class TransportList
    {
        public string AcademicYear { get; set; }
        public string StudentId { get; set; }
        public string RollNumber { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        public string StudentName { get; set; }
        public long? StudentContact { get; set; }
        public string Address { get; set; }
        public long? EmergencyContactPersonNo { get; set; }
        public string EmergencyContactPerson { get; set; }
        public string RoomNumber { get; set; }
        public bool? status { get; set; }
        public string BusNo { get; set; }
    }
}