﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SaveOtherEmployee
    {


        [Required(ErrorMessage = "Enter employee name")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "Employee name cannot exceed 30 characters")]
        public string txt_empname1 { get; set; }     
        [Required(ErrorMessage = "Select dob")]
        public DateTime txt_dob1 { get; set; }
        [Required(ErrorMessage = "Select gender")]
        public string txt_gender1 { get; set; }
        [Required(ErrorMessage = "Enter blood group")]
        public string txt_bgrp1 { get; set; }
        [Required(ErrorMessage = "Enter nationality")]
        public string txt_nationality1 { get; set; }
        [Required(ErrorMessage="Upload employee photo")]
        public string uploadfile1 { get; set; }
        [Required(ErrorMessage = "Enter employee contact")]
        public long txt_empcontact1 { get; set; }
        [Required(ErrorMessage = "Enter address")]
        public string txt_address1 { get; set; }
        [Required(ErrorMessage = "Enter city")]
        public string txt_city1 { get; set; }
        [Required(ErrorMessage = "Enter state")]
        public string txt_state1 { get; set; }
        [Required(ErrorMessage = "Select country")]
        public string CountryId { get; set; }
        [Required(ErrorMessage = "Enter pincode")]
        public string txt_pin1 { get; set; }
        [Required(ErrorMessage = "Enter qualification")]
        public string txt_qualif1 { get; set; }
        [Required(ErrorMessage = "Enter experience")]
        public string txt_Experience1 { get; set; }
        [Required(ErrorMessage = "Select date of joining")]
        public DateTime txt_doj1 { get; set; }
        [Required(ErrorMessage = "Enter emergency person name")]
        public string txt_econper1 { get; set; }
        [Required(ErrorMessage = "Enter emergency person contact")]
        public string txt_econno1 { get; set; }       
       
    }
}