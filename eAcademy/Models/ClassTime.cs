﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class ClassTime
    {
        public Boolean[] Status { get; set; }
        public int?[] ClassID { get; set; }        
        public int?[] ScheduleID { get; set; }
    }
}