﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SaveWarden
    {
        [Required(ErrorMessage = "Select Academic Year.")]
        public int AcademicID { get; set; }
        [Required(ErrorMessage = "Select Employee.")]
        public int EmployeeID { get; set; }
    }
}