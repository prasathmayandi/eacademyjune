﻿using System;
namespace eAcademy.Models
{
    public class M_StudentRegisteration
    {
        
        public string OfflineApplicationId { get; set; }
        public int? OnlineRegid { get; set; }
        public int? Stuadmissionid { get; set; }
        public int? StudentRegisterId { get; set; }
        public string StuFirstName { get; set; }
        public string StuMiddlename { get; set; }
        public string StuLastname { get; set; }
        public string Gender { get; set; }
        public DateTime? DOB { get; set; }
        public string txt_dob { get; set; }
        public string PlaceOfBirth { get; set; }
        public int? Community { get; set; }
        public string Religion { get; set; }
        public string Nationality { get; set; }
        public int? AcademicyearId { get; set; }
        public int? AdmissionClass { get; set; }
        public int? BloodGroup { get; set; }
        public string Height { get; set; }
        public string Weights { get; set; }
        public string IdentificationMark { get; set; }
        public string Email { get; set;}
        public long? Contact { get; set; }
        public string PrimaryUser { get; set; }
        public string primaryUserEmail { get; set; }        
        public string FatherFirstName { get; set; }
        public string FatherMiddleName { get; set; }
        public string FatherLastName { get; set; }
        public string FatherQualification { get; set; }
        public DateTime? FatherDOB { get; set; }
        public string txt_Fatherdob { get; set; }
        public string FatherOccupation { get; set; }
        public long? FatherMobileNo { get; set; }
        public string FatherEmail { get; set; }
        public string MotherFirstname { get; set; }
        public string MotherMiddleName { get; set; }
        public string MotherLastName { get; set; }
        public DateTime? MotherDOB { get; set; }
        public string txt_Motherdob { get; set; }
        public string MotherQualification { get; set; }
        public string MotherOccupation { get; set; }
        public long? MotherMobileNo { get; set; }
        public string MotherEmail { get; set; }
        public string GuardianRequried { get; set; }
        public long? TotalIncome { get; set; }
        public string UserCompanyname { get; set; }
        public string CompanyAddress1 { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyState { get; set; }
        public string CompanyCountry { get; set; }
        public long? CompanyPostelcode { get; set; }
        public long? CompanyContact { get; set; }
        public string GuardianFirstname { get; set; }
        public string GuardianMiddleName { get; set; }
        public string GuardianLastName { get; set; }
        public DateTime? GuardianDOB { get; set; }
        public string txt_Guardiandob { get; set; }
        public string GuardianGender { get; set; }
        public string GuRelationshipToChild { get; set; }
        public string GuardianQualification { get; set; }
        public string GuardianOccupation { get; set; }
        public long? GuardianMobileNo { get; set; }
        public string GuardianEmail { get; set; }
        public long? GuardianIncome { get; set; }
        public string LocAddress1 { get; set; }
        public string LocAddress2 { get; set; }
        public string LocCity { get; set; }
        public string LocState { get; set; }
        public string LocCountry { get; set; }
        public long? LocPostelcode { get; set; }
        public int? Distance { get; set; }
        public string PerAddress1 { get; set; }
        public string PerAddress2 { get; set; }
        public string PerCity { get; set; }
        public string PerState { get; set; }
        public string PerCountry { get; set; }
        public long? PerPostelcode { get; set; }
        public string EmergencyContactPersonName { get; set; }
        public long? EmergencyContactNumber { get; set; }
        public string ContactPersonRelationship { get; set; }
        public string StudentPhoto { get; set; }
        public string BirthCertificate { get; set; }
        public string CommunityCertificate { get; set; }
        public string TransferCertificate { get; set; }
        public string IncomeCertificate { get; set; }
        public int? NoOfSbling { get; set; }
        public string[] Ayear { get; set; }
        public string[] Aroll { get; set; }
        public string[] Aclass { get; set; }
        public string[] Asec { get; set; }
        public string WorkSameSchool { get; set; }
        public string PreSchool{ get; set; }
        public string PreMedium { get; set; }
        public int? Pre_BoardofSchool{ get; set; }
        public string PreClass { get; set; }
        public string  PreMarks{ get; set; }
        public DateTime? PreFromDate1{ get; set; }
        public DateTime? PreToDate{ get; set; }
        public string txt_PreFromDate1 { get; set; }
        public string txt_PreToDate { get; set; }
        public string txt_community { get; set; }
        public string txt_bloodgroup { get; set; }
        public string txt_ApplyClass { get; set; }
        public string txt_LocCountry { get; set; }
        public string txt_PerCountry { get; set; }
        public string txt_ComCountry { get; set; }
        public string txt_academicyear { get; set; }
        public string txt_worksameschool { get; set; }
        public string txt_Section { get; set; }
        public string txt_Class { get; set; }
        public string StudentId { get; set; }
        public string StudentRollNumber { get; set; }
        public int? EmployeeDesignationId { get; set; }
        public string EmployeeId { get; set; }
        public int ReceiptNo { get; set; }
        public long? primaryUserContact { get; set; }
        public bool? EmailRequired { get; set; }
        public bool? SmsRequired { get; set; }
        public bool? TransportRequired { get; set; }
        public int? TransportDestination { get; set; }
        public int? TransportPickpoint { get; set; }
        public Decimal? TransportFeeAmount { get; set; }
        public bool? HostelRequired { get; set; }
        public int? AccommodationFeeCategoryId { get; set; }
        public int? AccommodationSubFeeCategoryId { get; set; }
        public int? FoodFeeCategoryId { get; set; }
        public int? FoodSubFeeCategoryId { get; set; }
        public string txt_transportDestination { get; set; }
        public string txt_transportPickpoint { get; set; }
        public string txt_HostelAccomodation { get; set; }
        public string txt_HostelAccomodationSubCategory { get; set; }
        public string txt_HostelFood { get; set; }
        public string txt_HostelFoodSubCategory { get; set; }
        public string UserName { get; set; }
    }
}