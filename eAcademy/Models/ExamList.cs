﻿
namespace eAcademy.Models
{
    public class ExamList
    {
        public int ExamId { get; set; }
        public string Exam { get; set; }
        public string eid { get; set; }
        public bool? status { get; set; }
    }
}