﻿using System;

namespace eAcademy.Models
{
    public class M_FeeStructure
    {
       public string FeeName { get; set; }
       public Decimal? Amount { get; set; }
       public DateTime? LastDate { get; set; } 
       public string FeespaidDate { get; set; }
       public int? TermId { get; set; }
       public string TermName { get; set; }
       public string TermFeeName { get; set; }
       public Decimal? TermFeeAmount { get; set; }
       public DateTime? TermFeeLastDate { get; set; }
       public string FeeCategory { get; set; }
       public int? FeeCategoryId { get; set; }
       public string PaymentType { get; set; }
       public int? PaymentTypeId { get; set; }
       public string Ldate { get; set; } 
       public int FeeId { get; set; }
       public int TermFeeId { get; set; }
       public Decimal? ServicesTax { get; set; }
       public Decimal? Total { get; set; }
   
    }
}