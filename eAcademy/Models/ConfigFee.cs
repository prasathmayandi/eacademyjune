﻿using System;
namespace eAcademy.Models
{
    public class ConfigFee
    {
        public int FeeId { get; set; }
        public int AcYearId { get; set; }
        public string AcYear { get; set; }
        public int ClassId { get; set; }
        public string Class { get; set; }
        public int FeeCategoryId { get; set; }
        public string FeeCategory { get; set; }
        public Decimal? Amount { get; set; }
        public Decimal? Tax { get; set; }
        public Decimal? Total { get; set; }
        public Decimal? ReFund { get; set; }
        public DateTime LastDate { get; set; }
        public string Reason { get; set; }
        public string FeeConfigId { get; set; }
        public string Date { get; set; }
        public Decimal? TotalAmount { get; set; }
        public Decimal? NetAmount { get; set; }
        public int? EmployeeRegId { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Term { get; set; }
        public int? TermId { get; set; }
        public DateTime dateofmadified { get; set; }
        public string AcademicYear { get; set; }
        public string LastDates { get; set; }
        public Decimal? PayableAmount { get; set; }
    }
}