﻿
namespace eAcademy.Models
{
    public class M_ClassVacancy
    {
        public int AcademicYearId { get; set; }
        public int? classid { get; set; }
        public string txt_class { get; set; }
        public int? Vacancy { get; set; }
        public bool? status { get; set; }
        public string  AgeLimit { get; set; }
        public Date AgeLimit2 { get; set; }
    }
}