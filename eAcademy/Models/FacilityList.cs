﻿
namespace eAcademy.Models
{
    public class FacilityList
    {
        public string FacilityId { get; set; }
        public string Facility { get; set; }
        public string Desc { get; set; }
        public bool? Status { get; set; }
        public string Description { get; set; }
    }
}