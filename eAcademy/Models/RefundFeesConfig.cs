﻿using System;

namespace eAcademy.Models
{
    public class RefundFeesConfig
    {
        public string FeeCategoryname { get; set; }
        public Decimal? CollectedAmount { get; set; }  
        public Decimal? RefundAmount { get; set; }
        public int FeeId { get; set; }                            
    }
}