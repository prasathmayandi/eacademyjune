﻿using System;

namespace eAcademy.Models
{
    public class RoleList
    {
        public int RoleId { get; set; }
        public string Role { get; set; }
        public Boolean? Status { get; set; }
        public string RID { get; set; }
    }
}