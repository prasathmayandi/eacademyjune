﻿using System;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class SaveTechEmployee
    {

        [Required(ErrorMessage = "Enter Employee Name")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\.\)\(]+$", ErrorMessage = "The characters ':', '.' ';', '*', '/' and '\' are not allowed")]
        [StringLength(30, ErrorMessage = "Employee name cannot exceed 30 characters")]
        public string txt_empname { get; set; }
        [Required(ErrorMessage = "Select DOB")]
        public DateTime txt_dob { get; set; }
        [Required(ErrorMessage = "Select Gender")]
        public string txt_gender { get; set; }
        [Required(ErrorMessage = "Select Marital Status")]
        public string txt_MaritalStatus { get; set; }
        [Required(ErrorMessage = "Enter Blood Group")]
        public string txt_bgrp { get; set; }
        [Required(ErrorMessage = "Enter PAN Number")]
        public string txt_PAN { get; set; }
        [Required(ErrorMessage = "Enter Nationality")]
        public string txt_nationality { get; set; }
        [Required(ErrorMessage = "Upload Photo")]
        public HttpPostedFileBase txt_EmpPhoto { get; set; }
       [Required(ErrorMessage = "Enter Mobile Number")]
        public long txt_Mobile { get; set; }
        [Required(ErrorMessage = "Enter Email Address")]
        public string txt_email { get; set; }
       [Required(ErrorMessage = "Enter Address Line1")]
        public string txt_addressLine1 { get; set; }
        [Required(ErrorMessage = "Enter City")]
        public string txt_city { get; set; }
        [Required(ErrorMessage = "Enter State")]
        public string txt_state { get; set; }
        [Required(ErrorMessage = "Enter ZIP code")]
        public string txt_pin { get; set; }
        [Required(ErrorMessage = "Enter Country")]
        public string Country { get; set; }
       [Required(ErrorMessage = "Add Degree Certificate")]
        public HttpPostedFileBase txt_Certificate { get; set; }
       
        public string txt_Experience { get; set; }
        [Required(ErrorMessage = "Add Resume")]
        public HttpPostedFileBase txt_Resume { get; set; }
       [Required(ErrorMessage = "Select Date of Joining")]
        public DateTime txt_doj { get; set; }
        [Required(ErrorMessage = "Select Class for Staff")]
        public DateTime txt_Class { get; set; }
        [Required(ErrorMessage = "Select Subject for Staff")]
        public DateTime txt_Subject { get; set; }
 }
}