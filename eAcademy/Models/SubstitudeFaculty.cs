﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SubstitudeFaculty
    {
        [Required(ErrorMessage="Please select date")]
        public DateTime DateOfSubstitude { get; set; }
        [Required(ErrorMessage = "Please select year")]
        public int YearId { get; set; }
        [Required]
        public int DayOrder { get; set; }
        [Required]
        public string[] ClassId { get; set; }
        [Required]
        public string[] SectionId { get; set; }
        [Required]
        public string[] SubjectId { get; set; }
        [Required]
        public string[] PeriodId { get; set; }
        [Required]
        public string[] FacultyId { get; set; }
        [Required]
        public int TotalCount { get; set; }
        [Required]
        public string ActualEmployeeRegId { get; set; }      
    }
}