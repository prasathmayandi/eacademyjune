﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class EditMark
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Maximum_mark_is_required")]
        public int txt_maxMark { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Minimum_mark_is_required")]
        public int txt_minMark { get; set; }
        public string mid { get; set; }
        public int acid { get; set; }
        public int sec_id { get; set; }
        public int cid { get; set; }
        public int sid { get; set; }
    }
}