﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class GuardianStudentList
    {
        public string StudentPhoto { get; set; }
        public string StudentRegId { get; set; }
        [Required(ErrorMessage = "Please select student")]
        public string sel_StudentRegId { get; set; } 
        [Required(ErrorMessage="Please enter student Id")]
        public string txt_StudentId { get; set; }
        [Required(ErrorMessage = "Please select date of birth")]
        public DateTime txt_dob { get; set; }
        public string StudentId { get; set; }
        public DateTime DOB { get; set; }
   }
}
   


 