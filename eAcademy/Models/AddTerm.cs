﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class AddTerm
    {
        [Required(ErrorMessage = "Select Academic Year")]
        public int term_acYear { get; set; }
        [Required(ErrorMessage = "Term is required.")]
        [RegularExpression(@"^[^\\/:*<>?!~',;\)\(]+$", ErrorMessage = "Term should not allow the special characters like ':', ';', '*', '/' and '\' ")]
        [StringLength(20, ErrorMessage = "Term should contain 2 characters, can not exceed 20 characters.", MinimumLength = 2)]
        public string termname { get; set; }
        [Required(ErrorMessage = "Start Date Required")]
        public DateTime tacYSD1 { get; set; }
        [Required(ErrorMessage = "End Date Required")]
        public DateTime tacYED1 { get; set; }
    }
}