﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SaveSection
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Section_is_required")]
        [StringLength(5, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Section_should_not_exceed_5_characters")]        
        public string section { get; set; }

        public int classid { get; set; }
        public int sectionId { get; set; }
        public string txt_ClassType { get; set; }
        
    }
}