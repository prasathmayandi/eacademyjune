﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class AssignClass
    {
        [Required(ErrorMessage="Please select academic year")]
        public int ToAcademicYears { get; set; }
        [Required(ErrorMessage = "Please select class")]
        public int ToClass { get; set; }
        [Required(ErrorMessage = "Please select section")]
        public int ToSection { get; set; }
        public string[] checkboxId { get; set; }
        public string[] OnlineRegisterId { get; set; }
        public string[] StudentAdmissionId { get; set; }
        public int TotalCount { get; set; }
        public string[] StuRegisterId { get; set; }
        public string[] StudentRegisterId { get; set; }
        public string[] Conduct { get; set; }
        public string[] TransferReason { get; set; }
        public string[] StudentStatus { get; set; }
    }
}