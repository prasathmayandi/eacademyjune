﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Models
{
    public class ConfigFee
    {
        public int FeeId { get; set; }
        public int AcYearId { get; set; }
        public string AcYear { get; set; }
        public int ClassId { get; set; }
        public string Class { get; set; }
        public int FeeCategoryId { get; set; }
        public string FeeCategory { get; set; }
        public int Amount { get; set; }
        public DateTime LastDate { get; set; }
        
    }
}