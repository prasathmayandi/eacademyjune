﻿using System;
namespace eAcademy.Models
{
    public class SaveFeeCollection
    {
        public string FeeCollectiondate { get; set; }
        public int AcademicYearId { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int StudentRegId { get; set; }
        public Decimal FeeAmount { get; set; }
        public Decimal AmountCollected { get; set; }
        public Decimal Fine { get; set; }
        public Decimal Discount { get; set; }
        public Decimal VAT { get; set; }
        public string PaymentMode { get; set; }
        public string BankName { get; set; }
        public string ChequeNumber { get; set; }
        public string DDNumber { get; set; }
        public string Remark { get; set; }
        public int[] FeeCategoryId { get; set; }
        public int[] PaymentTypeId { get; set; }
        public int[] SubFeeCategoryId { get; set; }
        public int[] SubPaymentTypeId { get; set; }
    }
}