﻿
namespace eAcademy.Models
{
    public class CRSection
    {

        public int[] CRid { get; set; }
        public int[] SSId { get; set; }
        public int Count { get; set; }
        public int? Acyear { get; set; }
        public int Class { get; set; }
    }
}