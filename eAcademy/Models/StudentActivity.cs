﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class StudentActivity
    {
        public int AcYearId { get; set; }
        public string AcYear { get; set; }
        public int ClassId { get; set; }
        public string Class { get; set; }
        public int SectionId { get; set; }
        public string Section { get; set; }
        public int ActivityId { get; set; }
        public string Activity { get; set; }
        public int EmpRegId { get; set; }
        public string ActivityIncharge { get; set; }
        public int StdRegId { get; set; }
        public string Student { get; set; }
        public string Note { get; set; }
    }
    public class SaveStudentActivity
    {
        [Required(ErrorMessage="Please Select Academic Year")]
        public int Fac_AcYearId { get; set; }
        public string AcYear { get; set; }
        [Required(ErrorMessage = "Please Select Class")]
        public int Act_ClassId { get; set; }
        public string Class { get; set; }
        [Required(ErrorMessage = "Please Select Section")]
        public int Act_SectionId { get; set; }
        public string Section { get; set; }
        [Required(ErrorMessage = "Please Select Activity")]
        public int ActivityId { get; set; }
        public string Activity { get; set; }
        public int EmpRegId { get; set; }
        public string ActivityIncharge { get; set; }
        [Required(ErrorMessage = "Please Select Student")]
        public int Act_StudentId { get; set; }
        public string Student { get; set; }
        public string Note { get; set; }
    }
    public class EditStudentActivity
    {
        public string StudentActivityId { get; set; }
        [Required(ErrorMessage = "Please Select Acdemic Year")]
        public int Fac_AcYearId { get; set; }
        public string AcYear { get; set; }
        [Required(ErrorMessage = "Please Section Class")]
        public int Act_ClassId { get; set; }
        public string Class { get; set; }
        [Required(ErrorMessage = "Please Select Section")]
        public int Act_SectionId { get; set; }
        public string Section { get; set; }
        [Required(ErrorMessage = "Please Select Activity")]
        public int ActivityId { get; set; }
        public string Activity { get; set; }
        public int EmpRegId { get; set; }
        public string ActivityIncharge { get; set; }
        [Required(ErrorMessage = "Please Select Student")]
        public int Act_StudentId { get; set; }
        public string Student { get; set; }
        public string Note { get; set; }
        public string Status { get; set; }
    }
}