﻿using System.Collections.Generic;

namespace eAcademy.Models
{
    public enum Status
    {
        Ok,
        Error
    }
    public class JsonResponse
    {
        public Status Status { get; set; }
        public string Message { get; set; }
        public List<string> Errors { get; set; }
    }
}