﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class EmployeeLogin
    {
        [Required]
        public string admin_userid { get; set; }
        [Required]
        public string admin_password { get; set; }
        [Required]
        public string Captcha { get; set; }
    }
}