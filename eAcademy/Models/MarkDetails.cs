﻿
namespace eAcademy.Models
{
    public class MarkDetails
    {
        public int MarkId { get; set; }
        public string MId { get; set; }
        public int AcYearId { get; set; }
        public string AcYear { get; set; }
        public string AcademicYear { get; set; }
        public int ClassId { get; set; }
        public string Class { get; set; }
        public int SectionId { get; set; }
        public string Section { get; set; }
        public int SubjectId { get; set; }
        public string Subject { get; set; }
        public int? MaxMark { get; set; }
        public int? MinMark { get; set; }
        public int? MaximumMark { get; set; }
        public int? MinimumMark { get; set; }
    }
}