﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class AcademicYearValidation
    {

        public int AcYearId { get; set; }
        public string AcYear { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Start_date_is_required")]        
        public DateTime acYSD { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "End_date_is_required")]        
        public DateTime acYED { get; set; }

        public string Edit { get; set; }
        public string Status { get; set; }
        public DateTime AcademicYearStartDate { get; set; }
        public DateTime AcademicYearEndState { get; set; }
        public string AcademicYear { get; set; }
        public bool? AcademicYearStatus { get; set; }

    }
}