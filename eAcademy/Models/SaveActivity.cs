﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class SaveActivity
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Activity_is_required")]
        [StringLength(30, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Activity_should_contain_3_characters_can_not_exceed_20_characters", MinimumLength = 3)]
        public string txt_activity { get; set; }
        [StringLength(300, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Activity_description_should_not_exceed_300_characters")]
        public string txt_activity_desc { get; set; }
    }
}