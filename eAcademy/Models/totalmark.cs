﻿
namespace eAcademy.Models
{
    public class totalmark
    {
        public int? Totalmark { get; set; }
        public string Result { get; set; }
        public int? Rank { get; set; }
        public string ExamType { get; set; }
        public string subjectName { get; set; }
        public int? SubjectMark { get; set; }
        public string type { get; set; }
    }
}