﻿
namespace eAcademy.Models
{
    public class M_TechEmployeeEducational
    {
        public string Cours { get; set; }
        public string Institute { get; set; }
        public string University { get; set; }
        public string YearOfCompletion { get; set; }
        public double? Mark { get; set; }
        public int EducationalDetailsId { get; set; }
    }
}