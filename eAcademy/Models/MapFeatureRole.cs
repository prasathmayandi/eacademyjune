﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class MapFeatureRole
    {
        public int MapRoleFeatureId { get; set; }
        [Required(ErrorMessage = "Select Feature")]
        public int FeatureId { get; set; }
        public string MRid { get; set; }
        public string FeatureType { get; set; }
        [Required(ErrorMessage = "Select Role")]
        public int RoleId { get; set; }
        public string RoleType { get; set; }
        public string FeatureDetails { get; set; }
        public bool Status { get; set; }
        public string status { get; set; }
    }

  
}