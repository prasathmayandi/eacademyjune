﻿using System;

namespace eAcademy.Models
{
    public class EventList
    {
        public int EventId { get; set; }
        public string EventName { get; set; }
        public DateTime EventDate { get; set; }
        public string EventDecribtion { get; set; }
        public string Event_ID { get; set; }
        public string Date { get; set; }
        public bool? Status { get; set; }
        public string Acyear { get; set; }
        public int AcyearId { get; set; }
        public int? AcademicYear { get; set; }
        public string EventDates { get; set; }
        public int HolidayId { get; set; }
        public string Holiday_Id { get; set; }
        public string HolidayName { get; set; }
        public DateTime HolidayDate { get; set; }
        public string HolidayDates { get; set; }
        public string Description { get; set; }

    }
}