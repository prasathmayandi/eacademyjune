﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class SaveFacility
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Facility_is_required")]
        [StringLength(30, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Facility_should_contain_3_characters_can_not_exceed_30_characters", MinimumLength = 3)]
        public string txt_facility { get; set; }

         [StringLength(300, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Facility_description_should_not_exceed_250_characters")]
        public string txt_fac_desc { get; set; }
    }
}