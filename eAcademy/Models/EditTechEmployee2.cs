﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class EditTechEmployee2
    {
        [Required(ErrorMessage = "Add Degree Certificate")]
        public string txt_Certificate { get; set; }      
        public string txt_Experience { get; set; }
        [Required(ErrorMessage = "Add Resume")]
        public string txt_Resume { get; set; }
        public string EmpId { get; set; }
        public int Education_count { get; set; }
        public int[] EducationalDetailsId { get; set; }
        public string[] Course { get; set; }
        public string[] Insitute { get; set; }
        public string[] University { get; set; }
        public string[] Year { get; set; }
        public string[] Mark { get; set; }
        public int Employeement_count { get; set; }
        public int[] EmployeeExperienceId { get; set; }
        public string[] EmpInst { get; set; }
        public string[] Designation { get; set; }
        public string[] Expertise { get; set; }
        public string[] EmpSdate { get; set; }
        public string[] EmpEdate { get; set; }
        public string[] empTotal { get; set; }
    
    }
}