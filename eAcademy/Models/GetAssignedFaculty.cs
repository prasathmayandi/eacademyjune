﻿
namespace eAcademy.Models
{
    public class GetAssignedFaculty
    {
        public string year { get; set; }
        public string classs { get; set; }
        public string sec { get; set; }
        public string sub { get; set; }
        public int? subId { get; set; }
        public int? yearId { get; set; }
        public int? classId { get; set; }
        public int? secId { get; set; }
    }
}