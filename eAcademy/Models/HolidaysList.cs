﻿
namespace eAcademy.Models
{
    public class HolidaysList
    {
        public string Date { get; set; }
        public string Holiday { get; set; }
    }
}