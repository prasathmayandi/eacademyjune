﻿using eAcademy.HelperClass;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class SaveStrength
    {
       [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int Acid { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectClass")]
        public int Cid { get; set; }
        public int[] Secid { get; set; }          
        public int[] Strength { get; set; }
        public int Count { get; set; }
        public string[] Section { get; set; }
		public string Status { get; set; }
    }
    public class EditStrength
    {

        public int StrengthId { get; set; }
        public string Strength_Id { get; set; }
        public string AcademicYear { get; set; }
        public string ClassSection { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_class_strength")]
        public int Strength { get; set; }
        public bool? Status { get; set; }       
        public int Classid { get; set; }
        public int Acid { get; set; }
        public int Secid { get; set; }
        public string Status1 { get; set; }
    }
}