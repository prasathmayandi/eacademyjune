﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class ClassRoom
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Room_number_is_required")]
        [StringLength(20, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Room_number_should_not_exceed_20_characters")]       
        public string RoomNumber { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Room_capacity_is_required")]        
        public int RoomCapacity { get; set; }
        public string ClassRoomId { get; set; }
        public string Status { get; set; }
    }
}