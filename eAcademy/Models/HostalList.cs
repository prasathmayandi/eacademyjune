﻿
namespace eAcademy.Models
{
    public class HostalList
    {
        public int RoomAllotmentId { get; set; }
        public string AcademicYear { get; set; }
        public string StudentId { get; set; }
        public string RollNumber { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        public string StudentName { get; set; }
        public long? StudentContact { get; set; }
        public string Address { get; set; }
        public long? EmergencyContactPersonNo { get; set; }
        public string EmergencyContactPerson { get; set; }
        public string RoomNumber { get; set; }
        public bool? status { get; set; }
        public string BusNo { get; set; }
        public string BlockName { get; set; }
        public string EmployeeType { get; set; }
        public string EmployeeName{ get; set; }
        public string EmployeeDesignation { get; set; }
        public string EmployeeId { get; set; }
        public long? EmployeeContact { get; set; }
        public string AllomentIdEtype { get; set; }
        }
}    


    
