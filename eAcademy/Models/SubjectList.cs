﻿using System;

namespace eAcademy.Models
{
    public class SubjectList
    {
        public int SubjectId { get; set; }
        public string Subject { get; set; }
        public string Non_Subject { get; set; }
        public Boolean? Status { get; set; }
        public Boolean? GroupSubjectStatus { get; set; }
        public string SubId { get; set; }
    }
}