﻿using eAcademy.HelperClass;
using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class ExamAttendanceStudentList
    {
        public string StudentRegId { get; set; }
        public string RollNumber { get; set; }
        public string StudentName { get; set; }
        public string RollNumberId { get; set; }
        public string rid { get; set; }
        public string AttStatus { get; set; }
        public string Comment { get; set; }
    }
    public class SaveExamAttendance
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectAcademicYear")]  
        public int AcademicYears { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectExam")]  
        public int Exam { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectDate")]
        public DateTime DateVal { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectSchedule")]  
        public int Schedule { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectRoom")]  
        public int ClassRoomId { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "PleaseSelectClass")]  
        public string ClassWithSection { get; set; }
        public int TotalCount { get; set; }
        public string[] AttendanceStatus { get; set; }
        public string[] rid { get; set; }
        public string[] Rollnumberid { get; set; }
        public string[] Comment { get; set; }
        public string[] StudentRegId { get; set; }
        public string Date { get; set; }
    }
}