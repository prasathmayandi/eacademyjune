﻿
namespace eAcademy.DataModel
{
    public class getclassandsection
    {
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public string ClassTeacher { get; set; }
        public string subjectName { get; set; }
        public int StuRegId { get; set; }
        public string StudentName { get; set; }
        public string ExamType { get; set; }
        public int? SubjectMark { get; set; }
        public int? Totalmark { get; set; }
        public string Grade { get; set; }
        public string rollnumber { get; set; }
        public int? StudentRegId { get; set; }
        public string StudentId { get; set; }
        public string Rollnumber { get; set; }
        public int ClassId { get; set; }
        public int EmpRegId { get; set; }
        public int SectionId { get; set; }
        public int? subjectId { get; set; }
        public int? Id { get; set; }
        public int? ExamId { get; set; }
        public string ExamName { get; set; }
    }
}