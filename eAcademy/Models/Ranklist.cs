﻿using System;

namespace eAcademy.Models
{
    public class Ranklist
    {
        public int? sturegid { get; set; }
        public string StudentId { get; set; }
        public string rollnumber { get; set; }
        public string stuname { get; set; }
        public string StudentName { get; set; }
        public int? total { get; set; }
        public int? rank { get; set; }
        public string status { get; set; }
        public DateTime date { get; set; }
        public DateTime? from1 { get; set; }
        public DateTime? to1 { get; set; }
        public int? totalleave { get; set; }
        public string Result { get; set; }
        public string d { get; set; }
        public string Rank { get; set; }
    }
}