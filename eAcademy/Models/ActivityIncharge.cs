﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class ActivityIncharge
    {
        public int ActivityInchargeId { get; set; }
        [Required(ErrorMessage = "Select Academic Year.")]
        public int Fac_AcYearId { get; set; }
        public string Fac_AcYear { get; set; }
        [Required(ErrorMessage = "Select Activity.")]
        public int ActivityId { get; set; }
        public string Activity { get; set; }
        public int EmpRegId { get; set; }
        public string Fac_Incharge { get; set; }
        [Required(ErrorMessage = "Select Employee.")]
        public string EmpId { get; set; }
        public long? EmpContact { get; set; }
        public string Note { get; set; }
        public int StudentActivityId { get; set; }
        public int ClassId { get; set; }
        public string Class { get; set; }
        public int SectionId { get; set; }
        public string Section { get; set; }
        public int StdRegId { get; set; }
        public string Student { get; set; }
        public string AIID { get; set; }
        public bool? Status { get; set; }
        public string ClassSection { get; set; }
        public string StdActivityId { get; set; }
       [StringLength(250, ErrorMessage = "Description should not exceed 250 characters.")]
        public string Desc { get; set; }
       public string Description { get; set; }
       public string AcademicYear { get; set; }
       public string ActivityIncharges { get; set; }
       public string EmployeeId { get; set; }
       public long? EmployeeContactNo { get; set; }       
    }
}