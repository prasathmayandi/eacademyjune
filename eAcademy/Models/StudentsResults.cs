﻿namespace eAcademy.Models
{
    public class StudentsResults
    {
        public int StudentRegId { get; set; }
        public string StudentName { get; set; }
        public string StudentRollNumber { get; set; }
        public string StudentId { get; set; }
        public int ExamId { get; set; }
        public string Result { get; set; }
        public string StudentEducationStatus { get; set; }
        public string SchoolingCompleted { get; set; }
        public string FinalResult { get; set; }
        public string ClassAssignedForNextAcYear { get; set; }
        public int Subject { get; set; }
        
    }
}