﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class AcademicId
    {
        [Required(ErrorMessage = "Select Academic Year")]
        public int AcYearId { get; set; }
        [Required(ErrorMessage = "Select Class")]
        public int ClassId { get; set; }
        [Required(ErrorMessage = "Select Section")]
        public int SectionId { get; set; }
        [Required(ErrorMessage = "Select Student")]
        public int StudentId { get; set; }
       
    }
}