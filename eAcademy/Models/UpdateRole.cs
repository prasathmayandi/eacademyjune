﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class UpdateRole
    {
        public int roleid { get; set; }
        public string rid { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Role_name_is_required")]
        [StringLength(35, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Role_name_should_contain_3_characters_can_not_exceed_35_characters", MinimumLength = 3)]               
        public string role { get; set; }
        
        public string status { get; set; }
    }
}