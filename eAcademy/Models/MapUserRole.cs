﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class MapUserRole
    {
        public int MapRoleUserId { get; set; }
        public string MapRoleId { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Select_Role")]
        public int RoleId { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Select_User")]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string RoleType { get; set; }
        public bool Status { get; set; }
        public string Status1 { get; set; }
    }
}