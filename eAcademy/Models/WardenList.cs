﻿namespace eAcademy.Models
{
    public class WardenList
    {
        public int wid { get; set; }
        public int? acid { get; set; }
        public int? empid { get; set; }
        public string acYear { get; set;}
        public string ename { get; set; }
        public long? empContact { get; set; }
        public string empAddress { get; set; }
        public bool? status { get; set; }
        public string WardenID { get; set; }
    }
}