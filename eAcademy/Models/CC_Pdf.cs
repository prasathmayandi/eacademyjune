﻿
namespace eAcademy.Models
{
    public class CC_Pdf
    {
        public int? StudentRegId { get; set; }
        public int? AdmissionId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PrimaryUserFirstName { get; set; }
        public string FromYear { get; set; }
        public string toYear { get; set; }
        public string FromClass { get; set; }
        public string toClass { get; set; }
        public string ConductStatus { get; set; }
        public string Gender { get; set; }
    }
}