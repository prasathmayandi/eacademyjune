﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class M_NewUser
    {
        [Required(ErrorMessage="OfflineApplicationId is Required")]
        public string OfflineApplicationId { get; set; }
        [Required(ErrorMessage="Username is Required")]       
        public string PrimaryEmail { get; set; }
        public string enrolltype { get; set; }
    }
}