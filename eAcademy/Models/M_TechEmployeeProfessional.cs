﻿
namespace eAcademy.Models
{
    public class M_TechEmployeeProfessional
    {
        public string Course { get; set; }
        public string Certificate { get; set; }
        public string File { get; set; }
        public int ProfessionalDetailsId { get; set; }
    }
}