﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SaveTerm
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "SelectAcademicYear")]
        public int acYear { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Term_is_required")]
        [StringLength(20, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Term_should_contain_2_characters_can_not_exceed_20_characters", MinimumLength = 2)]
        public string txt_termname { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Start_date_is_required")]
        public DateTime tacYSD { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "End_date_is_required")]
        public DateTime tacYED { get; set; }

        public string TermID { get; set; }
    }
}