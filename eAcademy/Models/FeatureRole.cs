﻿
namespace eAcademy.Models
{
    public class FeatureRole
    {
        public int fid { get; set; }
        public string link { get; set; }
        public string feature { get; set; }
        public string gname { get; set; }
        public int order { get; set; }
        public string role { get; set; }
        public string cname { get; set; }
        public int groupOrder { get; set; }
    }
}