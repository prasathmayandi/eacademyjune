﻿namespace eAcademy.Models
{
    public class ClassStudent
    {
        public int StudentRegId { get; set; }
        public string StdRegid { get; set; }
        public string StudentName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string GuardianName { get; set; }
        public long? FatherContact { get; set; }
        public long? MotherContact { get; set; }
        public long? GuardiaContact { get; set; }
        public int? ClassId { get; set; }
        public int? SectionId { get; set; }
        public string Hostel { get; set; }
        public string Emg_Person { get; set; }
        public long? Emg_Contact { get; set; }
        public string ParentContact { get; set; }
        public int ExamId { get; set; }
        public string Exam { get; set; }
        public int SubjectId { get; set; }
        public string Subject { get; set; }
        public int MarkId { get; set; }
        public int Mark { get;set;}
        public int MaxMark { get; set; }
        public int MinMark { get; set; }
        public int Total { get; set; }
        public string RollNumber { get; set; }
        public int AcademicYearId { get; set; }
        public string StudentId { get; set; }
        public string ParentName { get; set; }
        public string GuardianDetais { get; set; }
        public string PrimaryUserName { get; set; }
        public string PrimaryUserEmail { get; set; }
        public string EmergencyContactPerson { get; set; }
        public long? ContactpersonContact { get; set; }
       
    }
}