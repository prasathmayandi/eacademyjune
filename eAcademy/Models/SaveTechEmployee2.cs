﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class SaveTechEmployee2
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_upload_employee_photo")]        
        public string txt_empphoto { get; set; }
        
        public string txt_Experience { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_enter_employee_salary")]        
        public long? txt_Salary { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Please_select_date_of_join")]       
        public DateTime txt_doj { get; set; }

        public string EmpId { get; set; }
        public int  Education_count { get; set; }
        public string[] Course { get; set; }
        public string Insitute { get; set; }
        public string University { get; set; }
        public string Year { get; set; }
        public string Mark { get; set; }
    }
}