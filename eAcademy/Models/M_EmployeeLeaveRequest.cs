﻿using System;
namespace eAcademy.Models
{
    public class M_EmployeeLeaveRequest
    {
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string status { get; set; }
        public DateTime date { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int? TotalLeave { get; set; }
        public DateTime? sdate { get; set; }
        public DateTime? Edate { get; set; }
    }
}