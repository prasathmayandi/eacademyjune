﻿namespace eAcademy.Models
{
    public class ELearnReplyList
    {
        public string Description { get; set; }
        public string Subject { get; set; }
        public string Title { get; set; }
        public string SubmissionDate { get; set; }
        public string[] Id { get; set; }
        public string[] ELearn_comment { get; set; }
        public int TotalCount { get; set; }
    }
}