﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Models
{
    public class EditAnnouncement
    {
       
        public string AnnouncementId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Announcement_name_is_required")]
        [StringLength(50, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Announcement_should_contain_3_characters_can_not_exceed_50_characters", MinimumLength = 3)]
        public string txt_Announcement { get; set; }

        [StringLength(300, ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Announcement_description_should_not_exceed_300_characters")]       
        public string txt_Announcement_desc { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Globalisation.AdminResources), ErrorMessageResourceName = "Announcement_date_is_required")]
        public DateTime txt_Announcement_date { get; set; }

        public string Status { get; set; }
        public bool? Push_notification { get; set; }
    }
}