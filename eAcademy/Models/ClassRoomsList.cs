﻿namespace eAcademy.Models
{
    public class ClassRoomsList
    {
        public string ClassRoomId { get; set; }
        public string RoomNumber { get; set; }
        public int? Capacity { get; set; }
        public bool? Status { get; set; }
        public int CRid { get; set; }
    }
}