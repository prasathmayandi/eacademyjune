﻿using System;
namespace eAcademy.Models
{
    public class ActiveClass
    {
        public int ClassId { get; set; }
        public string ClassType { get; set; }
        public Boolean Status { get; set; }
        public string ScheduleName { get; set; }
        public int ScheduleId { get; set; }
    }
}