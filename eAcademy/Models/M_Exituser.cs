﻿using System.ComponentModel.DataAnnotations;

namespace eAcademy.Models
{
    public class M_Exituser
    {
        [Required(ErrorMessage="ApplicationId is Required")]
        public string OfflineApplicationId {get;set;}
        public string ParentId {get;set;}
        public string StudentId { get; set; }
    }
}