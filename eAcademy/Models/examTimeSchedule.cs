﻿
namespace eAcademy.Models
{
    public class examTimeSchedule
    {
        public string ExamScheduleId { get; set; }
        public string ScheduleName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Status { get; set; }

        public int ScheduleId { get; set; }
    }
}