﻿using eAcademy.Areas.HostelManagement.Services;
using eAcademy.Areas.HostelManagement.ViewModel;
using eAcademy.HelperClass;
using eAcademy.Services;
using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using eAcademy.Areas.HostelManagement.LocalResources;
using eAcademy.Models;
namespace eAcademy.Areas.HostelManagement.Controllers
{
    [CheckSessionOutAttribute]
    public class HostelFeesCategoryController : Controller
    {
        AcademicyearServices obj_academicYear = new AcademicyearServices();
        HostelFeesCategoryServices obj_hosfeecategory = new HostelFeesCategoryServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        public int AdminId;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AdminId = Convert.ToInt32(Session["empRegId"]);
            ViewBag.acYear = obj_academicYear.AllList_AcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (Session["CurrentCulture"] != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["CurrentCulture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["CurrentCulture"].ToString());
            }
        }
        public ActionResult AddHostelFeescategory()
        {
            return View();
        }
        [HttpPost]
        public JsonResult AddHostelFeescategory(FeesCategory F)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var categoryname = obj_hosfeecategory.Category_Name(F.feescategoryname, F.AcYearId);
                    if (categoryname != null)
                    {
                        string BlockNameAlreadyExistsMessage = Resource.FeesCategorytypealreadyexistinthisacademicyear;
                        return Json(new { Errormessage = BlockNameAlreadyExistsMessage, JsonRequestBehavior.AllowGet });
                    }
                    else
                    {
                        obj_hosfeecategory.AddHostelFeesCategory(F.feescategoryname, F.camount, F.tax, F.AcYearId, AdminId);
                        string CategorySuccessMessage = Resource.SucessfullyAddedFeesCategory;
                        useractivity.AddEmployeeActivityDetails(Resource.SucessfullyAddedFeesCategory);
                        return Json(new { Message = CategorySuccessMessage, JsonRequestBehavior.AllowGet });
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch
            {
                string Message = Resource.ExceptionError;
                return Json(new { ExceptionError = Message, JsonRequestBehavior.AllowGet });
            }
        }
    public JsonResult FeescategoryList(string sord, int page, int rows)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var list = obj_hosfeecategory.categorylist();
            int totalRecords = list.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = list
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
         }
          public ActionResult HostelFeesDetailsExportToExcel()
        {
            try
            {
           var list1 = obj_hosfeecategory.categorylist1();
           string fileName = "HostelFeesdetails";
                GenerateExcel.ExportExcel(list1, fileName);
                return View("AddHostelFeescategory");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult HostelFeesDetailsExportToPDF()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = ResourceCache.Localize("HostelFeesdetails");
                var list1 = obj_hosfeecategory.categorylist1();
                GeneratePDF.ExportPDF_Landscape(list1, new string[] { "Acdemicyear", "HostelFeesCategoryName", "FeeAmount", "Status", "Tax", "Total" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "HostelFeeCategorydetails.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult EditHostelFeescategory(string FeesCategoryId)
        {
            var id = Convert.ToInt32(FeesCategoryId);
            try
            {
                var feesdetails = obj_hosfeecategory.editlist(id);
                bool existacademicyear = obj_academicYear.PastAcademicyear(feesdetails.AcYearId);
                if (existacademicyear == true)
                {
                   
                    return Json(new { feesdetails }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                    return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
           var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UpdateHostelFeescategory(FeesCategory f)
        {
            try
            {
                if (ModelState.IsValid)
                {
             var fname = obj_hosfeecategory.existname_list(f.feescategoryname, f.FeesCategoryId, f.AcYearId);
             var feepaidstatus = obj_hosfeecategory.checkfeepaid(f.FeesCategoryId);
             if (fname.Count > 0)
                    {
                        string BlockNameAlreadyExistsMessage = ResourceCache.Localize("FeesCategorytypealreadyexistinthisacademicyear");
                        return Json(new { Errormessage = BlockNameAlreadyExistsMessage, JsonRequestBehavior.AllowGet });
                    }
             else if (feepaidstatus != null)
             {
                 string BlockNameAlreadyExistsMessage = "Collecting fees for this category. So can't Edit";
                 return Json(new { Errormessage = BlockNameAlreadyExistsMessage, JsonRequestBehavior.AllowGet });
             }
                    else
                    {
                        obj_hosfeecategory.editable_list(f.FeesCategoryId, f.feescategoryname, f.camount, f.status, AdminId, f.tax);
                        string CategorySuccessMessage = ResourceCache.Localize("SucessfullyUpdatedFeesCategory");
                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("SucessfullyUpdatedFeesCategory"));
                        return Json(new { Message = CategorySuccessMessage, JsonRequestBehavior.AllowGet });
                    }
                }
           else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}