﻿using eAcademy.Areas.HostelManagement.Models;
using eAcademy.Areas.HostelManagement.Services;
using eAcademy.HelperClass;
using eAcademy.Services;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Threading;
using System.Globalization;
using eAcademy.Areas.HostelManagement.LocalResources;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Areas.HostelManagement.Resources;
namespace eAcademy.Areas.HostelManagement.Controllers
{
    [CheckSessionOutAttribute]
    public class HostelFeesCollectionController : Controller
    {
        AcademicyearServices obj_academicYear = new AcademicyearServices();
        ClassServices obj_class = new ClassServices();
        StudentServices obj_student = new StudentServices();
        HostelFeesCollectionServices obj_hosfeecollection = new HostelFeesCollectionServices();
        StudentRollNumberServices obj_sturollnumber = new StudentRollNumberServices();
        HostelFeesCategoryServices obj_hosfeecategory = new HostelFeesCategoryServices();
        HostelFeesCollectioncategoryServices obj_hosfeecolcategory = new HostelFeesCollectioncategoryServices();
        SectionServices obj_section = new SectionServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        public int AdminId;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AdminId = Convert.ToInt32(Session["empRegId"]);
            AcademicyearServices AcYear = new AcademicyearServices();
            ClassServices clas = new ClassServices();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.acYear = obj_academicYear.AllList_AcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
            ViewBag.allClasses = clas.ShowClasses();
        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (Session["CurrentCulture"] != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["CurrentCulture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["CurrentCulture"].ToString());
            }
        }
   
        public JsonResult GetSelectedClassSection(int passClassId)
        {
            SectionServices sec = new SectionServices();
            var List = sec.getSection(passClassId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        public JsonResult showStudents(int passYearId, int passClassId, int passSecId)
        {
            try
            {
                AssignClassToStudentServices stu = new AssignClassToStudentServices();
                var StudentList = stu.getStudents(passYearId, passClassId, passSecId);
                return Json(StudentList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getStudentRegisterId(int AcYearId, int ClassId, int SectionId, int StudentId)
        {
            StudentServices obj_student = new StudentServices();
            var studentregisternumber = obj_student.getStudentId(AcYearId, ClassId, SectionId, StudentId);
            string stu_id = studentregisternumber.StudentId;
            return Json(stu_id, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getStudentDetailsByRollNumber(string RegNo)
        {
            StudentServices obj_student = new StudentServices();
            try
            {
                var ans = obj_student.getStudentByStudentId(RegNo);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getClass()
        {
            ClassServices obj_class = new ClassServices();
            try
            {
                var ans = obj_class.Classes();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getSection(int cid)
        {
            SectionServices obj_section = new SectionServices();
            try
            {
                var ans = obj_section.getSectionByClassId(cid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getStudent(int acid, int sec_id, int cid)
        {
            StudentServices obj_student = new StudentServices();
            try
            {
                var ans = obj_student.getStudentByIDs(sec_id, cid, acid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        
        public ActionResult CollectHostelFee_Hostel()
        {
            try
            {
             var result = obj_hosfeecollection.feesid();
                if (result != 0)
                {
                    
                    ViewBag.recID = result + 1;
                }
                else
                {
                    ViewBag.recID = 1;
                }
                ViewBag.sysDate = DateTimeByZone.getCurrentDateTime().Date.ToString("dd/MM/yyyy");
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
                
        public JsonResult getStudentFeeDetails_Hostel(int acid, string StudentRegId)
        {
            try
            {
                if (ModelState.IsValid)
                {
                  
                    var stuid = Convert.ToInt16(StudentRegId);
                    var existregid = obj_hosfeecollection.Exist_student(stuid, acid);
                     if (existregid != null)
                     {
                         string ExceptionError1 = ResourceCache.Localize("StudentAlreadypaidhostelfeesthisacademicyear");
                         return Json(new { ExceptionError = ExceptionError1 }, JsonRequestBehavior.AllowGet);
                     }
                     else
                     {
                         var ans = obj_hosfeecollection.studentfeesdetails(acid);
                         return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
                     }
                    
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
   
        [HttpPost]
        public JsonResult CollectFee_Hostel(SaveFeeCollection1 model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DateTime FCDate = DateTimeByZone.getCurrentDate(); //DateTime.Parse(model.FeeCollectiondate.ToString());
                    int acid = model.AcademicYearId;
                    int stu_id =Convert.ToInt16(model.StudentRegId);
                    Decimal feeAmount = model.FeeAmount;
                    var existregid = obj_hosfeecollection.Exist_student(stu_id, acid);
                    if (existregid != null)
                    {
                        string ExceptionError1 = ResourceCache.Localize("StudentAlreadypaidhostelfeesthisacademicyear");
                        return Json(new { ExceptionError = ExceptionError1 }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        feeAmount = Decimal.Parse(string.Format("{0:0.00}", feeAmount));
                        Decimal amountCollected = model.AmountCollected;
                        amountCollected = Decimal.Parse(string.Format("{0:0.00}", amountCollected));
                        Decimal discount = Convert.ToDecimal(model.Discount);
                        discount = Decimal.Parse(string.Format("{0:0.00}", discount));
                        string mode = model.PaymentMode;
                        string bankName = model.BankName;
                        string chequeNumber = model.ChequeNumber;
                        string DDNumber = model.DDNumber;
                        string remark = model.Remark;
                        string paymentMode = "";
                        if (mode == "2")
                        {
                            paymentMode = "Cheque";
                        }
                        else if (mode == "3")
                        {
                            paymentMode = "DD";
                        }
                        else
                        {
                            paymentMode = "Cash";
                        }
                        var feescollectionid = obj_hosfeecollection.savefeescollection(acid, stu_id, feeAmount, amountCollected, paymentMode, bankName, chequeNumber, DDNumber, remark, discount, AdminId, model.SectionId, model.ClassId);
                        int count1 = 0;
                        count1 = model.FeeCategoryId.Count();
                        int[] FeeCatId = new int[count1];
                        if (count1 > 0)
                        {
                            for (int i = 0; i < count1; i++)
                            {
                                FeeCatId[i] = model.FeeCategoryId[i];
                                int id = FeeCatId[i];
                                var idamt = obj_hosfeecategory.category_id(id);
                                decimal Fees_Amount = Convert.ToDecimal(idamt.FeesAmount);
                                decimal Tax_Amount = Convert.ToDecimal(idamt.Tax);
                                decimal totamt = Convert.ToDecimal(idamt.Tax.Value + idamt.FeesAmount.Value);
                                obj_hosfeecolcategory.savefeescollectioncategoryid(feescollectionid, acid, stu_id, model.SectionId, model.ClassId, AdminId, id, Fees_Amount, Tax_Amount, totamt);
                            }
                        }
                        var Encrptid = QSCrypt.Encrypt(feescollectionid.ToString());
                        string SuceessMessage = ResourceCache.Localize("FeeCollectedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("FeeCollectedSuccessfully"));
                        return Json(new { SuceessMessage = SuceessMessage, Encrptid = Encrptid, feescollectionid = feescollectionid }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        //public JsonResult GetClassSection(string StudentRegId)
        //{
        //    try
        //    {
        //        var stid = obj_student.get_stdid(StudentRegId);
        //        var std_id = Convert.ToInt16(stid.StudentRegisterId);
        //        var stclass = obj_sturollnumber.get_sturollnumber(std_id);
        //        string sectionid = Convert.ToString(stclass.SectionId);
        //        string classid = Convert.ToString(stclass.ClassId);
        //        return Json(new { sectionid, classid }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(e.Message, JsonRequestBehavior.AllowGet);
        //    }
        //}
        public ActionResult HostelBillReceipt(string id)
        {
            try
            {
                HostelFeesCollectionServices Fee_Collection = new HostelFeesCollectionServices();
                int FeecolletionId = Convert.ToInt16(QSCrypt.Decrypt(id));
                var list = Fee_Collection.GetFeePaidStudents(FeecolletionId);
                return View(list);
            }
            catch (Exception e)
            {
                return View();
            }
        }
        [HttpGet]
        public JsonResult GetFeeCategory(int feecollectionId)
        {
            try
            {
                HostelFeesCollectionServices FeeCollect = new HostelFeesCollectionServices();
                HostelFeesCollectioncategoryServices FeeColleectCategory = new HostelFeesCollectioncategoryServices();
                var list = FeeColleectCategory.GetFeeCollectionCategory(feecollectionId);
                var data = FeeCollect.GetCollectionDetails(feecollectionId);                
                var discount = data.Discount;
                var totalamount = data.AmountCollected;                
                return Json(new { list = list,  discount = discount, totalamount = totalamount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}            
                        
                       
                       
                     
                        
                        
                      
                     
          
           
       
 
       
       
     
       
       
      
       
      
                   
       
                    