﻿using eAcademy.Areas.HostelManagement.Services;
using eAcademy.Areas.HostelManagement.ViewModel;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using eAcademy.Areas.HostelManagement.LocalResources;
namespace eAcademy.Areas.HostelManagement.Controllers
{
    [CheckSessionOutAttribute]
    public class HostelInfrastructureController : Controller
    {
        EacademyEntities db = new EacademyEntities();
        HostelBlockServices hostel_block = new HostelBlockServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        HostelRoomServices Hostel_room = new HostelRoomServices();
        public int AdminId;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AdminId = Convert.ToInt32(Session["empRegId"]);
        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (Session["CurrentCulture"] != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["CurrentCulture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["CurrentCulture"].ToString());
            }
        }
       public ActionResult AddBlockRoom()
        {
            return View();
        }
          [HttpPost]
           public JsonResult AddBlockRoom(Vm_Block H)
        {
           try
             {
            int count = H.count;
           if (ModelState.IsValid)
                {
          var blockexists = hostel_block.exist_BlocknameAdd(H.name);
           if (blockexists == null)
                    {
 var blockid=hostel_block.addblock(H.name, H.type,AdminId);
                     for (int i = 0; i < count; i++)
                     {
                            var rname = H.roomname[i];
                            var rcapacity = H.roomcapacity[i];
                            var rtype = H.roomtype[i];
                              Hostel_room.Add_room(rname, rcapacity, rtype, blockid, AdminId);
                     }
                     string BlockSuccessMessage =ResourceCache.Localize("SucessfullyAddedBlockRooms");
                     useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("SucessfullyAddedBlockRooms"));
                     return Json(new { Message = BlockSuccessMessage, JsonRequestBehavior.AllowGet });
                   }
                     else
                    {
                        string BlockNameAlreadyExistsMessage = ResourceCache.Localize("BlockNameAlreadyExistsPleasegiveanothername");
                         return Json(new { Errormessage = BlockNameAlreadyExistsMessage, JsonRequestBehavior.AllowGet });
               }
                     }
                else
                      {
                   string totalError = "";
                       string[] t = new string[ModelState.Values.Count];
                      int i = 0;
                      foreach (var obj in ModelState.Values)
                {
     foreach (var error in obj.Errors)
     {
         if (!string.IsNullOrEmpty(error.ErrorMessage))
         {
             totalError = totalError + error.ErrorMessage + Environment.NewLine;
             t[i] = error.ErrorMessage;
             i++;
         }
     }
 }              
return Json(new { Success = 0, ex = t });
                }
}
catch
{
    string Message =ResourceCache.Localize("ExceptionError");
    return Json(new { ExceptionError = Message, JsonRequestBehavior.AllowGet });
}
        }
      private static IList<BlockList> list = new List<BlockList>();
        public JsonResult BlockList(string sord, int page, int rows)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var list = hostel_block.Block_List();     
            int totalRecords = list.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = list
            };
           return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult HostelDetailsExportToExcel()
        {
            try
            {
                var list = hostel_block.Block_List1();
               
                string fileName = "HostelInfrastructuredetails";
                GenerateExcel.ExportExcel(list, fileName);
                return View("AddBlockRoom");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult HostelDetailsExportToPDF()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = ResourceCache.Localize("HostelInfrastructuredetails");
                var list = hostel_block.Block_List1();
               
                GeneratePDF.ExportPDF_Portrait(list, new string[] { "BlockName", "BlockType", "status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "HostelInfrastructuredetails.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
 public JsonResult EditHostelDetails(string BlockId)
        {
           var id=Convert.ToInt32(BlockId);
           try{
               var hosteldetails = hostel_block.Edit_Details(id);
               var roomdetails = Hostel_room.Edit_roomDetails(id);
               return Json(new { roomdetails, hosteldetails }, JsonRequestBehavior.AllowGet);
           }
           catch (Exception e)
           {
               string ExceptionError = e.Message.ToString();
               return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
           }
        }  
                           
public JsonResult UpdateHostelDetails(Vm_Block h)
      {
           HostelBlockServices hostel_block = new HostelBlockServices();
           try
           {
               var b = hostel_block.select_block(h.BlockId);
               var cnt = h.count;
               string[] rname = new string[cnt];
               int[] rcapacity = new int[cnt];
               int[] rmid = new int[cnt];
               string[] rtype = new string[cnt];
             if (ModelState.IsValid)
               {
var blockexists = hostel_block.exist_Blocknameupdate(h.BlockId, h.name);
if (blockexists.Count > 0)
                   {
                       string BlockNameAlreadyExistsMessage = ResourceCache.Localize("BlockNameAlreadyExistsPleasegiveanothername");
                       return Json(new { Errormessage = BlockNameAlreadyExistsMessage, JsonRequestBehavior.AllowGet });
}
 else
   {
                       hostel_block.updateblock(h.name, h.type, h.status, AdminId, h.BlockId);
}
for (int i = 0; i < cnt; i++)
                   {
                       rname[i] = h.roomname[i];
                       var r_name = rname[i];
                       var existroom = Hostel_room.updateroomdetails(h.BlockId);
                       var ecnt = existroom.Count();
                       int flog = 0;
                       for (int j = 0; j < ecnt; j++)
                       {
                           var eroom = existroom[j].RoomName;
                           if (r_name.Equals(eroom))
                           {
                               rmid[i] = h.roomid[i];
                               rname[i] = h.roomname[i];
                               var rcacity = h.roomcapacity[i];
                               rcapacity[i] = Convert.ToInt32(rcacity);
                               rtype[i] = h.roomtype[i];
                               var Rid = rmid[i];
                               var r_type = rtype[i];
                               flog = flog + 1;
                               Hostel_room.update_room(Rid, r_name, rcacity, r_type, AdminId);
                               break;
                           }
                       }
                      
                     if (flog == 0)
                       {
                           rmid[i] = h.roomid[i];
                           if (rmid[i] == 0)
                           {
                               var rnameadd = h.roomname[i];
                               var rcapacityadd = h.roomcapacity[i];
                               var rtypeadd = h.roomtype[i];
                               Hostel_room.Add_room(rnameadd, rcapacityadd, rtypeadd, h.BlockId, AdminId);
                          }
                           rmid[i] = h.roomid[i];
                           var Rid = rmid[i];
                           if (Rid > 0)
                           {
                               rmid[i] = h.roomid[i];
                               rname[i] = h.roomname[i];
                               var rcacity = h.roomcapacity[i];
                               rcapacity[i] = Convert.ToInt32(rcacity);
                               rtype[i] = h.roomtype[i];
                               var r_type = rtype[i];
                               Hostel_room.update_room(Rid, r_name, rcacity, r_type, AdminId);
                           }
                     }
}
                   string BlockSuccessMessage =ResourceCache.Localize("SucessfullyUpdatedBlockRooms");
                   useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("SucessfullyUpdatedBlockRooms"));
                   return Json(new { Message = BlockSuccessMessage, JsonRequestBehavior.AllowGet });
}
               else
               {
                   string totalError = "";
                   string[] t = new string[ModelState.Values.Count];
                   int i = 0;
                   foreach (var obj in ModelState.Values)
                   {
                       foreach (var error in obj.Errors)
                       {
                           if (!string.IsNullOrEmpty(error.ErrorMessage))
                           {
                               totalError = totalError + error.ErrorMessage + Environment.NewLine;
                               t[i] = error.ErrorMessage;
                               i++;
                           }
                       }
                   }
                   return Json(new { Success = 0, ex = t });
               }
           }
           catch
           {
               string Message =ResourceCache.Localize("ExceptionError");
               return Json(new { ExceptionError = Message, JsonRequestBehavior.AllowGet });
           }
       }
       public JsonResult deleteroomdetails(int drid)
       {
           RoomAllotMentServices Room_allotment = new RoomAllotMentServices();
           var result = Room_allotment.Delroomid(drid);
           if (result!= null)
           {
               string roomallotment = ResourceCache.Localize("This_room_allotted_student_or_staff_Can't_delete_this_room");
               return Json(new { Errormessage = roomallotment, JsonRequestBehavior.AllowGet });
           }
           else
           {
               Hostel_room.deleteroom(drid);
               string DeleteSucess = ResourceCache.Localize("Room_Deleted_sucessfully ");
               return Json(new { Message = DeleteSucess, JsonRequestBehavior.AllowGet });
           }
 } 
   public ActionResult StudentRoomAllotment()
       {
           HostelRoomServices Hostel_room = new HostelRoomServices();
           ViewBag.RoomNumber = Hostel_room.GetStudenthostelRoomsList();
           return View();
       }
       public JsonResult CheckStudent(string RegId)
       {
           try
           {
               StudentServices s_student = new StudentServices();
               var correctstudentid = s_student.getStudentByStudentId(RegId);
               if (correctstudentid != null)
               {
                   var alreadyhostelassigned = s_student.getstudentassignhostelornot(RegId);
                   if (alreadyhostelassigned==null)
                   {
                       var student = s_student.GetHostelStudent(RegId);
                       if (student != null)
                       {
                           return Json(new { student = student }, JsonRequestBehavior.AllowGet);
                       }
                       else
                       {
                           string ErrorMessage = ResourceCache.Localize("CannotassignRoomStudentdoesnotpayHostelFees");
                           return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                       }
                   }
                   else
                   {
                       string ErrorMessage = ResourceCache.Localize("Alreadyhostelroomallotedtothisstudent");
                       return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                   }
               }
               else
               {
                   string ErrorMessage = ResourceCache.Localize("InvalidStudentID");
                   return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
               }
           }
           catch (Exception e)
           {
               return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
           }
       }
       public JsonResult HostelRoomAllotmenttoStudent(VM_AllotHostelRooms cs)
       {
           try
           {
               if (ModelState.IsValid)
               {
                   RoomAllotMentServices Room_allotment = new RoomAllotMentServices();
                   bool result = Room_allotment.Addhostelroomtostudent(cs.UserId, cs.BlockName, cs.RoomNumber);
                   if (result == true)
                   {
                       string Message = ResourceCache.Localize("Successfullyhostelroomallotedtostudent");
                       useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Successfullyhostelroomallotedtostudent"));
                       return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                   }
                   else
                   {
                       string Error = ResourceCache.Localize("Alreadyhostelroomallotedtothisstudent");
                       return Json(new { ErrorMessage = Error }, JsonRequestBehavior.AllowGet);
                   }
               }
               else
               {
                   string totalError = "";
                   string[] t = new string[ModelState.Values.Count];
                   int i = 0;
                   foreach (var obj in ModelState.Values)
                   {
                       foreach (var error in obj.Errors)
                       {
                           if (!string.IsNullOrEmpty(error.ErrorMessage))
                           {
                               totalError = totalError + error.ErrorMessage + Environment.NewLine;
                               t[i] = error.ErrorMessage;
                               i++;
                           }
                       }
                   }
                   return Json(new { Success = 0, ex = t });
               }
           }
           catch (Exception e)
           {
               return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
           }
       }
       public JsonResult GetBlock(string hosteltype)
       {
           try
           {
               HostelBlockServices hostel_block = new HostelBlockServices();
               var list = hostel_block.BlockList(hosteltype);
               return Json(list, JsonRequestBehavior.AllowGet);
           }
           catch (Exception e)
           {
               return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
           }
       }
       public JsonResult GetStudentRoom(int BlockName)
       {
           try
           {
               HostelRoomServices Hostel_room = new HostelRoomServices();
               var list = Hostel_room.GetStudenthostelRooms(BlockName);
               return Json(list, JsonRequestBehavior.AllowGet);
           }
           catch (Exception e)
           {
               return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
           }
       }
       //JQgridTable for Hostel
       public static IList<HostalList> Hostellist = new List<HostalList>();
       public JsonResult StudentRoomAllotmentList(string sidx, string sord, int page, int rows, int? List_Roomnumber)
       {
           RoomAllotMentServices Room_Allotment = new RoomAllotMentServices();
           int pageIndex = Convert.ToInt32(page) - 1;
           int pageSize = rows;
           Hostellist = Room_Allotment.GetHostelStudentlist();
           if (List_Roomnumber.HasValue)
           {
               Hostellist = Room_Allotment.GetHostelroomstudentlist(Convert.ToInt16(List_Roomnumber));
           }
           int totalRecords = Hostellist.Count();
           var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
           if (sord != null)
           {
               if (sord.ToUpper() == "DESC")
               {
                   Hostellist = Hostellist.OrderByDescending(s => s.RoomNumber).ToList();
                   Hostellist = Hostellist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
               }
               else
               {
                   Hostellist = Hostellist.OrderBy(s => s.RoomNumber).ToList();
                   Hostellist = Hostellist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
               }
           }
           var jsonData = new
           {
               total = totalPages,
               page,
               records = totalRecords,
               rows = Hostellist
           };
           return Json(jsonData, JsonRequestBehavior.AllowGet);
       }
       public ActionResult StudentRoomAllotment_ExportToExcel()
       {
           try
           {
               var list = Hostellist.Select(o => new { StudentId = o.StudentId, StudentName = o.StudentName, AcademicYear = o.AcademicYear, Class = o.Class, Section = o.Section, BlockName = o.BlockName, RoomNumber = o.RoomNumber, Address = o.Address,EmergencyContactPerson = o.EmergencyContactPerson, EmergencyContactPersonNo = o.EmergencyContactPersonNo }).ToList();
               string heading = ResourceCache.Localize("HostelStudentList_filename");
               GenerateExcel.ExportExcel(list, heading);
               return View("HostalStudent");
           }
           catch (Exception ex)
           {
               ViewBag.error = ex.Message;
               return View("Error");
           }
       }
       public ActionResult StudentRoomAllotment_ExportToPdf()
       {
           try
           {
               string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
               string heading = ResourceCache.Localize("HostelStudentList");
               GeneratePDF.ExportPDF_Landscape(Hostellist, new string[] { "StudentId", "StudentName", "AcademicYear", "Class", "Section", "BlockName", "RoomNumber", "Address", "EmergencyContactPerson", "EmergencyContactPersonNo" }, xfilePath, heading);
               return File(xfilePath, "application/pdf", "HostalStudantList.pdf");
           }
           catch (Exception ex)
           {
               ViewBag.error = ex.Message;
               return View("Error");
           }
       }
       public JsonResult EditStudentRoomAllotment(int RoomAllotmentId)
       {
           try
           {
               RoomAllotMentServices Room_allotment = new RoomAllotMentServices();
               StudentServices Stu = new StudentServices();
               var RoomDetails = Room_allotment.GetStudentRoomDetails(RoomAllotmentId);
               if (RoomDetails != null)
               {
                   var StudentData = Stu.GetHostelStudentDetails(Convert.ToInt16(RoomDetails.StudentRegisterId));
                   return Json(new { RoomDetails = RoomDetails, StudentData = StudentData }, JsonRequestBehavior.AllowGet);
                   
               }
               else
               {
                   string Errormessage = ResourceCache.Localize("Studentdetailsnotfound");
                   return Json(new { Errormessage = Errormessage, JsonRequestBehavior.AllowGet });
               }
           }
           catch (Exception e)
           {
               return Json(new { Errormessage = e.Message.ToString(), JsonRequestBehavior.AllowGet });
           }
       }
       public JsonResult UpdateHostelRoomAllotmenttoStudent(VM_AllotHostelRooms cs)
       {
           try
           {
               if (ModelState.IsValid)
               {
                   RoomAllotMentServices Room_allotment = new RoomAllotMentServices();
                   bool result = Room_allotment.Updatehostelroomtostudent(cs.UserId, cs.BlockName, cs.RoomNumber, cs.RoomAllotmentId);
                   if (result == true)
                   {
                       string Message =ResourceCache.Localize("Successfullyhostelroomupdatedtostudent");
                       useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Successfullyhostelroomupdatedtostudent"));
                       return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                   }
                   else
                   {
                       string Error = ResourceCache.Localize("Choosecorrectroom");
                       return Json(new { ErrorMessage = Error }, JsonRequestBehavior.AllowGet);
                   }
               }
               else
               {
                   string totalError = "";
                   string[] t = new string[ModelState.Values.Count];
                   int i = 0;
                   foreach (var obj in ModelState.Values)
                   {
                       foreach (var error in obj.Errors)
                       {
                           if (!string.IsNullOrEmpty(error.ErrorMessage))
                           {
                               totalError = totalError + error.ErrorMessage + Environment.NewLine;
                               t[i] = error.ErrorMessage;
                               i++;
                           }
                       }
                   }
                   return Json(new { Success = 0, ex = t });
               }
           }
           catch (Exception e)
           {
               return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
           }
       }
       public ActionResult StaffRoomAllotment()
       {
           HostelRoomServices Hostel_room = new HostelRoomServices();
           ViewBag.RoomNumber = Hostel_room.GetStaffhostelRoomsList();
           return View();
       }
       public JsonResult CheckStaff(string RegId)
       {
           try
           {
               TechEmployeeServices T_Employee = new TechEmployeeServices();
               OtherEmployeeServices O_Employee = new OtherEmployeeServices();
               var Techemployee = T_Employee.GethostelEmployee(RegId);
               var otheremployee = O_Employee.GethostelEmployee(RegId);
               if (Techemployee != null)
               {
                   bool checktechemployee = T_Employee.Checkalreadyhostelroomtoemployee(RegId);
                   if (checktechemployee == true)
                   {
                       string ErrorMessage = ResourceCache.Localize("Hostelroomalreadyallotedtothisstaff");
                       return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                   }
                   else
                   {
                       bool checkotheremployee = O_Employee.Checkalreadyhostelroomtoemployee(RegId);
                       return Json(new { Techemployee = Techemployee, }, JsonRequestBehavior.AllowGet);
                   }
               }
               else if (otheremployee != null)
               {
                   bool checkotheremployee = O_Employee.Checkalreadyhostelroomtoemployee(RegId);
                   if (checkotheremployee == true)
                   {
                       string ErrorMessage = ResourceCache.Localize("Hostelroomalreadyallotedtothisstaff");
                       return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                   }
                   else
                   {
                       return Json(new { otheremployee = otheremployee, }, JsonRequestBehavior.AllowGet);
                   }
                   
               }
               else
               {
                       string ErrorMessage = ResourceCache.Localize("Entercorrectemployeeid");
                       return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
               }
           }
           catch (Exception e)
           {
               return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
           }
       }
       public JsonResult GetStaffRoom(int BlockName)
       {
           try
           {
               HostelRoomServices Hostel_room = new HostelRoomServices();
               var list = Hostel_room.GetStaffhostelRooms(BlockName);
               return Json(list, JsonRequestBehavior.AllowGet);
           }
           catch (Exception e)
           {
               return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
           }
       }
       //JQgridTable for Hostel
       public static IList<HostalList> Hostellist1 = new List<HostalList>();
       public JsonResult StaffRoomAllotmentList(string sidx, string sord, int page, int rows, int? List_Roomnumber)
       {
           RoomAllotMentServices Room_Allotment = new RoomAllotMentServices();
           int pageIndex = Convert.ToInt32(page) - 1;
           int pageSize = rows;
           Hostellist = Room_Allotment.GetHostelStafflist();
           if (List_Roomnumber.HasValue)
           {
               Hostellist = Room_Allotment.GetHostelRoomStafflist(Convert.ToInt16(List_Roomnumber));
           }
           int totalRecords = Hostellist.Count();
           var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
           if (sord != null)
           {
               if (sord.ToUpper() == "DESC")
               {
                   Hostellist = Hostellist.OrderByDescending(s => s.RoomNumber).ToList();
                   Hostellist = Hostellist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
               }
               else
               {
                   Hostellist = Hostellist.OrderBy(s => s.RoomNumber).ToList();
                   Hostellist = Hostellist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
               }
           }
           var jsonData = new
           {
               total = totalPages,
               page,
               records = totalRecords,
               rows = Hostellist
           };
           return Json(jsonData, JsonRequestBehavior.AllowGet);
       }
       public ActionResult StaffRoomAllotment_ExportToExcel()
       {
           try
           {
               var list = Hostellist.Select(o => new { EmployeeId = o.EmployeeId, EmployeeName = o.EmployeeName, EmployeeType = o.EmployeeType, EmployeeDesignation = o.EmployeeDesignation, BlockName = o.BlockName, RoomNumber = o.RoomNumber, Address = o.Address, EmployeeContact = o.EmployeeContact, EmergencyContactPerson = o.EmergencyContactPerson, EmergencyContactPersonNo = o.EmergencyContactPersonNo }).ToList();
               string heading = ResourceCache.Localize("HostelStaffList");
               GenerateExcel.ExportExcel(list, heading);
               return View("HostalStaff");
           }
           catch (Exception ex)
           {
               ViewBag.error = ex.Message;
               return View("Error");
           }
       }
       public ActionResult StaffRoomAllotment_ExportToPdf()
       {
           try
           {
               string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
               string heading = ResourceCache.Localize("HostelStaffList");
               GeneratePDF.ExportPDF_Portrait(Hostellist, new string[] { "EmployeeId", "EmployeeName", "EmployeeType", "EmployeeDesignation", "BlockName", "RoomNumber", "Address", "EmployeeContact", "EmergencyContactPerson", "EmergencyContactPersonNo" }, xfilePath, heading);
               return File(xfilePath, "application/pdf", "HostalStaffList.pdf");
           }
           catch (Exception ex)
           {
               ViewBag.error = ex.Message;
               return View("Error");
           }
       }
       public JsonResult HostelRoomAllotmenttoStaff(VM_AllotHostelRooms cs)
       {
           try
           {
               if (ModelState.IsValid)
               {
                   RoomAllotMentServices Room_allotment = new RoomAllotMentServices();
                   string Msg = "Ok";
                   if (cs.Etype == "Tech")
                   {
                       bool result = Room_allotment.AddhostelroomtoTechStaff(cs.UserId, cs.BlockName, cs.RoomNumber);
                       if (result == true)
                       {
                           string Message = ResourceCache.Localize("Successfullyupdatedhostelroomallotedtostaff");
                           //useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Successfullyupdatedhostelroomallotedtostaff"));
                           return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                       }
                       else
                       {
                           string Error = ResourceCache.Localize("Alreadyhostelroomallotedtothisstaff");
                           return Json(new { ErrorMessage = Error }, JsonRequestBehavior.AllowGet);
                       }
                       //return Json(new { Message = Msg }, JsonRequestBehavior.AllowGet);
                   }
                   else if (cs.Etype == "Other")
                   {
                       bool result = Room_allotment.AddhostelroomtoOtherStaff(cs.UserId, cs.BlockName, cs.RoomNumber);
                       if (result == true)
                       {
                           string Message = ResourceCache.Localize("Successfullyupdatedhostelroomallotedtostaff");
                           useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Successfullyupdatedhostelroomallotedtostaff"));
                           return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                       }
                       else
                       {
                           string Error = ResourceCache.Localize("Alreadyhostelroomallotedtothisstaff");
                           return Json(new { ErrorMessage = Error }, JsonRequestBehavior.AllowGet);
                       }
                      // return Json(new { Message = Msg }, JsonRequestBehavior.AllowGet);
                   }
                   else
                   {
                       return Json(new { Message = Msg }, JsonRequestBehavior.AllowGet);
                   }
               }
               else
               {
                   string totalError = "";
                   string[] t = new string[ModelState.Values.Count];
                   int i = 0;
                   foreach (var obj in ModelState.Values)
                   {
                       foreach (var error in obj.Errors)
                       {
                           if (!string.IsNullOrEmpty(error.ErrorMessage))
                           {
                               totalError = totalError + error.ErrorMessage + Environment.NewLine;
                               t[i] = error.ErrorMessage;
                               i++;
                           }
                       }
                   }
                   return Json(new { Success = 0, ex = t });
               }
           }
           catch (Exception e)
           {
               return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
           }
       }
       public JsonResult EditStaffRoomAllotment(string RoomAllotmentId)
       {
           try
           {
               RoomAllotMentServices Room_allotment = new RoomAllotMentServices();
               TechEmployeeServices Tech_Emp = new TechEmployeeServices();
               OtherEmployeeServices Other_Emp = new OtherEmployeeServices();
               string[] c_id = RoomAllotmentId.Split('/');
               string Message = "ok";
               if (c_id[1] == "Tech")
               {
                   var RoomDetails = Room_allotment.GetStudentRoomDetails(Convert.ToInt16(c_id[0]));
                   if (RoomDetails != null)
                   {
                       var EmployeeData = Tech_Emp.GethostelEditEmployee(Convert.ToInt16(RoomDetails.EmployeeregisterId));
                       return Json(new { RoomDetails = RoomDetails, EmployeeData = EmployeeData, JsonRequestBehavior.AllowGet });
                   }
                   return Json(new { Message = Message, JsonRequestBehavior.AllowGet });
               }
               else if (c_id[1] == "Other")
               {
                   var RoomDetails = Room_allotment.GetStudentRoomDetails(Convert.ToInt16(c_id[0]));
                   if (RoomDetails != null)
                   {
                       var EmployeeData = Other_Emp.GethostelEditEmployee(Convert.ToInt16(RoomDetails.OtherEmpoyeeRegisterId));
                       return Json(new { RoomDetails = RoomDetails, EmployeeData = EmployeeData, JsonRequestBehavior.AllowGet });
                   }
                   return Json(new { Message = Message, JsonRequestBehavior.AllowGet });
               }
               else
               {
                   string Errormessage = ResourceCache.Localize("staffdetailsnotfound");
                   return Json(new { Errormessage = Errormessage, JsonRequestBehavior.AllowGet });
               }
           }
           catch (Exception e)
           {
               return Json(new { Errormessage = e.Message.ToString(), JsonRequestBehavior.AllowGet });
           }
       }
       public JsonResult UpdateHostelRoomAllotmenttoStaff(VM_AllotHostelRooms cs)
       {
           try
           {
               if (ModelState.IsValid)
               {
                   RoomAllotMentServices Room_allotment = new RoomAllotMentServices();
                   string Msg = "Ok";
                   if (cs.Etype == "Tech")
                   {
                       bool result = Room_allotment.UpdatehostelroomtoTechStaff(cs.UserId, cs.BlockName, cs.RoomNumber, cs.RoomAllotmentId);
                       if (result == true)
                       {
                           string Message =ResourceCache.Localize("UpdatedSuccessfully");
                           useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdatedSuccessfully"));
                           return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                       }
                       else
                       {
                           string Error =ResourceCache.Localize("Alreadyhostelroomallotedtothisstaff");
                           return Json(new { ErrorMessage = Error }, JsonRequestBehavior.AllowGet);
                       }
                       return Json(new { Message = Msg }, JsonRequestBehavior.AllowGet);
                   }
                   else if (cs.Etype == "Other")
                   {
                       bool result = Room_allotment.UpdatehostelroomtoOtherStaff(cs.UserId, cs.BlockName, cs.RoomNumber, cs.RoomAllotmentId);
                       if (result == true)
                       {
                           string Message = ResourceCache.Localize("UpdatedSuccessfully");
                           useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdatedSuccessfully"));
                           return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                       }
                       else
                       {
                           string Error = ResourceCache.Localize("Alreadyhostelroomallotedtothisstaff");
                           return Json(new { ErrorMessage = Error }, JsonRequestBehavior.AllowGet);
                       }
                       return Json(new { Message = Msg }, JsonRequestBehavior.AllowGet);
                   }
                   else
                   {
                       return Json(new { Message = Msg }, JsonRequestBehavior.AllowGet);
                   }
               }
               else
               {
                   string totalError = "";
                   string[] t = new string[ModelState.Values.Count];
                   int i = 0;
                   foreach (var obj in ModelState.Values)
                   {
                       foreach (var error in obj.Errors)
                       {
                           if (!string.IsNullOrEmpty(error.ErrorMessage))
                           {
                               totalError = totalError + error.ErrorMessage + Environment.NewLine;
                               t[i] = error.ErrorMessage;
                               i++;
                           }
                       }
                   }
                   return Json(new { Success = 0, ex = t });
               }
           }
           catch (Exception e)
           {
               return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
           }
       }
                   
               
         
      
    }
}