﻿
namespace eAcademy.Areas.HostelManagement.Models
{
    public class M_RoomAllot
    {
        public int RoomAllotmentId { get; set; }
        public int? StudentRegisterId { get; set; }
        public int? EmployeeregisterId { get; set; }
        public int? OtherEmpoyeeRegisterId{ get; set; }
        public int? BlockId { get; set; }
        public int? RoomId { get; set; }
        
    }
}