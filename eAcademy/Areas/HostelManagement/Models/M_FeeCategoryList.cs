﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.HostelManagement.Models
{
    public class M_FeeCategoryList
    {
        public string CategoryName { get; set; }
        public int? FeeCategoryId { get; set; }
        public string SubCategoryName { get; set; }
        public int? SubcategoryId { get; set; }
        public decimal? Total { get; set; }
        public string Feetype { get; set; }
    }
}