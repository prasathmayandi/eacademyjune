﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using eAcademy.HelperClass;
namespace eAcademy.Areas.HostelManagement.Models
{
    public class SaveFeeCollection1
    {

         //[Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
         //              ErrorMessageResourceName = "plsenterfeesamount")]
        public string FeeCollectiondate { get; set; }
         

        public Decimal FeeAmount { get; set; }

        public Decimal AmountCollected { get; set; }
        public Decimal? Discount { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                       ErrorMessageResourceName = "Please_Select_Payment_Mode")]
        public string PaymentMode { get; set; }

         //[Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
         //              ErrorMessageResourceName = "EnterBankName")]
        public string BankName { get; set; }


        //[Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
        //               ErrorMessageResourceName = "EnterChequeNumber")]
        public string ChequeNumber { get; set; }
        //[Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
        //               ErrorMessageResourceName = "EnterDDNumber")]
        public string DDNumber { get; set; }
        public string Remark { get; set; }
        public int[] FeeCategoryId { get; set; }
        
        public string txt_Academicyear { get; set; }
        
        public string txt_Class { get; set; }
        
        public string txt_Section { get; set; }
        //[Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
        //               ErrorMessageResourceName = "PleaseSelectStudent")]
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string StudentRollNumber { get; set; }
        public int ReceiptNo { get; set; }
        public string FeeCategory { get; set; }
        public Decimal? Amount { get; set; }
        public Decimal? ServiceTax { get; set; }
        public Decimal? Total { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                       ErrorMessageResourceName = "SelectAcademicYear")]
        public int AcademicYearId { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                      ErrorMessageResourceName = "PleaseSelectClass")]
        public int ClassId { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                      ErrorMessageResourceName = "PleaseSelectSection")]
        public int SectionId { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                      ErrorMessageResourceName = "Please_Enter_Register_number")]
        public string StudentRegId { get; set; }
     }
}