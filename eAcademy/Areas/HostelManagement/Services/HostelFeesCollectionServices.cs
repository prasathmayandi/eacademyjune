﻿using eAcademy.Areas.HostelManagement.Models;
using eAcademy.Areas.HostelManagement.ViewModel;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.HostelManagement.Services
{
 public class HostelFeesCollectionServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
     
     public int feesid()
        {
            var result = (from a in db.HostelFeesCollections select a).Count();
            return result;
        }
     public List<HostelFeesList>studentfeesdetails(int acid)
     {
         var ans = (from a in db.HostelFeesCategories
                    where a.AcademicYear == acid
                    select new HostelFeesList { 
                        categoryid = a.Id,
                        categoryname = a.CategoryName,
                        Amount = a.FeesAmount.Value,
                        tax = a.Tax.Value, 
                        total = a.FeesAmount.Value + a.Tax.Value
                    }).ToList();
       return ans;
     }
     public HostelFeesCollection Exist_student(int std_id, int acid)
     {
         var existregid = (from a in db.HostelFeesCollections where std_id == a.StudentRegisterId && acid == a.AcademicYearId select a).FirstOrDefault();
         return existregid;
     }   
     
     public string savefeescollection(int acid, int std_id, decimal feeAmount, decimal amountCollected, string paymentMode, string bankName, string chequeNumber, string DDNumber, string remark,decimal discount,int AdminId,int sectionid,int classid)
     {
         var date = DateTimeByZone.getCurrentDateTime();
         HostelFeesCollection hostelfeescollection = new HostelFeesCollection()
                                {
                                    AcademicYearId = acid,
                                    CollectionDate = date,
                                    StudentRegisterId = std_id,
                                    FeeAmount = feeAmount,
                                    AmountCollected = amountCollected,
                                    PaymentMode = paymentMode,
                                    BankName = bankName,
                                    ChequeNumber = chequeNumber,
                                    DDNumber = DDNumber,
                                    Remark = remark,
                                    Discount = discount,
                                    SectionId = Convert.ToInt16(sectionid),
                                    ClassId = Convert.ToInt16(classid),
                                    CreatedBy = AdminId,
                                    CreatedDate = date,
                                    Status = "Active"
                                };
         db.HostelFeesCollections.Add(hostelfeescollection);
         db.SaveChanges();
         var feescolltionid=(from a in db.HostelFeesCollections select a.FeeCollectionId).Max();
         string fees_collectionid = Convert.ToString(feescolltionid);
         return fees_collectionid;
         
        }
     public SaveFeeCollection1 GetFeePaidStudents(int id)
     {
         var ans = (from a in db.HostelFeesCollections
                    from b in db.AcademicYears
                    from c in db.Classes
                    from d in db.Sections
                    from e in db.Students
                    from f in db.AssignClassToStudents
                    from g in db.StudentRollNumbers
                    where a.FeeCollectionId == id && e.StudentRegisterId == a.StudentRegisterId && f.StudentRegisterId == e.StudentRegisterId && f.Status == true && g.StudentRegisterId == e.StudentRegisterId && g.Status == true &&
                   b.AcademicYearId == a.AcademicYearId && c.ClassId == a.ClassId && d.SectionId == a.SectionId
                    select new SaveFeeCollection1
                    {
                        txt_Academicyear = b.AcademicYear1,
                        txt_Class = c.ClassType,
                        txt_Section = d.SectionName,
                        StudentName = e.FirstName + " " + e.LastName,
                        StudentId = e.StudentId,
                        StudentRollNumber = g.RollNumber,
                        ReceiptNo = a.FeeCollectionId
                    }).Distinct().FirstOrDefault();
         return ans;
     }
     public HostelFeesCollection GetCollectionDetails(int collectid)
     {
         var ans = db.HostelFeesCollections.Where(q => q.FeeCollectionId == collectid).FirstOrDefault();
         return ans;
     }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}