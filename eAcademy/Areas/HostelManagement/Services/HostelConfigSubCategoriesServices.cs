﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.HostelManagement.Services
{
    public class HostelConfigSubCategoriesServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public string GetSubCategory(int FeeCategoryId,int SubCategoryId)
        {
            var list = (from a in dc.HostelConfigCategories
                        from b in dc.HostelConfigSubCategories
                        where b.CategoryId == FeeCategoryId && b.SubCategoryId == SubCategoryId && a.Status == true && b.Status == true
                        select b).FirstOrDefault();
            if(list!=null)
            {
                return list.SubCategoryName;
            }
            else
            {
                return "";
            }
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}