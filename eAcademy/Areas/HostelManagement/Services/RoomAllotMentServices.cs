﻿using eAcademy.Areas.HostelManagement.Models;
using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.HostelManagement.Services
{
    public class RoomAllotMentServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        HostelRoomServices Hostel_room = new HostelRoomServices();
        public List<HostalList> GetHostelStudentlist()
        {
            var ans = (from a in dc.RoomAllotments
                       from b in dc.Students
                       from c in dc.AssignClassToStudents
                      // from d in dc.StudentRollNumbers
                       from e in dc.AcademicYears
                       from f in dc.Classes
                       from g in dc.Sections
                       from h in dc.HostelBlocks
                       from i in dc.HostelRooms
                       where c.StudentRegisterId == a.StudentRegisterId && c.Status == true && b.StudentRegisterId == c.StudentRegisterId
                           && e.AcademicYearId == c.AcademicYearId && f.ClassId == c.ClassId && g.SectionId == c.SectionId && h.BlockId == a.BlockId && i.RoomId == a.RoomId && a.status == true
                       select new HostalList
                       {
                           RoomAllotmentId=a.RoomAllotmentId,
                           StudentId = b.StudentId,
                           //RollNumber = d.RollNumber,
                           StudentName = b.FirstName + " " + b.LastName,
                           AcademicYear = e.AcademicYear1,
                           Class = f.ClassType,
                           Section = g.SectionName,
                           Address = b.LocalAddress1 + " " + b.LocalAddress2 + "," + b.LocalCity + "," + b.LocalState,
                           EmergencyContactPerson = b.EmergencyContactPerson,
                           EmergencyContactPersonNo = b.EmergencyContactNumber,
                           StudentContact = b.Contact,
                           BlockName = h.BlockName,
                           RoomNumber = i.RoomName
                       }).Distinct().ToList();
            return ans;
        }
        public List<HostalList> GetHostelStafflist()
        {
            var ans1 = (((from a in dc.TechEmployees
                        from b in dc.EmployeeTypes
                        from c in dc.EmployeeDesignations
                        from d in dc.RoomAllotments
                        from e in dc.HostelBlocks
                        from f in dc.HostelRooms
                        where a.EmployeeRegisterId == d.EmployeeregisterId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId && e.BlockId == d.BlockId && f.RoomId == d.RoomId && d.status == true
                        select new HostalList
                        {
                            RoomAllotmentId = d.RoomAllotmentId,
                            EmployeeName = a.EmployeeName + " " + a.LastName,
                            EmployeeType = b.EmployeeTypes,
                            EmployeeDesignation = c.Designation,
                            Address = a.AddressLine1 + " " + a.AddressLine2 + "," + a.City + "," + a.State,
                            EmergencyContactPerson = a.EmergencyContactPerson,
                            EmergencyContactPersonNo = a.EmergencyContactNumber,
                            EmployeeContact = a.Mobile,
                            BlockName = e.BlockName,
                            RoomNumber = f.RoomName,
                            EmployeeId = a.EmployeeId
                        }).ToList().Select(q => new HostalList
                        {
                            AllomentIdEtype = Convert.ToString(q.RoomAllotmentId) + "/" + "Tech",
                            RoomAllotmentId = q.RoomAllotmentId,
                            EmployeeName = q.EmployeeName,
                            EmployeeType = q.EmployeeType,
                            EmployeeDesignation = q.EmployeeDesignation,
                            Address = q.Address,
                            EmergencyContactPerson = q.EmergencyContactPerson,
                            EmergencyContactPersonNo = q.EmergencyContactPersonNo,
                            EmployeeContact = q.EmployeeContact,
                            BlockName = q.BlockName,
                            RoomNumber = q.RoomNumber,
                            EmployeeId = q.EmployeeId
                           
                        }))
                        .Union((
                       from a in dc.OtherEmployees
                       from b in dc.EmployeeTypes
                       from c in dc.EmployeeDesignations
                       from d in dc.RoomAllotments
                       from e in dc.HostelBlocks
                       from f in dc.HostelRooms
                       where a.EmployeeRegisterId == d.OtherEmpoyeeRegisterId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId && e.BlockId == d.BlockId && f.RoomId == d.RoomId && d.status == true
                       select new HostalList
                       {
                           RoomAllotmentId = d.RoomAllotmentId,
                           EmployeeName = a.EmployeeName + " " + a.Lastname,
                           EmployeeType = b.EmployeeTypes,
                           EmployeeDesignation = c.Designation,
                           Address = a.Address + " " + a.Address2 + "," + a.City + "," + a.State,
                           EmergencyContactPerson = a.EmergencyContactPerson,
                           EmergencyContactPersonNo = a.EmergencyContactNumber,
                           EmployeeContact = a.Contact,
                           BlockName = e.BlockName,
                           RoomNumber = f.RoomName,
                           EmployeeId = a.EmployeeId
                         
                       }).ToList().Select(q => new HostalList
                       {
                           RoomAllotmentId = q.RoomAllotmentId,
                           EmployeeName = q.EmployeeName,
                           EmployeeType = q.EmployeeType,
                           EmployeeDesignation = q.EmployeeDesignation,
                           Address = q.Address,
                           EmergencyContactPerson = q.EmergencyContactPerson,
                           EmergencyContactPersonNo = q.EmergencyContactPersonNo,
                           EmployeeContact = q.EmployeeContact,
                           BlockName = q.BlockName,
                           RoomNumber = q.RoomNumber,
                           EmployeeId = q.EmployeeId,
                           AllomentIdEtype = Convert.ToString(q.RoomAllotmentId) + "/" + "Other"
                       }))).OrderBy(q => q.RoomNumber).Distinct().ToList();
            return ans1;
        }
        public List<HostalList> GetHostelRoomStafflist(int roomid)
        {
            var ans1 = (((from a in dc.TechEmployees
                        from b in dc.EmployeeTypes
                        from c in dc.EmployeeDesignations
                        from d in dc.RoomAllotments
                        from e in dc.HostelBlocks
                        from f in dc.HostelRooms
                        where a.EmployeeRegisterId == d.EmployeeregisterId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId && e.BlockId == d.BlockId && f.RoomId == d.RoomId && d.status == true && f.RoomId == roomid
                        select new HostalList
                        {
                            RoomAllotmentId = d.RoomAllotmentId,
                            EmployeeName = a.EmployeeName + " " + a.LastName,
                            EmployeeType = b.EmployeeTypes,
                            EmployeeDesignation = c.Designation,
                            Address = a.AddressLine1 + " " + a.AddressLine2 + "," + a.City + "," + a.State,
                            EmergencyContactPerson = a.EmergencyContactPerson,
                            EmergencyContactPersonNo = a.EmergencyContactNumber,
                            EmployeeContact = a.Mobile,
                            BlockName = e.BlockName,
                            RoomNumber = f.RoomName,
                            EmployeeId = a.EmployeeId
                           
                        }).ToList().Select(q => new HostalList
                        {
                            RoomAllotmentId = q.RoomAllotmentId,
                            EmployeeName = q.EmployeeName,
                            EmployeeType = q.EmployeeType,
                            EmployeeDesignation = q.EmployeeDesignation,
                            Address = q.Address,
                            EmergencyContactPerson = q.EmergencyContactPerson,
                            EmergencyContactPersonNo = q.EmergencyContactPersonNo,
                            EmployeeContact = q.EmployeeContact,
                            BlockName = q.BlockName,
                            RoomNumber = q.RoomNumber,
                            EmployeeId = q.EmployeeId,
                            AllomentIdEtype = Convert.ToString(q.RoomAllotmentId) + "/" + "Tech"
                        })).Union((
                       from a in dc.OtherEmployees
                       from b in dc.EmployeeTypes
                       from c in dc.EmployeeDesignations
                       from d in dc.RoomAllotments
                       from e in dc.HostelBlocks
                       from f in dc.HostelRooms
                       where a.EmployeeRegisterId == d.OtherEmpoyeeRegisterId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId && e.BlockId == d.BlockId && f.RoomId == d.RoomId && d.status == true && f.RoomId == roomid
                       select new HostalList
                       {
                           RoomAllotmentId = d.RoomAllotmentId,
                           EmployeeName = a.EmployeeName + " " + a.Lastname,
                           EmployeeType = b.EmployeeTypes,
                           EmployeeDesignation = c.Designation,
                           Address = a.Address + " " + a.Address2 + "," + a.City + "," + a.State,
                           EmergencyContactPerson = a.EmergencyContactPerson,
                           EmergencyContactPersonNo = a.EmergencyContactNumber,
                           EmployeeContact = a.Contact,
                           BlockName = e.BlockName,
                           RoomNumber = f.RoomName,
                           EmployeeId = a.EmployeeId,
                          
                       }).ToList().Select(q => new HostalList
                       {
                           RoomAllotmentId = q.RoomAllotmentId,
                           EmployeeName = q.EmployeeName,
                           EmployeeType = q.EmployeeType,
                           EmployeeDesignation = q.EmployeeDesignation,
                           Address = q.Address,
                           EmergencyContactPerson = q.EmergencyContactPerson,
                           EmergencyContactPersonNo = q.EmergencyContactPersonNo,
                           EmployeeContact = q.EmployeeContact,
                           BlockName = q.BlockName,
                           RoomNumber = q.RoomNumber,
                           EmployeeId = q.EmployeeId,
                           AllomentIdEtype = Convert.ToString(q.RoomAllotmentId) + "/" + "Other"
                       }))).OrderBy(q => q.RoomNumber).Distinct().ToList();
            return ans1;
        }
        public List<HostalList> GetHostelroomstudentlist(int roomid)
        {
            var ans = (from a in dc.RoomAllotments
                       from b in dc.Students
                       from c in dc.AssignClassToStudents
                       //from d in dc.StudentRollNumbers
                       from e in dc.AcademicYears
                       from f in dc.Classes
                       from g in dc.Sections
                       from h in dc.HostelBlocks
                       from i in dc.HostelRooms
                       where c.StudentRegisterId == a.StudentRegisterId  && b.StudentRegisterId == c.StudentRegisterId 
                           && e.AcademicYearId == c.AcademicYearId && f.ClassId == c.ClassId && g.SectionId == c.SectionId && h.BlockId == a.BlockId && i.RoomId == a.RoomId && a.RoomId == roomid && i.RoomType == "Student"
                       select new HostalList
                       {
                           RoomAllotmentId = a.RoomAllotmentId,
                           StudentId = b.StudentId,
                           //RollNumber = d.RollNumber,
                           StudentName = b.FirstName + " " + b.LastName,
                           AcademicYear = e.AcademicYear1,
                           Class = f.ClassType,
                           Section = g.SectionName,
                           Address = b.LocalAddress1 + " " + b.LocalAddress2 + "," + b.LocalCity + "," + b.LocalState,
                           EmergencyContactPerson = b.EmergencyContactPerson,
                           EmergencyContactPersonNo = b.EmergencyContactNumber,
                           StudentContact = b.Contact,
                           BlockName = h.BlockName,
                           RoomNumber = i.RoomName
                       }).Distinct().ToList();
            return ans;
        }
        public bool Addhostelroomtostudent (int studentregid,int blockid,int roomid)
        {
           
            var get = dc.RoomAllotments.Where(q => q.StudentRegisterId == studentregid).FirstOrDefault();
            if (get == null)
            {
                RoomAllotment room = new RoomAllotment()
                {
                    StudentRegisterId = studentregid,
                    BlockId = blockid,
                    RoomId = roomid,
                    status = true
                };
                dc.RoomAllotments.Add(room);
                dc.SaveChanges();
                Hostel_room.ReduceRoomAvailablity(blockid, roomid);
               
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool AddhostelroomtoTechStaff(int EmpregId, int blockid, int roomid)
        {
            var get = dc.RoomAllotments.Where(q => q.EmployeeregisterId == EmpregId).FirstOrDefault();
            if (get == null)
            {
                RoomAllotment room = new RoomAllotment()
                {
                    EmployeeregisterId = EmpregId,
                    BlockId = blockid,
                    RoomId = roomid,
                    status = true
                };
                dc.RoomAllotments.Add(room);
                dc.SaveChanges();
                Hostel_room.ReduceRoomAvailablity(blockid, roomid);
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdatehostelroomtoTechStaff(int EmpregId, int blockid, int roomid,int RoomAllotmentId)
        {
            var get = dc.RoomAllotments.Where(q =>q.RoomAllotmentId == RoomAllotmentId && q.EmployeeregisterId == EmpregId).FirstOrDefault();
            if (get != null)
            {
                int existblockid = Convert.ToInt16(get.BlockId);
                int Existroomid = Convert.ToInt16(get.RoomId);
                if (existblockid != blockid)
                {
                    Hostel_room.increaseRoomAvailablity(existblockid, Existroomid);
                }
                get.BlockId = blockid;
                get.RoomId = roomid;
                dc.SaveChanges();
                if (existblockid != blockid)
                {
                    Hostel_room.ReduceRoomAvailablity(blockid, roomid);
                }
              
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool AddhostelroomtoOtherStaff(int EmpregId, int blockid, int roomid)
        {
            var get = dc.RoomAllotments.Where(q => q.OtherEmpoyeeRegisterId == EmpregId).FirstOrDefault();
            if (get == null)
            {
                RoomAllotment room = new RoomAllotment()
                {
                    OtherEmpoyeeRegisterId = EmpregId,
                    BlockId = blockid,
                    RoomId = roomid,
                    status = true
                };
                dc.RoomAllotments.Add(room);
                dc.SaveChanges();
                Hostel_room.ReduceRoomAvailablity(blockid, roomid);
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdatehostelroomtoOtherStaff(int EmpregId, int blockid, int roomid,int RoomAllotmentId)
        {
            var get = dc.RoomAllotments.Where(q =>q.RoomAllotmentId == RoomAllotmentId && q.OtherEmpoyeeRegisterId == EmpregId).FirstOrDefault();
            if (get != null)
            {
                int existblockid = Convert.ToInt16(get.BlockId);
                int existroomid = Convert.ToInt16(get.RoomId);
                if (existblockid != blockid)
                {
                    Hostel_room.increaseRoomAvailablity(existblockid, existroomid);
                }
                get.BlockId = blockid;
                get.RoomId = roomid;
                dc.SaveChanges();
                if (existblockid != blockid)
                {
                    Hostel_room.ReduceRoomAvailablity(blockid, roomid);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        public M_RoomAllot GetStudentRoomDetails(int roomallotmentid)
        {
            var ans = (from a in dc.RoomAllotments
                       where a.RoomAllotmentId == roomallotmentid && a.status == true
                       select new M_RoomAllot
                       {
                           RoomAllotmentId = a.RoomAllotmentId,
                           StudentRegisterId = a.StudentRegisterId,
                          EmployeeregisterId =a.EmployeeregisterId,
                            OtherEmpoyeeRegisterId=a.OtherEmpoyeeRegisterId,
                           BlockId = a.BlockId,
                           RoomId = a.RoomId,
                       }).FirstOrDefault();
            return ans;
        }
        public bool Updatehostelroomtostudent(int studentregid, int blockid, int roomid,int roomallotmentid)
        {
            var get = dc.RoomAllotments.Where(q => q.RoomAllotmentId == roomallotmentid && q.StudentRegisterId == studentregid).FirstOrDefault();
            if (get != null)
            {
                int existblockid = Convert.ToInt16(get.BlockId);
                int existroomid = Convert.ToInt16(get.RoomId);
                if (existblockid != blockid)
                {
                    Hostel_room.increaseRoomAvailablity(existblockid, existroomid);
                }
                get.BlockId = blockid;
                get.RoomId = roomid;
                dc.SaveChanges();
                if (existblockid != blockid)
                {
                    Hostel_room.ReduceRoomAvailablity(blockid, roomid);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        public RoomAllotment Delroomid(int roomid)
        {
            var result = (from a in dc.RoomAllotments where a.RoomId == roomid select a).FirstOrDefault();
            return result;
            
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}