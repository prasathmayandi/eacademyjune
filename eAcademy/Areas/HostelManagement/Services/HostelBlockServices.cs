﻿using eAcademy.Areas.HostelManagement.ViewModel;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.HostelManagement.Services
{
    public class HostelBlockServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public HostelBlock exist_BlocknameAdd(string name)
        {
            var blockexists = (from a in dc.HostelBlocks where a.BlockName == name select a).FirstOrDefault();
            return blockexists;
        }
        public int addblock(string name,string type,int id)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            HostelBlock hostelblock = new HostelBlock()
            {
                BlockName = name,
                HostelType = type,
                Status = "Active",
                CreatedBy = Convert.ToString(id),
                CreatedDate = date
            };
            dc.HostelBlocks.Add(hostelblock);
            dc.SaveChanges();
            var blockid = (from a in dc.HostelBlocks select a.BlockId).Max();
            return blockid;
        }
        public List<BlockList>Block_List()
        {
            var list = (from a in dc.HostelBlocks
                        select new BlockList { BlockId = a.BlockId, BlockName = a.BlockName, BlockType = a.HostelType, status = a.Status }).ToList();
            return list;
        }
        public List<Exportexcelblock> Block_List1()
        {
            var list = (from a in dc.HostelBlocks
                        select new Exportexcelblock { BlockName = a.BlockName, BlockType = a.HostelType, status = a.Status }).ToList();
            return list;
        }
        public EdithostelDetails Edit_Details(int id)
        {
                           var hosteldetails = (from a in dc.HostelBlocks
                                    where a.BlockId == id
                                    select new EdithostelDetails { BlockId = a.BlockId, blockname = a.BlockName, bnametype = a.HostelType, blockstatus = a.Status }).FirstOrDefault();
            return hosteldetails;
        }
        public HostelBlock select_block(int id)
        {
            var b = (from a in dc.HostelBlocks where a.BlockId == id select a).FirstOrDefault();
            return b;
        }
      public void  updateblock(string name, string type, string status,int  AdminId,int blockid)
      {
          var date = DateTimeByZone.getCurrentDateTime();
          var b = (from a in dc.HostelBlocks where a.BlockId == blockid select a).FirstOrDefault();
          b.BlockName = name;
          b.HostelType = type;
          b.Status = status;
          b.UpdatedBy = Convert.ToString(AdminId);
          b.UpdatedDate = date;
          dc.SaveChanges();
                     
      }
       
        public IEnumerable<Object> BlockList(string hosteltype)
        {
            var ans = (from a in dc.HostelBlocks
                       where a.HostelType == hosteltype && a.Status == "Active"
                       select new
                       {
                           BlockId = a.BlockId,
                           BlockName = a.BlockName
                       }).Distinct().ToList();
            return ans;
        }
        public List<HostelBlock> exist_Blocknameupdate(int BlockId, string name)
        {
            var blockexists = (from a in dc.HostelBlocks where a.BlockId != BlockId && name == a.BlockName select a).ToList();
            return blockexists;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}