﻿using eAcademy.Areas.HostelManagement.Models;
using eAcademy.Areas.HostelManagement.ViewModel;
using eAcademy.Controllers;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.HostelManagement.Services
{
    public class HostelFeesCategoryServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        public HostelFeesCategory Category_Name(string categoryname, int academicyearid)
        {
           var category_name = (from a in db.HostelFeesCategories
                               where a.AcademicYear == academicyearid && a.CategoryName == categoryname
                               select a).FirstOrDefault();
           return category_name;
        }
        public void AddHostelFeesCategory(string feescategoryname, decimal camount, decimal tax, int AcyearId, int adminid)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            HostelFeesCategory hostelfeescategory = new HostelFeesCategory()
            {
                CategoryName = feescategoryname,
                FeesAmount = camount,
                Tax = tax,
                AcademicYear = AcyearId,
                Status = "Active",
                CreatedBy = Convert.ToString(adminid),
                CreatedDate = date
            };
            db.HostelFeesCategories.Add(hostelfeescategory);
            db.SaveChanges();
}
        public List<FeesCategory> categorylist()
        {
            var list = (from a in db.HostelFeesCategories
                        from b in db.AcademicYears
                        where a.AcademicYear == b.AcademicYearId
                        select new FeesCategory
                        {
                            FeesCategoryId = a.Id,
                            acdemicyear = b.AcademicYear1,
                            feescategoryname = a.CategoryName,
                            camount = a.FeesAmount.Value,
                            status = a.Status,
                            tax = a.Tax.Value,
                            total = a.FeesAmount.Value + a.Tax.Value
                        }).ToList();
            return list;
        }
        public List<HostelFeesCategoryList> categorylist1()
        {
            var list1 = (from a in db.HostelFeesCategories
                        from b in db.AcademicYears
                        where a.AcademicYear == b.AcademicYearId
                         select new HostelFeesCategoryList
                        {
                           
                            Acdemicyear = b.AcademicYear1,
                            HostelFeesCategoryName = a.CategoryName,
                            FeeAmount= a.FeesAmount.Value,
                            Status = a.Status,
                            Tax = a.Tax.Value,
                            Total = a.FeesAmount.Value + a.Tax.Value
                        }).ToList();
            return list1;
        }
        public FeesCategory editlist(int id)
        {
            var feesdetails = (from a in db.HostelFeesCategories
                               from b in db.AcademicYears
                               where id == a.Id && a.AcademicYear == b.AcademicYearId 
                              
                               select new FeesCategory
                               {
                                   FeesCategoryId = a.Id,
                                   acdemicyear = b.AcademicYear1,
                                   AcYearId = b.AcademicYearId,
                                   feescategoryname = a.CategoryName,
                                   camount = a.FeesAmount.Value,
                                   status = a.Status,
                                   tax = a.Tax.Value,
                                   total = a.FeesAmount.Value + a.Tax.Value
                               }).FirstOrDefault();
            return feesdetails;
        }
        public List<HostelFeesCategory> existname_list(string name, int id, int acyearid)
        {
            var fname = (from a in db.HostelFeesCategories where a.Id != id && a.AcademicYear == acyearid && a.CategoryName == name select a).ToList();
            return fname;
        }
        public HostelFeesCollectioncategory checkfeepaid(int id)
        {
            var fname = (from a in db.HostelFeesCollectioncategories where a.FeeCategoryId== id select a).FirstOrDefault();
            return fname;
        }
        public void editable_list(int id,string feescategoryname,decimal camount, string status, int AdminId,decimal tax)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var fid = (from a in db.HostelFeesCategories where id == a.Id select a).FirstOrDefault();
            fid.CategoryName = feescategoryname;
            fid.FeesAmount = camount;
            fid.Tax = tax;
            fid.Status = status;
            fid.UpdatedBy = Convert.ToString(AdminId);
            fid.UpdatedDate = date;
            db.SaveChanges();
        }
        public HostelFeesCategory category_id(int id)
        {
            var idamt = (from a in db.HostelFeesCategories where id == a.Id select a).FirstOrDefault();
            return idamt;
        }
        public IEnumerable GetFeeSubcategory(int yearid,int feecategoryid)
     {
         var list = (from a in db.HostelFeesCategories
                     from b in db.HostelConfigSubCategories
                     where a.AcademicYear == yearid && a.CategoryId == feecategoryid && a.SubCategoryId == b.SubCategoryId && b.Status == true
                     select new
                     {
                         SubcategoryId = b.SubCategoryId,
                         SubCategoryName = b.SubCategoryName

                     }).ToList();
         return list;
     }
        public IList<M_FeeCategoryList> GetFeeStructure(int yearid)
        {
            var list = ((from a in db.HostelFeesCategories
                        from b in db.HostelConfigSubCategories
                        from c in db.HostelConfigCategories
                        where a.AcademicYear == yearid && a.CategoryId==c.CategoryId && c.Status==true && a.SubCategoryId == b.SubCategoryId && b.Status == true
                         select new M_FeeCategoryList
                        {
                            FeeCategoryId=a.CategoryId,
                            CategoryName=c.CategoryName,
                            SubcategoryId = b.SubCategoryId,
                            SubCategoryName = b.SubCategoryName,
                            Total=a.FeesAmount +a.Tax,
                            Feetype="sub"
                        }).Distinct().ToList()).Union(
                        (from a in db.HostelFeesCategories                        
                        from c in db.HostelConfigCategories
                        where a.AcademicYear == yearid && a.CategoryId==c.CategoryId && c.Status==true && a.SubCategoryId == null
                         select new M_FeeCategoryList
                        {
                            FeeCategoryId = a.CategoryId,
                            CategoryName = c.CategoryName,
                            Total = a.FeesAmount + a.Tax,
                            Feetype = "NoSub"
                        }
                        ).Distinct().ToList()).ToList();
            return list;
        }
        public Decimal? getFeeAmount(int yearid,int feectegoryId,int subcategoryId)
        {
            var Feeamount = (from a in db.HostelFeesCategories
                             where a.AcademicYear == yearid && a.CategoryId == feectegoryId && a.SubCategoryId == subcategoryId && a.Status == "Active"
                             select new
                             {
                                 FeeAmount = a.FeesAmount + a.Tax
                             }).FirstOrDefault();
            if (Feeamount != null)
            {
                return Feeamount.FeeAmount;
            }
            else
            {
                return 0;
            }
        }
        public Decimal? getFeeAmount(int yearid, int feectegoryId)
        {
            var Feeamount = (from a in db.HostelFeesCategories
                             where a.AcademicYear == yearid && a.CategoryId == feectegoryId && a.SubCategoryId == null && a.Status == "Active"
                             select new
                             {
                                 FeeAmount = a.FeesAmount + a.Tax
                             }).FirstOrDefault();
            if(Feeamount!=null)
            {
                return Feeamount.FeeAmount;
            }
            else
            {
                return 0;
            }
        }
        public List<GetFeeStructureTransportHostel> getHostelfee(int? AcademicyearId, int? AccommodationFeeCategoryId, int? AccommodationSubFeeCategoryId, int? FoodFeeCategoryId, int? FoodSubFeeCategoryId)
        {
            var result = (
                     (from a in db.HostelConfigCategories
                      from b in db.HostelConfigSubCategories
                      from c in db.HostelFeesCategories
                      where c.AcademicYear == AcademicyearId
               && c.Status == "Active" &&
               a.CategoryId == b.CategoryId &&
               a.CategoryId == c.CategoryId &&
               b.SubCategoryId == c.SubCategoryId &&
               c.CategoryId == AccommodationFeeCategoryId &&
               c.SubCategoryId == AccommodationSubFeeCategoryId
                      select new GetFeeStructureTransportHostel
                     {
                         CategoryId = a.CategoryId,
                         SubCategoryId = b.SubCategoryId,
                         CategoryName = a.CategoryName,
                         SubCategoryName = b.SubCategoryName,
                         hostelfee = c.FeesAmount + c.Tax
                     }).Distinct().ToList()).Union(from a in db.HostelConfigCategories
                                                    from b in db.HostelConfigSubCategories
                                                    from c in db.HostelFeesCategories
                                                    where c.AcademicYear == AcademicyearId
                                                    && c.Status == "Active" &&
                                                    a.CategoryId == b.CategoryId &&
                                                    a.CategoryId == c.CategoryId &&
                                                    b.SubCategoryId == c.SubCategoryId &&
                                                    c.CategoryId == FoodFeeCategoryId &&
                                                    c.SubCategoryId == FoodSubFeeCategoryId
                                                    select new GetFeeStructureTransportHostel
                                                   {
                                                       CategoryId = a.CategoryId,
                                                       SubCategoryId = b.SubCategoryId,
                                                       CategoryName = a.CategoryName,
                                                       SubCategoryName = b.SubCategoryName,
                                                       hostelfee = c.FeesAmount + c.Tax
                                                   }).Distinct().ToList().Union((from a in db.HostelConfigCategories
                                                                                 from b in db.HostelConfigSubCategories
                                                                                 from c in db.HostelFeesCategories
                                                                                 where c.SubCategoryId == null &&
                                                                                     c.AcademicYear == AcademicyearId &&
                                                                                 a.CategoryId == c.CategoryId &&
                                                                                 c.Status == "Active"
                                                                                 select new GetFeeStructureTransportHostel
                                                                                 {
                                                                                    CategoryId = a.CategoryId,
                                                                                    SubCategoryId = b.SubCategoryId,
                                                                                     CategoryName = a.CategoryName,
                                                                                     hostelfee = c.FeesAmount + c.Tax
                                                                                 }).Distinct().ToList()).ToList();
          
            
            return result;
           
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}