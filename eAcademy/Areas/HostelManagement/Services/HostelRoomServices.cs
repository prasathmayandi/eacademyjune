﻿using eAcademy.Areas.HostelManagement.ViewModel;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.HostelManagement.Services
{
    public class HostelRoomServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public void Add_room(string rname,string rcapacity,string rtype,int blockid,int AdminId)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            HostelRoom hostelroom = new HostelRoom()
            {
                RoomName = rname,
                RoomCapacity = Convert.ToInt32(rcapacity),
                RoomAvailablity = Convert.ToInt32(rcapacity),
                RoomType = rtype,
                BlockId = blockid,
                Status = "Active",
                CreatedBy = Convert.ToString(AdminId),
                CreatedDate = date
            };
            dc.HostelRooms.Add(hostelroom);
            dc.SaveChanges();
        }
       public List<EdithostelDetails>Edit_roomDetails(int id)
        {
            var roomdetails = (from a in dc.HostelRooms
                               where a.BlockId == id
                               select new EdithostelDetails { BlockId = a.BlockId.Value, roomId = a.RoomId, roomname = a.RoomName, roomcapacity = a.RoomCapacity.Value, roomtype = a.RoomType }).ToList();
            return roomdetails;
        }
        public List<HostelRoom>updateroomdetails(int id)
       {
           var existroom = (from a in dc.HostelRooms where id == a.BlockId select a).ToList();
           return existroom;
       }
       public void update_room(int Rid, string r_name, string rcacity, string r_type, int AdminId)
       {
           var date = DateTimeByZone.getCurrentDateTime();
           var c = (from a in dc.HostelRooms where Rid == a.RoomId select a).FirstOrDefault();
           c.RoomCapacity =Convert.ToInt16(rcacity);
           c.RoomName = r_name;
           c.RoomType = r_type;
           c.UpdatedBy = Convert.ToString(AdminId);
           c.UpdatedDate = date;
           dc.SaveChanges();
       }
       public void deleteroom(int drid)
       {
           var delrow = (from a in dc.HostelRooms where a.RoomId == drid select a).FirstOrDefault();
           dc.HostelRooms.Remove(delrow);
           dc.SaveChanges();
       }
        public IEnumerable<Object> GetStudenthostelRooms(int Blockid)
        {
            var ans = (from a in dc.HostelRooms
                       where a.BlockId == Blockid && a.RoomAvailablity > 0 && a.Status == "Active" && a.RoomType == "Student"
                       select new
                       {
                           RoomId = a.RoomId,
                           RoomName = a.RoomName
                       }).Distinct().ToList();
            return ans;
        }
        public IEnumerable<Object> GetStaffhostelRooms(int Blockid)
        {
            var ans = (from a in dc.HostelRooms
                       where a.BlockId == Blockid && a.RoomAvailablity > 0 && a.Status == "Active" && a.RoomType == "Employee"
                       select new
                       {
                           RoomId = a.RoomId,
                           RoomName = a.RoomName
                       }).Distinct().ToList();
            return ans;
        }
        public IEnumerable<Object> GetStudenthostelRoomsList()
        {
            var ans = (from a in dc.HostelRooms
                       where  a.Status == "Active" && a.RoomType == "Student"
                       select new
                       {
                           RoomId = a.RoomId,
                           RoomName = a.RoomName
                       }).Distinct().ToList();
            return ans;
        }
        public IEnumerable<Object> GetStaffhostelRoomsList()
        {
            var ans = (from a in dc.HostelRooms
                       where a.Status == "Active" && a.RoomType == "Employee"
                       select new
                       {
                           RoomId = a.RoomId,
                           RoomName = a.RoomName
                       }).Distinct().ToList();
            return ans;
        }
        public void ReduceRoomAvailablity(int blockid,int roomid)
        {
            var getrow = dc.HostelRooms.Where(q => q.BlockId == blockid && q.RoomId == roomid && q.RoomAvailablity > 0).FirstOrDefault();
            if(getrow!=null)
            {
                int count =Convert.ToInt16(  getrow.RoomAvailablity);
                count--;
                getrow.RoomAvailablity = count;
                dc.SaveChanges();
            }
        }
        public void increaseRoomAvailablity(int blockid, int roomid)
        {
            var getrow = dc.HostelRooms.Where(q => q.BlockId == blockid && q.RoomId == roomid && q.RoomAvailablity > 0).FirstOrDefault();
            if (getrow != null)
            {
                int count = Convert.ToInt16(getrow.RoomAvailablity);
                count++;
                getrow.RoomAvailablity = count;
                dc.SaveChanges();
            }
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}