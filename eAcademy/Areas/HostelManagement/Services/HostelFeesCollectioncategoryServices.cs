﻿using eAcademy.Areas.HostelManagement.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.HostelManagement.Services
{
    public class HostelFeesCollectioncategoryServices : IDisposable
    
    {
        EacademyEntities db = new EacademyEntities();
        public void savefeescollectioncategoryid(string feescollectionid, int acid, int std_id,int sectionid,int classid, int AdminId,int id,decimal FeesAmount,decimal Tax,decimal totamt)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            HostelFeesCollectioncategory hostelfeescollectioncategory = new HostelFeesCollectioncategory()
            {
                FeeCollectionId = Convert.ToInt16(feescollectionid),
                AcademicYearId = acid,
                StudentRegisterId = std_id,
                SectionId = Convert.ToInt16(sectionid),
                ClassId = Convert.ToInt16(classid),
                CreatedBy = AdminId,
                CreatedDate = date,
                Status = "Active",
                FeeCategoryId = id,
                Amount = FeesAmount,
                ServiceTax = Tax,
                TotalAmount = totamt,
            };
            db.HostelFeesCollectioncategories.Add(hostelfeescollectioncategory);
            db.SaveChanges();
        }
        public List<SaveFeeCollection1> GetFeeCollectionCategory(int CollectId)
        {
            var ans =
                    (from a in db.HostelFeesCollections
                     from b in db.HostelFeesCollectioncategories
                     from c in db.HostelFeesCategories
                     where a.FeeCollectionId == CollectId && b.FeeCollectionId == a.FeeCollectionId && c.Id == b.FeeCategoryId 
                     select new SaveFeeCollection1
                     {
                         FeeCategory = c.CategoryName,
                         Amount = b.Amount,
                         ServiceTax = b.ServiceTax,
                         Total = b.TotalAmount
                     })
                .Distinct().OrderBy(q => q.FeeCategory).ToList();
            return ans;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}