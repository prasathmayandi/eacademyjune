﻿namespace eAcademy.Areas.HostelManagement.ViewModel
{
    public class Exportexcelblock
    {
        public string BlockName { get; set; }
        public string BlockType { get; set; }
        public string status { get; set; }
    }
}