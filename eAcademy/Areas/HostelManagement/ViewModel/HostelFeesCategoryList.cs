﻿namespace eAcademy.Areas.HostelManagement.ViewModel
{
    public class HostelFeesCategoryList
    {
        public string HostelFeesCategoryName { get; set; }
        public decimal FeeAmount { get; set; }
        public string Status { get; set; }
        public decimal Tax { get; set; }
        public string Acdemicyear { get; set; }
        public decimal Total { get; set; }
    }
}