﻿namespace eAcademy.Areas.HostelManagement.ViewModel
{
    public class BlockList
    {
        public int BlockId { get; set; }
        public string BlockName { get; set; }
        public string BlockType { get; set; }
        public string status { get; set; }
   }
}