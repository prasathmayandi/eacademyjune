﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Areas.HostelManagement.ViewModel
{
    public class VM_AllotHostelRooms
    {
       
        //[Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
        //              ErrorMessageResourceName = "plsenterfeesamount")]
        public int UserId { get; set; }
        
         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                       ErrorMessageResourceName = "PleaseselectBlockName")]
        public int BlockName { get; set; }
        
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                      ErrorMessageResourceName = "PleaseselectRoomName")]
        public int RoomNumber { get; set; }

        public string Etype { get; set; }
        public int RoomAllotmentId { get; set; }
    }
}