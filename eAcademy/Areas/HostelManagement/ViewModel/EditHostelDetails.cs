﻿using System.ComponentModel.DataAnnotations;
namespace eAcademy.Areas.HostelManagement.ViewModel
{
    public class EdithostelDetails
    {
        public int  BlockId { get; set; }
            [StringLength(50, ErrorMessageResourceType = typeof(LocalResources.Resource),
            ErrorMessageResourceName = "Blocknamemaximum45characters")]
            [Required(ErrorMessageResourceType = typeof(LocalResources.Resource),
            ErrorMessageResourceName = "PleaseEnterBlockName")]
            public string blockname { get; set; }
            [Required(ErrorMessageResourceType = typeof(LocalResources.Resource),
            ErrorMessageResourceName = "pleaseselecthosteltype")]
            public string bnametype { get; set; }
            [Required(ErrorMessageResourceType = typeof(LocalResources.Resource),
            ErrorMessageResourceName = "PleaseEnterRoomName")]
            public string roomname { get; set; }
            [Required(ErrorMessageResourceType = typeof(LocalResources.Resource),
            ErrorMessageResourceName = "PleaseEnterRoomtype")]
            public int roomcapacity { get; set; }
            public string blockstatus { get; set; }
            public int count { get; set; }
            public int roomId { get; set; }
            public string roomtype { get; set; }
    }
}  
        
        
        
       