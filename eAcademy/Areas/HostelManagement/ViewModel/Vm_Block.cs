﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using eAcademy.HelperClass;
namespace eAcademy.Areas.HostelManagement.ViewModel
{
    public class Vm_Block
    {
        public int BlockId { get; set; }

        [StringLength(50,ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                      ErrorMessageResourceName = "Blocknamemaximum45characters")]
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                      ErrorMessageResourceName = "PleaseEnterBlockName")]
        public string name { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                       ErrorMessageResourceName = "pleaseselecthosteltype")]
        public string type { get; set; }

       
        [StringArrayRequired(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                       ErrorMessageResourceName = "PleaseEnterRoomName")]
        public string[] roomname { get; set; }


        [StringArrayRequired(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                       ErrorMessageResourceName = "PleaseEnterRoomCapacity")]
        public string[] roomcapacity { get; set; }

        public string status { get; set; }
        public int count { get; set; }

        [StringArrayRequired(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                       ErrorMessageResourceName = "PleaseEnterRoomtype")]
        public string[] roomtype { get; set; }
        public int[] roomid { get; set; }
    }
}
        
       
       
        
        