﻿
namespace eAcademy.Areas.HostelManagement.ViewModel
{
    public class HostelFeesList
    {
        public int acyearid { get; set; }
        public int categoryid { get; set; }
        public string categoryname { get; set; }
        public decimal Amount { get; set; }
        public decimal tax { get; set; }
        public decimal total { get; set; }
    }
}