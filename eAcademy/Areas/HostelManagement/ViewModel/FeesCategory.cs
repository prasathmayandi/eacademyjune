﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using eAcademy.HelperClass;
namespace eAcademy.Areas.HostelManagement.ViewModel
{
    public class FeesCategory
    {
        public int FeesCategoryId { get; set; }

        //[Required(ErrorMessageResourceType = typeof(LocalResources.Resource),
        //ErrorMessageResourceName = "plsenterfeescategoryname")]
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                       ErrorMessageResourceName = "plsenterfeescategoryname")]
        public string feescategoryname { get; set; }

         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                       ErrorMessageResourceName = "plsenterfeesamount")]
        public decimal camount { get; set; }
        public string status { get; set; }
        
         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                       ErrorMessageResourceName = "plsentertax")]
        public decimal tax { get; set; }
        
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.HostelManagement.Resources.HostelResources),
                       ErrorMessageResourceName = "SelectAcademicYear")]
        public int AcYearId { get; set; }
        public string acdemicyear { get; set; }
        public decimal total { get; set; }
      
     }
}