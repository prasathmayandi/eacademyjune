﻿using eAcademy.Services;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.HostelManagement.Resources
{
    public class HostelResources
    {
        public static string plsenterfeescategoryname
        {
            get
            {
                return ResourceCache.Localize("plsenterfeescategoryname");
            }
        }
        public static string plsenterfeesamount
        {
            get
            {
                return ResourceCache.Localize("plsenterfeesamount");
            }
        }
        public static string SelectAcademicYear
        {
            get
            {
                return ResourceCache.Localize("SelectAcademicYear");
            }
        }
        public static string PleaseEnterBlockName
        {
            get
            {
                return ResourceCache.Localize("PleaseEnterBlockName");
            }
        }
        public static string PleaseEnterRoomtype
        {
            get
            {
                return ResourceCache.Localize("PleaseEnterRoomtype");
            }
        }
        public static string pleaseselecthosteltype
        {
            get
            {
                return ResourceCache.Localize("pleaseselecthosteltype");
            }
        }
        public static string PleaseEnterRoomCapacity
        {
            get
            {
                return ResourceCache.Localize("PleaseEnterRoomCapacity");
            }
        }
        public static string PleaseEnterRoomName
        {
            get
            {
                return ResourceCache.Localize("PleaseEnterRoomName");
            }
        }
        public static string plsentertax
        {
            get
            {
                return ResourceCache.Localize("plsentertax");
            }
        }
        public static string PleaseselectBlockName
        {
            get
            {
                return ResourceCache.Localize("PleaseselectBlockName");
            }
        }
        public static string PleaseselectRoomName
        {
            get
            {
                return ResourceCache.Localize("PleaseselectRoomName");
            }
        }
        public static string Please_Enter_Register_number
        {
            get
            {
                return ResourceCache.Localize("Please_Enter_Register_number");
            }
        }
        public static string Please_Select_Payment_Mode
        {
            get
            {
                return ResourceCache.Localize("Please_Select_Payment_Mode");
            }
        }
        public static string EnterBankName
        {
            get
            {
                return ResourceCache.Localize("EnterBankName");
            }
        }
        public static string EnterChequeNumber
        {
            get
            {
                return ResourceCache.Localize("EnterChequeNumber");
            }
        }
        public static string EnterDDNumber
        {
            get
            {
                return ResourceCache.Localize("EnterDDNumber");
            }
        }
        public static string PleaseSelectClass
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectClass");
            }
        }
        public static string PleaseSelectSection
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectSection");
            }
        }
       
       
       
       
       
       
       
       
       
       












    }
}