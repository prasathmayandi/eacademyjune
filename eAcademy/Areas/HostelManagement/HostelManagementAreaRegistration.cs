﻿using System.Web.Mvc;

namespace eAcademy.Areas.HostelManagement
{
    public class HostelManagementAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HostelManagement";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HostelManagement_default",
                "HostelManagement/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
