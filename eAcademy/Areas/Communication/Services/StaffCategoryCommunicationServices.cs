﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
namespace eAcademy.Areas.Communication.Services
{
    public class StaffCategoryCommunicationServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public void addCommunication(int YearId, DateTime date, string title, string desc, string fileName, int EmpTypeId, int markedby)
        {
            StaffCategoryCommunication add = new StaffCategoryCommunication()
            {
                AcademicYearId = YearId,
                DateOfAnnouncement = date,
                Title = title,
                Description = desc,
                FileName = fileName,
                EmployeeTypeId = EmpTypeId,
                MarkedBy = markedby,
                MarkedDate = DateTimeByZone.getCurrentDateTime(),
                Status = true
            };
            dc.StaffCategoryCommunications.Add(add);
            dc.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}