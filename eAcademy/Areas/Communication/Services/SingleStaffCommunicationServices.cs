﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
namespace eAcademy.Areas.Communication.Services
{
    public class SingleStaffCommunicationServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public void addCommunication(int YearId, DateTime date, string title, string desc, string CommnFileName, int empTypeId, int eid, int markedby)
        {
            SingleStaffCommunication add = new SingleStaffCommunication()
            {
                AcademicYearId = YearId,
                DateOfAnnouncement = date,
                Title = title,
                Description = desc,
                FileName = CommnFileName,
                EmployeeTypeId = empTypeId,
                EmployeeRegisterId = eid,
                MarkedBy = markedby,
                MarkedDate = DateTimeByZone.getCurrentDateTime(),
                Status = true
            };
            dc.SingleStaffCommunications.Add(add);
            dc.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}