﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
namespace eAcademy.Areas.Communication.Services
{
    public class ClassCommunicationServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public void addCommunication(int YearId, DateTime date,string title,string desc,string fileName, int cid, int markedby)
        {
            ClassCommunication add = new ClassCommunication()
            {
                AcademicYearId = YearId,
                DateOfAnnouncement = date,
                Title = title,
                Description = desc,
                FileName = fileName,
                ClassId = cid,
                MarkedBy = markedby,
                MarkedDate = DateTimeByZone.getCurrentDateTime(),
                Status = true
            };
            dc.ClassCommunications.Add(add);
            dc.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}