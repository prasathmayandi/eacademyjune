﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
namespace eAcademy.Areas.Communication.Services
{
    public class StudentCommunicationServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        private static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");

        public void addCommunication(int YearId, DateTime date, string title, string desc, string fileName,int classid,int secid,int sid, int markedby)
        {

          //  DateTime now = DateTime.UtcNow;
           // DateTime localNow = TimeZoneInfo.ConvertTimeFromUtc(now, TimeZoneInfo.Local);

          //  DateTime localNow = TimeZoneInfo.ConvertTimeFromUtc(now, TimeZoneInfo.Local);

           // DateTime x = TimeZoneInfo.ConvertTimeFromUtc(DateTimeByZone.getCurrentDateTime(), TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"));

          //  DateTime time = DateTime.UtcNow.ToTimeZoneTime("Pacific Standard Time");

          //   DateTime convertedDate = DateTime.SpecifyKind(DateTime.Parse(DateTimeByZone.getCurrentDateTime().ToString()),DateTimeKind.Utc);

         //   var tz = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
         //   DateTime time2 = DateTime.UtcNow..ToTimeZoneTime(tz);



            //TimeZoneInfo maltaTimeZone = TimeZoneInfo.FindSystemTimeZoneById("...");
            //DateTime utc = DateTime.UtcNow;
            //DateTime malta = TimeZoneInfo.ConvertTimeFromUtc(utc, maltaTimeZone);

          //  var startDate = DateTimeByZone.getCurrentDateTime();

            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            

          //  var StartDateUtc = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.SpecifyKind(startDate.Date, DateTimeKind.Unspecified), "Asia/Kolkata", "UTC"); 
         //   var StartDateUtc = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.SpecifyKind(startDate.Date, DateTimeKind.Unspecified), "Indian Standard Time", "UTC");



            StudentCommunication add = new StudentCommunication()
            {
                AcademicYearId = YearId,
                DateOfAnnouncement = date,
                Title = title,
                Description = desc,
                FileName = fileName,
                ClassId = classid,
                SectionId = secid,
                StudentRegisterId = sid,
                MarkedBy = markedby,
                MarkedDate = DateTimeByZone.getCurrentDateTime(),
                Status = true,
                NotificationStatus = "UnRead"
            };
            dc.StudentCommunications.Add(add);
            dc.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}