﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.Communication.ViewModel
{
    public class stuMessage
    {
        public string allAcademicYears { get; set; }
        public string allClass { get; set; }
        public int Section { get; set; }
        public int[] Id { get; set; }
        public int TotalCount { get; set; }
    }
}