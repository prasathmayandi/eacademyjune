﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.Communication.Models
{
    public class EmailClassStudentList
    {
        public string Email { get; set; }
        public long? MobileNum { get; set; }
    }
}