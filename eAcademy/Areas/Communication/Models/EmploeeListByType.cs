﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.Communication.Models
{
    public class EmploeeListByType
    {
        public string EmployeeId { get; set; }
        public string Name { get; set; }
        public string EmployeeRegId { get; set; }
    }
}