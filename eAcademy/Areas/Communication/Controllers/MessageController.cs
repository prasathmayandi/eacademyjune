﻿using eAcademy.Areas.Communication.Models;
using eAcademy.Areas.Communication.Services;
using eAcademy.Areas.Communication.ViewModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
namespace eAcademy.Areas.Communication.Controllers
{
    [CheckSessionOutAttribute]
    public class MessageController : Controller
    {
        public int AdminId;
        UserActivityHelper useractivity = new UserActivityHelper();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AdminId = Convert.ToInt32(Session["empRegId"]);
            AcademicyearServices AcYear = new AcademicyearServices();
            ClassServices clas = new ClassServices();
            EmployeeTypeServices empType = new EmployeeTypeServices();
            ViewBag.allAcademicYears = AcYear.CurrentDateAcademicYear();
            ViewBag.allClasses = clas.ShowClasses();
            ViewBag.AllEmployeeType = empType.ShowEmployeeType();
        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (Session["CurrentCulture"] != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["CurrentCulture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["CurrentCulture"].ToString());
            }
        }
        public JsonResult GetSelectedClassSection(int passClassId)
        {
            SectionServices sec = new SectionServices();
            var List = sec.getSection(passClassId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FutureActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    EventServices holidays = new EventServices();
                    int acid = CurrentAcYear[i].AcademicYearId;
                    var HolidaysList = holidays.getHolidaysList(acid);
                    return Json(new { StartingEnableDate, EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { StartingEnableDate, EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetCommunicationStatus()
        {
            SchoolSettingsServices setServ = new SchoolSettingsServices();
            var row = setServ.SchoolSettings();
            var EmailStatus = row.EmailRequired;
            var MobileStatus = row.SmsRequired;
            return Json(new { EmailStatus, MobileStatus }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStudentList(int passYearId, int passClassId,int passSecId)
        {
            AssignClassToStudentServices stu = new AssignClassToStudentServices();
            var list = stu.getStudentListToStudentCommunication(passYearId, passClassId, passSecId);
            int Count = list.ToList().Count;
            string StudentCount="";
            if (Count == 0)
            {
                StudentCount = "empty";               
            }
            return Json(new { StudentCount, list }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetEmployeeList(string passEmployeeTypeId)
        {
            int empTypeId = Convert.ToInt32(passEmployeeTypeId);
               TechEmployeeServices techEmpSer = new TechEmployeeServices();
                var list = techEmpSer.getEmploeeListByEmployeeTypeId(empTypeId);
                int Count = list.Count;
                string EmployeeCount = "";
                if (Count == 0)
                {
                    EmployeeCount = "empty";
                }
                return Json(new { EmployeeCount, list }, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    //OtherEmployeeServices otheEmpSer = new OtherEmployeeServices();
            //    //var list = otheEmpSer.getEmploeeListByEmployeeTypeId(empTypeId);
            //    //int Count = list.Count;
            //    //string EmployeeCount = "";
            //    //if (Count == 0)
            //    //{
            //    //    EmployeeCount = "empty";
            //    //}
            //    //return Json(new { EmployeeCount, list }, JsonRequestBehavior.AllowGet);
            //}
        }
        [HttpPost]
        public JsonResult SendMessage()
        {
            var year = Request["year"];
            DateTime date = Convert.ToDateTime(Request["date"]);
            string title = Request["title"];
            string desc = Request["desc"];
            string email = Request["email"];
            string sms = Request["sms"];
            string To = Request["To"];
            if (year != null && date != null && title != null && desc != null )
            {
                int YearId = Convert.ToInt32(year);
                int currentAcYearId = Convert.ToInt32(Session["CurrentAcYearId"]);
                if(YearId == currentAcYearId)
                {
                    string CommnFileName = null;
                    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                    {
                        var file1 = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                        string filenamewithoutextention;
                        Random random = new Random();
                        string fname = random.Next(00000, 99999).ToString();
                        string extention = System.IO.Path.GetExtension(file1.FileName);
                        var fileSize = file1.ContentLength;
                        fileSize = file1.ContentLength / 1048576;
                        if (extention == ".pdf" || extention == ".docx" || extention == ".doc")
                        {
                            if (file1 != null && file1.ContentLength > 0 && fileSize <= 3)
                            {
                                filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file1.FileName);
                                CommnFileName = "Communication" + fname + extention;
                                string path = System.IO.Path.Combine(Server.MapPath("~/Areas/Communication/Documents"), CommnFileName);
                                file1.SaveAs(path);
                            }
                            else
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("FileSizeExceeds");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("FileFormatDoesNotSupport");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (To == "Class")
                    {
                        string Check_class = Request["allclass"];
                        string single_clas = Request["singleClass"];
                        ClassCommunicationServices classCommunication = new ClassCommunicationServices();
                        AssignClassToStudentServices ClasStu = new AssignClassToStudentServices();
                        if (Check_class != "off")
                        {  //all
                            ClassServices obj_class = new ClassServices();
                            var ans = obj_class.getAllClassId();
                            for (int k = 0; k < ans.Count; k++)
                            {
                                int cid = ans[k];
                                classCommunication.addCommunication(YearId, date, title, desc, CommnFileName, cid, AdminId);
                                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("AddClassCommunication"));
                                var studentEmailList = ClasStu.getClassStudentList(YearId, cid);
                                if (email != null && studentEmailList.Count != 0)
                                {
                                    BulkMail(studentEmailList, date, title, desc, CommnFileName);
                                }
                                if (sms != null && studentEmailList.Count != 0)
                                {
                                    BulkSms(studentEmailList, date, title, desc);
                                }
                            }
                        }
                        else
                        {
                            string classid = single_clas;
                            string[] c_id = classid.Split(',');
                            for (int a = 0; a < c_id.Length; a++)
                            {
                                int cid = Convert.ToInt32(c_id[a]);
                                classCommunication.addCommunication(YearId, date, title, desc, CommnFileName, cid, AdminId);
                                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("AddClassCommunication"));
                                var studentEmailList = ClasStu.getClassStudentList(YearId, cid);
                                if (email != null && studentEmailList.Count != 0)
                                {
                                    BulkMail(studentEmailList, date, title, desc, CommnFileName);
                                }
                                if (sms != null && studentEmailList.Count != 0)
                                {
                                    BulkSms(studentEmailList, date, title, desc);
                                }
                            }
                        }
                    }
                    else if (To == "Student")
                    {
                        string classId = Request["Stu_allClass"];
                        string SecId = Request["Stu_Section"];
                        string stuRegId = Request["Stu_RegId"];
                        int classid = Convert.ToInt32(classId);
                        int secid = Convert.ToInt32(SecId);
                        StudentCommunicationServices stuCommnServ = new StudentCommunicationServices();
                        AssignClassToStudentServices ClasStu = new AssignClassToStudentServices();

                        string[] s_id = stuRegId.Split(',');
                        for (int a = 0; a < s_id.Length; a++)
                        {
                            if (s_id[a] != "No")
                            {
                                int sid = Convert.ToInt32(QSCrypt.Decrypt(s_id[a]));
                                stuCommnServ.addCommunication(YearId, date, title, desc, CommnFileName, classid, secid, sid, AdminId);
                                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("AddStudentCommunication"));
                                var getPrimaryUserEmail = ClasStu.getStudentPrmaryUserEmail(YearId, classid, secid, sid);
                                if (email != null && getPrimaryUserEmail != null)
                                {
                                    string Tomail = getPrimaryUserEmail.Email;
                                    SingleMail(Tomail, date, title, desc, CommnFileName);
                                }
                                if (sms != null && getPrimaryUserEmail != null)
                                {
                                    string ToNumber = Convert.ToString(getPrimaryUserEmail.MobileNum);
                                    SingleSms(ToNumber, date, title, desc);
                                }
                            }
                        }
                    }
                    else if (To == "AllStaff")
                    {
                        string Check_AllstaffCategory = Request["allstaffCategory"];
                        string single_staffCategory = Request["singleStaffCategory"];
                        StaffCategoryCommunicationServices staffCommunication = new StaffCategoryCommunicationServices();
                        TechEmployeeServices techEmp = new TechEmployeeServices();
                        if (Check_AllstaffCategory == "on")
                        {
                            EmployeeTypeServices obj_class = new EmployeeTypeServices();
                            var ans = obj_class.getAllEmployeeTypeId();
                            for (int k = 0; k < ans.Count; k++)
                            {
                                int EmpTypeId = ans[k];
                                staffCommunication.addCommunication(YearId, date, title, desc, CommnFileName, EmpTypeId, AdminId);
                                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("AddStaffCommunication"));
                                IList<EmailClassStudentList> EmailList;
                                if (EmpTypeId == 1 || EmpTypeId == 2 || EmpTypeId == 3)
                                {
                                    EmailList = techEmp.getFacultyEmailList(YearId, EmpTypeId);
                                }
                                else
                                {
                                    EmailList = techEmp.OtherEmployeeEmailList(YearId, EmpTypeId);
                                }
                                if (email != null && EmailList.Count != 0)
                                {
                                    BulkMail(EmailList, date, title, desc, CommnFileName);
                                }
                                if (sms != null && EmailList.Count != 0)
                                {
                                    BulkSms(EmailList, date, title, desc);
                                }
                            }
                        }
                        else
                        {
                            string staffCategoryid = single_staffCategory;
                            string[] c_id = staffCategoryid.Split(',');
                            for (int a = 0; a < c_id.Length; a++)
                            {
                                int EmpTypeId = Convert.ToInt32(c_id[a]);
                                staffCommunication.addCommunication(YearId, date, title, desc, CommnFileName, EmpTypeId, AdminId);
                                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("AddStaffCommunication"));
                                IList<EmailClassStudentList> EmailList;
                                if (EmpTypeId == 1 || EmpTypeId == 2 || EmpTypeId == 3)
                                {
                                    EmailList = techEmp.getFacultyEmailList(YearId, EmpTypeId);
                                }
                                else
                                {
                                    EmailList = techEmp.OtherEmployeeEmailList(YearId, EmpTypeId);
                                }
                                if (email != null && EmailList.Count != 0)
                                {
                                    BulkMail(EmailList, date, title, desc, CommnFileName);
                                }
                                if (sms != null && EmailList.Count != 0)
                                {
                                    BulkSms(EmailList, date, title, desc);
                                }
                            }
                        }
                    }
                    else if (To == "SingleStaff")
                    {
                        string empTypeId = Request["emp_allStaff"];
                        string empRegId = Request["emp_RegId"];
                        int emptypeId = Convert.ToInt32(empTypeId);
                        SingleStaffCommunicationServices singleStaffCommnServ = new SingleStaffCommunicationServices();
                        TechEmployeeServices techEmp = new TechEmployeeServices();
                        string[] e_id = empRegId.Split(',');
                        for (int a = 0; a < e_id.Length; a++)
                        {
                            if (e_id[a] != "No")
                            {
                                int eid = Convert.ToInt32(QSCrypt.Decrypt(e_id[a]));
                                singleStaffCommnServ.addCommunication(YearId, date, title, desc, CommnFileName, emptypeId, eid, AdminId);
                                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_single_staff_communication"));
                                string EmailId = ""; string MobNo = "";
                                if (emptypeId == 1 || emptypeId == 2 || emptypeId == 3)
                                {
                                    var EmailList = techEmp.getFacultyEmail(YearId, emptypeId, eid);
                                    EmailId = EmailList.Email;
                                    MobNo = Convert.ToString(EmailList.MobileNum);
                                }
                                else
                                {
                                    var EmailList = techEmp.OtherEmployeeEmail(YearId, emptypeId, eid);
                                    EmailId = EmailList.Email;
                                    MobNo = Convert.ToString(EmailList.MobileNum);
                                }
                                if (email != null)
                                {
                                    string Tomail = EmailId;
                                    if (Tomail != null && Tomail != "")
                                    {
                                        SingleMail(EmailId, date, title, desc, CommnFileName);
                                    }
                                }
                                if (sms != null)
                                {
                                    string ToNumber = Convert.ToString(MobNo);
                                    if (ToNumber != null && ToNumber != "")
                                    {
                                        SingleSms(ToNumber, date, title, desc);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        StaffCategoryCommunicationServices staffCommunication = new StaffCategoryCommunicationServices();
                        EmployeeTypeServices empTypeServ = new EmployeeTypeServices();
                        TechEmployeeServices techEmp = new TechEmployeeServices();
                        var EmployeeTypeList = empTypeServ.getAllEmployeeTypeId();
                        for (int i = 0; i < EmployeeTypeList.Count; i++)
                        {
                            int EmpTypeId = EmployeeTypeList[i];
                            staffCommunication.addCommunication(YearId, date, title, desc, CommnFileName, EmpTypeId, AdminId);
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_single_staff_communication"));
                            IList<EmailClassStudentList> EmailList;
                            if (EmpTypeId == 1 || EmpTypeId == 2 || EmpTypeId == 3)
                            {
                                EmailList = techEmp.getFacultyEmailList(YearId, EmpTypeId);
                            }
                            else
                            {
                                EmailList = techEmp.OtherEmployeeEmailList(YearId, EmpTypeId);
                            }
                            if (email != null && EmailList.Count != 0)
                            {
                                BulkMail(EmailList, date, title, desc, CommnFileName);
                            }
                            if (sms != null && EmailList.Count != 0)
                            {
                                BulkSms(EmailList, date, title, desc);
                            }
                        }
                        ClassServices obj_class = new ClassServices();
                        ClassCommunicationServices classCommunication = new ClassCommunicationServices();
                        AssignClassToStudentServices ClasStu = new AssignClassToStudentServices();
                        var ans = obj_class.getAllClassId();
                        for (int k = 0; k < ans.Count; k++)
                        {
                            int cid = ans[k];
                            classCommunication.addCommunication(YearId, date, title, desc, CommnFileName, cid, AdminId);
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("AddClassCommunication"));
                            var studentEmailList = ClasStu.getClassStudentList(YearId, cid);
                            if (email != null && studentEmailList.Count != 0)
                            {
                                BulkMail(studentEmailList, date, title, desc, CommnFileName);
                            }
                            if (sms != null && studentEmailList.Count != 0)
                            {
                                BulkSms(studentEmailList, date, title, desc);
                            }
                        }
                    }
                    var Message = eAcademy.Models.ResourceCache.Localize("MessageSentSuccessfully");
                    return Json(new { Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string InfoMessage = ResourceCache.Localize("Message_can_send_for_current_academic_year_only");
                    return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var ErrorMessage = eAcademy.Models.ResourceCache.Localize("MessageNotSent");
                return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public void BulkMail(IList<EmailClassStudentList> EmailList,DateTime date, string title,string desc,string CommnFileName)
        {
            int count = EmailList.Count;
            string[] Email = new string[count];
            for (int i = 0; i < count; i++)
            {
                Email[i] = EmailList[i].Email;
            }
            string Tomail = string.Join(",", Email);
            StreamReader reader = new StreamReader(Server.MapPath("~/Areas/Communication/Template/ClassCommunication.html"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
           // myString = myString.Replace("$$subject$$", title);
            myString = myString.Replace("$$Date$$", date.ToString("dd-MM-yyyy"));
            myString = myString.Replace("$$title$$", title.ToString());
            myString = myString.Replace("$$description$$", desc.ToString());
            string sub = "Announcement on " + date.ToString("dd-MM-yyyy");
            string path = "";
            if (CommnFileName != null && CommnFileName != "")
            {
                path = System.IO.Path.Combine(Server.MapPath("~/Areas/Communication/Documents"), CommnFileName);
            }
            string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
            string fromDisplayName = ConfigurationManager.AppSettings["EmailFromDisplayName"];
            string PlaceName = "Communication-Message-Announcement";
            string UserName = "Employee";
            string UserId = Convert.ToString(Session["username"]);

            Mail.SendMultipleMail(fromDisplayName, supportEmail, Tomail, sub, myString.ToString(), true, path, PlaceName, UserName, UserId);  
        }
        public void SingleMail(string Tomail, DateTime date, string title, string desc, string CommnFileName)
        {
            StreamReader reader = new StreamReader(Server.MapPath("~/Areas/Communication/Template/ClassCommunication.html"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$subject$$", title);
            myString = myString.Replace("$$Date$$", date.ToString("dd-MM-yyyy"));
            myString = myString.Replace("$$title$$", title.ToString());
            myString = myString.Replace("$$description$$", desc.ToString());
            string sub = "Announcement on " + date.ToString("dd-MM-yyyy");
            string path = "";
            if (CommnFileName != null && CommnFileName != "")
            {
                path = System.IO.Path.Combine(Server.MapPath("~/Areas/Communication/Documents"), CommnFileName);
            }
            string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
            string fromDisplayName = ConfigurationManager.AppSettings["EmailFromDisplayName"];
            string PlaceName = "MessageController-Announcement Mail";
            string UserName = "Employee";
            string UserId = Convert.ToString(Session["empRegId"]);
            Mail.SendMailWithFile(fromDisplayName, supportEmail, Tomail, sub, myString.ToString(), true, path,PlaceName, UserName, UserId);  
        }
        public void BulkSms(IList<EmailClassStudentList> MobileNumberList, DateTime date, string title, string desc)
        {
            int count = MobileNumberList.Count;
            string[] mobileNumber = new string[count];
            for (int i = 0; i < count; i++)
            {
                mobileNumber[i] = Convert.ToString(MobileNumberList[i].MobileNum);
            }
            string ToNumber = string.Join(",", mobileNumber);
            string sub = "Announcement on" + date.ToString("dd-MM-yyyy");
            string Description = desc;
            string fromDisplayName = ConfigurationManager.AppSettings["EmailFromDisplayName"];
            SMS.Main(fromDisplayName, ToNumber, sub, Description);
        }
        public void SingleSms(string ToNumber,DateTime date, string title, string desc)
        {
            string sub = "Announcement on" + date.ToString("dd-MM-yyyy");
            string Description = desc;
            string fromDisplayName = ConfigurationManager.AppSettings["EmailFromDisplayName"];
            SMS.Main(fromDisplayName, ToNumber, sub, Description);
        }

    }
}
