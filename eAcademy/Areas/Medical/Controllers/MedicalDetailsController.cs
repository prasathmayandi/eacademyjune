﻿using eAcademy.Areas.Medical.Services;
using eAcademy.Areas.Medical.ViewModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace eAcademy.Areas.Medical.Controllers
{
    [CheckSessionOutAttribute]
    public class MedicalDetailsController : Controller
    {
        public int AdminId;       
        MedicalServices ms = new MedicalServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        AcademicyearServices acservice = new AcademicyearServices();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ViewBag.acYear = ms.AcademicYear();
            ViewBag.ClassList = ms.Classes();
            AdminId = Convert.ToInt32(Session["empRegId"]);
        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (Session["CurrentCulture"] != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["CurrentCulture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["CurrentCulture"].ToString());
            }
        }
        //Language change
        public ActionResult ChangeCulture(string PassCulture)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo(PassCulture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(PassCulture);

            Session["CurrentCulture"] = PassCulture;
            //string currenturl = Request.Url.AbsoluteUri;
            string Message = "Language changed";
            string selVal = PassCulture;
            return Json(new { Message, selVal }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCulture()
        {
            var cul = Session["CurrentCulture"];
            if (cul != "" && cul != null)
            {
                return Json(new { cul }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                cul = "empty";
                return Json(new { cul }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getClass()
        {
            try
            {
                var ans = ms.Classes();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult getSection(int cid)
        {            
            try
            {
                var ans = ms.getSectionByClassId(cid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult getStudent(int acid, int sec_id, int cid)
        {           
            try
            {               
                var ans = ms.getStudentByIDs(sec_id, cid, acid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult MedicalForm()
        {
            return View();
        } 
 
        public JsonResult AddMedicalDetails(MedicalViewModel mvm) 
        {
            try
            {               
                if (ModelState.IsValid)
                {                    
                    int RegId =Convert.ToInt16(mvm.StudentRegisterId);
                    MedicalServices ms = new MedicalServices();
                    var student = ms.getMedicalStudentInfo(RegId);
                    int wgt = Convert.ToInt16(mvm.Weight);
                    int hgt = Convert.ToInt16(mvm.Height);
                    int YearId = Convert.ToInt16(mvm.Acid);

                    var year = acservice.getCurrentAcademicYearId(DateTimeByZone.getCurrentDate());
                    if (year != null)
                    {
                        Session["CurrentAcYearId"] = year.AcademicYearId;
                    }
                    else
                    {
                        Session["CurrentAcYearId"] = "";
                    }

                    int CurrentYearId;
                    if (Session["CurrentAcYearId"] != "")
                    {
                        CurrentYearId = Convert.ToInt16(Session["CurrentAcYearId"]);
                    }
                    else
                    {
                        CurrentYearId = 0;
                    }
                    if (CurrentYearId == YearId)
                    {
                        if (student != null)
                        {
                            ms.UpdateMedicalInformation(RegId, mvm.BloodGrp, wgt, hgt, mvm.Concerns_Status, mvm.Concerns_Desc, mvm.Allergies_Status, mvm.Allegries_Desc, mvm.Vision_Status, mvm.Vision_Desc, mvm.Glasses_Status, mvm.Glasses_Desc, mvm.Hearing_Status, mvm.Hearing_Desc, mvm.Speech_Status, mvm.Speech_Desc, mvm.Emergency_Status, mvm.Emergency_Desc, mvm.Broken_Status, mvm.Broken_Desc, mvm.Injuries_Status, mvm.Injuries_Desc, mvm.Problems_Status, mvm.Problems_Desc, mvm.Dental_Status, mvm.Dental_Desc, mvm.Chest_Status, mvm.Chest_Desc, mvm.Heart_Status, mvm.Heart_Desc, mvm.Highbp_Status, mvm.Highbp_Desc, mvm.Lowbp_Status, mvm.Lowbp_Desc, mvm.Bleeding_Status, mvm.Bleeding_Desc, mvm.Breathing_Status, mvm.Breathing_Desc, mvm.Asthma_Status, mvm.Asthma_Desc, mvm.Seizure_Status, mvm.Seizure_Desc);
                            string Message = ResourceCache.Localize("SuccessfullyUpdatedMedicalInformation");
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdateStudentMedicalInformation"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            ms.AddMedicalInformation(RegId, mvm.BloodGrp, wgt, hgt, mvm.Concerns_Status, mvm.Concerns_Desc, mvm.Allergies_Status, mvm.Allegries_Desc, mvm.Vision_Status, mvm.Vision_Desc, mvm.Glasses_Status, mvm.Glasses_Desc, mvm.Hearing_Status, mvm.Hearing_Desc, mvm.Speech_Status, mvm.Speech_Desc, mvm.Emergency_Status, mvm.Emergency_Desc, mvm.Broken_Status, mvm.Broken_Desc, mvm.Injuries_Status, mvm.Injuries_Desc, mvm.Problems_Status, mvm.Problems_Desc, mvm.Dental_Status, mvm.Dental_Desc, mvm.Chest_Status, mvm.Chest_Desc, mvm.Heart_Status, mvm.Heart_Desc, mvm.Highbp_Status, mvm.Highbp_Desc, mvm.Lowbp_Status, mvm.Lowbp_Desc, mvm.Bleeding_Status, mvm.Bleeding_Desc, mvm.Breathing_Status, mvm.Breathing_Desc, mvm.Asthma_Status, mvm.Asthma_Desc, mvm.Seizure_Status, mvm.Seizure_Desc);
                            string Message = ResourceCache.Localize("SuccessfullyAddedMedicalInformation");
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddStudentMedicalInformation"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                   }
                    else
                    {
                        String ErrorMessage = ResourceCache.Localize("PleaseSelectCurrentAcademicYear");          

                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }                                       
                }
                else
                {
                    String ErrorMessage = ResourceCache.Localize("PleaseFillAllMandatoryFields");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Check(int RegId)
        {
            try
            {              
                var student = ms.getMedicalStudentInfo(RegId);
                if (student != null)
                {
                    return Json(new { student = student }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var nostudent = ResourceCache.Localize("No_student_available_for_the_selected_class");
                    return Json(new { nostudent = nostudent }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        } 

    }
}
