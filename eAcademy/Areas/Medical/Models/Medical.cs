﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Medical.Models
{
    public class Medical
    {
        public int MedicalInfoId { get; set; }

        public int StudentRegisterId { get; set; }

        public DateTime Date  { get; set; }

        public string BloodGrp { get; set; }

        public Int16 Weight { get; set; }
        public Int16 Height { get; set; }

        public string Concerns_Status { get; set; }
        public string Concerns_Desc { get; set; }

        public string Allergies_Status { get; set; }
        public string Allegries_Desc { get; set; }

        public string Vision_Status { get; set; }
        public string Vision_Desc { get; set; }

        public string Glasses_Status { get; set; }
        public string Glasses_Desc { get; set; }

        public string Hearing_Status { get; set; }
        public string Hearing_Desc { get; set; }

        public string Speech_Status { get; set; }
        public string Speech_Desc { get; set; }

        public string Emergency_Status { get; set; }
        public string Emergency_Desc { get; set; }

        public string Broken_Status { get; set; }
        public string Broken_Desc { get; set; }

        public string Injuries_Status { get; set; }
        public string Injuries_Desc { get; set; }

        public string Problems_Status { get; set; }
        public string Problems_Desc { get; set; }

        public string Dental_Status { get; set; }
        public string Dental_Desc { get; set; }

        public string Chest_Status { get; set; }
        public string Chest_Desc { get; set; }

        public string Heart_Status { get; set; }
        public string Heart_Desc { get; set; }

        public string Highbp_Status { get; set; }
        public string Highbp_Desc { get; set; }

        public string Lowbp_Status { get; set; }
        public string Lowbp_Desc { get; set; }

        public string Bleeding_Status { get; set; }
        public string Bleeding_Desc { get; set; }

        public string Breathing_Status { get; set; }
        public string Breathing_Desc { get; set; }

        public string Asthma_Status { get; set; }
        public string Asthma_Desc { get; set; }

        public string Seizure_Status { get; set; }
        public string Seizure_Desc { get; set; }

 
    }
}