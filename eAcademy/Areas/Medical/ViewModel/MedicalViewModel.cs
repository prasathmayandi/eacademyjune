﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Medical.ViewModel
{
    
    public class MedicalViewModel
    {
        public int Acid { get; set; }
        public int? StudentRegisterId { get; set; }
        public int? Weight { get; set; }
        public int? Height { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                         ErrorMessageResourceName = "Pleaseselectbloodgroup")]
        public string BloodGrp { get; set; }

        public string Concerns_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources), ErrorMessageResourceName = "Anyhealthconcernscommentsmaximumallows200characters")]
        public string Concerns_Desc { get; set; }

        public string Allergies_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Anyotherallergiescommentsmaximumallows200characters")]
        public string Allegries_Desc { get; set; }

        public string Vision_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Anyproblemswithvisioncommentsmaximumallows200characters")]
        public string Vision_Desc { get; set; }

        public string Glasses_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Usescontactsorglassescommentsmaximumallows200characters")]
        public string Glasses_Desc { get; set; }

        public string Hearing_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Anyproblemshearingcommentsmaximumallows200characters")]
        public string Hearing_Desc { get; set; }

        public string Speech_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Anyproblemswithspeechcommentsmaximumallows200characters")]
        public string Speech_Desc { get; set; }

        public string Emergency_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "HospitalizationorEmergencyRoomvisitcommentsmaximumallows200characters")]
        public string Emergency_Desc { get; set; }

        public string Broken_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Anybrokenbonesordislocationcommentsmaximumallows200characters")]
        public string Broken_Desc { get; set; }

        public string Injuries_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Anymuscleorjointinjuriescommentsmaximumallows200characters")]
        public string Injuries_Desc { get; set; }

        public string Problems_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Problemsrunningcommentsmaximumallows200characters")]
        public string Problems_Desc { get; set; }

        public string Dental_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Dentalbracescapsorbridgecommentsmaximumallows200characters")]
        public string Dental_Desc { get; set; }

        public string Chest_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Chestpaincommentsmaximumallows200characters")]
        public string Chest_Desc { get; set; }

        public string Heart_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Heartproblemscommentsmaximumallows200characters")]
        public string Heart_Desc { get; set; }

        public string Highbp_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Highbloodpressurecommentsmaximumallows200characters")]
        public string Highbp_Desc { get; set; }

        public string Lowbp_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Lowbloodpressurecommentsmaximumallows200characters")]
        public string Lowbp_Desc { get; set; }

        public string Bleeding_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Bleedingmorethanexpectedcommentsmaximumallows200characters")]
        public string Bleeding_Desc { get; set; }

        public string Breathing_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Problemsbreathingorcoughingcommentsmaximumallows200characters")]
        public string Breathing_Desc { get; set; }

        public string Asthma_Status { get; set; }

        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Asthmatreatmentcommentsmaximumallows200characters")]
        public string Asthma_Desc { get; set; }

        public string Seizure_Status { get; set; }
        [StringLength(200, ErrorMessageResourceType = typeof(eAcademy.Areas.Medical.Resources.MedicalResources),
                 ErrorMessageResourceName = "Seizuretreatmentcommentsmaximumallows200characters")]
        public string Seizure_Desc { get; set; }



















        //public int Acid { get; set; }

        //public int? StudentRegisterId { get; set; }
        //public string BloodGrp { get; set; }


        //public int? Weight { get; set; }

        //public int? Height { get; set; }

        //public string Concerns_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Any health concerns  comments maximum allows 200 characters")]
        //public string Concerns_Desc { get; set; }

        //public string Allergies_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Any other allergies   comments maximum allows 200 characters")]
        //public string Allegries_Desc { get; set; }

        //public string Vision_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Any problems with vision  comments maximum allows 200 characters")]
        //public string Vision_Desc { get; set; }

        //public string Glasses_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Uses contacts or glasses  comments maximum allows 200 characters")]
        //public string Glasses_Desc { get; set; }

        //public string Hearing_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Any problems hearing  comments maximum allows 200 characters")]
        //public string Hearing_Desc { get; set; }

        //public string Speech_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Any problems with speech  comments maximum allows 200 characters")]
        //public string Speech_Desc { get; set; }

        //public string Emergency_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Hospitalization or Emergency Room visit  comments maximum allows 200 characters")]
        //public string Emergency_Desc { get; set; }

        //public string Broken_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Any broken bones or dislocation  comments maximum allows 200 characters")]
        //public string Broken_Desc { get; set; }

        //public string Injuries_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Any muscle or joint injuries  comments maximum allows 200 characters")]
        //public string Injuries_Desc { get; set; }

        //public string Problems_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Problems running  comments maximum allows 200 characters")]
        //public string Problems_Desc { get; set; }

        //public string Dental_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Dental braces, caps, or bridges  comments maximum allows 200 characters")]
        //public string Dental_Desc { get; set; }

        //public string Chest_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Chest pain  comments maximum allows 200 characters")]
        //public string Chest_Desc { get; set; }

        //public string Heart_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Heart problems  comments maximum allows 200 characters")]
        //public string Heart_Desc { get; set; }

        //public string Highbp_Status { get; set; }
        //[StringLength(200, ErrorMessage = "High blood pressure comments maximum allows 200 characters")]
        //public string Highbp_Desc { get; set; }

        //public string Lowbp_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Low blood pressure  comments maximum allows 200 characters")]
        //public string Lowbp_Desc { get; set; }

        //public string Bleeding_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Bleeding more than expected  comments maximum allows 200 characters")]
        //public string Bleeding_Desc { get; set; }

        //public string Breathing_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Problems breathing or coughing  comments maximum allows 200 characters")]
        //public string Breathing_Desc { get; set; }

        //public string Asthma_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Asthma treatment  comments maximum allows 200 characters")]
        //public string Asthma_Desc { get; set; }

        //public string Seizure_Status { get; set; }
        //[StringLength(200, ErrorMessage = "Seizure treatment  comments maximum allows 200 characters")]
        //public string Seizure_Desc { get; set; }
        
    }
}