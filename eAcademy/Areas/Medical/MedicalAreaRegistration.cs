﻿using System.Web.Mvc;

namespace eAcademy.Areas.Medical
{
    public class MedicalAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Medical";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Medical_default",
                "Medical/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
