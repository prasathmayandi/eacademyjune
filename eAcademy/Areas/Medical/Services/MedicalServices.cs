﻿using eAcademy.Areas.Medical.ViewModel;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Medical.Services
{
    public class MedicalServices
    {
        EacademyEntities dc = new EacademyEntities();
        public void AddMedicalInformation(int StudentRegisterId, string BloodGrp, int Weight, int Height, string Concerns_Status, string Concerns_Desc, string Allergies_Status, string Allegries_Desc, string Vision_Status, string Vision_Desc, string Glasses_Status, string Glasses_Desc, string Hearing_Status, string Hearing_Desc, string Speech_Status, string Speech_Desc, string Emergency_Status, string Emergency_Desc, string Broken_Status, string Broken_Desc, string Injuries_Status, string Injuries_Desc, string Problems_Status, string Problems_Desc, string Dental_Status, string Dental_Desc, string Chest_Status, string Chest_Desc, string Heart_Status, string Heart_Desc, string Highbp_Status, string Highbp_Desc, string Lowbp_Status, string Lowbp_Desc, string Bleeding_Status, string Bleeding_Desc, string Breathing_Status, string Breathing_Desc, string Asthma_Status, string Asthma_Desc, string Seizure_Status, string Seizure_Desc)
        {
            DateTime date = DateTimeByZone.getCurrentDate();
            MedicalInfo mi = new MedicalInfo()
            {
                StudentRegisterId = StudentRegisterId,
                Date = date,
                BloodGrp=BloodGrp,
                Weight=Weight,
                Height=Height,
                Concerns_Status=Concerns_Status,
                Concerns_Desc=Concerns_Desc,
                Allergies_Status = Allergies_Status,
                Allegries_Desc=Allegries_Desc,
                Vision_Status=Vision_Status,
                Vision_Desc=Vision_Desc,
                Glasses_Status=Glasses_Status,
                Glasses_Desc=Glasses_Desc,
                Hearing_Status=Hearing_Status,
                Hearing_Desc=Hearing_Desc,
                Speech_Status=Speech_Status,
                Speech_Desc=Speech_Desc,
                Emergency_Status=Emergency_Status,
                Emergency_Desc=Emergency_Desc,
                Broken_Status=Broken_Status,
                Broken_Desc=Broken_Desc,
                Injuries_Status=Injuries_Status,
                Injuries_Desc=Injuries_Desc,
                Problems_Status=Problems_Status,
                Problems_Desc=Problems_Desc,
                Dental_Status=Dental_Status,
                Dental_Desc=Dental_Desc,
                Chest_Status=Chest_Status,
                Chest_Desc=Chest_Desc,
                Heart_Status=Heart_Status,
                Heart_Desc=Heart_Desc,
                Highbp_Status=Highbp_Status,
                Highbp_Desc=Highbp_Desc,
                Lowbp_Status=Lowbp_Status,
                Lowbp_Desc=Lowbp_Desc,
                Bleeding_Status=Bleeding_Status,
                Bleeding_Desc=Bleeding_Desc,
                Breathing_Status=Breathing_Status,
                Breathing_Desc=Breathing_Desc,
                Asthma_Status=Asthma_Status,
                Asthma_Desc=Asthma_Desc,
                Seizure_Status=Seizure_Status,
                Seizure_Desc=Seizure_Desc
           };
            dc.MedicalInfoes.Add(mi);
            dc.SaveChanges();
        }
        DateTime date = DateTimeByZone.getCurrentDate();
        public void UpdateMedicalInformation(int StudentRegisterId, string BloodGrp, int Weight, int Height, string Concerns_Status, string Concerns_Desc, string Allergies_Status, string Allegries_Desc, string Vision_Status, string Vision_Desc, string Glasses_Status, string Glasses_Desc, string Hearing_Status, string Hearing_Desc, string Speech_Status, string Speech_Desc, string Emergency_Status, string Emergency_Desc, string Broken_Status, string Broken_Desc, string Injuries_Status, string Injuries_Desc, string Problems_Status, string Problems_Desc, string Dental_Status, string Dental_Desc, string Chest_Status, string Chest_Desc, string Heart_Status, string Heart_Desc, string Highbp_Status, string Highbp_Desc, string Lowbp_Status, string Lowbp_Desc, string Bleeding_Status, string Bleeding_Desc, string Breathing_Status, string Breathing_Desc, string Asthma_Status, string Asthma_Desc, string Seizure_Status, string Seizure_Desc)
        {
            var getRow = (from a in dc.MedicalInfoes where a.StudentRegisterId == StudentRegisterId select a).FirstOrDefault();          
           
                getRow.StudentRegisterId = StudentRegisterId;
                getRow.Date = date;
                getRow.BloodGrp = BloodGrp;
                getRow.Weight = Weight;
                getRow.Height = Height;
                getRow.Concerns_Status = Concerns_Status;
                getRow.Concerns_Desc = Concerns_Desc;
                getRow.Allergies_Status = Allergies_Status;
                getRow.Allegries_Desc = Allegries_Desc;
                getRow.Vision_Status = Vision_Status;
                getRow.Vision_Desc = Vision_Desc;
                getRow.Glasses_Status = Glasses_Status;
                getRow.Glasses_Desc = Glasses_Desc;
                getRow.Hearing_Status = Hearing_Status;
                getRow.Hearing_Desc = Hearing_Desc;
                getRow.Speech_Status = Speech_Status;
                getRow.Speech_Desc = Speech_Desc;
                getRow.Emergency_Status = Emergency_Status;
                getRow.Emergency_Desc = Emergency_Desc;
                getRow.Broken_Status = Broken_Status;
                getRow.Broken_Desc = Broken_Desc;
                getRow.Injuries_Status = Injuries_Status;
                getRow.Injuries_Desc = Injuries_Desc;
                getRow.Problems_Status = Problems_Status;
                getRow.Problems_Desc = Problems_Desc;
                getRow.Dental_Status = Dental_Status;
                getRow.Dental_Desc = Dental_Desc;
                getRow.Chest_Status = Chest_Status;
                getRow.Chest_Desc = Chest_Desc;
                getRow.Heart_Status = Heart_Status;
                getRow.Heart_Desc = Heart_Desc;
                getRow.Highbp_Status = Highbp_Status;
                getRow.Highbp_Desc = Highbp_Desc;
                getRow.Lowbp_Status = Lowbp_Status;
                getRow.Lowbp_Desc = Lowbp_Desc;
                getRow.Bleeding_Status = Bleeding_Status;
                getRow.Bleeding_Desc = Bleeding_Desc;
                getRow.Breathing_Status = Breathing_Status;
                getRow.Breathing_Desc = Breathing_Desc;
                getRow.Asthma_Status = Asthma_Status;
                getRow.Asthma_Desc = Asthma_Desc;
                getRow.Seizure_Status = Seizure_Status;
                getRow.Seizure_Desc = Seizure_Desc;                       
            dc.SaveChanges();
        }
        public MedicalViewModel getMedicalStudentInfo(int id)
        {
            var getRow = (from a in dc.MedicalInfoes where a.StudentRegisterId == id select new MedicalViewModel {                 
                BloodGrp =a.BloodGrp,
                Weight = a.Weight,
                Height = a.Height,
                Concerns_Status = a.Concerns_Status,
                Concerns_Desc = a.Concerns_Desc,    
                Allergies_Status =a.Allergies_Status,
                Allegries_Desc = a.Allegries_Desc,
                Vision_Status = a.Vision_Status,
                Vision_Desc = a.Vision_Desc,
                Glasses_Status = a.Glasses_Status,
                Glasses_Desc = a.Glasses_Desc,
                Hearing_Status =a.Hearing_Status,
                Hearing_Desc = a.Hearing_Desc,
                Speech_Status = a.Speech_Status,
                Speech_Desc = a.Speech_Desc,
                Emergency_Status = a.Emergency_Status,
                Emergency_Desc = a.Emergency_Desc,
                Broken_Status = a.Broken_Status,
                Broken_Desc = a.Broken_Desc,
                Injuries_Status = a.Injuries_Status,
                Injuries_Desc = a.Injuries_Desc,
                Problems_Status = a.Problems_Status,
                Problems_Desc = a.Problems_Desc,
                Dental_Status = a.Dental_Status,
                Dental_Desc = a.Dental_Desc,
                Chest_Status = a.Chest_Status,
                Chest_Desc = a.Chest_Desc,
                Heart_Status = a.Heart_Status,
                Heart_Desc = a.Heart_Desc,
                Highbp_Status = a.Highbp_Status,
                Highbp_Desc = a.Highbp_Desc,
                Lowbp_Status = a.Lowbp_Status,
                Lowbp_Desc = a.Lowbp_Desc,
                Bleeding_Status = a.Bleeding_Status,
                Bleeding_Desc = a.Bleeding_Desc,
                Breathing_Status = a.Breathing_Status,
                Breathing_Desc = a.Breathing_Desc,
                Asthma_Status = a.Asthma_Status,
                Asthma_Desc = a.Asthma_Desc,
                Seizure_Status = a.Seizure_Status,
                Seizure_Desc = a.Seizure_Desc           
            }).FirstOrDefault();
            return getRow;
        }
 
        public List<ClassSection> Classes()
        {
            var ans = (from t1 in dc.Classes
                       where t1.Status == true
                       select new ClassSection { ClassId = t1.ClassId, ClassType = t1.ClassType }).OrderBy(q => q.ClassId).ToList();
            return ans;
        }
        public IEnumerable<object> getStudentByIDs(int sec_id, int cid, int acid)
        {
            var ans = from t1 in dc.AssignClassToStudents
                      from t2 in dc.Students
                      where t1.StudentRegisterId == t2.StudentRegisterId && t1.SectionId == sec_id && t1.ClassId == cid && t1.AcademicYearId == acid && t2.StudentStatus == true
                      select new { sname = t2.FirstName + " " + t2.LastName, sid = t2.StudentRegisterId };
            return ans;
        }
        public List<ClassSection> getSectionByClassId(int cid)
        {
            var ans1 = (from t1 in dc.Classes
                        from t2 in dc.Sections

                        where t1.ClassId == t2.ClassId && t2.ClassId == cid
                        select new ClassSection { ClassId = t1.ClassId, ClassType = t1.ClassType, Section = t2.SectionName, SectionId = t2.SectionId }).ToList();
            return ans1;
        }
        public List<ClassSection> getSectionByClassId(int acid, int cid)
        {
            var ans1 = (from a in dc.Sections
                        where a.ClassId == cid && !(from b in dc.SectionStrengths
                                                    where b.ClassId == a.ClassId &&
                                                   b.SectionId == a.SectionId &&
                                                    b.ClassId == cid
                                                    select b.SectionId)
                                  .Contains(a.SectionId)
                        select new ClassSection { Section = a.SectionName, SectionId = a.SectionId }).ToList();
            return ans1;
        }
        public List<AcademicYear> AcademicYear()
        {
            var ayear = dc.AcademicYears.Select(q => q).ToList();
            return ayear;
        }
 

    }
}