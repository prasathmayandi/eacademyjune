﻿using eAcademy.Services;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Medical.Resources
{
    public class MedicalResources
    {
        public static string Pleaseselectbloodgroup
        {
            get
            {
                return ResourceCache.Localize("Pleaseselectbloodgroup");
            }
        }
        public static string Anyhealthconcernscommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Anyhealthconcernscommentsmaximumallows200characters");
            }
        }
        public static string Anyotherallergiescommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Anyotherallergiescommentsmaximumallows200characters");
            }
        }

        public static string Anyproblemswithvisioncommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Anyproblemswithvisioncommentsmaximumallows200characters");
            }
        }
        public static string Usescontactsorglassescommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Usescontactsorglassescommentsmaximumallows200characters");
            }
        }
        public static string Anyproblemshearingcommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Anyproblemshearingcommentsmaximumallows200characters");
            }
        }
        public static string Anyproblemswithspeechcommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Anyproblemswithspeechcommentsmaximumallows200characters");
            }
        }
        public static string HospitalizationorEmergencyRoomvisitcommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("HospitalizationorEmergencyRoomvisitcommentsmaximumallows200characters");
            }
        }
        public static string Anybrokenbonesordislocationcommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Anybrokenbonesordislocationcommentsmaximumallows200characters");
            }
        }
        public static string Anymuscleorjointinjuriescommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Anymuscleorjointinjuriescommentsmaximumallows200characters");
            }
        }
        public static string Problemsrunningcommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Problemsrunningcommentsmaximumallows200characters");
            }
        }
        public static string Dentalbracescapsorbridgecommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Dentalbracescapsorbridgecommentsmaximumallows200characters");
            }
        }
        public static string Chestpaincommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Chestpaincommentsmaximumallows200characters");
            }
        }
        public static string Heartproblemscommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Heartproblemscommentsmaximumallows200characters");
            }
        }
        public static string Highbloodpressurecommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Highbloodpressurecommentsmaximumallows200characters");
            }
        }
        public static string Lowbloodpressurecommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Lowbloodpressurecommentsmaximumallows200characters");
            }
        }
        public static string Bleedingmorethanexpectedcommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Bleedingmorethanexpectedcommentsmaximumallows200characters");
            }
        }
        public static string Problemsbreathingorcoughingcommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Problemsbreathingorcoughingcommentsmaximumallows200characters");
            }
        }
        public static string Asthmatreatmentcommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Asthmatreatmentcommentsmaximumallows200characters");
            }
        }
        public static string Seizuretreatmentcommentsmaximumallows200characters
        {
            get
            {
                return ResourceCache.Localize("Seizuretreatmentcommentsmaximumallows200characters");
            }
        }
    }
}