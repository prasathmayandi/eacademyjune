﻿using eAcademy.Areas.FinancialAccounting.LocalResource;
using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Areas.FinancialAccounting.ViewModel
{
    public class VM_SalaryIncrement
    {

        public int SalaryConfigId { get; set; }
        public int EmployeeRegisterId { get; set; }


        public string Etype { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
               ErrorMessageResourceName = "Pleaseenterbasicsalary")]
        public Decimal BasicSalary { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
               ErrorMessageResourceName = "Pleaseentergrossearning")]
        public Decimal GrossEarning { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
               ErrorMessageResourceName = "Pleaseentergrossdeduction")]
        public Decimal GrossDeduction { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
               ErrorMessageResourceName = "Pleaseenteroldnetsalary")]
        public Decimal NetSalary { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
               ErrorMessageResourceName = "Pleaseenterincrementsalary")]
        public Decimal IncrementSalary { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
               ErrorMessageResourceName = "Pleaseenternewnetsalary")]
        public Decimal NewNetSalary { get; set; }
        public int SalaryIncrementId { get; set; }













    //    public int SalaryConfigId { get; set; }
    //    public int EmployeeRegisterId { get; set; }
    //    public string Etype { get; set; }
    //    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Pleaseenterbasicsalary")]
    //    public Decimal BasicSalary { get; set; }
    //    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Pleaseentergrossearning")]
    //    public Decimal GrossEarning { get; set; }
    //    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Pleaseentergrossdeduction")]
    //    public Decimal GrossDeduction { get; set; }
    //    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Pleaseenternetsalary")]
    //    public Decimal NetSalary { get; set; }
    //    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Pleaseenterincrementsalary")]
    //    public Decimal IncrementSalary { get; set; }
    //    [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Pleaseenternewnetsalary")]
    //    public Decimal NewNetSalary { get; set; }
    //    public int SalaryIncrementId { get; set; }
    }
}