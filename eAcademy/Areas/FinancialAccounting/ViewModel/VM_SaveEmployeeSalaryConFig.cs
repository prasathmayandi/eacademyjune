﻿using eAcademy.Areas.FinancialAccounting.LocalResource;
using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Areas.FinancialAccounting.ViewModel
{
    public class VM_SaveEmployeeSalaryConFig
    {
        public int EmployeeRegisterId { get; set; }
        public string Etype { get; set; }
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
                  ErrorMessageResourceName = "Pleaseenterbasicsalary")]
        public Decimal BasicSalary { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
                 ErrorMessageResourceName = "Pleaseenterdearnessallowanceminmumvalue0")]
        public Decimal DearnessAllowance { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
                 ErrorMessageResourceName = "Pleaseenterhouserentallowanceminmumvalue0")]
        public Decimal HouseRenAllowance { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
                 ErrorMessageResourceName = "Pleaseentermedicalallowanceminmumvalue0")]
        public Decimal MedicalAllowance { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
                 ErrorMessageResourceName = "Pleaseenterotherallowanceminmumvalue0")]
        public Decimal OtherAllowance { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
                 ErrorMessageResourceName = "Pleaseentergrossearning")]
        public Decimal GrossEarning { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
                 ErrorMessageResourceName = "Pleaseenterprovidentfundminmumvalue0")]
        public Decimal ProvidentFund { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
                 ErrorMessageResourceName = "Pleaseenteresiminmumvalue0")]
        public Decimal ESI { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
                 ErrorMessageResourceName = "Pleaseentergrossdeduction")]
        public Decimal GrossDeduction { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
                 ErrorMessageResourceName = "Pleaseenternetsalary")]
        public Decimal NetSalary { get; set; }




        //public int EmployeeRegisterId { get; set; }
        //public string Etype { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Pleaseenterbasicsalary")]
        //public Decimal BasicSalary { get; set; }
        //public Decimal DearnessAllowance { get; set; }
        //public Decimal HouseRenAllowance { get; set; }
        //public Decimal MedicalAllowance { get; set; }
        //public Decimal OtherAllowance { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Pleaseentergrossearning")]
        //public Decimal GrossEarning { get; set; }
        //public Decimal ProvidentFund { get; set; }
        //public Decimal ESI { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Pleaseentergrossdeduction")]
        //public Decimal GrossDeduction { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Pleaseenternetsalary")]
        //public Decimal NetSalary { get; set; }

    }
}