﻿using eAcademy.Areas.FinancialAccounting.LocalResource;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Areas.FinancialAccounting.ViewModel
{
    public class Vm_EmployeeMonthSalary
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
                ErrorMessageResourceName = "PleaseSelectEmployeeCategory")]
        public int EmployeeCategoryId { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
                ErrorMessageResourceName = "PleaseSelectMonth")]
        public int monthId { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.FinancialAccounting.Resources.FinancialAccountResources),
                ErrorMessageResourceName = "PleaseenterYear")]
        public int Year { get; set; }









        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Pleaseselectemployeecategory")]
        //public int EmployeeCategoryId { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Pleaseselectmonth")]
        //public int monthId { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "PleaseenterYear")]
        //public int Year { get; set; }
    }
}