﻿using System;
namespace eAcademy.Areas.FinancialAccounting.Models
{
    public class EmployeeSalaryConfigList
    {
        public int? EmployeeRegisterId { get; set; }
        public string Etype { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeType { get; set; }
        public string EmployeeDesignation { get; set; }
        public Decimal? BasicSalary { get; set; }
        public Decimal? DearnessAllowance { get; set; }
        public Decimal? HouseRentAllowance { get; set; }
        public Decimal? MedicalAllowance { get; set; }
        public Decimal? OtherAllowance { get; set; }
        public Decimal? GrossEarning { get; set; }
        public Decimal? ProvidentFund { get; set; }
        public Decimal? ESI { get; set; }
        public Decimal? GrossDeduction { get; set; }
        public Decimal? NetSalary { get; set; }
        public int EmployeeSalaryConfigId { get; set; }
        public string EmployeeSalaryConfigIdEtype { get; set; }
        public string ImgFile { get; set; }
        public Decimal? IncrementId { get; set; }
        public Decimal? IncrementSalary { get; set; }
        public Decimal? NewNetSalary { get; set; }
        public int TotalNoofWorkingDays { get; set; }
        public int WorkingDays { get; set; }
        public int NoOfDaysLeave { get; set; }
        public Decimal? ActualSalary { get; set; }
        public Decimal? DeductionAmount { get; set; }
    }
}