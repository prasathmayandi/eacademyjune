﻿using eAcademy.Areas.FinancialAccounting.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Linq;
namespace eAcademy.Areas.FinancialAccounting.Services
{
    public class EmployeeSalaryincrementServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public int AddSalaryIncrement(int EmployeeRegisterId, string Etype, Decimal BS, Decimal GE, Decimal GD, Decimal OldNS, Decimal Salaryincrement, Decimal NewNs, int adminid)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            if (Etype == "Tech")
            {
                EmployeeSalaryIncrement incement = new EmployeeSalaryIncrement()
                {
                    EmployeeRegisterid = EmployeeRegisterId,
                    LastbasicSalary = BS,
                    LastGrossEarning = GE,
                    LastGrossDeduction = GD,
                    LastNetSalary = OldNS,
                    IncrementAmount = Salaryincrement,
                    Entryby = adminid,
                    Entrydate = date
                };
                dc.EmployeeSalaryIncrements.Add(incement);
                dc.SaveChanges();
                return incement.EmployeeSalaryIncrementId;
            }
            else if (Etype == "Other")
            {
                EmployeeSalaryIncrement incement = new EmployeeSalaryIncrement()
                {
                    OtherEmployeeRegisterid = EmployeeRegisterId,
                    LastbasicSalary = BS,
                    LastGrossEarning = GE,
                    LastGrossDeduction = GD,
                    LastNetSalary = OldNS,
                    IncrementAmount = Salaryincrement,
                    Entryby = adminid,
                    Entrydate = date
                };
                dc.EmployeeSalaryIncrements.Add(incement);
                dc.SaveChanges();
                return incement.EmployeeSalaryIncrementId;
            }
            else
            {
                return 0;
            }
        }
        public void UpdateSalaryIncrement(int SalaryIncrementId, Decimal Salaryincrement, int adminid)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var get = dc.EmployeeSalaryIncrements.Where(q => q.EmployeeSalaryIncrementId == SalaryIncrementId).FirstOrDefault();
            if (get != null)
            {
                get.IncrementAmount = Salaryincrement;
                get.Entryby = adminid;
                get.Entrydate = date;
                dc.SaveChanges();
            }
        }
        public EmployeeSalaryConfigList getTechSalaryincrementDetails(int incrementId)
        {
            var ans = (from a in dc.EmployeeSalaryIncrements
                       from b in dc.EmployeeSalaryConfigurations
                       from c in dc.TechEmployees
                       from d in dc.EmployeeTypes
                       from e in dc.EmployeeDesignations
                       where a.EmployeeSalaryIncrementId == incrementId && b.EmployeeRegisterId == a.EmployeeRegisterid && b.Status == true && c.EmployeeRegisterId == a.EmployeeRegisterid && c.EmployeeStatus == true && d.EmployeeTypeId == c.EmployeeTypeId && e.EmployeeDesignationId == c.EmployeeDesignationId
                       select new EmployeeSalaryConfigList
                       {
                           IncrementId = a.EmployeeSalaryIncrementId,
                           BasicSalary = a.LastbasicSalary,
                           GrossEarning = a.LastGrossEarning,
                           GrossDeduction = a.LastGrossDeduction,
                           NetSalary = a.LastNetSalary,
                           IncrementSalary = a.IncrementAmount,
                           EmployeeSalaryConfigId = b.EmployeeSalaryConfigurationId,
                           EmployeeRegisterId = a.EmployeeRegisterid,
                           Etype = "Tech",
                           EmployeeName = c.EmployeeName + " " + c.LastName,
                           EmployeeId = c.EmployeeId,
                           EmployeeType = d.EmployeeTypes,
                           EmployeeDesignation = e.Designation,
                           ImgFile = c.ImgFile
                       }).FirstOrDefault();
            return ans;
        }
        public EmployeeSalaryConfigList getOtherSalaryincrementDetails(int incrementId)
        {
            var ans = (from a in dc.EmployeeSalaryIncrements
                       from b in dc.EmployeeSalaryConfigurations
                       from c in dc.OtherEmployees
                       from d in dc.EmployeeTypes
                       from e in dc.EmployeeDesignations
                       where a.EmployeeSalaryIncrementId == incrementId && b.OtherEmployeeregisterId == a.OtherEmployeeRegisterid && b.Status == true && c.EmployeeRegisterId == a.OtherEmployeeRegisterid && c.EmployeeStatus == true && d.EmployeeTypeId == c.EmployeeTypeId && e.EmployeeDesignationId == c.EmployeeDesignationId
                       select new EmployeeSalaryConfigList
                       {
                           IncrementId = a.EmployeeSalaryIncrementId,
                           BasicSalary = a.LastbasicSalary,
                           GrossEarning = a.LastGrossEarning,
                           GrossDeduction = a.LastGrossDeduction,
                           NetSalary = a.LastNetSalary,
                           IncrementSalary = a.IncrementAmount,
                           EmployeeSalaryConfigId = b.EmployeeSalaryConfigurationId,
                           EmployeeRegisterId = a.OtherEmployeeRegisterid,
                           Etype = "Other",
                           EmployeeName = c.EmployeeName + " " + c.Lastname,
                           EmployeeId = c.EmployeeId,
                           EmployeeType = d.EmployeeTypes,
                           EmployeeDesignation = e.Designation,
                           ImgFile = c.ImgFile
                       }).FirstOrDefault();
            return ans;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}