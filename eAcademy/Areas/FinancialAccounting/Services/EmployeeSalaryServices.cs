﻿using eAcademy.Areas.FinancialAccounting.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
namespace eAcademy.Areas.FinancialAccounting.Services
{
    public class EmployeeSalaryServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public EmployeeSalaryConfiguration checkSalarygenerated(int EmployeetypeId)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            if (EmployeetypeId < 4)
            {
                var ans = (from a in dc.EmployeeSalaries
                           from b in dc.EmployeeSalaryConfigurations
                           where b.EntryDate.Value.Month == date.Month && b.EntryDate.Value.Year == date.Year && a.EmployeeTypeId == EmployeetypeId && b.EmployeeRegisterId != a.EmployeeRegisterId && b.EntryDate.Value.Month ==a.EntryDate.Value.Month && b.EntryDate.Value.Year==a.EntryDate.Value.Year
                           select b).FirstOrDefault();
                return ans;
            }
            else
            {
                var ans = (from a in dc.EmployeeSalaries
                           from b in dc.EmployeeSalaryConfigurations
                           where b.EntryDate.Value.Month == date.Month && b.EntryDate.Value.Year == date.Year && a.EmployeeTypeId == EmployeetypeId && b.OtherEmployeeregisterId != a.OtherEmployeeregisterId && b.EntryDate.Value.Month == a.EntryDate.Value.Month && b.EntryDate.Value.Year == a.EntryDate.Value.Year
                           select b).FirstOrDefault();
                return ans;
            }
        }
        public List<EmployeeSalaryConfigList> GetEmployeeSalaryDetails(int EmployeeTypeid, int MonthId, int year)
        {
            List<EmployeeSalaryConfigList> Salarylist = new List<EmployeeSalaryConfigList>();
            if (EmployeeTypeid < 4)
            {
                var ans = (from a in dc.EmployeeSalaryConfigurations
                           from b in dc.TechEmployees
                           from c in dc.EmployeeTypes
                           from d in dc.EmployeeDesignations
                          // from e in dc.EmployeeSalaries
                           where c.EmployeeTypeId == EmployeeTypeid && b.EmployeeTypeId == c.EmployeeTypeId && d.EmployeeDesignationId == b.EmployeeDesignationId && b.EmployeeRegisterId == a.EmployeeRegisterId && a.Status == true && b.EmployeeStatus == true //&& e.EmployeeRegisterId != a.EmployeeRegisterId
                           select new EmployeeSalaryConfigList
                           {
                               EmployeeRegisterId = a.EmployeeRegisterId,
                               EmployeeId = b.EmployeeId,
                               EmployeeName = b.EmployeeName + " " + b.LastName,
                               EmployeeType = c.EmployeeTypes,
                               EmployeeDesignation = d.Designation,
                               BasicSalary = a.BasicSalary,
                               ActualSalary = a.NetSalary,
                           }
                         ).Distinct().ToList().Select(a => new EmployeeSalaryConfigList
                         {
                             EmployeeRegisterId = a.EmployeeRegisterId,
                             EmployeeId = a.EmployeeId,
                             EmployeeName = a.EmployeeName,
                             EmployeeType = a.EmployeeType,
                             EmployeeDesignation = a.EmployeeDesignation,
                             BasicSalary = a.BasicSalary,
                             ActualSalary = a.ActualSalary,
                             TotalNoofWorkingDays = GetWorkingDays(),
                             WorkingDays = TechFacultyWorkingdays(Convert.ToInt16(a.EmployeeRegisterId), MonthId, year),
                             NoOfDaysLeave = TechFacultyleavedays(Convert.ToInt16(a.EmployeeRegisterId), MonthId, year),
                             DeductionAmount = Math.Round(CalCulateDeductionAmount(Convert.ToDecimal(a.ActualSalary), MonthId, year, Convert.ToInt16(a.EmployeeRegisterId), "Tech"), 2, MidpointRounding.AwayFromZero),
                             NetSalary = Math.Round(Convert.ToDecimal(a.ActualSalary) - CalCulateDeductionAmount(Convert.ToDecimal(a.ActualSalary), MonthId, year, Convert.ToInt16(a.EmployeeRegisterId), "Tech"), 2, MidpointRounding.AwayFromZero)
                         }).ToList();
                Salarylist = ans;
            }
            else
            {
                var ans = (from a in dc.EmployeeSalaryConfigurations
                           from b in dc.OtherEmployees
                           from c in dc.EmployeeTypes
                           from d in dc.EmployeeDesignations
                           from e in dc.EmployeeSalaries
                           where c.EmployeeTypeId == EmployeeTypeid && b.EmployeeTypeId == c.EmployeeTypeId && d.EmployeeDesignationId == b.EmployeeDesignationId && b.EmployeeRegisterId == a.OtherEmployeeregisterId && a.Status == true && b.EmployeeStatus == true && e.OtherEmployeeregisterId != a.OtherEmployeeregisterId
                           select new EmployeeSalaryConfigList
                           {
                               EmployeeRegisterId = a.OtherEmployeeregisterId,
                               EmployeeId = b.EmployeeId,
                               EmployeeName = b.EmployeeName + " " + b.Lastname,
                               EmployeeType = c.EmployeeTypes,
                               EmployeeDesignation = d.Designation,
                               BasicSalary = a.BasicSalary,
                               ActualSalary = a.NetSalary
                           }
                        ).Distinct().ToList().Select(a => new EmployeeSalaryConfigList
                        {
                            EmployeeRegisterId = a.EmployeeRegisterId,
                            EmployeeId = a.EmployeeId,
                            EmployeeName = a.EmployeeName,
                            EmployeeType = a.EmployeeType,
                            EmployeeDesignation = a.EmployeeDesignation,
                            BasicSalary = a.BasicSalary,
                            ActualSalary = a.ActualSalary,
                            TotalNoofWorkingDays = GetWorkingDays(),
                            WorkingDays = OtherFacultyWorkingdays(Convert.ToInt16(a.EmployeeRegisterId), MonthId, year),
                            NoOfDaysLeave = OtherFacultyleavedays(Convert.ToInt16(a.EmployeeRegisterId), MonthId, year),
                            DeductionAmount = Math.Round(CalCulateDeductionAmount(Convert.ToDecimal(a.ActualSalary), MonthId, year, Convert.ToInt16(a.EmployeeRegisterId), "Other"), 2, MidpointRounding.AwayFromZero),
                            NetSalary = Math.Round(Convert.ToDecimal(a.ActualSalary) - CalCulateDeductionAmount(Convert.ToDecimal(a.ActualSalary), MonthId, year, Convert.ToInt16(a.EmployeeRegisterId), "Other"), 2, MidpointRounding.AwayFromZero)
                        }).ToList();
                Salarylist = ans;
            }
            return Salarylist;
        }

        //public List<EmployeeSalaryConfigList> GetEmployeeSalaryDetails(int EmployeeTypeid, int MonthId, int year)
        //{
        //    List<EmployeeSalaryConfigList> Salarylist = new List<EmployeeSalaryConfigList>();
        //    //if (EmployeeTypeid < 4)
        //    //{
        //        var ans = (from a in dc.EmployeeSalaryConfigurations
        //                   from b in dc.TechEmployees
        //                   from c in dc.EmployeeTypes
        //                   from d in dc.EmployeeDesignations
        //                   //from e in dc.EmployeeSalaries
        //              where// b.EmployeeStatus == true && c.EmployeeTypeId == EmployeeTypeid && b.EmployeeRegisterId == a.EmployeeRegisterId
        //                   c.EmployeeTypeId == EmployeeTypeid && b.EmployeeTypeId == c.EmployeeTypeId && d.EmployeeDesignationId == b.EmployeeDesignationId && b.EmployeeRegisterId == a.EmployeeRegisterId && a.Status == true && b.EmployeeStatus == true 
        //                   select new EmployeeSalaryConfigList
        //                   {
        //                       EmployeeRegisterId = a.EmployeeRegisterId,
        //                       EmployeeId = b.EmployeeId,
        //                       EmployeeName = b.EmployeeName + " " + b.LastName,
        //                       EmployeeType = c.EmployeeTypes,
        //                       EmployeeDesignation = d.Designation,
        //                       BasicSalary = a.BasicSalary,
        //                       ActualSalary = a.NetSalary,
        //                   }
        //                 ).ToList();
        //   // }
        //    return ans;
        //}
                         
        //    //             .Distinct().ToList().Select(a => new EmployeeSalaryConfigList
        //    //             {
        //    //                 EmployeeRegisterId = a.EmployeeRegisterId,
        //    //                 EmployeeId = a.EmployeeId,
        //    //                 EmployeeName = a.EmployeeName,
        //    //                 EmployeeType = a.EmployeeType,
        //    //                 EmployeeDesignation = a.EmployeeDesignation,
        //    //                 BasicSalary = a.BasicSalary,
        //    //                 ActualSalary = a.ActualSalary,
        //    //                 TotalNoofWorkingDays = GetWorkingDays(),
        //    //                 WorkingDays = TechFacultyWorkingdays(Convert.ToInt16(a.EmployeeRegisterId), MonthId, year),
        //    //                 NoOfDaysLeave = TechFacultyleavedays(Convert.ToInt16(a.EmployeeRegisterId), MonthId, year),
        //    //                 DeductionAmount = Math.Round(CalCulateDeductionAmount(Convert.ToDecimal(a.ActualSalary), MonthId, year, Convert.ToInt16(a.EmployeeRegisterId), "Tech"), 2, MidpointRounding.AwayFromZero),
        //    //                 NetSalary = Math.Round(Convert.ToDecimal(a.ActualSalary) - CalCulateDeductionAmount(Convert.ToDecimal(a.ActualSalary), MonthId, year, Convert.ToInt16(a.EmployeeRegisterId), "Tech"), 2, MidpointRounding.AwayFromZero)
        //    //             }).ToList();
        //    //    Salarylist = ans;
        //    //}
        //    //else
        //    //{
        //    //    var ans = (from a in dc.EmployeeSalaryConfigurations
        //    //               from b in dc.OtherEmployees
        //    //               from c in dc.EmployeeTypes
        //    //               from d in dc.EmployeeDesignations
        //    //               from e in dc.EmployeeSalaries
        //    //               where c.EmployeeTypeId == EmployeeTypeid && b.EmployeeTypeId == c.EmployeeTypeId && d.EmployeeDesignationId == b.EmployeeDesignationId && b.EmployeeRegisterId == a.OtherEmployeeregisterId && a.Status == true && b.EmployeeStatus == true && e.OtherEmployeeregisterId != a.OtherEmployeeregisterId
        //    //               select new EmployeeSalaryConfigList
        //    //               {
        //    //                   EmployeeRegisterId = a.OtherEmployeeregisterId,
        //    //                   EmployeeId = b.EmployeeId,
        //    //                   EmployeeName = b.EmployeeName + " " + b.Lastname,
        //    //                   EmployeeType = c.EmployeeTypes,
        //    //                   EmployeeDesignation = d.Designation,
        //    //                   BasicSalary = a.BasicSalary,
        //    //                   ActualSalary = a.NetSalary
        //    //               }
        //    //            ).Distinct().ToList().Select(a => new EmployeeSalaryConfigList
        //    //            {
        //    //                EmployeeRegisterId = a.EmployeeRegisterId,
        //    //                EmployeeId = a.EmployeeId,
        //    //                EmployeeName = a.EmployeeName,
        //    //                EmployeeType = a.EmployeeType,
        //    //                EmployeeDesignation = a.EmployeeDesignation,
        //    //                BasicSalary = a.BasicSalary,
        //    //                ActualSalary = a.ActualSalary,
        //    //                TotalNoofWorkingDays = GetWorkingDays(),
        //    //                WorkingDays = OtherFacultyWorkingdays(Convert.ToInt16(a.EmployeeRegisterId), MonthId, year),
        //    //                NoOfDaysLeave = OtherFacultyleavedays(Convert.ToInt16(a.EmployeeRegisterId), MonthId, year),
        //    //                DeductionAmount = Math.Round(CalCulateDeductionAmount(Convert.ToDecimal(a.ActualSalary), MonthId, year, Convert.ToInt16(a.EmployeeRegisterId), "Other"), 2, MidpointRounding.AwayFromZero),
        //    //                NetSalary = Math.Round(Convert.ToDecimal(a.ActualSalary) - CalCulateDeductionAmount(Convert.ToDecimal(a.ActualSalary), MonthId, year, Convert.ToInt16(a.EmployeeRegisterId), "Other"), 2, MidpointRounding.AwayFromZero)
        //    //            }).ToList();
        //    //    Salarylist = ans;
        //    //}
        //    //return Salarylist;
        ////}

        public int GetWorkingDays()//int monthid,int year
        {
            var today = DateTimeByZone.getCurrentDate();
            var month = new DateTime(today.Year, today.Month, 1);
            var first = month.AddMonths(-1);
            var last = month.AddDays(-1);
            DateTime startDate = first;
            DateTime endDate = last;
            TimeSpan diff = endDate - startDate;
            int days = diff.Days;
            int j = 0;
            for (var i = 0; i <= days; i++)
            {
                var testDate = startDate.AddDays(i);
                if (testDate.DayOfWeek != DayOfWeek.Saturday && testDate.DayOfWeek != DayOfWeek.Sunday)
                {
                    j = j + 1;
                }
            }
            return j;
        }
        public int TechFacultyWorkingdays(int employeeregisterid, int monthid, int year)
        {
            int workingdays = dc.FacultyDailyAttendances.Where(q => q.CurrentDate.Value.Month == monthid && q.CurrentDate.Value.Year == year && q.AttendanceStatus == "P" && q.EmployeeRegisterId == employeeregisterid).ToList().Count;
            return workingdays;
        }
        public int TechFacultyleavedays(int employeeregisterid, int monthid, int year)
        {
            int workingdays = dc.FacultyDailyAttendances.Where(q => q.CurrentDate.Value.Month == monthid && q.CurrentDate.Value.Year == year && q.AttendanceStatus == "L" && q.EmployeeRegisterId == employeeregisterid).ToList().Count;
            return workingdays;
        }
        public int OtherFacultyWorkingdays(int employeeregisterid, int monthid, int year)
        {
            int workingdays = dc.EmployeeDailyAttendances.Where(q => q.DateOfAttendance.Value.Month == monthid && q.DateOfAttendance.Value.Year == year && q.AttendanceStatus == "P" && q.EmployeeRegisterId == employeeregisterid).ToList().Count;
            return workingdays;
        }
        public int OtherFacultyleavedays(int employeeregisterid, int monthid, int year)
        {
            int workingdays = dc.EmployeeDailyAttendances.Where(q => q.DateOfAttendance.Value.Month == monthid && q.DateOfAttendance.Value.Year == year && q.AttendanceStatus == "L" && q.EmployeeRegisterId == employeeregisterid).ToList().Count;
            return workingdays;
        }
        public Decimal CalCulateDeductionAmount(Decimal ActualSalary, int monthid, int year, int EmployeeRegisterId, string Type)
        {
            int totalnoofworkingdays = GetWorkingDays();
            int leavedays = 0;
            if (Type == "Tech")
            {
                leavedays = TechFacultyleavedays(EmployeeRegisterId, monthid, year);
            }
            else
            {
                leavedays = OtherFacultyleavedays(EmployeeRegisterId, monthid, year);
            }
            Decimal PerdaySalary = ActualSalary / totalnoofworkingdays;
            Decimal DeductionAmount = PerdaySalary * leavedays;
            return DeductionAmount;
        }
        public void ADDEmployeeSalary(List<EmployeeSalaryConfigList> ss, int type, int adminid, int EmployeecategoryId)
        {
            var date=DateTimeByZone.getCurrentDateTime();
            if (ss.Count > 0)
            {
                foreach (var a in ss)
                {
                    if (type < 4)
                    {
                        EmployeeSalary sal = new EmployeeSalary()
                        {
                            EmployeeRegisterId = a.EmployeeRegisterId,
                            ActualSalary = a.ActualSalary,
                            TotalNumberOfWorkingDays = a.TotalNoofWorkingDays,
                            NoOfDaysLeave = a.NoOfDaysLeave,
                            SalaryDeduction = a.DeductionAmount,
                            NetSalary = a.NetSalary,
                            EnteredBy = adminid,
                            EntryDate = date,
                            EmployeeTypeId = EmployeecategoryId
                        };
                        dc.EmployeeSalaries.Add(sal);
                        dc.SaveChanges();
                    }
                    else
                    {
                        EmployeeSalary sal = new EmployeeSalary()
                        {
                            OtherEmployeeregisterId = a.EmployeeRegisterId,
                            ActualSalary = a.ActualSalary,
                            TotalNumberOfWorkingDays = a.TotalNoofWorkingDays,
                            NoOfDaysLeave = a.NoOfDaysLeave,
                            SalaryDeduction = a.DeductionAmount,
                            NetSalary = a.NetSalary,
                            EnteredBy = adminid,
                            EntryDate = date,
                            EmployeeTypeId = EmployeecategoryId
                        };
                        dc.EmployeeSalaries.Add(sal);
                        dc.SaveChanges();
                    }
                }
            }
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}