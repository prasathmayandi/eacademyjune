﻿using eAcademy.Areas.FinancialAccounting.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
namespace eAcademy.Areas.FinancialAccounting.Services
{
    public class EmployeeSalaryConfigurationServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public bool AddEmployeeSalaryConfiguration(int EmployeeRegId, string etype, Decimal BS, Decimal DA, Decimal HRA, Decimal MA, Decimal OA, Decimal GE, Decimal PF, Decimal ESI, Decimal GD, Decimal NS, int AdminId)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            if (etype == "Tech")
            {
                var get = dc.EmployeeSalaryConfigurations.Where(q => q.EmployeeRegisterId == EmployeeRegId && q.Status == true).FirstOrDefault();
                if (get == null)
                {
                   
                    EmployeeSalaryConfiguration Sal = new EmployeeSalaryConfiguration()
                    {
                        EmployeeRegisterId = EmployeeRegId,
                        BasicSalary = BS,
                        DearnessAllowance = DA,
                        HouseRentAllowance = HRA,
                        MedicalAllowance = MA,
                        OtherAllowance = OA,
                        GrossEarning = GE,
                        ProvidentFund = PF,
                        Esi = ESI,
                        GrossDeduction = GD,
                        NetSalary = NS,
                        EnteredBy = AdminId,
                        EntryDate = date,
                        Status = true
                    };
                    dc.EmployeeSalaryConfigurations.Add(Sal);
                    dc.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                var get = dc.EmployeeSalaryConfigurations.Where(q => q.OtherEmployeeregisterId == EmployeeRegId && q.Status == true).FirstOrDefault();
                if (get == null)
                {
                    EmployeeSalaryConfiguration Sal = new EmployeeSalaryConfiguration()
                    {
                        OtherEmployeeregisterId = EmployeeRegId,
                        BasicSalary = BS,
                        DearnessAllowance = DA,
                        HouseRentAllowance = HRA,
                        MedicalAllowance = MA,
                        OtherAllowance = OA,
                        GrossEarning = GE,
                        ProvidentFund = PF,
                        Esi = ESI,
                        GrossDeduction = GD,
                        NetSalary = NS,
                        EnteredBy = AdminId,
                        EntryDate = date,
                        Status = true
                    };
                    dc.EmployeeSalaryConfigurations.Add(Sal);
                    dc.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool UpdateEmployeeSalaryConfiguration(int EmployeeRegId, string etype, Decimal BS, Decimal DA, Decimal HRA, Decimal MA, Decimal OA, Decimal GE, Decimal PF, Decimal ESI, Decimal GD, Decimal NS, int AdminId)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            if (etype == "Tech")
            {
                var get = dc.EmployeeSalaryConfigurations.Where(q => q.EmployeeRegisterId == EmployeeRegId && q.Status == true).FirstOrDefault();
                if (get != null)
                {
                    get.BasicSalary = BS;
                    get.DearnessAllowance = DA;
                    get.HouseRentAllowance = HRA;
                    get.MedicalAllowance = MA;
                    get.OtherAllowance = OA;
                    get.GrossEarning = GE;
                    get.ProvidentFund = PF;
                    get.Esi = ESI;
                    get.GrossDeduction = GD;
                    get.NetSalary = NS;
                    get.EnteredBy = AdminId;
                    get.EntryDate = date;
                    dc.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                var get = dc.EmployeeSalaryConfigurations.Where(q => q.OtherEmployeeregisterId == EmployeeRegId && q.Status == true).FirstOrDefault();
                if (get != null)
                {
                    get.BasicSalary = BS;
                    get.DearnessAllowance = DA;
                    get.HouseRentAllowance = HRA;
                    get.MedicalAllowance = MA;
                    get.OtherAllowance = OA;
                    get.GrossEarning = GE;
                    get.ProvidentFund = PF;
                    get.Esi = ESI;
                    get.GrossDeduction = GD;
                    get.NetSalary = NS;
                    get.EnteredBy = AdminId;
                    get.EntryDate = date;
                    dc.SaveChanges();
                    dc.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public List<EmployeeSalaryConfigList> GetEmployeeSalaryConfigurationList()
        {
            var ans1 = (((from a in dc.TechEmployees
                          from b in dc.EmployeeTypes
                          from c in dc.EmployeeDesignations
                          from d in dc.EmployeeSalaryConfigurations
                          where a.EmployeeRegisterId == d.EmployeeRegisterId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId && d.Status == true && a.EmployeeStatus == true
                          select new EmployeeSalaryConfigList
                          {
                              EmployeeSalaryConfigId = d.EmployeeSalaryConfigurationId,
                              EmployeeName = a.EmployeeName + " " + a.LastName,
                              EmployeeType = b.EmployeeTypes,
                              EmployeeDesignation = c.Designation,
                              BasicSalary = d.BasicSalary,
                              DearnessAllowance = d.DearnessAllowance,
                              HouseRentAllowance = d.HouseRentAllowance,
                              MedicalAllowance = d.MedicalAllowance,
                              OtherAllowance = d.OtherAllowance,
                              GrossEarning = d.GrossEarning,
                              ProvidentFund = d.ProvidentFund,
                              ESI = d.Esi,
                              GrossDeduction = d.GrossDeduction,
                              NetSalary = d.NetSalary,
                              EmployeeId = a.EmployeeId,
                              IncrementId = d.IncrementId
                          }).ToList().Select(q => new EmployeeSalaryConfigList
                        {
                            EmployeeSalaryConfigIdEtype = Convert.ToString(q.EmployeeSalaryConfigId) + "/" + "Tech" + "/" + Convert.ToString(q.IncrementId),
                            EmployeeSalaryConfigId = q.EmployeeSalaryConfigId,
                            EmployeeName = q.EmployeeName,
                            EmployeeType = q.EmployeeType,
                            EmployeeDesignation = q.EmployeeDesignation,
                            BasicSalary = q.BasicSalary,
                            DearnessAllowance = q.DearnessAllowance,
                            HouseRentAllowance = q.HouseRentAllowance,
                            MedicalAllowance = q.MedicalAllowance,
                            OtherAllowance = q.OtherAllowance,
                            GrossEarning = q.GrossEarning,
                            ProvidentFund = q.ProvidentFund,
                            ESI = q.ESI,
                            GrossDeduction = q.GrossDeduction,
                            NetSalary = q.NetSalary,
                            EmployeeId = q.EmployeeId
                            // 
                        }))
                        .Union((
                       from a in dc.OtherEmployees
                       from b in dc.EmployeeTypes
                       from c in dc.EmployeeDesignations
                       from d in dc.EmployeeSalaryConfigurations
                       where a.EmployeeRegisterId == d.OtherEmployeeregisterId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId && d.Status == true && a.EmployeeStatus == true
                       select new EmployeeSalaryConfigList
                       {
                           EmployeeSalaryConfigId = d.EmployeeSalaryConfigurationId,
                           EmployeeName = a.EmployeeName + " " + a.Lastname,
                           EmployeeType = b.EmployeeTypes,
                           EmployeeDesignation = c.Designation,
                           BasicSalary = d.BasicSalary,
                           DearnessAllowance = d.DearnessAllowance,
                           HouseRentAllowance = d.HouseRentAllowance,
                           MedicalAllowance = d.MedicalAllowance,
                           OtherAllowance = d.OtherAllowance,
                           GrossEarning = d.GrossEarning,
                           ProvidentFund = d.ProvidentFund,
                           ESI = d.Esi,
                           GrossDeduction = d.GrossDeduction,
                           NetSalary = d.NetSalary,
                           EmployeeId = a.EmployeeId,
                           IncrementId = d.IncrementId
                       }).ToList().Select(q => new EmployeeSalaryConfigList
                       {
                           EmployeeSalaryConfigIdEtype = Convert.ToString(q.EmployeeSalaryConfigId) + "/" + "Other" + "/" + Convert.ToString(q.IncrementId),
                           EmployeeSalaryConfigId = q.EmployeeSalaryConfigId,
                           EmployeeName = q.EmployeeName,
                           EmployeeType = q.EmployeeType,
                           EmployeeDesignation = q.EmployeeDesignation,
                           BasicSalary = q.BasicSalary,
                           DearnessAllowance = q.DearnessAllowance,
                           HouseRentAllowance = q.HouseRentAllowance,
                           MedicalAllowance = q.MedicalAllowance,
                           OtherAllowance = q.OtherAllowance,
                           GrossEarning = q.GrossEarning,
                           ProvidentFund = q.ProvidentFund,
                           ESI = q.ESI,
                           GrossDeduction = q.GrossDeduction,
                           NetSalary = q.NetSalary,
                           EmployeeId = q.EmployeeId
                       }))).OrderBy(q => q.EmployeeSalaryConfigId).Distinct().ToList();
            return ans1;
        }
        public EmployeeSalaryConfigList GettechEmployeeSalaryConfigusingEmployeeId(int ConfigId)
        {
            var ans = (from a in dc.EmployeeSalaryConfigurations
                       from c in dc.TechEmployees
                       from d in dc.EmployeeTypes
                       from e in dc.EmployeeDesignations
                       where a.EmployeeSalaryConfigurationId == ConfigId && a.Status == true && c.EmployeeRegisterId == a.EmployeeRegisterId && d.EmployeeTypeId == c.EmployeeTypeId && e.EmployeeDesignationId == c.EmployeeDesignationId && !(from b in dc.EmployeeSalaries
                                                                                                                                                                                                                                                where b.EmployeeRegisterId == a.EmployeeRegisterId
                                                                                                                                                                                                                                                select b.EmployeeRegisterId).Contains(a.EmployeeRegisterId)
                                                                                                                                                                                                                                     && !(from f in dc.EmployeeSalaryIncrements
                                                                                                                                                                                                                                          where f.EmployeeRegisterid == a.EmployeeRegisterId
                                                                                                                                                                                                                                          select f.EmployeeRegisterid).Contains(a.EmployeeRegisterId)
                       select new EmployeeSalaryConfigList
                       {
                           EmployeeSalaryConfigId = a.EmployeeSalaryConfigurationId,
                           EmployeeName = c.EmployeeName + " " + c.LastName,
                           EmployeeType = d.EmployeeTypes,
                           EmployeeDesignation = e.Designation,
                           BasicSalary = a.BasicSalary,
                           DearnessAllowance = a.DearnessAllowance,
                           HouseRentAllowance = a.HouseRentAllowance,
                           MedicalAllowance = a.MedicalAllowance,
                           OtherAllowance = a.OtherAllowance,
                           GrossEarning = a.GrossEarning,
                           ProvidentFund = a.ProvidentFund,
                           ESI = a.Esi,
                           GrossDeduction = a.GrossDeduction,
                           NetSalary = a.NetSalary,
                           EmployeeId = c.EmployeeId,
                           Etype = "Tech",
                           EmployeeRegisterId = c.EmployeeRegisterId,
                           ImgFile = c.ImgFile
                       }).FirstOrDefault();
            return ans;
        }
        public EmployeeSalaryConfigList GetothherEmployeeSalaryConfigusingEmployeeId(int ConfigId)
        {
            var ans = (from a in dc.EmployeeSalaryConfigurations
                       from c in dc.OtherEmployees
                       from d in dc.EmployeeTypes
                       from e in dc.EmployeeDesignations
                       where a.EmployeeSalaryConfigurationId == ConfigId && a.Status == true && c.EmployeeRegisterId == a.OtherEmployeeregisterId && d.EmployeeTypeId == c.EmployeeTypeId && e.EmployeeDesignationId == c.EmployeeDesignationId && !(from b in dc.EmployeeSalaries
                                                                                                                                                                                                                                                     where b.OtherEmployeeregisterId == a.OtherEmployeeregisterId
                                                                                                                                                                                                                                                     select b.OtherEmployeeregisterId).Contains(a.OtherEmployeeregisterId)
                                                                                                                                                                                                                                            && !(from f in dc.EmployeeSalaryIncrements
                                                                                                                                                                                                                                                 where f.OtherEmployeeRegisterid == a.OtherEmployeeregisterId
                                                                                                                                                                                                                                                 select f.OtherEmployeeRegisterid).Contains(a.OtherEmployeeregisterId)
                       select new EmployeeSalaryConfigList
                       {
                           EmployeeSalaryConfigId = a.EmployeeSalaryConfigurationId,
                           EmployeeName = c.EmployeeName + " " + c.Lastname,
                           EmployeeType = d.EmployeeTypes,
                           EmployeeDesignation = e.Designation,
                           BasicSalary = a.BasicSalary,
                           DearnessAllowance = a.DearnessAllowance,
                           HouseRentAllowance = a.HouseRentAllowance,
                           MedicalAllowance = a.MedicalAllowance,
                           OtherAllowance = a.OtherAllowance,
                           GrossEarning = a.GrossEarning,
                           ProvidentFund = a.ProvidentFund,
                           ESI = a.Esi,
                           GrossDeduction = a.GrossDeduction,
                           NetSalary = a.NetSalary,
                           EmployeeId = c.EmployeeId,
                           Etype = "Other",
                           EmployeeRegisterId = c.EmployeeRegisterId,
                           ImgFile = c.ImgFile
                       }).FirstOrDefault();
            return ans;
        }
        public EmployeeSalaryConfigList GettechEmployeeSalaryConfig(int ConfigId)
        {
            var ans = (from a in dc.EmployeeSalaryConfigurations
                       from c in dc.TechEmployees
                       from d in dc.EmployeeTypes
                       from e in dc.EmployeeDesignations
                       where a.EmployeeSalaryConfigurationId == ConfigId && a.Status == true && c.EmployeeRegisterId == a.EmployeeRegisterId && d.EmployeeTypeId == c.EmployeeTypeId && e.EmployeeDesignationId == c.EmployeeDesignationId
                       select new EmployeeSalaryConfigList
                       {
                           EmployeeSalaryConfigId = a.EmployeeSalaryConfigurationId,
                           EmployeeName = c.EmployeeName + " " + c.LastName,
                           EmployeeType = d.EmployeeTypes,
                           EmployeeDesignation = e.Designation,
                           BasicSalary = a.BasicSalary,
                           DearnessAllowance = a.DearnessAllowance,
                           HouseRentAllowance = a.HouseRentAllowance,
                           MedicalAllowance = a.MedicalAllowance,
                           OtherAllowance = a.OtherAllowance,
                           GrossEarning = a.GrossEarning,
                           ProvidentFund = a.ProvidentFund,
                           ESI = a.Esi,
                           GrossDeduction = a.GrossDeduction,
                           NetSalary = a.NetSalary,
                           EmployeeId = c.EmployeeId,
                           Etype = "Tech",
                           EmployeeRegisterId = c.EmployeeRegisterId,
                           ImgFile = c.ImgFile
                       }).FirstOrDefault();
            return ans;
        }
        public EmployeeSalaryConfigList GetotherEmployeeSalaryConfig(int ConfigId)
        {
            var ans = (from a in dc.EmployeeSalaryConfigurations
                       from c in dc.OtherEmployees
                       from d in dc.EmployeeTypes
                       from e in dc.EmployeeDesignations
                       where a.EmployeeSalaryConfigurationId == ConfigId && a.Status == true && c.EmployeeRegisterId == a.OtherEmployeeregisterId && d.EmployeeTypeId == c.EmployeeTypeId && e.EmployeeDesignationId == c.EmployeeDesignationId
                       select new EmployeeSalaryConfigList
                       {
                           EmployeeSalaryConfigId = a.EmployeeSalaryConfigurationId,
                           EmployeeName = c.EmployeeName + " " + c.Lastname,
                           EmployeeType = d.EmployeeTypes,
                           EmployeeDesignation = e.Designation,
                           BasicSalary = a.BasicSalary,
                           DearnessAllowance = a.DearnessAllowance,
                           HouseRentAllowance = a.HouseRentAllowance,
                           MedicalAllowance = a.MedicalAllowance,
                           OtherAllowance = a.OtherAllowance,
                           GrossEarning = a.GrossEarning,
                           ProvidentFund = a.ProvidentFund,
                           ESI = a.Esi,
                           GrossDeduction = a.GrossDeduction,
                           NetSalary = a.NetSalary,
                           EmployeeId = c.EmployeeId,
                           Etype = "Other",
                           EmployeeRegisterId = c.EmployeeRegisterId,
                           ImgFile = c.ImgFile
                       }).FirstOrDefault();
            return ans;
        }
        public void AddSalaryincrement(int Salaryconfigid, Decimal IncrementAmt, int incrementid, int Adminid)
        {
            var date=DateTimeByZone.getCurrentDateTime();
            var get = dc.EmployeeSalaryConfigurations.Where(q => q.EmployeeSalaryConfigurationId == Salaryconfigid).FirstOrDefault();
            if (get != null)
            {
                Decimal bs = Convert.ToDecimal(get.BasicSalary) + IncrementAmt;
                get.BasicSalary = bs;
                Decimal Ge = Convert.ToDecimal(get.GrossEarning) + IncrementAmt;
                get.GrossEarning = Ge;
                Decimal Ns = Convert.ToDecimal(get.NetSalary) + IncrementAmt;
                get.NetSalary = Ns;
                get.IncrementId = incrementid;
                get.Increment = IncrementAmt;
                get.EnteredBy = Adminid;
                get.EntryDate = date;
                dc.SaveChanges();
            }
        }
        public void UpdateSalaryincrement(int Salaryconfigid, Decimal BS, Decimal GE, Decimal GD, Decimal OldNS, Decimal Salaryincrement, int incrementid, int Adminid)
        {
            var date=DateTimeByZone.getCurrentDateTime();
            var get = dc.EmployeeSalaryConfigurations.Where(q => q.EmployeeSalaryConfigurationId == Salaryconfigid && q.IncrementId == incrementid).FirstOrDefault();
            if (get != null)
            {
                Decimal bs = BS + Salaryincrement;
                get.BasicSalary = bs;
                Decimal Ge = GE + Salaryincrement;
                get.GrossEarning = Ge;
                Decimal Ns = OldNS + Salaryincrement;
                get.NetSalary = Ns;
                get.IncrementId = incrementid;
                get.Increment = Salaryincrement;
                get.EnteredBy = Adminid;
                get.EntryDate = date;
                dc.SaveChanges();
            }
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}