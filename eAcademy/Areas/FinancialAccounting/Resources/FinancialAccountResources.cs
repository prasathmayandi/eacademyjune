﻿using eAcademy.Services;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.FinancialAccounting.Resources
{
    public class FinancialAccountResources
    {
        public static string Pleaseenterbasicsalary
        {
            get
            {
                return ResourceCache.Localize("Pleaseenterbasicsalary");
            }
        }
        public static string Pleaseenterdearnessallowanceminmumvalue0
        {
            get
            {
                return ResourceCache.Localize("Pleaseenterdearnessallowanceminmumvalue0");
            }
        }
        public static string Pleaseenterhouserentallowanceminmumvalue0
        {
            get
            {
                return ResourceCache.Localize("Pleaseenterhouserentallowanceminmumvalue0");
            }
        }
        public static string Pleaseentermedicalallowanceminmumvalue0
        {
            get
            {
                return ResourceCache.Localize("Pleaseentermedicalallowanceminmumvalue0");
            }
        }
        public static string Pleaseenterotherallowanceminmumvalue0
        {
            get
            {
                return ResourceCache.Localize("Pleaseenterotherallowanceminmumvalue0");
            }
        }
        public static string Pleaseentergrossearning
        {
            get
            {
                return ResourceCache.Localize("Pleaseentergrossearning");
            }
        }
        public static string Pleaseenterprovidentfundminmumvalue0
        {
            get
            {
                return ResourceCache.Localize("Pleaseenterprovidentfundminmumvalue0");
            }
        }
        public static string Pleaseenteresiminmumvalue0
        {
            get
            {
                return ResourceCache.Localize("Pleaseenteresiminmumvalue0");
            }
        }
        public static string Pleaseentergrossdeduction
        {
            get
            {
                return ResourceCache.Localize("Pleaseentergrossdeduction");
            }
        }
        public static string Pleaseenternetsalary
        {
            get
            {
                return ResourceCache.Localize("Pleaseenternetsalary");
            }
        }
        public static string Pleaseenteroldnetsalary
        {
            get
            {
                return ResourceCache.Localize("Pleaseenteroldnetsalary");
            }
        }
        public static string Pleaseenterincrementsalary
        {
            get
            {
                return ResourceCache.Localize("Pleaseenterincrementsalary");
            }
        }
        public static string Pleaseenternewnetsalary
        {
            get
            {
                return ResourceCache.Localize("Pleaseenternewnetsalary");
            }
        }
        public static string PleaseSelectEmployeeCategory
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectEmployeeCategory");
            }
        }
        public static string PleaseenterYear
        {
            get
            {
                return ResourceCache.Localize("PleaseenterYear");
            }
        }
        public static string PleaseSelectMonth
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectMonth");
            }
        }
    }
}