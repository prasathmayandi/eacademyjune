﻿using eAcademy.Areas.FinancialAccounting.LocalResource;
using eAcademy.Areas.FinancialAccounting.Models;
using eAcademy.Areas.FinancialAccounting.Services;
using eAcademy.Areas.FinancialAccounting.ViewModel;
using eAcademy.HelperClass;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
namespace eAcademy.Areas.FinancialAccounting.Controllers
{
    [CheckSessionOutAttribute]
    public class EmployeeSalaryController : Controller
    {
        public int AdminId;
        UserActivityHelper useractivity = new UserActivityHelper();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            ViewBag.academicyear = As.GetAcademicYear();
            ViewBag.Classes = Cs.GetClass();
            AdminId = Convert.ToInt32(Session["empRegId"]);
        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (Session["CurrentCulture"] != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["CurrentCulture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["CurrentCulture"].ToString());
            }
        }
        public ActionResult EmployeeSalaryConfiguration()
        {
            return View();
        }
        public JsonResult CheckStaff(string RegId)
        {
            try
            {
                TechEmployeeServices T_Employee = new TechEmployeeServices();
                OtherEmployeeServices O_Employee = new OtherEmployeeServices();
                var Techemployee = T_Employee.GetEmployeeForSalaryConfig(RegId);
                var otheremployee = O_Employee.GetEmployeeForSalaryConfig(RegId);
                if (Techemployee != null)
                {

                    if (Techemployee.Salary == null)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Cant_configure_salary_in_this_staff_So_check_Employee_Configuration");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { Techemployee = Techemployee, }, JsonRequestBehavior.AllowGet);
                    }
                    
                }
                else if (otheremployee != null)
                {
                    if (otheremployee.Salary == null)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Cant_configure_salary_in_this_staff_So_check_Employee_Configuration");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { otheremployee = otheremployee, }, JsonRequestBehavior.AllowGet);
                    }
                   
                }
                else
                {
                    bool checktechemployee = T_Employee.Checkalreadysalaryconfigtoemployee(RegId);
                    bool checkotheremployee = O_Employee.Checkalreadysalaryconfigtoemployee(RegId);
                    if (checktechemployee == true)
                    {
                        string ErrorMessage =eAcademy.Models.ResourceCache.Localize("Employeesalaryalreadyconfigurated");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else if (checkotheremployee == true)
                    {
                        string ErrorMessage =eAcademy.Models.ResourceCache.Localize("Employeesalaryalreadyconfigurated");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage =eAcademy.Models.ResourceCache.Localize("Entercorrectemployeeid");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveEmployeeSalaryConfig(VM_SaveEmployeeSalaryConFig cs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    EmployeeSalaryConfigurationServices salconfig = new EmployeeSalaryConfigurationServices();
                    bool Employee = salconfig.AddEmployeeSalaryConfiguration(cs.EmployeeRegisterId, cs.Etype, cs.BasicSalary, cs.DearnessAllowance, cs.HouseRenAllowance, cs.MedicalAllowance, cs.OtherAllowance, cs.GrossEarning, cs.ProvidentFund, cs.ESI, cs.GrossDeduction, cs.NetSalary, AdminId);
                    if (Employee == true)
                    {
                        string Message = eAcademy.Models.ResourceCache.Localize("Successfullyemployeesalaryconfiguartionadded");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("AddEmployeeSalaryConfiguration"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage =eAcademy.Models.ResourceCache.Localize("Alreadyaddedemployeesalaryconfiguartion");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public static IList<EmployeeSalaryConfigList> Hostellist = new List<EmployeeSalaryConfigList>();
        public JsonResult EmployeeSalaryConfigList(string sidx, string sord, int page, int rows)
        {
            EmployeeSalaryConfigurationServices salconfig = new EmployeeSalaryConfigurationServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            Hostellist = salconfig.GetEmployeeSalaryConfigurationList();
            int totalRecords = Hostellist.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    Hostellist = Hostellist.OrderByDescending(s => s.EmployeeSalaryConfigId).ToList();
                    Hostellist = Hostellist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    Hostellist = Hostellist.OrderBy(s => s.EmployeeSalaryConfigId).ToList();
                    Hostellist = Hostellist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = Hostellist
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EmployeeSalaryConfig_ExportToExcel()
        {
            try
            {
                var list = Hostellist.Select(o => new { EmployeeId = o.EmployeeId, EmployeeName = o.EmployeeName, EmployeeType = o.EmployeeType, EmployeeDesignation = o.EmployeeDesignation, BasicSalary = o.BasicSalary, DearnessAllowance = o.DearnessAllowance, HouseRentAllowance = o.HouseRentAllowance, MedicalAllowance = o.MedicalAllowance, OtherAllowance = o.OtherAllowance, GrossEarning = o.GrossEarning, ProvidentFund = o.ProvidentFund, ESI = o.ESI, GrossDeduction = o.GrossDeduction, NetSalary = o.NetSalary }).ToList();
                string heading =eAcademy.Models.ResourceCache.Localize("EmployeeSalaryConfigurationList");
                GenerateExcel.ExportExcel(list, heading);
                return View("HostalStudent");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult EmployeeSalaryConfig_ExportToPdf()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading =eAcademy.Models.ResourceCache.Localize("EmployeeSalaryConfigurationList");
                GeneratePDF.ExportPDF_Landscape(Hostellist, new string[] { "EmployeeId", "EmployeeName", "EmployeeType", "EmployeeDesignation", "BasicSalary", "DearnessAllowance", "HouseRentAllowance", "MedicalAllowance", "OtherAllowance", "GrossEarning", "ProvidentFund", "ESI", "GrossDeduction", "NetSalary" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", heading + ".pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public JsonResult EditEmployeesalaryConfig(string id)
        {
            try
            {
                EmployeeSalaryConfigurationServices salconfig = new EmployeeSalaryConfigurationServices();
                EmployeeSalaryincrementServices Salincrement = new EmployeeSalaryincrementServices();
                string[] c_id = id.Split('/');
                if (c_id[1] == "Tech")
                {
                    int count = c_id.Length;
                    var techEmployee = salconfig.GettechEmployeeSalaryConfigusingEmployeeId(Convert.ToInt16(c_id[0]));
                    if (techEmployee != null)
                    {
                        return Json(new { Employee = techEmployee }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        int incrementid; ;
                        if(c_id[2]=="")
                        {
                            incrementid = 0;
                        }
                        else
                        {
                            incrementid = Convert.ToInt16(c_id[2]);
                        }
                        
                        if (incrementid != 0)
                        {
                            var EmployeeIncrement = Salincrement.getTechSalaryincrementDetails(incrementid);
                            if (EmployeeIncrement != null)
                            {
                                return Json(new { EmployeeIncrement = EmployeeIncrement }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                string ErrorMsg =eAcademy.Models.ResourceCache.Localize("Employeeincrementdetailsnotfound");
                                return Json(new { ErrorMessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            string ErrorMsg =eAcademy.Models.ResourceCache.Localize("Alreadyemployeesalarypayedsocantedited");
                            return Json(new { ErrorMessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else if (c_id[1] == "Other")
                {
                    var otherEmployee = salconfig.GetothherEmployeeSalaryConfigusingEmployeeId(Convert.ToInt16(c_id[0]));
                    if (otherEmployee != null)
                    {
                        return Json(new { Employee = otherEmployee }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        int incrementid = Convert.ToInt16(c_id[2]);
                        if (incrementid != 0)
                        {
                            var EmployeeIncrement = Salincrement.getOtherSalaryincrementDetails(incrementid);
                            if (EmployeeIncrement != null)
                            {
                                return Json(new { EmployeeIncrement = EmployeeIncrement }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                string ErrorMsg = eAcademy.Models.ResourceCache.Localize("Employeeincrementdetailsnotfound");
                                return Json(new { ErrorMessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            string ErrorMsg = eAcademy.Models.ResourceCache.Localize("Alreadyemployeesalarypayedsocantedited");
                            return Json(new { ErrorMessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    string ErrMsg = eAcademy.Models.ResourceCache.Localize("ChoosecorrectEmployee");
                    return Json(new { ErrorMessage = ErrMsg }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UpdateEmployeeSalaryConfig(VM_SaveEmployeeSalaryConFig cs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    EmployeeSalaryConfigurationServices salconfig = new EmployeeSalaryConfigurationServices();
                    bool Employee = salconfig.UpdateEmployeeSalaryConfiguration(cs.EmployeeRegisterId, cs.Etype, cs.BasicSalary, cs.DearnessAllowance, cs.HouseRenAllowance, cs.MedicalAllowance, cs.OtherAllowance, cs.GrossEarning, cs.ProvidentFund, cs.ESI, cs.GrossDeduction, cs.NetSalary, AdminId);
                    if (Employee == true)
                    {
                        string Message =eAcademy.Models.ResourceCache.Localize("Successfullyupdatedemployeesalaryconfiguartion");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("UpdateEmployeeSalaryConfiguration"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Alreadyaddedemployeesalaryconfiguartion");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Employeeincrement(string id)
        {
            try
            {
                EmployeeSalaryConfigurationServices salconfig = new EmployeeSalaryConfigurationServices();
                string[] c_id = id.Split('/');
                if (c_id[1] == "Tech")
                {
                    var techEmployee = salconfig.GettechEmployeeSalaryConfig(Convert.ToInt16(c_id[0]));
                    if (techEmployee != null)
                    {
                        return Json(new { Employee = techEmployee }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMsg =eAcademy.Models.ResourceCache.Localize("Employeedetailsnotfound");
                        return Json(new { ErrorMessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (c_id[1] == "Other")
                {
                    var otherEmployee = salconfig.GetotherEmployeeSalaryConfig(Convert.ToInt16(c_id[0]));
                    if (otherEmployee != null)
                    {
                        return Json(new { Employee = otherEmployee }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMsg = eAcademy.Models.ResourceCache.Localize("Employeedetailsnotfound");
                        return Json(new { ErrorMessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string ErrMsg =eAcademy.Models.ResourceCache.Localize("ChoosecorrectEmployee");
                    return Json(new { ErrorMessage = ErrMsg }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveEmployeeSalaryIncrement(VM_SalaryIncrement cs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    EmployeeSalaryincrementServices salincrement = new EmployeeSalaryincrementServices();
                    EmployeeSalaryConfigurationServices SalConfig = new EmployeeSalaryConfigurationServices();
                    int Incrementid = salincrement.AddSalaryIncrement(cs.EmployeeRegisterId, cs.Etype, cs.BasicSalary, cs.GrossEarning, cs.GrossDeduction, cs.NetSalary, cs.IncrementSalary, cs.NewNetSalary, AdminId);
                    if (Incrementid != 0)
                    {
                        SalConfig.AddSalaryincrement(cs.SalaryConfigId, cs.IncrementSalary, Incrementid, AdminId);
                        string Message = eAcademy.Models.ResourceCache.Localize("Successfullyaddedemployeesalaryincrement");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("AddEmployeeSalaryIncrement"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage =eAcademy.Models.ResourceCache.Localize("ChoosecorrectEmployee");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UpdateEmployeeSalaryIncrement(VM_SalaryIncrement cs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    EmployeeSalaryincrementServices salincrement = new EmployeeSalaryincrementServices();
                    EmployeeSalaryConfigurationServices SalConfig = new EmployeeSalaryConfigurationServices();
                    salincrement.UpdateSalaryIncrement(cs.SalaryIncrementId, cs.IncrementSalary, AdminId);
                    SalConfig.UpdateSalaryincrement(cs.SalaryConfigId, cs.BasicSalary, cs.GrossEarning, cs.GrossDeduction, cs.NetSalary, cs.IncrementSalary, cs.SalaryIncrementId, AdminId);
                    string Message = eAcademy.Models.ResourceCache.Localize("Successfullyupdatedemployeesalaryincrement");
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("UpdateEmployeeSalaryIncrement"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EmployeeMonthlySalary()
        {
            MonthsServices Ms = new MonthsServices();
            EmployeeTypeServices Emptype = new EmployeeTypeServices();
            ViewBag.month = Ms.GetPreviousMonth();
            ViewBag.EmployeeTypes = Emptype.GetEmployeeType();
            ViewBag.year = Ms.GetPreviousMonthYear();
            return View();
        }
        public JsonResult EmployeeSalaryDetails(Vm_EmployeeMonthSalary cs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    EmployeeSalaryServices EmployeeSal = new EmployeeSalaryServices();
                    var CheckAlreadySalarygenerate = EmployeeSal.checkSalarygenerated(cs.EmployeeCategoryId);
                    if (CheckAlreadySalarygenerate == null)
                    {
                            var Salary = EmployeeSal.GetEmployeeSalaryDetails(cs.EmployeeCategoryId, cs.monthId, cs.Year);
                        if (Salary.Count > 0)
                        {
                            return Json(new { Salary = Salary }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Employeedetailsnotfound");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        var Salary = EmployeeSal.GetEmployeeSalaryDetails(cs.EmployeeCategoryId, cs.monthId, cs.Year);
                        string InfoMsg =eAcademy.Models.ResourceCache.Localize("Alreadyemployeesalarycalculated");
                        return Json(new { InfoMsg = InfoMsg, AlreadySalary = Salary }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GenerateEmployeeSalary(Vm_EmployeeMonthSalary cs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    EmployeeSalaryServices EmployeeSal = new EmployeeSalaryServices();
                    var CheckAlreadySalarygenerate = EmployeeSal.checkSalarygenerated(cs.EmployeeCategoryId);
                    if (CheckAlreadySalarygenerate == null)
                    {
                        var Salary = EmployeeSal.GetEmployeeSalaryDetails(cs.EmployeeCategoryId, cs.monthId, cs.Year);
                        if (Salary.Count > 0)
                        {
                            EmployeeSal.ADDEmployeeSalary(Salary, cs.EmployeeCategoryId, AdminId, cs.EmployeeCategoryId);
                            string Message =eAcademy.Models.ResourceCache.Localize("Successfullyemployeesalarygenerated");
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("EmployeeSalaryGenerated"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Employeedetailsnotfound");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string ErrorMessage =  eAcademy.Models.ResourceCache.Localize("Alreadyemployeesalarycalculated");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}