﻿using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.Transport.Resources
{
    public class TransportResources
    {
        //FeeConfig
        public static string Please_enter_destination_name
        {
            get
            {
                return ResourceCache.Localize("Please_enter_destination_name");
            }
        }
        public static string Fill_pick_point_to_all_Added_Rows
        {
            get
            {
                return ResourceCache.Localize("Fill_pick_point_to_all_Added_Rows");
            }
        }
        public static string Fill_distance_to_all_Added_Rows
        {
            get
            {
                return ResourceCache.Localize("Fill_distance_to_all_Added_Rows");
            }
        }
        public static string Fill_charge_to_all_Added_Rows
        {
            get
            {
                return ResourceCache.Localize("Fill_charge_to_all_Added_Rows");
            }
        }
        //Mapping 
        public static string Please_enter_route_name
        {
            get
            {
                return ResourceCache.Localize("Please_enter_route_name");
            }
        }
        public static string Please_select_register_number
        {
            get
            {
                return ResourceCache.Localize("Please_select_register_number");
            }
        }
        public static string Please_select_destination
        {
            get
            {
                return ResourceCache.Localize("Please_select_destination");
            }
        }
        public static string Please_select_school_in_time
        {
            get
            {
                return ResourceCache.Localize("Please_select_school_in_time");
            }
        }
        public static string Please_select_school_out_time
        {
            get
            {
                return ResourceCache.Localize("Please_select_school_out_time");
            }
        }
        public static string Please_select_parking_start_time
        {
            get
            {
                return ResourceCache.Localize("Please_select_parking_start_time");
            }
        }
        public static string Please_select_parking_reach_time
        {
            get
            {
                return ResourceCache.Localize("Please_select_parking_reach_time");
            }
        }
        public static string PleaseSelectDriver
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectDriver");
            }
        }
        public static string PleaseSelectHelper
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectHelper");
            }
        }
        public static string Please_select_departure_time_to_all_pick_point
        {
            get
            {
                return ResourceCache.Localize("Please_select_departure_time_to_all_pick_point");
            }
        }
        public static string Please_select_leaving_time_to_all_pick_point
        {
            get
            {
                return ResourceCache.Localize("Please_select_leaving_time_to_all_pick_point");
            }
        }
        //Transport Allotment
        public static string PleaseSelectAcademicYear
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectAcademicYear");
            }
        }
        public static string PleaseSelectClass
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectClass");
            }
        }
        public static string PleaseSelectSection
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectSection");
            }
        }
        public static string PleaseSelectStudent
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectStudent");
            }
        }
        public static string Please_select_pickpoint
        {
            get
            {
                return ResourceCache.Localize("Please_select_pickpoint");
            }
        }
        //Transport fees collection
        public static string Please_select_fee_amount
        {
            get
            {
                return ResourceCache.Localize("Please_select_fee_amount");
            }
        }
        public static string Please_select_total_amount
        {
            get
            {
                return ResourceCache.Localize("Please_select_total_amount");
            }
        }
        public static string Please_Select_Payment_Mode
        {
            get
            {
                return ResourceCache.Localize("Please_Select_Payment_Mode");
            }
        }
        //VehicleConfiguration
        public static string Please_enter_registration_number
        {
            get
            {
                return ResourceCache.Localize("Please_enter_registration_number");
            }
        }
        public static string Please_enter_registration_date
        {
            get
            {
                return ResourceCache.Localize("Please_enter_registration_date");
            }
        }
        public static string Please_enter_owner_first_name
        {
            get
            {
                return ResourceCache.Localize("Please_enter_owner_first_name");
            }
        }
        public static string Please_enter_owner_last_name
        {
            get
            {
                return ResourceCache.Localize("Please_enter_owner_last_name");
            }
        }
        public static string Please_enter_address_Line1
        {
            get
            {
                return ResourceCache.Localize("Please_enter_address_Line1");
            }
        }
        public static string Please_enter_address_Line2
        {
            get
            {
                return ResourceCache.Localize("Please_enter_address_Line2");
            }
        }
        public static string Please_enter_city
        {
            get
            {
                return ResourceCache.Localize("Please_enter_city");
            }
        }
        public static string Please_Enter_State  
        {
            get
            {
                return ResourceCache.Localize("Please_Enter_State");
            }
        }

        public static string PleaseSelectCountry
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectCountry");
            }
        }
        public static string Please_enter_pincode
        {
            get
            {
                return ResourceCache.Localize("Please_enter_pincode");
            }
        }
        public static string Please_enter_bought_date
        {
            get
            {
                return ResourceCache.Localize("Please_enter_bought_date");
            }
        }
        public static string Please_select_vehicle_type
        {
            get
            {
                return ResourceCache.Localize("Please_select_vehicle_type");
            }
        }
        public static string Please_enter_manufacture_name
        {
            get
            {
                return ResourceCache.Localize("Please_enter_manufacture_name");
            }
        }
        public static string Please_enter_color
        {
            get
            {
                return ResourceCache.Localize("Please_enter_color");
            }
        }
        public static string Please_enter_engine_number
        {
            get
            {
                return ResourceCache.Localize("Please_enter_engine_number");
            }
        }
        public static string Please_enter_chase_number
        {
            get
            {
                return ResourceCache.Localize("Please_enter_chase_number");
            }
        }
        public static string Please_select_fuel_type
        {
            get
            {
                return ResourceCache.Localize("Please_select_fuel_type");
            }
        }
        public static string Please_enter_fuel_capacity
        {
            get
            {
                return ResourceCache.Localize("Please_enter_fuel_capacitys");
            }
        }
        public static string Please_enter_seat_capacity
        {
            get
            {
                return ResourceCache.Localize("Please_enter_seat_capacity");
            }
        }
        public static string Please_enter_register_upto_date
        {
            get
            {
                return ResourceCache.Localize("Please_enter_register_upto_date");
            }
        }
        public static string Please_enter_tax_valid_upto
        {
            get
            {
                return ResourceCache.Localize("Please_enter_tax_valid_upto");
            }
        }
        public static string Please_enter_insurance_number
        {
            get
            {
                return ResourceCache.Localize("Please_enter_insurance_number");
            }
        }
        public static string Please_enter_insurance_valid_from_date
        {
            get
            {
                return ResourceCache.Localize("Please_enter_insurance_valid_from_date");
            }
        }
        public static string Please_enter_insurance_valid_upto_date
        {
            get
            {
                return ResourceCache.Localize("Please_enter_insurance_valid_upto_date");
            }
        }
    }
}