﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Transport.Models
{
    public class StudentTransportList
    {
        public string StudentName { get; set; }
        public string Year { get; set; }
        public string Class { get; set; }
        public string Destination { get; set; }
        public string PickPoint { get; set; }
        public int Id { get; set; }
        public string RouteNumber { get; set; }
        public bool? Status { get; set; }
    }
}