﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Transport.Models
{
    public class VehicleList
    {
        public string RegistrationNumber { get; set; }
        public string VehicleType { get; set; }
        public string Manufactured { get; set; }
        public string Color { get; set; }
        public int SeatCapacity { get; set; }
        public string availableSeat { get; set; }
        public string Id { get; set; }
        public string status { get; set; }
    }
}