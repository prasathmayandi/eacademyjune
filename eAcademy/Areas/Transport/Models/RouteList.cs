﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.Transport.Models
{
    public class RouteList
    {
        public string Id { get; set; }
        public string RegistrationNumber { get; set; }
        public string RouteName { get; set; }
        public string Destination { get; set; }
        public string PickPoints { get; set; }
        public string status { get; set; }
    }
}