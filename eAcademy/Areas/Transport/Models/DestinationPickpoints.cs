﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.Transport.Models
{
    public class DestinationPickpoints
    {
        public string DestinationId { get; set; }
        public string DestinationName { get; set; }
        public string[] PickPointName { get; set; }
        public string[] Distance { get; set; }
        public decimal[] Charge { get; set; }
        public int Count { get; set; }
        public Boolean? Status { get; set; }
        public string PickPointsList { get; set; }
    }
}