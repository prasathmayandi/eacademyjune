﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.Transport.Models
{
    public class StudentTransportDetail
    {
        public int? DestinationId { get; set; }
         public int? PickPointId { get; set; }
         public Decimal FeeAmount { get; set; }
         public Decimal Discount { get; set; }
         public Decimal LateFee { get; set; }
         public Decimal Vat { get; set; }
         public Decimal TotalAmount { get; set; }
         public string PaymentMode { get; set; }
         public string BankName { get; set; }
         public string ChequeNo { get; set; }
         public string DDno { get; set; }
         public string Remark { get; set; }
         public string DestinationName { get; set; }
         public string PickPointName { get; set; }
         public string RouteName { get; set; }
         public int DestinationId1 { get; set; }
         public string destination_Id { get; set; }
         public string PickPoint_Id { get; set; }
         public string txt_AcademicYear { get; set; }
         public string txt_Class { get; set; }
         public string txt_Section { get; set; }
         public string StudentName { get; set; }
         public string StudentRollNumber { get; set; }
         public string StudentId { get; set; }
         public int ReceiptNo { get; set; }
         public Decimal? old_FeeAmount { get; set; }
         public Decimal? old_TotalAmount { get; set; }
         public Decimal? old_Discount { get; set; }
         public Decimal? old_LateFee { get; set; }
         public Decimal? old_Vat { get; set; }
    }
}