﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.Transport.Models
{
    public class PickPointList
    {
        public string id { get; set; }
        public string PickPoint { get; set; }
        public string DepartureTime { get; set; }
        public string LeavingTime { get; set; }
        public string DriverName { get; set; }
        public string HelperName { get; set; }
        public int DriverId { get; set; }
    }
}