﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.Transport.Models
{
    public class PickPoints
    {
        public string DestinationId { get; set; }
        public string DestinationName { get; set; }
        public string PickPointId { get; set; }
        public string PickPointName { get; set; }
        public string Distance { get; set; }
        public decimal Charge { get; set; }
        public int Count { get; set; }
        public Boolean? Status { get; set; }
        public string Charge1 { get; set; }
        public decimal Distance1 { get; set; }
    }
}