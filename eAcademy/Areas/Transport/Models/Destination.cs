﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.Transport.Models
{
    public class Destination
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_destination_name")]
        public string DestinationName { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Fill_pick_point_to_all_Added_Rows")]
        public string[] PickPointName { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                 ErrorMessageResourceName = "Fill_distance_to_all_Added_Rows")]
        public string[] Distance { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Fill_charge_to_all_Added_Rows")]
        public decimal[] Charge { get; set; }

        public int Count { get; set; }
        public Boolean? Status { get; set; }












        //public string DestinationName { get; set; }
        //public string[] PickPointName { get; set; }
        //public string[] Distance { get; set; }
        //public decimal[] Charge { get; set; }
        //public int Count { get; set; }
        //public Boolean? Status { get; set; }
    }
}