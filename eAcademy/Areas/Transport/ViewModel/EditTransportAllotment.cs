﻿
namespace eAcademy.Areas.Transport.ViewModel
{
    public class EditTransportAllotment
    {
        public int TransportAllotmentId { get; set; }
        public int? academicYearId { get; set; }
        public string AcademicYear { get; set; }
        public int? classId { get; set; }
        public string Class { get; set; }
        public int? sectionId { get; set; }
        public string Section { get; set; }
        public int? studentId { get; set; }
        public string Student { get; set; }
        public int? destination_Id { get; set; }
        public string Destinaion { get; set; }
        public int? pickpoint_Id { get; set; }
        public string Pickpoint { get; set; }
        public int? Route_Id { get; set; }
        public string Route{ get; set; }
        public bool? Status { get; set; }
    }
}

