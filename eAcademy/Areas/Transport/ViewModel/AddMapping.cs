﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Areas.Transport.ViewModel
{
    public class AddMapping
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_route_name")]
        public string txt_RouteName { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_select_register_number")]
        public int RegisterNumber { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_select_destination")]
        public int Destination { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_select_school_in_time")]
        public TimeSpan txt_SchoolInTime { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_select_school_out_time")]
        public TimeSpan txt_SchoolOutTime { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_select_parking_start_time")]
        public TimeSpan txt_ParkingStartTime { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_select_parking_reach_time")]
        public TimeSpan txt_ParkingEndTime { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "PleaseSelectDriver")]
        public int Driver { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                 ErrorMessageResourceName = "PleaseSelectHelper")]
        public int Helper { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                 ErrorMessageResourceName = "Please_select_departure_time_to_all_pick_point")]
        public TimeSpan[] DepartureTime { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                 ErrorMessageResourceName = "Please_select_leaving_time_to_all_pick_point")]
        public TimeSpan[] LeavingTime { get; set; }

        //[Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
        //         ErrorMessageResourceName = "PleaseSelectHelper")]
        public int TotalCount { get; set; }

        ////[Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
        ////         ErrorMessageResourceName = "PleaseSelectHelper")]
        public string[] PickPointId { get; set; }
        public bool Status { get; set; }
        public string rowId { get; set; }
        public string Edit_txt_SchoolInTime { get; set; }
        public string Edit_txt_SchoolOutTime { get; set; }
        public string Edit_txt_ParkingStartTime { get; set; }
        public string Edit_txt_ParkingEndTime { get; set; }
       



         //public string txt_RouteName { get; set; }
         //public int RegisterNumber { get; set; }
         //public int Destination { get; set; }
         //public TimeSpan txt_SchoolInTime { get; set; }
         //public TimeSpan txt_SchoolOutTime { get; set; }
         //public TimeSpan txt_ParkingStartTime { get; set; }
         //public TimeSpan txt_ParkingEndTime { get; set; }
         //public int Driver { get; set; }
         //public int Helper { get; set; }
         //public int TotalCount { get; set; }
         //public string[] PickPointId { get; set; }
         //public TimeSpan[] DepartureTime { get; set; }
         //public TimeSpan[] LeavingTime { get; set; }
         //public bool Status { get; set; }
         //public string rowId { get; set; }
         //public string Edit_txt_SchoolInTime { get; set; }
         //public string Edit_txt_SchoolOutTime { get; set; }
         //public string Edit_txt_ParkingStartTime { get; set; }
         //public string Edit_txt_ParkingEndTime { get; set; }
        
    }
}