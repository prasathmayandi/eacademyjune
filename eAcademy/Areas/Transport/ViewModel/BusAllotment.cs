﻿
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Areas.Transport.ViewModel
{
    public class BusAllotment
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int academicYearId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "PleaseSelectClass")]
        public int classId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "PleaseSelectSection")]
        public int sectionId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "PleaseSelectStudent")]
        public int studentId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_select_destination")]
        public string destination_Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_select_pickpoint")]
        public string pickpoint_Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_route_name")]
        public string Route_Id { get; set; }


        //public int academicYearId { get; set; }
        //public int classId { get; set; }
        //public int sectionId { get; set; }
        //public int studentId { get; set; }
        //public string destination_Id { get; set; }
        //public string pickpoint_Id { get; set; }
        //public string Route_Id { get; set; }
    }
}