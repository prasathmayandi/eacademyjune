﻿using System;
using System.ComponentModel.DataAnnotations;
namespace eAcademy.Areas.Transport.ViewModel
{
    public class AddVehicle
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
               ErrorMessageResourceName = "Please_enter_registration_number")]
        public string txt_RegNumber { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_registration_date")]
        public DateTime txt_RegDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_owner_first_name")]
        public string txt_OwnerFirstName { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_owner_last_name")]
        public string txt_OwnerLastName { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_address_Line1")]
        public string txt_Address1 { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_address_Line2")]
        public string txt_Address2 { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_city")]
        public string txt_City { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_Enter_State")]
        public string txt_State { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "PleaseSelectCountry")]
        public int Country { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_pincode")]
        public string txt_Pincode { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_bought_date")]
        public DateTime txt_BoughtOn { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_select_vehicle_type")]
        public int Vehicle { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_manufacture_name")]
        public string txt_Manufacture { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_color")]
        public string txt_Color { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_engine_number")]
        public string txt_EngineNum { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_chase_number")]
        public string txt_ChaseNum { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_select_fuel_type")]
        public int FuelType { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_fuel_capacity")]
        public string txt_FuelCapacity { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_seat_capacity")]
        public int txt_SeatCapacity { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_register_upto_date")]
        public DateTime txt_RegisterUptoDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                 ErrorMessageResourceName = "Please_enter_tax_valid_upto")]
        public string txt_TaxValidUpto { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_insurance_number")]
        public string txt_InsuranceNum { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_insurance_valid_from_date")]
        public DateTime txt_InsuranceValidFromDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_insurance_valid_upto_date")]
        public DateTime txt_InsuranceValidUpto { get; set; }


        public string txt_Reg_Date { get; set; }
        public string txt_Bought_On_Date { get; set; }
        public string txt_RegisterUpto_Date { get; set; }
        public string txt_InsuranceValidFrom_Date { get; set; }
        public string txt_InsuranceValid_Upto_Date { get; set; }
        public bool? Status { get; set; }
        public string Vehicle_Id { get; set; }
  


        //public string txt_RegNumber { get; set; }
        //public DateTime txt_RegDate { get; set; }
        //public string txt_OwnerFirstName { get; set; }
        //public string txt_OwnerLastName { get; set; }
        //public string txt_Address1 { get; set; }
        //public string txt_Address2 { get; set; }
        //public string txt_City { get; set; }
        //public string txt_State { get; set; }
        //public int Country { get; set; }
        //public string txt_Pincode { get; set; }
        //public DateTime txt_BoughtOn { get; set; }
        //public int Vehicle { get; set; }
        //public string txt_Manufacture { get; set; }
        //public string txt_Color { get; set; }
        //public string txt_EngineNum { get; set; }
        //public string txt_ChaseNum { get; set; }
        //public int FuelType { get; set; }
        //public string txt_FuelCapacity { get; set; }
        //public int txt_SeatCapacity { get; set; }
        //public DateTime txt_RegisterUptoDate { get; set; }
        //public string txt_TaxValidUpto { get; set; }
        //public string txt_InsuranceNum { get; set; }
        //public DateTime txt_InsuranceValidFromDate { get; set; }
        //public DateTime txt_InsuranceValidUpto { get; set; }
        //public string txt_Reg_Date { get; set; }
        //public string txt_Bought_On_Date { get; set; }
        //public string txt_RegisterUpto_Date { get; set; }
        //public string txt_InsuranceValidFrom_Date { get; set; }
        //public string txt_InsuranceValid_Upto_Date { get; set; }
        //public bool? Status { get; set; }
        //public string Vehicle_Id { get; set; }
  
    }
}