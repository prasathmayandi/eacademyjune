﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.Areas.Transport.ViewModel
{
    public class CollectFee
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
               ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int academicYearId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "PleaseSelectClass")]
        public int classId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "PleaseSelectSection")]
        public int sectionId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "PleaseSelectStudent")]
        public int studentId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_enter_destination_name")]
        public int destinationId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_select_pickpoint")]
        public int pickpointId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_select_fee_amount")]
        public Decimal FeeAmount { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                ErrorMessageResourceName = "Please_select_total_amount")]
        public Decimal AmountCollected { get; set; }


        public Decimal? FineAmount { get; set; }
        public Decimal? Discount { get; set; }
        public Decimal? VAT { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Transport.Resources.TransportResources),
                 ErrorMessageResourceName = "Please_Select_Payment_Mode")]
        public string PaymentMode { get; set; }

        public string BankName { get; set; }
        public string ChequeNumber { get; set; }
        public string BankName2 { get; set; }
        public string DDNumber { get; set; }
        public string Remark { get; set; }


        //public int academicYearId { get; set; }
        //public int classId { get; set; }
        //public int sectionId { get; set; }
        //public int studentId { get; set; }
        //public int destinationId { get; set; }
        //public int pickpointId { get; set; }
        //public Decimal FeeAmount { get; set; }
        //public Decimal AmountCollected { get; set; }
        //public Decimal? FineAmount { get; set; }
        //public Decimal? Discount { get; set; }
        //public Decimal? VAT { get; set; }
        //public string PaymentMode { get; set; }
        //public string BankName { get; set; }
        //public string ChequeNumber { get; set; }
        //public string BankName2 { get; set; }
        //public string DDNumber { get; set; }
        //public string Remark { get; set; }
    }
}