﻿using eAcademy.Areas.Transport.Models;
using eAcademy.Areas.Transport.ViewModel;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
namespace eAcademy.Areas.Transport.Services
{
    public class TransportMappingServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public IEnumerable<Object> GetRouteList()
        {
            var FuelList = (from a in dc.TransportMappings
                            where a.status == true
                            select new
                            {
                                Id = a.RouteId,
                                RouteNumber = a.RouteNumber
                            }).ToList().AsEnumerable();
            return FuelList;
        }
        
        public TransportMapping IsRootnumExist(string rootname)
        {
            var row = (from a in dc.TransportMappings where a.RouteNumber == rootname select a).FirstOrDefault();
            return row;
        }
        public void addMapping(string txt_RouteName, int RegisterNumber, int Destination, TimeSpan txt_SchoolInTime, TimeSpan txt_SchoolOutTime,TimeSpan txt_ParkingStartTime, TimeSpan txt_ParkingEndTime, 
          int Driver,int Helper,int AdminId )
        {
            DateTime date = DateTimeByZone.getCurrentDate();
            TransportMapping add = new TransportMapping()
            {
               RouteNumber = txt_RouteName,
               RegistrationNumberId = RegisterNumber,
               Destination = Destination,
               SchoolArrivalTime = txt_SchoolInTime,
               SchoolOutTime = txt_SchoolOutTime,
               StartTimeFromParking = txt_ParkingStartTime,
               ParkingReachTime = txt_ParkingEndTime,
               DriverId = Driver,
               HelperId = Helper,
               CreatedBy = AdminId,
               CreatedDate = date,
               status = true
            };
            dc.TransportMappings.Add(add);
            dc.SaveChanges();
        }
        public TransportMapping getRouteId()
        {
            var row = (from a in dc.TransportMappings orderby a.RouteId descending select a).FirstOrDefault();
            return row;
        }
        public List<RouteList>getRouteList()
        {
            var List = (from a in dc.TransportMappings
                        from b in dc.TransportDestinations
                        from c in dc.TransportTimings
                        from d in dc.TransportPickPoints
                        from e in dc.VehicleDetails
                        where a.Destination == b.DestinationId &&
                        a.RouteId == c.RouteId &&
                        c.PickPointId == d.Id &&
                        a.RegistrationNumberId == e.Id 
                        select new 
                        {
                            RegistrationNumber = e.RegistrationNumber,
                            RouteName = a.RouteNumber,
                            Destination = b.DestinationName,
                            RouteId = a.RouteId,
                            status = a.status
                        }).Distinct().ToList().Select(a => new RouteList {
                            Id = QSCrypt.Encrypt(a.RouteId.ToString()),
                            RegistrationNumber = a.RegistrationNumber,
                            RouteName = a.RouteName,
                            Destination = a.Destination,
                            PickPoints = getpickPoint(a.RouteId),
                            status = GenerateExcel.statusValue(a.status)
                        }).Distinct().ToList();
            return List;
        }
     
        public string getpickPoint(int RouteId)
        {
            var list = (from a in dc.TransportTimings
                        from b in dc.TransportPickPoints
                        where a.PickPointId == b.Id &&
                        a.RouteId == RouteId &&
                        a.status == true
                        select new
                        {
                            PickPoint = b.PickPointName
                        }).ToList();
            string[] PPName = new string[list.Count];
            for (int i = 0; i < list.Count; i++)
            {
                PPName[i] = list[i].PickPoint;
            }
            string PPNameList = string.Join(",", PPName);
            return PPNameList;
        }
        public List<RouteList> getRouteList(int VehicleRegId)
        {
            //var List = (from a in dc.TransportMappings
            //            from b in dc.TransportDestinations
            //            from c in dc.TransportTimings
            //            from d in dc.TransportPickPoints
            //            from e in dc.VehicleDetails
            //            where a.Destination == b.DestinationId &&
            //            a.RouteId == c.RouteId &&
            //            a.Destination == d.DestinationId &&
            //            c.PickPointId == d.Id &&
            //            a.RegistrationNumberId == e.Id &&
            //            a.RegistrationNumberId == VehicleRegId
            //            select new 
            //            {
            //                RegistrationNumber = e.RegistrationNumber,
            //                RouteName = a.RouteNumber,
            //                Destination = b.DestinationName,
            //                RouteId = a.RouteId,
            //                status = a.status
            //            }).ToList().Select(a => new RouteList
            //            {
            //                RegistrationNumber = a.RegistrationNumber,
            //                RouteName = a.RouteName,
            //                Destination = a.Destination,
            //                PickPoints = getpickPoint(a.RouteId),
            //                status = GenerateExcel.statusValue(a.status)
            //            }).ToList();

            var List = (from a in dc.TransportMappings
             from b in dc.TransportDestinations
             from c in dc.TransportTimings
             from d in dc.TransportPickPoints
             from e in dc.VehicleDetails
             where a.Destination == b.DestinationId &&
             a.RouteId == c.RouteId &&
             c.PickPointId == d.Id &&
             a.RegistrationNumberId == e.Id &&
             a.RegistrationNumberId == VehicleRegId
             select new
             {
                 RegistrationNumber = e.RegistrationNumber,
                 RouteName = a.RouteNumber,
                 Destination = b.DestinationName,
                 RouteId = a.RouteId,
                 status = a.status
             }).Distinct().ToList().Select(a => new RouteList
             {
                 Id = QSCrypt.Encrypt(a.RouteId.ToString()),
                 RegistrationNumber = a.RegistrationNumber,
                 RouteName = a.RouteName,
                 Destination = a.Destination,
                 PickPoints = getpickPoint(a.RouteId),
                 status = GenerateExcel.statusValue(a.status)
             }).Distinct().ToList();
            return List;
        }
        public AddMapping getRowDetail(int id)
        {
            var row = (from a in dc.TransportMappings
                       where a.RouteId == id
                       select new AddMapping
                       {
                           txt_RouteName = a.RouteNumber,
                           RegisterNumber = a.RegistrationNumberId.Value,
                           Destination = a.Destination.Value,
                           txt_SchoolInTime = a.SchoolArrivalTime.Value,
                           txt_SchoolOutTime = a.SchoolOutTime.Value,
                           txt_ParkingStartTime = a.StartTimeFromParking.Value,
                           txt_ParkingEndTime = a.ParkingReachTime.Value,
                           Driver = a.DriverId.Value,
                           Helper = a.HelperId.Value,
                           Status  = a.status.Value
                       }).ToList().Select(a => new AddMapping
                       {
                           txt_RouteName = a.txt_RouteName,
                           RegisterNumber = a.RegisterNumber,
                           Destination = a.Destination,
                           Edit_txt_SchoolInTime = a.txt_SchoolInTime.ToString(@"hh\:mm"),
                           Edit_txt_SchoolOutTime = a.txt_SchoolOutTime.ToString(@"hh\:mm"),
                           Edit_txt_ParkingStartTime = a.txt_ParkingStartTime.ToString(@"hh\:mm"),
                           Edit_txt_ParkingEndTime = a.txt_ParkingEndTime.ToString(@"hh\:mm"),
                           Driver = a.Driver,
                           Helper = a.Helper,
                           Status = a.Status
                       }).FirstOrDefault();
            return row;
        }
        public TransportMapping IsRowExist(int id)
        {
            var row = (from a in dc.TransportMappings where a.RouteId == id select a).FirstOrDefault();
            return row;
        }
        public TransportMapping Registernumber_Exist(int id)
        {
            var registernumberexist = (from a in dc.TransportMappings where a.RegistrationNumberId == id select a).FirstOrDefault();
            return registernumberexist;
        }
        public TransportMapping RouteNoExist(int id, string RouteNo)
        {
            var row = (from a in dc.TransportMappings where a.RouteId != id && a.RouteNumber == RouteNo select a).FirstOrDefault();
            return row;
        }
        public void updateMapping(int id, string txt_RouteName,int RegisterNumber,int Destination, TimeSpan txt_SchoolInTime, TimeSpan txt_SchoolOutTime,TimeSpan txt_ParkingStartTime,TimeSpan txt_ParkingEndTime, int Driver,
                     int Helper, int AdminId, bool Status)
        {
            var row = (from a in dc.TransportMappings where a.RouteId == id select a).FirstOrDefault();
            row.RouteNumber = txt_RouteName;
            row.RegistrationNumberId = RegisterNumber;
            row.Destination = Destination;
            row.SchoolArrivalTime = txt_SchoolInTime;
            row.SchoolOutTime = txt_SchoolOutTime;
            row.StartTimeFromParking = txt_ParkingStartTime;
            row.ParkingReachTime = txt_ParkingEndTime;
            row.DriverId = Driver;
            row.HelperId = Helper;
            row.UpdatedBy = AdminId;
            row.UpdatedDate = DateTimeByZone.getCurrentDateTime();
            row.status = Status;
            dc.SaveChanges();
        }
        
        public TransportMapping getRouteName(int destinationId)
        {
            var row = (from a in dc.TransportMappings where a.Destination == destinationId select a).FirstOrDefault();
            return row;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}