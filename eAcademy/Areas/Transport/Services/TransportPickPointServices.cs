﻿using eAcademy.Areas.Transport.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
namespace eAcademy.Areas.Transport.Services
{
    public class TransportPickPointServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        DateTime date = DateTimeByZone.getCurrentDate();
        public void addPickPoint(int DestinationId,string PicpointName, string Distance, decimal Amount,int markedby)
        {
            TransportPickPoint add = new TransportPickPoint()
            {
                DestinationId = DestinationId,
                PickPointName = PicpointName,
                Distance = Distance,
                Amount = Amount,
                status = true,
                CreatedBy = markedby,
                CreatedDate = date
            };
            dc.TransportPickPoints.Add(add);
            dc.SaveChanges();
        }
        public void updatePickPoint(int DestinationId,int PicPointid,string PickPointName,string Distance,decimal Amount)
        {
            var row = (from a in dc.TransportPickPoints where a.DestinationId == DestinationId && a.Id == PicPointid select a).FirstOrDefault();
            row.PickPointName = PickPointName;
            row.Distance = Distance;
            row.Amount = Amount;
            dc.SaveChanges();
        }
        public List<PickPointList> getPickPointListByDestination(int destinationId)
        {
            var list = (from a in dc.TransportPickPoints
                        where a.DestinationId == destinationId
                        select new
                        {
                            id = a.Id,
                            PickPoint = a.PickPointName,
                        }).ToList().Select(a => new PickPointList
                        {
                            id = QSCrypt.Encrypt(a.id.ToString()),
                            PickPoint = a.PickPoint,
                        }).ToList();
            
            return list;
        }
        public List<PickPointList> getPickPointListByMappingRoute(int RouteId)
        {
            var list = (from a in dc.TransportTimings
                        from b in dc.TransportPickPoints
                        where a.PickPointId == b.Id &&
                        a.RouteId == RouteId &&
                        a.status == true
                        select new
                        {
                            id = a.PickPointId,
                            PickPoint = b.PickPointName,
                            DepartureTime = a.DepartureTime,
                            LeavingTime = a.DepartureTime
                        }).ToList().Select(a => new PickPointList
                        {
                            id = QSCrypt.Encrypt(a.id.ToString()),
                            PickPoint = a.PickPoint,
                            DepartureTime = a.DepartureTime.Value.ToString(@"hh\:mm"),
                            LeavingTime = a.LeavingTime.Value.ToString(@"hh\:mm")
                        }).ToList();
            return list;
        }
        public TransportDestination checkDestinationExist(string DestinationName)
        {
            var row = (from a in dc.TransportDestinations where a.DestinationName == DestinationName select a).FirstOrDefault();
            return row;
        }
        public IEnumerable<Object> getPickPoints(int DestinationId)
        {
            var list = (from a in dc.TransportPickPoints
                        where a.DestinationId == DestinationId &&
                        a.status == true
                        select new
                        {
                            PickPointId = a.Id,
                            PickPointName = a.PickPointName
                        }).ToList();
            return list;
        }
      
        public TransportPickPoint getPickPointAmount(int id)
        {
            var row = (from a in dc.TransportPickPoints where a.Id == id select a).FirstOrDefault();
            return row;
        }

        public void DeletePickPoint(int pickPointId)
        {
            var row = (from a in dc.TransportPickPoints where a.Id == pickPointId select a).FirstOrDefault();
            dc.TransportPickPoints.Remove(row);
            dc.SaveChanges();
        }

        public string  GetPickPoint(int DestinationId,int pickpointId)
        {
            var list = (from a in dc.TransportDestinations
                        from b in dc.TransportPickPoints
                        where b.DestinationId == DestinationId && b.Id == pickpointId && a.DestinationId == b.DestinationId && b.status == true
                        select b).FirstOrDefault();
            if(list!=null)
            {
                return list.PickPointName;
            }
            else
            {
                return "";
            }
        }

        public GetFeeStructureTransportHostel gettransfortfeestructure(int? TransportDesignation, int? TransportPickpoint)
        {
            var res = (from a in dc.TransportPickPoints
                       from b in dc.TransportDestinations
                       where b.DestinationId == a.DestinationId && a.DestinationId == TransportDesignation && a.Id == TransportPickpoint
                       select new
                       {
                           DestinationId = a.DestinationId,
                           Id = a.Id,
                           TransportAmount = a.Amount,
                           Destinationname = b.DestinationName,
                           PickPointName = a.PickPointName
                       }).ToList().Select(s => new GetFeeStructureTransportHostel
                       {
                           DestinationId = s.DestinationId,
                           Id = s.Id,
                            DestinationName=s.Destinationname,
                            Pickpoint=s.PickPointName,
                            TransportFee=s.TransportAmount
                       }).FirstOrDefault();
            return res;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}