﻿using eAcademy.Areas.Transport.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Areas.Transport.Services
{
    public class TransportFeesCollectionServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public int addFee(int academicYearId, int classId, int sectionId, int studentId, int destinationId, int pickpointId, Decimal FeeAmount, Decimal AmountCollected, Decimal Fine,
            Decimal? Discount, Decimal VAT, string PaymentMode, string BankName, string ChequeNumber, string DDNumber, string Remark,int adminId)
        {
            DateTime date = DateTimeByZone.getCurrentDate();
            TransportFeesCollection add = new TransportFeesCollection()
            {

                CollectionDate = date,
                AcademicYearId = academicYearId,
                ClassId = classId,
                SectionId = sectionId,
                StudentRegisterId = studentId,
                DestinationId = destinationId,
                PickPointId = pickpointId,
                FeeAmount = FeeAmount,
                AmountCollected = AmountCollected,
                Fine = Fine,
                Discount = Discount,
                VAT = VAT,
                PaymentMode = PaymentMode,
                BankName = BankName,
                ChequeNumber = ChequeNumber,
                DDNumber = DDNumber,
                Remark = Remark,
                CreatedBy = adminId,
                Status = true,
                CreatedDate = date
            };

            db.TransportFeesCollections.Add(add);
            db.SaveChanges();

            return add.FeeCollectionId;
        }

        public List<TransportFeesCollection> getFeeCollection()
        {
            var row = (from a in db.TransportFeesCollections select a).ToList();
            return row;
        }

        public int getFeeCollectionOrderByFeeCollectionID()
        {
            var ans = db.TransportFeesCollections.OrderByDescending(q => q.FeeCollectionId).Select(s => s.FeeCollectionId).First();
            return ans;
        }

        public StudentTransportDetail PaidStudentDetail(int passYearId, int PassClassId, int PassSecId, int PassStuRegId)
        {
            var row = (from a in db.TransportFeesCollections
                       where a.AcademicYearId == passYearId &&
                       a.ClassId == PassClassId &&
                       a.SectionId == PassSecId &&
                       a.StudentRegisterId == PassStuRegId
                       select new
                       {
                           DestinationId = a.DestinationId,
                           PickPointId = a.PickPointId,
                           FeeAmount = a.FeeAmount,
                           Discount = a.Discount,
                           LateFee = a.Fine,
                           Vat = a.VAT,
                           TotalAmount = a.AmountCollected,
                           PaymentMode = a.PaymentMode,
                           BankName = a.BankName,
                           ChequeNo = a.ChequeNumber,
                           DDno = a.DDNumber,
                           Remark = a.Remark

                       }).ToList().Select(a => new StudentTransportDetail
                            {
                                DestinationId = a.DestinationId,
                                PickPointId = a.PickPointId,
                                FeeAmount =  Math.Round(a.FeeAmount.Value, 2, MidpointRounding.AwayFromZero),
                                Discount = Math.Round(a.Discount.Value, 2, MidpointRounding.AwayFromZero),
                                LateFee = Math.Round(a.LateFee.Value, 2, MidpointRounding.AwayFromZero),
                                Vat = Math.Round(a.Vat.Value, 2, MidpointRounding.AwayFromZero),
                                TotalAmount = Math.Round(a.TotalAmount.Value, 2, MidpointRounding.AwayFromZero),
                                PaymentMode = a.PaymentMode,
                                ChequeNo = a.ChequeNo,
                                DDno = a.DDno,
                                Remark = a.Remark
                            }).FirstOrDefault();
            return row;
        }

        public TransportFeesCollection IsRecordExist(int passYearId, int PassClassId, int PassSecId, int PassStuRegId)
        {
            var row = (from a in db.TransportFeesCollections where a.AcademicYearId == passYearId && a.ClassId == PassClassId && a.SectionId == PassSecId && a.StudentRegisterId == PassStuRegId select a).FirstOrDefault();
            return row;
        }

        public int UpdateFee(int academicYearId, int classId, int sectionId, int studentId, int destinationId, int pickpointId, Decimal FeeAmount, Decimal AmountCollected, Decimal Fine,
            Decimal? Discount, Decimal VAT, string PaymentMode, string BankName, string ChequeNumber, string DDNumber, string Remark,int adminId)
        {
            DateTime date = DateTimeByZone.getCurrentDate();
            var row = (from a in db.TransportFeesCollections where a.AcademicYearId == academicYearId && a.ClassId == classId && a.SectionId == sectionId && a.StudentRegisterId == studentId select a).FirstOrDefault();
            row.DestinationId = destinationId;
            row.PickPointId = pickpointId;
            row.FeeAmount = FeeAmount;
            row.AmountCollected = AmountCollected;
            row.Fine = Fine;
            row.Discount = Discount;
            row.VAT = VAT;
            row.PaymentMode = PaymentMode;
            row.BankName = BankName;
            row.ChequeNumber = ChequeNumber;
            row.DDNumber = DDNumber;
            row.Remark = Remark;
            row.UpdatedBy = adminId;
            row.UpdatedDate = date;
            db.SaveChanges();

            return row.FeeCollectionId;
        }

        public StudentTransportDetail getStudentTransportDetail(int passYearId, int passClassId, int passSecId, int passStudentId)
        {
            var row = (from a in db.TransportFeesCollections
                       from b in db.TransportDestinations
                       from c in db.TransportPickPoints
                     
                       where a.DestinationId == b.DestinationId &&
                     
                       a.PickPointId == c.Id &
                           a.AcademicYearId == passYearId &&
                           a.ClassId == passClassId &&
                           a.SectionId == passSecId &&
                           a.StudentRegisterId == passStudentId
                       select new 
                       {
                           DestinationName = b.DestinationName,
                           PickPointName = c.PickPointName,
                     
                           destination_Id = b.DestinationId,
                           PickPoint_Id = c.Id
                       }).ToList().Select(a=>new StudentTransportDetail
                           {
                               DestinationId1 = a.destination_Id,
                               DestinationName = a.DestinationName,
                               PickPointName = a.PickPointName,
                               destination_Id =  QSCrypt.Encrypt(a.destination_Id.ToString()),
                               PickPoint_Id = QSCrypt.Encrypt(a.PickPoint_Id.ToString()),
                           }).FirstOrDefault();
            return row;
        }

        public StudentTransportDetail GetFeeCollectiondetails(int id)
        {
            var ans=(from a in db.TransportFeesCollections
                     from b in db.AcademicYears
                     from c in db.Classes
                     from d in db.Sections
                     from e in db.Students
                     from f in db.AssignClassToStudents
                     from g in db.StudentRollNumbers
                     from h in db.TransportDestinations
                     from i in db.TransportPickPoints
                     where a.FeeCollectionId ==id && e.StudentRegisterId == a.StudentRegisterId && f.StudentRegisterId == a.StudentRegisterId && f.Status == true && g.StudentRegisterId ==a.StudentRegisterId && g.Status==true && b.AcademicYearId == a.AcademicYearId && c.ClassId ==a.ClassId && d.SectionId == a.SectionId && h.DestinationId == a.DestinationId && i.Id==a.PickPointId
                     select new StudentTransportDetail
                     {
                         txt_AcademicYear = b.AcademicYear1,
                         txt_Class = c.ClassType,
                         txt_Section = d.SectionName,
                         StudentName = e.FirstName + " " + e.LastName,
                         StudentRollNumber = g.RollNumber,
                         StudentId = e.StudentId,
                         ReceiptNo = a.FeeCollectionId,
                         old_FeeAmount=a.FeeAmount,
                         old_TotalAmount=a.AmountCollected,
                         old_LateFee=a.Fine,
                         old_Vat=a.VAT,
                         old_Discount=a.Discount,
                         DestinationName=h.DestinationName,
                         PickPointName=i.PickPointName


                     }).Distinct().FirstOrDefault();
            return ans;
        }



        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}