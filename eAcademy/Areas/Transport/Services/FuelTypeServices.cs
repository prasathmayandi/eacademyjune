﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
namespace eAcademy.Services
{
    public class FuelTypeServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        public IEnumerable<Object> ShowFuelTypes()
        {
            var FuelList = (from a in db.FuelTypes
                            select new
                            {
                                FuelId = a.Id,
                                FuelType = a.FuelType1
                            }).ToList().AsEnumerable();
            return FuelList;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}