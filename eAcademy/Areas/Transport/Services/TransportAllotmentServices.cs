﻿using eAcademy.Areas.Transport.Models;
using eAcademy.Areas.Transport.ViewModel;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
namespace eAcademy.Areas.Transport.Services
{
    public class TransportAllotmentServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        DateTime date = DateTimeByZone.getCurrentDate();
        public void allotStudent(int academicYearId, int classId, int sectionId, int studentId, int destinationId, int pickpointId, int routeId, int AdminId)
        {
            TransportAllotment add = new TransportAllotment()
            {
              AcademicYearId = academicYearId,
              ClassId = classId,
              SectionId = sectionId,
              StudentRegisterId = studentId,
              DestinationId = destinationId,
              PickPointId = pickpointId,
              RouteMapId = routeId,
             CreatedBy = AdminId,
                Status = true,
              CreatedDate = date
            };
            db.TransportAllotments.Add(add);         
            db.SaveChanges();
        }
        public List<StudentTransportList> getTransportStudentList()
        {
            var list = (from a in db.TransportAllotments
                        from b in db.Students
                        from c in db.AcademicYears
                        from d in db.Classes
                        from e in db.Sections
                        from f in db.TransportDestinations
                        from g in db.TransportPickPoints
                        from h in db.TransportMappings

                        where a.StudentRegisterId == b.StudentRegisterId &&
                        a.AcademicYearId == c.AcademicYearId &&
                        a.ClassId == d.ClassId &&
                        a.SectionId == e.SectionId &&
                        a.DestinationId == f.DestinationId &&
                        a.PickPointId == g.Id &&
                        a.RouteMapId == h.RouteId
                        select new StudentTransportList
                        {
                            StudentName = b.FirstName+" "+b.LastName,
                            Year = c.AcademicYear1,
                            Class = d.ClassType+" "+e.SectionName,
                            Destination = f.DestinationName,
                            PickPoint = g.PickPointName, 
                            Id = a.TransportAllotmentId,
                            RouteNumber = h.RouteNumber,
                            Status = a.Status
                        }).ToList();
            return list;
        }
        public List<StudentTransportList> getTransportStudentList(int RouteMapId)
        {
            var list = (from a in db.TransportAllotments
                        from b in db.Students
                        from c in db.AcademicYears
                        from d in db.Classes
                        from e in db.Sections
                        from f in db.TransportDestinations
                        from g in db.TransportPickPoints
                        from h in db.TransportMappings
                        where a.StudentRegisterId == b.StudentRegisterId &&
                        a.AcademicYearId == c.AcademicYearId &&
                        a.ClassId == d.ClassId &&
                        a.SectionId == e.SectionId &&
                        a.DestinationId == f.DestinationId &&
                        a.PickPointId == g.Id &&
                        a.RouteMapId == h.RouteId &&
                        a.RouteMapId == RouteMapId
                        
                        select new StudentTransportList
                        {
                            StudentName = b.FirstName + " " + b.LastName,
                            Year = c.AcademicYear1,
                            Class = d.ClassType + " " + e.SectionName,
                            Destination = f.DestinationName,
                            PickPoint = g.PickPointName,
                            Id = a.TransportAllotmentId,
                            RouteNumber = h.RouteNumber,
                            Status = a.Status
                        }).ToList();
            return list;
        }
        public TransportAllotment IsRecordExist(int academicYearId, int classId, int sectionId, int studentId)
        {
            var row = (from a in db.TransportAllotments where a.AcademicYearId == academicYearId && a.ClassId == classId && a.SectionId == sectionId && a.StudentRegisterId == studentId select a).FirstOrDefault();
            return row;
        }
        public TransportAllotment IsDestinationUsed(int DestinationId)
        {
            var row = (from a in db.TransportAllotments where a.DestinationId == DestinationId select a).FirstOrDefault();
            return row;
        }
        public EditTransportAllotment Edit_transportallotment(int id)
        {
            var Edit_Allotment = (from a in db.TransportAllotments
                                  from b in db.AcademicYears
                                  from c in db.Classes
                                  from d in db.Sections
                                  from e in db.TransportPickPoints
                                  from f in db.TransportMappings
                                  from g in db.Students
                                  from h in db.TransportDestinations
                                  where a.TransportAllotmentId == id && a.SectionId == d.SectionId && a.ClassId == c.ClassId && a.AcademicYearId == b.AcademicYearId
                                  && a.PickPointId == e.Id && f.RouteId == a.RouteMapId && a.StudentRegisterId == g.StudentRegisterId && h.DestinationId == a.DestinationId
                                  select new EditTransportAllotment
                                  {
                                      TransportAllotmentId = a.TransportAllotmentId,
                                      academicYearId = a.AcademicYearId,
                                      AcademicYear = b.AcademicYear1,
                                      classId = a.ClassId,
                                      Class = c.ClassType,
                                      destination_Id = a.DestinationId,
                                      Destinaion = h.DestinationName,
                                      pickpoint_Id = a.PickPointId,
                                      Pickpoint = e.PickPointName,
                                      Route_Id = a.RouteMapId,
                                      Route = f.RouteNumber,
                                      sectionId = a.SectionId,
                                      Section = d.SectionName,
                                      studentId = a.StudentRegisterId,
                                      Student = g.FirstName,
                                      Status = a.Status
                                  }).FirstOrDefault();
            return Edit_Allotment;
        }
        public void Update_transportallotments(int TransportAllotmentId, int? academicYearId, int? classId, int? sectionId, int? studentId, int? destinationId, int? pickpointId, int? routeId, int AdminId, bool? status)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var row = (from a in db.TransportAllotments where a.TransportAllotmentId == TransportAllotmentId select a).FirstOrDefault();
           // row.TransportAllotmentId = TransportAllotmentId;
            row.StudentRegisterId = studentId;
            row.AcademicYearId = academicYearId;
            row.ClassId = classId;
            row.SectionId =sectionId;
            row.DestinationId = destinationId;
            row.PickPointId = pickpointId;
            row.RouteMapId = routeId;
            row.UpdatedDate = date;
            row.UpdatedBy = AdminId;
            row.Status = status;
            db.SaveChanges();
        }
        public TransportAllotment IsPickPointUsed(int pickPointId)
        {
            var row = (from a in db.TransportAllotments where a.PickPointId == pickPointId && a.Status == true select a).FirstOrDefault();
            return row;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}