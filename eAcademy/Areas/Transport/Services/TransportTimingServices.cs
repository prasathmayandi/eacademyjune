﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Linq;
namespace eAcademy.Areas.Transport.Services
{
    public class TransportTimingServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public void addPickpointTiming(int RouteId, int pickPointId, TimeSpan DepartureTime, TimeSpan ArrivalTime, int AdminId)
        {
            TransportTiming add = new TransportTiming()
            {
                RouteId = RouteId,
                PickPointId = pickPointId,
                DepartureTime = DepartureTime,
                ArrivalTime = ArrivalTime,
                CreatedBy = AdminId,
                CreatedDate = DateTimeByZone.getCurrentDateTime(),
                status = true
            };
            dc.TransportTimings.Add(add);
            dc.SaveChanges();
        }
        public void UpdateRouteTiming(int id, int PickPointId, TimeSpan DepartureTime, TimeSpan LeavingTime, int AdminId) 
        {
            var row = (from a in dc.TransportTimings where a.RouteId == id && a.PickPointId == PickPointId select a).FirstOrDefault();
            if (row != null)
            {
                row.DepartureTime = DepartureTime;
                row.ArrivalTime = LeavingTime;
                row.UpdatedBy = AdminId;
                row.UpdatedDate = DateTimeByZone.getCurrentDateTime();
                row.status = true;
                dc.SaveChanges();
            }
            else
            {
                TransportTiming add = new TransportTiming()
                {
                    RouteId = id,
                    PickPointId = PickPointId,
                    DepartureTime = DepartureTime,
                    ArrivalTime = LeavingTime,
                    CreatedBy = AdminId,
                    CreatedDate = DateTimeByZone.getCurrentDateTime(),
                    status = true
                };
                dc.TransportTimings.Add(add);
                dc.SaveChanges();
            }
        }

        public void FalseUnusedPickPoint(int routeId, int[] pickPointId)
        {
            var oldPickPointList = (from a in dc.TransportTimings
                                    where a.RouteId == routeId
                                    select new
                                    {
                                        PickPointId = a.PickPointId
                                    }).ToList();

            int size = oldPickPointList.Count;
            int[] PickPoint = new int[size];
            for (int i = 0; i < oldPickPointList.Count; i++)
            {
                int pickpointId = oldPickPointList[i].PickPointId.Value;
                PickPoint[i] = pickpointId;
            }

            var UnUsedPickPoints = PickPoint.Except(pickPointId).ToList();

            for (int i = 0; i < UnUsedPickPoints.Count; i++)
            {
                int id = UnUsedPickPoints[i];
                var row = (from a in dc.TransportTimings where a.PickPointId == id && a.RouteId == routeId select a).FirstOrDefault();
                row.status = false;
                row.UpdatedDate = DateTimeByZone.getCurrentDateTime();
                dc.SaveChanges();
            }
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}