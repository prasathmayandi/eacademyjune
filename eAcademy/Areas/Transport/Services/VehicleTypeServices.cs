﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
namespace eAcademy.Services
{
    public class VehicleTypeServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        public IEnumerable<Object> ShowVehicleTypes()
        {
            var VehicleTypeList = (from a in db.VehicleTypes
                            select new
                            {
                                VehicleId = a.Id,
                                VehicleType = a.VehicleType1
                            }).ToList().AsEnumerable();
            return VehicleTypeList;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}