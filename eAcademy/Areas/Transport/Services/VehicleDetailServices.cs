﻿using eAcademy.Areas.Transport.Models;
using eAcademy.Areas.Transport.ViewModel;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
namespace eAcademy.Areas.Transport.Services
{
    public class VehicleDetailServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        DateTime date = DateTimeByZone.getCurrentDate();
        public void addVehicle(string txt_RegNumber, DateTime txt_RegDate, string txt_OwnerFirstName, string txt_OwnerLastName, string txt_Address1, string txt_Address2, string txt_City, string txt_State, int Country, string txt_Pincode,
            DateTime txt_BoughtOn, int Vehicle, string txt_Manufacture,
                        string txt_Color, string txt_EngineNum, string txt_ChaseNum, int FuelType, string txt_FuelCapacity, int txt_SeatCapacity, DateTime txt_RegisterUptoDate, string txt_TaxValidUpto, string txt_InsuranceNum,
            DateTime txt_InsuranceValidFromDate,
                        DateTime txt_InsuranceValidUpto, int adminId)

        {
            VehicleDetail add = new VehicleDetail()
            {
                RegistrationNumber = txt_RegNumber,
                RegistrationDate = txt_RegDate,
                OwnerFirstName = txt_OwnerFirstName,
                OwnerLastName = txt_OwnerLastName,
                Address1 = txt_Address1,
                Address2 = txt_Address2,
                City = txt_City,
                Country = Country,
                Pincode = txt_Pincode,
                BoughtOn = txt_BoughtOn,
                VehicleType = Vehicle,
                Manufacture = txt_Manufacture,
                Color = txt_Color,
                EngineNumber = txt_EngineNum,
                txt_ChaseNum = txt_ChaseNum,
                FuelType = FuelType,
                FuelCapacity = txt_FuelCapacity,
                SeatCapacity = txt_SeatCapacity,
                RegisterUptoDate = txt_RegisterUptoDate,
                TaxValidUpto = txt_TaxValidUpto,
                InsuranceNumber = txt_InsuranceNum,
                InsuranceValidFromDate = txt_InsuranceValidFromDate,
                InsuranceValidUpto = txt_InsuranceValidUpto,
                CreatedBy = adminId,
                CreatedDate = date,
                status = true,
                State = txt_State
            };
            db.VehicleDetails.Add(add);
            db.SaveChanges();
        }
        public List<VehicleList> getVehicleList()
        {
            var list = (from a in db.VehicleDetails
                        from b in db.VehicleTypes
                        from c in db.FuelTypes
                        where a.VehicleType == b.Id &&
                        a.FuelType == c.Id
                        select new
                        {
                            RegistrationNumber = a.RegistrationNumber,
                            VehicleType = b.VehicleType1,
                            Manufactured = a.Manufacture,
                            Color = a.Color,
                            SeatCapacity = a.SeatCapacity.Value,
                            Id = a.Id,
                            status = a.status
                        }).ToList().Select(a => new VehicleList
                        {
                            RegistrationNumber = a.RegistrationNumber,
                            VehicleType = a.VehicleType,
                            Manufactured = a.Manufactured,
                            Color = a.Color,
                            SeatCapacity = a.SeatCapacity,
                            status = GenerateExcel.statusValue(a.status),
                            Id = QSCrypt.Encrypt(a.Id.ToString())
                        }).ToList();
            return list;
        }
        public List<VehicleList> getVehicleList(int Id)
        {
            var list = (from a in db.VehicleDetails
                        from b in db.VehicleTypes
                        from c in db.FuelTypes
                        where a.VehicleType == b.Id &&
                    a.FuelType == c.Id &&
                    a.Id == Id
                        select new
                        {
                            RegistrationNumber = a.RegistrationNumber,
                            VehicleType = b.VehicleType1,
                            Manufactured = a.Manufacture,
                            Color = a.Color,
                            SeatCapacity = a.SeatCapacity.Value,
                            Id = a.Id,
                            status = a.status
                        }).ToList().Select(a => new VehicleList
                        {
                            RegistrationNumber = a.RegistrationNumber,
                            VehicleType = a.VehicleType,
                            Manufactured = a.Manufactured,
                            Color = a.Color,
                            SeatCapacity = a.SeatCapacity,
                            Id = QSCrypt.Encrypt(a.Id.ToString()),
                            status = GenerateExcel.statusValue(a.status)
                        }).ToList();
            return list;
        }
        public IEnumerable<Object> VehicleRegistrationNumberList()
        {
            var FuelList = (from a in db.VehicleDetails
                            where a.status == true
                            select new
                            {
                                Id = a.Id,
                                RegistrationNumber = a.RegistrationNumber
                            }).ToList().AsEnumerable();
            return FuelList;
        }
        public VehicleDetail IsDataExist(string RegistrationNumber)
        {
            var row = (from a in db.VehicleDetails where a.RegistrationNumber == RegistrationNumber select a).FirstOrDefault();
            return row;
        }
        public AddVehicle getRowDetail(int id)
        {
            var row = (from a in db.VehicleDetails
                       where a.Id == id
                       select new AddVehicle
                       {
                           txt_RegNumber = a.RegistrationNumber,
                           txt_Reg_Date = SqlFunctions.DateName("day", a.RegistrationDate).Trim()+ "/" +SqlFunctions.StringConvert((double)a.RegistrationDate.Value.Month).TrimStart()+ "/" +SqlFunctions.DateName("year", a.RegistrationDate),
                           txt_OwnerFirstName = a.OwnerFirstName,
                           txt_OwnerLastName = a.OwnerLastName,
                           txt_Address1 = a.Address1,
                           txt_Address2 = a.Address2,
                           txt_City = a.City,
                           Country = a.Country.Value,
                           txt_State = a.State,
                           txt_Pincode = a.Pincode,
                           txt_Bought_On_Date = SqlFunctions.DateName("day", a.BoughtOn).Trim()+ "/" +SqlFunctions.StringConvert((double)a.BoughtOn.Value.Month).TrimStart()+ "/" +SqlFunctions.DateName("year", a.BoughtOn),
                           Vehicle = a.VehicleType.Value,
                           txt_Manufacture = a.Manufacture,
                           txt_Color = a.Color,
                           txt_EngineNum = a.EngineNumber,
                           txt_ChaseNum = a.txt_ChaseNum,
                           FuelType = a.FuelType.Value,
                           txt_FuelCapacity = a.FuelCapacity,
                           txt_SeatCapacity = a.SeatCapacity.Value,
                           txt_RegisterUpto_Date = SqlFunctions.DateName("day", a.RegisterUptoDate).Trim()+ "/" +SqlFunctions.StringConvert((double)a.RegisterUptoDate.Value.Month).TrimStart()+ "/" +SqlFunctions.DateName("year", a.RegisterUptoDate),
                           txt_TaxValidUpto = a.TaxValidUpto,
                           txt_InsuranceNum = a.InsuranceNumber,
                           txt_InsuranceValidFrom_Date =  SqlFunctions.DateName("day", a.InsuranceValidFromDate).Trim()+ "/" +SqlFunctions.StringConvert((double)a.InsuranceValidFromDate.Value.Month).TrimStart()+ "/" +SqlFunctions.DateName("year", a.InsuranceValidFromDate),
                           txt_InsuranceValid_Upto_Date = SqlFunctions.DateName("day", a.InsuranceValidUpto).Trim()+ "/" +SqlFunctions.StringConvert((double)a.InsuranceValidUpto.Value.Month).TrimStart()+ "/" +SqlFunctions.DateName("year", a.InsuranceValidUpto),
                           Status = a.status
                       }).FirstOrDefault();
            return row;
        }
        
        public VehicleDetail IsRowExist(int id)
        {
            var row = (from a in db.VehicleDetails where a.Id == id select a).FirstOrDefault();
            return row;
        }
        public VehicleDetail RegNoExist(int id, string regNo)
        {
            var row = (from a in db.VehicleDetails where a.Id != id && a.RegistrationNumber == regNo select a).FirstOrDefault();
            return row;
        }
        public void UpdateVehicle(string txt_RegNumber, DateTime txt_RegDate, string txt_OwnerFirstName, string txt_OwnerLastName, string txt_Address1, string txt_Address2, string txt_City, string txt_State, int Country, string txt_Pincode,
            DateTime txt_BoughtOn, int Vehicle, string txt_Manufacture,
                        string txt_Color, string txt_EngineNum, string txt_ChaseNum, int FuelType, string txt_FuelCapacity, int txt_SeatCapacity, DateTime txt_RegisterUptoDate, string txt_TaxValidUpto, string txt_InsuranceNum,
            DateTime txt_InsuranceValidFromDate,
                        DateTime txt_InsuranceValidUpto, bool status, int adminId, int Id)
        {
            var row = (from a in db.VehicleDetails where a.Id == Id select a).FirstOrDefault();
            row.RegistrationNumber = txt_RegNumber;
            row.RegistrationDate = txt_RegDate;
            row.OwnerFirstName = txt_OwnerFirstName;
            row.OwnerLastName = txt_OwnerLastName;
            row.Address1 = txt_Address1;
            row.Address2 = txt_Address2;
            row.City = txt_City;
            row.State = txt_State;
            row.Country = Country;
            row.Pincode = txt_Pincode;
            row.BoughtOn = txt_BoughtOn;
            row.VehicleType = Vehicle;
            row.Manufacture = txt_Manufacture;
            row.Color = txt_Color;
            row.EngineNumber = txt_EngineNum;
            row.txt_ChaseNum = txt_ChaseNum;
            row.FuelType = FuelType;
            row.FuelCapacity = txt_FuelCapacity;
            row.SeatCapacity = txt_SeatCapacity;
            row.RegisterUptoDate = txt_RegisterUptoDate;
            row.TaxValidUpto = txt_TaxValidUpto;
            row.InsuranceNumber = txt_InsuranceNum;
            row.InsuranceValidFromDate = txt_InsuranceValidFromDate;
            row.InsuranceValidUpto = txt_InsuranceValidUpto;
            row.UpdatedBy = adminId;
            row.UpdatedDate = date;
            row.status = status;
            db.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}