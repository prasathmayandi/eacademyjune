﻿using eAcademy.Areas.Transport.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
namespace eAcademy.Areas.Transport.Services
{
    public class TransportDestinationServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public void addDestination(string DestinationName, int markedby)
        {
            DateTime date = DateTimeByZone.getCurrentDate();
            TransportDestination add = new TransportDestination()
            {
                DestinationName = DestinationName,
                status = true,
                CreatedBy = markedby,
                CreatedDate = date
            };
            dc.TransportDestinations.Add(add);
            dc.SaveChanges();
           
        }
        public TransportDestination checkDestinationExist(string DestinationName)
        {
            var row = (from a in dc.TransportDestinations where a.DestinationName == DestinationName select a).FirstOrDefault();
            return row;
        }
        public List<DestinationPickpoints> getPickPointList()
        {
            var ans = (from t1 in dc.TransportDestinations
                       select new  { 
                           DestinationId = t1.DestinationId,
                           DestinationName = t1.DestinationName, 
                           Status = t1.status
                       }).ToList().Select(a => new DestinationPickpoints
                       {
                           DestinationId = QSCrypt.Encrypt(a.DestinationId.ToString()),
                           DestinationName = a.DestinationName,
                           Status = a.Status,
                           PickPointsList = getPickPointNamesByDestination(a.DestinationId)
                       }).ToList();
            return ans;
        }

        public string getPickPointNamesByDestination(int DestinationId)
        {
            var list = (from a in dc.TransportPickPoints
                        where a.DestinationId == DestinationId &&
                        a.status == true
                        select new DestinationPickpoints
                        {
                            PickPointsList = a.PickPointName
                        }).ToList();

            string[] PickPoints = new string[list.Count];
            for (int i = 0; i < list.Count; i++)
            {
                PickPoints[i] = list[i].PickPointsList;
            }
            string floorsList = string.Join(",", PickPoints);
            return floorsList;
        }

        public PickPoints getDestination(int DestinationId)
        {
            var ans = (from t1 in dc.TransportDestinations
                       where t1.DestinationId == DestinationId
                       select new 
                       {
                           DestinationId = t1.DestinationId,
                           DestinationName = t1.DestinationName,
                           status = t1.status
                       }).ToList().Select(a => new PickPoints
                           {
                               DestinationId = QSCrypt.Encrypt(a.DestinationId.ToString()),
                               DestinationName = a.DestinationName,
                               Status = a.status,
                           }).FirstOrDefault();
            return ans;
        }
        public List<PickPoints> getPickPointListByDestination(int DestinationId)
        {
            var ans = (from t1 in dc.TransportDestinations
                       from t2 in dc.TransportPickPoints
                       where t1.DestinationId == t2.DestinationId &&
                       t1.DestinationId == DestinationId
                       select new
                       {
                           
                           PickPointId = t2.Id,
                           picpointName = t2.PickPointName,
                           Distance = t2.Distance,
                           Charge = t2.Amount
                       }).ToList().Select(a => new PickPoints
                       {
                           PickPointId = QSCrypt.Encrypt(a.PickPointId.ToString()),
                           PickPointName = a.picpointName,
                           Distance1 = Math.Round(Convert.ToDecimal(a.Distance),2,MidpointRounding.AwayFromZero),   
                           Charge = Math.Round(a.Charge.Value, 2, MidpointRounding.AwayFromZero),
                    
                       }).ToList();
            return ans;
        }
        public TransportDestination IsDestinationExist(int DestinationId, string Destination)
        {
            var row = (from a in dc.TransportDestinations where a.DestinationId != DestinationId && a.DestinationName == Destination select a).FirstOrDefault();
            return row;
        }
        public void UpdateDestination (int DestinationId, string Destination, bool status)
        {
            var row = (from a in dc.TransportDestinations where a.DestinationId == DestinationId select a).FirstOrDefault();
            row.DestinationName = Destination;
            row.status = status;
            dc.SaveChanges();
        }
        public IEnumerable<Object> DestinationList()
        {
            var FuelList = (from a in dc.TransportDestinations
                            where a.status == true
                            select new
                            {
                                DestinationId = a.DestinationId,
                                DestinationName = a.DestinationName
                            }).ToList().AsEnumerable();
            return FuelList;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
    }
}