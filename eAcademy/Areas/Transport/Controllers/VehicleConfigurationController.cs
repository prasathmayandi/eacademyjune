﻿using eAcademy.Areas.Transport.Models;
using eAcademy.Areas.Transport.Services;
using eAcademy.Areas.Transport.ViewModel;
using eAcademy.Helpers;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using eAcademy.HelperClass;
using eAcademy.Models;

namespace eAcademy.Areas.Transport.Controllers
{
    [CheckSessionOutAttribute]
    public class VehicleConfigurationController : Controller
    {
        UserActivityHelper useractivity = new UserActivityHelper();
        public int AdminId;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AdminId = Convert.ToInt32(Session["empRegId"]);
            tblCountryServices obj_country = new tblCountryServices();
            VehicleDetailServices vehicleServ = new VehicleDetailServices();
            ViewBag.Country = obj_country.CountryList();
            ViewBag.RegistrationNumber = vehicleServ.VehicleRegistrationNumberList();
           
        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (Session["CurrentCulture"] != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["CurrentCulture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["CurrentCulture"].ToString());
            }
        }
        public ActionResult Index()
        {
            FuelTypeServices fuelSer = new FuelTypeServices();
            VehicleTypeServices vehicleSer = new VehicleTypeServices();
            ViewBag.VehicleList = vehicleSer.ShowVehicleTypes();
            ViewBag.FuelList = fuelSer.ShowFuelTypes();
            return View();
        }

        public JsonResult VehicleListResults(string sidx, string sord, int page, int rows, int? VehicleRegistrationNumber)
        {
            IList<VehicleList> VehicleList1 = new List<VehicleList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            VehicleDetailServices vehicleServ = new VehicleDetailServices();
            VehicleList1 = vehicleServ.getVehicleList();
            if (VehicleRegistrationNumber != 0 && VehicleRegistrationNumber != null)
            {
                VehicleList1 = vehicleServ.getVehicleList(VehicleRegistrationNumber.Value);
            }
            int totalRecords = VehicleList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    VehicleList1 = VehicleList1.OrderByDescending(s => s.RegistrationNumber).ToList();
                    VehicleList1 = VehicleList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    VehicleList1 = VehicleList1.OrderBy(s => s.RegistrationNumber).ToList();
                    VehicleList1 = VehicleList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = VehicleList1;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = VehicleList1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
       
        public ActionResult Vehicle_ExportToExcel()
        {
            try
            {
                List<VehicleList> VehicleList1 = (List<VehicleList>)Session["JQGridList"];
                var list = VehicleList1.Select(o => new {
                    RegistrationNumber = o.RegistrationNumber,
                    VehicleType=o.VehicleType, 
                    Manufactured=o.Manufactured,
                    Color=o.Color,
                    SeatCapacity=o.SeatCapacity,
                    status=o.status }).ToList();
                string fileName = "VehicleList";
                GenerateExcel.ExportExcel(list, fileName);
                return View("");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult Vehicle_ExportToPdf()
        {
            try
            {
                List<VehicleList> VehicleList1 = (List<VehicleList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = ResourceCache.Localize("VehicleList");
                GeneratePDF.ExportPDF_Landscape(VehicleList1, new string[] { "RegistrationNumber", "VehicleType", "Manufactured", "Color", "SeatCapacity", "status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("VehicleList.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public JsonResult AddVehicleDetail(AddVehicle m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    VehicleDetailServices vehicleServ = new VehicleDetailServices();
                    var DataExist = vehicleServ.IsDataExist(m.txt_RegNumber);
                    if (DataExist == null)
                    {
                        vehicleServ.addVehicle(m.txt_RegNumber, m.txt_RegDate, m.txt_OwnerFirstName, m.txt_OwnerLastName, m.txt_Address1, m.txt_Address2, m.txt_City,m.txt_State, m.Country, m.txt_Pincode, m.txt_BoughtOn, m.Vehicle, m.txt_Manufacture,
                       m.txt_Color, m.txt_EngineNum, m.txt_ChaseNum, m.FuelType, m.txt_FuelCapacity, m.txt_SeatCapacity, m.txt_RegisterUptoDate, m.txt_TaxValidUpto, m.txt_InsuranceNum, m.txt_InsuranceValidFromDate,
                       m.txt_InsuranceValidUpto, AdminId);
                      var RegistrationNumber = vehicleServ.VehicleRegistrationNumberList();

                      string Message = ResourceCache.Localize("Vehicle_details_saved_successfully");
                      useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Vehicle_details_saved_successfully"));
                        return Json(new { Message = Message, RegistrationNumber = RegistrationNumber }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = ResourceCache.Localize("Vehicle_registration_number_exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditVehicle(string passId)
        {
            try
            {
                int eid = Convert.ToInt32(QSCrypt.Decrypt(passId));
                tblCountryServices obj_country = new tblCountryServices();
                VehicleDetailServices vehicleServ = new VehicleDetailServices();
                FuelTypeServices fuelSer = new FuelTypeServices();
                VehicleTypeServices vehicleSer = new VehicleTypeServices();
                var getRow = vehicleServ.getRowDetail(eid);
                
                return Json(new { getRow }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult UpdateVehicleDetail(AddVehicle m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    VehicleDetailServices vehicleServ = new VehicleDetailServices();
                    int id = Convert.ToInt32(QSCrypt.Decrypt(m.Vehicle_Id));
                    var DataExist = vehicleServ.IsRowExist(id);
                    if (DataExist != null)
                    {
                        var RegExist = vehicleServ.RegNoExist(id, m.txt_RegNumber);
                        if (RegExist == null)
                        {
                            vehicleServ.UpdateVehicle(m.txt_RegNumber, m.txt_RegDate, m.txt_OwnerFirstName, m.txt_OwnerLastName, m.txt_Address1, m.txt_Address2, m.txt_City,m.txt_State, m.Country, m.txt_Pincode, m.txt_BoughtOn, m.Vehicle, m.txt_Manufacture,
                  m.txt_Color, m.txt_EngineNum, m.txt_ChaseNum, m.FuelType, m.txt_FuelCapacity, m.txt_SeatCapacity, m.txt_RegisterUptoDate, m.txt_TaxValidUpto, m.txt_InsuranceNum, m.txt_InsuranceValidFromDate,
                  m.txt_InsuranceValidUpto, m.Status.Value, AdminId, id);
                            string Message = ResourceCache.Localize("Vehicle_details_updated_successfully");
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Vehicle_details_updated_successfully"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string ErrorMessage = ResourceCache.Localize("Registration_number_already_exist");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string ErrorMessage = ResourceCache.Localize("Vehicle_registration_number_exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}