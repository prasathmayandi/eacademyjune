﻿using eAcademy.Areas.Transport.Models;
using eAcademy.Areas.Transport.Services;
using eAcademy.Helpers;
using eAcademy.Services;
using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using eAcademy.HelperClass;
using eAcademy.Models;
namespace eAcademy.Areas.Transport.Controllers
{
    [CheckSessionOutAttribute]
    public class FeeConfigController : Controller
    {
        UserActivityHelper useractivity = new UserActivityHelper();
        public int AdminId;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AdminId = Convert.ToInt32(Session["empRegId"]);
      
        }
        public ActionResult Index()
        {
            try
            {
                TransportDestinationServices TrDestnServ = new TransportDestinationServices();
                ViewBag.count = TrDestnServ.getPickPointList().Count();
                return View(TrDestnServ.getPickPointList());
                
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        [HttpPost]
        public JsonResult PickPointSettings(Destination model)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    int TotalCount = model.Count;
                    string destination = model.DestinationName;
                    Boolean Status = model.Status.Value;
                    string[] Pickpoint = new string[TotalCount];
                    string[] distance = new string[TotalCount];
                    Decimal[] charge = new Decimal[TotalCount];
                    TransportDestinationServices TrDestnServ = new TransportDestinationServices();
                    TransportPickPointServices TrPickPtServ = new TransportPickPointServices();
                    var DestinationExist = TrDestnServ.checkDestinationExist(destination);
                    if (DestinationExist == null)
                    {
                      TrDestnServ.addDestination(destination, AdminId);
                        var DestinationId = TrDestnServ.checkDestinationExist(destination).DestinationId;
                        for (int i = 0; i < TotalCount; i++)
                        {
                            int DestnId = DestinationId;
                            Pickpoint[i] = model.PickPointName[i];
                            distance[i] = model.Distance[i];
                            charge[i] = model.Charge[i];
                            TrPickPtServ.addPickPoint(DestnId, Pickpoint[i], distance[i], charge[i], AdminId);
                        }
                        string Message = ResourceCache.Localize("Destination_Pickpoints_SavedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Destination_Pickpoints_SavedSuccessfully"));
                        return Json(new { Message = Message}, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = ResourceCache.Localize("Destination_already_exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }      
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }                          
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult EditPickPoints(string DestinationId)
        {
            try
            {
                int DestnId = Convert.ToInt32(QSCrypt.Decrypt(DestinationId));
                TransportDestinationServices TrDestnServ = new TransportDestinationServices();
                var Pickpt = TrDestnServ.getPickPointListByDestination(DestnId);
                var ans = TrDestnServ.getDestination(DestnId);
                return Json(new { Pickpt = Pickpt, ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckPickPointUsed(string pickPointId)
        {
            int PickPointId = Convert.ToInt32(QSCrypt.Decrypt(pickPointId));
            TransportAllotmentServices transAllotServ = new TransportAllotmentServices();
            var PickPointUsed = transAllotServ.IsPickPointUsed(PickPointId);
            string msg = "";
            if(PickPointUsed != null)
            {
                msg = "AlreadyUsed";
                return Json(new { msg }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                TransportPickPointServices transPickPointServ = new TransportPickPointServices();
                transPickPointServ.DeletePickPoint(PickPointId);
                msg = "NotUsed";
                return Json(new { msg }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UpdatePickPointSettings(UpdatePickPoints model)
        {
            try
            {
                int TotalCount = model.Count;
                int DestinationId = Convert.ToInt32(QSCrypt.Decrypt(model.DestinationId));
                string Destination = model.DestinationName;
                Boolean Status = model.Status.Value;
                string[] PickpointId = new string[TotalCount];
                string[] PickPointName = new string[TotalCount];
                string[] Distance = new string[TotalCount];
                Decimal[] Amount = new Decimal[TotalCount];
                TransportDestinationServices TrDestnServ = new TransportDestinationServices();
                TransportPickPointServices TrPickPtServ = new TransportPickPointServices();
                TransportAllotmentServices transAllotServ = new TransportAllotmentServices();
                var CheckDestinationExist = TrDestnServ.IsDestinationExist(DestinationId, Destination);
                if (CheckDestinationExist != null)
                {
                    string ErrorMessage = ResourceCache.Localize("Destination_already_exist");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                var checkDestinationUsed = transAllotServ.IsDestinationUsed(DestinationId);
                if (checkDestinationUsed != null)
                {
                }
                else
                {
                    TrDestnServ.UpdateDestination(DestinationId, Destination, Status);
                }
                for (int i = 0; i < TotalCount; i++)
                {
                    PickpointId[i] = model.PickPointId[i];
                    PickPointName[i] = model.PickPointName[i];
                    Distance[i] = model.Distance[i];
                    Amount[i] = model.Charge[i];
                    if (PickpointId[i] == "")
                    {
                        TrPickPtServ.addPickPoint(DestinationId, PickPointName[i], Distance[i], Amount[i], AdminId);
                    }
                    else
                    {
                        int PicPointid = Convert.ToInt32(QSCrypt.Decrypt(PickpointId[i]));
                        var PickPointUsed = transAllotServ.IsPickPointUsed(PicPointid);
                        if (PickPointUsed != null)
                        {
                        }
                        else
                        {
                            TrPickPtServ.updatePickPoint(DestinationId, PicPointid, PickPointName[i], Distance[i], Amount[i]);
                        }
                    }
                }
                string Message = ResourceCache.Localize("UpdatedSuccessfully");
                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdatedSuccessfully"));
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (Session["CurrentCulture"] != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["CurrentCulture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["CurrentCulture"].ToString());
            }
        }
        
    }
}