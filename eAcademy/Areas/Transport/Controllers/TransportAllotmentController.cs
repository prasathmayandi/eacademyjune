﻿using eAcademy.Areas.Transport.Models;
using eAcademy.Areas.Transport.Services;
using eAcademy.Areas.Transport.ViewModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using eAcademy.DataModel;
using eAcademy.Models;
namespace eAcademy.Areas.Transport.Controllers
{
    [CheckSessionOutAttribute]
    public class TransportAllotmentController : Controller
    {

        UserActivityHelper useractivity = new UserActivityHelper();
        AcademicyearServices AcYear = new AcademicyearServices();
        public int AdminId;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AdminId = Convert.ToInt32(Session["empRegId"]);
            AcademicyearServices AcYear = new AcademicyearServices();
            ClassServices clas = new ClassServices();
            VehicleDetailServices vehicleServ = new VehicleDetailServices();
            TransportMappingServices transMapping = new TransportMappingServices();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.AddacYear = AcYear.Add_formAcademicYear();
            ViewBag.allClasses = clas.ShowClasses();
            ViewBag.allVehicleRegisterNumber = vehicleServ.VehicleRegistrationNumberList();
            ViewBag.allRouteList = transMapping.GetRouteList();
        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (Session["CurrentCulture"] != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["CurrentCulture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["CurrentCulture"].ToString());
            }
        }
        public JsonResult GetSelectedClassSection(int passClassId)
        {
            SectionServices sec = new SectionServices();
            var List = sec.getSection(passClassId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        public JsonResult showStudents(int passYearId, int passClassId, int passSecId)
        {
            try
            {
                AssignClassToStudentServices stu = new AssignClassToStudentServices();
                var StudentList = stu.getfeepaidStudents(passYearId, passClassId, passSecId);
                
                return Json(StudentList, JsonRequestBehavior.AllowGet);
           }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getStudentTransport(int passYearId, int passClassId, int passSecId, int passStudentId)
        {
            TransportFeesCollectionServices transFeeCol_ser = new TransportFeesCollectionServices();
            var TransportDetail = transFeeCol_ser.getStudentTransportDetail(passYearId, passClassId, passSecId, passStudentId);
            TransportMappingServices transMapp_ser = new TransportMappingServices();
            var routeNo = transMapp_ser.getRouteName(TransportDetail.DestinationId1);
            var RouteNumber = routeNo.RouteNumber;
            var RouteId = QSCrypt.Encrypt(routeNo.RouteId.ToString());
            return Json(new { TransportDetail, RouteNumber, RouteId }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult BusAllotment(BusAllotment m)
        {
            if(ModelState.IsValid)
            {
                TransportAllotmentServices transportAllot_serv = new TransportAllotmentServices();
                int destinationId = Convert.ToInt32(QSCrypt.Decrypt(m.destination_Id));
                int pickpointId = Convert.ToInt32(QSCrypt.Decrypt(m.pickpoint_Id));
                int routeId = Convert.ToInt32(QSCrypt.Decrypt(m.Route_Id));
                var RecordExist = transportAllot_serv.IsRecordExist(m.academicYearId, m.classId, m.sectionId, m.studentId);
                if (RecordExist == null)
                {
                    transportAllot_serv.allotStudent(m.academicYearId, m.classId, m.sectionId, m.studentId, destinationId, pickpointId, routeId, AdminId);
                    string Message = ResourceCache.Localize("Bus_alloted_Successfully");
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Bus_alloted_Successfully"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("Bus_already_alloted_to_selected_student");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }            
            }
            else
            {
                string totalError = "";
                string[] t = new string[ModelState.Values.Count];
                int i = 0;
                foreach (var obj in ModelState.Values)
                {
                    foreach (var error in obj.Errors)
                    {
                        if (!string.IsNullOrEmpty(error.ErrorMessage))
                        {
                            totalError = totalError + error.ErrorMessage + Environment.NewLine;
                            t[i] = error.ErrorMessage;
                            i++;
                        }
                    }
                }
                return Json(new { Success = 0, ex = t });
            }
        }
        public JsonResult EditBusAllotment(string TransportallotmentId)
        {
            TransportAllotmentServices transportAllot_serv = new TransportAllotmentServices();
            var id = Convert.ToInt16(TransportallotmentId);
            var Edit_Allotment = transportAllot_serv.Edit_transportallotment(id);
            bool existacademicyear = AcYear.PastAcademicyear(Edit_Allotment.academicYearId);
            if (existacademicyear == true)
            {
               
                return Json(new { Edit_Allotment }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UpdateBusAllotment(EditTransportAllotment t)
        {
            TransportAllotmentServices transportAllot_serv = new TransportAllotmentServices();
            transportAllot_serv.Update_transportallotments(t.TransportAllotmentId,t.academicYearId,t.classId,t.sectionId, t.studentId, t.destination_Id, t.pickpoint_Id, t.Route_Id,AdminId, t.Status);
            string Message = ResourceCache.Localize("Route_Updated_sucessfully");
            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Route_Updated_sucessfully"));
            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
        }
         
        public JsonResult BusAllotmentListResults(string sidx, string sord, int page, int rows, int? ListRouteNumber)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            TransportAllotmentServices transportAllot_serv = new TransportAllotmentServices();
            IList<StudentTransportList> studentList = new List<StudentTransportList>();
            studentList = transportAllot_serv.getTransportStudentList();
            if (ListRouteNumber != 0 && ListRouteNumber != null)
            {
                studentList = transportAllot_serv.getTransportStudentList(ListRouteNumber.Value);
            }
            int totalRecords = studentList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = studentList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RouteList_ExportToExcel()
        {
            try
            {
                List<StudentTransportList> studentList = (List<StudentTransportList>)Session["JQGridList"];
                var list = studentList.Select(o => new { Name = o.StudentName, Year = o.Year, Class = o.Class, Destination = o.Destination, RouteNumber = o.RouteNumber, PickPoint = o.PickPoint, Status = o.Status }).ToList();
                string fileName = "TransportStudentList";
                GenerateExcel.ExportExcel(list, fileName);
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult RouteList_ExportToPdf()
        {
            try
            {
                List<StudentTransportList> studentList = (List<StudentTransportList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("TransportStudentList");
                GeneratePDF.ExportPDF_Portrait(studentList, new string[] { "StudentName", "Year", "Class", "Destination", "RouteNumber", "PickPoint", "Status" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("TransportStudentList.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
    }
}