﻿using eAcademy.Areas.Transport.Services;
using eAcademy.Areas.Transport.ViewModel;
using eAcademy.Services;
using System;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
namespace eAcademy.Areas.Transport.Controllers
{
    [CheckSessionOutAttribute]
    public class TransportFeesCollectionController : Controller
    {

        UserActivityHelper useractivity = new UserActivityHelper();
        public int AdminId;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AdminId = Convert.ToInt32(Session["empRegId"]);
            AcademicyearServices AcYear = new AcademicyearServices();
            ClassServices clas = new ClassServices();
            TransportDestinationServices TrDestnServ = new TransportDestinationServices();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.allClasses = clas.ShowClasses();
            ViewBag.AddacYear = AcYear.Add_formAcademicYear();
            ViewBag.allDestination = TrDestnServ.DestinationList();
        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (Session["CurrentCulture"] != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["CurrentCulture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["CurrentCulture"].ToString());
            }
        }
        public JsonResult GetSelectedClassSection(int passClassId)
        {
            SectionServices sec = new SectionServices();
            var List = sec.getSection(passClassId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        public JsonResult showStudents(int passYearId, int passClassId, int passSecId)
        {
            try
            {
                AssignClassToStudentServices stu = new AssignClassToStudentServices();
                var StudentList = stu.getStudents(passYearId, passClassId, passSecId);
                return Json(StudentList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetSelectedDestinationPickPoints(int passDestinationId)
        {
            TransportPickPointServices pickPtServ = new TransportPickPointServices();
            var List = pickPtServ.getPickPoints(passDestinationId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPickPointAmount(int passPickPointId)
         {
            TransportPickPointServices pickPtServ = new TransportPickPointServices();
            var getFeeAmount = pickPtServ.getPickPointAmount(passPickPointId);
            Decimal FeeAmount =  Math.Round(getFeeAmount.Amount.Value, 2, MidpointRounding.AwayFromZero);
            SchoolSettingsServices obj_settings = new SchoolSettingsServices();
            var ans = obj_settings.SchoolSettings();
            string ServiceTax = ans.ServicesTax.ToString();
            string VAT = ans.VAT.ToString();
            var Total = FeeAmount;
            return Json(new { FeeAmount, ServiceTax, VAT, Total }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index()
        {
            var date = DateTimeByZone.getCurrentDateTime();
            try
            {
                TransportFeesCollectionServices obj_feeCollection = new TransportFeesCollectionServices();
                var result = obj_feeCollection.getFeeCollection();
                if (result.Count != 0)
                {
                    var ans = obj_feeCollection.getFeeCollectionOrderByFeeCollectionID();
                    ViewBag.recID = ans + 1;
                }
                else
                {
                    ViewBag.recID = 1;
                }
                ViewBag.sysDate = date.Date.ToString("dd/MM/yyyy");
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult GetPaidStudentDetail(int passYearId, int PassClassId, int PassSecId, int PassStuRegId)
        {
             TransportFeesCollectionServices obj_feeCollection = new TransportFeesCollectionServices();
             var getRow = obj_feeCollection.IsRecordExist(passYearId, PassClassId, PassSecId, PassStuRegId);
            if(getRow != null)
            {
                var record = "Exist";
                var detail = obj_feeCollection.PaidStudentDetail(passYearId, PassClassId, PassSecId, PassStuRegId);
                return Json(new { detail, record }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var record = "New";
                return Json(new { record }, JsonRequestBehavior.AllowGet);
            }
             
        }
        public JsonResult getStudentRegisterId(int AcYearId, int ClassId, int SectionId, int StudentId)
        {
            StudentServices obj_student = new StudentServices();
            var studentregisternumber = obj_student.getStudentId(AcYearId, ClassId, SectionId, StudentId);
            string stu_id = studentregisternumber.StudentId;
            return Json(stu_id, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getStudentDetailsByRollNumber(string RegNo)
        {
            StudentServices obj_student = new StudentServices();
            try
            {
                var ans = obj_student.getStudentByStudentId(RegNo);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getClass()
        {
            ClassServices obj_class = new ClassServices();
            try
            {
                var ans = obj_class.Classes();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getSection(int cid)
        {
            SectionServices obj_section = new SectionServices();
            try
            {
                var ans = obj_section.getSectionByClassId(cid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getStudent(int acid, int sec_id, int cid)
        {
            StudentServices obj_student = new StudentServices();
            try
            {
                var ans = obj_student.getStudentByIDs(sec_id, cid, acid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult CollectFee(CollectFee m)
        {
           if(ModelState.IsValid)
           {
               int collectionid = 0;
               string paymentMode = "";
               string mode = m.PaymentMode;
               if (mode == "2")
               {
                   paymentMode = ResourceCache.Localize("Cheque");
               }
               else if (mode == "3")
               {
                   paymentMode = ResourceCache.Localize("DD");
               }
               else
               {
                   paymentMode = ResourceCache.Localize("cash");
               }
               TransportFeesCollectionServices transFeeCollection = new TransportFeesCollectionServices();
               var getRow = transFeeCollection.IsRecordExist(m.academicYearId, m.classId, m.sectionId, m.studentId);
               if (getRow != null)
               {
                   collectionid = transFeeCollection.UpdateFee(m.academicYearId, m.classId, m.sectionId, m.studentId, m.destinationId, m.pickpointId, m.FeeAmount, m.AmountCollected, m.FineAmount.Value, m.Discount, m.VAT.Value,
                  paymentMode, m.BankName, m.ChequeNumber, m.DDNumber, m.Remark, AdminId);
               }
               else
               {
                   collectionid=transFeeCollection.addFee(m.academicYearId, m.classId, m.sectionId, m.studentId, m.destinationId, m.pickpointId, m.FeeAmount, m.AmountCollected, m.FineAmount.Value, m.Discount, m.VAT.Value,
                    paymentMode, m.BankName, m.ChequeNumber, m.DDNumber, m.Remark, AdminId);
               }
               string EncrptCollectionid = QSCrypt.Encrypt(collectionid.ToString());
               string Message = ResourceCache.Localize("Fees_collected_successfully");
               useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Fees_collected_successfully"));
               return Json(new { Message = Message, EncrptCollectionid = EncrptCollectionid, collectionid = collectionid }, JsonRequestBehavior.AllowGet);
           }
           else
           {
               string totalError = "";
               string[] t = new string[ModelState.Values.Count];
               int i = 0;
               foreach (var obj in ModelState.Values)
               {
                   foreach (var error in obj.Errors)
                   {
                       if (!string.IsNullOrEmpty(error.ErrorMessage))
                       {
                           totalError = totalError + error.ErrorMessage + Environment.NewLine;
                           t[i] = error.ErrorMessage;
                           i++;
                       }
                   }
               }
               return Json(new { Success = 0, ex = t });
           }
            
        }

        public ActionResult TransportBillReceipt(string id)
        {
            TransportFeesCollectionServices Fee_Collection = new TransportFeesCollectionServices();
            int FeecolletionId = Convert.ToInt16(QSCrypt.Decrypt(id));
            var list = Fee_Collection.GetFeeCollectiondetails(FeecolletionId);
            return View(list);
        }
       
    }
}