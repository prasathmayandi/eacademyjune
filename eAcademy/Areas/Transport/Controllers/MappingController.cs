﻿using eAcademy.Areas.Transport.Models;
using eAcademy.Areas.Transport.Services;
using eAcademy.Areas.Transport.ViewModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using eAcademy.DataModel;
using eAcademy.Models;
namespace eAcademy.Areas.Transport.Controllers
{
    [CheckSessionOutAttribute]
    public class MappingController : Controller
    {
        EacademyEntities db = new EacademyEntities();
        UserActivityHelper useractivity = new UserActivityHelper();
        public int AdminId;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AdminId = Convert.ToInt32(Session["empRegId"]);
        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (Session["CurrentCulture"] != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["CurrentCulture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["CurrentCulture"].ToString());
            }
        }
        public ActionResult Index()
        {
            VehicleDetailServices vehicleServ = new VehicleDetailServices();
            TransportDestinationServices TrDestnServ = new TransportDestinationServices();
            OtherEmployeeServices otherEmpServ = new OtherEmployeeServices();
            ViewBag.allVehicleRegisterNumber = vehicleServ.VehicleRegistrationNumberList();
            ViewBag.allDestination = TrDestnServ.DestinationList();
            ViewBag.allDriver = otherEmpServ.DriverList();
            ViewBag.allHelper = otherEmpServ.CleanerList();
            return View();
        }
        public JsonResult getPickPoints(int PassDestinationId)
        {
            TransportPickPointServices TrPickPtServ = new TransportPickPointServices();
            var PickPoint = TrPickPtServ.getPickPointListByDestination(PassDestinationId);
            var Message = "gffffffffff";
            return Json(new { Message, PickPoint }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AddMapping(AddMapping m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    TransportMappingServices TransportMappServ = new TransportMappingServices();
                    var RootNumExist = TransportMappServ.IsRootnumExist(m.txt_RouteName);
                    if (RootNumExist == null)
                    {
                        var registernumberexist = TransportMappServ.Registernumber_Exist(m.RegisterNumber);
                        if (registernumberexist == null)
                        {
                            TransportMappServ.addMapping(m.txt_RouteName, m.RegisterNumber, m.Destination, m.txt_SchoolInTime, m.txt_SchoolOutTime, m.txt_ParkingStartTime, m.txt_ParkingEndTime, m.Driver,
                         m.Helper, AdminId);
                            var RouteId = TransportMappServ.getRouteId();
                            int RId = RouteId.RouteId;
                            TransportTimingServices TrTime = new TransportTimingServices();
                            for (int i = 0; i <= m.TotalCount; i++)
                            {
                                int pickpointId = Convert.ToInt32(QSCrypt.Decrypt(m.PickPointId[i].ToString()));
                                TrTime.addPickpointTiming(RId, pickpointId, m.DepartureTime[i], m.LeavingTime[i], AdminId);
                            }
                            string Message = ResourceCache.Localize("Transport_mapping_saved_successfully");
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Transport_mapping_saved_successfully"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string ErrorMessage = ResourceCache.Localize("Registration_number_already_exist");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        }
                    
                    else
                    {
                        string ErrorMessage = ResourceCache.Localize("Route_name_already_exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }            
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult RouteListResults(string sidx, string sord, int page, int rows, int? ListRegisterNumber)  
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            TransportMappingServices TransportMappServ = new TransportMappingServices();
            IList<RouteList> RouteList1 = new List<RouteList>();
            RouteList1 = TransportMappServ.getRouteList();
            if (ListRegisterNumber != 0 && ListRegisterNumber != null)
            {
                RouteList1 = TransportMappServ.getRouteList(ListRegisterNumber.Value);
            }
            int totalRecords = RouteList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    RouteList1 = RouteList1.OrderByDescending(s => s.RegistrationNumber).ToList();
                    RouteList1 = RouteList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    RouteList1 = RouteList1.OrderBy(s => s.RegistrationNumber).ToList();
                    RouteList1 = RouteList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = RouteList1;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = RouteList1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditRoute(string passId)
        {
            try
            {
                int eid = Convert.ToInt32(QSCrypt.Decrypt(passId));
                TransportMappingServices TransportMappServ = new TransportMappingServices();
                 TransportPickPointServices TrPickPtServ = new TransportPickPointServices();
                var getRow = TransportMappServ.getRowDetail(eid);
                var PickPointList = TrPickPtServ.getPickPointListByMappingRoute(eid);
                return Json(new { getRow, PickPointList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpPost]
        public JsonResult UpdateMapping(EditMapping m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    TransportMappingServices TransportMappServ = new TransportMappingServices();
                    int id = Convert.ToInt32(QSCrypt.Decrypt(m.rowId));
                    var DataExist = TransportMappServ.IsRowExist(id);
                    int size = m.TotalCount + 1;
                    if (DataExist != null)
                    {
                        var RegExist = TransportMappServ.RouteNoExist(id, m.Edit_txt_RouteName);
                        if (RegExist == null)
                        {
                            TransportTimingServices TrTime = new TransportTimingServices();
                            
                            TransportMappServ.updateMapping(id, m.Edit_txt_RouteName, m.Edit_RegisterNumber, m.Edit_Destination, m.Edit_txt_SchoolInTime, m.Edit_txt_SchoolOutTime, m.Edit_txt_ParkingStartTime, m.Edit_txt_ParkingEndTime, m.Edit_Driver,
                            m.Edit_Helper, AdminId, m.Status);
                            int[] PickPoint = new int[size];
                            for (int i = 0; i <= m.TotalCount; i++)
                            {
                                int pickpointId = Convert.ToInt32(QSCrypt.Decrypt(m.PickPointId[i].ToString()));
                                TrTime.UpdateRouteTiming(id, pickpointId, m.DepartureTime[i], m.LeavingTime[i], AdminId);
                                PickPoint[i] = pickpointId;
                            }
                            TrTime.FalseUnusedPickPoint(id, PickPoint);
                            string Message = ResourceCache.Localize("Route_details_updated_successfully");
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Route_details_updated_successfully"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string ErrorMessage = ResourceCache.Localize("Registration_number_already_exist");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string ErrorMessage = ResourceCache.Localize("Registration_number_already_exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult RouteList_ExportToExcel()
        {
            try
            {
                List<RouteList> RouteList1 = (List<RouteList>)Session["JQGridList"];
                var list = RouteList1.Select(o => new { RouteName = o.RouteName, RegistrationNumber = o.RegistrationNumber, Destination = o.Destination, PickPoint = o.PickPoints, status=o.status}).ToList();
                string fileName = "RouteList";
                GenerateExcel.ExportExcel(list, fileName);
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult RouteList_ExportToPdf()
        {
            try
            {
                List<RouteList> RouteList1 = (List<RouteList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("RouteList");
                GeneratePDF.ExportPDF_Portrait(RouteList1, new string[] { "RouteName", "RegistrationNumber", "Destination", "PickPoints", "status" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("TransportStudentList.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
    }
}