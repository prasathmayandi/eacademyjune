﻿using eAcademy.Areas.LibraryManagement.LocalResource;
using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.Areas.LibraryManagement.ViewModel
{
    public class VM_FineCollect
    {
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                        ErrorMessageResourceName = "Pleaseselectacademicyear")]
        public int AcademicYear { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                         ErrorMessageResourceName = "Pleaseselectclass")]
        public int Class { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                         ErrorMessageResourceName = "Pleaseselectsection")]
        public int Section { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                        ErrorMessageResourceName = "Pleaseselectstudentname")]
        public int StudentRegisterId { get; set; }
        public Decimal Fineamount { get; set; }
        public string[] id { get; set; }



        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Selectacademicyear")]
        //public int AcademicYear { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Selectclass")]
        //public int Class { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Selectsection")]
        //public int Section { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Selectstudentid")]
        //public int StudentRegisterId { get; set; }
        //public Decimal Fineamount { get; set; }
        //public string[] id { get; set; }
    }
}