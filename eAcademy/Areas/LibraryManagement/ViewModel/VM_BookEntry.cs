﻿using eAcademy.Areas.LibraryManagement.LocalResource;
using eAcademy.HelperClass;
using System;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.Areas.LibraryManagement.ViewModel
{
    public class VM_BookEntry
    {
        public int BookEntryRegisterId { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                        ErrorMessageResourceName = "Enterbooktitle")]
        public string BookTitle { get; set; }


        //[DisplayName("AuthorName")]
        //[StringArrayRequired(ErrorMessage = "Content name is required")]
        [StringArrayRequired(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                        ErrorMessageResourceName = "Enterauthorname")]
        public string[] AuthorName { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                        ErrorMessageResourceName = "Enterpublicationname")]
        public string PublicationName { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                         ErrorMessageResourceName = "Enterbooklanaguage")]
        public string Booklanaguage { get; set; }
        public string Edition { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                         ErrorMessageResourceName = "Enterbooktype")]
        public string BookType { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                         ErrorMessageResourceName = "Enterbookprice")]
        public Decimal BookPrice { get; set; }


        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                          ErrorMessageResourceName = "Enternoofcopy")]
        public int NoOfCopy { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                         ErrorMessageResourceName = "Enterbooksection")]
        public string BookSection { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                          ErrorMessageResourceName = "Enterrackno")]
        public string RackNo { get; set; }

        public int NoOfAuthor { get; set; }
        public string[] AuthorId { get; set; }





        //public int BookEntryRegisterId { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Enterbooktitle")]
        //public string BookTitle { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Enterauthorname")]
        //public string[] AuthorName { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Enterpublicationname")]
        //public string PublicationName { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Enterbooklanaguage")]
        //public string Booklanaguage { get; set; }
        //public string Edition { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Enterbooktype")]
        //public string BookType { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Enterbookprice")]
        //public Decimal BookPrice { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Enternoofcopy")]
        //public int NoOfCopy { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Enterbooksection")]
        //public string BookSection { get; set; }
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Enterrackno")]
        //public string RackNo { get; set; }
        //public int NoOfAuthor { get; set; }
        //public string[] AuthorId { get; set; }
    }
}