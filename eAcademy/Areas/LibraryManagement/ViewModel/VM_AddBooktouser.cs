﻿using eAcademy.Areas.LibraryManagement.LocalResource;
using System.ComponentModel.DataAnnotations;

namespace eAcademy.Areas.LibraryManagement.ViewModel
{
    public class VM_AddBooktouser
    {
       // [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Enterbookid")]
        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                        ErrorMessageResourceName = "Entercorrectbookid")]
        public string[] BookId { get; set; }
        public string[] BookEntryId{ get; set; }
        
        public string[] DateOfTaken { get; set; }
        public string[] DateOfReturn { get; set; }


       // [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Enterrequestid")]

         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.LibraryManagement.Resources.LibraryResources),
                        ErrorMessageResourceName = "Enterrequestid")]
        public string RequestId { get; set; }
        public int count { get; set; }
    }
}