﻿using eAcademy.Areas.LibraryManagement.LocalResource;
using eAcademy.Areas.LibraryManagement.Models;
using eAcademy.Areas.LibraryManagement.Services;
using eAcademy.Areas.LibraryManagement.ViewModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
namespace eAcademy.Areas.LibraryManagement.Controllers
{
    [CheckSessionOutAttribute]
    public class LibraryController : Controller
    {
        public int AdminId;
        UserActivityHelper useractivity = new UserActivityHelper();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            ViewBag.academicyear = As.GetAcademicYear();
            ViewBag.AddacYear = As.Add_formAcademicYear();
            ViewBag.Classes = Cs.GetClass();
            AdminId = Convert.ToInt32(Session["empRegId"]);
        }
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (Session["CurrentCulture"] != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["CurrentCulture"].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["CurrentCulture"].ToString());
            }
        }
        public ActionResult BookRegister()
        {
            return View();
        }
        public JsonResult BookEntry(VM_BookEntry m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    BookEntryServices bookentry = new BookEntryServices();
                    string BookIdFormat = ConfigurationManager.AppSettings["LibraryBookIdFormat"];
                    bookentry.AddBook(m.BookTitle, m.AuthorName, m.PublicationName, m.Booklanaguage, m.Edition, m.BookType, m.BookPrice, m.NoOfCopy, m.BookSection, m.RackNo, m.NoOfAuthor, AdminId, BookIdFormat);
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Addbookdetails"));
                    string Message = eAcademy.Models.ResourceCache.Localize("Successfullybookdetailsareadded");
                    return Json(new { Message = Message, JsonRequestBehavior.AllowGet });
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { Errormessage = e.Message.ToString(), JsonRequestBehavior.AllowGet });
            }
        }
        public JsonResult CheckBookExist(int PassBookRegId, string PassReqId)
                {
            
                BookEntryServices bs = new BookEntryServices();

                StudentServices s_student = new StudentServices();
                TechEmployeeServices T_Employee = new TechEmployeeServices();
                StudentBookTakenReturnServices Student_Taken = new StudentBookTakenReturnServices();
                StaffBookTakenReturnServices staff_Taken = new StaffBookTakenReturnServices();

                var student = s_student.GetLibrayStudent(PassReqId);
                var employee = T_Employee.GetlibraryEmployee(PassReqId);

                if (student != null)
                {

                    bool cc = bs.StudentBookIdExist(Convert.ToInt16(student.StudentRegisterId), PassBookRegId);
                    if (cc == true) 
                    { 
                    //return Json(new { list = cc }, JsonRequestBehavior.AllowGet);
                    string Message = "ok";
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("NotAllowtoTackSameBookStudent");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (employee != null)
                {

                    bool cc = bs.EmployeeBookIdExist(Convert.ToInt16(employee.EmployeeRegisterId), PassBookRegId);
                    if (cc == true)
                    {
                        string Message = "ok";
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        //return Json(new { list = cc }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("NotAllowtoTackSameBookEmployee");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("EnterCorrectStudentrollnumberorEmployeeId");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            

        }
        
        static IList<BookEntryList> todoListsResults1 = new List<BookEntryList>();
        public JsonResult BookDetailsResults(string sidx, string sord, int page, int rows)
        {
            BookEntryServices Bookentry = new BookEntryServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults1 = Bookentry.BookEntryDetails();
            int totalRecords = todoListsResults1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults1 = todoListsResults1.OrderByDescending(s => s.BookEntryRegisterId).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults1 = todoListsResults1.OrderBy(s => s.BookEntryRegisterId).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BookDetailsExportToExcel()
        {
            try
            {
                var list = todoListsResults1.Select(o => new { BookTitle = o.BookTitle,  Booklanaguage = o.Booklanaguage, BookType = o.BookType, BookPrice = o.BookPrice, NoOfCopy = o.NoOfCopy, BookSection = o.BookSection, RackNo = o.RackNo, DateofEntry = o.DateofEntry }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("Librarybookdetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult BookDetailsExportToPDF()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName =eAcademy.Models.ResourceCache.Localize("Librarybookdetails");
                GeneratePDF.ExportPDF_Landscape(todoListsResults1, new string[] { "BookTitle",  "Booklanaguage",  "BookType", "BookPrice", "NoOfCopy", "BookSection", "RackNo", "DateofEntry" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "Librarybookdetails.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public JsonResult EditBookEntry(int Bookregisterid)
        {
            try
            {
                BookEntryServices bookEntry = new BookEntryServices();
                BookAuthorsServices bookauthor = new BookAuthorsServices();
                var ans = bookEntry.GetparticularbookDetails(Bookregisterid);
                var author = bookauthor.GetParticularBookAuthorDetails(Bookregisterid);
                return Json(new { ans = ans, author = author, JsonRequestBehavior.AllowGet });
            }
            catch (Exception e)
            {
                return Json(new { Errormessage = e.Message.ToString(), JsonRequestBehavior.AllowGet });
            }
        }
        public JsonResult Update(VM_BookEntry m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    BookEntryServices bookentry = new BookEntryServices();
                    BookAuthorsServices bookauthor = new BookAuthorsServices();
                    bookentry.BookEntryUpdate(m.BookEntryRegisterId, m.BookTitle, m.PublicationName, m.Booklanaguage, m.Edition, m.BookType, m.BookPrice, m.BookSection, m.RackNo, AdminId);
                    bookauthor.UpdateBookAuthor(m.AuthorName, m.AuthorId, m.NoOfAuthor);
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Updatebookdetails"));
                    string Message = eAcademy.Models.ResourceCache.Localize("SuccessfullybookdetailsareUpdated");
                    return Json(new { Message = Message, JsonRequestBehavior.AllowGet });
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { Errormessage = e.Message.ToString(), JsonRequestBehavior.AllowGet });
            }
        }
        public ActionResult BookTaken()
        {
            return View();
        }
        public JsonResult Check(string RegId)
        {
            try
            {

                StudentServices s_student = new StudentServices();
                TechEmployeeServices T_Employee = new TechEmployeeServices();
                StudentBookTakenReturnServices Lib_Student = new StudentBookTakenReturnServices();
                StaffBookTakenReturnServices Lib_Staff = new StaffBookTakenReturnServices();
                var student = s_student.GetLibrayStudent(RegId);
                var employee = T_Employee.GetlibraryEmployee(RegId);
                if (student != null)
                {
                    var list = Lib_Student.GetStudentOldBookDetails(Convert.ToInt16(student.StudentRegisterId));
                    var circualtiondays = Convert.ToInt16(ConfigurationManager.AppSettings["StudentBookCirculationDays"]);//no of due days or circulation days
                    var NoOfBook = Convert.ToInt16(ConfigurationManager.AppSettings["StudentNoOfBooktake"]);// no of book permission to take
                    //var circualtiondays = 15; //no of due days or circulation days
                   // var NoOfBook = 5; // no of book permission to take
                    return Json(new { list = list, student = student, circualtiondays = circualtiondays, NoOfBook = NoOfBook }, JsonRequestBehavior.AllowGet);
                }
                else if (employee != null)
                {
                    var list2 = Lib_Staff.GetStaffOldBookDetails(Convert.ToInt16(employee.EmployeeRegisterId));
                    var circualtiondays = Convert.ToInt16(ConfigurationManager.AppSettings["EmployeeBookCirculationDays"]);//no of due days or circulation days
                    var NoOfBook = Convert.ToInt16(ConfigurationManager.AppSettings["EmployeeNoOfBooktake"]);// no of book permission to take
                    //var circualtiondays = 30;//no of due days or circulation days
                    //var NoOfBook = 10;// no of book permission to take
                    return Json(new { list2 = list2, employee = employee, circualtiondays = circualtiondays, NoOfBook = NoOfBook }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("EnterCorrectStudentrollnumberorEmployeeId");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetBook(string bookid)
        {
            try
            {
                EntrybookIdServices EntryBook = new EntrybookIdServices();
                BookEntryServices ES = new BookEntryServices();
                var ans = EntryBook.CheckBookId(bookid);
                                 
                    if (ans != null)
                    {
                        var file = EntryBook.GetParticularbookdetailsUsingBookId(bookid);
                        if (file != null)
                        {
                            return Json(file, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Now_this_book_issued_another_Student/Staff");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Entercorrectbookid");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }

                
                
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveBookToUser(VM_AddBooktouser cs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    BookEntryServices bs = new BookEntryServices();
                    StudentServices s_student = new StudentServices();
                    TechEmployeeServices T_Employee = new TechEmployeeServices();
                    StudentBookTakenReturnServices Student_Taken = new StudentBookTakenReturnServices();
                    StaffBookTakenReturnServices staff_Taken = new StaffBookTakenReturnServices();
                    int numberofbook = cs.count;
                    var student = s_student.GetLibrayStudent(cs.RequestId);
                    var employee = T_Employee.GetlibraryEmployee(cs.RequestId);

                    if (student != null)
                    {
                            string[] BookEntryId = cs.BookEntryId;                           
                            for (int i = 0; i < BookEntryId.Length; i++)
                            {
                                for (int j = 0; j < BookEntryId.Length; j++)
                                {
                                    if (i == j)
                                    {

                                    }
                                    else 
                                    {
                                        if(cs.BookEntryId[i]==BookEntryId[j])
                                        {
                                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Dontentersamebookid");
                                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                        }
                                        
                                    }
                                }
                            }
                            for (int i = 0; i < BookEntryId.Length; i++)
                            {
                                int  EntryId = Convert.ToInt16(BookEntryId[i]);
                                bool cc = bs.StudentBookIdExist(Convert.ToInt16(student.StudentRegisterId), EntryId);
                                if (cc == false)
                                {
                                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("NotAllowtoTackSameBookStudent");                                   
                                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            string[] BookId = cs.BookId; 
                            for (int i = 0; i < BookId.Length; i++)
                            {
                                string BookCopyId = BookId[i];
                                var  cc = bs.CheckBookStatus(BookCopyId);
                                if (cc == null )
                                {
                                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Now_this_book_issued_another_Student/Staff");
                                    //string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Dontentersamebookid");
                                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            Student_Taken.AddStudentBook(Convert.ToInt16(student.StudentRegisterId), cs.RequestId, cs.BookId, cs.BookEntryId, cs.DateOfTaken, cs.DateOfReturn, AdminId, numberofbook);
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Givebooktostudent"));
                            var list = Student_Taken.GetStudentOldBookDetails(Convert.ToInt16(student.StudentRegisterId));
                            return Json(new { list = list }, JsonRequestBehavior.AllowGet);
                        
                        
                    }
                    else if (employee != null)
                    {
                        string[] BookEntryId = cs.BookEntryId;
                        for (int i = 0; i < BookEntryId.Length; i++)
                        {
                            for (int j = 0; j < BookEntryId.Length; j++)
                            {
                                if (i == j)
                                {

                                }
                                else
                                {
                                    if (cs.BookEntryId[i] == BookEntryId[j])
                                    {
                                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Dontentersamebookid");
                                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                    }

                                }
                            }
                        }
                        for (int i = 0; i < BookEntryId.Length; i++)
                        {
                            int EntryId = Convert.ToInt16(BookEntryId[i]);
                            bool cc = bs.StudentBookIdExist(Convert.ToInt16(student.StudentRegisterId), EntryId);
                            if (cc == false)
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("NotAllowtoTackSameBookEmployee");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        string[] BookId = cs.BookId;
                        for (int i = 0; i < BookId.Length; i++)
                        {
                            string BookCopyId = BookId[i];
                            var cc = bs.CheckBookStatus(BookCopyId);
                            if (cc == null)
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Now_this_book_issued_another_Student/Staff");
                                //string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Dontentersamebookid");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        
                            staff_Taken.AddEmployeeBook(Convert.ToInt16(employee.EmployeeRegisterId), cs.RequestId, cs.BookId, cs.BookEntryId, cs.DateOfTaken, cs.DateOfReturn, AdminId, numberofbook);
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Givebooktostaff"));
                            var list2 = staff_Taken.GetStaffOldBookDetails(Convert.ToInt16(employee.EmployeeRegisterId));
                            return Json(new { list = list2 }, JsonRequestBehavior.AllowGet);
                       
                       
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("EnterCorrectStudentrollnumberorEmployeeId");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UserReturnBook(string[] BookId, string RequestId)
        {
            try
            {
                StudentServices s_student = new StudentServices();
                TechEmployeeServices T_Employee = new TechEmployeeServices();
                StudentBookTakenReturnServices Student_Taken = new StudentBookTakenReturnServices();
                StaffBookTakenReturnServices staff_Taken = new StaffBookTakenReturnServices();
                int count = BookId.Length;
                var student = s_student.GetLibrayStudent(RequestId);
                var employee = T_Employee.GetlibraryEmployee(RequestId);
                if (student != null)
                {
                    Student_Taken.AfterFineReturn(Convert.ToInt16(student.StudentRegisterId), BookId, count, AdminId);
                    Student_Taken.StudentFineEntry(Convert.ToInt16(student.StudentRegisterId), BookId, count, AdminId);
                    Student_Taken.StudentReturnbook(Convert.ToInt16(student.StudentRegisterId), BookId, count, AdminId);
                    var FineList = Student_Taken.GetStudentFineBookLis(Convert.ToInt16(student.StudentRegisterId));
                    if (FineList == 0)
                    {
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Collectbookfromstudent"));
                        var list = Student_Taken.GetStudentOldBookDetails(Convert.ToInt16(student.StudentRegisterId));
                        return Json(new { list = list }, JsonRequestBehavior.AllowGet);
                        
                    }
                    else
                    {
                        var list = Student_Taken.GetStudentOldBookDetails(Convert.ToInt16(student.StudentRegisterId));
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Fine_not_Collected_So_Cannot_return_this_book");//add the value for sheet
                        return Json(new { ErrorMessage = ErrorMessage, list = list }, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (employee != null)
                {
                    staff_Taken.EmployeeReturnbook(Convert.ToInt16(employee.EmployeeRegisterId), BookId, count, AdminId);
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Collectbookfromstaff"));
                    var list2 = staff_Taken.GetStaffOldBookDetails(Convert.ToInt16(employee.EmployeeRegisterId));
                    return Json(new { list = list2 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage =eAcademy.Models.ResourceCache.Localize("EnterCorrectStudentrollnumberorEmployeeId");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult BookSearchReport()
        {
            BookEntryServices BookEntry = new BookEntryServices();
            BookAuthorsServices BookAuthor = new BookAuthorsServices();
            ViewBag.AllBookTitle = BookEntry.Getbooktitle();
            return View();
        }
        public JsonResult GetbookAuthor(string title)
        {
            try
            {
                BookAuthorsServices BookAuthor = new BookAuthorsServices();
                var list = BookAuthor.GetrequiredBoookAuthor(title);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        static IList<M_SearchBookList> todoListsResults = new List<M_SearchBookList>();
        public JsonResult BookSearchResults(string sidx, string sord, int page, int rows, string Lib_BookTitile, int? Lib_BookAuthor)
        {
            BookEntryServices Bookentry = new BookEntryServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (Lib_BookTitile != "" && Lib_BookAuthor != 0 && Lib_BookAuthor != null)
            {
                todoListsResults = Bookentry.GetSearchBookList(Lib_BookTitile, Lib_BookAuthor);
                
            }
            int totalRecords = todoListsResults.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults = todoListsResults.OrderByDescending(s => s.BookTitle).ToList();
                    todoListsResults = todoListsResults.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults = todoListsResults.OrderBy(s => s.BookTitle).ToList();
                    todoListsResults = todoListsResults.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BookSearchExportToExcel()
        {
            try
            {
                var list = todoListsResults.Select(o => new { BookId = o.BookId, BookTitle = o.BookTitle, AuthorName = o.AuthorName, BookEdition = o.BookEdition, BookSection = o.BookSection, BookType = o.BookType, RackNo = o.RackNo, TotalNoOfBook = o.TotalNoOfBook, NoOfAvailable = o.NoOfAvailable, Status = o.Status }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("Searchbookdetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult BookSearchExportToPDF()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("Searchbookdetails");
                GeneratePDF.ExportPDF_Landscape(todoListsResults, new string[] { "BookId", "BookTitle", "AuthorName", "BookEdition", "BookSection", "BookType", "RackNo", "TotalNoOfBook", "NoOfAvailable", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "Searchbookdetails.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult FineCollection()
        {
            return View();
        }
        static IList<M_FineCollectionList> todoListsResults4 = new List<M_FineCollectionList>();
        public JsonResult StudentlibraryfinelistResults(string sidx, string sord, int page, int rows, int? AcademicYear, int? Class, int? Rpt_Section, int? StudentId)
        {
            StudentBookTakenReturnServices Student_Taken = new StudentBookTakenReturnServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (StudentId != 0 && StudentId != null && AcademicYear != 0 && AcademicYear != null && Class != 0 && Class != null && Rpt_Section != 0 && Rpt_Section != null)
            {
                todoListsResults4 = Student_Taken.CollectFineList(StudentId, AcademicYear, Class, Rpt_Section);
            }
            int totalRecords = todoListsResults.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults4 = todoListsResults4.OrderByDescending(s => s.BookTitle).ToList();
                    todoListsResults4 = todoListsResults4.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults4 = todoListsResults4.OrderBy(s => s.BookTitle).ToList();
                    todoListsResults4 = todoListsResults4.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults4
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult StudentlibraryfinelistExportToExcel()
        {
            try
            {
                var list = todoListsResults4.Select(o => new { BookId = o.BookId, BookTitle = o.BookTitle, Dateoftaken = o.Dateoftaken, DateofReturn = o.DateofReturn, DueDays = o.DueDays, FineAmount = o.FineAmount }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("Studentlibraryfinelist");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult StudentlibraryfinelistExportToPDF()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("Studentlibraryfinelist");
                GeneratePDF.ExportPDF_Landscape(todoListsResults4, new string[] { "BookId", "BookTitle", "Dateoftaken", "DateofReturn", "DueDays", "FineAmount" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "Studentlibraryfinelist.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public JsonResult GetFineTotal(VM_FineCollect cs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    StudentBookTakenReturnServices Student_Taken = new StudentBookTakenReturnServices();
                    var result = Student_Taken.CollectFineList(cs.StudentRegisterId, cs.AcademicYear, cs.Class, cs.Section);
                    var list = Student_Taken.getFineAmountTotal(result);
                    return Json(list, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CollectFine(VM_FineCollect cs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    StudentBookTakenReturnServices Student_taken = new StudentBookTakenReturnServices();
                    LibraryFeeCollectionServices Lib_Fee = new LibraryFeeCollectionServices();
                    Student_taken.updatefinecollection(cs.id, AdminId);
                   int FeeColletionId= Lib_Fee.CollectFees(cs.AcademicYear, cs.Class, cs.Section, cs.StudentRegisterId, cs.Fineamount, AdminId);
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Collectlibrayfeefinefromstudent"));
                    string Message =eAcademy.Models.ResourceCache.Localize("SuccessfullyLibraryfeecollected");
                    string Encryptid = QSCrypt.Encrypt(FeeColletionId.ToString());
                    return Json(new { Message = Message, Encryptid = Encryptid }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LibraryBillReceipt(string id)
        {
            LibraryFeeCollectionServices Fee_Collection = new LibraryFeeCollectionServices();
            int FeecolletionId = Convert.ToInt16(QSCrypt.Decrypt(id));
            var list = Fee_Collection.GetFeeCollectiondetails(FeecolletionId);
            return View(list);
        }
    }
}