﻿using eAcademy.Areas.LibraryManagement.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
namespace eAcademy.Areas.LibraryManagement.Services
{
    public class BookEntryServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public void AddBook(string Booktitle, string[] Authorname, string publication, string lanquage, string edition, string booktype, Decimal bookprice, int noofcopy, string booksection, string rackno, int noofauthor, int empregid, string format)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            BookEntry add = new BookEntry()
            {
                BookTitle = Booktitle,
                PublicationName = publication,
                BookLanaguage = lanquage,
                Edition = edition,
                BookType = booktype,
                BookPrice = bookprice,
                NoOfCopies = noofcopy,
                BookSection = booksection,
                RackNo = rackno,
                DateOfEntry = date,
                EmployeeRegisterId = empregid,
                Status = true
            };
            dc.BookEntries.Add(add);
            dc.SaveChanges();
            int count = Convert.ToInt16(noofauthor);
            string[] author = new string[count];
            for (int i = 0; i < noofauthor; i++)
            {
                author[i] = String.Join(" ", Authorname[i]);
                if (author[i] != null && author[i] != "")
                {
                    BookAuthor au = new BookAuthor()
                    {
                        BookEntryRegisterId = add.BookEntryRegisterId,
                        AuthorName = author[i]
                    };
                    dc.BookAuthors.Add(au);
                    dc.SaveChanges();
                }
            }
            var NoOfBookid = dc.EntryBookIds.Select(q => q).ToList().Count;
            for (int i = (NoOfBookid + 1); i <= (NoOfBookid + noofcopy); i++)
            {
                EntryBookId bookid = new EntryBookId()
                {
                    BookEntryRegisterId = add.BookEntryRegisterId,
                    BookId = format + i,
                    StatusFlag = "IN",
                    status = true
                };
                dc.EntryBookIds.Add(bookid);
                dc.SaveChanges();
            }
        }
        public bool StudentBookIdExist(int PassReqId, int PassBookRegId)
        {
            int count;
            var row = (from a in dc.StudentBookTakenReturns

                       where a.BookEntryRegisterId == PassBookRegId && a.StudentRegisterId == PassReqId && a.StatusFlag =="IN"
                       select a).ToList();

            count = row.Count;

            if (count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public EntryBookId CheckBookStatus(string PassBookId)
        {
            //var getrow = dc.EntryBookIds.Where(q => q.BookId == PassBookId).FirstOrDefault();
            
            var row = (from a in dc.EntryBookIds

                       where a.BookId == PassBookId && a.StatusFlag=="IN"
                       select a).FirstOrDefault();

            return row;
        }
        public bool EmployeeBookIdExist(int PassBookRegId, int PassReqId)
        {
            int count;
            var row = (from a in dc.StaffBookTakenReturns

                       where a.BookEntryRegisterId == PassBookRegId && a.EmployeeRegisterId == PassReqId && a.StatusFlag == "IN"
                       select a).ToList();

            count = row.Count;

            if (count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        
        public List<BookEntryList> BookEntryDetails()
        {
            var ans = (from a in dc.BookEntries
                       
                       select new 
                       {
                           BookEntryRegisterId = a.BookEntryRegisterId,
                           BookTitle = a.BookTitle,
                           PublicationName = a.PublicationName,
                           lanaguage = a.BookLanaguage,
                           Edition = a.Edition,
                           BookType = a.BookType,
                           BookPrice = a.BookPrice,
                           NoOfCopys = a.NoOfCopies,
                           //BookIds=BookIdCollection(a.BookEntryRegisterId),
                           BookSection = a.BookSection,
                           RackNo = a.RackNo,
                           DateofEntrys=SqlFunctions.DateName("day", a.DateOfEntry).Trim() + " " + SqlFunctions.DateName("month", a.DateOfEntry).Remove(3) + ", " + SqlFunctions.DateName("year", a.DateOfEntry)
                           //DateofEntry = SqlFunctions.DateName("year", a.DateOfEntry) + "/" + SqlFunctions.StringConvert((double)a.DateOfEntry.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("day", a.DateOfEntry).Trim(),
                       //
                       }).ToList().Select(a => new BookEntryList
                       {
                           BookEntryRegisterId = a.BookEntryRegisterId,
                           BookTitle = a.BookTitle,
                           PublicationName = a.PublicationName,
                           Booklanaguage = a.lanaguage,
                           Edition = a.Edition,
                           BookType = a.BookType,
                           BookPrice = a.BookPrice,
                           NoOfCopy = a.NoOfCopys,
                           BookIds = GetbookStartIdandenid(a.BookEntryRegisterId),
                           BookSection = a.BookSection,
                           RackNo = a.RackNo,
                           DateofEntry = a.DateofEntrys
                           
                       }).Distinct().OrderBy(q => q.BookEntryRegisterId).ToList();
            return ans;
        }
        
        //public string BookIdCollection(int BookEntryRegisterId)
        //{

        //    var File = (from a in dc.EntryBookIds

        //                where a.BookEntryRegisterId == BookEntryRegisterId

        //                select new
        //                {
        //                    BookId = a.BookId

        //                }).ToList();


        //    string[] BookIds = new string[File.Count];
        //  for (int i = 0; i < File.Count; i++)
        //    {
        //           BookIds[i] = File[i].BookId;
        //    }
        //  string BookIdsList = string.Join(",", BookIds);
        //  return BookIdsList;

        //}
        public string GetbookStartIdandenid(int BookEntryRegisterId)
        {
            var s1 = (from a in dc.EntryBookIds
                      where a.BookEntryRegisterId == BookEntryRegisterId && a.status == true
                      select a).FirstOrDefault();
            var s2 = (from a in dc.EntryBookIds
                      where a.BookEntryRegisterId == BookEntryRegisterId && a.status == true
                      select a).OrderByDescending(q => q.BookRegisterId).FirstOrDefault();
            string ss = s1.BookId + "-" + s2.BookId;
            return ss;
        }
        
        public BookEntryList GetparticularbookDetails(int Bookregid)
        {
            var ans = (from a in dc.BookEntries
                       where a.BookEntryRegisterId == Bookregid
                       select new BookEntryList
                       {
                           BookEntryRegisterId = a.BookEntryRegisterId,
                           BookTitle = a.BookTitle,
                           PublicationName = a.PublicationName,
                           Booklanaguage = a.BookLanaguage,
                           Edition = a.Edition,
                           BookType = a.BookType,
                           BookPrice = a.BookPrice,
                           NoOfCopy = a.NoOfCopies,
                           BookSection = a.BookSection,
                           RackNo = a.RackNo,
                       }).FirstOrDefault();
            return ans;
        }
        public void BookEntryUpdate(int bookRegId, string Booktitle, string publication, string lanquage, string edition, string booktype, Decimal bookprice, string booksection, string rackno, int empregid)
        {
            var getBookEntry = dc.BookEntries.Where(q => q.BookEntryRegisterId == bookRegId).FirstOrDefault();
            if (getBookEntry != null)
            {
                getBookEntry.BookTitle = Booktitle;
                getBookEntry.PublicationName = publication;
                getBookEntry.BookLanaguage = lanquage;
                getBookEntry.Edition = edition;
                getBookEntry.BookType = booktype;
                getBookEntry.BookPrice = bookprice;
                getBookEntry.BookSection = booksection;
                getBookEntry.RackNo = rackno;
                getBookEntry.EmployeeRegisterId = empregid;
                dc.SaveChanges();
            }
        }
        public IEnumerable Getbooksection()
        {
            var ans = (from a in dc.BookEntries
                       select new
                       {
                           BookSectionId = a.BookSection,
                           BookSection = a.BookSection
                       }).Distinct().ToList();
            return ans;
        }
        public IEnumerable Getbooktitle()
        {
            var ans = (from a in dc.BookEntries
                       select new
                       {
                           BookTitleId = a.BookTitle,
                           BookTitle = a.BookTitle
                       }).Distinct().ToList();
            return ans;
        }
        public List<M_SearchBookList> GetSearchBookList(string title, int? authorid)
        {
            var ans = (from a in dc.BookEntries
                       from b in dc.BookAuthors
                       from c in dc.EntryBookIds
                       where a.BookTitle == title && b.BookAuthorId == authorid && b.BookEntryRegisterId == a.BookEntryRegisterId && c.BookEntryRegisterId == a.BookEntryRegisterId 
                       select new M_SearchBookList
                       {
                           BookEntryRegisterId = a.BookEntryRegisterId,
                           BookId=c.BookId,
                           BookTitle = a.BookTitle,
                           AuthorName = b.AuthorName,
                           BookEdition = a.Edition,
                           BookSection = a.BookSection,
                           PublicationName = a.PublicationName,
                           BookLanguage = a.BookLanaguage,
                           BookType = a.BookType,
                           RackNo = a.RackNo,
                           Status = c.StatusFlag,
                           TotalNoOfBook = a.NoOfCopies
                       }
                         ).Distinct().ToList().Select(q => new M_SearchBookList
                         {
                             BookId = q.BookId,
                             BookTitle = q.BookTitle,
                             AuthorName = q.AuthorName,
                             BookEdition = q.BookEdition,
                             BookSection = q.BookSection,
                             PublicationName = q.PublicationName,
                             BookLanguage = q.BookLanguage,
                             BookType = q.BookType,
                             RackNo = q.RackNo,
                             Status = q.Status,
                             TotalNoOfBook = q.TotalNoOfBook,
                             NoOfAvailable = GetNoOfbookAvailable(q.BookEntryRegisterId)
                         }).Distinct().ToList();
            return ans;
        }
        //public string GetbookStartIdandenid(int bookentryregid)
        //{
        //    var s1 = (from a in dc.EntryBookIds
        //              where a.BookEntryRegisterId == bookentryregid && a.status == true
        //              select a).FirstOrDefault();
        //    var s2 = (from a in dc.EntryBookIds
        //              where a.BookEntryRegisterId == bookentryregid && a.status == true
        //              select a).OrderByDescending(q => q.BookRegisterId).FirstOrDefault();
        //    string ss = s1.BookId + "-" + s2.BookId;
        //    return ss;
        //}
        public int GetNoOfbookAvailable(int bookentryregid)
        {
            var s1 = (from a in dc.EntryBookIds
                      where a.BookEntryRegisterId == bookentryregid && a.StatusFlag == "IN"
                      select a).Distinct().ToList();
            return s1.Count;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}