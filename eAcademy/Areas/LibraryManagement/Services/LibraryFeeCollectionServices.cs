﻿using eAcademy.Areas.LibraryManagement.Models;
using eAcademy.DataModel;
using System;
using System.Linq;
namespace eAcademy.Areas.LibraryManagement.Services
{
    public class LibraryFeeCollectionServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public int CollectFees(int acyear, int clas, int sec, int studentregid, Decimal fineamount, int empregid)
        {
            LibraryFeeCollection fee = new LibraryFeeCollection()
            {
                AcademicYearId = acyear,
                ClassId = clas,
                SectionId = sec,
                StudentRegisterId = studentregid,
                FineAmount = fineamount,
                TotalAmount = fineamount,
                EmployeeRegisterId = empregid
            };
            dc.LibraryFeeCollections.Add(fee);
            dc.SaveChanges();
            return fee.CollectionId;
        }
       
        public M_FineCollectionList GetFeeCollectiondetails(int id)
        {
            
            var ans = (from a in dc.LibraryFeeCollections
                       from b in dc.AcademicYears
                       from c in dc.Classes
                       from d in dc.Sections
                       from e in dc.Students
                       from f in dc.AssignClassToStudents
                       from g in dc.StudentRollNumbers                    
                       where a.CollectionId == id && e.StudentRegisterId == a.StudentRegisterId && f.StudentRegisterId == a.StudentRegisterId && f.Status == true && g.StudentRegisterId == a.StudentRegisterId && g.Status == true && b.AcademicYearId == a.AcademicYearId && c.ClassId == a.ClassId && d.SectionId == a.SectionId 
                       select new M_FineCollectionList
                       {
                           txt_AcademicYear = b.AcademicYear1,
                           txt_Class = c.ClassType,
                           txt_Section = d.SectionName,
                           StudentName = e.FirstName + " " + e.LastName,
                           StudentRollNumber = g.RollNumber,
                           StudentId = e.StudentId,
                           ReceiptNo = a.CollectionId,
                           old_FeeAmount = a.FineAmount,
                           old_TotalAmount = a.TotalAmount,                                        


                       }).Distinct().FirstOrDefault();
            return ans;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}