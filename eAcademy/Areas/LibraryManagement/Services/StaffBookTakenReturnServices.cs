﻿using eAcademy.Areas.LibraryManagement.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
namespace eAcademy.Areas.LibraryManagement.Services
{
    public class StaffBookTakenReturnServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public List<M_StaffBookTakenReturn> GetStaffOldBookDetails(int EmpReg)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var ans = (from a in dc.StaffBookTakenReturns
                       from b in dc.EntryBookIds
                       from c in dc.BookEntries
                       where a.EmployeeRegisterId == EmpReg && b.BookRegisterId == a.BookRegisterId && a.StatusFlag == "IN" && a.status == true && c.BookEntryRegisterId == b.BookEntryRegisterId
                       select new M_StaffBookTakenReturn
                       {
                           BookId = b.BookId,
                           BookTitle = c.BookTitle,
                           DateOfTaken = SqlFunctions.DateName("day", a.DateOfTaken).Trim() + " " + SqlFunctions.DateName("month", a.DateOfTaken).Remove(3) + ", " + SqlFunctions.DateName("year", a.DateOfTaken),
                           DateOfReturn = SqlFunctions.DateName("day", a.DateOfReturn).Trim() + " " + SqlFunctions.DateName("month", a.DateOfReturn).Remove(3) + ", " + SqlFunctions.DateName("year", a.DateOfReturn),
                           TakeDate = a.DateOfTaken,
                           RerunDate = a.DateOfReturn,
                           OverDuedays = System.Data.Objects.EntityFunctions.DiffDays(a.DateOfReturn, date),
                           Status = a.StatusFlag
                       }).Distinct().ToList().Select(a => new M_StaffBookTakenReturn
                       {
                           BookId = a.BookId,
                           BookTitle = a.BookTitle,
                           DateOfTaken = a.DateOfTaken,
                           DateOfReturn = a.DateOfReturn,
                           OverDuedays = a.OverDuedays,
                           TakeDate = a.TakeDate,
                           RerunDate = a.RerunDate,
                           Status = a.Status
                       }).ToList();
            return ans;
        }

        public void AddEmployeeBook(int Employeeregid, string EmployeeId, string[] bookid, string[] BookEntryId, string[] dateOftaken, string[] dateofreturn, int EmpRegid, int count)
        {
            string[] bid = new string[count];
            string[] beid = new string[count];
            string[] dot = new string[count];
            string[] dor = new string[count];
            for (int i = 0; i < count; i++)
            {
                bid[i] = String.Join(" ", bookid[i]);
                beid[i] = String.Join(" ", BookEntryId[i]);
                dot[i] = String.Join(" ", dateOftaken[i]);
                dor[i] = String.Join(" ", dateofreturn[i]);
                DateTime DOT = Convert.ToDateTime(dot[i]);
                DateTime DOR = Convert.ToDateTime(dor[i]);
                string bbid = Convert.ToString(bid[i]);
                if (bid[i] != "" && bid[i] != null && beid[i] != "" && beid[i] != null && DOT != null && DOR != null)
                {
                    var getrow = dc.EntryBookIds.Where(q => q.BookId == bbid).FirstOrDefault();
                    if (getrow != null)
                    {
                        StaffBookTakenReturn add = new StaffBookTakenReturn()
                        {
                            EmployeeRegisterId = Employeeregid,
                            EmployeeId = EmployeeId,
                            BookRegisterId = Convert.ToInt16(getrow.BookRegisterId),
                            BookEntryRegisterId = Convert.ToInt16(getrow.BookEntryRegisterId), 
                            DateOfTaken = DOT,
                            DateOfReturn = DOR,
                            StatusFlag = "IN",
                            status = true,
                            GivenBy = EmpRegid
                        };
                        dc.StaffBookTakenReturns.Add(add);
                        dc.SaveChanges();
                        getrow.StatusFlag = "OUT";
                        dc.SaveChanges();
                    }
                }
            }
        }
        public void EmployeeReturnbook(int Employeeregid, string[] BookId, int count, int EmpRegid)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            string[] bid = new string[count];
            for (int i = 0; i < count; i++)
            {
                bid[i] = String.Join(" ", BookId[i]);
                string bbid = Convert.ToString(bid[i]);
                if (bbid != null && bbid != "")
                {
                    var getrow = (from a in dc.StaffBookTakenReturns
                                  from b in dc.EntryBookIds
                                  where a.EmployeeRegisterId == Employeeregid && a.StatusFlag == "IN" && b.BookId == bbid && a.BookRegisterId == b.BookRegisterId
                                  select a).FirstOrDefault();
                    if (getrow != null)
                    {
                        getrow.StatusFlag = "OUT";
                        getrow.status = false;
                        getrow.DateOfReturn = date;
                        getrow.GivenBy = EmpRegid;
                        var GetEntryBook = dc.EntryBookIds.Where(q => q.BookRegisterId == getrow.BookRegisterId && q.StatusFlag == "OUT").FirstOrDefault();
                        {
                            GetEntryBook.StatusFlag = "IN";
                            dc.SaveChanges();
                        }
                        dc.SaveChanges();
                    }
                }
            }
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}