﻿using eAcademy.Areas.LibraryManagement.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects.SqlClient;
using System.Linq;
namespace eAcademy.Areas.LibraryManagement.Services
{
    public class StudentBookTakenReturnServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public List<M_StudentBooktakenReturn> GetStudentOldBookDetails(int StudentRegisterId)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var ans = (from a in dc.StudentBookTakenReturns
                       from b in dc.EntryBookIds
                       from c in dc.BookEntries
                       where a.StudentRegisterId == StudentRegisterId && b.BookRegisterId == a.BookRegisterId && a.StatusFlag == "IN" && a.status == true && c.BookEntryRegisterId == b.BookEntryRegisterId
                       select new M_StudentBooktakenReturn
                       {
                           BookId = b.BookId,
                           BookTitle = c.BookTitle,
                           BookEntryRegisterId=a.BookEntryRegisterId,
                           DateOfTaken = SqlFunctions.DateName("day", a.DateOfTaken).Trim() + " " + SqlFunctions.DateName("month", a.DateOfTaken).Remove(3) + ", " + SqlFunctions.DateName("year", a.DateOfTaken),
                           DateOfReturn = SqlFunctions.DateName("day", a.DateOfReturn).Trim() + " " + SqlFunctions.DateName("month", a.DateOfReturn).Remove(3) + ", " + SqlFunctions.DateName("year", a.DateOfReturn),
                           TakeDate = a.DateOfTaken,
                           RerunDate = a.DateOfReturn,
                           OverDuedays = System.Data.Objects.EntityFunctions.DiffDays(a.DateOfReturn, date), // find no of days from two dates
                           Status = a.StatusFlag
                       }).Distinct().ToList().Select(a => new M_StudentBooktakenReturn
                       {
                           BookId = a.BookId,
                           BookTitle = a.BookTitle,
                           BookEntryRegisterId=a.BookEntryRegisterId,
                           DateOfTaken = a.DateOfTaken,
                           DateOfReturn = a.DateOfReturn,
                           OverDuedays = a.OverDuedays,
                           TakeDate = a.TakeDate,
                           RerunDate = a.RerunDate,
                           Status = a.Status
                       }).ToList();
            return ans;
        }

        public void AddStudentBook(int StudentRegId, string sturollnumber, string[] bookid, string[] BookEntryId, string[] dateOftaken, string[] dateofreturn, int EmpRegid, int count)
        {
            string[] bid = new string[count];
            string[] beid = new string[count];
            string[] dot = new string[count];
            string[] dor = new string[count];

            for (int i = 0; i < count; i++)
            {
                bid[i] = String.Join(" ", bookid[i]);
                beid[i] = String.Join(" ", BookEntryId[i]);
                dot[i] = String.Join(" ", dateOftaken[i]);
                dor[i] = String.Join(" ", dateofreturn[i]);
                DateTime DOT = Convert.ToDateTime(dot[i]);
                DateTime DOR = Convert.ToDateTime(dor[i]);
                string bbid = Convert.ToString(bid[i]);
                if (bid[i] != "" && bid[i] != null && beid[i] != "" && beid[i] != null && DOT != null && DOR != null)
                {
                    var getrow = dc.EntryBookIds.Where(q => q.BookId == bbid).FirstOrDefault();
                    if (getrow != null)
                    {
                        StudentBookTakenReturn add = new StudentBookTakenReturn()
                        {
                            StudentRegisterId = StudentRegId,
                            StudentRollNumber = sturollnumber,
                            BookRegisterId = Convert.ToInt16(getrow.BookRegisterId),
                            BookEntryRegisterId = Convert.ToInt16(getrow.BookEntryRegisterId),
                            DateOfTaken = DOT,
                            DateOfReturn = DOR,
                            StatusFlag = "IN",
                            FineStatusFlag = "NO",
                            status = true,
                            GivenBy = EmpRegid
                        };
                        dc.StudentBookTakenReturns.Add(add);
                        dc.SaveChanges();
                        getrow.StatusFlag = "OUT";
                        dc.SaveChanges();
                    }
                }
            }
        }
        public void  StudentReturnbook(int StudentRegId, string[] BookId, int count, int EmpRegid)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            string[] bid = new string[count];
            for (int i = 0; i < count; i++)
            {
                bid[i] = String.Join(" ", BookId[i]);
                string bbid = Convert.ToString(bid[i]);
                if (bbid != null && bbid != "")
                {
                    var getrow = (from a in dc.StudentBookTakenReturns
                                  from b in dc.EntryBookIds
                                  where a.StudentRegisterId == StudentRegId && a.StatusFlag == "IN" && b.BookId == bbid && a.BookRegisterId == b.BookRegisterId && a.FineStatusFlag=="NO"
                                  select a).FirstOrDefault();
                    if (getrow != null)
                    {
                        getrow.StatusFlag = "OUT";
                        getrow.status = false;
                        getrow.DateOfReturn = date;
                        getrow.GivenBy = EmpRegid;
                        var GetEntryBook = dc.EntryBookIds.Where(q => q.BookRegisterId == getrow.BookRegisterId && q.StatusFlag == "OUT").FirstOrDefault();
                        {
                            GetEntryBook.StatusFlag = "IN";
                            dc.SaveChanges();
                        }
                        dc.SaveChanges();
                        
                    }
                }
            }
           
        }
        public void AfterFineReturn(int StudentRegId, string[] BookId, int count, int EmpRegid)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            string[] bid = new string[count];
            for (int i = 0; i < count; i++)
            {
                bid[i] = String.Join(" ", BookId[i]);
                string bbid = Convert.ToString(bid[i]);
                if (bbid != null && bbid != "")
                {
                    var getrow = (from a in dc.StudentBookTakenReturns
                                  from b in dc.EntryBookIds
                                  where a.StudentRegisterId == StudentRegId && a.StatusFlag == "IN" && b.BookId == bbid && a.BookRegisterId == b.BookRegisterId && a.FinePaid == "YES" && a.FineStatusFlag == "YES"
                                  select a).FirstOrDefault();
                    if (getrow != null)
                    {
                        getrow.StatusFlag = "OUT";
                        getrow.status = false;
                        getrow.DateOfReturn = date;
                        getrow.GivenBy = EmpRegid;
                        var GetEntryBook = dc.EntryBookIds.Where(q => q.BookRegisterId == getrow.BookRegisterId && q.StatusFlag == "OUT").FirstOrDefault();
                        {
                            GetEntryBook.StatusFlag = "IN";
                            dc.SaveChanges();
                        }
                        dc.SaveChanges();

                    }
                }
            }

        }
        public void  StudentFineEntry(int StudentRegId, string[] BookId, int count, int EmpRegid)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            string[] bid = new string[count];
            for (int i = 0; i < count; i++)
            {
                bid[i] = String.Join(" ", BookId[i]);
                string bbid = Convert.ToString(bid[i]);
                if (bbid != null && bbid != "")
                {
                    var getrow = (from a in dc.StudentBookTakenReturns
                                  from b in dc.EntryBookIds
                                  where a.StudentRegisterId == StudentRegId && a.StatusFlag == "IN" && b.BookId == bbid && a.BookRegisterId == b.BookRegisterId
                                  select a).FirstOrDefault();
                    if (getrow != null)
                    {

                        TimeSpan? Over = date.Subtract(getrow.DateOfReturn.Value);
                        int? OverDuedays = (int)Over.Value.TotalDays;
                        if (OverDuedays > 0)
                        {

                            var Fineamount = Convert.ToInt16(ConfigurationManager.AppSettings["LibraryFineAmount"]);//fine Re.1 per over days
                            getrow.FineAmount = OverDuedays * Fineamount;//fine Re.1 per over days
                            getrow.FineStatusFlag = "YES";
                            //getrow.StatusFlag = "IN";
                            getrow.FinePaid = "NO";
                            dc.SaveChanges();

                        }
                    }
                }
            }
        }
        public int GetStudentFineBookLis(int StudentRegId)
        {
            int AnsCount;
            var ans = (from a in dc.StudentBookTakenReturns
                       from b in dc.EntryBookIds
                       from c in dc.BookEntries
                       where a.StudentRegisterId == StudentRegId &&a.BookEntryRegisterId == c.BookEntryRegisterId && a.FineStatusFlag == "YES" && a.FinePaid=="NO" && a.StatusFlag=="IN"
                       select new StudentBookFineList
                       {
                           
                           BookTitle = c.BookTitle,
                           
                       }).Distinct().ToList();
            AnsCount = ans.Count;
            return AnsCount;
        }
        public List<M_FineCollectionList> CollectFineList(int? studentRegisterId, int? acyear, int? classs, int? sec)
        {
            var ans = (from a in dc.StudentBookTakenReturns
                       from b in dc.AssignClassToStudents
                       from c in dc.BookEntries
                       from d in dc.EntryBookIds
                       where a.StudentRegisterId == studentRegisterId && b.StudentRegisterId == a.StudentRegisterId && b.AcademicYearId == acyear && b.ClassId == classs && b.SectionId == sec && b.Status == true && b.HaveRollNumber == true && d.BookRegisterId == a.BookRegisterId && c.BookEntryRegisterId == d.BookEntryRegisterId && a.status == true && a.FineStatusFlag == "YES" &&  a.FinePaid == "NO"
                       select new M_FineCollectionList
                       {
                           StudentTakkenId = a.StudentTakenId,
                           BookId = d.BookId,
                           BookTitle = c.BookTitle,
                            Dateoftaken = SqlFunctions.DateName("day", a.DateOfTaken).Trim() + " " + SqlFunctions.DateName("month", a.DateOfTaken).Remove(3) + ", " + SqlFunctions.DateName("year", a.DateOfTaken),
                            DateofReturn = SqlFunctions.DateName("day", a.DateOfReturn).Trim() + " " + SqlFunctions.DateName("month", a.DateOfReturn).Remove(3) + ", " + SqlFunctions.DateName("year", a.DateOfReturn),
                           dot = a.DateOfTaken,
                           dor = a.DateOfReturn,
                           FineAmount = a.FineAmount,
                       }).Distinct().ToList().Select(q => new M_FineCollectionList
                       {
                           StudentTakkenId = q.StudentTakkenId,
                           BookId = q.BookId,
                           BookTitle = q.BookTitle,
                           DateofReturn = q.DateofReturn,
                           Dateoftaken = q.Dateoftaken,
                           FineAmount = q.FineAmount,
                           DueDays = GetOverduedays(q.dor, q.dot)
                       }).ToList();
            return ans;
        }
        public int? GetOverduedays(DateTime? returndate, DateTime? takedate)
        {
            DateTime dt = takedate.Value.AddDays(15);
            TimeSpan? difference = returndate - dt;
            int days = (int)difference.Value.TotalDays;
            return days;
        }
        public Decimal? getFineAmountTotal(IList<M_FineCollectionList> ss)
        {
            Decimal? sum = 0;
            foreach (var v in ss)
            {
                sum = sum + v.FineAmount;
            }
            return sum;
        }
        public void updatefinecollection(string[] id, int EmpRegid)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            int count = id.Length;
            string[] bid = new string[count];
            for (int i = 0; i < count; i++)
            {
                bid[i] = String.Join(" ", id[i]);
                int bbid = Convert.ToInt16(bid[i]);
                if (bbid != null && bbid != 0)
                {
                    var getrow = dc.StudentBookTakenReturns.Where(q => q.StudentTakenId == bbid).FirstOrDefault();
                    if (getrow != null)
                    {

                        getrow.FinePaid = "YES";
                        dc.SaveChanges();
                        
                    }
                }
            }
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}