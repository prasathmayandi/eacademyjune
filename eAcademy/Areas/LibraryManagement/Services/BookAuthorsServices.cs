﻿using eAcademy.DataModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace eAcademy.Areas.LibraryManagement.Services
{
    public class BookAuthorsServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public IEnumerable<Object> GetParticularBookAuthorDetails(int Bookregid)
        {
            var ans = (from a in dc.BookAuthors
                       where a.BookEntryRegisterId == Bookregid
                       select new
                       {
                           Authorid = a.BookAuthorId,
                           AuthorName = a.AuthorName
                       }).ToList();
            return ans;
        }
        public void UpdateBookAuthor(string[] Authorname, string[] AuthorId, int NoOfAuthor)
        {
            int count = Convert.ToInt16(NoOfAuthor);
            string[] author = new string[count];
            string[] authorid = new string[count];
            for (int i = 0; i < NoOfAuthor; i++)
            {
                author[i] = String.Join(" ", Authorname[i]);
                authorid[i] = String.Join(" ", AuthorId[i]);
                int auid = Convert.ToInt16(authorid[i]);
                if (author[i] != null && author[i] != "")
                {
                    var getAuthor = dc.BookAuthors.Where(q => q.BookAuthorId == auid).FirstOrDefault();
                    if (getAuthor != null)
                    {
                        getAuthor.AuthorName = author[i];
                        dc.SaveChanges();
                    }
                }
                else
                {
                    if (author[i] == "")
                    {
                        var getAuthor = dc.BookAuthors.Where(q => q.BookAuthorId == auid).FirstOrDefault();
                        if (getAuthor != null)
                        {
                            dc.BookAuthors.Remove(getAuthor);
                            dc.SaveChanges();
                        }
                    }
                }
            }
        }
        public IEnumerable GetBoookAuthor()
        {
            var ans = (from a in dc.BookAuthors
                       select new
                       {
                           AuthorId = a.AuthorName,
                           AuthorName = a.AuthorName
                       }).Distinct().ToList();
            return ans;
        }
        public IEnumerable GetrequiredBoookAuthor(string title)
        {
            var ans = (from a in dc.BookAuthors
                       from b in dc.BookEntries
                       where b.BookTitle == title && a.BookEntryRegisterId == b.BookEntryRegisterId
                       select new
                       {
                           AuthorId = a.BookAuthorId,
                           AuthorName = a.AuthorName
                       }).Distinct().ToList();
            return ans;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}