﻿using eAcademy.Areas.LibraryManagement.Models;
using eAcademy.DataModel;
using System;
using System.Linq;
namespace eAcademy.Areas.LibraryManagement.Services
{
    public class EntrybookIdServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public BookEntryList CheckBookId(string bookid)
        {
            var ans = (from a in dc.EntryBookIds
                       from b in dc.BookEntries
                       where a.BookId == bookid &&  b.BookEntryRegisterId == a.BookEntryRegisterId
                       select new BookEntryList
                       {
                           BookTitle = b.BookTitle,
                           BookEntryRegisterId=b.BookEntryRegisterId
                       }).FirstOrDefault();
                return ans;
        }
        public BookEntryList GetParticularbookdetailsUsingBookId(string bookid)
        {
            var ans = (from a in dc.EntryBookIds
                       from b in dc.BookEntries
                       where a.BookId == bookid && b.BookEntryRegisterId == a.BookEntryRegisterId && a.StatusFlag=="IN"
                       select new BookEntryList
                       {
                           BookTitle = b.BookTitle,
                           BookEntryRegisterId = b.BookEntryRegisterId
                       }).FirstOrDefault();
            return ans;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}