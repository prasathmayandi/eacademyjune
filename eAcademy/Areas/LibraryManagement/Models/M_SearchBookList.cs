﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.LibraryManagement.Models
{
    public class M_SearchBookList
    {
        public int BookEntryRegisterId { get; set; }
        public string BookId { get; set; }
        public string BookTitle { get; set; }
        public string BookEdition { get; set; }
        public string AuthorName { get; set; }
        public string PublicationName { get; set; }
        public string BookSection { get; set; }
        public string BookLanguage { get; set; }
        public string BookType { get; set; }
        public string RackNo { get; set; }
        public string Status { get; set; }
        public int? TotalNoOfBook { get; set; }
        public int NoOfAvailable { get; set; }
    }
}