﻿using System;

namespace eAcademy.Areas.LibraryManagement.Models
{
    public class M_StudentBooktakenReturn
    {
        public int BookTakenId { get; set; }
        public int StudentRegId { get; set; }
        public string StudentRollNumber { get; set; }
        public int BookRegisterId { get; set; }
        public DateTime? TakeDate { get; set; }
        public string DateOfTaken { get; set; }
        public DateTime? RerunDate { get; set; }
        public string DateOfReturn { get; set; }
        public double? OverDuedays { get; set; }
        public string Status { get; set; }
        public Decimal? FineAmount { get; set; }
        public string BookTitle { get; set; }
        public string BookId { get; set; }
        public TimeSpan? over { get; set; }
        public int? BookEntryRegisterId { get; set; }
    }
    public class StudentBookFineList
    {
        public int BookTakenId { get; set; }       
        public string BookTitle { get; set; }
        public string BookId { get; set; }
        
    }
}