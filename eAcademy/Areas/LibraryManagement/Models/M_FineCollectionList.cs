﻿using System;

namespace eAcademy.Areas.LibraryManagement.Models
{
    public class M_FineCollectionList
    {
        public int StudentTakkenId { get; set; }
        public string BookId { get; set; }
        public string BookTitle { get; set; }
        public string Dateoftaken { get; set; }
        public string DateofReturn { get; set; }
        public int? DueDays { get; set; }
        public Decimal? FineAmount { get; set; }
        public DateTime? dor { get; set; }
        public DateTime? dot { get; set; }

        public string txt_AcademicYear { get; set; }
        public string txt_Class { get; set; }
        public string txt_Section { get; set; }
        public string StudentName { get; set; }
        public string StudentRollNumber { get; set; }
        public string StudentId { get; set; }
        public int ReceiptNo { get; set; }
        public Decimal? old_FeeAmount { get; set; }
        public Decimal? old_TotalAmount { get; set; }
        public Decimal? old_Discount { get; set; }
        public Decimal? old_LateFee { get; set; }
        public Decimal? old_Vat { get; set; }
    }
}