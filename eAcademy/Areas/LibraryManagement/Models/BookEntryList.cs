﻿using System;
namespace eAcademy.Areas.LibraryManagement.Models
{
    public class BookEntryList
    {
        public int BookEntryRegisterId { get; set; }
        public string BookTitle { get; set; }
        public string[] AuthorName { get; set; }
        public string PublicationName { get; set; }
        public string Booklanaguage { get; set; }
        public string Edition { get; set; }
        public string BookType { get; set; }
        public Decimal? BookPrice { get; set; }
        public int? NoOfCopy { get; set; }
        public string BookIds{ get; set; }
        public string BookSection { get; set; }
        public string RackNo { get; set; }
        public int NoOfAuthor { get; set; }
        public DateTime date { get; set; }
        public string DateofEntry { get; set; }
        public string DateofEntrys{ get; set; }

    }
}