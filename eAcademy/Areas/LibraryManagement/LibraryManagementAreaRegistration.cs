﻿using System.Web.Mvc;

namespace eAcademy.Areas.LibraryManagement
{
    public class LibraryManagementAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "LibraryManagement";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "LibraryManagement_default",
                "LibraryManagement/{controller}/{action}/{id}",
                new { action = "BookRegister", id = UrlParameter.Optional }
            );
        }
    }
}
