﻿using eAcademy.Services;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
namespace eAcademy.Areas.LibraryManagement.Resources
{
    public class LibraryResources
    {
        public static string Enterauthorname         {
            get
            {
                return ResourceCache.Localize("Enterauthorname");
            }
        }
        public static string Enterbooktitle
        {
            get
            {
                return ResourceCache.Localize("Enterbooktitle");
            }
        }
        public static string Enterpublicationname
        {
            get
            {
                return ResourceCache.Localize("Enterpublicationname");
            }
        }
        public static string Enterbooklanaguage
        {
            get
            {
                return ResourceCache.Localize("Enterbooklanaguage");
            }
        }
        public static string Enterbooktype
        {
            get
            {
                return ResourceCache.Localize("Enterbooktype");
            }
        }
        public static string Enterbookprice
        {
            get
            {
                return ResourceCache.Localize("Enterbookprice");
            }
        }
        public static string Enternoofcopy
        {
            get
            {
                return ResourceCache.Localize("Enternoofcopy");
            }
        }
        public static string Enterbooksection
        {
            get
            {
                return ResourceCache.Localize("Enterbooksection");
            }
        }
        public static string Enterrackno
        {
            get
            {
                return ResourceCache.Localize("Enterrackno");
            }
        }
        public static string Pleaseselectacademicyear
        {
            get
            {
                return ResourceCache.Localize("Pleaseselectacademicyear");
            }
        }
        public static string Pleaseselectclass
        {
            get
            {
                return ResourceCache.Localize("Pleaseselectclass");
            }
        }
        public static string Pleaseselectsection
        {
            get
            {
                return ResourceCache.Localize("Pleaseselectsection");
            }
        }
        public static string Pleaseselectstudentname
        {
            get
            {
                return ResourceCache.Localize("Pleaseselectstudentname");
            }
        }
    }
}