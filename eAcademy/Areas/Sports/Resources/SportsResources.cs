﻿using eAcademy.Services;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Resources
{
    public class SportsResources
    {
         private static bool CultureEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["CultureEnabled"]);
        private static bool OrientationEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["OrientationEnabled"]);
        private static bool CountryEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["CountryEnabled"]);


        public SportsResources()
        {
            HttpSessionStateBase session = new HttpSessionStateWrapper(HttpContext.Current.Session);
            var CultureName = Convert.ToString(session["Language"]);
            var db = new ResourceValueService();
            if (CultureName == "")
            {
                CultureName = CultureInfo.CurrentUICulture.TextInfo.CultureName;
                session["Language"] = CultureName;
            }
            else
            {
                CultureName = CultureInfo.CurrentUICulture.TextInfo.CultureName;
                var list1 = db.GetResourceValues(ResourceType.UI, CultureEnabled ? CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);
                foreach (var item in list1) if (HttpRuntime.Cache.Get(item.Key) != null) HttpRuntime.Cache.Remove(item.Key);
                CultureName = Convert.ToString(session["Language"]);
            }
            // preload all localized text resources into cache

            var list = db.GetResourceValues(ResourceType.UI, CultureEnabled ? CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);
            foreach (var item in list) if (HttpRuntime.Cache.Get(item.Key) == null) HttpRuntime.Cache.Insert(item.Key, item.Value);
        }

        //public static string Localize(string key)
        //{

        //    if (HttpRuntime.Cache.Get(key) == null)
        //    {
        //        string value = ResourceValueService.GetResourceValue(key, CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

        //        if (!string.IsNullOrEmpty(value))
        //        {
        //            HttpRuntime.Cache.Insert(key, value);
        //        }
        //    }
        //    var val = HttpRuntime.Cache.Get(key);
        //    return val != null ? val.ToString() : null;
        //}

        //House
        public static string PleaseEnterHouse
        {
            get
            {
                return ResourceValueService.GetResourceValue("PleaseEnterHouse", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

            }
        }

        //Session
        public static string PleaseEnterSession
        {
            get
            {
                return ResourceValueService.GetResourceValue("PleaseEnterSession", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

            }
        }
        public static string PleaseSelectAcademicYear
        {
            get
            {
                return ResourceValueService.GetResourceValue("PleaseSelectAcademicYear", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

            }
        }

        //Game
        public static string PleaseEnterGameName
        {
            get
            {
                return ResourceValueService.GetResourceValue("PleaseEnterGameName", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

            }
        }

        public static string Pleaseselectgametype
        {
            get
            {
                return ResourceValueService.GetResourceValue("Pleaseselectgametype", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

            }
        }


        public static string PleaseEnterMainplayers
        {
            get
            {
                return ResourceValueService.GetResourceValue("PleaseEnterMainplayers", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

            }
        }

        public static string PleaseEnterSubplayers
        {
            get
            {
                return ResourceValueService.GetResourceValue("PleaseEnterSubplayers", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

            }
        }

        public static string Please_select_Points
        {
            get
            {
                return ResourceValueService.GetResourceValue("Please_select_Points", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

            }
        }

        //Game Level
        public static string Pleaseentergamelevelname
        {
            get
            {
                return ResourceValueService.GetResourceValue("Pleaseentergamelevelname", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

            }
        }

        public static string PleaseSelectSessionName
        {
            get
            {
                return ResourceValueService.GetResourceValue("PleaseSelectSessionName", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

            }
        }

        public static string PleaseSelectGameName
         {
             get
             {
                 return ResourceValueService.GetResourceValue("PleaseSelectGameName", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

             }
         }

        //public static string PleaseSelectAcademicYear
        // {
        //     get
        //     {
        //         return ResourceValueService.GetResourceValue("PleaseSelectAcademicYear", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

        //     }
        // }


        //Assign Game to Student
        public static string PleaseSelectSession
         {
             get
             {
                 return ResourceValueService.GetResourceValue("PleaseSelectSession", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

             }
         }

        public static string PleaseSelectClass
         {
             get
             {
                 return ResourceValueService.GetResourceValue("PleaseSelectClass", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

             }
         }

        public static string PleaseSelectSection
         {
             get
             {
                 return ResourceValueService.GetResourceValue("PleaseSelectSection", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

             }
         }

        public static string PleaseSelectStudent
         {
             get
             {
                 return ResourceValueService.GetResourceValue("PleaseSelectStudent", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

             }
         }

        public static string PleaseSelectHouseName
         {
             get
             {
                 return ResourceValueService.GetResourceValue("PleaseSelectHouseName", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

             }
         }

        public static string pleaseselectStudentType
         {
             get
             {
                 return ResourceValueService.GetResourceValue("pleaseselectStudentType", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

             }
         }

        public static string PleaseSelectLevel
         {
             get
             {
                 return ResourceValueService.GetResourceValue("PleaseSelectLevel", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

             }
         }



        //Game Schedule
        public static string PleaseSelectLevelName
         {
             get
             {
                 return ResourceValueService.GetResourceValue("PleaseSelectLevelName", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

             }
         }

        public static string PleaseSelectPoint
         {
             get
             {
                 return ResourceValueService.GetResourceValue("PleaseSelectPoint", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

             }
         }

        public static string PleaseSelectScheduleDate
         {
             get
             {
                 return ResourceValueService.GetResourceValue("PleaseSelectScheduleDate", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

             }
         }

        public static string PleaseSelectScheduleTime
         {
             get
             {
                 return ResourceValueService.GetResourceValue("PleaseSelectScheduleTime", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

             }
         }

        //Session Result
        public static string PleaseSelectYear
        {
            get
            {
                return ResourceValueService.GetResourceValue("PleaseSelectYear", CultureEnabled ? CultureInfo.CurrentUICulture.TextInfo.CultureName : ConfigurationManager.AppSettings["DefaultCultureValue"], OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"], CountryEnabled ? ConfigurationManager.AppSettings["CountryCode"] : ConfigurationManager.AppSettings["DefaultCountryCode"]);

            }
        }








    }
}