﻿using eAcademy.Areas.Sports.Models;
using eAcademy.DataModel;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eAcademy.HelperClass;
using System.Data.Objects.SqlClient;

namespace eAcademy.Areas.Sports.Services
{
    public class IndividualStudentReportServices : IDisposable
    {

        EacademyEntities db = new EacademyEntities();


        //public IEnumerable<Object> getClass(int passYearId)
        //{

        //    var List = (from t1 in db.AssignClassToStudents
        //                from t2 in db.AcademicYears
        //                from t3 in db.Classes
        //                where
        //                t1.AcademicYearId == passYearId &&
        //               t1.AcademicYearId==t2.AcademicYearId  
        //               &&  t1.ClassId==t3.ClassId
        //                select new
        //                { 
        //                    ClassId =t3.ClassId,
        //                    ClassName = t3.ClassType
        //                }).Distinct().ToList().AsEnumerable();

        //    return List;
        //}

        //public IEnumerable<Object> getStudentService(int passYearId, int passClassId, int passSecId)
        //{
        //    var List = (from t1 in db.Students
        //                    from t2 in db.AssignClassToStudents
        //                    from t3 in db.AcademicYears
        //                    from t4 in db.Classes
        //                    from t5 in db.Sections
        //                    where
        //              t1.StudentRegisterId==t2.StudentRegisterId &&
        //              t2.AcademicYearId == passYearId &&
        //              t2.ClassId == passClassId &&
        //              t2.SectionId == passSecId &&
        //              t2.AcademicYearId==t3.AcademicYearId &&
        //              t2.ClassId==t4.ClassId &&
        //              t2.SectionId==t5.SectionId
        //                select new 
        //                    {
        //                       StudentRegisterId=t1.StudentRegisterId,
        //                       StudentName=t1.FirstName+" "+t1.LastName,
        //                    }).ToList();
           

        //    return List;
        //}


        public List<IndividualStudentReportValidation> getIndividualStudentReportList(int? AcademicYearId, int? ClassId, int? SectionId, int? StudentRegId)
        {
            var List = (from t1 in db.SportsAssignGametoStudents
                        from t2 in db.SportsAssignGametoStudentConfigs
                        from t3 in db.Students
                        from t4 in db.AssignClassToStudents
                        from t5 in db.SportsHouses
                        from t6 in db.SportsSessions
                        from t7 in db.SportsGames
                        from t8 in db.SportsGameLevelConfigs
                        from t9 in db.AcademicYears
                        from t10 in db.SportsGamePoints
                        from t11 in db.Classes
                        from t12 in db.Sections
                        where
                       t1.AssignGamestoStudentId == t2.AssignGametoStudentId &&
                       t1.AcademicYearId == AcademicYearId &&
                       t1.ClassId == ClassId &&
                       t1.SectionId == SectionId &&
                       t1.StudentRegisterId == StudentRegId &&
                       t1.StudentRegisterId == t3.StudentRegisterId &&
                       t1.ClassId == t4.ClassId &&
                       t1.ClassId == t11.ClassId &&
                       t1.StudentRegisterId == t4.StudentRegisterId &&
                       t1.SectionId == t4.SectionId &&
                       t1.SectionId == t12.SectionId &&
                       t1.SessionId==t6.SessionId &&
                       t1.HouseId == t5.HouseId &&
                       t1.AcademicYearId==t9.AcademicYearId &&
                       t2.PointId == t10.PointId &&
                       t2.GameId==t7.GameId
                       && t2.GameLevelId==t8.Id
                        select new IndividualStudentReportValidation
                        {
                            StudentRegId=t1.StudentRegisterId,
                            AcademicYear = t9.AcademicYear1,
                            ClassName = t11.ClassType,
                            SectionName = t12.SectionName,
                            SessionName = t6.SessionName,
                            GameName = t7.GameName,
                            LevelName = t8.GameLevelName,
                            StudentType = t1.StudentType,
                            HouseName = t5.HouseName,
                            GameType = t7.GameType,
                            PrizeStatus = t10.PointName,
                            Marks = t2.Marks,
                            Description = t2.Description
                        }).Distinct().ToList().Select(s => new IndividualStudentReportValidation
                        {
                            StudentRegId=s.StudentRegId,
                            AcademicYear = s.AcademicYear,
                            ClassName = s.ClassName,
                            SectionName = s.SectionName,
                            SessionName = s.SessionName,
                            GameName = s.GameName,
                            LevelName = s.LevelName,
                            StudentType = s.StudentType,
                            HouseName = s.HouseName,
                            GameType = s.GameType,
                            PrizeStatus = s.PrizeStatus,
                            Marks = s.Marks,
                            Description = s.Description
                        }).ToList();


            return List;

        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        }
}
