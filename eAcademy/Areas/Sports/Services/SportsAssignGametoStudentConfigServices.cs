﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Services
{
    public class SportsAssignGametoStudentConfigServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        public void AddGameDetails(int AssgId,int GameId,int LevelId, int PointId, int Marks, string Description)
        {
            SportsAssignGametoStudentConfig add = new SportsAssignGametoStudentConfig()
            {
                AssignGametoStudentId=AssgId,
                GameId = GameId,
                GameLevelId=LevelId,
                PointId = PointId,
                Marks = Marks,
                Description = Description
                
            };
            db.SportsAssignGametoStudentConfigs.Add(add);
            db.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}