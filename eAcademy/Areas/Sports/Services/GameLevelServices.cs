﻿using eAcademy.Areas.Sports.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Areas.Sports.Services
{
    public class GameLevelServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public IEnumerable<Object> ShowLevels()
        {
            var LevelList = (from a in db.SportsGameLevels
                            from b in db.SportsGameLevelConfigs
                             select new
                             {
                                 LevelId = a.LevelId,
                                 GameLevelName = b.GameLevelName,
                             }).ToList().AsEnumerable();
            return LevelList;
        }

        public SportsGameLevel checkrecordsexist(int acyear, int SessionId, int GameId)
        {
           
            var Records = (from a in db.SportsGameLevels
                       where a.AcademicYearId == acyear && a.SessionId == SessionId && a.GameId == GameId
                       select a).FirstOrDefault();
            return Records;
        }

        //public IEnumerable<Object> getGameDetail(int passYearId)
        //{
        //    var GameList = (from b in db.SportsGames
        //                    select new
        //                    {

        //                        GameName = b.GameName,
        //                        GameId = b.GameId

        //                    }).Distinct().ToList().AsEnumerable();


        //    return GameList;
        //}
        //public IEnumerable<Object> getSessionDetails(int passYearId)
        //{
        //    var SessionList = (from a in db.SportsSessions
        //                       where a.AcademicYearId == passYearId
        //                       select new
        //                       {
        //                           SessionName = a.SessionName,
        //                           SessionId = a.SessionId

        //                       }).ToList().AsEnumerable();


        //    return SessionList;
        //}


        //public IEnumerable<Object> getHouselist()
        //{
        //    var GameHouseList = (from a in db.SportsHouses
        //                         select new
        //                         {
        //                             HouseName = a.HouseName,
        //                             HouseId = a.HouseId

        //                         }).ToList().AsEnumerable();


        //    return GameHouseList;
        //}


        public SportsAssignGametoStudentConfig CheckGameLevelExist(int LevelId)
        {
            var row = (from a in db.SportsAssignGametoStudentConfigs
                       where a.GameLevelId == LevelId
                       select a).FirstOrDefault();
            return row;
        }


       
        public void DeleteGameLevelName(int id)
        {
            var delete = db.SportsGameLevelConfigs.Where(query => query.Id.Equals(id)).FirstOrDefault();
            db.SportsGameLevelConfigs.Remove(delete);
            db.SaveChanges();
        }


        public IEnumerable<Object> getLevellist()
        {
            var Levellist = (from a in db.SportsGameLevelConfigs
                             select new
                             {
                                 LevelId = a.GameLevelId,
                                 LevelName = a.GameLevelName
                             }).ToList().AsEnumerable();
            return Levellist;
        }

       
        public int addGameLevel(int acyear, int SessionId, int GameId, int LevelId, int empid)
        {
            DateTime DateWithTime = DateTimeByZone.getCurrentDate();
            SportsGameLevel add = new SportsGameLevel()
            {
              
                AcademicYearId = acyear,
                SessionId = SessionId,
                GameId = GameId,             
                Status = true,
                Createdby = empid,
                Updatedby = empid,
                CreatedDate = DateWithTime,
                UpdatedDate = DateWithTime

            };
            db.SportsGameLevels.Add(add);
            db.SaveChanges();
            int id = add.LevelId;
            return id;

        }

     
        public void AddGameLevelExits(int LevelId, int SessionId, int GameId, int acyear, string LevelName, bool Status)
        {
            // 
            var update = db.SportsGameLevels.Where(query => query.LevelId.Equals(LevelId)).FirstOrDefault();
            if (update != null)
            {

                update.SessionId = SessionId;
                update.GameId = GameId;
                update.AcademicYearId = acyear;
                update.Status = Status;

                db.SaveChanges();
            }
        }

        public string GetGameNameLevels(int? GLNid)
        {

            var File = (from a in db.SportsGameLevels
                        from b in db.SportsGameLevelConfigs
                        where a.LevelId == b.GameLevelId &&
                        a.LevelId == GLNid
                        select new
                        {
                            GameLevelId = b.GameLevelId,
                            GameLevelName = b.GameLevelName
                        }).ToList();

            string[] Levels = new string[File.Count];
            for (int i = 0; i < File.Count; i++)
            {
                Levels[i] = File[i].GameLevelName;
            }
            string Levelslist = string.Join(",", Levels);
            return Levelslist;

        }

        public List<GameLevelValidation> getGameLevelName()
        {
            var val = (from t1 in db.SportsGameLevels
                       from t2 in db.AcademicYears
                       from t3 in db.SportsGames
                       from t4 in db.SportsSessions
                    

                       where t1.AcademicYearId == t2.AcademicYearId &&
                       t1.GameId == t3.GameId &&
                       t1.SessionId == t4.SessionId

                       select new GameLevelValidation
                       {
                           LevelId = t1.LevelId,
                           AcademicYearId = t2.AcademicYear1,                    
                           SessionName = t4.SessionName,
                           GameName = t3.GameName,
                           Status = t1.Status,

                       }).ToList().Select(a => new GameLevelValidation
                       {
                           LevelId = a.LevelId,
                           AcademicYearId = a.AcademicYearId,                  
                           SessionName = a.SessionName,
                           GameName = a.GameName,
                           Status = a.Status,
                           GameLevelName = GetGameNameLevels(a.LevelId)
                       }).ToList();

            return val;
        }


        public bool checkAcitivity(int acyear, int SessionId, int GameId, int LevelId)
        {
            int count;

            var ans = (from t1 in db.SportsGameLevels
                       where t1.AcademicYearId == acyear && t1.SessionId == SessionId && t1.GameId == GameId && t1.LevelId != LevelId
                       select t1).ToList();

            count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public EditGameLevel getEditGameLevel(int GLid)
        {

            var row = (from a in db.SportsGameLevels
                       from b in db.SportsGameLevelConfigs
                       from c in db.AcademicYears
                       from d in db.SportsSessions
                       where a.LevelId == GLid && a.AcademicYearId ==c.AcademicYearId && a.SessionId == d.SessionId
                       select new EditGameLevel
            {

                LevelId = a.LevelId,
                GameLevelName = b.GameLevelName,          
                GameId = a.GameId,
                acyear = a.AcademicYearId,
                AcademicYearName = c.AcademicYear1,
                SessionName = d.SessionName,
                SessionId = a.SessionId,
                Status = a.Status
            }).FirstOrDefault();
            return row;
        }

        public List<EditGameLevel> getEditGameLevelName(int Gid)
        {

            var RowLevels = (from a in db.SportsGameLevels
                             from b in db.SportsGameLevelConfigs
                             where a.LevelId == b.GameLevelId &&
                             a.LevelId == Gid
                             select new
                             {
                                 AcademicYearname =  a.AcademicYearId,
                                 sessionname = a.SessionId,
                                GameLevelId = b.GameLevelId,
                                GameLevelName = b.GameLevelName,  
                                ConID =b.Id,
                                LevelId = a.LevelId
                             }).ToList().Select(x => new EditGameLevel
                             {
                                 acyear = x.AcademicYearname,
                                 SessionId = x.sessionname,
                                GameLevelName = x.GameLevelName,
                                Id = x.ConID ,                           
                                GameLevelId = x.GameLevelId,
                                LevelId = x.LevelId
                             }).ToList();
            return RowLevels;
        }
     
        public void updateGameLevel(int LevelId, int SessionId, int GameId, int acyear, string LevelName, bool Status)
        {
            // 
            var update = db.SportsGameLevels.Where(query => query.LevelId.Equals(LevelId)).FirstOrDefault();
            if (update != null)
            {
              
                update.SessionId = SessionId;
                update.GameId = GameId;
                update.AcademicYearId = acyear;             
                update.Status = Status;

                db.SaveChanges();
            }
        }

        public void UpdateGameLevelname(int Idarr ,int GameLevelId, string GameLevelName)
        {
            var Updatearr = db.SportsGameLevelConfigs.Where(q => q.Id.Equals(Idarr)).FirstOrDefault();
            if(Updatearr != null)
            {
              
                Updatearr.GameLevelId = GameLevelId;
                Updatearr.GameLevelName = GameLevelName;
            }       
            db.SaveChanges();
        }

        //public bool checkDeleteActivity(int LevelId)
        //{
        //    int count;
        //    var d = (from a in db.SportsAssignGametoStudentConfigs
        //             where a.GameLevelId == LevelId
        //             select a).ToList();
        //    count = d.Count;
        //    if (count > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}


        public bool checkDeleteActivity(int Pid)
        {
            int count;
            var d = (from a in db.SportsAssignGametoStudentConfigs
                     where a.GameLevelId == Pid
                     select a).ToList();
            count = d.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public IEnumerable<Object> getPrimaryIds(int LevelId)
        {
            var ListPId = (from a in db.SportsGameLevelConfigs
                           where a.GameLevelId == LevelId
                           select new
                           {
                              id=a.Id
                           }).ToList().AsEnumerable();


            return ListPId;
        }


        //public IEnumerable<Object> getPrimaryIds(int LevelId)
        //{
        //    var ListPId = (from a in db.SportsGameLevelConfigs
        //                   where a.GameLevelId == LevelId
        //                   select new
        //                   {
        //                       GameLevelId = a.GameLevelId
        //                   }).ToList().AsEnumerable();


        //    return ListPId;
        //}



        //public void deleteLevel(int LevelId)
        //{
        //    var delete = db.SportsGameLevelConfigs.Where(query => query.GameLevelId.Equals(LevelId)).FirstOrDefault();
        //    db.SportsGameLevelConfigs.Remove(delete);
        //    db.SaveChanges();
        //}
        public void deleteLevel(int LevelId)
        {
            var delete = db.SportsGameLevels.Where(query => query.LevelId.Equals(LevelId)).FirstOrDefault();
            while (db.SportsGameLevelConfigs.Where(x => x.GameLevelId == LevelId).Count() != 0)
            {
                var row = db.SportsGameLevelConfigs.Where(x => x.GameLevelId == LevelId).FirstOrDefault();
                db.SportsGameLevelConfigs.Remove(row);
                db.SaveChanges();
            }
            db.SportsGameLevels.Remove(delete);
            db.SaveChanges();
        }
        


        //public List<GameLevelList> getGameLevelSearch()
        //{
        //    var val = (from t1 in db.SportsLevelReports
        //               from t2 in db.AcademicYears
        //               from t3 in db.SportsGames
        //               from t4 in db.SportsSessions
        //               from t5 in db.Students
        //               from t6 in db.SportsHouses
        //               from t7 in db.SportsGamePoints
        //               from t8 in db.SportsGameLevels

        //               where
        //               t1.AcademicYearId == t2.AcademicYearId &&
        //               t1.GameId == t3.GameId &&
        //               t1.SessionId == t4.SessionId &&
        //               t1.LevelId == t8.LevelId &&
        //               t1.StudentId == t5.StudentRegisterId &&
        //               t1.HouseId == t6.HouseId &&
        //               t1.PointId == t7.PointId

        //               select new GameLevelList
        //               {
        //                   GameLevelReportId = t1.LevelReportId,
        //                   Acyear = t2.AcademicYearId,
        //                   SessionName = t4.SessionName,
        //             //      LevelName = t8.GameLevelName,
        //                   StudentName = t5.FirstName,
        //                   HouseName = t6.HouseName,
        //                   PrizeStatus = t7.PointName,
        //                   GameName = t3.GameName

        //               }).Distinct().ToList().Select(a => new GameLevelList
        //               {
        //                   GameLevelReportId = a.GameLevelReportId,
        //                   Acyear = a.Acyear,
        //                   SessionName = a.SessionName,
        //                   LevelName = a.LevelName,
        //                   GameName = a.GameName,
        //                   StudentName = a.StudentName,
        //                   HouseName = a.HouseName,
        //                   PrizeStatus = a.PrizeStatus


        //               }).Distinct().ToList();

        //    return val;
        //}



        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        
    }
}

