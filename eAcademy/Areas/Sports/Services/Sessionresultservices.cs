﻿using eAcademy.Areas.Sports.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Services
{
    public class Sessionresultservices : IDisposable                                                                                                                                                                             
    {

        EacademyEntities db = new EacademyEntities();

        public IEnumerable<Object> getGameDetail(int passYearId)
        {
            var GameList = (from b in db.SportsGames
                           
                            select new
                            {
                                GameName = b.GameName,
                                GameId = b.GameId

                            }).Distinct().ToList().AsEnumerable();


            return GameList;
        }

        public IEnumerable<Object> getSessionDetails(int passYearId)
        {
            var SessionList = (from a in db.SportsSessions
                               where a.AcademicYearId == passYearId
                               select new
                               {
                                   SessionName = a.SessionName,
                                   SessionId = a.SessionId

                               }).ToList().AsEnumerable();
            return SessionList;
        }


        public IEnumerable<Object> getHouseid( int YearId, int  SessionsId, int  GamesId)
        {
            var GameHouseList = (from a in db.SportsSessionResults
                                 where a.AcademicYearId == YearId &&
                                 a.SessionId == SessionsId &&
                                 a.GameId == GamesId
                                 select new
                                 {                                   
                                     HouseId = a.HouseId

                                 }).ToList().AsEnumerable();

            return GameHouseList;
        }

        
        public IEnumerable<Object> getHouselist()
        {
            var GameHouseList = (from a in db.SportsHouses
                                 select new
                                 {
                                     HouseName = a.HouseName,
                                     HouseId = a.HouseId

                                 }).ToList().AsEnumerable();

            return GameHouseList;
        }


        public void addSessionResult(int acyear, int GameId, int SessionId, int HouseId, bool status)
        {
            DateTime DateWithTime = DateTimeByZone.getCurrentDate();
            SportsSessionResult add = new SportsSessionResult()
            {
                
                AcademicYearId = acyear,
                GameId = GameId,
                SessionId = SessionId,              
                HouseId = HouseId,
                Status = status,
                CreatedDate = DateWithTime,
                UpdatedDate = DateWithTime                 
           
            };
            db.SportsSessionResults.Add(add);
            db.SaveChanges();
        }

        public List<SportsReportValidation> GetDetails(int passYearId)
        {
            var getRow = (from t1 in db.SportsSessionResults
                          where
                          t1.AcademicYearId == passYearId &&
                           t1.Status == true
                          select new SportsReportValidation
                          {               
                              SessionResultId = t1.SessionResultId,
                              GameId = t1.GameId,
                              SessionId = t1.SessionId,
                              HouseId = t1.HouseId
                             
                          }).ToList();   
            return getRow;
        }

        public List<SportsSessionResultCount> GetSessionCount(int passYearId)
        {
            var getRow = (from t1 in db.SportsSessionResults
                          where
                          t1.AcademicYearId == passYearId &&
                           t1.Status == true
                          select new SportsSessionResultCount
                          {                             
                              SessionId = t1.SessionId                            
                          }).Distinct().ToList();
            return getRow;
        }

        public List<SportsGameResultCount> GetGameCount(int passYearId)
        {
            var getRow = (from t1 in db.SportsSessionResults
                          where
                          t1.AcademicYearId == passYearId &&
                           t1.Status == true
                          select new SportsGameResultCount
                          {
                             GameId = t1.GameId
                          }).Distinct().ToList();
            return getRow;
        }


        public void UpdateHouseId(int editHouseId,int editIdupdate)
        {
            var Updatearr = db.SportsSessionResults.Where(q => q.SessionResultId.Equals(editIdupdate)).FirstOrDefault();
            if (Updatearr != null)
            {
                Updatearr.HouseId = editHouseId;               
            }
            db.SaveChanges();
        }

        public void AddNewHouseId(int editHouseId, bool status)
        {
            DateTime DateWithTime = DateTimeByZone.getCurrentDate();
            SportsSessionResult add = new SportsSessionResult()
            {               
                HouseId = editHouseId,
                Status = status,
                CreatedDate = DateWithTime,
                UpdatedDate = DateWithTime

            };
            db.SportsSessionResults.Add(add);
            db.SaveChanges();
        }
       
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}