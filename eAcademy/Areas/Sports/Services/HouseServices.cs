﻿using eAcademy.Areas.Sports.Controllers;
using eAcademy.Areas.Sports.Models;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eAcademy.HelperClass;

namespace eAcademy.Areas.Sports.Services
{
    public class HouseServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public IEnumerable<Object> ShowHouses()
        {
            var HouseList = (from a in db.SportsHouses
                                    select new
                                    {
                                        HouseId = a.HouseId,
                                        HouseName = a.HouseName
                                    }).ToList().AsEnumerable();
            return HouseList;
        }

        public void addhouse(string HouseName, int EmployeeId)
        {
            SportsHouse add = new SportsHouse()
                {
       HouseName = HouseName,
       Status= true,
       Createdby = EmployeeId,
       Created_date = DateTimeByZone.getCurrentDateTime(),
                 };
            db.SportsHouses.Add(add);
            db.SaveChanges();
        }

        public List<HouseValidation> getHouseName()
        {
            var ans = (from t1 in db.SportsHouses
                       select new HouseValidation
                       {
                      HouseId=t1.HouseId,
                       HouseName = t1.HouseName,
                           Status = t1.Status
                       }).ToList();
            return ans;
        }
           public EditHouse getEditHouse(int Hid)
        {

            var row = (from a in db.SportsHouses
                       where a.HouseId == Hid
                       select new EditHouse
                        {
                            HouseId = a.HouseId,
                            HName = a.HouseName,
                            status = a.Status
                        }).FirstOrDefault();
            return row;
        }
           public void updateHouse(int HouseId, string HName, bool Status, int EmpregId)
        {
            var update = db.SportsHouses.Where(query => query.HouseId.Equals(HouseId)).FirstOrDefault();
            if (update != null)
            {
                update.HouseName = HName;
                update.Status = Status;
                update.Updatedby = EmpregId;
                update.Updated_date = DateTimeByZone.getCurrentDateTime();
                db.SaveChanges();
            }
        }


        public SportsHouse checkAddActivity(string HouseName)
        {
            var row =(from t in db.SportsHouses where t.HouseName == HouseName select t).FirstOrDefault();
            return row;
        }


        //public bool checkAddActivity(string HouseName)
        //{
        //    int count;
        //    var ans = (from t in db.SportsHouses where t.HouseName == HouseName select t).ToList();
        //    count = ans.Count;
        //    if(count>0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        public bool checkActivity(int HouseId,string HouseName)
        {
            int count;
            //var c = (from t1 in db.SportsHouses where t1.HouseId == HouseId && t1.HouseName == HouseName select t1).ToList();
            var c = (from t1 in db.SportsHouses where t1.HouseId != HouseId && t1.HouseName == HouseName select t1).ToList();
            count = c.Count;
            if(count>0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void deleteHouse(int id)
        {
            var delete = db.SportsHouses.Where(query => query.HouseId.Equals(id)).FirstOrDefault();
            db.SportsHouses.Remove(delete);
            db.SaveChanges();
        }


       public bool checkDeleteActivity(int HouseId)
        {
            int count;
            var d = (from a in db.SportsAssignGametoStudents where a.HouseId == HouseId select a).ToList();
            count = d.Count;
           if(count>0)
           {
               return true;
           }
           else
           {
               return false;
           }
        }



       private bool disposed = false;
       protected virtual void Dispose(bool disposing)
       {
           if (!this.disposed)
           {
               if (disposing)
               {
                   db.Dispose();
               }
           }
       }
       public void Dispose()
       {
           Dispose(true);
           GC.SuppressFinalize(this);
       }


    }
}

