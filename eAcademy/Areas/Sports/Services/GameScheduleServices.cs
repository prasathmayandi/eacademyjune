﻿using eAcademy.Areas.Sports.Models;
using eAcademy.DataModel;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eAcademy.HelperClass;
using System.Data.Objects.SqlClient;

namespace eAcademy.Areas.Sports.Services
{
    public class GameScheduleServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

//ADDSCHEDULE-START
        public int addgamescheduleService(int acyear, int SessionId, int GameId, int LevelConFigId, string GameType, DateTime EventDate, TimeSpan Eventtime, int empid)
        {
            var CDate = DateTimeByZone.getCurrentDateTime();
            SportsGameSchedule add = new SportsGameSchedule()
            {
                AcademicYearId = acyear,
                SessionId = SessionId,
                GameId = GameId,
                GameType = GameType,
              // playerName = playername,
               // HouseId = Houseid,
               // PlayerStatusId = PlayerStatus,
                ScheduleDate = EventDate,
                ScheduleTime = Eventtime,
                Status = true,
                CreatedBy = empid,
                CreatedDate = CDate,
                LevelConFigId = LevelConFigId
            };
            db.SportsGameSchedules.Add(add);
            db.SaveChanges();
            var gsid = add.GameScheduleId;
            return gsid;
        }


        //Get Gametype from db
        public string getGameType(int PGameId)
        {
            string gametype= (from a in db.SportsGames
                        where
                        a.GameId == PGameId
                        select 
                         a.GameType
                        ).FirstOrDefault();
            return gametype;
        }

        public IEnumerable<Object> getLevel(int passGameId)
        {
            var List = (from a in db.SportsGameLevels
                        from b in db.SportsGameLevelConfigs
                        where a.LevelId == b.GameLevelId&&
                        a.GameId == passGameId
                        select new
                        {
                            GameLevelId = b.Id,
                            GameLevelName = b.GameLevelName
                        }).ToList();
            return List;
        }

       

        public IEnumerable<Object> getSession(int passAcademicYearId)
        {
            var List = (from a in db.AcademicYears
                        from b in db.SportsSessions
                        where a.AcademicYearId == b.AcademicYearId &&
                        a.AcademicYearId == passAcademicYearId
                        select new
                        {
                            SessionId = b.SessionId,
                            SessionName = b.SessionName
                        }).ToList();
            return List;
        }

        public IEnumerable<Object> getHouselist()
        {
            var GameHouseList = (from a in db.SportsHouses
                                 select new
                                 {
                                     HouseName = a.HouseName,
                                     HouseId = a.HouseId

                                 }).ToList().AsEnumerable();


            return GameHouseList;
        }
        //Get Gamepoints from db
        public IEnumerable<Object> getPointlist(int passGameId)
        {
            var PointList = ( from t1 in db.SportsGamePointConfigs
                             from t2 in db.SportsGamePoints
                             where t1.GameId == passGameId &&
                                 t1.PointId==t2.PointId
                             select new
                             {
                                 PointId = t1.PointId,
                                 PointValue = t2.PointValue,
                                 PointName = t2.PointName
                             }).ToList().Select(s => new
                            
                               {
                                   PointName = s.PointName + "(" + s.PointValue + " Points)",
                                   PointId=s.PointId
                               }).ToList().AsEnumerable();
            return PointList;

        }




        ////Get Gamepoints from db
        //public IEnumerable<Object> getPoint()
        //{
        //    var GamePointList = (from a in db.SportsGamePoints
        //                         select new
        //                         {
        //                             PointName = a.PointName,
        //                             PointValue = a.PointValue,
        //                             PointId = a.PointId

        //                         }).ToList().Select(a => new
        //                         {
        //                             PointName = a.PointName + " (" + a.PointValue + " Points)",
        //                             PointId = a.PointId
        //                         }).ToList().AsEnumerable();


        //    return GamePointList;
        //}

        //ADDGAMESCHEDULE-END
         //from t7 in db.SportsHouses
         //              from t8 in db.SportsGamePoints
        //LISTGAMESCHEDULE
        public List<GameScheduleValidation> getGameSchedule(int? fAcademicYear, int? fSessionName)
        {
            var val = (from t1 in db.SportsGameSchedules
                       from t2 in db.SportsGamesheduleConfigs
                       from t3 in db.SportsGames
                       from t4 in db.SportsGameLevelConfigs
                       where
                       t1.GameScheduleId == t2.GameScheduleId &&
                       t1.GameId == t3.GameId &&
                       t1.LevelConFigId==t4.Id &&
                       t1.AcademicYearId == fAcademicYear &&
                       t1.SessionId == fSessionName
                       select new
                       { 
                           GameScheduleId = t1.GameScheduleId,
                           GameType = t3.GameType,
                           GameName=t3.GameName,
                           LevelName=t4.GameLevelName,
                           Date = SqlFunctions.DateName("day", t1.ScheduleDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.ScheduleDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.ScheduleDate),
                           Time = t1.ScheduleTime.Value,
                           Status = t1.Status
                       }).Distinct().ToList().Select(s => new GameScheduleValidation
                       {
                           GameScheduleId = s.GameScheduleId,
                           GameName=s.GameName,
                           LevelName=s.LevelName,
                           GameType = s.GameType,
                           HouseName = GetHouseWithPoint(s.GameScheduleId),
                           Date = s.Date,
                           // Time=s.Time,
                           GTime = s.Time.ToString(@"hh\:mm"),
                           Status = s.Status
                       }).Distinct().ToList();

            return val;

        }


        public string GetHouseWithPoint(int GameScheduleId)
        {
            var hp = (from t1 in db.SportsGamesheduleConfigs
                      from t2 in db.SportsHouses
                      from t3 in db.SportsGamePoints
                      from t4 in db.SportsGameSchedules
                      where
                      t1.GameScheduleId == GameScheduleId &&
                      t1.HouseId == t2.HouseId &&
                      t1.PointId == t3.PointId
                      select new
                      {
                          // HouseId = t2.HouseId,
                          HouseName = t2.HouseName,
                          // PointId = t3.PointId,
                          PointName = t3.PointName,
                          PointValue = t3.PointValue
                      }).Distinct().ToList().Select(s => new GameScheduleValidation
                    {
                        HouseNameWithPoints = s.HouseName + "-" + s.PointName + " (" + s.PointValue + " Points)"
                    }).Distinct().ToList();

            string[] HouseNameList = new string[hp.Count];
            for (int i = 0; i < hp.Count; i++)
            {
                HouseNameList[i] = hp[i].HouseNameWithPoints;
            }
            string PointsList = string.Join(",", HouseNameList);
            return PointsList;

        }




       

//EditGameSchedule

        public EditGameSchedule getEditGameschedule(int EGSid)
        {
            var row = (from t1 in db.SportsGameSchedules
                       from t2 in db.SportsGamesheduleConfigs
                       from t3 in db.AcademicYears
                       from t4 in db.SportsSessions
                       from t5 in db.SportsGames
                       from t6 in db.SportsGameLevelConfigs
                       where
                       t1.GameScheduleId == EGSid &&
                       t1.AcademicYearId == t3.AcademicYearId &&
                       t1.SessionId == t4.SessionId &&
                       t1.GameId == t5.GameId &&
                       t1.LevelConFigId == t6.Id
                       select new
                       {
                           GameScheduleId = t1.GameScheduleId,
                           AcademicYearId = t3.AcademicYearId,
                           AcademicYear = t3.AcademicYear1,
                           SessionId = t4.SessionId,
                           SessionName = t4.SessionName,
                           //HouseId = t7.HouseId,
                           //HouseName = t7.HouseName,
                           //PointId = t8.PointId,
                           //PointName = t8.PointName,
                           GameId = t5.GameId,
                           GameName = t5.GameName,
                           LevelId=t6.Id,
                           LevelName = t6.GameLevelName,
                           GameType = t5.GameType,
                           //EDate = t1.ScheduleDate,
                           EDate = SqlFunctions.DateName("day", t1.ScheduleDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.ScheduleDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.ScheduleDate),
                           ETime = t1.ScheduleTime.Value,
                           Status = t1.Status
                       }).Distinct().ToList().Select(s => new EditGameSchedule
                         {
                             GameScheduleId = s.GameScheduleId,
                             AcademicYearId = s.AcademicYearId,
                             AcademicYear = s.AcademicYear,
                             SessionName = s.SessionName,
                             SessionId = s.SessionId,
                             //HouseId=s.HouseId,
                             //HouseName=s.HouseName,
                             //PointId=s.PointId,
                             //PointName=s.PointName,
                             GameId = s.GameId,
                             GameName = s.GameName,
                             LevelId = s.LevelId,
                             LevelName = s.LevelName,
                             GameType = s.GameType,
                             //EDate = s.EDate.Value.ToString(@"dd\:mm\:yyyy"),
                             EDate = s.EDate,
                             ETime = s.ETime.ToString(@"hh\:mm"),
                             Status = s.Status
                         }).FirstOrDefault();

            return row;
        }

        public List<EditGameSchedule> getEditDetailGameschedule(int EGSid)
        {

            var editrow=(from t1 in db.SportsGamesheduleConfigs
                         from t2 in db.SportsHouses
                         from t3 in db.SportsGamePoints
                         where
                         t1.GameScheduleId == EGSid &&
                         t1.HouseId==t2.HouseId &&
                         t1.PointId==t3.PointId 
                         select new 
                     {
                    ScheduleId=t1.SheduleConfigId,
                     HouseId=t1.HouseId,
                     HouseName=t2.HouseName,
                     PointId=t1.PointId,
                     PointName = t3.PointName
                     }).Distinct().ToList().Select(s=>new EditGameSchedule
                     {
                         ScheduleConfigId=s.ScheduleId,
                         HouseId=s.HouseId,
                         HouseName=s.HouseName,
                         PointId=s.PointId,
                         PointName=s.PointName
                     }).Distinct().ToList();

            return editrow;

        }

        public IEnumerable<Object> getPointDetail(int passGameId)
        {
            var PointList = (from t1 in db.SportsGameSchedules
                             from t2 in db.SportsGamesheduleConfigs
                             from t3 in db.SportsGamePoints
                             where 
                             t1.GameId == passGameId &&
                             t2.PointId== t3.PointId
                             select new
                             {
                                PointId=t2.PointId,
                                PointName=t3.PointName,
                                PointValue=t3.PointValue
                             }).ToList().Select(s => new

                             {
                                 PointName = s.PointName + "(" + s.PointValue + " Points)",
                                 PointId = s.PointId
                             }).Distinct().ToList();
            return PointList;

        }


        public IEnumerable<Object> getEditHouselist()
        {
            var GameHouseList = (from a in db.SportsHouses
                                 select new
                                 {
                                     HouseName = a.HouseName,
                                     HouseId = a.HouseId

                                 }).ToList().AsEnumerable();


            return GameHouseList;
        }

        public IEnumerable<Object> getEditPointlist()
        {
            var GamePointList = (from a in db.SportsGamePoints
                                 select new
                                 {
                                     PointName = a.PointName,
                                     PointValue = a.PointValue,
                                     PointId = a.PointId

                                 }).ToList().Select(a => new
                                 {
                                     PointName = a.PointName + " (" + a.PointValue + " Points)",
                                     PointId = a.PointId
                                 }).ToList().AsEnumerable();


            return GamePointList;
        }

//delete row
        public void deleteSchedule(int passGameScheduleConficId)
        {
            var delete = db.SportsGameSchedules.Where(query => query.GameScheduleId.Equals(passGameScheduleConficId)).FirstOrDefault();
            db.SportsGameSchedules.Remove(delete);
           // db.SaveChanges();
        }

//Update
        public void UpdateGameSchedule(int GameScheduleId, DateTime dt, TimeSpan testtime, int EmpId, bool Status)
        {
            var update = db.SportsGameSchedules.Where(query => query.GameScheduleId.Equals(GameScheduleId)).FirstOrDefault();
            if (update != null)
            {
                //update.EDate = Date.ToString(@"dd\:mm\:yyyy");
                //update.ETime = Time.ToString(@"hh\:mm");
               
                //update.ScheduleDate = Date.ToString(@"dd\:mm\:yyyy");
                //update.ScheduleTime = Time.ToString(@"hh\:mm");
                update.ScheduleDate = dt;
                update.ScheduleTime = testtime;
                update.UpdatedBy = EmpId;
                update.Status = Status;
               // update.EDate = DateTimeByZone.getCurrentDateTime();
                //update.EDate = DateTimeByZone.getCurrentDateTime();
                update.UpdatedDate = DateTimeByZone.getCurrentDateTime();
                db.SaveChanges();
            }
        }


        public void UpdateGameScheduleDetails(int SchedConId, int HId, int PId)
        {
            var update = db.SportsGamesheduleConfigs.Where(query => query.SheduleConfigId.Equals(SchedConId)).FirstOrDefault();
            if (update != null)
            {
                update.HouseId = HId;
                update.PointId = PId;
                db.SaveChanges();
            }

        }


        public bool checkActivity(int GameScheduleId, int HId, int PId)
        {
            int count;

            //var ans = (from t1 in db.SportsGamesheduleConfigs
            //           where t1.SheduleConfigId == SchedConId && t1.HouseId == HId && t1.PointId == PId
            //           select t1).ToList();
            var ans = (from t1 in db.SportsGamesheduleConfigs
                       where t1.GameScheduleId == GameScheduleId && t1.HouseId == HId && t1.PointId == PId
                       select t1).ToList();


            count = ans.Count;

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public void DeleteGameSched(int GameScheduleId)
        {
            var delete = db.SportsGameSchedules.Where(query => query.GameScheduleId.Equals(GameScheduleId)).FirstOrDefault();
            db.SportsGameSchedules.Remove(delete);
            db.SaveChanges();
        }

        public bool CheckDeleteActivity(int GameScheduleId)
        {
            int count;
            var d = (from t1 in db.SportsGamesheduleConfigs
                     where t1.GameScheduleId == GameScheduleId
                     select t1).ToList();
            count = d.Count;
            if(count>0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void DeleteGameScheduleConfig(int GameScheduleId)
        {
            while (db.SportsGamesheduleConfigs.Where(d => d.GameScheduleId == GameScheduleId).Count()!=0)
            {
                var del = db.SportsGamesheduleConfigs.Where(query => query.GameScheduleId.Equals(GameScheduleId)).FirstOrDefault();
            
            db.SportsGamesheduleConfigs.Remove(del);
            db.SaveChanges();
            }
        }


        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }


    }
