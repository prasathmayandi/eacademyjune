﻿using eAcademy.Areas.Sports.Models;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eAcademy.HelperClass;
namespace eAcademy.Areas.Sports.Services
{
    public class AssignGametoStudentServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public IEnumerable<Object> getGameDetail()
        {
            var GameList = (from a in db.SportsGames
                            select new
                            {
                                GameName = a.GameName,
                                GameId = a.GameId

                            }).Distinct().ToList().AsEnumerable();


            return GameList;
        }
        public IEnumerable<Object> EditgetLevel()
        {
            var List = (from b in db.SportsGameLevelConfigs
                        select new
                        {

                            LevelId = b.Id,
                            LevelName= b.GameLevelName
                        }).ToList();

            return List;


        }
        public IEnumerable<Object> EditGetMarks()
        {
            var List = (from b in db.SportsAssignGametoStudentConfigs
                        select new
                        {
                            GameAssConfigs = b.AssignGametoStudentConficId,
                            GameMark = b.Marks
                        }).ToList();

            return List;


        }
        public IEnumerable<Object> EditGetDescription()
        {
            var List = (from b in db.SportsAssignGametoStudentConfigs
                        select new
                        {
                            GameAssConfigs = b.AssignGametoStudentConficId,
                            GameDescription = b.Description
                        }).ToList();

            return List;


        }
        

        public SportsAssignGametoStudent CheckGameNameExist(int acyear, int classId, int sectionId, int stuId)
        {
            var row = (from a in db.SportsAssignGametoStudents
                       where a.StudentRegisterId == stuId &&
                       a.AcademicYearId == acyear &&
                       a.ClassId == classId &&
                       a.SectionId == sectionId
                       select a).FirstOrDefault();
            return row;
        }

        public IEnumerable<Object> getprizelist()
        {
            var GamePointList = (from a in db.SportsGamePoints
                                 select new
                                 {
                                     PointName = a.PointName,
                                     PointValue = a.PointValue,
                                     PointId = a.PointId

                                 }).ToList().Select(a => new
                                 {
                                     PointName = a.PointName + " (" + a.PointValue + " Points)",
                                     PointId = a.PointId
                                 }).ToList().AsEnumerable();


            return GamePointList;
        }
        public IEnumerable<Object> getHouselist()
        {
            var GameHouseList = (from a in db.SportsHouses
                                 select new
                                 {
                                     HouseName = a.HouseName,
                                     HouseId = a.HouseId

                                 }).ToList().AsEnumerable();


            return GameHouseList;
        }
        public IEnumerable<Object> getStudents1(int passYearId, int classId, int secId)
        {
            var StudentList = (from a in db.AssignClassToStudents
                               from b in db.Students
                               where a.SectionId == secId &&
                                     a.ClassId == classId &&
                                     a.AcademicYearId == passYearId &&
                                     a.StudentRegisterId == b.StudentRegisterId &&
                                     b.StudentStatus == true
                               select new
                               {
                                   StudentRegId = a.StudentRegisterId,
                                   StudentName = b.FirstName + " " + b.LastName
                               }).ToList();

            return StudentList;
        }
        //public IEnumerable<object> getClassWithSection(int passYearId)
        //{
        //    var getList = (from a in db.AssignFacultyToSubjects
        //                   from b in db.Classes
        //                   from c in db.Sections
        //                   where a.ClassId == b.ClassId &&
        //                   a.SectionId == c.SectionId &&
        //                   b.Status == true &&

        //                   a.AcademicYearId == passYearId
        //                   select new
        //                   {
        //                       ClassName = b.ClassType,
        //                       SecName = c.SectionName,
        //                       ClassId = a.ClassId,
        //                       SecId = a.SectionId
        //                   }).ToList().Select(c => new
        //                   {
        //                       ClassNameSecName = c.ClassName + " " + c.SecName,
        //                       ClassIdSecId = c.ClassId + " " + c.SecId
        //                   }).Distinct().ToList();
        //    return getList;
        //}

        public int addStudent(int acyear, int SessionId, int ClassId, int SectionId, int StudentId, int HouseId, string StudentType,int EmployeeId)
        {
            SportsAssignGametoStudent add = new SportsAssignGametoStudent()
            {
                AcademicYearId = acyear,
                SessionId = SessionId,
                ClassId = ClassId,
                SectionId = SectionId,
                StudentRegisterId = StudentId,
                HouseId = HouseId,                
                StudentType = StudentType,
                
                Status = true,
                CreatedBy = EmployeeId,
                CreatedDate = DateTimeByZone.getCurrentDateTime(),
                

            };
            db.SportsAssignGametoStudents.Add(add);
            db.SaveChanges();            
            int id = add.AssignGamestoStudentId;
            return id;
        }



        public List<StudentGamevalidation> getstudentgameList(int? fAcyear, int? fSession, int? fClass, int? fSection)
        {
            var val = (from t1 in db.SportsAssignGametoStudents
                       from t3 in db.AcademicYears
                       from t4 in db.SportsGames
                       from t5 in db.Students
                       from t6 in db.SportsHouses
                       from t7 in db.SportsGamePoints
                       from t8 in db.SportsGameLevelConfigs
                       from t9 in db.SportsSessions
                       from t10 in db.AssignClassToStudents
                       where
                       t1.AcademicYearId == fAcyear &&
                       t1.AcademicYearId == t3.AcademicYearId &&
                       t1.StudentRegisterId == t5.StudentRegisterId &&
                       t1.HouseId == t6.HouseId &&
                       t1.SessionId == fSession &&
                       t1.SessionId == t9.SessionId &&
                       t1.ClassId == fClass &&
                       t1.ClassId == t10.ClassId &&
                       t1.SectionId == fSection &&
                       t1.SectionId == t10.SectionId

                       select new StudentGamevalidation
                       {
                           AssignGamestoStudentId = t1.AssignGamestoStudentId,
                           AcademicYear = t3.AcademicYear1,
                           AcademicYearId = t3.AcademicYearId,
                           ClassId = t1.ClassId.Value,
                           SectionId = t1.SectionId.Value,
                           StudentregId = t1.StudentRegisterId,
                           StudentFName = t5.FirstName,
                           StudentLName = t5.LastName,
                           SessionId = t9.SessionId,
                           HouseName = t6.HouseName,
                           StudentType = t1.StudentType,
                           Status = t1.Status,

                       }).Distinct().ToList().Select(a => new StudentGamevalidation
                       {
                           GameId = a.GameId,
                           AcademicYearId = a.AcademicYearId,
                           ClassId = a.ClassId,
                           SectionId = a.SectionId,
                           SessionId = a.SessionId,
                           AssignGamestoStudentId = a.AssignGamestoStudentId,
                           AcademicYear = a.AcademicYear,
                           StudentName = a.StudentFName + "" + a.StudentLName,
                           HouseName = a.HouseName,
                           StudentType = a.StudentType,
                           GameName = GetGameNameWithLevel(a.AssignGamestoStudentId),
                           Status = a.Status
                       }).ToList();

            return val;
        }
        
        public string GetGameNameWithLevel(int? AssignGamestoStudentId)
        {

            var File = (from a in db.SportsAssignGametoStudentConfigs
                        from b in db.SportsGames
                        from c in db.SportsGameLevels
                        from d in db.SportsGameLevelConfigs
                        where
                        a.AssignGametoStudentId == AssignGamestoStudentId && a.GameLevelId == d.Id //&& a.GameLevelId == d.GameLevelId && c.LevelId == d.GameLevelId 
                        &&
                        a.GameId == b.GameId  
                        select new
                        {
                            a.AssignGametoStudentConficId,
                            LevelName =d.GameLevelName,
                            GameName = b.GameName
                        }).Distinct().OrderByDescending(c => c.AssignGametoStudentConficId).ToList().Select(x => new StudentGamevalidation
                        {
                            GameNameWithLevel = x.GameName + "(" + x.LevelName + ")",
                        }).ToList(); ;


            string[] Gamelevels = new string[File.Count];
            for (int i = 0; i < File.Count; i++)
            {
                Gamelevels[i] = File[i].GameNameWithLevel;
            }
            string PointsList = string.Join(",", Gamelevels);
            return PointsList;

        }




        public bool checkAcitivity(int AssignGametoStudentId, int GameId, int LevelId, int PointId)
        {
            int count;

            var ans = (from t1 in db.SportsAssignGametoStudentConfigs
                       where t1.AssignGametoStudentId == AssignGametoStudentId && t1.GameId == GameId && t1.GameLevelId == LevelId && t1.PointId == PointId 
                       select t1).ToList();

            count = ans.Count;

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public EditStudentGame getEditGamestudent(int EGSid)
        {

            var row = (from t1 in db.SportsAssignGametoStudents                       
                       from t2 in db.AcademicYears                      
                       from t3 in db.Students
                       from t4 in db.SportsHouses                      
                       from t5 in db.SportsSessions
                       from t6 in db.AssignClassToStudents
                       from t7 in db.Classes
                       from t8 in db.Sections
                       where
                       t1.AssignGamestoStudentId==EGSid&&
                       t1.AcademicYearId == t2.AcademicYearId &&
                       t1.StudentRegisterId == t3.StudentRegisterId &&
                       t1.HouseId == t4.HouseId &&
                       t1.SessionId == t5.SessionId &&
                       t1.ClassId == t7.ClassId &&
                       t1.SectionId == t8.SectionId
                       select new EditStudentGame
                       {
                           AssignGametoStudentId = t1.AssignGamestoStudentId,
                           Acyear = t2.AcademicYear1,
                           AcyearId = t2.AcademicYearId,
                           SessionId = t1.SessionId.Value,
                           SessionName=t5.SessionName,
                           ClassId = t1.ClassId.Value,
                           Class=t7.ClassType,
                           SectionId = t1.SectionId.Value,
                           Section=t8.SectionName,
                           HouseId = t1.HouseId.Value,
                           HouseName=t4.HouseName,
                           StudentRegisterId = t1.StudentRegisterId,
                           StudentName=t3.FirstName+""+t3.LastName,
                           StudentType = t1.StudentType,
                           Status = t1.Status
                       }).FirstOrDefault();
            return row;
        }
        public List<EditStudentGame> getEditGameDetails(int EGSid)
        {
            var row = (from t1 in db.SportsAssignGametoStudentConfigs
                       from t2 in db.SportsGameLevelConfigs
                       from t3 in db.SportsGames
                       from t4 in db.SportsGamePoints
                       where t1.AssignGametoStudentId == EGSid &&
                       t1.GameId==t3.GameId &&
                       t1.PointId==t4.PointId &&
                       t1.GameLevelId==t2.Id
                       select new 
                       {
                           AssignGamestoStudentConfigId = t1.AssignGametoStudentConficId,
                           GameName=t3.GameName,
                           GameId =t1.GameId,
                           LevelName=t2.GameLevelName,
                           levelId=t1.GameLevelId,
                           PointName = t4.PointName,
                           PointId=t1.PointId,
                           PointValue = t4.PointValue,
                           GameMark = t1.Marks,
                           GameDescription = t1.Description

                       }).Distinct().ToList().Select(x => new EditStudentGame
                             {
                                 AssignGamestoStudentConfigId = x.AssignGamestoStudentConfigId,
                                 GameName=x.GameName,
                                 GameId=x.GameId,
                                 LevelName = x.LevelName,
                                 LevelId = x.levelId,
                                 PointName = x.PointName + "(" + x.PointValue + ")",
                                 PointId=x.PointId,
                                 GameMark = x.GameMark,
                                 GameDescription = x.GameDescription
                                
                             }).OrderByDescending(c => c.AssignGamestoStudentConfigId).ToList();
            return row;

        }
        public void updateGameStudent(int AssignGamestoStudentId, int HouseId, string StudentType, int EmployeeId,bool Status)
        {
            var update = db.SportsAssignGametoStudents.Where(query => query.AssignGamestoStudentId.Equals(AssignGamestoStudentId)).FirstOrDefault();
            if (update != null)
            {

                
                update.HouseId = HouseId;
                update.StudentType = StudentType;
                update.UpdatedBy = EmployeeId;
                update.Status = Status;
                update.UpdatedDate = DateTimeByZone.getCurrentDateTime();
                
                db.SaveChanges();
            }
        }
        public void UpdateGameDetails(int AssgConfigId, int GameId, int LevelId, int PointId, int Marks, string Description)
        {
            var update = db.SportsAssignGametoStudentConfigs.Where(query => query.AssignGametoStudentConficId.Equals(AssgConfigId)).FirstOrDefault();
            if (update != null)
            {
                update.GameId = GameId;
                update.GameLevelId = LevelId;
                update.PointId = PointId;
                update.Marks = Marks;
                update.Description = Description;
                db.SaveChanges();
            }

        }



        


        public void updateGameStudent(int AssignGamestoStudentId, int acyear, int HouseId, int LevelId, string Studenttype, int GameId, int PointId, int MArks, string Description, bool Status)
        {
            var update = db.SportsAssignGametoStudents.Where(query => query.AssignGamestoStudentId.Equals(AssignGamestoStudentId)).FirstOrDefault();
            if (update != null)
            {
                update.AcademicYearId = acyear;
                update.HouseId = HouseId;
                
                update.StudentType = Studenttype;
                
                update.Status = Status;

                db.SaveChanges();
            }
        }




        public void DeleteStudentGame(int AssignGamestoStudentId)
        {
            var delete = db.SportsAssignGametoStudents.Where(query => query.AssignGamestoStudentId.Equals(AssignGamestoStudentId)).FirstOrDefault();
            db.SportsAssignGametoStudents.Remove(delete);
            db.SaveChanges();
        }

        public void DeleteStudentGameDetails(int AssignGametoStudentConficId)
        {
            while (db.SportsAssignGametoStudentConfigs.Where(x => x.AssignGametoStudentConficId == AssignGametoStudentConficId).Count() != 0)
            {
                var delete = db.SportsAssignGametoStudentConfigs.Where(query => query.AssignGametoStudentConficId.Equals(AssignGametoStudentConficId)).FirstOrDefault();
                db.SportsAssignGametoStudentConfigs.Remove(delete);
                db.SaveChanges();
            }
        }
 //       public List<IndividualStudentValidation> getIStudentList()
 //       {
 //           var val = (from t1 in db.SportsIndividualStudents
 //                      from t2 in db.AcademicYears
 //                      from t3 in db.SportsGames
 //                      from t4 in db.Students
 //                      from t5 in db.SportsHouses
 //                      from t6 in db.SportsGamePoints
 //                      from t7 in db.SportsGameLevels

 //                      where
 //                      t1.AcademicYearId == t2.AcademicYearId &&
 //                      t1.GameId == t3.GameId &&
 //                      t1.StudentRegisterId == t4.StudentRegisterId &&
 //                      t1.HouseId == t5.HouseId &&
 //                      t1.LevelId == t7.LevelId
 //                      && t1.PointId == t6.PointId
 //                      select new IndividualStudentValidation
 //                      {
 //                   IStudentId = t1.IStudentId,
 //                          AcademicYear = t2.AcademicYear1,
 //                          AcademicYearId = t2.AcademicYearId,
 //                          ClassId = t1.ClassId.Value,
 //                          SectionId = t1.SectionId.Value,
 //                          StudentregId = t1.StudentRegisterId,
 //                          prize = t6.PointName,
                           
 //                          HouseName = t5.HouseName,
 //                          StudentType = t1.StudentType,
 //                          StudentName = t4.FirstName,
 //                          GameName = t3.GameName,
 //                          Status = t1.Status,
 //                          Marks = t1.Marks,
 //                          Description = t1.Description,

 //                      }).Distinct().ToList().Select(a=> new IndividualStudentValidation
 //                      {
 //                          IStudentId=a.IStudentId,
 //                          AcademicYear=a.AcademicYear,
 //                          AcademicYearId=a.AcademicYearId,
 //                          ClassId=a.ClassId,
 //                          SectionId=a.SectionId,
 //                          StudentregId=a.StudentregId,
 //                          prize=a.prize,
 //                          LevelName=a.LevelName,
 //                          HouseName=a.HouseName,
 //                          StudentType=a.StudentType,
 //                          StudentName=a.StudentName,
 //                          GameName=a.GameName,
 //                          Status =a.Status,
 //                          Marks=a.Marks,
 //                          Description=a.Description
 //}).Distinct().ToList();

 //           return val;
 //       }



        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}