﻿using eAcademy.Areas.Sports.Models;
using eAcademy.DataModel;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eAcademy.HelperClass;
using System.Data.Objects.SqlClient;

namespace eAcademy.Areas.Sports.Services
{
    public class GameLevelReportServices : IDisposable 
    {

       EacademyEntities db = new EacademyEntities();

       public IEnumerable<Object> getSession(int passAcademicYearId)
       {
           var List = (from a in db.AcademicYears
                       from b in db.SportsSessions
                       where a.AcademicYearId == b.AcademicYearId &&
                       a.AcademicYearId == passAcademicYearId
                       select new
                       {
                           SessionId = b.SessionId,
                           SessionName = b.SessionName
                       }).ToList();
           return List;
       }

       public IEnumerable<object> getGameName(int passAcademicYearId)
       {
           var List = (from a in db.SportsAssignGametoStudents
                       from b in db.SportsAssignGametoStudentConfigs 
                       from c in db.SportsGames
                       where
                       a.AssignGamestoStudentId==b.AssignGametoStudentConficId &&
                       a.AcademicYearId == passAcademicYearId &&
                       b.GameId==c.GameId
                       select new
                       {   GameId=c.GameId,
                           GameName =c.GameName
                       }).Distinct().ToList().AsEnumerable();
           return List;

       }


       public IEnumerable<Object> getLevel(int passGameId)
       {
           var List = (from a in db.SportsGameLevels
                       from b in db.SportsGameLevelConfigs
                       where a.LevelId == b.GameLevelId &&
                       a.GameId == passGameId
                       select new
                       {
                           GameLevelId = b.Id,
                           GameLevelName = b.GameLevelName
                       }).ToList();
           return List;
       }


       public List<GameLevelReportValidation> getLevelReportList(int? AcademicYear, int? SessionName, int? Level, int? GameName)
        {
            var List = (from t1 in db.SportsAssignGametoStudents
                        from t2 in db.SportsAssignGametoStudentConfigs
                        from t3 in db.Students
                        from t4 in db.AssignClassToStudents
                        from t5 in db.SportsHouses
                        from t6 in db.SportsSessions
                        from t7 in db.SportsGames
                        from t8 in db.SportsGameLevelConfigs
                        from t9 in db.AcademicYears
                        from t10 in db.SportsGamePoints
                        from t11 in db.Classes
                        from t12 in db.Sections
                        where
                       t1.AssignGamestoStudentId == t2.AssignGametoStudentId &&
                       t1.AcademicYearId == AcademicYear &&
                       t1.SessionId == SessionName &&
                       t2.GameLevelId == Level &&
                       t2.GameId == GameName &&
                       t1.StudentRegisterId == t3.StudentRegisterId &&
                       t1.ClassId == t4.ClassId &&
                       t1.ClassId == t11.ClassId &&
                       t1.StudentRegisterId == t4.StudentRegisterId &&
                       t1.SectionId == t4.SectionId &&
                       t1.SectionId == t12.SectionId &&
                       t1.HouseId == t5.HouseId &&
                       t2.PointId == t10.PointId

                        select new
                        {
                            StudentFName = t3.FirstName,
                            StudentLastName = t3.LastName,
                            ClassName = t11.ClassType,
                            SectionName = t12.SectionName,
                            HouseName = t5.HouseName,
                            PointName = t10.PointName,
                           
                        }).Distinct().ToList()
            .Select(s => new GameLevelReportValidation
            {
               
                StudentName = s.StudentFName + " " + s.StudentLastName,
                Class=s.ClassName +" " +"- "+s.SectionName,
                HouseName = s.HouseName,
                Result = s.PointName
            }).ToList();

            return List;

        }


       private bool disposed = false;
       protected virtual void Dispose(bool disposing)
       {
           if (!this.disposed)
           {
               if (disposing)
               {
                   db.Dispose();
               }
           }
       }
       public void Dispose()
       {
           Dispose(true);
           GC.SuppressFinalize(this);
       }

    }
}
