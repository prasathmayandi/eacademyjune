﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eAcademy.Areas.Sports.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;

namespace eAcademy.Areas.Sports.Services
{
    public class SportsChampionHouseServices : IDisposable 
    {
          EacademyEntities db = new EacademyEntities();
          DateTime DateWithTime = DateTimeByZone.getCurrentDate();
        public void AddChampionHouse(int acyear, int gameId, int FinalResult, bool status)
        {
            
            SportsChampionHouse addchampion = new SportsChampionHouse()
            {
                AcademicYearId = acyear,
                GameId = gameId,
                ChampionHouseId = FinalResult,
                Status = status,
                CreatedDate = DateWithTime,
                UpdatedDate = DateWithTime                               
            };
            db.SportsChampionHouses.Add(addchampion);
            db.SaveChanges();

        }

        public SportsChampionHouse YearExist(int yearId)
        {
            var getRow = (from a in db.SportsChampionHouses
                          where a.AcademicYearId == yearId 
                          select a).FirstOrDefault();
            return getRow;
        }
      
        public List<SportsChampionHouseValidation> CheckChId(int passYearId)
        {
            var getRow = (from t1 in db.SportsChampionHouses
                          where
                          t1.AcademicYearId == passYearId &&
                           t1.Status == true
                          select new SportsChampionHouseValidation
                          {
                              Id = t1.Id,
                              GameId = t1.GameId,
                              ChampionHouseId = t1.ChampionHouseId
                          }).ToList();                          
            return getRow;
        }

        public void UpdateChampionHouseId(int editFRHouseId, int editFRIdupdate)
        {
            var Updatearr = db.SportsChampionHouses.Where(q => q.Id.Equals(editFRIdupdate)).FirstOrDefault();
            if (Updatearr != null)
            {
                Updatearr.ChampionHouseId = editFRHouseId;               
            }
            db.SaveChanges();
        }

        public void AddNewChampionHouseId(int editFRHouseId, bool status)
        {
            SportsChampionHouse addchampion = new SportsChampionHouse()
            {

                ChampionHouseId = editFRHouseId,
                Status = status,
                CreatedDate = DateWithTime,
                UpdatedDate = DateWithTime                               
            };
            db.SportsChampionHouses.Add(addchampion);
            db.SaveChanges();

        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}


