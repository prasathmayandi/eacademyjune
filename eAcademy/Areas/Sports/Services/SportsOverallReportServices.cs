﻿using eAcademy.Areas.Sports.Models;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Services
{
    public class SportsOverallReportServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public IEnumerable<Object> getSession(int passYearId)
        {
            var List = (from a in db.SportsSessions
                        where
                        a.AcademicYearId == passYearId
                        select new
                        {
                            SessionlId = a.SessionId,
                            SessionName = a.SessionName
                        }).ToList();

            return List;
        }



        public IEnumerable<OverAllList> getOverAllReport(int passYearId, int PassSessionId)
        {

            var AllList = (from t1 in db.SportsAssignGametoStudents
                           from t2 in db.SportsAssignGametoStudentConfigs
                           from t3 in db.SportsHouses

                           where t1.AcademicYearId == passYearId &&
                           t1.SessionId == PassSessionId &&
                           t1.HouseId == t3.HouseId &&
                           t1.AssignGamestoStudentId == t2.AssignGametoStudentId
                           select new
                           {
                               HouseName = t3.HouseName,
                               AcademicYearId = t1.AcademicYearId,
                               SessionId = t1.SessionId,
                               HouseId = t1.HouseId

                           }).ToList().Select(q => new

                           {
                               AcademicYearId = q.AcademicYearId,
                               SessionId = q.SessionId,
                               HouseName = q.HouseName,
                               HouseId = q.HouseId,
                               NumberofGamesConducted = getGameConductedCount(q.AcademicYearId, q.SessionId),
                               NumberofGamesParticipated = getGamesParticipatedCount(q.AcademicYearId, q.SessionId, q.HouseId),
                               WinnerPoints = getWinnerPoints(q.AcademicYearId, q.SessionId, q.HouseId),
                               RunnerPoints = getRunnerPoints(q.AcademicYearId, q.SessionId, q.HouseId),
                               //NoParticipation = (NumberofGamesConducted,NumberofGamesParticipated)
                           }).Distinct().ToList().Select(a => new OverAllList
                           {
                               HouseName = a.HouseName,
                               NumberofGamesConducted = a.NumberofGamesConducted,
                               NumberofGamesParticipated = a.NumberofGamesParticipated,
                               Points = (a.WinnerPoints + a.RunnerPoints),
                               NoParticipation = (a.NumberofGamesConducted - a.NumberofGamesParticipated)

                           }).Distinct().ToList();
            return AllList;
        }

        public int? getGameConductedCount(int? AcademicYearId, int? SessionId)
        {
            var GameCList = (from t1 in db.SportsAssignGametoStudents
                             from t2 in db.SportsAssignGametoStudentConfigs

                             where t1.AcademicYearId == AcademicYearId &&
                             t1.SessionId == SessionId &&
                            t1.AssignGamestoStudentId == t2.AssignGametoStudentId
                             select new
                             {
                                 GameId = t2.GameId
                             }).Distinct().ToList().Count();
            return GameCList;
        }
        public int? getGamesParticipatedCount(int? AcademicYearId, int? SessionId, int? HouseId)
        {
            var GamePList = (from t1 in db.SportsAssignGametoStudents
                             from t2 in db.SportsAssignGametoStudentConfigs

                             where t1.AcademicYearId == AcademicYearId &&
                             t1.SessionId == SessionId &&
                             t1.HouseId == HouseId &&
                             t1.AssignGamestoStudentId == t2.AssignGametoStudentId
                             select new
                             {
                                 GameId = t2.GameId
                             }).Distinct().ToList().Count();
            return GamePList;
        }
        public int? getWinnerPoints(int? AcademicYearId, int? SessionId, int? HouseId)
        {
            int? WinnerPoints;
            var WonList = (from t1 in db.SportsAssignGametoStudents
                           from t2 in db.SportsAssignGametoStudentConfigs

                           where t1.AcademicYearId == AcademicYearId &&
                           t1.SessionId == SessionId &&
                           t1.AssignGamestoStudentId == t2.AssignGametoStudentId &&
                           t1.HouseId == HouseId &&
                           t2.PointId == 2

                           select new
                           {
                               PointId = t2.PointId
                           }).ToList().Count();
            WinnerPoints = WonList * 10;
            return WinnerPoints;
        }
        public int? getRunnerPoints(int? AcademicYearId, int? SessionId, int? HouseId)
        {
            int RunnerPoints;
            var RunnerList = (from t1 in db.SportsAssignGametoStudents
                              from t2 in db.SportsAssignGametoStudentConfigs

                              where t1.AcademicYearId == AcademicYearId &&
                              t1.AssignGamestoStudentId == t2.AssignGametoStudentId &&
                              t1.SessionId == SessionId &&
                              t1.HouseId == HouseId &&
                              t2.PointId == 3

                              select new
                              {
                                  PointId = t2.PointId
                              }).ToList().Count();
            RunnerPoints = RunnerList * 9;
            return RunnerPoints;
        }

        public IEnumerable<OverAllList> getOverAllYearReport(int passYearId)
        {

            var AllList = (from t1 in db.SportsAssignGametoStudents
                           from t2 in db.SportsAssignGametoStudentConfigs
                           from t3 in db.SportsHouses

                           where t1.AcademicYearId == passYearId &&
                           t1.HouseId == t3.HouseId &&
                           t1.AssignGamestoStudentId == t2.AssignGametoStudentId
                           select new
                           {
                               HouseName = t3.HouseName,
                               AcademicYearId = t1.AcademicYearId,
                               //SessionId = t1.SessionId,
                               HouseId = t1.HouseId

                           }).Distinct().ToList().Select(q => new

                           {
                               AcademicYearId = q.AcademicYearId,
                               //SessionId = q.SessionId,
                               HouseName = q.HouseName,
                               HouseId = q.HouseId,
                               NumberofGamesConducted = getGameConductedCount(q.AcademicYearId),
                               NumberofGamesParticipated = getGamesParticipatedCount(q.AcademicYearId, q.HouseId),
                               WinnerPoints = getWinnerPoints(q.AcademicYearId, q.HouseId),
                               RunnerPoints = getRunnerPoints(q.AcademicYearId, q.HouseId),
                               //NoParticipation = (NumberofGamesConducted,NumberofGamesParticipated)
                           }).Distinct().ToList().Select(a => new OverAllList
                           {
                               HouseName = a.HouseName,
                               NumberofGamesConducted = a.NumberofGamesConducted,
                               NumberofGamesParticipated = a.NumberofGamesParticipated,
                               Points = (a.WinnerPoints + a.RunnerPoints),
                               NoParticipation = (a.NumberofGamesConducted - a.NumberofGamesParticipated)

                           }).Distinct().ToList();
            return AllList;
        }

        public int? getGameConductedCount(int? AcademicYearId)
        {
            var GameCList = (from t1 in db.SportsAssignGametoStudents
                             from t2 in db.SportsAssignGametoStudentConfigs

                             where t1.AcademicYearId == AcademicYearId &&

                            t1.AssignGamestoStudentId == t2.AssignGametoStudentId
                             select new
                             {
                                 GameId = t2.GameId
                             }).Distinct().ToList().Count();
            return GameCList;
        }
        public int? getGamesParticipatedCount(int? AcademicYearId, int? HouseId)
        {
            var GamePList = (from t1 in db.SportsAssignGametoStudents
                             from t2 in db.SportsAssignGametoStudentConfigs

                             where t1.AcademicYearId == AcademicYearId &&

                             t1.HouseId == HouseId &&
                             t1.AssignGamestoStudentId == t2.AssignGametoStudentId
                             select new
                             {
                                 GameId = t2.GameId
                             }).Distinct().ToList().Count();
            return GamePList;
        }
        public int? getWinnerPoints(int? AcademicYearId, int? HouseId)
        {
            int? WinnerPoints;
            var WonList = (from t1 in db.SportsAssignGametoStudents
                           from t2 in db.SportsAssignGametoStudentConfigs

                           where t1.AcademicYearId == AcademicYearId &&

                           t1.AssignGamestoStudentId == t2.AssignGametoStudentId &&
                           t1.HouseId == HouseId &&
                           t2.PointId == 2

                           select new
                           {
                               PointId = t2.PointId
                           }).ToList().Count();
            WinnerPoints = WonList * 10;
            return WinnerPoints;
        }
        public int? getRunnerPoints(int? AcademicYearId, int? HouseId)
        {
            int RunnerPoints;
            var RunnerList = (from t1 in db.SportsAssignGametoStudents
                              from t2 in db.SportsAssignGametoStudentConfigs

                              where t1.AcademicYearId == AcademicYearId &&
                              t1.AssignGamestoStudentId == t2.AssignGametoStudentId &&

                              t1.HouseId == HouseId &&
                              t2.PointId == 3

                              select new
                              {
                                  PointId = t2.PointId
                              }).ToList().Count();
            RunnerPoints = RunnerList * 9;
            return RunnerPoints;
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



    }
}







