﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eAcademy.DataModel;

namespace eAcademy.Areas.Sports.Services
{
    public class SportsGameScheduleConfigServices : IDisposable 
    {
        EacademyEntities db = new EacademyEntities();

        public void addGameScheduleConfig(int GameScheduleId, int HId, int PId)
        {
            SportsGamesheduleConfig add = new SportsGamesheduleConfig()
            {
                GameScheduleId = GameScheduleId,
                HouseId = HId,
                PointId=PId
            };
            db.SportsGamesheduleConfigs.Add(add);
            db.SaveChanges();

        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}