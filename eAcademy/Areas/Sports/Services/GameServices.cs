﻿using eAcademy.Areas.Sports.Models;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eAcademy.HelperClass;

namespace eAcademy.Areas.Sports.Services
{
    public class GameServices : IDisposable
    {

        EacademyEntities db = new EacademyEntities();

           public IEnumerable<Object> ShowGameNames()
        {
            var GameNameList = (from a in db.SportsGames

                                    select new
                                    {
                                        GameName = a.GameName,
                                        GameId = a.GameId
                                    }).ToList().AsEnumerable();
            return GameNameList;
        }

          
        public SportsGame CheckGameExist(string GameName)
        {
            var row = (from a in db.SportsGames
                       where a.GameName == GameName 
                       select a).FirstOrDefault();
            return row;
        }

        public IEnumerable<Object> getprizelist()
        {
            var GamePointList = (from a in db.SportsGamePoints
                                 where a.PointId != 1 
                                 select new
                                 {
                                     PointName = a.PointName,
                                     PointValue = a.PointValue,
                                     PointId = a.PointId

                                 }).ToList().Select(a => new
                                 {
                                     PointName = a.PointName + " (" + a.PointValue + " Points)",
                                     PointId = a.PointId
                                 }).ToList().AsEnumerable();


            return GamePointList;
        }

        public int addgame(string GameName, string GameType, int NumberOfMainplayers, int NumberOfSubstituteplayers, int empid)
        {
            SportsGame add = new SportsGame()
            {
                GameName = GameName,
                NumberOfMainplayers = NumberOfMainplayers,
                NumberOfSubstituteplayers = NumberOfSubstituteplayers,
                GameType = GameType,
             
                Status = true,
                Createdby = empid,                
                CreatedDate = DateTimeByZone.getCurrentDateTime(),
                

            };
            db.SportsGames.Add(add);
            db.SaveChanges();
            int id = add.GameId;
            return id;
        }
        public string GetGamePoints(int GameId)
        {
            
            var File = (from a in db.SportsGamePointConfigs
                        from b in db.SportsGamePoints
                        where a.PointId == b.PointId &&
                        a.GameId == GameId
                        select new
                        {
                            PointName = b.PointName,
                            PointValue = b.PointValue
                        }).ToList().Select(x => new GameValidation
                        {
                            PointName = x.PointName + "(" + x.PointValue + ")",
                        }).ToList(); ;


            string[] Points = new string[File.Count];
            for (int i = 0; i < File.Count; i++)
            {
                Points[i] = File[i].PointName;
            }
            string PointsList = string.Join(",", Points);
            return PointsList;

        }
       

        public List<GameValidation> getGameName()

        {
            var ans = (from t1 in db.SportsGames
                       select new{
                        GameId = t1.GameId,
                           GameName = t1.GameName,
                           GameType=t1.GameType,
                           NumberOfMainPlayers=t1.NumberOfMainplayers,
                           NumberOfSubPlayers=t1.NumberOfSubstituteplayers,
                           Status = t1.Status,
                       }).ToList().Select(a => new  GameValidation
                       {
                           GameId = a.GameId,
                           GameName =a.GameName,
                           GameType=a.GameType,
                           NumberOfMainPlayers=a.NumberOfMainPlayers,
                           NumberOfSubPlayers=a.NumberOfSubPlayers,
                           Status = a.Status,
                           Points = GetGamePoints(a.GameId)
                       }).ToList();

            return ans;
        }
        

        public bool checkAcitivity(int GameId, string GameName,string GameType,int NumberOfMainPlayers,int NumberOfSubPlayers)
        {
            int count;

            var ans = (from t1 in db.SportsGames
                       where t1.GameId != GameId && t1.GameName == GameName && t1.GameType == GameType && t1.NumberOfMainplayers == NumberOfMainPlayers && t1.NumberOfSubstituteplayers == NumberOfSubPlayers
                       select t1).ToList();

            count = ans.Count;

            if (count > 0)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }

        public EditGame getEditGame(int Gid)
        {

            var row = (from a in db.SportsGames
                       where a.GameId == Gid
                       select new EditGame
                       {
                           GameId = a.GameId,
                           GameName = a.GameName,
                           GameType = a.GameType,
                           NumberOfMainPlayers=a.NumberOfMainplayers,
                           NumberOfSubPlayers=a.NumberOfSubstituteplayers,
                          
                           Status = a.Status
                       }).FirstOrDefault();
            return row;
        }
        public List<EditGame> getEditGamePoints(int Gid)
        {

            var RowPoints= (from a in db.SportsGamePointConfigs
                        from b in db.SportsGamePoints
                        where a.PointId == b.PointId &&
                        a.GameId == Gid
                        select new
                        {
                            PointName = b.PointName,
                            PointValue = b.PointValue,
                            Id =a.Id,
                            point_id=a.PointId
                        }).ToList().Select(x => new EditGame
                        {
                            PointName = x.PointName + "(" + x.PointValue + ")",
                            GamePointId =x.Id,
                            PointId=x.point_id
                        }).ToList();
            return RowPoints;
        }
        public SportsAssignGametoStudentConfig CheckGamePointsExist(int GameId)
        {


            var row = (from a in db.SportsAssignGametoStudentConfigs
                       where a.GameId == GameId
                       select a).FirstOrDefault();
            return row;
        }
        public void  DeleteGamePoints(int GamePointId)
        {
            var delete = db.SportsGamePointConfigs.Where(query => query.Id.Equals(GamePointId)).FirstOrDefault();
            db.SportsGamePointConfigs.Remove(delete);
            db.SaveChanges();
        }

        public void updateGame(int GameId, string gname, string GameType, int Mplayer, int Splayer, bool Status, int EmpregId)
        {
            var update = db.SportsGames.Where(query => query.GameId.Equals(GameId)).FirstOrDefault();
            if (update != null)
            {
                update.GameName = gname;
                update.GameType = GameType;
                update.NumberOfMainplayers =Mplayer;
                update.NumberOfSubstituteplayers=Splayer;
                update.Status = Status;
                update.UpdatedDate = DateTimeByZone.getCurrentDateTime();
                update.Updatedby = EmpregId;

                db.SaveChanges();
            }
        }
        public bool checkUpdateGameName(string GameName, int Game_id)
        {
            //var ans = dc.Subjects.Where(q => q.SubjectName.Equals(subjectName) && q.Status.Value.Equals(true)).ToList();
            var ans = (from a in db.SportsGames where GameName == a.GameName && a.GameId != Game_id select a).FirstOrDefault();
            if (ans == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void UpdateGamePoints(int GamePointId, int GameId,int PointId )
        {
            var update = db.SportsGamePointConfigs.Where(query => query.Id.Equals(GamePointId)).FirstOrDefault();
            if (update != null)
            {
                update.GameId = GameId;
                update.PointId = PointId;
                db.SaveChanges();
            }
           
        }

       
        
        public void DeleteGame(int GameId)
        {
            var delete = db.SportsGames.Where(query => query.GameId.Equals(GameId)).FirstOrDefault();
            while (db.SportsGamePointConfigs.Where(x => x.GameId == GameId).Count() != 0)
            {
                var row = db.SportsGamePointConfigs.Where(x => x.GameId == GameId).FirstOrDefault();
                db.SportsGamePointConfigs.Remove(row);
                db.SaveChanges();
            }
            db.SportsGames.Remove(delete);
            db.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}







