﻿using eAcademy.Areas.Sports.Controllers;
using eAcademy.Areas.Sports.Models;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eAcademy.HelperClass;

namespace eAcademy.Areas.Sports.Services
{
    public class SessionServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        public IEnumerable<Object> ShowSessionNames()
        {
            var SessionList = (from a in db.SportsSessions
                               select new
                               {
                                   SessionId = a.SessionId,
                                   SessionName = a.SessionName
                               }).ToList().AsEnumerable();
            return SessionList;
        }

        //public bool CheckSessionExist(string SessionName, int acyear)
        //{
        //    int count;
        //    var row = (from a in db.SportsSessions where a.SessionName == SessionName && a.AcademicYearId == acyear select a).ToList();
        //    count = row.Count;
        //    if (count > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        public SportsSession CheckSessionExist(string SessionName, int acyear)
        {
            var row = (from a in db.SportsSessions where a.SessionName == SessionName && a.AcademicYearId == acyear select a).FirstOrDefault();
            return row;
        }


        public void addsession(string SessionName, int empid,int acyear)
        {
                       SportsSession add = new SportsSession()
            {
               SessionName = SessionName,
               AcademicYearId=acyear,
               Status = true,
               CreatedBy = empid,
               CreatedDate =DateTimeByZone.getCurrentDateTime()
            };
            db.SportsSessions.Add(add);
            db.SaveChanges();
        }

      

        public List<SessionValidate> getSessionName()
        {
            var val = (from t1 in db.SportsSessions
                       from t2 in db.AcademicYears
                   where  t1.AcademicYearId == t2.AcademicYearId 

                       
                       select new SessionValidate
                       {
                         
                           SessionId = t1.SessionId,
                           SessionName = t1.SessionName,
                           AcademicYear = t1.AcademicYearId,
                           Status = t1.Status

                       }).Distinct().ToList();

            return val;
        }
      

        public bool checkAcitivity(int SessionId, string SessionName )
        {
            int count;
            var ans = (from t1 in db.SportsSessions
                       where t1.SessionId != SessionId && t1.SessionName == SessionName 
                       select t1).ToList();

            //var ans = (from t1 in db.SportsSessions
            //           where t1.SessionId == SessionId && t1.SessionName == SessionName
            //           select t1).ToList();

            count = ans.Count;

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public EditSession getEditSession(int Hid)
        {

            var row = (from a in db.SportsSessions
                       where a.SessionId == Hid
                       select new EditSession
                       {
                           SessionId = a.SessionId,
                           SessionName = a.SessionName,
                           acyear=a.AcademicYearId,
                           Status = a.Status
                       }).FirstOrDefault();
            return row;
        }

        public void updateSession(int SessionId, string SessionName, int acyear, bool Status, int EmpregId)
        {
           
            var update = db.SportsSessions.Where(query => query.SessionId.Equals(SessionId)).FirstOrDefault();
            if (update != null)
            {
                update.SessionName = SessionName;
                update.AcademicYearId = acyear;
                update.Status = Status;
                update.UpdatedBy = EmpregId;
                update.UpdatedDate = DateTimeByZone.getCurrentDateTime();
                db.SaveChanges();
            }
            
        }
      
        public void deleteSession(int SessionId)
        {
            var delete = db.SportsSessions.Where(query => query.SessionId.Equals(SessionId)).FirstOrDefault();
            db.SportsSessions.Remove(delete);
            db.SaveChanges();
        }


        public bool checkDeleteActivity(int SessionId)
        {
            int count;
            var d = (from a in db.SportsAssignGametoStudents where a.SessionId == SessionId select a).ToList();
            count = d.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }




        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}