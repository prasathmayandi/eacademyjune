﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Services
{
    public class SportsGamePointConficServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        public void AddgamePoints(int GameId, int PointId)
        {
            
            SportsGamePointConfig add = new SportsGamePointConfig()
            {
                GameId = GameId,
                PointId = PointId
            };
           db.SportsGamePointConfigs.Add(add);
           db.SaveChanges();

        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}