﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eAcademy.Areas.Sports.Models;
namespace eAcademy.Areas.Sports.Services
{
    public class GamePointServices : IDisposable 
    {

        EacademyEntities db = new EacademyEntities();

        public IEnumerable<Object> ShowSportsGamepoints()
        {
            var GamePointList = (from a in db.SportsGamePoints
                                 where a.PointId != 1 
                                 select new
                                 {
                                     PointName = a.PointName,
                                     PointValue = a.PointValue,
                                     PointId = a.PointId

                                 }).ToList().Select(a => new
                                 {
                                     PointName = a.PointName + " (" + a.PointValue + " Points)",
                                     PointId = a.PointId
                                 }).ToList().AsEnumerable();


            return GamePointList;
        }

        public IEnumerable<Object> SportsGamepoints()
        {
            var GamePointList = (from a in db.SportsGamePoints
                                 select new
                                 {
                                     PointName = a.PointName,
                                     PointValue = a.PointValue,
                                     PointId = a.PointId

                                 }).ToList().Select(a => new
                                 {
                                     PointName = a.PointName + " (" + a.PointValue + " Points)",
                                     PointId = a.PointId
                                 }).ToList().AsEnumerable();


            return GamePointList;
        }
        public IEnumerable<Object> getprizelist()
        {
            var GamePointList = (from a in db.SportsGamePoints
                                 select new
                                 {
                                     PointName = a.PointName,
                                     PointValue = a.PointValue,
                                     PointId = a.PointId

                                 }).ToList().Select(a => new
                                 {
                                     PointName = a.PointName + " (" + a.PointValue + " Points)",
                                     PointId = a.PointId
                                 }).ToList().AsEnumerable();


            return GamePointList;
        }

        //public List<GameValidation> ShowSportsGamepoints()
        //{
        //    var ans = (from t1 in db.SportsGamePoints
        //               where t1.PointId == PointId
        //               select new GameValidation { PointId = t1.PointId, PointName = t1.PointName }).OrderBy(q => q.PointId).ToList();
        //    return ans;
        //}


        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}