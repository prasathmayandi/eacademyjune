﻿using System;
using System.Collections.Generic;
using System.Linq;
using eAcademy.Areas.Sports.Models;
using eAcademy.DataModel;


namespace eAcademy.Areas.Sports.Services
{
    public class SportsGameLevelConfigServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        public void AddGameLevelname(int GameLevelId, string GameLevelName)
        {
           
            SportsGameLevelConfig addln = new SportsGameLevelConfig()
            {            
                GameLevelId = GameLevelId,
                GameLevelName = GameLevelName
            };
            db.SportsGameLevelConfigs.Add(addln);
            db.SaveChanges();        
        }

                            
        public SportsGameLevelConfig CheckglExists(int GameLevelId, string GameLevelName)
        {
            EacademyEntities db = new EacademyEntities();
            var row = (from b in db.SportsGameLevelConfigs
                       where b.GameLevelId == GameLevelId && b.GameLevelName == GameLevelName
                       select b).FirstOrDefault();
             
            return row;
        }


        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



    }
}