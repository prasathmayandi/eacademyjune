﻿using eAcademy.Areas.Sports.Models;
using eAcademy.Areas.Sports.Services;
using eAcademy.Areas.Sports.ViewModel;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace eAcademy.Areas.Sports.Controllers
{
    [CheckSessionOutAttribute]
    public class SportsConfigurationController : Controller
    {

        int empregid;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AcademicyearServices Acyear = new AcademicyearServices();
            ViewBag.allAcademicYears = Acyear.ShowAcademicYears();
            HouseServices allHouse = new HouseServices();
            ViewBag.allHouses = allHouse.ShowHouses();
            GamePointServices GamePrize = new GamePointServices();
            ViewBag.allSportsGamePoints = GamePrize.ShowSportsGamepoints();        

            SessionServices SName = new SessionServices();
            ViewBag.allSessionNames = SName.ShowSessionNames();
            GameServices GameName = new GameServices();
            ViewBag.allGameNames = GameName.ShowGameNames();
           // GameLevelServices allLevel = new GameLevelServices();
           // ViewBag.allLevels = allLevel.ShowLevels();

            ViewBag.acYear = Acyear.AllList_AcademicYear();
            ViewBag.AddacYear = Acyear.Add_formAcademicYear();
        }


        //House Form--Start
        public ActionResult House()
        {
            return View();
        }
        public JsonResult addhouse(VM_Model vh)
        {
            SportsHouse sh = new SportsHouse();
            HouseServices hs = new HouseServices();
            try
            {

                if (ModelState.IsValid)
                {
                    string hname = vh.HouseName;
                    int empregid = Convert.ToInt16(Session["empRegId"].ToString());
                    //bool cha = hs.checkAddActivity(hname);
                    var isHouseExist = hs.checkAddActivity(hname);
                    if (isHouseExist == null)
                    {
                        hs.addhouse(vh.HouseName, empregid);
                        string Message = eAcademy.Models.ResourceCache.Localize("SuccessfullySaved");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
    
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("HouseExist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    
                    //if (cha == true)
                    //{
                    //    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("HouseExist");
                    //    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    //}
                    //else
                    //{
                    //    hs.addhouse(vh.HouseName, empregid);
                    //    string Message = eAcademy.Models.ResourceCache.Localize("SuccessfullySaved");
                    //    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    //}
               
                
                }

                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }

            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }
        static IList<HouseValidation> HouseList1 = new List<HouseValidation>();
        public JsonResult HouseList(string sidx, string sord, int page, int rows)
        {
            HouseServices obj_hs = new HouseServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            HouseList1 = obj_hs.getHouseName();

            int totalRecords = HouseList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    HouseList1 = HouseList1.OrderByDescending(s => s.HouseId).ToList();
                    HouseList1 = HouseList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    HouseList1 = HouseList1.OrderBy(s => s.HouseId).ToList();
                    HouseList1 = HouseList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = HouseList1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        public JsonResult EditHouse(int id)
        {
            try
            {
                HouseServices obj_es = new HouseServices();
                int Hid = Convert.ToInt32(id);
                var ans = obj_es.getEditHouse(Hid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult updateHouse(EditHouse model)
        {
            try
            {
                HouseServices obj_uh = new HouseServices();
                if (ModelState.IsValid)
                {
                    int id = model.HouseId;
                    String hname = model.HName;
                    bool status = model.status.Value;
                    int empregid = Convert.ToInt16(Session["empRegId"].ToString());
                    bool cc = obj_uh.checkActivity(id, hname);
                    if (cc == true)
                    {
                        //obj_uh.updateHouse(id, hname, status);
                        //string Message = eAcademy.Models.ResourceCache.Localize("Houseupdatedsuccessfully");
                        //return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);

                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("HouseExist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //string ErrorMessage = eAcademy.Models.ResourceCache.Localize("HouseExist");
                        //return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);

                        obj_uh.updateHouse(id, hname, status, empregid);
                        string Message = eAcademy.Models.ResourceCache.Localize("Houseupdatedsuccessfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult DeleteHouse(int id)
        {
            try
            {
                HouseServices obj_es = new HouseServices();
                bool dh = obj_es.checkDeleteActivity(id);
                if (dh == true)
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("HouseAlreadyUsedinGamesSoCannotDelete");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    obj_es.deleteHouse(id);
                    string Message = eAcademy.Models.ResourceCache.Localize("DeletedSuccessfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult HouseNameExportToExcel()
        {
            try
            {
                var list = HouseList1.Select(o => new { HouseName = o.HouseName, Status = o.Status }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("House_List");
               // string fileName = "HouseList1";
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");


                //var list = HouseList1.Select(o => new { HouseName = o.HouseName, Status = o.Status }).ToList();
                //GenerateExcel.ExportExcel(list);
                //return View("HouseList1");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult HouseNameExportToPDF()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
               // string fileName = "HouseNameList";
                string fileName = eAcademy.Models.ResourceCache.Localize("House_List");
                GeneratePDF.ExportPDF_Portrait(HouseList1, new string[] { "HouseName", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "HouseName.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        //House Form--End

        //SessionSchedule-Start
        public ActionResult SessionSchedule()
        {
            return View();
        }

        [HttpPost]
        public JsonResult addsession(VM_Session vh)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    SessionServices ss = new SessionServices();
                    int empregid = Convert.ToInt16(Session["empRegId"].ToString());
                    var isSessionExist = ss.CheckSessionExist(vh.ssname, vh.acyear);
                    if (isSessionExist == null)
                    {
                        ss.addsession(vh.ssname, empregid, vh.acyear);
                        string Message = eAcademy.Models.ResourceCache.Localize("SuccessfullySaved");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("SessionExist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        static IList<SessionValidate> SessionList1 = new List<SessionValidate>();

        public JsonResult SessionList(string sidx, string sord, int page, int rows)
        {
            SessionServices obj_hs = new SessionServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            SessionList1 = obj_hs.getSessionName();

            int totalRecords = GameList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    SessionList1 = SessionList1.OrderByDescending(s => s.SessionId).ToList();
                    SessionList1 = SessionList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    SessionList1 = SessionList1.OrderBy(s => s.SessionId).ToList();
                    SessionList1 = SessionList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = SessionList1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EditSession(int SessionId)
        {
            try
            {
                SessionServices obj_es = new SessionServices();
                int Hid = Convert.ToInt32(SessionId);
                var ans = obj_es.getEditSession(Hid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult updateSession(EditSession model)
        {
            try
            {
                SessionServices obj_uh = new SessionServices();
                if (ModelState.IsValid)
                {
                    int passId = model.SessionId;
                    String SessionName = model.SessionName;
                    int academicyear = model.acyear.Value;
                    bool status = model.Status.Value;
                    int empregid = Convert.ToInt16(Session["empRegId"].ToString());

                   // bool cc = obj_uh.checkAcitivity(passId, SessionName, academicyear);
                    bool cc = obj_uh.checkAcitivity(passId, SessionName);
                    if (cc == true)
                    {

                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("SessionExist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_uh.updateSession(passId, SessionName, academicyear, status, empregid);
                        string Message = eAcademy.Models.ResourceCache.Localize("SessionUpdatedSuccessfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);

                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteSession(int SessionId)
        {
            try
            {
                SessionServices obj_es = new SessionServices();
                bool dh = obj_es.checkDeleteActivity(SessionId);
                if (dh == true)
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("SessionAlreadyUsedinGamesSoCannotDelete");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    obj_es.deleteSession(SessionId);
                    string Message = eAcademy.Models.ResourceCache.Localize("DeletedSuccessfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);

                }


                //obj_es.deleteSession(SessionId);
                //string Message = "Deleted Successfully";
                //return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SessionListExportToPdf(bool? pdf)
        {
            try
            {
                if (!pdf.HasValue)
                {
                    return View();
                }
                else
                {
                    string filePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                  //  string heading = "SessionList1";
                    string heading = eAcademy.Models.ResourceCache.Localize("SessionList");
                    GeneratePDF.ExportPDF_Portrait(SessionList1, new string[] { "AcademicYear", "SessionName", "Status" }, filePath, heading);
                    return File(filePath, "application/pdf", "SessionList1.pdf");
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }

        }

        public ActionResult SessionListExportToExcel()
        {
            try
            {
                var list = SessionList1.Select(e => new { SessionName = e.SessionName, AcedemicYear = e.AcademicYear, Status = Convert.ToString(e.Status) }).ToList();
               // string fileName = "SessionList1";
                string fileName = eAcademy.Models.ResourceCache.Localize("SessionList");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");



                //var list = SessionList1.Select(e => new { SessionName = e.SessionName, AcedemicYear = e.AcademicYear, Status = Convert.ToString(e.Status) }).ToList();
                //GenerateExcel.ExportExcel(list);
                //return View("SessionList");

            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }

        }

        //SessionShedule-End




     

        //Game-Start
        public ActionResult Game()
        {
            return View();
        }
        [HttpPost]
        public JsonResult addgame(VM_Game vg)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    GameServices hs = new GameServices();
                    SportsGamePointConficServices gs = new SportsGamePointConficServices();

                    var isGameExist = hs.CheckGameExist(vg.gname);
                    if (isGameExist == null)
                    {
                        int empregid = Convert.ToInt16(Session["empRegId"].ToString());
                        var GameId = hs.addgame(vg.gname, vg.GameType, vg.Mplayer, vg.Splayer, empregid);
                        int counter = vg.GamePrize.Length;
                        int PointId = 0;
                        for (int i = 0; i < counter; i++)
                        {
                            PointId = Convert.ToInt32(vg.GamePrize[i]);
                            gs.AddgamePoints(GameId, PointId);

                        }

                        string Message = eAcademy.Models.ResourceCache.Localize("Game_Added_Successfully"); 
                        //string ErrorMessage = eAcademy.Models.ResourceCache.Localize("GameName_already_Exist");
                        return Json(new { Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("GameName_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }


        }

        static IList<GameValidation> GameList1 = new List<GameValidation>();

        public JsonResult GameList(string sidx, string sord, int page, int rows, int? GameName)
        {
            GameServices obj_gs = new GameServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            GameList1 = obj_gs.getGameName();



            if (GameName.HasValue)
            {
                GameList1 = GameList1.Where(p => p.GameId == GameName).ToList();
            }




            int totalRecords = GameList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    GameList1 = GameList1.OrderByDescending(s => s.GameId).ToList();
                    GameList1 = GameList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    GameList1 = GameList1.OrderBy(s => s.GameId).ToList();
                    GameList1 = GameList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = GameList1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EditGame(int GameId)
        {
            try
            {
                GameServices obj_gs = new GameServices();

                int Hid = Convert.ToInt32(GameId);
                var ans = obj_gs.getEditGame(Hid);
                var AnsPoint = obj_gs.getEditGamePoints(Hid);

                return Json(new { ans, AnsPoint }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult updateGame(EditGame model)
        {
            try
            {
                SportsGamePointConficServices gs = new SportsGamePointConficServices();
                GameServices obj_gs = new GameServices();
                if (ModelState.IsValid)
                {
                    int empregid = Convert.ToInt16(Session["empRegId"].ToString());
                    int EmpregId = empregid;
                    int GameId = model.GameId;
                    string gname = model.GameName;
                    String GameType = model.GameType;
                    int Mplayer = model.NumberOfMainPlayers;
                    int Splayer = model.NumberOfSubPlayers;
                    bool Status = model.Status.Value;

                    int counter = model.UpdatePointId.Length;
                    int PointId = 0;
                    int GamePointId = 0;
                    for (int i = 0; i < counter; i++)
                    {
                        PointId = Convert.ToInt32(model.UpdatePointId[i]);
                        if (model.IdUpdate[i] == null)
                        {
                            gs.AddgamePoints(GameId, PointId);
                        }
                        else
                        {
                            GamePointId = Convert.ToInt32(model.IdUpdate[i]);
                            obj_gs.UpdateGamePoints(GamePointId, GameId, PointId);
                        }


                    }

                    bool cc = obj_gs.checkAcitivity(GameId, gname, GameType, Mplayer, Splayer);
                    bool cg = obj_gs.checkUpdateGameName(gname, GameId);
                    if (cc == true)
                    {
                        //string ErrorMessage = "Game exist";
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Game_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }                   
                    else if (cg == true)
                    {
                        //string ErrorMessage = "Game name exist";
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("GameName_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_gs.updateGame(GameId, gname, GameType, Mplayer, Splayer, Status, EmpregId);
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");                        
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult DeleteGame(int GameId)
        {
            try
            {
                GameServices gs = new GameServices();
                var isGamePointsExist = gs.CheckGamePointsExist(GameId);
                if (isGamePointsExist == null)
                {
                    GameServices obj_gs = new GameServices();
                    obj_gs.DeleteGame(GameId);
                    string Message = "Deleted Successfully";
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var DataExits = 1;

                    return Json(new { DataExits = DataExits }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CheckGamePoints(int GameId, int GamePointId)
        {
            GameServices gs = new GameServices();
            var isGamePointsExist = gs.CheckGamePointsExist(GameId);
            if (isGamePointsExist == null)
            {

                gs.DeleteGamePoints(GamePointId);
                string Message = "Deleted Successfully";
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var DataExits = 1;

                return Json(new { DataExits = DataExits }, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetPrizeStatus()
        {
            GameServices obj_as = new GameServices();
            try
            {
                var list = obj_as.getprizelist();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetEditPrizeStatus()
        {
            GameServices obj_as = new GameServices();
            try
            {
                var list = obj_as.getprizelist();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GameListToPdf(bool? pdf)
        {
            try
            {
                if (!pdf.HasValue)
                {
                    return View();
                }
                else
                {
                    string filePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                    string heading = eAcademy.Models.ResourceCache.Localize("GameDetails");
                    GeneratePDF.ExportPDF_Portrait(GameList1, new string[] { "GameName", "GameType", "NumberOfMainPlayers", "NumberOfSubPlayers", "Points", "Status" }, filePath, heading);
                    return File(filePath, "application/pdf", "GameList.pdf");

                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }

        }
        public ActionResult GameListToExcel()
        {
            try
            {
                var list = GameList1.Select(e => new { GameName = e.GameName, GameType = e.GameType, MainPlayers = e.NumberOfMainPlayers, SubPlayers = e.NumberOfSubPlayers, Points = e.Prizetype, Status = Convert.ToString(e.Status) }).ToList();
                string heading = eAcademy.Models.ResourceCache.Localize("GameDetails");
                GenerateExcel.ExportExcel(list, heading);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        //GameLevel 

        public ActionResult GameLevel()
        {
            return View();
        }

        [HttpPost]
        public JsonResult addlevel(VM_GameLevel vl)
        {
            try
            {


                if (ModelState.IsValid)
                {
                    GameLevelServices ls = new GameLevelServices();
                    SportsGameLevelConfigServices ln = new SportsGameLevelConfigServices();
                    int empregid = Convert.ToInt16(Session["empRegId"].ToString());
                    var checkrecords = ls.checkrecordsexist(vl.acyear, vl.SessionId, vl.GameId);
                    int count = vl.GameLevelName.Length;

                    for (int i = 0; i < count; i++)
                    {
                        for (int j = i + 1; j < count; j++)
                        {
                            if (vl.GameLevelName[i].Equals(vl.GameLevelName[j]))
                            {
                               // string Message = eAcademy.Models.ResourceCache.Localize("Game_Added_Successfully"); 
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("LevelNamesAlreadyExists");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }

                    if (checkrecords != null)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("RecordsAlreadyExists");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {

                        int counter1 = vl.GameLevelName.Length;
                        var GameLevelId = ls.addGameLevel(vl.acyear, vl.SessionId, vl.GameId, vl.LevelId, empregid);
                        for (int i = 0; i < counter1; i++)
                        {

                            string LevelName = vl.GameLevelName[i];

                            var CheckLevelNameExist = ln.CheckglExists(GameLevelId, LevelName);
                        }


                        int counter = vl.GameLevelName.Length;
                        for (int i = 0; i < counter; i++)
                        {

                            string LevelName = vl.GameLevelName[i];

                            ln.AddGameLevelname(GameLevelId, LevelName);
                        }

                        string Message = eAcademy.Models.ResourceCache.Localize("SuccessfullySaved");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }


        }

        static IList<GameLevelValidation> GameLevelList1 = new List<GameLevelValidation>();

        public JsonResult GameLevelList(string sidx, string sord, int page, int rows)
        {
            GameLevelServices obj_ls = new GameLevelServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            GameLevelList1 = obj_ls.getGameLevelName();

            int totalRecords = GameLevelList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    GameLevelList1 = GameLevelList1.OrderByDescending(s => s.LevelId).ToList();
                    GameLevelList1 = GameLevelList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    GameLevelList1 = GameLevelList1.OrderBy(s => s.LevelId).ToList();
                    GameLevelList1 = GameLevelList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = GameLevelList1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EditGameLevel(int LevelId)
        {
            try
            {
                GameLevelServices obj_ls = new GameLevelServices();
                int Hid = Convert.ToInt32(LevelId);
                var ans = obj_ls.getEditGameLevel(Hid);
                var AnsPoint = obj_ls.getEditGameLevelName(Hid);
                return Json(new { ans, AnsPoint }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetLevelStatus()
        {
            GameLevelServices obj_ss = new GameLevelServices();
            try
            {
                var list = obj_ss.getLevellist();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult updateGameLevel(EditGameLevel model)
        {
            try
            {
                GameLevelServices obj_ls = new GameLevelServices();
                SportsGameLevelConfigServices ln = new SportsGameLevelConfigServices();
                if (ModelState.IsValid)
                {
                    int empregid = Convert.ToInt16(Session["empRegId"].ToString());
                    int EmpregId = empregid;
                    int LevelId = model.LevelId;
                    int?[] UpdateId = model.Idarr;
                    String[] LevelNamearr = model.GameLevelNamearr;
                    int GameLevelId = model.LevelId;
                    String LevelName = model.GameLevelName;
                    int acyear = model.acyear.Value;
                    int SessionId = model.SessionId.Value;
                    int GameId = model.GameId.Value;
                    bool Status = model.Status.Value;

                    int count = model.GameLevelNamearr.Length;

                    for (int i = 0; i < count; i++)
                    {
                        for (int j = i + 1; j < count; j++)
                        {
                            if (model.GameLevelNamearr[i].Equals(model.GameLevelNamearr[j]))
                            {

                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("LevelNamesAlreadyExists");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }

                    int counter = model.Idarr.Length;
                    for (int i = 0; i < counter; i++)
                    {
                        int Idupdate = 0;
                        int editIdupdate = 0;
                        Idupdate = Convert.ToInt32(model.Idarr[i]);
                        if (model.Idarr[i] == null)
                        {

                            string lnamne = model.GameLevelNamearr[i];
                            ln.AddGameLevelname(GameLevelId, lnamne);

                        }
                        else
                        {

                            editIdupdate = Convert.ToInt32(model.Idarr[i]);
                            string editlname = model.GameLevelNamearr[i];

                            obj_ls.UpdateGameLevelname(editIdupdate, GameLevelId, editlname);

                        }
                    }
                    bool cc = obj_ls.checkAcitivity(acyear, SessionId, GameId, LevelId);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("GameNameAlreadyExist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_ls.AddGameLevelExits(LevelId, SessionId, GameId, acyear, LevelName, Status);
                        string Message = eAcademy.Models.ResourceCache.Localize("Levelnameupdatedsuccessfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }

       
        
        public JsonResult CheckGameLevel(int LevelId, int Id)
        {
            GameLevelServices obj_ls = new GameLevelServices();
            var isGamePointsExist = obj_ls.CheckGameLevelExist(LevelId);
            if (isGamePointsExist == null)
            {
                //var DataExits = 0;
                obj_ls.DeleteGameLevelName(Id);
                string Message = eAcademy.Models.ResourceCache.Localize("DeletedSuccessfully");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var DataExits = 1;
                //  string ErrorMessage = "Game Points are alrety exist";
                return Json(new { DataExits = DataExits }, JsonRequestBehavior.AllowGet);
            }
        }


        //public JsonResult DeleteGameLevel(int LevelId)
        //{
        //    try
        //    {
        //        GameLevelServices obj_gs = new GameLevelServices();
        //        var Pid = obj_gs.getPrimaryIds(LevelId);

        //        bool dh = obj_gs.checkDeleteActivity(Pid);
        //        if (dh == true)
        //        {
        //            // string Message = eAcademy.Models.ResourceCache.Localize("DeletedSuccessfully");
        //            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("LevelAlreadyUsedinGamesSoCannotDelete");
        //            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            obj_gs.deleteLevel(LevelId);
        //            string Message = eAcademy.Models.ResourceCache.Localize("DeletedSuccessfully");
        //            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        string ExceptionError = e.Message.ToString();
        //        return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
        //    }
        //}


        public JsonResult DeleteGameLevel(int LevelId)
        {
            try
            {
                GameLevelServices gs = new GameLevelServices();
                var isGameLevelExist = gs.CheckGameLevelExist(LevelId);
                if (isGameLevelExist == null)
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("LevelAlreadyUsedinGamesSoCannotDelete");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    
                }
                else
                {
                   // var DataExits = 1;
                    GameLevelServices obj_gs = new GameLevelServices();
                    obj_gs.deleteLevel(LevelId);
                    string Message = eAcademy.Models.ResourceCache.Localize("DeletedSuccessfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet); 
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult GameLevelListExportToExcel()
        {
            try
            {
                var GLlist = GameLevelList1.Select(o => new { AcademicYearId = o.AcademicYearId, SessionName = o.SessionName, GameName = o.GameName, GameLevelName = o.GameLevelName, Status = o.Status }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("GameLevelList");
                GenerateExcel.ExportExcel(GLlist, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult GameLevelListExportToPdf()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("GameLevelList");
                GeneratePDF.ExportPDF_Portrait(GameLevelList1, new string[] { "AcademicYearId", "SessionName", "GameName", "GameLevelName", "Status", }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "GameLevelList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }







    
    }
}