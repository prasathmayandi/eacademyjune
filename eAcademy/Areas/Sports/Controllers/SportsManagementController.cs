﻿using eAcademy.Areas.Sports.Models;
using eAcademy.Areas.Sports.Services;
using eAcademy.Areas.Sports.ViewModel;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eAcademy.Areas.Sports.Controllers
{
    [CheckSessionOutAttribute]
    public class SportsManagementController : Controller
    {
        int empregid;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AcademicyearServices Acyear = new AcademicyearServices();
            ViewBag.allAcademicYears = Acyear.ShowAcademicYears();
            SessionServices SName = new SessionServices();
            ViewBag.allSessionNames = SName.ShowSessionNames();
            GamePointServices GamePrize = new GamePointServices();
            ViewBag.allSportsGamePoints = GamePrize.ShowSportsGamepoints();
            ViewBag.SportsGamePoints = GamePrize.SportsGamepoints();

            ClassServices allClass = new ClassServices();
            ViewBag.allClasses = allClass.ShowClasses();

            HouseServices allHouse = new HouseServices();
            ViewBag.allHouses = allHouse.ShowHouses();


            GameLevelServices allLevel = new GameLevelServices();
            //ViewBag.allLevels = allLevel.ShowLevels();
            //GamePointServices allPoint = new GamePointServices();
            //ViewBag.allPoints = allPoint.ShowSportsGamepoints();
            //SectionServices allSection = new SectionServices();
            GameServices GameName = new GameServices();
            ViewBag.allGameNames = GameName.ShowGameNames();

            StudentServices allStudent = new StudentServices();
            // ViewBag.allStudents = allStudent.ShowStudents();

            AssignGametoStudentServices obj_as = new AssignGametoStudentServices();

            GamePointServices allPoint = new GamePointServices();
            ViewBag.allPoints = allPoint.ShowSportsGamepoints();
            SectionServices allSection = new SectionServices();
            ViewBag.acYear = Acyear.AllList_AcademicYear();
            ViewBag.AddacYear = Acyear.Add_formAcademicYear();

        }



        //Assaign Game to Student

        public JsonResult GetSelectedClassSection(int passClassId)
        {
            SectionServices sec = new SectionServices();
            var List = sec.getSection(passClassId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSelectedYearSession(int passYearId)
        {
            AssignClassToStudentServices stu = new AssignClassToStudentServices();
            var List = stu.getSession(passYearId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetSelectedGameLevel(int passGameId)
        {

            AssignClassToStudentServices stu = new AssignClassToStudentServices();
            var List = stu.getLevel(passGameId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getLevel()
        {

            AssignGametoStudentServices stu = new AssignGametoStudentServices();
            var List = stu.EditgetLevel();
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMarks()
        {

            AssignGametoStudentServices stu = new AssignGametoStudentServices();
            var List = stu.EditGetMarks();
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDescription()
        {

            AssignGametoStudentServices stu = new AssignGametoStudentServices();
            var List = stu.EditGetDescription();
            return Json(List, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetGameNamelist()
        {
            AssignGametoStudentServices obj_as = new AssignGametoStudentServices();
            try
            {
                var list = obj_as.getGameDetail();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetPrizeStatus()
        {
            AssignGametoStudentServices obj_as = new AssignGametoStudentServices();
            try
            {
                var list = obj_as.getprizelist();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetSectionStudents(int passYearId, int passClassId, int passSecId)
        {
            try
            {
                AssignClassToStudentServices stu = new AssignClassToStudentServices();
                var StudentList = stu.getStudents(passYearId, passClassId, passSecId);
                return Json(StudentList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }



        public JsonResult GetHouseStatus()
        {
            AssignGametoStudentServices obj_as = new AssignGametoStudentServices();
            try
            {
                var list = obj_as.getHouselist();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetEditPrizeStatus()
        {
            AssignGametoStudentServices obj_as = new AssignGametoStudentServices();
            try
            {
                var list = obj_as.getprizelist();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult GetPointPrizeStatus()
        {
            AssignGametoStudentServices obj_as = new AssignGametoStudentServices();
            try
            {
                var list = obj_as.getprizelist();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }


        public ActionResult AssignGamestoStudent()
        {
            return View();
        }

        [HttpPost]
        public JsonResult addStudent(VM_Student vs)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    AssignGametoStudentServices ags = new AssignGametoStudentServices();
                    SportsAssignGametoStudentConfigServices asc = new SportsAssignGametoStudentConfigServices();
                    int empregid = Convert.ToInt16(Session["empRegId"].ToString());
                    var isStudentExist = ags.CheckGameNameExist(vs.acyear, vs.ClassId, vs.SectionId, vs.StudentId);



                    if (isStudentExist == null)
                    {
                        var StuAssId = ags.addStudent(vs.acyear, vs.SessionId, vs.ClassId, vs.SectionId, vs.StudentId, vs.HouseId, vs.StudentType, empregid);
                        int counter = vs.GameId.Length;
                        for (int i = 0; i < counter; i++)
                        {

                            string Desc = vs.Description[i].ToString();
                            int marks = Convert.ToInt32(vs.Marks[i]);
                            int gameid = Convert.ToInt32(vs.GameId[i]);
                            int levelid = Convert.ToInt32(vs.levelId[i]);
                            int pointid = Convert.ToInt32(vs.PointId[i]);

                            bool cc = ags.checkAcitivity(StuAssId, gameid, levelid, pointid);
                            {

                                if (cc == false)
                                {
                                    asc.AddGameDetails(StuAssId, gameid, levelid, pointid, marks, Desc);

                                }
                                else
                                {
                                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("This_GameDetails_Alrety_Assign_This_Student");
                                    //string ErrorMessage = "GameDetailsAreExist";
                                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }

                            }
                        }
                        string Message = eAcademy.Models.ResourceCache.Localize("Game_Assigned_Successfully"); 
                        //string Message = "Saved Success";
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("StudentNameExist");
                        //string ErrorMessage = "StudentNameExist";
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }




         IList<StudentGamevalidation> studentgameList1 = new List<StudentGamevalidation>();

        public JsonResult StudentGameList(string sidx, string sord, int page, int rows, int? fAcyear, int? fSession, int? fClass, int? fSection)
        {
            AssignGametoStudentServices obj_ags = new AssignGametoStudentServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            //studentgameList1 = obj_ags.getStudentGameName();
            if (fAcyear.HasValue && fClass.HasValue && fSection.HasValue && fSession.HasValue)
            {
                studentgameList1 = obj_ags.getstudentgameList(fAcyear, fSession, fClass, fSection);
            }
            

            int totalRecords = studentgameList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    studentgameList1 = studentgameList1.OrderByDescending(s => s.AssignGamestoStudentId).ToList();
                    studentgameList1 = studentgameList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    studentgameList1 = studentgameList1.OrderBy(s => s.AssignGamestoStudentId).ToList();
                    studentgameList1 = studentgameList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = studentgameList1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        //public ActionResult studentgameExportToPDF()
        //{
        //    try
        //    {
        //        string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
        //      //  string fileName = "GameList";
        //        string fileName = eAcademy.Models.ResourceCache.Localize("GameStudentList");
        //        GeneratePDF.ExportPDF_Portrait(studentgameList1, new string[] { "AcademicYear", "StudentName", "HouseName", "LevelName", "GameName", "prize", "Marks", "Description", "Status", "Action" }, xfilePath, fileName);
        //        return File(xfilePath, "application/pdf", "GameName.pdf");
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.error = ex.Message;
        //        return View("Error");
        //    }


        //}


        public JsonResult GetStudents(int passYearId, string passClassSecId)
        {
            try
            {
                string clasSec = passClassSecId;
                string[] words = clasSec.Split(' ');
                int classId = Convert.ToInt32(words[0]);
                int secId = Convert.ToInt32(words[1]);
                AssignClassToStudentServices stu = new AssignClassToStudentServices();
                var StudentList = stu.getStudents1(passYearId, classId, secId);
                return Json(StudentList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EditStudentGame(int AssignGamestoStudentId)
        {
            try
            {
                AssignGametoStudentServices obj_ags = new AssignGametoStudentServices();
                int EGSid = Convert.ToInt32(AssignGamestoStudentId);
                var ans = obj_ags.getEditGamestudent(EGSid);
                var ansGameDetails = obj_ags.getEditGameDetails(EGSid);
                return Json(new { ans, ansGameDetails }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }



        public JsonResult UpdateStudentGame(UpdateStudentGame model)
        {
            try
            {
                AssignGametoStudentServices obj_ags = new AssignGametoStudentServices();
                SportsAssignGametoStudentConfigServices asc = new SportsAssignGametoStudentConfigServices();
                if (ModelState.IsValid)
                {
                    int EmployeeId = Convert.ToInt16(Session["empRegId"].ToString());
                    int AssignGametoStudentId = model.AssignGametoStudentId;
                    int HouseId = model.HouseId;
                    string StudentType = model.StudentType;
                    bool Status = model.Status;
                    int[] UpdateLevelId = model.UpdateLevelId;
                    int[] UpdatePointId = model.UpdatePointId;
                    int[] UpdateMarks = model.UpdateMarks;
                    string[] UpdateDescription = model.UpdateDescription;

                    obj_ags.updateGameStudent(AssignGametoStudentId, HouseId, StudentType, EmployeeId, Status);

                    int AssgConfigId = 0;
                    int GameId = 0;
                    int LevelId = 0;
                    int PointId = 0;
                    int Marks = 0;
                    string Description = null;
                    int counter = model.UpdateGameId.Length;
                    for (int i = 0; i < counter; i++)
                    {
                        AssgConfigId = Convert.ToInt32(model.UpdateAssgConfigId[i]);
                        GameId = Convert.ToInt32(model.UpdateGameId[i]);
                        LevelId = Convert.ToInt32(model.UpdateLevelId[i]);
                        PointId = Convert.ToInt32(model.UpdatePointId[i]);
                        Marks = Convert.ToInt32(model.UpdateMarks[i]);
                        Description = model.UpdateDescription[i];

                        if (model.UpdateAssgConfigId[i] == null)
                        {

                            bool cc = obj_ags.checkAcitivity(AssignGametoStudentId, GameId, LevelId, PointId);
                            if (cc == false)
                            {
                                asc.AddGameDetails(AssignGametoStudentId, GameId, LevelId, PointId, Marks, Description);

                            }
                            else
                            {
                                
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("StudentGameDetailsExist");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }

                        }
                        else
                        {

                            obj_ags.UpdateGameDetails(AssgConfigId, GameId, LevelId, PointId, Marks, Description);
                        }

                    }
                    string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }


                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }



        public ActionResult DeleteStudentGame(int AssignGamestoStudentId)
        {
            try
            {
                AssignGametoStudentServices ags = new AssignGametoStudentServices();
                ags.DeleteStudentGame(AssignGamestoStudentId);
                ags.DeleteStudentGameDetails(AssignGamestoStudentId);
                string Message = "Deleted Successfully";
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DeleteStudentGameDetails(int passGameDetailsConficId)
        {
            try
            {
                AssignGametoStudentServices ags = new AssignGametoStudentServices();
                ags.DeleteStudentGameDetails(passGameDetailsConficId);
                string Message = "Deleted Successfully";
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult studentgameListExportToPdf(bool? pdf)
        {
            try
            {
                if (!pdf.HasValue)
                {
                    return View();
                }
                else
                {
                    string filePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                   // string heading = "studentgameList1";
                    string heading = eAcademy.Models.ResourceCache.Localize("GameStudentList");
                    GeneratePDF.ExportPDF_Portrait(studentgameList1, new string[] { "AcademicYear", "StudentName", "HouseName", "StudentType", "GameName", "Status" }, filePath, heading);
                    return File(filePath, "application/pdf", "studentgameList1.pdf");

                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }

        }
        public ActionResult studentgameListExportToExcel()
        {
            try
            {
                var list = studentgameList1.Select(e => new { AcademicYear = e.AcademicYear, StudentName = e.StudentName, HouseName = e.HouseName, StudentType = e.StudentType, GameName = e.GameName, Status = Convert.ToString(e.Status) }).ToList();
                //string fileName = "StudentGameList1";
                string fileName = eAcademy.Models.ResourceCache.Localize("GameStudentList");
                GenerateExcel.ExportExcel(list, fileName);                
                return View("studentgameList1");

            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        //public ActionResult studentgameListExportToExcel()
        //{
        //    try
        //    {
        //        //IList<StudentGamevalidation> studentgameList1 = new List<StudentGamevalidation>();
        //        IList<StudentGamevalidation> studentgameList1 = (List<StudentGamevalidation>)Session["JQGridList"];
        //        var list = studentgameList1.Select(e => new { AcademicYear = e.AcademicYear, StudentName = e.StudentName, HouseName = e.HouseName, StudentType = e.StudentType, GameName = e.GameName, Status = Convert.ToString(e.Status) }).ToList();
        //        string fileName = "StudentGameList1";
        //        GenerateExcel.ExportExcel(list, fileName);
        //        return View("StudentGameList1");
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.error = ex.Message;
        //        return View("Error");
        //    }
        //}
        //public ActionResult studentgameListExportToPdf()
        //{
        //    try
        //    {
        //        IList<StudentGamevalidation> studentgameList1 = (List<StudentGamevalidation>)Session["JQGridList"];
        //        //List<IndividualStudentReportValidation> IndividualStudentListSearch1 = (List<IndividualStudentReportValidation>)Session["JQGridList"];
        //        string filePath = Server.MapPath("~/Views/") + "Sample1.pdf";
        //        string heading = "studentgameList1";
        //        GeneratePDF.ExportPDF_Portrait(studentgameList1, new string[] { "AcademicYear", "StudentName", "HouseName", "StudentType", "GameName", "Status" }, filePath, heading);
        //        return File(filePath, "application/pdf", "studentgameList1.pdf");
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.error = ex.Message;
        //        return View("Error");
        //    }
        //}







        //GAME SCHEDULE
        
        public ActionResult GameSchedule()
        {
            return View();
        }

        //AddGameSchedule-START
        [HttpPost]
        public JsonResult AddGameSchedule(VM_GameSchedule vgs)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    GameScheduleServices gss = new GameScheduleServices();
                    SportsGameScheduleConfigServices gsc = new SportsGameScheduleConfigServices();
                    int empid = Convert.ToInt16(Session["empRegId"].ToString());


                    int gsid = gss.addgamescheduleService(vgs.AcademicYearId, vgs.SessionId, vgs.GameId, vgs.LevelConFigId, vgs.GameType, vgs.ScheduleDate, vgs.ScheduleTime, empid);

                    int count = vgs.HouseId.Length;
                    // int count1 = vgs.PlayerStatusId;S
                    int GameScheduleId = gsid;
                    for (int i = 0; i < count; i++)
                    {
                        int HId = vgs.HouseId[i];
                        int PId = vgs.PlayerStatusId[i];
                        gsc.addGameScheduleConfig(GameScheduleId, HId, PId);
                    }
                    string Message = eAcademy.Models.ResourceCache.Localize("SuccessfullySaved");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }

        //Get Value from Change function GameName to GameType
        public JsonResult GetSelectedGameType(int PGameId)
        {
            GameScheduleServices gss = new GameScheduleServices();
            var gtype = gss.getGameType(PGameId);
            return Json(new { gtype = gtype }, JsonRequestBehavior.AllowGet);
        }


        //Get value from change function gamename to gamepoints
        public JsonResult GetSelectedGamePoints(int passGameId)
        {
            GameScheduleServices gss = new GameScheduleServices();
            var List = gss.getPointlist(passGameId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }

        //Get Values From Change function for GameName, Level
        public JsonResult GetGameLevel(int passGameId)
        {
            GameScheduleServices gss = new GameScheduleServices();
            var List = gss.getLevel(passGameId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }

        //Get Values From Change function for AcademicYear,Session
        public JsonResult GetSelectedSession(int passAcademicYearId)
        {
            GameScheduleServices gss = new GameScheduleServices();
            var List = gss.getSession(passAcademicYearId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CustomeActiveDate(int acid)
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var ans = AcYear.date(acid);
            //string sdate1 = ans.StartDate.ToString("dd/MM/yyyy");
            //string edate1 = ans.EndDate.ToString("dd/MM/yyyy");
            string StartingEnableDate ;
            string EndingEnableDate ;
            EventServices holidays = new EventServices();
            var HolidaysList = holidays.getHolidaysList(acid);
            DateTime CurrentDate = DateTimeByZone.getCurrentDateTime();
            DateTime sdate = ans.StartDate;
            DateTime edate = ans.EndDate;
            if (sdate <= CurrentDate && CurrentDate <= edate)
            {
                StartingEnableDate = CurrentDate.ToString("dd/MM/yyyy");
                EndingEnableDate = edate.ToString("dd/MM/yyyy");
                return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                EndingEnableDate = edate.ToString("dd/MM/yyyy");
                return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }



        }



        //public JsonResult GetHouseStatus()
        //{
        //    //AssignGametoStudentServices obj_as = new AssignGametoStudentServices();
        //    GameScheduleServices gss = new GameScheduleServices();
        //    try
        //    {
        //        var list = gss.getHouselist();
        //        return Json(list, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        string ExceptionError = e.Message.ToString();
        //        return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
        //    }

        //}

        //public JsonResult GetPointStatus()
        //{

        //    GameScheduleServices gss = new GameScheduleServices();

        //    try
        //    {
        //        var list = gss.getPointlist();
        //        return Json(list, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        string ExceptionError = e.Message.ToString();
        //        return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
        //    }

        //}
        //AddGameSchedule-END

        //GameScheduleList

        IList<GameScheduleValidation> GamescheduleList1 = new List<GameScheduleValidation>();

        public JsonResult GamescheduleList(string sidx, string sord, int page, int rows, int? fAcademicYear, int? fSessionName)
        {
            GameScheduleServices obj_gss = new GameScheduleServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;


            if (fAcademicYear.HasValue && fSessionName.HasValue)
            {
                GamescheduleList1 = obj_gss.getGameSchedule(fAcademicYear, fSessionName);
            }

            int totalRecords = GamescheduleList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    GamescheduleList1 = GamescheduleList1.OrderByDescending(s => s.GameScheduleId).ToList();
                    GamescheduleList1 = GamescheduleList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    GamescheduleList1 = GamescheduleList1.OrderBy(s => s.GameScheduleId).ToList();
                    GamescheduleList1 = GamescheduleList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = GamescheduleList1;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = GamescheduleList1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        //EditGameSchedule-START

        public JsonResult EditGameSchedule(int GameScheduleId)
        {
            try
            {
                GameScheduleServices gss = new GameScheduleServices();
                int EGSid = Convert.ToInt32(GameScheduleId);
                var ans = gss.getEditGameschedule(EGSid);
                var detail = gss.getEditDetailGameschedule(EGSid);
                return Json(new { ans, detail }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        //gethouse value from list
        public JsonResult GetEditHouseDetail()
        {
            GameScheduleServices gss = new GameScheduleServices();
            try
            {
                var list = gss.getHouselist();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }
        //getPoint value from list
        public JsonResult GetEditPointDetail(int passGameId)
        {
            GameScheduleServices gss = new GameScheduleServices();
            try
            {
                var list = gss.getPointDetail(passGameId);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }


        public JsonResult GetEditHouseStatus()
        {

            GameScheduleServices gss = new GameScheduleServices();
            try
            {
                var list = gss.getEditHouselist();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetEditPointStatus()
        {

            GameScheduleServices gss = new GameScheduleServices();

            try
            {
                var list = gss.getEditPointlist();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }

        //delete row
        public ActionResult DeleteGameScheduleDetails(int passGameScheduleConficId)
        {
            try
            {
                GameScheduleServices ags = new GameScheduleServices();
                ags.deleteSchedule(passGameScheduleConficId);
                string Message = eAcademy.Models.ResourceCache.Localize("DeletedSuccessfully");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }



        //Update
        GameScheduleServices gss = new GameScheduleServices();
        SportsGameScheduleConfigServices gsc = new SportsGameScheduleConfigServices();
        public JsonResult updateGameSchedule(UpdateGameSchedule model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int EmpId = Convert.ToInt16(Session["empRegId"].ToString());
                    int GameScheduleId = model.GameScheduleId;
                    int[] HouseId = model.HouseId;
                    int[] PointId = model.PointId;
                    bool Status = model.Status.Value;

                    gss.UpdateGameSchedule(GameScheduleId, model.ScheduleDate, model.ScheduleTime, EmpId, Status);

                    int counter = model.HouseId.Length;
                    for (int i = 0; i < counter; i++)
                    {
                        int SchedConId = Convert.ToInt32(model.ScheduleConfigId[i]);
                        int HId = Convert.ToInt32(model.HouseId[i]);
                        int PId = Convert.ToInt32(model.PointId[i]);
                        bool cc = gss.checkActivity(GameScheduleId, HId, PId);

                        if (cc == false)
                        {
                            if (SchedConId == null || SchedConId == 0)
                            {
                                gsc.addGameScheduleConfig(GameScheduleId, HId, PId);

                            }
                            else
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("ScheduleDetailsexist");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }

                        }
                        else
                        {
                            gss.UpdateGameScheduleDetails(SchedConId, HId, PId);
                            //string ErrorMessage = "Schedule Details exist";
                            //return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully"); ;

                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }


                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteGameSchedule(int GameScheduleId)
        {
            try
            {
                GameScheduleServices gss = new GameScheduleServices();


                bool dh = gss.CheckDeleteActivity(GameScheduleId);

                if (dh == true)
                {
                    gss.DeleteGameScheduleConfig(GameScheduleId);

                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("AlreadyUsedinGameScheduleSoCannotDelete");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }

                gss.DeleteGameSched(GameScheduleId);
                string Message = eAcademy.Models.ResourceCache.Localize("ResourceCache.Localize(");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult GameScheduleListToExcel()
        {
            try
            {
                // IList<GameScheduleValidation> GamescheduleList1 = new List<GameScheduleValidation>();
                List<GameScheduleValidation> GamescheduleList1 = (List<GameScheduleValidation>)Session["JQGridList"];
                var list = GamescheduleList1.Select(o => new { GameName = o.GameName, LevelName = o.LevelName, GameType = o.GameType, Date = o.Date, Time = o.Time, Status = o.Status }).ToList();
               // string fileName = "GamescheduleList1";
                string fileName = eAcademy.Models.ResourceCache.Localize("GamescheduleList");
                GenerateExcel.ExportExcel(list, fileName);
                return View("GamescheduleList");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult GameScheduleListToPdf()
        {
            try
            {
                List<GameScheduleValidation> GamescheduleList1 = (List<GameScheduleValidation>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                //string fileName = "GamescheduleList1";
                string fileName = eAcademy.Models.ResourceCache.Localize("GamescheduleList");
                GeneratePDF.ExportPDF_Portrait(GamescheduleList1, new string[] { "GameName", "LevelName", "GameType", "Date", "Time", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "GamescheduleList1.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }






        ////GAME SCHEDULE

        //public ActionResult GameSchedule()
        //{
        //    return View();
        //}

        ////AddGameSchedule-START
        //[HttpPost]
        //public JsonResult AddGameSchedule(VM_GameSchedule vgs)
        //{
        //    try
        //    {

        //        if (ModelState.IsValid)
        //        {
        //            GameScheduleServices gss = new GameScheduleServices();
        //            SportsGameScheduleConfigServices gsc = new SportsGameScheduleConfigServices();
        //            int empid = Convert.ToInt16(Session["empRegId"].ToString());


        //            int gsid = gss.addgamescheduleService(vgs.AcademicYearId, vgs.SessionId, vgs.GameId, vgs.LevelConFigId, vgs.GameType, vgs.ScheduleDate, vgs.ScheduleTime, empid);

        //            int count = vgs.HouseId.Length;
        //            // int count1 = vgs.PlayerStatusId;
        //            int GameScheduleId = gsid;
        //            for (int i = 0; i < count; i++)
        //            {

        //                int HId = vgs.HouseId[i];
        //                int PId = vgs.PlayerStatusId[i];
        //                gsc.addGameScheduleConfig(GameScheduleId, HId, PId);
        //            }
        //            string Message = "Saved Success";
        //            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            string totalError = "";
        //            string[] t = new string[ModelState.Values.Count];
        //            int i = 0;
        //            foreach (var obj in ModelState.Values)
        //            {
        //                foreach (var error in obj.Errors)
        //                {
        //                    if (!string.IsNullOrEmpty(error.ErrorMessage))
        //                    {
        //                        totalError = totalError + error.ErrorMessage + Environment.NewLine;
        //                        t[i] = error.ErrorMessage;
        //                        i++;
        //                    }
        //                }
        //            }
        //            return Json(new { Success = 0, ex = t });
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        string ExceptionError = e.Message.ToString();
        //        return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
        //    }

        //}

        ////Get Value from Change function GameName to GameType
        //public JsonResult GetSelectedGameType(int PGameId)
        //{
        //    GameScheduleServices gss = new GameScheduleServices();
        //    var gtype = gss.getGameType(PGameId);
        //    return Json(new { gtype = gtype }, JsonRequestBehavior.AllowGet);
        //}


        ////Get value from change function gamename to gamepoints
        //public JsonResult GetSelectedGamePoints(int passGameId)
        //{
        //    GameScheduleServices gss = new GameScheduleServices();
        //    var List = gss.getPointlist(passGameId);
        //    return Json(List, JsonRequestBehavior.AllowGet);
        //}

        ////Get Values From Change function for GameName, Level
        //public JsonResult GetGameLevel(int passGameId)
        //{
        //    GameScheduleServices gss = new GameScheduleServices();
        //    var List = gss.getLevel(passGameId);
        //    return Json(List, JsonRequestBehavior.AllowGet);
        //}

        ////Get Values From Change function for AcademicYear,Session
        //public JsonResult GetSelectedSession(int passAcademicYearId)
        //{
        //    GameScheduleServices gss = new GameScheduleServices();
        //    var List = gss.getSession(passAcademicYearId);
        //    return Json(List, JsonRequestBehavior.AllowGet);
        //}

        ////public JsonResult GetHouseStatus()
        ////{
        ////    //AssignGametoStudentServices obj_as = new AssignGametoStudentServices();
        ////    GameScheduleServices gss = new GameScheduleServices();
        ////    try
        ////    {
        ////        var list = gss.getHouselist();
        ////        return Json(list, JsonRequestBehavior.AllowGet);
        ////    }
        ////    catch (Exception e)
        ////    {
        ////        string ExceptionError = e.Message.ToString();
        ////        return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
        ////    }

        ////}

        ////public JsonResult GetPointStatus()
        ////{

        ////    GameScheduleServices gss = new GameScheduleServices();

        ////    try
        ////    {
        ////        var list = gss.getPointlist();
        ////        return Json(list, JsonRequestBehavior.AllowGet);
        ////    }
        ////    catch (Exception e)
        ////    {
        ////        string ExceptionError = e.Message.ToString();
        ////        return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
        ////    }

        ////}
        ////AddGameSchedule-END

        ////GameScheduleList

        //static IList<GameScheduleValidation> GamescheduleList1 = new List<GameScheduleValidation>();

        //public JsonResult GamescheduleList(string sidx, string sord, int page, int rows, int? fAcademicYear, int? fSessionName)
        //{
        //    GameScheduleServices obj_gss = new GameScheduleServices();
        //    int pageIndex = Convert.ToInt32(page) - 1;
        //    int pageSize = rows;


        //    if (fAcademicYear.HasValue && fSessionName.HasValue)
        //    {
        //        GamescheduleList1 = obj_gss.getGameSchedule(fAcademicYear, fSessionName);
        //    }

        //    int totalRecords = GamescheduleList1.Count();
        //    var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
        //    if (sord != null)
        //    {
        //        if (sord.ToUpper() == "DESC")
        //        {
        //            GamescheduleList1 = GamescheduleList1.OrderByDescending(s => s.GameScheduleId).ToList();
        //            GamescheduleList1 = GamescheduleList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
        //        }
        //        else
        //        {
        //            GamescheduleList1 = GamescheduleList1.OrderBy(s => s.GameScheduleId).ToList();
        //            GamescheduleList1 = GamescheduleList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
        //        }
        //    }
        //    var jsonData = new
        //    {
        //        total = totalPages,
        //        page,
        //        records = totalRecords,
        //        rows = GamescheduleList1
        //    };
        //    return Json(jsonData, JsonRequestBehavior.AllowGet);
        //}


        ////EditGameSchedule-START

        //public JsonResult EditGameSchedule(int GameScheduleId)
        //{
        //    try
        //    {
        //        GameScheduleServices gss = new GameScheduleServices();
        //        int EGSid = Convert.ToInt32(GameScheduleId);
        //        var ans = gss.getEditGameschedule(EGSid);
        //        var detail = gss.getEditDetailGameschedule(EGSid);
        //        return Json(new { ans, detail }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        var ErrorMessage = e.Message.ToString();
        //        return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
        //    }
        //}
        ////gethouse value from list
        //public JsonResult GetEditHouseDetail()
        //{
        //    GameScheduleServices gss = new GameScheduleServices();
        //    try
        //    {
        //        var list = gss.getHouselist();
        //        return Json(list, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        string ExceptionError = e.Message.ToString();
        //        return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
        //    }

        //}
        ////getPoint value from list
        //public JsonResult GetEditPointDetail(int passGameId)
        //{
        //    GameScheduleServices gss = new GameScheduleServices();
        //    try
        //    {
        //        var list = gss.getPointDetail(passGameId);
        //        return Json(list, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        string ExceptionError = e.Message.ToString();
        //        return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
        //    }

        //}


        //public JsonResult GetEditHouseStatus()
        //{

        //    GameScheduleServices gss = new GameScheduleServices();
        //    try
        //    {
        //        var list = gss.getEditHouselist();
        //        return Json(list, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        string ExceptionError = e.Message.ToString();
        //        return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
        //    }

        //}

        //public JsonResult GetEditPointStatus()
        //{

        //    GameScheduleServices gss = new GameScheduleServices();

        //    try
        //    {
        //        var list = gss.getEditPointlist();
        //        return Json(list, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        string ExceptionError = e.Message.ToString();
        //        return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
        //    }

        //}

        ////delete row
        //public ActionResult DeleteGameScheduleDetails(int passGameScheduleConficId)
        //{
        //    try
        //    {
        //        GameScheduleServices ags = new GameScheduleServices();
        //        ags.deleteSchedule(passGameScheduleConficId);
        //        string Message = "Deleted Successfully";
        //        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        string ExceptionError = e.Message.ToString();
        //        return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
        //    }
        //}



        ////Update
        //GameScheduleServices gss = new GameScheduleServices();
        //SportsGameScheduleConfigServices gsc = new SportsGameScheduleConfigServices();
        //public JsonResult updateGameSchedule(UpdateGameSchedule model)
        //{

        //    try
        //    {

        //        if (ModelState.IsValid)
        //        {
        //            int EmpId = Convert.ToInt16(Session["empRegId"].ToString());
        //            int GameScheduleId = model.GameScheduleId;
        //            int[] HouseId = model.HouseId;
        //            int[] PointId = model.PointId;
        //            bool Status = model.Status.Value;

        //            gss.UpdateGameSchedule(GameScheduleId, model.ScheduleDate, model.ScheduleTime, EmpId, Status);

        //            int counter = model.HouseId.Length;
        //            for (int i = 0; i < counter; i++)
        //            {
        //                int SchedConId = Convert.ToInt32(model.ScheduleConfigId[i]);
        //                int HId = Convert.ToInt32(model.HouseId[i]);
        //                int PId = Convert.ToInt32(model.PointId[i]);
        //                bool cc = gss.checkActivity(GameScheduleId, HId, PId);

        //                if (cc == false)
        //                {
        //                    if (SchedConId == null || SchedConId == 0)
        //                    {
        //                        gsc.addGameScheduleConfig(GameScheduleId, HId, PId);

        //                    }
        //                    else
        //                    {
        //                        string ErrorMessage = "Schedule Details exist";
        //                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
        //                    }

        //                }
        //                else
        //                {
        //                    gss.UpdateGameScheduleDetails(SchedConId, HId, PId);
        //                    //string ErrorMessage = "Schedule Details exist";
        //                    //return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            string Message = " Updated_Successfully";

        //            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
        //        }


        //        else
        //        {
        //            string totalError = "";
        //            string[] t = new string[ModelState.Values.Count];
        //            int i = 0;
        //            foreach (var obj in ModelState.Values)
        //            {
        //                foreach (var error in obj.Errors)
        //                {
        //                    if (!string.IsNullOrEmpty(error.ErrorMessage))
        //                    {
        //                        totalError = totalError + error.ErrorMessage + Environment.NewLine;
        //                        t[i] = error.ErrorMessage;
        //                        i++;
        //                    }
        //                }
        //            }
        //            return Json(new { Success = 0, ex = t });
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        string ExceptionError = e.Message.ToString();
        //        return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
        //    }



        //}

        //public ActionResult DeleteGameSchedule(int GameScheduleId)
        //{
        //    try
        //    {
        //        GameScheduleServices gss = new GameScheduleServices();


        //        bool dh = gss.CheckDeleteActivity(GameScheduleId);

        //        if (dh == true)
        //        {
        //            gss.DeleteGameScheduleConfig(GameScheduleId);

        //        }
        //        else
        //        {
        //            string ErrorMessage = " Already Used in GameSchedule So Cannot Delete";
        //            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
        //        }

        //        gss.DeleteGameSched(GameScheduleId);
        //        string Message = "Deleted Successfully";
        //        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);


        //    }
        //    catch (Exception e)
        //    {
        //        string ExceptionError = e.Message.ToString();
        //        return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
        //    }
        //}


        //public ActionResult GameScheduleListToExcel()
        //{
        //    try
        //    {
        //        var list = GamescheduleList1.Select(o => new { Academicyear = o.AcademicYear, Session = o.SessionName, GameName = o.GameName, LevelName = o.LevelName, GameType = o.GameType, Date = o.Date, Time = o.Time, Status = o.Status }).ToList();
        //        GenerateExcel.ExportExcel(list);
        //        return View("GamescheduleList1");
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.error = ex.Message;
        //        return View("Error");
        //    }
        //}
        //public ActionResult GameScheduleListToPdf()
        //{
        //    try
        //    {
        //        string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
        //        string fileName = "GamescheduleList1";
        //        GeneratePDF.ExportPDF_Portrait(GamescheduleList1, new string[] { "GameName", "LevelName", "GameType", "Date", "Time", "Status", "Action" }, xfilePath, fileName);
        //        return File(xfilePath, "application/pdf", "GamescheduleList1.pdf");
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.error = ex.Message;
        //        return View("Error");
        //    }
        //}

        //// GameSchedule-End






        //Session Result

        public ActionResult SessionResult()
        {
            return View();
        }

        public JsonResult GetHouse()
        {
            Sessionresultservices obj_as = new Sessionresultservices();
            try
            {
                var Houselist = obj_as.getHouselist();
                return Json(Houselist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]

        public JsonResult addSessionresult(VM_Sessionresult vsr)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    Sessionresultservices obj_srs = new Sessionresultservices();
                    SportsChampionHouseServices obj_schs = new SportsChampionHouseServices();
                    int AcyearId = vsr.AcademicYearId;
                    int GameIdCount = vsr.GameId.Length;
                    int? FinalResultIdCount = vsr.ChampionHouseId.Length;
                    int SessionIdCount = vsr.SessionId.Length;
                    int? HouseIdCount = vsr.HouseId.Length;
                    bool status = true;
                    var k = 0;
                    var m = 0;
                    var n = 0;
                    var l = 0;
                    var v = 0;
                    int editHouseId = 0;
                    int editIdPk = 0;
                    int editFRIdPk = 0;
                    int editFRHouseId = 0;


                    for (var i = 0; i < GameIdCount; i++)
                    {
                        int Game = Convert.ToInt32(vsr.GameId[i]);
                        for (var j = 0; j < SessionIdCount; j++)
                        {
                            int Session = Convert.ToInt32(vsr.SessionId[j]);
                            int House = Convert.ToInt32(vsr.HouseId[k]);
                            if (House == 0)
                            {
                                int NextSession = Convert.ToInt32(vsr.SessionId[j]);
                                k++;
                                l++;
                            }

                            else
                            {
                                if (vsr.HouseIdPK == null)
                                {
                                    obj_srs.addSessionResult(AcyearId, Game, Session, House, status);
                                }
                                else if (vsr.HouseIdPK[l] == null)
                                {
                                    obj_srs.addSessionResult(AcyearId, Game, Session, House, status);
                                }
                                else
                                {
                                    editIdPk = Convert.ToInt32(vsr.HouseIdPK[l]);
                                    editHouseId = Convert.ToInt32(vsr.HouseId[k]);
                                    obj_srs.UpdateHouseId(editHouseId, editIdPk);
                                }
                                k++;
                                l++;
                            }
                        }

                    }


                    for (var i = 0; i < GameIdCount; i++)
                    {
                        int Game = Convert.ToInt32(vsr.GameId[i]);
                        int FinalResult = Convert.ToInt32(vsr.ChampionHouseId[m]);
                        if (FinalResult == 0)
                        {
                            int NextFinalResult = Convert.ToInt32(vsr.ChampionHouseId[m]);
                            m++;
                            v++;
                        }
                        else
                        {
                            if (vsr.FinalResultPk == null)
                            {
                                obj_schs.AddChampionHouse(AcyearId, Game, FinalResult, status);
                            }

                            else if (vsr.FinalResultPk[v] == null)
                            {
                                obj_schs.AddChampionHouse(AcyearId, Game, FinalResult, status);
                            }
                            else
                            {
                                editFRIdPk = Convert.ToInt32(vsr.FinalResultPk[v]);
                                editFRHouseId = Convert.ToInt32(vsr.ChampionHouseId[m]);
                                obj_schs.UpdateChampionHouseId(editFRHouseId, editFRIdPk);
                                n++;
                            }
                            m++;
                            v++;
                        }
                    }
                  //  string Message = eAcademy.Models.ResourceCache.Localize("Game_Added_Successfully"); 
                    string Message = eAcademy.Models.ResourceCache.Localize("SuccessfullySaved");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }

                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }


        static IList<SportsReportValidation> SessionResultList1 = new List<SportsReportValidation>();


        [HttpGet]
        public JsonResult GetGameDetail(int passYearId)
        {
            Sessionresultservices obj_as = new Sessionresultservices();
            SportsChampionHouseServices obj_schs = new SportsChampionHouseServices();
            try
            {
                var GameNameList = obj_as.getGameDetail(passYearId);
                var SessionNameList = obj_as.getSessionDetails(passYearId);
                var checkDataExist = obj_schs.YearExist(passYearId);

                if (checkDataExist != null)
                {
                    var getHid = obj_as.GetDetails(passYearId);
                    var getCHid = obj_schs.CheckChId(passYearId);
                    var SessionCounts = obj_as.GetSessionCount(passYearId);
                    var GameCounts = obj_as.GetGameCount(passYearId);

                    return Json(new { Message = "old", GameNameList, SessionNameList, getHid, getCHid, SessionCounts, GameCounts }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Message = "new", GameNameList, SessionNameList }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }

        }   
       
    }
    }

