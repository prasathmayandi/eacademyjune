﻿
using eAcademy.Areas.Sports.Models;
using eAcademy.Areas.Sports.Services;
using eAcademy.Areas.Sports.ViewModel;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace eAcademy.Areas.Sports.Controllers
{

    [CheckSessionOutAttribute]
    public class SportsReportController : Controller
    {
        int empregid;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AcademicyearServices Acyear = new AcademicyearServices();
            ViewBag.allAcademicYears = Acyear.ShowAcademicYears();
            SessionServices SName = new SessionServices();
            ViewBag.allSessionNames = SName.ShowSessionNames();
            GamePointServices GamePrize = new GamePointServices();
            ViewBag.allSportsGamePoints = GamePrize.ShowSportsGamepoints();
            ViewBag.SportsGamePoints = GamePrize.SportsGamepoints();

            ClassServices allClass = new ClassServices();
            ViewBag.allClasses = allClass.ShowClasses();

            HouseServices allHouse = new HouseServices();
            ViewBag.allHouses = allHouse.ShowHouses();

            GameServices GameName = new GameServices();
            ViewBag.allGameNames = GameName.ShowGameNames();
            GamePointServices allPoint = new GamePointServices();
            ViewBag.allPoints = allPoint.ShowSportsGamepoints();
            SectionServices allSection = new SectionServices();

            ViewBag.acYear = Acyear.AllList_AcademicYear();
            ViewBag.AddacYear = Acyear.Add_formAcademicYear();

            //AcademicyearServices Acyear = new AcademicyearServices();
            //ViewBag.allAcademicYears = Acyear.ShowAcademicYears();
            //SessionServices SName = new SessionServices();
            //ViewBag.allSessionNames = SName.ShowSessionNames();

        }


        //LEVELREPORT
        public ActionResult LevelReport()
        {
            return View();
        }

        GameLevelReportServices glr = new GameLevelReportServices();
        //Get Values From Change function for AcademicYearto Session
        public JsonResult GetSessionReport(int passAcademicYearId)
        {

            var List = glr.getSession(passAcademicYearId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }



        //Get Values From Change function for AcademicYear to GameName
        public JsonResult GetGameReport(int passAcademicYearId)
        {

            var List = glr.getGameName(passAcademicYearId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }



        //Get Values From Change function for GameName, Level
        public JsonResult GetGameLevelReport(int passGameId)
        {

            var List = glr.getLevel(passGameId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }


      IList<GameLevelReportValidation> GameLevelReportList1 = new List<GameLevelReportValidation>();

        public JsonResult GameLevelReportList(string sidx, string sord, int page, int rows, int? Acyear, int? SName, int? Level, int? GName)
        {

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            if (Acyear.HasValue && SName.HasValue && SName.HasValue && Level.HasValue && GName.HasValue)
            {
                GameLevelReportList1 = glr.getLevelReportList(Acyear, SName, Level, GName);
            }

            int totalRecords = GameLevelReportList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {

                    GameLevelReportList1 = GameLevelReportList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    GameLevelReportList1 = GameLevelReportList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = GameLevelReportList1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        public ActionResult LevelSearchResultToPDF(bool? pdf)
        {
            try
            {
                if (!pdf.HasValue)
                {
                    return View();
                }
                else
                {
                    string filePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                    string heading = "GameLevelReportList";
                    GeneratePDF.ExportPDF_Portrait(GameLevelReportList1, new string[] { "StudentName", "HouseName", "Result" }, filePath, heading);
                    return File(filePath, "application/pdf", "GameLevelReportList.pdf");
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }

        }

        public ActionResult LevelSearchResultToExcel()
        {
            try
            {
                var list = GameLevelReportList1.Select(e => new { StudentName = e.StudentFName + " " + e.StudentLName, HouseName = e.HouseName, Result = e.Result }).ToList();
                GenerateExcel.ExportExcel(list);
                return View("GameLevelReportList");

            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }

        }

        //Level-Report End



        //  Individual Student Report-Start

        public ActionResult IndividualStudentReport()
        {
            return View();
        }

        public JsonResult GetSelectedClassSection(int passClassId)
        {
            SectionServices sec = new SectionServices();
            var List = sec.getSection(passClassId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetSectionStudents(int passYearId, int passClassId, int passSecId)
        {
            try
            {
                AssignClassToStudentServices stu = new AssignClassToStudentServices();
                var StudentList = stu.getStudents(passYearId, passClassId, passSecId);
                return Json(StudentList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        IList<IndividualStudentReportValidation> IndividualStudentListSearch1 = new List<IndividualStudentReportValidation>();

        public JsonResult IndividualStudentListSearch(string sidx, string sord, int page, int rows, int? fAcyearId, int? fClassId, int? fSectionId, int? fStudentId)
        {
            IndividualStudentReportServices isr = new IndividualStudentReportServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            if (fAcyearId.HasValue && fClassId.HasValue && fSectionId.HasValue && fStudentId.HasValue)
            {
                IndividualStudentListSearch1 = isr.getIndividualStudentReportList(fAcyearId, fClassId, fSectionId, fStudentId);
            }

            int totalRecords = IndividualStudentListSearch1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    IndividualStudentListSearch1 = IndividualStudentListSearch1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    IndividualStudentListSearch1 = IndividualStudentListSearch1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = IndividualStudentListSearch1;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = IndividualStudentListSearch1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StudentListSearchExportToExcel()
        {
            try
            {
                List<IndividualStudentReportValidation> IndividualStudentListSearch1 = (List<IndividualStudentReportValidation>)Session["JQGridList"];
                var list = IndividualStudentListSearch1.Select(e => new { AcademicYear = e.AcademicYear, ClassName = e.ClassName, SectionName = e.SectionName, SessionName = e.SessionName, GameName = e.GameName, LevelName = e.LevelName, StudentType = e.StudentType, HouseName = e.HouseName, GameType = e.GameType, PrizeStatus = e.PrizeStatus, Marks = e.Marks, Description = e.Description }).ToList();
               // string fileName = "IndividualStudentListSearch1";
                string fileName = eAcademy.Models.ResourceCache.Localize("IndividualStudentList");
                GenerateExcel.ExportExcel(list, fileName);
                return View("IndividualStudentListSearch1");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult StudentListSearchExportToPDF()
        {
            try
            {
                List<IndividualStudentReportValidation> IndividualStudentListSearch1 = (List<IndividualStudentReportValidation>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                //string fileName = "IndividualStudentListSearch1";
                string fileName = eAcademy.Models.ResourceCache.Localize("IndividualStudentList");
                GeneratePDF.ExportPDF_Portrait(IndividualStudentListSearch1, new string[] { "AcademicYear", "ClassName", "SectionName", "SessionName", "GameName", "LevelName", "StudentType", "HouseName", "GameType", "PrizeStatus", "Marks", "Description" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "IndividualStudentListSearch1.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        //Individual Student Report-End



        public ActionResult OverAllReport()
        {
            return View();
        }

        public JsonResult GetSelectedYearSession(int passYearId)
        {
            SportsOverallReportServices stu = new SportsOverallReportServices();
            var List = stu.getSession(passYearId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetOverAllReport(int passYearId, int PassSessionId)
        {
            try
            {
                SportsOverallReportServices obj_ss = new SportsOverallReportServices();
                var AllReportList = obj_ss.getOverAllReport(passYearId, PassSessionId);
                if (AllReportList.Count() == 0)
                {
                    String ErrorMessage = "NoRecords";
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { AllReportList = AllReportList }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetOverAllYearReport(int passYearId)
        {
            try
            {
                SportsOverallReportServices obj_ss = new SportsOverallReportServices();
                var AllYearReportList = obj_ss.getOverAllYearReport(passYearId);
                if (AllYearReportList.Count() == 0)
                {
                    String ErrorMessage = "NoRecords";
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { AllYearReportList = AllYearReportList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}














       


      
    
