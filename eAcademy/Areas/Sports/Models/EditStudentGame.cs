﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class EditStudentGame
    {
        public int AssignGamestoStudentConfigId{ get; set; }
         //[Required(ErrorMessage = "Please Select Academic Year")]
        public int AcyearId{ get; set; }
        //[Required(ErrorMessage = "Please Select Session")]
         public int SessionId { get; set; }
        //[Required(ErrorMessage = "Please Select Class")]
         public int ClassId { get; set; }
        //[Required(ErrorMessage = "Please Select Section")]
        public int SectionId { get; set; }
        //[Required(ErrorMessage = "Please Select Student")]
          public int StudentRegisterId { get; set; }
        //[Required(ErrorMessage = "Please Select Type of the Student")]
         public string StudentType { get; set; }
        //[Required(ErrorMessage = "Please Select House")]
         public int HouseId { get; set; }
        //[Required(ErrorMessage = "Please Select Academic Year")]
        public int LevelId { get; set; }
        //[Required(ErrorMessage = "Please Select Academic Year")]
        public int GameId { get; set; }
        //[Required(ErrorMessage = "Please Select Academic Year")]
        public int PointId { get; set; }
        //[Required(ErrorMessage = "Please Select Academic Year")]
        // public int Marks { get; set; }
        //[Required(ErrorMessage = "Please Select Academic Year")]
        // public string Description { get; set; }
        //[Required(ErrorMessage = "Please Select Academic Year")]

        public bool Status { get; set; }
        public string Acyear { get; set; }
        public string SessionName { get; set; }
        public string Class{ get; set; }
        public string Section { get; set; }
        public string HouseName{ get; set; }    
        public string StudentName { get; set; }
        public int AssignGametoStudentId { get; set; }
        public string GameName { get; set; }
        public string LevelName{ get; set; }
        

        public string PointName { get; set; }
        public int? GameMark { get; set; }
        public string GameDescription { get; set; }

        
    }
    public class UpdateStudentGame
    {
        public int AssignGametoStudentId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                ErrorMessageResourceName = "PleaseSelectHouseName")]
        public int HouseId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                ErrorMessageResourceName = "pleaseselectStudentType")]
        public string StudentType { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectGameName")]
        public int[] UpdateGameId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                ErrorMessageResourceName = "PleaseSelectLevel")]
        public int[] UpdateLevelId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "Please_select_Points")]
        public int[] UpdatePointId { get; set; }

        public bool Status { get; set; }
        public int[] UpdateMarks { get; set; }
        public string[] UpdateDescription { get; set; }
        public int?[] UpdateAssgConfigId { get; set; }
    }
}