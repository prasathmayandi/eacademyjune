﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{

    public class UpdateGameSchedule
    {
        public int GameScheduleId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
               ErrorMessageResourceName = "PleaseSelectScheduleDate")]
        public DateTime ScheduleDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                ErrorMessageResourceName = "PleaseSelectScheduleTime")]
        public TimeSpan ScheduleTime { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                ErrorMessageResourceName = "PleaseSelectHouse")]
        public int[] HouseId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectPoint")]
        public int[] PointId { get; set; }

        public int?[] ScheduleConfigId { get; set; }
        public bool? Status { get; set; }
    }
    public class EditGameSchedule
    {

        public string AcademicYear { get; set; }
        public string SessionName { get; set; }
        public string LevelName { get; set; }
        public string GameName { get; set; }
        public string EDate { get; set; }
        public string ETime { get; set; }

        public int? GameScheduleId { get; set; }
        public int? AcademicYearId { get; set; }
        public int? SessionId { get; set; }
        public int? GameId { get; set; }
        public int? LevelId { get; set; }
        public string GameType { get; set; }
        public int? HouseId { get; set; }
        public string HouseName { get; set; }
        public int? PointId { get; set; }
        public string PointName { get; set; }
     
        public bool? Status { get; set; }
        public int? ScheduleConfigId { get; set; }
    }
}