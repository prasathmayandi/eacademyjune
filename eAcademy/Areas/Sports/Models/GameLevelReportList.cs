﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class GameLevelReportList
    {
        public int GameLevelReportId { get; set; }
        public int Acyear { get; set; }
        public string  SessionName { get; set; }
        public int SessionId { get; set; }
        public int GameId { get; set; }
        public int LevelId { get; set; }
        public string GameName { get; set; }
        public string LevelName { get; set; }
        public string StudentName { get; set; }
        public string HouseName { get; set; }
        public string PrizeStatus { get; set; }
    }
}