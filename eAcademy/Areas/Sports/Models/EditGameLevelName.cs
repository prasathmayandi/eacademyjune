﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class EditGameLevelName
    {
        public int GameLevelNameId { get; set; }
       
       
        public int LevelId { get; set; }
        [Required(ErrorMessage = "Please Enter LevelName")]
        public string GameLevelName { get; set; }
       

        public bool Status { get; set; }



    }
}