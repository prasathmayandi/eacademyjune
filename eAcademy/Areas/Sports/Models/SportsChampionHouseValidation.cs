﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class SportsChampionHouseValidation
    {
        public int? Id { get; set; }

        public int? AcademicYearId { get; set; }

        public int? GameId { get; set; }

        public string GameName { get; set; }

        public int? HouseId { get; set; }

        public string HouseName { get; set; }

        public int? ChampionHouseId { get; set; }

        public string ChampionHouseName { get; set; }
    }
}