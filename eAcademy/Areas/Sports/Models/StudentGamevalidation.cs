﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class StudentGamevalidation
    {
     


        public int? AssignGamestoStudentId { get; set; }
        public string AcademicYear { get; set; }
        public string SessionName { get; set; }
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public string StudentFName { get; set; }
        public string StudentLName { get; set; }
        public string StudentName { get; set; }
        public string GameNameWithLevel { get; set; }
        
        public string StudentType { get; set; }
        public string HouseName { get; set; }
        //public string LevelName { get; set; }
        public string GameName { get; set; }
        //public string prize { get; set; }
        //public int? Marks { get; set; }
        //public string Description { get; set; }
        public int AcademicYearId { get; set; }
        public int? ClassId { get; set; }
        public int? SectionId { get; set; }
        public int? SessionId { get; set; }
        public int? StudentregId { get; set; }
        public int? GameId{ get; set; }
        public bool? Status { get; set; }
  




    }
}