﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class GameLevelReportValidation
    {
       // public int AcademicYearId { get; set; }
       // public int SessionId { get; set; }
       // public int GameId { get; set; }
       // public int LevelId { get; set; }
        public int PointId { get; set; }
        public int StudentRegId { get; set; }
        public string StudentFName { get; set; }
        public string StudentLName { get; set; }
       // public string StudentMName { get; set; }
        public string Class { get; set; }
        public int HouseId { get; set; }
        public string HouseName { get; set; }

        //public int? LevelId { get; set; }
        //public string  GameLevelName { get; set; }
       // public string AcademicYear { get; set; }
       // public string SessionName { get; set; }
       // public string GameName { get; set; }
       // public bool? Status { get; set; }
        public string Result { get; set; }
        public string StudentName { get; set; }
        public int AssignGameStudentId { get; set; }
    }
}