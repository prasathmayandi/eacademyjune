﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace eAcademy.Areas.Sports.Models
{
    public class EditSession
    {
        public int SessionId { get; set; }

        //[Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Models.Resources.ResourceCache),
        //         ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int? acyear { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseEnterSession")]
        public string SessionName { get; set; }

        public bool? Status { get; set; }
    }
}
