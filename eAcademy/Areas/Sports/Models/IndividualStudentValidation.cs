﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{

    public class IndividualStudentReportValidation
    {
       
        public string AcademicYear { get; set; }
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public string SessionName { get; set; }
        public string GameName { get; set; }
        public string LevelName { get; set; }
        public string StudentType { get; set; }
        public string HouseName { get; set; }
        public string GameType { get; set; }
        public string PrizeStatus { get; set; }
        public int? Marks { get; set; }
        public string Description { get; set; }
       // public int? AcademicYearId { get; set; }
       // public int? ClassId { get; set; }
       // public int? SectionId { get; set; }
        public int StudentRegId { get; set; }
    }

    public class IndividualStudentValidation
    {

        public int? IStudentId { get; set; }
        public string AcademicYear { get; set; }
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public string SessionName { get; set; }
        public string GameName { get; set; }
        public string LevelName { get; set; }
        public string StudentType { get; set; }
        public string HouseName { get; set; }
        public string GameType { get; set; }
        public string StudentName { get; set; }
        public string PrizeStatus { get; set; }
        public int? Marks { get; set; }
        public string Description { get; set; }

        public string prize { get; set; }
        public int AcademicYearId { get; set; }
        public int? ClassId { get; set; }
        public int? SectionId { get; set; }
        public int? StudentregId { get; set; }
        public bool? Status { get; set; }
    }
}