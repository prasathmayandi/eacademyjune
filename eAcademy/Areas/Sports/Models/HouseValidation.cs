﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class HouseValidation
    {
        public int HouseId { get; set; }
        public string HouseName { get; set; }
        public bool? Status{ get; set; }
       
    }
}