﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class EditPointGame
    {
        public int? GameTransactionId { get; set; }
        public int? GameId { get; set; }

        public bool? Status { get; set; }
        public int? PointId { get; set; }
    }
}