﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class EditSessionResult
    {


        public int? SessionResultId { get; set; }

        public int? GameId { get; set; }
        [Required(ErrorMessage = "Please Select Session")]
      
        public int? HouseId { get; set; }
        [Required(ErrorMessage = "Please Select Game")]
        public int? HouseId1 { get; set; }
        [Required(ErrorMessage = "Please Select AcademicYear")]
        public int? HouseId2 { get; set; }
        public int? HouseId3 { get; set; }
        public bool? Status { get; set; }

    }
}