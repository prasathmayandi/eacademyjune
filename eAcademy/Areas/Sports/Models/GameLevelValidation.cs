﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class GameLevelValidation
    {


        public int? LevelId { get; set; }
        public string  GameLevelName { get; set; }
        public string AcademicYearId { get; set; }
        public string SessionName { get; set; }
        public string GameName { get; set; }
        public int GameLevelId { get; set; }
        public bool? Status { get; set; }
    }
}