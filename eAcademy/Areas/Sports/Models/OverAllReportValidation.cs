﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class OverAllReportValidation
    {
        public int HouseId { get; set; }
        public int GameId { get; set; }
        public int PointId { get; set; }
        public int Gamecount { get; set; }
        public int GameParticipate { get; set; }
        public int GamePoint { get; set; }
    }
}