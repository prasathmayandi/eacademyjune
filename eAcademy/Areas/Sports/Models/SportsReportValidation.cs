﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class SportsReportValidation
    {

        public int? SessionResultId { get; set; }

        public int? Id { get; set; }

        public int? AcademicYearId { get; set; }

        public int? GameId { get; set; }

        public string GameName { get; set; }
      
        public int? SessionId { get; set; }

        public string SessionName { get; set; }
     
        public int? HouseId { get; set; }

       public string HouseName { get; set; }

       public int? ChampionHouseId { get; set; }      
    }
    public class SportsSessionResultCount
    {
        public int? SessionId { get; set; }

    }
    public class SportsGameResultCount
    {
        public int? GameId { get; set; }

    }

}