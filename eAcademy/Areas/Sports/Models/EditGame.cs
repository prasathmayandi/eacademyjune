﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class EditGame
    {

        public int GameId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseEnterGameName")]
        public string GameName { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "Pleaseselectgametype")]
        public string GameType { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseEnterMainplayers")]
        public int NumberOfMainPlayers { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseEnterSubplayers")]
        public int NumberOfSubPlayers { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "Please_select_Points")]
        public int[] UpdatePointId { get; set; }

        public int Id { get; set; }
        public int?[] IdUpdate { get; set; }
        public bool? Status { get; set; }
        public string PointName { get; set; }
        public int GamePointId { get; set; }
        public int PointId { get; set; }


    }
}