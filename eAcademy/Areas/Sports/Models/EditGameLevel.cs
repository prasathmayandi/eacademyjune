﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class EditGameLevel
    {
        // public int Id { get; set; }
        // public Nullable<int>[] Idarr { get; set; }
        // public int LevelId { get; set; }
        // //  public int?[] LevelIdarr { get; set; }
        // public int GameLevelId { get; set; }
        // public string GameLevelName { get; set; }


        //  [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Models.Resources.ResourceCache),
        //          ErrorMessageResourceName = "Pleaseentergamelevelname")]
        // public string[] GameLevelNamearr { get; set; }

        // [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Models.Resources.ResourceCache),
        //          ErrorMessageResourceName = "PleaseSelectSessionName")]
        // public int SessionId { get; set; }

        //  [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Models.Resources.ResourceCache),
        //          ErrorMessageResourceName = "PleaseSelectGameName")]
        // public int GameId { get; set; }

        //[Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Models.Resources.ResourceCache),
        //          ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        // public int acyear { get; set; }


        // public string AcademicYearName { get; set; }
        // public string SessionName { get; set; }
        // public bool? Status { get; set; }


        public int Id { get; set; }
        public Nullable<int>[] Idarr { get; set; }
        public int LevelId { get; set; }
        //  public int?[] LevelIdarr { get; set; }
        public int GameLevelId { get; set; }
        public string GameLevelName { get; set; }
        public string[] GameLevelNamearr { get; set; }
        [Required(ErrorMessage = "Please Select Session")]
        public int? SessionId { get; set; }
        [Required(ErrorMessage = "Please Select Game")]
        public int? GameId { get; set; }
        [Required(ErrorMessage = "Please Select AcademicYear")]
        public int? acyear { get; set; }
        public string AcademicYearName { get; set; }
        public string SessionName { get; set; }
        public bool? Status { get; set; }
    }
}