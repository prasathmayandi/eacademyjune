﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class GameScheduleValidation
    {
        public int GameScheduleId { get; set; }
        public string AcademicYear { get; set; }
        public string SessionName { get; set; }
        public string GameName { get; set; }
        public string LevelName { get; set; }
        public string GameType { get; set; }
        public string Date { get; set; }
        public TimeSpan Time { get; set; }
        public bool? Status { get; set; }
       // public int HouseId { get; set; }
       // public int PointId { get; set; }
        public string HouseName { get; set; }
       // public string PointName { get; set; }
        public int? AcademicYearId { get; set; }
        public int? SessionId { get; set; }
        public int? LevelConFigId { get; set; }
        public int? GameId { get; set; }
        public string GTime { get; set; }
        public string HouseNameWithPoints { get; set; }
    }
}