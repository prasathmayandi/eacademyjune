﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class GameValidation
    {

        public int GameId { get; set; }
        public string GameName { get; set; }
        

        public string GameType { get; set; }
        public int NumberOfMainPlayers { get; set; }
        public int NumberOfSubPlayers { get; set; }
        public bool? Status { get; set; }
        public string Prizetype { get; set; }
        public string PointName{ get; set; }

        public string Points { get; set; }

    }
}