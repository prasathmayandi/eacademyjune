﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.Models
{
    public class EditHouse
    {

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseEnterHouse")]
        public string HName { get; set; }
        // [Required(ErrorMessage = "Please Enter Status")]
        public bool? status { get; set; }
         //[Required(ErrorMessage = "Please Enter house Id")]
        public int HouseId { get; set; }
    }
}