﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eAcademy.Areas.Sports.ViewModel
{
    public class VM_GameSchedule
    {

         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int AcademicYearId { get; set; }


         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectSessionName")]
        public int SessionId { get; set; }


         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectGameName")]
        public int GameId { get; set; }


         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectLevelName")]
        public int LevelConFigId { get; set; }


         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "Pleaseselectgametype")]
        public string GameType { get; set; }


         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectHouseName")]
        public int[] HouseId { get; set; }

         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectPoint")]
        public int[] PlayerStatusId { get; set; }

         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectScheduleDate")]
        public DateTime ScheduleDate { get; set; }


         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectScheduleTime")]
        public TimeSpan ScheduleTime { get; set; }
    }

    public class VM_Model
    {
         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseEnterHouse")]
        public String HouseName { get; set; }
        public int HouseId { get; set; }
        public int EmployeeId { get; set; }
    }

    public class VM_Session
    {

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseEnterSession")]
        public String ssname { get; set; }

         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int acyear { get; set; }
    }

    public class VM_Game
    {
       [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseEnterGameName")]
        public string gname { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "Pleaseselectgametype")]
        public string GameType { get; set; }

       [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseEnterMainplayers")]
        public int Mplayer { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseEnterSubplayers")]
        public int Splayer { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "Please_select_Points")]
        public int[] GamePrize { get; set; }
    }

    //public class VM_GameLevel
    //{
    //    public int LevelId { get; set; }

    //    public string[] GameLevelName { get; set; }
    //    //[Required(ErrorMessage = "Please Select Session")]
    //    public int SessionId { get; set; }
    //    //[Required(ErrorMessage = "Please Select Game")]
    //    public int GameId { get; set; }
    //    //[Required(ErrorMessage = "Please Select AcademicYear")]
    //    public int acyear { get; set; }

    //}



    public class VM_GameLevel
    {
        public int LevelId { get; set; }

        //[Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
        //         ErrorMessageResourceName = "Pleaseentergamelevelname")]
        public int GameLevelId { get; set; }

         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "Pleaseentergamelevelname")]
        public string[] GameLevelName { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectSessionName")]
        public int SessionId { get; set; }

         [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectGameName")]
        public int GameId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int acyear { get; set; }

    }


    public class VM_Student
    {
        public int AssignGamestoStudentId { get; set; }

        [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectAcademicYear")]
        public int acyear { get; set; }

          [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectSession")]
        public int SessionId { get; set; }

          [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectClass")]
        public int ClassId { get; set; }

          [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectSection")]
        public int SectionId { get; set; }

          [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectStudent")]
        public int StudentId { get; set; }

          [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectHouseName")]
        public int HouseId { get; set; }

          [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "pleaseselectStudentType")]
        public string StudentType { get; set; }

          [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectGameName")]
        public int[] GameId { get; set; }

          [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectLevel")]
        public int[] levelId { get; set; }

          [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "Please_select_Points")]
        public int[] PointId { get; set; }


        public int[] Marks { get; set; }
        public string[] Description { get; set; }
    }


    //public class VM_GSchedule
    //{
    //    public int AcademicYearId { get; set; }
    //    public int SessionId { get; set; }
    //   // public int AcademicYear { get; set; }
    //    public int GameId { get; set; }
    //    public int LevelId { get; set; }

    //    public string GameType { get; set; }
    //   // public string[] playerName { get; set; }
    //    public int[] HouseId { get; set; }
    //    public int[] PlayerStatusId { get; set; }
    //    public DateTime ScheduleDate { get; set; }
    //    public TimeSpan ScheduleTime { get; set; }
    //  //  public int GameScheduleId { get; set; }
    //}

    public class VM_Sessionresult
    {
     [Required(ErrorMessageResourceType = typeof(eAcademy.Areas.Sports.Resources.SportsResources),
                 ErrorMessageResourceName = "PleaseSelectYear")]
    public int AcademicYearId { get; set; }

     public int[] GameId { get; set; }
       
    public int[] SessionId { get; set; }

    public Nullable<int>[] HouseId { get; set; }

    public Nullable<int>[] ChampionHouseId { get; set; }   

    public Nullable<int>[] HouseIdPK { get; set; }
  
    public Nullable<int>[] FinalResultPk { get; set; }

    }


    //public class VM_Sessionresult
    //{

    //    public int[] GameId { get; set; }
    //    public int[] HouseId { get; set; }
    //    public int[] HouseId1 { get; set; }
    //    public int[] HouseId2 { get; set; }
    //    public int[] HouseId3 { get; set; }
    //}
    public class Test
    {
        // [Required(ErrorMessage = "Please Enter House Name")]
        public int AcademicYearId { get; set; }
        // [Required(ErrorMessage = "Please Enter House Name")]
        public int SessionId { get; set; }
        // [Required(ErrorMessage = "Please Enter House Name")]
        public int GameId { get; set; }
        // [Required(ErrorMessage = "Please Enter House Name")]
        public int LevelId { get; set; }
        // [Required(ErrorMessage = "Please Enter House Name")]
        public string GameType { get; set; }
        // [Required(ErrorMessage = "Please Enter House Name")]
        public int[] HouseId { get; set; }
        // [Required(ErrorMessage = "Please Enter House Name")]
        public int[] PlayerStatusId { get; set; }
        // [Required(ErrorMessage = "Please Enter House Name")]
        public DateTime ScheduleDate { get; set; }
        //[Required(ErrorMessage = "Please Enter House Name")]
        public TimeSpan ScheduleTime { get; set; }

    }

    public class Test1
    {

        public int AcademicYearId { get; set; }
        public int SessionId { get; set; }
        public int GameId { get; set; }
        public int LevelId { get; set; }
        public string GameType { get; set; }
        public int[] HouseId { get; set; }
        public int[] PlayerStatusId { get; set; }
        public DateTime ScheduleDate { get; set; }
        public TimeSpan ScheduleTime { get; set; }
    }
}