﻿using eAcademy.DataModel;
using System;
using System.Linq;

namespace eAcademy.Services
{
    public class EnrollmentUserConfirmationServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public void AddUseConfirmation(string offlineid,string email,string request,long? contact,string statusflag,string des)
        {
            EnrollmentUserConfirmation user = new EnrollmentUserConfirmation()
            {
                ApplicationNumber = offlineid,
                PrimaryUserEmail = email,
                RequestSend = request,
                ContactNumber = contact,
                StatusFlag = statusflag,
                Descriptions = des
            };
            dc.EnrollmentUserConfirmations.Add(user);
            dc.SaveChanges();
        }

        public void UpdateUseConfirmation(string offlineid, string email, string request, long? contact, string statusflag, string des)
        {
            var get = dc.EnrollmentUserConfirmations.Where(q => q.ApplicationNumber == offlineid).FirstOrDefault();
            if (get != null)
            {
                get.PrimaryUserEmail = email;
                get.RequestSend = request;
                get.ContactNumber = contact;
                get.StatusFlag = statusflag;
                get.Descriptions = des;
                dc.SaveChanges();
            }
        }

        public EnrollmentUserConfirmation CheckApplication(string offlineid)
        {
            var ans = dc.EnrollmentUserConfirmations.Where(q => q.ApplicationNumber == offlineid).FirstOrDefault();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}