﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using eAcademy.Models;
using System.Data.Objects.SqlClient;
using eAcademy.Helpers;
using System.Collections;
using eAcademy.HelperClass;

namespace eAcademy.Services
{
    public class EventServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public List<EventList> getEventList()
        {
            var ans = (from t1 in dc.Events
                       from t2 in dc.AcademicYears
                       where t1.AcademicYearId==t2.AcademicYearId
                      select new EventList  { EventId = t1.EventId, EventName = t1.EventName, EventDate = t1.EventDate, EventDecribtion = t1.EventDecribtion,Status=t1.Status,Acyear=t2.AcademicYear1, AcyearId=t2.AcademicYearId,
                                              Date = SqlFunctions.DateName("day", t1.EventDate).Trim() + "-" + SqlFunctions.StringConvert((double)t1.EventDate.Month).TrimStart() + "-" + SqlFunctions.DateName("year", t1.EventDate) }).ToList().
                                              Select(q => new EventList { Holiday_Id = QSCrypt.Encrypt(q.EventId.ToString()), Date = q.EventDate.ToString("MMM dd, yyyy"), HolidayName = q.EventName, Description = q.EventDecribtion, Status = q.Status, AcyearId = q.AcyearId, Acyear = q.Acyear }).ToList();
            return ans;
        }

        public List<EventList> getEventListByAcademicYearID(int acid)
        {
            var ans = (from t1 in dc.Events
                       where t1.AcademicYearId==acid
                       select new EventList { EventId = t1.EventId, EventName = t1.EventName, EventDate = t1.EventDate, EventDecribtion = t1.EventDecribtion }).ToList();


            return ans;
        }


        public bool deleteAcademicYear(int acid)
        {
            var ans = (from c in dc.Events
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {               
                return true;
            }
            else
            {
                return false;
            }
        }  

        //public EventList getEventByEventId(int eventId)
        //{
        //    var ans = (from t1 in dc.Events
        //               from t2 in dc.AcademicYears
        //               where t1.AcademicYearId == t2.AcademicYearId && t1.EventId == eventId
        //               select new EventList { EventId = t1.EventId, EventName = t1.EventName,
        //                                      Date = SqlFunctions.DateName("day", t1.EventDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.EventDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.EventDate), 
        //                                                                 EventDecribtion = t1.EventDecribtion, AcademicYear = t2.AcademicYearId ,Acyear=t2.AcademicYear1, Status=t1.Status}).FirstOrDefault();
        //    return ans;
        //}

        public EventList getEventByEventId(int eventId)
        {
            var ans = (from t1 in dc.Events
                       from t2 in dc.AcademicYears
                       where t1.AcademicYearId == t2.AcademicYearId && t1.EventId == eventId
                       select new EventList
                       {
                           EventId = t1.EventId,
                           EventName = t1.EventName,
                           Date = SqlFunctions.DateName("day", t1.EventDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.EventDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.EventDate),
                           EventDecribtion = t1.EventDecribtion,
                           AcademicYear = t2.AcademicYearId,
                           Acyear = t2.AcademicYear1,
                           Status = t1.Status                           
                       }).FirstOrDefault();
            return ans;
        }
        public bool checkCalendar(string eventName, DateTime date, string eventDesc)
        {
            var ans = dc.Events.Where(q => q.EventName.Equals(eventName) && q.EventDate.Equals(date)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkCalendar(int eid, string eventName, DateTime date, string eventDesc, string status)
        {
            int count;
            if (status == "Active")
            {

                var ans = (from t1 in dc.Events
                           where t1.EventId != eid && t1.EventName == eventName && t1.EventDate == date && t1.Status == true
                           select t1).ToList();
                count = ans.Count;
            }

            else
            {
                var ans = (from t1 in dc.Events
                           where t1.EventId != eid && t1.EventName == eventName && t1.EventDate == date && t1.Status == false
                           select t1).ToList();

                count = ans.Count;
            }

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public List<Event> getEvent()
        {
            var ans = dc.Events.Select(query => query).ToList();
            return ans;
        }
        public int addEvent(int acYid, DateTime event_date, string eventtype, string event_desc, int adminid)
        {
            Event eve = new Event()
            {
                AcademicYearId = acYid,
                EventDate = event_date,
                EventName = eventtype,
                EventDecribtion = event_desc,
                Created_date = DateTimeByZone.getCurrentDateTime(),
                Createdby = adminid,
                Updated_date = DateTimeByZone.getCurrentDateTime(),
                Updatedby = adminid,
                Status = true
            };
            dc.Events.Add(eve);
            dc.SaveChanges();
            return eve.EventId;
        }
        //public void addEvent(int acYid, DateTime event_date, string eventtype, string event_desc, int EmpRegId)
        //{
        //    Event eve = new Event()
        //    {
        //        AcademicYearId = acYid,
        //        EventDate = event_date,
        //        EventName = eventtype,
        //        EventDecribtion = event_desc,
        //        Created_date=DateTime.Now,
        //        Updated_date=DateTime.Now,
        //        Createdby=EmpRegId,
        //        Updatedby=EmpRegId,                
        //        Status=true
        //    };
        //    dc.Events.Add(eve);
        //    dc.SaveChanges();
        //}
        public Event getEventdetails(int eventid)
        {
            var ans = (from a in dc.Events where a.EventId == eventid select a).FirstOrDefault();
            return ans;
        }
        DateTime DateWithTime = DateTimeByZone.getCurrentDate();
        public void updateEvent(int eventid, string eventName, DateTime date, string eventDesc, string status, int EmpRegId)
        {
            var update = dc.Events.Where(q => q.EventId.Equals(eventid)).FirstOrDefault();
            if (update != null)
            {
                update.EventName = eventName;
                update.EventDate = date;
                update.EventDecribtion = eventDesc;
                update.Updatedby = EmpRegId;
                update.Updated_date = DateWithTime;

                if (status == "Active")
                {
                    update.Status = true;
                }
                else
                {
                    update.Status = false;
                }
                dc.SaveChanges();
            }
        }

        public void deleteEvent(int eventId)
        {
            var delete = dc.Events.Where(q => q.EventId.Equals(eventId)).First();
            dc.Events.Remove(delete);
        }
        //jegadeesh
        public List<EventList> GetEvents(int? yearid)
        {
            var ans =
                (from a in dc.Events
                 where a.AcademicYearId == yearid
                 select new EventList
                 {
                     AcademicYear = a.AcademicYearId,
                     EventDate=a.EventDate,
                     EventDates = SqlFunctions.DateName("day", a.EventDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.EventDate.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.EventDate),//a.EventDate,
                     EventName = a.EventName,
                     EventDecribtion = a.EventDecribtion
                 }).OrderBy(q => q.EventDates).ToList().Select(a => new EventList {
                 AcademicYear=a.AcademicYear,
                 EventName=a.EventName,
                 EventDecribtion=a.EventDecribtion,
                 EventDates = a.EventDate.ToString("MMM dd, yyyy")
                 }).ToList();
            return ans;
        }
        public IEnumerable GetJsonEvents(int yearid)
        {
            var ans = dc.Events.Select(q => new { dat = q.EventDate, name = q.EventName, id = q.EventId, des = q.EventDecribtion }).ToList();
            return ans;
        }

        public List<HolidaysList> getHolidaysList (int yearId)
        {
            var List = (from a in dc.Events
                        where a.AcademicYearId == yearId &&
                        a.Status == true
                        select new
                        {
                            HolidayDate = a.EventDate,
                            Reason = a.EventName                          
                        }).ToList().Select(a => new HolidaysList
                            {
                                Date = a.HolidayDate.ToString("dd/MM/yyyy"),
                                Holiday = a.Reason
                            }).ToList();
            return List;
        }
        public List<HolidaysList> getAllHolidaysList()
        {
            var List = (from a in dc.Events
                        where
                        a.Status == true
                        select new
                        {
                            HolidayDate = a.EventDate,
                            Reason = a.EventName
                        }).ToList().Select(a => new HolidaysList
                        {
                            Date = a.HolidayDate.ToString("dd/MM/yyyy"),
                            Holiday = a.Reason
                        }).ToList();
            return List;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}