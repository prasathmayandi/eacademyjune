﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class AssignmentReplyFromStudentServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public List<HomeworkReplyList> getAssignmentReplyCount(int AssignmentId, int StudentRegId)
        {
            var list = (from a in db.AssignmentReplyFromStudents
                        where a.AssignmentId == AssignmentId &&
                        a.StudentRegisterId == StudentRegId
                        select new HomeworkReplyList
                        {
                            Description = a.Description
                        }).ToList();
            return list;
        }

        public void addAnswerForAssignment(int AId, string reply, string ReplyFileName, int StudentRegId)
        {
            DateTime date = DateTimeByZone.getCurrentDate();
            AssignmentReplyFromStudent add = new AssignmentReplyFromStudent()
            {
                AssignmentId = AId,
                Description = reply,
                AnswerFileName = ReplyFileName,
                StudentRegisterId = StudentRegId,
                AnswerDate = date,
            };
            db.AssignmentReplyFromStudents.Add(add);
            db.SaveChanges();
        }

        public List<HomeworkReplyStudentList> getStudentList(int AssignmentId, int yearId, int classId, int secId)
        {
            var List = (from a in db.AssignmentReplyFromStudents
                        from b in db.StudentRollNumbers
                        from c in db.Students
                        where a.StudentRegisterId == b.StudentRegisterId &&
                        a.StudentRegisterId == c.StudentRegisterId &&
                        a.AssignmentId == AssignmentId
                        orderby b.RollNumber
                        select new
                        {
                            StudentRegId = a.StudentRegisterId,
                            StudentRollNumber = b.RollNumber,
                            FName = c.FirstName,
                            LName = c.LastName,
                            Answer = a.Description,
                            AssignmentAnswerId = a.Id,
                            Remark = a.Remark
                        }).ToList().Select(a => new HomeworkReplyStudentList
                            {
                                StudentReg_Id = QSCrypt.Encrypt(a.StudentRegId.ToString()),
                                StudentRollNumber = a.StudentRollNumber,
                                Name = a.FName + " " + a.LName,
                                Answer = a.Answer,
                                HomeworkAnswer_Id = QSCrypt.Encrypt(a.AssignmentAnswerId.ToString()),
                                Remark = a.Remark
                            }).ToList();
            return List;
        }
        DateTime date = DateTimeByZone.getCurrentDate();
        public void UpdateRemark(int AssignmentAnswerId, string comment, int FacultyId)
        {
            var row = (from a in db.AssignmentReplyFromStudents where a.Id == AssignmentAnswerId select a).FirstOrDefault();
            row.Remark = comment;
            row.RemarkGivenBy = FacultyId;
            row.RemarkDate = date;
            db.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}