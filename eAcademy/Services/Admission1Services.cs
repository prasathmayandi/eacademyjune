﻿using eAcademy.DataModel;
using System;
using System.Collections;
using System.Linq;

namespace eAcademy.Services
{
    public class Admission1Services:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        
        //public Object AddFeeCollection(DateTime Collectiondate,int acyearid,int classid,decimal totalamount,int ApplicationId)
        //{
        //    FeeCollection add = new FeeCollection()
        //    {
        //        CollectionDate=Collectiondate,
        //        AcademicYearId=acyearid,
        //        ClassId=classid,
        //        FeeAmount=totalamount,
        //        AmountCollected=totalamount,
        //        ApplicationId=ApplicationId
        //    };
        //    dc.FeeCollections.Add(add);
        //    dc.SaveChanges();
        //    var ans = (from a in dc.FeeCollections
        //               where a.AcademicYearId == acyearid && a.ApplicationId == ApplicationId
        //               select a).FirstOrDefault().FeeCollectionId;
        //    return ans;

        //}

        public Object AddFeeCollection(DateTime Collectiondate, int acyearid, int classid, decimal totalamount, int ApplicationId)
        {
            FeeCollection add = new FeeCollection()
            {
                CollectionDate = Collectiondate,
                AcademicYearId = acyearid,
                ClassId = classid,
                FeeAmount = totalamount,
                AmountCollected = totalamount,
                ApplicationId = ApplicationId

            };
            dc.FeeCollections.Add(add);
            dc.SaveChanges();

            var ans = (from a in dc.FeeCollections
                       where a.AcademicYearId == acyearid && a.ApplicationId == ApplicationId
                       select a).FirstOrDefault().FeeCollectionId;
            return ans;

        }

        //public void AddFeeCollectionCategoryFees(int FeecollectionId, int feecategoryid, int PaymenttypeId, decimal amount, decimal servicetax, decimal total, int ayear, int classid, int appid, int PickPoint, int Destination, int AccommodationCategoryId, int AccommodationSubCategoryId, int FoodCategoryId, int FoodSubCategoryId)
        //{
        //    FeeCollectionCategory add = new FeeCollectionCategory()
        //    {

        //        FeeCollectionId = FeecollectionId,
        //        FeeCategoryId = feecategoryid,
        //        PaymentTypeId = PaymenttypeId,
        //        Amount = amount,
        //        ServicesTax = servicetax,
        //        TotalAmount = total,
        //        AcademicYearId = ayear,
        //        ClassId = classid,
        //        ApplicationId = appid,
        //        DestinationId = Destination,
        //        PickPointId = PickPoint,      
        //        AccommodationCategoryId = AccommodationCategoryId,
        //        AccommodationSubCategoryId = AccommodationSubCategoryId,
        //        FoodCategoryId = FoodCategoryId,
        //       FoodSubCategoryId = FoodSubCategoryId     
        //    };
        //    dc.FeeCollectionCategories.Add(add);
        //    dc.SaveChanges();
        //}        
        public bool AddFeeCollectionCategoryFees(int FeecollectionId, int feecategoryid, int PaymenttypeId, decimal amount, decimal servicetax, decimal total, int ayear, int classid, int appid)
        {
            FeeCollectionCategory add = new FeeCollectionCategory()
            {

                FeeCollectionId = FeecollectionId,
                FeeCategoryId = feecategoryid,
                PaymentTypeId = PaymenttypeId,
                Amount = amount,
                ServicesTax = servicetax,
                TotalAmount = total,
                AcademicYearId = ayear,
                ClassId = classid,
                ApplicationId = appid

            };

            dc.FeeCollectionCategories.Add(add);
            dc.SaveChanges();
            return true;
        }
        public IEnumerable getCurrentAcademicYear(DateTime current)
        {
            var ans = (from a in dc.AcademicYears
                       where a.StartDate <= current && a.EndDate >= current
                       select a).ToList();
            return ans;
        }
        public int  getCurrentAcademicYearId(DateTime current)
        {
            var ans = (from a in dc.AcademicYears
                       where a.StartDate <= current && a.EndDate >= current
                       select a).FirstOrDefault();
           if(ans !=null)
           {
               return ans.AcademicYearId;
           }
           else
           {
               return 0;
           }
        }

        public IEnumerable getCurrentAdmissionClass(int yearid)
        {
            var ans = (from a in dc.AsignClassVacancies
                       from b in dc.Classes
                       where a.AcademicYearId==yearid && a.Vacancy!=0 && b.ClassId==a.ClassId
                       select b).ToList();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

      
    }
}