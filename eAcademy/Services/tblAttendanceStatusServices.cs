﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class tblAttendanceStatusServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public IEnumerable<Object> AttendanceStatusList()
        {
            var ans = (from a in dc.tblAttendanceStatus
                       where a.AttendanceStatus == true
                       select new
                       {
                           StatusName = a.AttendanceStatusName,
                           StatusValue = a.AttendanceStatusValue
                       }).ToList();
            return ans;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}