﻿using eAcademy.DataModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class BoardofSchoolServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public IEnumerable Showpreviousmedium()
        {
            var ans = (from a in db.BoardofSchools where a.status==true
                       select new
                       {
                           BoardId =a.BoardId,
                           BoardName =a.BoardName
                       }).ToList();
            return ans;
        }


        public IEnumerable Showpreviousmedium(int boardid)
        {
            var ans = (from a in db.BoardofSchools where a.BoardId == boardid select a.BoardName).FirstOrDefault();
            return ans;
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}