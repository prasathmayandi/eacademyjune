﻿using eAcademy.DataModel;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class ModulesInfoStudentRemarkServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public void addModulesInfoRemark(int FacultyId,int StudentRegisterId,int AcademicYearId,int ClassId,int SectionId,int SubjectId,DateTime DateOfRemarks,string Remark)
        {
            ModuleInfoStudentRemark e = new ModuleInfoStudentRemark()
            {
                EmployeeRegisterId = FacultyId,
                StudentRegisterId = StudentRegisterId,
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                SubjectId = SubjectId,
                DateOfRemarks = DateOfRemarks,
                Remark = Remark
            };
            db.ModuleInfoStudentRemarks.Add(e);
            db.SaveChanges();
        }

        public List<RemarkList> getStudentRemarksList(int yearId, int classId, int secId, int stuId, DateTime fromDate, DateTime toDate, int FacultyId)
        {
            var list = ((from t1 in db.ModuleInfoStudentRemarks
                         from t2 in db.TechEmployees
                         where t1.AcademicYearId == yearId &&
                         t1.ClassId == classId &&
                         t1.SectionId == secId &&
                         t1.StudentRegisterId == stuId &&
                         t1.DateOfRemarks >= fromDate &&
                         t1.DateOfRemarks <= toDate &&
                         t1.EmployeeRegisterId == t2.EmployeeRegisterId
                         select new RemarkList { Remark = t1.Remark, DateOfRemarks = t1.DateOfRemarks, UserName = t2.EmployeeName })

                      .Union(from t2 in db.ExamRemarks
                             where t2.AcademicYearId == yearId &&
                          t2.ClassId == classId &&
                          t2.SectionId == secId &&
                          t2.StudentRegisterId == stuId &&
                          t2.ExamDate >= fromDate &&
                          t2.ExamDate <= toDate
                             select new RemarkList { Remark = t2.ExamRemark1, DateOfRemarks = t2.ExamDate, UserName = t2.ExamRemark1 })).ToList();
            return list;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}