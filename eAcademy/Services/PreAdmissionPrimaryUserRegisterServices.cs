﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace eAcademy.Services
{
    public class PreAdmissionPrimaryUserRegisterServices :IDisposable
    {

        EacademyEntities dc = new EacademyEntities();
        public bool Updateregister(int id, string pwd,string type,string username)
        {
            var ans = new PreAdmissionPrimaryUserRegister();
            var master = new PrimaryUserRegister();
            if(type=="Email")
            {
                ans = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserEmail.Equals(username)).FirstOrDefault();
            }
            else
            {
                long contact = Convert.ToInt64(username.Trim());
                ans = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserContactNo.Value.Equals(contact)).FirstOrDefault();
            }
            
            var Onlineregister = dc.PreAdmissionOnLineRegisters.Where(q => q.Username.Equals(username)).FirstOrDefault();
            if(type=="Email")
            {
                master = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserEmail.Equals(username)).FirstOrDefault();
            }
            else
            {
                long contact = Convert.ToInt64(username.Trim());
                master = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserContactNo.Value.Equals(contact)).FirstOrDefault();
            }
            
            int? onlineid = 0;
            if (ans != null)
            {
                if (ans.OnlineRegisterId != null)
                {
                    var getrow = dc.PreAdmissionOnLineRegisters.Where(q => q.Username == username).FirstOrDefault();
                    if (getrow != null)
                    {
                        getrow.Passwords = pwd;
                        dc.SaveChanges();
                    }
                    onlineid = ans.OnlineRegisterId;
                }
                else if (ans.OnlineRegisterId == null)
                {
                    if (type == "Email")
                    {
                        PreAdmissionOnLineRegister ss = new PreAdmissionOnLineRegister()
                        {
                            Username = ans.PrimaryUserEmail,
                            Passwords = pwd
                        };
                        dc.PreAdmissionOnLineRegisters.Add(ss);
                        dc.SaveChanges();
                        onlineid = ss.OnlineRegisterId;
                    }
                    else
                    {
                        string contact = ans.PrimaryUserContactNo.ToString();
                        PreAdmissionOnLineRegister ss = new PreAdmissionOnLineRegister()
                        {
                            Username = contact,
                            Passwords = pwd
                        };
                        dc.PreAdmissionOnLineRegisters.Add(ss);
                        dc.SaveChanges();
                        onlineid = ss.OnlineRegisterId;
                    }
                   var checkstudent = (from a in dc.PreAdmissionTransactions
                                       from b in dc.PreAdmissionStudentRegisters
                                       where a.PrimaryUserAdmissionId == ans.PrimaryUserAdmissionId && b.StudentAdmissionId == a.StudentAdmissionId && b.Statusflag == "InComplete"
                                       select b).FirstOrDefault();
                    if(checkstudent !=null)
                    {
                        checkstudent.OnlineRegisterId = onlineid;
                        dc.SaveChanges();
                    }
                }
                ans.PrimaryUserPwd = pwd;
                ans.OnlineRegisterId = onlineid;
                dc.SaveChanges();
                return true;
            }
            else if(Onlineregister!=null)
            {
                Onlineregister.Passwords = pwd;
                dc.SaveChanges();
                return true;
            }
            else if(master!=null)
            {
                List<M_ParentSibling> si = new List<M_ParentSibling>();
                int? OnLineRegId = 0; int? PrimaryUserRegId = 0; int? FatherRegId = 0; int? MotherRegId = 0; int? GuardianRegId = 0;
                var old = new AdmissionTransaction();
                if (type == "Email")
                {
                    string uname = master.PrimaryUserEmail;
                    old = (from a in dc.PrimaryUserRegisters
                           from b in dc.AdmissionTransactions
                           where a.PrimaryUserEmail.Equals(uname) && b.PrimaryUserRegisterId == a.PrimaryUserRegisterId
                           select b).OrderByDescending(q => q.StudentRegisterId).FirstOrDefault();
                }
                else
                {
                    long uname =Convert.ToInt64(master.PrimaryUserContactNo);
                    old = (from a in dc.PrimaryUserRegisters
                           from b in dc.AdmissionTransactions
                           where a.PrimaryUserContactNo.Value.Equals(uname) && b.PrimaryUserRegisterId == a.PrimaryUserRegisterId
                           select b).OrderByDescending(q => q.StudentRegisterId).FirstOrDefault();
                }
                if (old.PrimaryUserRegisterId != null)
                {
                    var getrow = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserRegisterId == old.PrimaryUserRegisterId).FirstOrDefault();
                    if (getrow != null)
                    {
                        if (type == "Email")
                        {
                            PreAdmissionOnLineRegister OR = new PreAdmissionOnLineRegister()
                            {
                                Username = getrow.PrimaryUserEmail,
                                Passwords = pwd
                            };
                            dc.PreAdmissionOnLineRegisters.Add(OR);
                            dc.SaveChanges();
                            OnLineRegId = OR.OnlineRegisterId;
                        }
                        else
                        {
                            string contact = getrow.PrimaryUserContactNo.ToString();
                            PreAdmissionOnLineRegister OR = new PreAdmissionOnLineRegister()
                            {
                                Username = contact,
                                Passwords = pwd
                            };
                            dc.PreAdmissionOnLineRegisters.Add(OR);
                            dc.SaveChanges();
                            OnLineRegId = OR.OnlineRegisterId;
                        }

                        if (OnLineRegId != null)
                        {

                            PreAdmissionPrimaryUserRegister PU = new PreAdmissionPrimaryUserRegister()
                            {
                                OnlineRegisterId = OnLineRegId,
                                PrimaryUser = getrow.PrimaryUser,
                                PrimaryUserName = getrow.PrimaryUserName,
                                PrimaryUserEmail = getrow.PrimaryUserEmail,
                                CompanyAddress1 = getrow.CompanyAddress1,
                                CompanyAddress2 = getrow.CompanyAddress2,
                                CompanyCity = getrow.CompanyCity,
                                CompanyState = getrow.CompanyState,
                                CompanyCountry = getrow.CompanyCountry,
                                CompanyPostelcode = getrow.CompanyPostelcode,
                                CompanyContact = getrow.CompanyContact,
                                PrimaryUserPwd = pwd,
                                PrimaryUserContactNo=getrow.PrimaryUserContactNo,
                                EmailRequired=getrow.EmailRequired,
                                SmsRequired=getrow.SmsRequired
                            };
                            dc.PreAdmissionPrimaryUserRegisters.Add(PU);
                            dc.SaveChanges();
                            PrimaryUserRegId = PU.PrimaryUserAdmissionId;

                            if (PrimaryUserRegId != null)
                            {

                                if (old.FatherRegisterId != null)
                                {
                                    var fatherrow = dc.FatherRegisters.Where(q => q.FatherRegisterId == old.FatherRegisterId).FirstOrDefault();

                                    PreAdmissionFatherRegister PFR = new PreAdmissionFatherRegister()
                                    {
                                        FatherFirstName = fatherrow.FatherFirstName,
                                        FatherMiddleName = fatherrow.FatherMiddleName,
                                        FatherLastName = fatherrow.FatherLastName,
                                        FatherDOB = fatherrow.FatherDOB,
                                        FatherQualification = fatherrow.FatherQualification,
                                        FatherOccupation = fatherrow.FatherOccupation,
                                        FatherMobileNo = fatherrow.FatherMobileNo,
                                        FatherEmail = fatherrow.FatherEmail
                                    };
                                    dc.PreAdmissionFatherRegisters.Add(PFR);
                                    dc.SaveChanges();
                                    FatherRegId = PFR.FatherAdmissionId;
                                }
                                if (old.MotherRegisterId != null)
                                {
                                    var motherrow = dc.MotherRegisters.Where(q => q.MotherRegisterId == old.MotherRegisterId).FirstOrDefault();

                                    PreAdmissionMotherRegister PMR = new PreAdmissionMotherRegister()
                                    {
                                        MotherFirstname = motherrow.MotherFirstname,
                                        MotherMiddleName = motherrow.MotherMiddleName,
                                        MotherLastName = motherrow.MotherLastName,
                                        MotherDOB = motherrow.MotherDOB,
                                        MotherQualification = motherrow.MotherQualification,
                                        MotherOccupation = motherrow.MotherOccupation,
                                        MotherMobileNo = motherrow.MotherMobileNo,
                                        MotherEmail = motherrow.MotherEmail
                                    };
                                    dc.PreAdmissionMotherRegisters.Add(PMR);
                                    dc.SaveChanges();
                                    MotherRegId = PMR.MotherAdmissionId;
                                }
                                if (old.GuardianRegisterId != null)
                                {
                                    var guardianrow = dc.GuardianRegisters.Where(q => q.GuardianRegisterId == old.GuardianRegisterId).FirstOrDefault();

                                    PreAdmissionGuardianRegister PGR = new PreAdmissionGuardianRegister()
                                    {
                                        GuardianFirstname = guardianrow.GuardianFirstname,
                                        GuardianMiddleName = guardianrow.GuardianMiddleName,
                                        GuardianLastName = guardianrow.GuardianLastName,
                                        GuardianDOB = guardianrow.GuardianDOB,
                                        GuardianQualification = guardianrow.GuardianQualification,
                                        GuardianOccupation = guardianrow.GuardianOccupation,
                                        GuardianMobileNo = guardianrow.GuardianMobileNo,
                                        GuardianEmail = guardianrow.GuardianEmail
                                    };
                                    dc.PreAdmissionGuardianRegisters.Add(PGR);
                                    dc.SaveChanges();
                                    GuardianRegId = PGR.GuardianAdmissionId;
                                }
                                var oldPrimaryUser = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserRegisterId == old.PrimaryUserRegisterId).FirstOrDefault().PrimaryUser;

                                if (oldPrimaryUser == "Father")
                                {
                                    var Sbling = (from a in dc.AdmissionTransactions
                                                  from f in dc.Students
                                                  where a.PrimaryUserRegisterId == old.PrimaryUserRegisterId && a.FatherRegisterId == old.FatherRegisterId &&
                                                  f.StudentRegisterId == a.StudentRegisterId
                                                  select new M_ParentSibling
                                                  {
                                                      Studentid = f.StudentId,
                                                      StudentRegId = f.StudentRegisterId
                                                  }).Distinct().ToList();
                                    si = Sbling;

                                }
                                else if (oldPrimaryUser == "Mother")
                                {
                                    var Sbling = (from a in dc.AdmissionTransactions
                                                  from f in dc.Students
                                                  where a.PrimaryUserRegisterId == old.PrimaryUserRegisterId && a.MotherRegisterId == old.MotherRegisterId &&
                                                  f.StudentRegisterId == a.StudentRegisterId
                                                  select new M_ParentSibling
                                                  {
                                                      Studentid = f.StudentId,
                                                      StudentRegId = f.StudentRegisterId
                                                  }).Distinct().ToList();
                                    si = Sbling;
                                }
                                else if (oldPrimaryUser == "Guardian")
                                {

                                    var Sbling = (from a in dc.AdmissionTransactions
                                                  from f in dc.Students
                                                  where a.PrimaryUserRegisterId == old.PrimaryUserRegisterId && a.GuardianRegisterId == old.GuardianRegisterId &&
                                                  f.StudentRegisterId == a.StudentRegisterId
                                                  select new M_ParentSibling
                                                  {
                                                      Studentid = f.StudentId,
                                                      StudentRegId = f.StudentRegisterId
                                                  }).Distinct().ToList();
                                    si = Sbling;
                                }

                                foreach (var v in si)
                                {
                                    PreAdmissionParentSibling sibling = new PreAdmissionParentSibling()
                                    {
                                        OnlineRegisterId = OnLineRegId,
                                        SiblingStudentId = v.Studentid,
                                        SblingStudentRegId = v.StudentRegId
                                    };
                                    dc.PreAdmissionParentSiblings.Add(sibling);
                                    dc.SaveChanges();
                                }

                                string primaryuser = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserAdmissionId == PrimaryUserRegId).FirstOrDefault().PrimaryUser;
                                if (primaryuser == "Father")
                                {
                                    if (MotherRegId == 0 && GuardianRegId == 0)
                                    {
                                        PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                        {
                                            PrimaryUserAdmissionId = PrimaryUserRegId,
                                            FatherAdmissionId = FatherRegId,
                                        };
                                        dc.PreAdmissionTransactions.Add(PAT);
                                        dc.SaveChanges();
                                    }
                                    else if (MotherRegId != 0 && GuardianRegId == 0)
                                    {
                                        PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                        {
                                            PrimaryUserAdmissionId = PrimaryUserRegId,
                                            FatherAdmissionId = FatherRegId,
                                            MotherAdmissionId = MotherRegId,
                                        };
                                        dc.PreAdmissionTransactions.Add(PAT);
                                        dc.SaveChanges();
                                    }
                                    else if (MotherRegId == 0 && GuardianRegId != 0)
                                    {
                                        PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                        {
                                            PrimaryUserAdmissionId = PrimaryUserRegId,
                                            FatherAdmissionId = FatherRegId,
                                            GuardianAdmissionId = GuardianRegId,
                                        };
                                        dc.PreAdmissionTransactions.Add(PAT);
                                        dc.SaveChanges();
                                    }
                                    else
                                    {
                                        PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                        {
                                            PrimaryUserAdmissionId = PrimaryUserRegId,
                                            FatherAdmissionId = FatherRegId,
                                            MotherAdmissionId = MotherRegId,
                                            GuardianAdmissionId = GuardianRegId
                                        };
                                        dc.PreAdmissionTransactions.Add(PAT);
                                        dc.SaveChanges();
                                    }
                                }

                                else if (primaryuser == "Mother")
                                {
                                    if (FatherRegId == 0 && GuardianRegId == 0)
                                    {
                                        PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                        {
                                            PrimaryUserAdmissionId = PrimaryUserRegId,
                                            MotherAdmissionId = MotherRegId,
                                        };
                                        dc.PreAdmissionTransactions.Add(PAT);
                                        dc.SaveChanges();
                                    }
                                    else if (FatherRegId != 0 && GuardianRegId == 0)
                                    {
                                        PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                        {
                                            PrimaryUserAdmissionId = PrimaryUserRegId,
                                            FatherAdmissionId = FatherRegId,
                                            MotherAdmissionId = MotherRegId,
                                        };
                                        dc.PreAdmissionTransactions.Add(PAT);
                                        dc.SaveChanges();
                                    }
                                    else if (FatherRegId == 0 && GuardianRegId != 0)
                                    {
                                        PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                        {
                                            PrimaryUserAdmissionId = PrimaryUserRegId,
                                            MotherAdmissionId = MotherRegId,
                                            GuardianAdmissionId = GuardianRegId,
                                        };
                                        dc.PreAdmissionTransactions.Add(PAT);
                                        dc.SaveChanges();
                                    }
                                    else
                                    {
                                        PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                        {
                                            PrimaryUserAdmissionId = PrimaryUserRegId,
                                            FatherAdmissionId = FatherRegId,
                                            MotherAdmissionId = MotherRegId,
                                            GuardianAdmissionId = GuardianRegId
                                        };
                                        dc.PreAdmissionTransactions.Add(PAT);
                                        dc.SaveChanges();
                                    }
                                }

                                else if (primaryuser == "Guardian")
                                {
                                    if (FatherRegId == 0 && MotherRegId == 0)
                                    {
                                        PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                        {
                                            PrimaryUserAdmissionId = PrimaryUserRegId,
                                            GuardianAdmissionId = GuardianRegId,

                                        };
                                        dc.PreAdmissionTransactions.Add(PAT);
                                        dc.SaveChanges();
                                    }
                                    else if (FatherRegId != 0 && MotherRegId == 0)
                                    {
                                        PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                        {
                                            PrimaryUserAdmissionId = PrimaryUserRegId,
                                            FatherAdmissionId = FatherRegId,
                                            GuardianAdmissionId = GuardianRegId,
                                        };
                                        dc.PreAdmissionTransactions.Add(PAT);
                                        dc.SaveChanges();
                                    }
                                    else if (FatherRegId == 0 && MotherRegId != 0)
                                    {
                                        PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                        {
                                            PrimaryUserAdmissionId = PrimaryUserRegId,
                                            MotherAdmissionId = MotherRegId,
                                            GuardianAdmissionId = GuardianRegId,

                                        };
                                        dc.PreAdmissionTransactions.Add(PAT);
                                        dc.SaveChanges();
                                    }
                                    else
                                    {
                                        PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                        {
                                            PrimaryUserAdmissionId = PrimaryUserRegId,
                                            FatherAdmissionId = FatherRegId,
                                            MotherAdmissionId = MotherRegId,
                                            GuardianAdmissionId = GuardianRegId
                                        };
                                        dc.PreAdmissionTransactions.Add(PAT);
                                        dc.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public int FindMobilePrimaryUser(string mobile)
        {
            long? mno = Convert.ToInt64(mobile.Trim());
            var ans = (from a in dc.PreAdmissionPrimaryUserRegisters
                   from b in dc.PreAdmissionTransactions
                       where a.PrimaryUserContactNo == mno && b.PrimaryUserAdmissionId == a.PrimaryUserAdmissionId
                       select a).ToList();
            return ans.Count;
        }
        public int FindEmailPrimaryUser(string email)
        {
            var ans = (from a in dc.PreAdmissionPrimaryUserRegisters
                       from b in dc.PreAdmissionTransactions
                       where a.PrimaryUserEmail == email && b.PrimaryUserAdmissionId == a.PrimaryUserAdmissionId
                       select a).ToList();
            return ans.Count;
        }

        public int AddPrimaryUser(int onlineregid, string primaryuser, string primaryuseremail, long? PrimaryuserContact, string companyAdd1, string companyAdd2, string companycity, string companystate, string companycountry, long? companypostel, long? companycontact,
             string companyname, string worksameschool,int? employeedesignation,bool? EmailRequired,bool? SmsRequired,string employeeid)
        {
            PreAdmissionOnlineRegisterServices Ponline = new PreAdmissionOnlineRegisterServices();
            string pwd = Ponline.getRow(onlineregid).Passwords;
            long contact = Convert.ToInt64(PrimaryuserContact);
            if (employeedesignation != 0)
            {
                PreAdmissionPrimaryUserRegister user = new PreAdmissionPrimaryUserRegister()
                {
                    OnlineRegisterId = onlineregid,
                    PrimaryUser = primaryuser,
                    PrimaryUserEmail = primaryuseremail,
                    PrimaryUserContactNo=contact,
                    CompanyAddress1 = companyAdd1,
                    CompanyAddress2 = companyAdd2,
                    CompanyCity = companycity,
                    CompanyState = companystate,
                    CompanyCountry = companycountry,
                    CompanyPostelcode = companypostel,
                    CompanyContact = companycontact,
                    PrimaryUserPwd = pwd,
                    UserCompanyname = companyname,
                    WorkSameSchool = worksameschool,
                    EmployeeDesignationId = employeedesignation,
                    EmailRequired=EmailRequired,
                    SmsRequired=SmsRequired,
                    EmployeeId = employeeid

                };
                dc.PreAdmissionPrimaryUserRegisters.Add(user);
                dc.SaveChanges();
                return user.PrimaryUserAdmissionId;
            }
            else
            {
                PreAdmissionPrimaryUserRegister user = new PreAdmissionPrimaryUserRegister()
                {
                    OnlineRegisterId = onlineregid,
                    PrimaryUser = primaryuser,
                    PrimaryUserEmail = primaryuseremail,
                    PrimaryUserContactNo = contact,
                    CompanyAddress1 = companyAdd1,
                    CompanyAddress2 = companyAdd2,
                    CompanyCity = companycity,
                    CompanyState = companystate,
                    CompanyCountry = companycountry,
                    CompanyPostelcode = companypostel,
                    CompanyContact = companycontact,
                    PrimaryUserPwd = pwd,
                    UserCompanyname = companyname,
                    EmailRequired = EmailRequired,
                    SmsRequired = SmsRequired,
                    WorkSameSchool = worksameschool                  

                };
                dc.PreAdmissionPrimaryUserRegisters.Add(user);
                dc.SaveChanges();
                return user.PrimaryUserAdmissionId;
            }
        }
        public int UpdatePrimaryUser(int onlineregid, string primaryuser, string primaryuseremail, long? primaryUserContact, string companyAdd1, string companyAdd2, string companycity, string companystate, string companycountry, long? companypostel, long? companycontact,
           string companyname, string worksameschool, int? employeedesignation, bool? EmailRequired, bool? SmsRequired, string employeeid)
        {
            long mno = Convert.ToInt64(primaryUserContact);
            var GetRow = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.OnlineRegisterId == onlineregid).FirstOrDefault();
            if (GetRow != null)
            {
                GetRow.PrimaryUser = primaryuser;
                GetRow.PrimaryUserEmail = primaryuseremail;
                GetRow.PrimaryUserContactNo = mno;
                GetRow.CompanyAddress1 = companyAdd1;
                GetRow.CompanyAddress2 = companyAdd2;
                GetRow.CompanyCity = companycity;
                GetRow.CompanyState = companystate;
                GetRow.CompanyCountry = companycountry;
                GetRow.CompanyPostelcode = companypostel;
                GetRow.CompanyContact = companycontact;
                GetRow.UserCompanyname = companyname;
                GetRow.WorkSameSchool = worksameschool;
                GetRow.EmailRequired = EmailRequired;
                GetRow.SmsRequired = SmsRequired;
                if (employeedesignation != 0)
                {
                    GetRow.EmployeeDesignationId = employeedesignation;
                    GetRow.EmployeeId = employeeid;
                }
                dc.SaveChanges();
            }
            return GetRow.PrimaryUserAdmissionId;
        }

        public int UpdatePrimaryUserStepForm(int onlineregid, string primaryuser, string primaryuseremail, string companyAdd1, string companyAdd2, string companycity, string companystate, string companycountry, long? companypostel, long? companycontact,
            string companyname)
        {
            var GetRow = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.OnlineRegisterId == onlineregid).FirstOrDefault();
              if(GetRow!= null)
              {
                  GetRow.PrimaryUser = primaryuser;
                  GetRow.PrimaryUserEmail = primaryuseremail;
                  GetRow.CompanyAddress1 = companyAdd1;
                  GetRow.CompanyAddress2 = companyAdd2;
                  GetRow.CompanyCity = companycity;
                  GetRow.CompanyState = companystate;
                  GetRow.CompanyCountry = companycountry;
                  GetRow.CompanyPostelcode = companypostel;
                  GetRow.CompanyContact = companycontact;
                  GetRow.UserCompanyname = companyname;                  
                  dc.SaveChanges();
              }
              return GetRow.PrimaryUserAdmissionId;
        }

        public void UpdatePrimaryUsername(int pid,string firstname,string lastname,string Primaryuseremail,long? contactno,long? income)
          {
              var p = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserAdmissionId == pid).FirstOrDefault();
            if(p!=null)
            {
                p.PrimaryUserName = firstname + " " + lastname;
                p.PrimaryUserContactNo = contactno;
                p.PrimaryUserEmail = Primaryuseremail;
                p.TotalIncome = income;
                dc.SaveChanges();
            }
          }

        public PreAdmissionPrimaryUserRegister GetPrimaryuserdetails(string email)
        {
            var ans = (from a in dc.PreAdmissionPrimaryUserRegisters

                       where a.PrimaryUserEmail.Equals(email)
                       select a).FirstOrDefault();
            return ans;
        }
        public PreAdmissionPrimaryUserRegister GetMobilePrimaryuserdetails(long Contact)
        {
            var ans = (from a in dc.PreAdmissionPrimaryUserRegisters

                       where a.PrimaryUserContactNo.Value.Equals(Contact)
                       select a).FirstOrDefault();
            return ans;
        }
       
       public bool CheckPrimaryUserEmailExistusingMobile(long contact,string Email)
        {
            PrimaryUserRegisterServices purs = new PrimaryUserRegisterServices();
            var PreadmissionPrimaryuser = GetMobilePrimaryuserdetails(contact);
            var materprimaryuser = purs.GetMobilePrimaryuserdetails(contact);
            string Temp = "";
            string master = "";
            if(PreadmissionPrimaryuser != null)
           {
               var check = (from a in dc.PreAdmissionPrimaryUserRegisters
                            where a.PrimaryUserAdmissionId != PreadmissionPrimaryuser.PrimaryUserAdmissionId && a.PrimaryUserEmail == Email
                            select a).ToList();
                if(check.Count>0)
                {
                    Temp = "Exist"; // already Email Exist
                }
                else
                {
                    Temp = "Not Exist"; // already Email not Exist
                }
           }
            else 
            {
                var CheckEmailPreAdmission = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserEmail == Email).ToList();
                if(CheckEmailPreAdmission.Count>0)
                {
                    Temp = "Exist"; // already Email Exist
                }
                else
                {
                    Temp = "Not Exist"; // already Email not Exist
                }
            }
            if(materprimaryuser != null)
            {
                var check = (from a in dc.PrimaryUserRegisters
                             where a.PrimaryUserRegisterId != materprimaryuser.PrimaryUserRegisterId && a.PrimaryUserEmail == Email
                             select a).ToList();
                if (check.Count > 0)
                {
                    master = "Exist"; // already Email Exist
                }
                else
                {
                    master = "Not Exist"; // already Email not Exist
                }
            }
            else
            {
                var CheckEmailmaster = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserEmail == Email).ToList();
                if (CheckEmailmaster.Count > 0)
                {
                    master = "Exist"; // already Email Exist
                }
                else
                {
                    master = "Not Exist"; // already Email not Exist
                }
            }
           if(Temp=="Exist" || master=="Exist")
           {
               return true;
           }
           else
           {
               return false;
           }

        }
       public bool CheckPrimaryUserMobileExistusingMobile(long contact, string Email)
       {
           PrimaryUserRegisterServices purs = new PrimaryUserRegisterServices();
           var PreadmissionPrimaryuser = GetPrimaryuserdetails(Email);
           var materprimaryuser = purs.GetPrimaryUserdetails(Email);
           string Temp = "";
           string master = "";
           if (PreadmissionPrimaryuser != null)
           {
               var check = (from a in dc.PreAdmissionPrimaryUserRegisters
                            where a.PrimaryUserAdmissionId != PreadmissionPrimaryuser.PrimaryUserAdmissionId && a.PrimaryUserContactNo == contact
                            select a).ToList();
               if (check.Count > 0)
               {
                   Temp = "Exist"; // already Email Exist
               }
               else
               {
                   Temp = "Not Exist"; // already Email not Exist
               }
           }
           else
           {
               var CheckEmailPreAdmission = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserContactNo == contact).ToList();
               if (CheckEmailPreAdmission.Count > 0)
               {
                   Temp = "Exist"; // already Email Exist
               }
               else
               {
                   Temp = "Not Exist"; // already Email not Exist
               }
           }
           if (materprimaryuser != null)
           {
               var check = (from a in dc.PrimaryUserRegisters
                            where a.PrimaryUserRegisterId != materprimaryuser.PrimaryUserRegisterId && a.PrimaryUserContactNo == contact
                            select a).ToList();
               if (check.Count > 0)
               {
                   master = "Exist"; // already Email Exist
               }
               else
               {
                   master = "Not Exist"; // already Email not Exist
               }
           }
           else
           {
               var CheckEmailmaster = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserContactNo == contact).ToList();
               if (CheckEmailmaster.Count > 0)
               {
                   master = "Exist"; // already Email Exist
               }
               else
               {
                   master = "Not Exist"; // already Email not Exist
               }
           }
           if (Temp == "Exist" || master == "Exist")
           {
               return true;
           }
           else
           {
               return false;
           }

       }

        //steps form

           public int StepAddPrimaryUser(int onlineregid, string primaryuser, string primaryuseremail,long? income,int? employeeDesignid,string EmployeeId,string worksameschool,long? PrimaryuserContact,bool? Email,bool? sms)
        {
                PreAdmissionOnlineRegisterServices Ponline = new PreAdmissionOnlineRegisterServices();
            string pwd = Ponline.getRow(onlineregid).Passwords;
            long contact = Convert.ToInt64(PrimaryuserContact);
            if (employeeDesignid != 0)
            {
                PreAdmissionPrimaryUserRegister user = new PreAdmissionPrimaryUserRegister()
                {
                    OnlineRegisterId = onlineregid,
                    PrimaryUser = primaryuser,
                    PrimaryUserEmail = primaryuseremail,
                    PrimaryUserContactNo = contact,
                    EmailRequired=Email,
                    SmsRequired=sms,
                    PrimaryUserPwd = pwd,
                    TotalIncome = income,
                    EmployeeDesignationId = employeeDesignid,
                    EmployeeId = EmployeeId,
                    WorkSameSchool = worksameschool
                };
                dc.PreAdmissionPrimaryUserRegisters.Add(user);
                dc.SaveChanges();
                return user.PrimaryUserAdmissionId;
            }
            else
            {
                PreAdmissionPrimaryUserRegister user = new PreAdmissionPrimaryUserRegister()
                {
                    OnlineRegisterId = onlineregid,
                    PrimaryUser = primaryuser,
                    PrimaryUserEmail = primaryuseremail,
                    PrimaryUserContactNo = contact,
                    EmailRequired = Email,
                    SmsRequired = sms,
                    PrimaryUserPwd = pwd,
                    TotalIncome = income,                   
                    WorkSameSchool = worksameschool
                };
                dc.PreAdmissionPrimaryUserRegisters.Add(user);
                dc.SaveChanges();
                return user.PrimaryUserAdmissionId;
            }
        }

           public int StepUpdatePrimaryUser(int onlineregid, string primaryuser, string primaryuseremail, long? income, int? employeeDesignid, string EmployeeId, string worksameschool,long? Primaryusercontact,bool? Email,bool? Sms)
           {
               long contact = Convert.ToInt64(Primaryusercontact);
                var GetRow = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.OnlineRegisterId == onlineregid).FirstOrDefault();
                if (GetRow != null)
                {
                    GetRow.PrimaryUser = primaryuser;
                    GetRow.PrimaryUserEmail = primaryuseremail;
                    GetRow.TotalIncome = income;
                    GetRow.WorkSameSchool = worksameschool;
                    GetRow.EmailRequired = Email;
                    GetRow.SmsRequired = Sms;
                    GetRow.PrimaryUserContactNo = contact;
                    if (employeeDesignid != 0)
                    {
                        GetRow.EmployeeDesignationId = employeeDesignid;
                        GetRow.EmployeeId = EmployeeId;
                    }
                    dc.SaveChanges();
                }
                return GetRow.PrimaryUserAdmissionId;
           }

           
        public PreAdmissionPrimaryUserRegister FindPrimary(int onlineid)
           {
               var ans = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.OnlineRegisterId == onlineid).FirstOrDefault();
               return ans;
           }

        //offline

        public List<M_PrimaryUser> GetExitUsers(string email)
        {
            string MatchEmailPattern =
                       @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
            string uname = "";
            if (Regex.IsMatch(email, MatchEmailPattern))
            {

                var ans = (from a in dc.PreAdmissionPrimaryUserRegisters

                           where a.PrimaryUserEmail.Equals(email)
                           select new M_PrimaryUser
                           {
                               PrimaryUser = a.PrimaryUser,
                               PrimaryUserName = a.PrimaryUserName,
                               PrimaryUserEmail = a.PrimaryUserEmail,
                               PrimaryUserContactNo = a.PrimaryUserContactNo,
                               PrimaryUserAdmissionId=a.PrimaryUserAdmissionId
                           }).ToList();
                return ans;
            }
            else
            {
                long contact = Convert.ToInt64(email);
                var ans = (from a in dc.PreAdmissionPrimaryUserRegisters

                           where a.PrimaryUserContactNo.Value.Equals(contact)
                           select new M_PrimaryUser
                           {
                               PrimaryUser = a.PrimaryUser,
                               PrimaryUserName = a.PrimaryUserName,
                               PrimaryUserEmail = a.PrimaryUserEmail,
                               PrimaryUserContactNo = a.PrimaryUserContactNo,
                               PrimaryUserAdmissionId = a.PrimaryUserAdmissionId
                           }).ToList();
                return ans;
            }
        }
        public List<M_PrimaryUser> GetMasterExitUsers(string email)
        {

                        string MatchEmailPattern =
                       @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
            string uname = "";
            if (Regex.IsMatch(email, MatchEmailPattern))
            {

                var res = (from a in dc.PrimaryUserRegisters
                           where a.PrimaryUserEmail.Equals(email)
                           select new M_PrimaryUser
                           {
                               PrimaryUser = a.PrimaryUser,
                               PrimaryUserName = a.PrimaryUserName,
                               PrimaryUserEmail = a.PrimaryUserEmail,
                               PrimaryUserContactNo = a.PrimaryUserContactNo
                           }).ToList();
                return res;
            }
            else
            {
                long contact = Convert.ToInt64(email);
                var res = (from a in dc.PrimaryUserRegisters
                           where a.PrimaryUserContactNo.Value.Equals(contact)
                           select new M_PrimaryUser
                           {
                               PrimaryUser = a.PrimaryUser,
                               PrimaryUserName = a.PrimaryUserName,
                               PrimaryUserEmail = a.PrimaryUserEmail,
                               PrimaryUserContactNo = a.PrimaryUserContactNo
                           }).ToList();
                return res;
            }
        }
        public PreAdmissionStudentRegister CheckEmail (string email)
        {
            string MatchEmailPattern =
                       @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
            string uname = "";
            if (Regex.IsMatch(email, MatchEmailPattern))
            {
                var ans = (from a in dc.PreAdmissionPrimaryUserRegisters
                           from b in dc.PreAdmissionTransactions
                           from c in dc.PreAdmissionStudentRegisters
                           where a.PrimaryUserEmail == email && b.PrimaryUserAdmissionId == a.PrimaryUserAdmissionId && c.StudentAdmissionId == b.StudentAdmissionId
                           select c).OrderByDescending(q => q.StudentAdmissionId).FirstOrDefault();
                return ans;
            }
            else
            {
                long contact = Convert.ToInt64(email);
                var ans = (from a in dc.PreAdmissionPrimaryUserRegisters
                           from b in dc.PreAdmissionTransactions
                           from c in dc.PreAdmissionStudentRegisters
                           where a.PrimaryUserContactNo == contact && b.PrimaryUserAdmissionId == a.PrimaryUserAdmissionId && c.StudentAdmissionId == b.StudentAdmissionId
                           select c).OrderByDescending(q => q.StudentAdmissionId).FirstOrDefault();
                return ans;
            }
        }

        public int AddPrimaryUserOffline(string primaryuser, string primaryuseremail, long? primaryuserContact, string companyAdd1, string companyAdd2, string companycity, string companystate, string companycountry, long? companypostel, long? companycontact,
           string companyname, string worksameschool, int? employeeDesignationId, string employeeId, bool? Email, bool? Sms)
        {
            long contact = Convert.ToInt64(primaryuserContact);
            if (employeeDesignationId != 0)
            {
                PreAdmissionPrimaryUserRegister user = new PreAdmissionPrimaryUserRegister()
                {
                    PrimaryUser = primaryuser,
                    PrimaryUserEmail = primaryuseremail,
                    CompanyAddress1 = companyAdd1,
                    CompanyAddress2 = companyAdd2,
                    CompanyCity = companycity,
                    CompanyState = companystate,
                    CompanyCountry = companycountry,
                    CompanyPostelcode = companypostel,
                    CompanyContact = companycontact,
                    UserCompanyname = companyname,
                    WorkSameSchool = worksameschool,
                    EmployeeDesignationId=employeeDesignationId,
                    EmployeeId  =employeeId,
                    PrimaryUserContactNo=contact,
                    EmailRequired=Email,
                    SmsRequired=Sms,

                };
                dc.PreAdmissionPrimaryUserRegisters.Add(user);
                dc.SaveChanges();
                return user.PrimaryUserAdmissionId;
            }
            else
            {
                PreAdmissionPrimaryUserRegister user = new PreAdmissionPrimaryUserRegister()
                {
                    PrimaryUser = primaryuser,
                    PrimaryUserEmail = primaryuseremail,
                    CompanyAddress1 = companyAdd1,
                    CompanyAddress2 = companyAdd2,
                    CompanyCity = companycity,
                    CompanyState = companystate,
                    CompanyCountry = companycountry,
                    CompanyPostelcode = companypostel,
                    CompanyContact = companycontact,
                    UserCompanyname = companyname,
                    WorkSameSchool = worksameschool,
                    PrimaryUserContactNo = contact,
                    EmailRequired = Email,
                    SmsRequired = Sms,

                };
                dc.PreAdmissionPrimaryUserRegisters.Add(user);
                dc.SaveChanges();
                return user.PrimaryUserAdmissionId;
            }
        }
        public int UpdatePrimaryUserOffline(int PrimaryuserAdmissionId, string primaryuser, string primaryuseremail, long? primaryuserContact, string companyAdd1, string companyAdd2, string companycity, string companystate, string companycountry, long? companypostel, long? companycontact,
           string companyname, string worksameschool, int? employeeDesignationId, string employeeId, bool? Email, bool? Sms)
        {
            long contact = Convert.ToInt64(primaryuserContact);
            var GetRow = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserAdmissionId == PrimaryuserAdmissionId).FirstOrDefault();
            if (employeeDesignationId != 0)
            {   
                if (GetRow != null)
                {
                    GetRow.PrimaryUser = primaryuser;
                    GetRow.PrimaryUserEmail = primaryuseremail;
                    GetRow.CompanyAddress1 = companyAdd1;
                    GetRow.CompanyAddress2 = companyAdd2;
                    GetRow.CompanyCity = companycity;
                    GetRow.CompanyState = companystate;
                    GetRow.CompanyCountry = companycountry;
                    GetRow.CompanyPostelcode = companypostel;
                    GetRow.CompanyContact = companycontact;
                    GetRow.UserCompanyname = companyname;
                    GetRow.WorkSameSchool = worksameschool;
                    GetRow.EmployeeDesignationId = employeeDesignationId;
                    GetRow.EmployeeId = employeeId;
                    GetRow.PrimaryUserContactNo = contact;
                    GetRow.EmailRequired = Email;
                    GetRow.SmsRequired = Sms;
                    dc.SaveChanges();
                }
                return GetRow.PrimaryUserAdmissionId;
            }
            else
            {               
                if (GetRow != null)
                {
                    GetRow.PrimaryUser = primaryuser;
                    GetRow.PrimaryUserEmail = primaryuseremail;
                    GetRow.CompanyAddress1 = companyAdd1;
                    GetRow.CompanyAddress2 = companyAdd2;
                    GetRow.CompanyCity = companycity;
                    GetRow.CompanyState = companystate;
                    GetRow.CompanyCountry = companycountry;
                    GetRow.CompanyPostelcode = companypostel;
                    GetRow.CompanyContact = companycontact;
                    GetRow.UserCompanyname = companyname;
                    GetRow.WorkSameSchool = worksameschool;
                    GetRow.PrimaryUserContactNo = contact;
                    GetRow.EmailRequired = Email;
                    GetRow.SmsRequired = Sms;
                    dc.SaveChanges();
                }
                return GetRow.PrimaryUserAdmissionId;
            }
        }
        public int UpdatePrimaryUserOfflinestep(string primaryuser, string primaryuseremail, long? PrimaryuserContact, string companyAdd1, string companyAdd2, string companycity, string companystate, string companycountry, long? companypostel, long? companycontact,
           string companyname)
        {
            long contact = Convert.ToInt64(PrimaryuserContact);
            var GetRow=new PreAdmissionPrimaryUserRegister();
            if (primaryuseremail != "" && primaryuseremail != null)
            {
                GetRow = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserEmail == primaryuseremail).FirstOrDefault();
            }
            else
            {
                GetRow = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserContactNo == contact).FirstOrDefault();
            }           
            if (GetRow != null)
            {
                GetRow.PrimaryUser = primaryuser;
                GetRow.PrimaryUserEmail = primaryuseremail;
                GetRow.CompanyAddress1 = companyAdd1;
                GetRow.CompanyAddress2 = companyAdd2;
                GetRow.CompanyCity = companycity;
                GetRow.CompanyState = companystate;
                GetRow.CompanyCountry = companycountry;
                GetRow.CompanyPostelcode = companypostel;
                GetRow.CompanyContact = companycontact;
                GetRow.UserCompanyname = companyname;
                GetRow.PrimaryUserContactNo = contact;
                dc.SaveChanges();
            }
            return GetRow.PrimaryUserAdmissionId;
        }

        public int AddPrimaryUserOffline3(string primaryuser, string primaryuseremail, long? income, int? employeeDesignid, string EmployeeId, string worksameschool, long? primaryuserContact, bool? Email, bool? Sms)
        {
            long contact = Convert.ToInt64(primaryuserContact);
            if (employeeDesignid != 0)
            {
                PreAdmissionPrimaryUserRegister user = new PreAdmissionPrimaryUserRegister()
                {
                    PrimaryUser = primaryuser,
                    PrimaryUserEmail = primaryuseremail,
                    TotalIncome=income,
                    WorkSameSchool=worksameschool,
                    EmployeeDesignationId=employeeDesignid,
                    EmployeeId=EmployeeId,
                    PrimaryUserContactNo=contact,
                    EmailRequired=Email,
                    SmsRequired=Sms
                };
                dc.PreAdmissionPrimaryUserRegisters.Add(user);
                dc.SaveChanges();
                return user.PrimaryUserAdmissionId;
            }
            else
            {
                PreAdmissionPrimaryUserRegister user = new PreAdmissionPrimaryUserRegister()
                {
                    PrimaryUser = primaryuser,
                    PrimaryUserEmail = primaryuseremail,
                    TotalIncome = income,
                    WorkSameSchool = worksameschool,
                    PrimaryUserContactNo=contact,
                    EmailRequired=Email,
                    SmsRequired=Sms
                };
                dc.PreAdmissionPrimaryUserRegisters.Add(user);
                dc.SaveChanges();
                return user.PrimaryUserAdmissionId;
            }
        }
        public int UpdatePrimaryUserOffline3(int PrimaryUserAdmissionId,string primaryuser, string primaryuseremail, long? income, int? employeeDesignid, string EmployeeId, string worksameschool, long? primaryuserContact, bool? Email, bool? Sms)
        {
            long contact = Convert.ToInt64(primaryuserContact);
            var GetRow = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserAdmissionId == PrimaryUserAdmissionId).FirstOrDefault();
            if(employeeDesignid != 0)
            {  
            if (GetRow != null)
            {
                GetRow.PrimaryUser = primaryuser;
                GetRow.PrimaryUserEmail = primaryuseremail;
                GetRow.TotalIncome = income;
                GetRow.WorkSameSchool = worksameschool;
                GetRow.EmployeeDesignationId = employeeDesignid;
                GetRow.EmployeeId = EmployeeId;
                GetRow.PrimaryUserContactNo = contact;
                GetRow.SmsRequired = Sms;
                GetRow.EmailRequired = Email;
                dc.SaveChanges();
            }
            return GetRow.PrimaryUserAdmissionId;
            }
            else
            {              
                if (GetRow != null)
                {
                    GetRow.PrimaryUser = primaryuser;
                    GetRow.PrimaryUserEmail = primaryuseremail;
                    GetRow.TotalIncome = income;
                    GetRow.PrimaryUserContactNo = contact;
                    GetRow.SmsRequired = Sms;
                    GetRow.EmailRequired = Email;
                    dc.SaveChanges();
                }
                return GetRow.PrimaryUserAdmissionId;
            }
        }
       
        public PreAdmissionPrimaryUserRegister getprimaryuserusingstudentadmissionid(int studentadmissionid)
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters
                       from b in dc.PreAdmissionTransactions
                       from c in dc.PreAdmissionPrimaryUserRegisters
                       where a.StudentAdmissionId == studentadmissionid && b.StudentAdmissionId == a.StudentAdmissionId && c.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId
                       select c).FirstOrDefault();
            return ans;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}