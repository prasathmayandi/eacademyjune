﻿using eAcademy.DataModel;
using System;

namespace eAcademy.Services
{
    public class Admission4Services :IDisposable
    {

        EacademyEntities dc = new EacademyEntities();

        public void AddParentDetails(int regid, int stuid, string primaryuser, string faName, string faOccu, long? faContact, string faEmail, string moName, string MoOccu, long? MoContact, string MoEmail, string CompanyName, string Offaddr1, string offaddr2,
            string offcity, string offstate, int? offcountry, long? offpostel, long? offcontact, string guname, string gurelation, string gugender, string guQual, long? guMobile, string guemail, string guOccu, long? guAnnual, string username, string userpwd,
            string contactpersonName, long? ContactPersoncontact, string contactPersonRelation, string guardianReq,string fatherlastname,DateTime? fatherdob,string fatherquali,string motherlastname,DateTime? motherdob,string motherquali,long? totalincome)
        {
            ParentRegister add = new ParentRegister()
            {
                StudentAdmissionId = stuid,
                OnlineRegisterId = regid,
                PrimaryUser = primaryuser,
                FatherFirstName = faName,
                FatherOccupation = faOccu,
                FatherMobileNo = faContact,
                FatherEmail = faEmail,
                MotherFirstname = moName,
                MotherOccupation = MoOccu,
                MotherMobileNo = MoContact,
                MotherEmail = MoEmail,
                UserCompanyname = CompanyName,
                CompanyAddress1 = Offaddr1,
                CompanyAddress2 = offaddr2,
                CompanyCity = offcity,
                CompanyState = offstate,
                CompanyCountry = offcountry,
                CompanyPostelcode = offpostel,
                CompanyContact=offcontact,
                GuardianFirstname = guname,
                GuRelationshipToChild = gurelation,
                GuardianGender = gugender,
                GuardianQualification = guQual,
                GuardianMobileNo = guMobile,
                GuardianEmail = guemail,
                GuardianOccupation = guOccu,
                GuardianIncome = guAnnual,
                UserName = username,
                UserPassword = userpwd,
                EmergencyContactPersonName = contactpersonName,
                EmergencyContactNumber = ContactPersoncontact,
                ContactPersonRelationship = contactPersonRelation,
                GuardianRequried = guardianReq,
                FatherLastName=fatherlastname,
                FatherDOB=fatherdob,
                FatherQualification=fatherquali,
                MotherLastName=motherlastname,
                MotherDOB=motherdob,
                MotherQualification=motherquali,
                TotalIncome=totalincome
            };
            dc.ParentRegisters.Add(add);
            dc.SaveChanges();
        }
      
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}