﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.ViewModel;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using eAcademy.HelperClass;

namespace eAcademy.Services
{
    public class AnnouncementServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
       
        public List<AnnouncementList> getAnnouncementList(DateTime currentDate)
        {
            var announcementList = (from a in dc.Announcements
                                    where a.AnnouncementDate.Year == currentDate.Year &&
                                    a.AnnouncementDate.Month == currentDate.Month &&
                                    a.Status == true
                                    select new AnnouncementList
                                    {
                                        id = a.AnnouncementId,
                                        Announcement = a.AnnouncementName,
                                        date = a.AnnouncementDate
                                    }).ToList();

            return announcementList;
        }

        public Announcement getAnnoucementDesc(int id)
        {
            var desc = (from a in dc.Announcements where a.AnnouncementId == id select a).FirstOrDefault();
            return desc;
        }

        public List<AnnouncementsList> getAnnouncementList()
        {
            var ans = (from t1 in dc.Announcements
                       select new AnnouncementsList { AnnouncementId = t1.AnnouncementId, AnnouncementName = t1.AnnouncementName, AnnouncementDate = t1.AnnouncementDate, AnnouncementDescription = t1.AnnouncementDescription }).ToList();
            return ans;
        }

        public List<AnnouncementsList> getAnnouncementListByAcademicYear(int acid)
        {
            var ans = (from t1 in dc.Announcements
                       where t1.AcademicYearId == acid
                       select new AnnouncementsList { AnnouncementId = t1.AnnouncementId, AnnouncementName = t1.AnnouncementName, AnnouncementDate = t1.AnnouncementDate, AnnouncementDescription = t1.AnnouncementDescription }).ToList();
            return ans;
        }

        public bool deleteAcademicYear(int acid)
        {
            var ans = (from c in dc.Announcements
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {            
                return true;
            }
            else
            {
                return false;
            }
        } 

        //public Announcements getAnnouncementByAnnouncementID(int AnnouncementId)
        //{
        //    var ans = (from t1 in dc.Announcements
        //               from t2 in dc.AcademicYears
        //               where t1.AcademicYearId == t2.AcademicYearId && t1.AnnouncementId==AnnouncementId
        //               select new Announcements { AnnoumcementId = t1.AnnouncementId, Heading = t1.AnnouncementName,
        //                                          Date = SqlFunctions.DateName("day", t1.AnnouncementDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.AnnouncementDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.AnnouncementDate),                                                  
        //                   Desc = t1.AnnouncementDescription, AcademicYear = t2.AcademicYear1, acYear = t2.AcademicYearId, Status = t1.Status }).FirstOrDefault();
        //    return ans;
        //}
        public Announcements getAnnouncementByAnnouncementID(int AnnouncementId)
        {
            var ans = (from t1 in dc.Announcements
                       from t2 in dc.AcademicYears
                       where t1.AcademicYearId == t2.AcademicYearId && t1.AnnouncementId == AnnouncementId
                       select new Announcements
                       {
                           AnnoumcementId = t1.AnnouncementId,
                           Heading = t1.AnnouncementName,
                           Date = SqlFunctions.DateName("day", t1.AnnouncementDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.AnnouncementDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.AnnouncementDate),
                           Desc = t1.AnnouncementDescription,
                           AcademicYear = t2.AcademicYear1,
                           acYear = t2.AcademicYearId,
                           Status = t1.Status,
                           Pushnotification = t1.PushNotification
                       }).FirstOrDefault();
            return ans;
        }

        public bool checkAnnouncement(string Announcement_type, DateTime Announcement_date, string Announcement_desc)
        {
            var ans = dc.Announcements.Where(q => q.AnnouncementName.Equals(Announcement_type) && q.AnnouncementDate.Equals(Announcement_date) ).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

       public bool checkAnnouncement(int aid, string Announcement_type, DateTime Announcement_date, string Announcement_desc, string status)
        {

            int count;
            if (status == "Active")
            {

                var ans = (from t1 in dc.Announcements
                           where t1.AnnouncementId != aid && t1.AnnouncementName == Announcement_type && t1.AnnouncementDate==Announcement_date && t1.Status == true
                           select t1).ToList();

                count = ans.Count;
            }

            else
            {
                var ans = (from t1 in dc.Announcements
                           where t1.AnnouncementId != aid && t1.AnnouncementName == Announcement_type && t1.AnnouncementDate == Announcement_date && t1.Status == false
                           select t1).ToList();

                count = ans.Count;
            }

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public List<Announcement> getAnnouncement()
        {
            var ans = dc.Announcements.Select(query => query).ToList();
            return ans;
        }

        public List<Announcements> getAnnouncements()
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Announcements
                       where t1.AcademicYearId == t2.AcademicYearId
                       select new Announcements { acYear=t1.AcademicYearId, AcademicYear = t1.AcademicYear1, AnnoumcementId = t2.AnnouncementId, tacYSD = t2.AnnouncementDate, Heading = t2.AnnouncementName, Desc = t2.AnnouncementDescription, Status = t2.Status,AnnouncementName=t2.AnnouncementName,
                                                  Date = SqlFunctions.DateName("day", t2.AnnouncementDate).Trim() + "-" + SqlFunctions.StringConvert((double)t2.AnnouncementDate.Month).TrimStart() + "-" + SqlFunctions.DateName("year", t2.AnnouncementDate) }).ToList().
                       Select(q => new Announcements { acYear = q.acYear, AcademicYear = q.AcademicYear, AnnoumcementId = q.AnnoumcementId, Date = q.tacYSD.Value.ToString("MMM dd, yyyy"),AnnouncementDate=q.tacYSD.Value.ToString("MMM dd, yyyy"), Aid = QSCrypt.Encrypt(q.AnnoumcementId.ToString()), Heading = q.Heading, Desc = q.Desc, Status = q.Status, AnnouncementName = q.AnnouncementName, Description=q.Desc }).ToList();
            return ans;
        }
        public void addAnnouncements(int acid, DateTime date, string heading, string desc, int adminid, bool pushnofication)
        {
            Announcement announ = new Announcement()
            {
                AcademicYearId = acid,
                AnnouncementDate = date,
                AnnouncementName = heading,
                AnnouncementDescription = desc,
                Created_date = DateTimeByZone.getCurrentDateTime(),
                Createdby = adminid,
                Updated_date = DateTimeByZone.getCurrentDateTime(),
                Updatedby = adminid,
                Status = true,
                PushNotification = pushnofication,
                PushNotificationDeliveryStatus = "Pending"
            };
            dc.Announcements.Add(announ);
            dc.SaveChanges();
        }
        public Announcements geteventid(int Eid)
        {
            var ans = (from t1 in dc.Announcements
                       where t1.EventId == Eid
                       select new Announcements
                       {
                           Status = t1.Status,
                           eventid = t1.EventId
                       }).FirstOrDefault();
            return ans;
        }
        public void updateEventAnnouncement(int? eid, bool? status, string eventName, DateTime date, string eventDesc, int adminid)
        {
            var announcementevent = (from a in dc.Announcements where a.EventId == eid select a).FirstOrDefault();
            if (announcementevent != null)
            {
                announcementevent.Status = status;
                announcementevent.AnnouncementName = eventName;
                announcementevent.AnnouncementDescription = eventDesc;
                announcementevent.Updatedby = adminid;
                announcementevent.Updated_date = DateTimeByZone.getCurrentDateTime();
                dc.SaveChanges();
            }

        }


        public void addAnnouncements(int? acid, DateTime date, string heading, string desc, int adminid, int eventid)
        {
            Announcement announ = new Announcement()
            {
                AcademicYearId = acid,
                AnnouncementDate = date,
                AnnouncementName = heading,
                AnnouncementDescription = desc,
                Created_date = DateTimeByZone.getCurrentDateTime(),
                Createdby = adminid,
                Updated_date = DateTimeByZone.getCurrentDateTime(),
                Updatedby = adminid,
                Status = true,
                EventId = eventid
            };
            dc.Announcements.Add(announ);
            dc.SaveChanges();
        }
        public void addAnnouncements(int acid, DateTime date, string heading, string desc)
        {
            Announcement announ = new Announcement()
            {
                AcademicYearId = acid,
                AnnouncementDate = date,
                AnnouncementName = heading,
                AnnouncementDescription = desc,
                Status = true
            };
            dc.Announcements.Add(announ);
            dc.SaveChanges();
        }
        public void updateAnnouncement(int newsId, DateTime date, string heading, string desc, string Status, bool push)
        {
            var update = dc.Announcements.Where(q => q.AnnouncementId.Equals(newsId)).First();
            update.AnnouncementDate = date;
            update.AnnouncementName = heading;
            update.AnnouncementDescription = desc;
            update.PushNotification = push;
            if (Status == "Active")
            {
                update.Status = true;
            }

            else
            {
                update.Status = false;
            }

            dc.SaveChanges();
        }
        public void updateAnnouncement(int newsId, DateTime date, string heading, string desc, string Status)
        {
            var update = dc.Announcements.Where(q => q.AnnouncementId.Equals(newsId)).First();
            update.AnnouncementDate = date;
            update.AnnouncementName = heading;
            update.AnnouncementDescription = desc;
            if (Status == "Active")
            {
                update.Status = true;
            }

            else
            {
                update.Status = false;
            }

            dc.SaveChanges();
        }

       
        public void deleteAnnouncement(int AnnouncementId)
        {
            var delete = dc.Announcements.Where(q => q.AnnouncementId.Equals(AnnouncementId)).First();
            dc.Announcements.Remove(delete);
            dc.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}