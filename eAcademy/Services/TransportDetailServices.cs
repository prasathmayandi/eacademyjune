﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class TransportDetailServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public List<StudentTransport> getStudentTransport(int acid, int cid, int sec_id)
        {
            var ans = (from t1 in dc.Students
                       from t2 in dc.TransportDetails
                       where t1.StudentRegisterId == t2.StudentRegisterId && t2.AcademicYearId == acid && t2.ClassId == cid && t2.SectionId == sec_id
                       select new StudentTransport { StudentName = t1.FirstName+" "+t1.LastName, RoutetNo = t2.routeNo, BusNo = t2.BusNO, Location = t2.Location, Driver = t2.BusDriver, Contact = t2.driverContact, TransportId = t2.TransportId }).ToList();
            return ans;
        }


        public StudentTransport getTransportDetails(int trans_id)
        {
            var ans = (from t1 in dc.TransportDetails
                       from t2 in dc.AcademicYears
                       from t3 in dc.Classes
                       from t4 in dc.Sections
                       from t5 in dc.Students
                       where t1.AcademicYearId == t2.AcademicYearId && t1.ClassId == t3.ClassId && t1.SectionId == t4.SectionId && t1.StudentRegisterId == t5.StudentRegisterId && t1.TransportId==trans_id
                       select new StudentTransport { StudentRegId = t1.StudentRegisterId, AcademicYr = t2.AcademicYear1, ClassType=t3.ClassType, Section=t4.SectionName, StudentName =t5.FirstName+" "+t5.LastName,  AcYearId = t2.AcademicYearId, ClassId = t3.ClassId, SectionId = t4.SectionId, RoutetNo = t1.routeNo, BusNo = t1.BusNO, Location = t1.Location, Driver = t1.BusDriver,Contact=t1.driverContact, Status = t1.Status }).SingleOrDefault();             
            return ans;
        
        }


        public List<StudentTransport> getTransportDetails()
        {
            var ans =(from t1 in dc.TransportDetails
                      from t2 in dc.AcademicYears
                      from t3 in dc.Classes
                      from t4 in dc.Sections
                      from t5 in dc.Students
                      where t1.AcademicYearId==t2.AcademicYearId && t1.ClassId==t3.ClassId && t1.SectionId==t4.SectionId && t1.StudentRegisterId==t5.StudentRegisterId
                      select new  {tid=t1.TransportId,Acyear=t2.AcademicYear1,classtype=t3.ClassType, sectionType=t4.SectionName,stdContact=t5.Contact, stdname=t5.FirstName+" "+t5.LastName,StudentRegId = t1.StudentRegisterId, AcYearId = t1.AcademicYearId, ClassId = t1.ClassId, SectionId = t1.SectionId, RoutetNo = t1.routeNo, BusNo = t1.BusNO, Location = t1.Location, Driver = t1.BusDriver, Contact = t1.driverContact,status=t1.Status }).ToList().
                      Select(s => new StudentTransport { StudentRegId = s.StudentRegId, StdTransportId = QSCrypt.Encrypt(s.tid.ToString()), AcademicYr = s.Acyear, ClassSection = s.classtype + "-" + s.sectionType, StudentName = s.stdname, stdContact = s.stdContact, AcYearId = s.AcYearId, ClassId = s.ClassId, SectionId = s.SectionId, RoutetNo = s.RoutetNo, BusNo = s.BusNo, Location = s.Location, Driver = s.Driver, Contact = s.Contact, DriverContact=s.Driver+"-"+s.Contact , Status=s.status}).ToList();
            return ans;
        }

        public void addTransportDetails(int acid, int cid, int sec_id, int std_id, string routeNo, string busNo, string location, string driver, long driverContact)
        {
            TransportDetail trans = new TransportDetail
            {
                AcademicYearId = acid,
                ClassId = cid,
                SectionId = sec_id,
                StudentRegisterId = std_id,
                routeNo = routeNo,
                BusNO = busNo,
                Location = location,
                BusDriver = driver,
                driverContact = driverContact,
                Status=true
            };
            dc.TransportDetails.Add(trans);
            dc.SaveChanges();
        }



        public void updateTransportDetails(int transid, int acid, int cid, int sec_id, int std_id, string routeNo, string busNo, string location, string driver, long driverContact)
        {
            var update = dc.TransportDetails.Where(q => q.TransportId.Equals(transid)).First();
            update.AcademicYearId = acid;
            update.ClassId = cid;
            update.SectionId = sec_id;
            update.StudentRegisterId = std_id;
            update.routeNo = routeNo;
            update.BusNO = busNo;
            update.BusDriver = driver;
            update.driverContact = driverContact;
            dc.SaveChanges();
        }

        public void deleteTransportDetails(int transid)
        {
            var delete = dc.TransportDetails.Where(query => query.TransportId.Equals(transid)).First();
            dc.TransportDetails.Remove(delete);
            dc.SaveChanges();
        }

        internal bool checkTransportDetails(int acid, int cid, int sec_id, int std_id)
        {
            var ans = dc.TransportDetails.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) && q.StudentRegisterId.Value.Equals(std_id)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal void updateTransportDetails(int transId, string routeNo, string busNo, string location, string driver, long driverNo, string Status)
        {
            var update = dc.TransportDetails.Where(q => q.TransportId.Equals(transId)).First();
         
            update.routeNo = routeNo;
            update.BusNO = busNo;
            update.BusDriver = driver;
            update.driverContact = driverNo;

            if (Status == "Active")
            {
                update.Status = true;
            }

            else
            {
                update.Status = false;
            }

            dc.SaveChanges();
        }

        internal bool checkTransportDetails(int transId, string routeNo, string busNo, string location)
        {
            var ans = dc.TransportDetails.Where(q => q.TransportId.Equals(transId)&&q.routeNo.Equals(routeNo)&&q.BusNO.Equals(busNo)&&q.Location.Equals(location)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //jegadeesh

        public List<TransportList> GetTransportStudent(int yearid)
        {
            var ans = (from a in dc.AssignClassToStudents
                       from b in dc.Students
                       from c in dc.Classes
                       from d in dc.Sections
                       from e in dc.AcademicYears
                       from f in dc.TransportAllotments
                       from g in dc.StudentRollNumbers
                       from h in dc.TransportMappings
                       where a.AcademicYearId == yearid && a.AcademicYearId == e.AcademicYearId && a.StudentRegisterId == b.StudentRegisterId && a.ClassId == c.ClassId && a.ClassId == d.ClassId && f.AcademicYearId ==a.AcademicYearId && h.RouteId == f.RouteMapId && f.Status ==true &&
                       a.SectionId == d.SectionId && a.StudentRegisterId == f.StudentRegisterId && g.StudentRegisterId==b.StudentRegisterId && a.Status==true && g.Status == true && 
                       a.AcademicYearId==g.AcademicYearId &&
                       a.SectionId==g.SectionId &&
                       a.ClassId==g.ClassId
                       select new TransportList
                       {
                           AcademicYear = e.AcademicYear1,
                           StudentId=b.StudentId,
                           RollNumber=g.RollNumber,
                           StudentName = b.FirstName +" "+b.LastName,
                           Class = c.ClassType,
                           Section = d.SectionName,
                           StudentContact = b.Contact,
                           BusNo = h.RouteNumber,
                           EmergencyContactPerson = b.EmergencyContactPerson,
                           EmergencyContactPersonNo = b.EmergencyContactNumber,
                           Address = b.LocalAddress1+" "+ b.LocalAddress2 + "," + b.LocalCity + "," + b.LocalState
                       }).ToList();
            return ans;

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}