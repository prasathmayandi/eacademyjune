﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System.Collections.Generic;
using System.Linq;
using System;

namespace eAcademy.Services
{
    public class RoleServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public RoleList getRole(int rid)
        {
            var ans = dc.Roles.Where(query => query.RoleId.Equals(rid)).Select(q => new RoleList { Role = q.RoleType, Status = q.Status }).FirstOrDefault();
            return ans;
        }

        public List<Role> getRole()
        {
            var ans = dc.Roles.Where(q => q.Status.Value.Equals(true)).ToList();
            return ans;
        }

        public bool checkRole(string rname)
        {
            var ans = dc.Roles.Where(q => q.RoleType.Equals(rname)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<RoleList> getRoles()
        {
            var ans = (from t1 in dc.Roles
                       select new { rid = t1.RoleId, role = t1.RoleType, status = t1.Status }).ToList().
                     Select(q => new RoleList { RID = QSCrypt.Encrypt(q.rid.ToString()),Role=q.role,Status=q.status,RoleId=q.rid }).ToList();
            return ans;
        }

        public void addRole(string roleName)
        {
            Role add = new Role()
            {
                RoleType = roleName,
                Status = true
            };

            dc.Roles.Add(add);
            dc.SaveChanges();
        }


        internal bool checkRole(string rolename, string status)
        {
            int count;
            if (status == "Active")
            {

                var ans = dc.Roles.Where(q => q.RoleType.Equals(rolename) && q.Status.Value.Equals(true)).ToList();
                count = ans.Count;
            }

            else
            {
                var ans = dc.Roles.Where(q => q.RoleType.Equals(rolename) && q.Status.Value.Equals(false)).ToList();
                count = ans.Count;
            }

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void updateRole(int rid, string roleName, string status)
        {
            var update = dc.Roles.Where(query => query.RoleId.Equals(rid)).First();
            update.RoleType = roleName;

            if (status == "Active")
            {
                update.Status = true;
            }

            else
            {
                update.Status = false;
            }
            dc.SaveChanges();
        }

        public void deleteRole(int rid)
        {
            var delete = dc.Roles.Where(query => query.RoleId.Equals(rid)).First();
            dc.Roles.Remove(delete);
            dc.SaveChanges();
        }

        internal List<Role> getallRole()
        {
            var ans = dc.Roles.Select(q => q).OrderBy(q=>q.RoleType).ToList();
            return ans;
        }

        internal List<FeatureRole> getFeatureLink(int empid)
        {
           var ans= (from t1 in dc.MapRoleFeatures
             from t2 in dc.Features
             from t3 in dc.MapRoleUsers
             from t4 in dc.Roles
             where t1.FeatureId == t2.FeatureId && t3.RoleId == t4.RoleId && t3.RoleId == t1.RoleId && t3.EmployeeRegisterId == empid && t2.FeatureStatus == true && t3.status.Value == true && t1.status.Value == true && t4.Status.Value == true
                     select new FeatureRole { fid = t2.FeatureId, link = t2.FeatureLink, feature = t2.FaetureType, gname = t2.GroupName, order = t2.Priority.Value, groupOrder = t2.GroupPriority.Value }).Distinct().OrderBy(o => o.groupOrder).OrderBy(q => q.order).ToList();
           return ans;

        }

        internal List<FeatureRole> getRoleType(int empid)
        {

          var ans=  (from t1 in dc.MapRoleFeatures
             from t2 in dc.Features
             from t3 in dc.MapRoleUsers
             from t4 in dc.Roles
             where t1.FeatureId == t2.FeatureId && t3.RoleId == t4.RoleId && t3.RoleId == t1.RoleId && t3.EmployeeRegisterId == empid && t2.FeatureStatus == true && t4.Status == true && t3.status == true
                     select new FeatureRole { role = t4.RoleType }).Distinct().ToList();
          return ans;
        }

        internal List<FeatureRole> getControllerName(int empid)
        {
           var ans= (from t1 in dc.MapRoleFeatures
             from t2 in dc.Features
             from t3 in dc.MapRoleUsers
             from t4 in dc.Roles
             where t1.FeatureId == t2.FeatureId && t3.RoleId == t4.RoleId && t3.RoleId == t1.RoleId && t3.EmployeeRegisterId == empid && t2.FeatureStatus == true && t3.status.Value == true && t1.status.Value == true && t4.Status.Value == true
                     select new FeatureRole { cname = t2.ControllerName }).Distinct().ToList();
           return ans;
        }

        internal List<FeatureRole> getGName(int empid)
        {
            var ans = (from t1 in dc.MapRoleFeatures
                       from t2 in dc.Features
                       from t3 in dc.MapRoleUsers
                       from t4 in dc.Roles
                       where t1.FeatureId == t2.FeatureId && t3.RoleId == t4.RoleId && t3.RoleId == t1.RoleId && t3.EmployeeRegisterId == empid && t2.FeatureStatus == true && t3.status.Value == true && t1.status.Value == true && t4.Status.Value == true
                       select new FeatureRole { gname = t2.GroupName, groupOrder=t2.GroupPriority.Value }).Distinct().OrderBy(o=>o.groupOrder).ToList();
            return ans;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}