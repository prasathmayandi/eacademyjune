﻿using eAcademy.DataModel;
using System;
using System.Collections;
using System.Linq;

namespace eAcademy.Services
{
    public class PeriodsScheduleServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        internal void addPeriodsSchedule(int p1, string p2, string p3, TimeSpan p4, TimeSpan p5, int? Pe_Num)
        {
            PeriodsSchedule add = new PeriodsSchedule()
            {
                TimeScheduleId = p1,
                Periods = p2,
                Type = p3,
                StartTime = p4,
                EndTime = p5,
                PeriodNumber = Pe_Num
                
            };
            dc.PeriodsSchedules.Add(add);
            dc.SaveChanges();
        }

        internal IEnumerable getPeriodsById(int p)
        {
            var ans = dc.PeriodsSchedules.
                Where(q => q.TimeScheduleId.Value.Equals(p)).
                Select(s => new { Periods = s.Periods, Type = s.Type, StartTime = s.StartTime.Value, EndTime = s.EndTime.Value, PeriodScheduleId = s.PeriodScheduleId}).ToList().
                Select(q => new { PeriodScheduleId = q.PeriodScheduleId, Periods = q.Periods, Type = q.Type,  StartTime = new DateTime(q.StartTime.Ticks).ToString("HH:mm tt"), EndTime = new DateTime(q.EndTime.Ticks).ToString("HH:mm tt") });
            return ans;
        }

        internal void updatePeriodsSchedule(int p1, int p2, string p3, string p4, TimeSpan timeSpan1, TimeSpan timeSpan2, int? p5)
        {
            var update = dc.PeriodsSchedules.Where(q => q.PeriodScheduleId.Equals(p2)).FirstOrDefault();
            update.Periods = p3;
            update.PeriodNumber = p5;
            update.Type = p4;
            update.StartTime = timeSpan1;
            update.EndTime = timeSpan2;

            dc.SaveChanges();
        }

        //public AssignSubjectToPeriod checkperiodusingdayorder(int periodid)
        //{
        //    var checkperiod = 0;
        //    return checkperiod;
        //}
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}