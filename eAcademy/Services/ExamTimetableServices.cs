﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class ExamTimetableServices:IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public IEnumerable<Object> getExamList(int yearId, int classId,int secId,  int passExamId)
        {
            var ExamList = (from a in db.ExamTimetables
                            from b in db.Subjects
                            from c in db.AssignSubjectToSections
                            where a.AcademicYearId == yearId &&
                            a.ClassId == classId &&
                            a.ExamId == passExamId &&

                            c.SectionId == secId &&

                            c.AcademicYearId == yearId &&
                            c.ClassId == classId &&
                            a.SubjectId == b.SubjectId &&
                            a.SubjectId==c.SubjectId &&
                            b.SubjectId==c.SubjectId
                            select new
                            {
                                Date = SqlFunctions.DateName("day", a.ExamDate).Trim() + " " + SqlFunctions.DateName("month", a.ExamDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.ExamDate),
                                Subject = b.SubjectName

                            }).ToList();
            return ExamList;
        }
        public ExamTimetable chexkRecordExist(int yearId, int classId, int examId)
        {
            var getRow = (from a in db.ExamTimetables where a.AcademicYearId == yearId && a.ClassId == classId && a.ExamId == examId select a).FirstOrDefault();
            return getRow;
        }
        public ExamTimetable checkDataExistForSubject(int yearId, int classId, int examId, int subId)
        {
            var getRow = (from a in db.ExamTimetables where a.AcademicYearId == yearId && a.ClassId == classId && a.ExamId == examId && a.SubjectId == subId select a).FirstOrDefault();
            return getRow;
        }
        public void addExamTimetable(int AcademicYearId, int ClassId, int ExamId, int SubjectId, DateTime ExamDate, int schedule)
        {
            ExamTimetable ex = new ExamTimetable()
            {
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                ExamId = ExamId,
                SubjectId = SubjectId,
                ExamDate = ExamDate,
                TimeScheduleId = schedule
            };
            db.ExamTimetables.Add(ex);
            db.SaveChanges();
        }
        public void UpdateExamTimetable(int AcademicYearId, int ClassId, int ExamId, int SubjectId, DateTime ExamDate, int schedule)
        {
            var checkExamFinished = (from a in db.ExamAttendances where a.AcademicYearId == AcademicYearId && a.ClassId == ClassId && a.ExamId == ExamId && a.SubjectId == SubjectId && a.AttendanceStatus == "P" select a).FirstOrDefault();
            if (checkExamFinished == null)
            {
                var update = (from a in db.ExamTimetables where a.AcademicYearId == AcademicYearId && a.ClassId == ClassId && a.ExamId == ExamId && a.SubjectId == SubjectId select a).First();
                // update.SubjectId = SubjectId;
                update.ExamDate = ExamDate;
                update.TimeScheduleId = schedule;
                db.SaveChanges();
            }
        }
        public string UpdateExamTimetable(int id, int AcademicYearId, int ClassId, int ExamId, int SubjectId, DateTime ExamDate, int schedule)
        {
            string msg = "a";
            var checkExamFinished = (from a in db.ExamAttendances where a.AcademicYearId == AcademicYearId && a.ClassId == ClassId && a.ExamId == ExamId && a.SubjectId == SubjectId && a.AttendanceStatus == "P" select a).FirstOrDefault();
            if(checkExamFinished == null)
            {
                var update = (from a in db.ExamTimetables where a.Id == id && a.ClassId == ClassId && a.ExamId == ExamId select a).First();
                update.SubjectId = SubjectId;
                update.ExamDate = ExamDate;
                update.TimeScheduleId = schedule;
                db.SaveChanges();
                return msg = "updated";
            }
            else
            {
                return msg = "Exam_Already_Finished_So_unable_to_change_date";
            }
        }

        public List<ExistExamTimetableList> ExistList(int yearId, int classId, int examId)
        {
            var list = (from a in db.ExamTimetables
                        from b in db.Subjects
                        where a.SubjectId == b.SubjectId &&

                         a.AcademicYearId == yearId &&
                         a.ClassId == classId &&
                        a.ExamId == examId
                        select new
                        {
                            id = a.Id,
                            SubjectId = a.SubjectId,
                            date = a.ExamDate,
                            schedule = a.TimeScheduleId,
                            SubjectName = b.SubjectName,
                        }).Distinct().ToList().Select(a => new ExistExamTimetableList
                        {
                            id = QSCrypt.Encrypt(a.id.ToString()),
                            SubjectId = a.SubjectId,
                            date = a.date.Value.ToString("dd/MM/yyyy"),
                            Schedule = a.schedule,
                            SubjectName = a.SubjectName,
                        }).ToList();
            return list;
        }

        public List<ExamTimetableList> getExamTimetableList(int AcademicYearId,int ExamId,int ClassId,int SectionId)
        {
            var list = (from a in db.ExamTimetables
                        from b in db.Subjects
                        where a.SubjectId == b.SubjectId &&
                              a.AcademicYearId == AcademicYearId &&
                              a.ExamId == ExamId &&
                              a.ClassId == ClassId &&
                              a.SectionId == SectionId
                        select new ExamTimetableList
                        {
                            Id = a.Id,
                            ExamDate = a.ExamDate,
                            SubjectName = b.SubjectName,
                            StartTime = a.StartTime,
                            EndTime = a.EndTime
                        }).ToList();
            return list;
        }

        public List<ExamTimetableList> getExamTimetableList()
        {
            var list = (from a in db.ExamTimetables
                        from b in db.Subjects
                        where a.SubjectId == b.SubjectId
                        select new ExamTimetableList
                        {
                            ExamDate = a.ExamDate,
                            SubjectName = b.SubjectName,
                            StartTime = a.StartTime,
                            EndTime = a.EndTime,
                            Id = a.Id
                        }).ToList();
            return list;
        }

        public ExamTimetable checkDataExist(int tallAcademicYears, int allClass, int Section, int allExams, int allSubjects)
        {
           var CheckExamDateAlreadyAssignedForSubject = (from a in db.ExamTimetables where a.AcademicYearId == tallAcademicYears && a.ClassId == allClass && a.SectionId == Section && a.ExamId == allExams && a.SubjectId == allSubjects select a).FirstOrDefault();
           return CheckExamDateAlreadyAssignedForSubject;
        }

        public void addExamTimetable(int AcademicYearId,int ClassId,int SectionId,int ExamId,int SubjectId,DateTime ExamDate,TimeSpan StartTime,TimeSpan EndTime)
        {
            ExamTimetable ex = new ExamTimetable()
            {
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                ExamId = ExamId,
                SubjectId = SubjectId,
                ExamDate = ExamDate,
                StartTime = StartTime,
                EndTime = EndTime
            };
            db.ExamTimetables.Add(ex);
            db.SaveChanges();
        }

        public ExamTimetable getRecord(int id)
        {
            var getRecord = (from a in db.ExamTimetables where a.Id == id select a).FirstOrDefault();
            return getRecord;
        }

        public void updateExamTimetable(int id,int AcademicYearId,int ExamId,TimeSpan StartTime,TimeSpan EndTime,int ClassId,int SectionId,int SubjectId,DateTime ExamDate)
        {
            var update = (from a in db.ExamTimetables where a.Id == id select a).First();
            update.AcademicYearId = AcademicYearId;
            update.ExamId = ExamId;
            update.StartTime = StartTime;
            update.EndTime = EndTime;
            update.ClassId = ClassId;
            update.SectionId = SectionId;
            update.SubjectId = SubjectId;
            update.ExamDate = ExamDate;
            db.SaveChanges();
        }

        public ExamTimetable getExamDate(int? allAcademicYears, int? allClass, int allExams, int allSubjects)
        {
            var CheckRecordExist = (from a in db.ExamTimetables where a.AcademicYearId == allAcademicYears && a.ClassId == allClass && a.ExamId == allExams && a.SubjectId == allSubjects select a).FirstOrDefault();
            return CheckRecordExist;
        }
        public List<ExamDates> getExamDates(int yearId, int examId)
        {
            var examList = (from a in db.ExamTimetables
                            where a.AcademicYearId == yearId &&
                            a.ExamId == examId
                            select new
                            {
                                date = a.ExamDate
                            }).Distinct().ToList().Select(a => new ExamDates
                            {
                                date = a.date.Value.ToString("d-M-yyyy")
                            }).Distinct().ToList();
            return examList;
        }

        public IEnumerable<Object> getScheduleBydate(int year, int exam, DateTime date)
        {
            var list = (from a in db.ExamTimetables
                        from b in db.ExamTimeSchedules
                        where a.TimeScheduleId == b.Id &&
                        a.AcademicYearId == year &&
                        a.ExamId == exam &&
                        a.ExamDate == date
                        select new
                        {
                            ScheduleValue = a.TimeScheduleId,
                            ScheduleName = b.ScheduleName
                        }).Distinct().ToList();
            return list;
        }

        public ExamTimetable IsScheduleUsed(int scheduleId)
        {
            var row = (from a in db.ExamTimetables where a.TimeScheduleId == scheduleId select a).FirstOrDefault();
            return row;
        }

        public List<ExamTable> GetExamTimeTable(int yid, int cid, int sid, int eid)
        {
            var ans = (from a in db.ExamTimetables
                       from b in db.Subjects
                       from c in db.Exams
                       from d in db.ExamTimeSchedules
                       where a.AcademicYearId == yid && a.ClassId == cid  && a.ExamId == eid && a.SubjectId == b.SubjectId && a.ExamId == c.ExamId && d.Id == a.TimeScheduleId
                       select new ExamTable
                       {
                           SubjectName = b.SubjectName,
                           dates = a.ExamDate,      
                           sttime = d.StartTime,
                           endtime = d.EndTime
                       }).ToList().Select(a => new ExamTable
                       {
                           SubjectName = a.SubjectName,
                           ExamDates = a.dates.Value.ToString("MMM dd, yyyy"),
                           StartTime = a.sttime.Value.ToString(@"hh\:mm"),
                           EndTime = a.endtime.Value.ToString(@"hh\:mm")
                       }).OrderBy(q=>q.ExamDates).ToList();

            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}