﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using eAcademy.Helpers;

namespace eAcademy.Services
{
    public class OnLineRegisterServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public IEnumerable<Object> getStudentListToAssignClass(int yearId, int classId)
        {
            var List = (from a in dc.PreAdmissionStudentRegisters
                        where a.AcademicyearId == yearId &&
                        a.AdmissionClass == classId &&
                        a.Statusflag == "Accepted"
                        select new
                        {
                            onlineRegId = a.OnlineRegisterId,
                            stuAdmissionId = a.StudentAdmissionId,
                            AdmissionId = a.StudentAdmissionId,
                            FName = a.StuFirstName,
                            LName = a.StuLastname,
                            Gender = a.Gender,
                            FirstLanguage = a.FirstLanguage,
                            MediumOfStudy = a.PreSchoolMedium,
                            WayOfEnrollment = a.ApplicationSource
                        }).ToList().Select(a => new 
                         {
                             onlineRegId = QSCrypt.Encrypt(a.onlineRegId.ToString()),
                             stuAdmissionId = QSCrypt.Encrypt(a.stuAdmissionId.ToString()),
                             AdmissionId = a.AdmissionId,
                             FName = a.FName,
                             LName = a.LName,
                             Gender = a.Gender,
                             FirstLanguage = a.FirstLanguage,
                             MediumOfStudy = a.MediumOfStudy,
                             WayOfEnrollment = a.WayOfEnrollment
                        }).ToList();
            return List;
        }
     
        public ParentRegister CheckUserName(string uname)
        {
            var res = (from a in dc.PreAdmissionOnLineRegisters
                       from b in dc.ParentRegisters
                       where a.Username == uname && b.UserName == uname
                       select b).FirstOrDefault();
            return res;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}