﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class ExamAttendanceServices:IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public ExamAttendance getExamAttendance(int passYearId, int passClassId, int passSecId, int passExamId, int passSubId)
        {
            var getRow = (from a in db.ExamAttendances where a.AcademicYearId == passYearId && a.ClassId == passClassId && a.SectionId == passSecId && a.ExamId == passExamId && a.SubjectId == passSubId select a).FirstOrDefault();
            return getRow;
        }

        public ExamAttendance isRecordExist(int YearId,int classId,int secId,int ExamId,DateTime date,int schedule,int RollNumId)
        {
            var getRow = (from a in db.ExamAttendances where a.AcademicYearId == YearId && a.ClassId == classId && a.SectionId == secId && a.ExamId == ExamId && a.ExamDate == date && a.TimeScheduleId == schedule && a.RollNumberId == RollNumId select a).FirstOrDefault();
            return getRow;
        }
        public ExamAttendance isRecordExist(int YearId, int classId, int secId, int ExamId, DateTime date, int schedule)
        {
            var getRow = (from a in db.ExamAttendances where a.AcademicYearId == YearId && a.ClassId == classId && a.SectionId == secId && a.ExamId == ExamId && a.ExamDate == date && a.TimeScheduleId == schedule  select a).FirstOrDefault();
            return getRow;
        }
        public ExamAttendance CheckAttentedExam(int year,int classId,int secId,int studentRegisterId)
        {
            var row = (from a in db.ExamAttendances where a.AcademicYearId == year && a.ClassId == classId && a.SectionId == secId select a).FirstOrDefault();
            return row;
        }
        public ExamAttendance CheckAttendanceExist(int? allAcademicYears,int? allClass,int secId,int allSubjects,int allExams,DateTime ExamDate)
        {
            var row = (from a in db.ExamAttendances where a.AcademicYearId == allAcademicYears && a.ClassId == allClass && a.SectionId == secId && a.SubjectId == allSubjects && a.ExamId == allExams select a).FirstOrDefault();
            return row;
        }
        public List<ExamAttendanceStudentList> getExistList(int YearId,int classId,int secId,int ExamId,DateTime date,int schedule,int StartRollNumberId,int EndRollNumberId)
        {
            var List = (from a in db.ExamAttendances
                        from b in db.Students
                        from c in db.StudentRollNumbers
                        where a.StudentRegisterId == b.StudentRegisterId &&
                        a.StudentRegisterId == c.StudentRegisterId &&
                        a.AcademicYearId == YearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.ExamId == ExamId &&
                        a.ExamDate == date &&
                        a.TimeScheduleId == schedule &&
                        a.RollNumberId >= StartRollNumberId &&
                        a.RollNumberId <= EndRollNumberId &&
                        c.AcademicYearId == YearId &&
                        c.ClassId == classId &&
                        c.SectionId == secId 
                        select new
                        {
                            AttStatus = a.AttendanceStatus,
                            RollNum = c.RollNumber,
                            FirstName = b.FirstName,
                            LastName = b.LastName,
                            rid = a.AttendanceId,
                            comment = a.LeaveReason
                        }).ToList().Select(a => new ExamAttendanceStudentList 
                        {
                            AttStatus = a.AttStatus,
                            RollNumber = a.RollNum,
                            StudentName = a.FirstName+" "+a.LastName,
                            rid = QSCrypt.Encrypt(a.rid.ToString()),
                            Comment = a.comment
                        }).ToList();
            return List;
        }
        public List<ExamAttendanceStudentList> getExistList(int YearId, int classId, int secId, int ExamId, DateTime date, int schedule)
        {
            var List = (from a in db.ExamAttendances
                        from b in db.Students
                        from c in db.StudentRollNumbers
                        where a.StudentRegisterId == b.StudentRegisterId &&
                        a.StudentRegisterId == c.StudentRegisterId &&
                        a.AcademicYearId == YearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.ExamId == ExamId &&
                        a.ExamDate == date &&
                        a.TimeScheduleId == schedule &&                       
                        c.AcademicYearId == YearId &&
                        c.ClassId == classId &&
                        c.SectionId == secId
                        select new
                        {
                            AttStatus = a.AttendanceStatus,
                            RollNum = c.RollNumber,
                            FirstName = b.FirstName,
                            LastName = b.LastName,
                            rid = a.AttendanceId,
                            comment = a.LeaveReason
                        }).ToList().Select(a => new ExamAttendanceStudentList
                        {
                            AttStatus = a.AttStatus,
                            RollNumber = a.RollNum,
                            StudentName = a.FirstName + " " + a.LastName,
                            rid = QSCrypt.Encrypt(a.rid.ToString()),
                            Comment = a.comment
                        }).ToList();
            return List;
        }

        public void addExamAttendance(int year, int ExamId, DateTime date, int schedule, int classId, int secId, int rollNumid, int sRegId, string AttendanceStatus, string Reason, int facultyid, int subId)
        {
            ExamAttendance add = new ExamAttendance()
            {
                AcademicYearId = year,
                ClassId = classId,
                SectionId = secId,
                SubjectId = subId,
                StudentRegisterId = sRegId,
                ExamId = ExamId,
                ExamDate = date,
                TimeScheduleId = schedule,
                AttendanceStatus = AttendanceStatus,
                LeaveReason = Reason,
                RollNumberId = rollNumid,
                MarkedBy = facultyid
            };
            db.ExamAttendances.Add(add);
            db.SaveChanges();
        }

        public void UpdateExamAttendance(int year, int ExamId, DateTime date, int schedule, int classId, int secId, string AttendanceStatus, string Reason, int facultyid)
        {
            var getRow = (from a in db.ExamAttendances where a.AcademicYearId == year && a.ClassId == classId && a.SectionId == secId && a.ExamId == ExamId && a.ExamDate == date && a.TimeScheduleId == schedule  select a).FirstOrDefault();
            getRow.AttendanceStatus = AttendanceStatus;
            getRow.LeaveReason = Reason;
            getRow.UpdatedBy = facultyid;
            db.SaveChanges();
        }

        public ExamAttendance CheckAttendanceExist(int yearId,int classId,int SecId,int subId,int examId,DateTime date,int schedule,int startRollNumId,int StartRegId)
        {
            var Row = (from a in db.ExamAttendances where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == SecId && a.SubjectId == subId && a.ExamId == examId && a.ExamDate == date && a.TimeScheduleId == schedule && a.RollNumberId == startRollNumId && a.StudentRegisterId == StartRegId select a).FirstOrDefault();
            return Row;
        }

        public ExamAttendance CheckAttendanceExistForSchedule(int year,int ExamId,DateTime date,int schedule)
        {
            var Row = (from a in db.ExamAttendances where a.AcademicYearId == year && a.ExamId == ExamId && a.ExamDate == date && a.TimeScheduleId == schedule select a).FirstOrDefault();
            return Row;
        }

        public ExamAttendance CheckExamAttendanceExist(int passYearId, int passClassId, int passSecId, int passSubId, int passExamId)
        {
            var row = (from a in db.ExamAttendances where a.AcademicYearId == passYearId && a.ClassId == passClassId && a.SectionId == passSecId && a.SubjectId == passSubId && a.ExamId == passExamId select a).FirstOrDefault();
            return row;      
        }

        //jegadeesh
        public List<ExamTable> GetExamAttendance(int yid, int cid, int sid, int eid, int subid)
        {
            var ans = (from a in db.ExamAttendances
                       from b in db.Subjects
                       from c in db.Exams
                       from d in db.StudentRollNumbers
                       from e in db.Students
                       from f in db.AssignClassToStudents
                       where a.AcademicYearId == yid && 
                       a.ClassId == cid && 
                       a.SectionId == sid && 
                       a.ExamId == eid && 
                       a.SubjectId == subid &&                       
                       a.SubjectId == b.SubjectId && 
                       a.ExamId == c.ExamId &&
                       f.AcademicYearId == yid &&
                       f.ClassId == cid &&
                       f.SectionId == sid &&
                       d.AcademicYearId == yid &&
                       d.ClassId == cid &&
                       d.SectionId == sid && 
                       a.StudentRegisterId ==  d.StudentRegisterId && 
                       a.StudentRegisterId ==  e.StudentRegisterId &&
                       a.StudentRegisterId == f.StudentRegisterId &&
                       d.Status == true &&                      
                       f.StudentRegisterId == e.StudentRegisterId                      
                       
                       select new ExamTable
                       {
                           ExamType = c.ExamName,
                           RollNumber = d.RollNumber,
                           studentname = e.FirstName + " " + e.LastName,
                           SubjectName = b.SubjectName,
                           dates=a.ExamDate.Value,
                           ExamDates = SqlFunctions.DateName("day", a.ExamDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.ExamDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.ExamDate),      // a.DOB,
                           AttendanceStatus = a.AttendanceStatus
                       }).OrderBy(q => q.RollNumber).Distinct().ToList().Select(a => new ExamTable
                       {                       
                           ExamType=a.ExamType,
                           RollNumber=a.RollNumber,
                           studentname=a.studentname,
                           SubjectName=a.SubjectName,
                           AttendanceStatus=a.AttendanceStatus,
                           ExamDates = a.dates.Value.ToString("MMM dd, yyyy")
                       }).Distinct().ToList();
            return ans;
        }
        public List<ExamAttendance> GetExamPresent(int yid, int cid, int sid, int eid, int subid)
        {
            var ans = (from a in db.ExamAttendances

                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.SubjectId == subid && a.AttendanceStatus == "P" && a.ExamId == eid
                       select a).ToList();
            return ans;
        }
        public List<ExamAttendance> GetExamAbsent(int yid, int cid, int sid, int eid, int subid)
        {
            var ans = (from a in db.ExamAttendances

                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.SubjectId == subid && a.AttendanceStatus == "A" && a.ExamId == eid
                       select a).ToList();
            return ans;
        }
        //priya
        public ExamAttendance getattendance(int examid)
        {

            var ans = (from a in db.ExamAttendances where a.ExamId == examid select a).FirstOrDefault();
            return ans;

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}