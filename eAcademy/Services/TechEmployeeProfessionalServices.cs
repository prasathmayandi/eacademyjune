﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eAcademy.Services
{
    public class TechEmployeeProfessionalServices : Controller
    {
        EacademyEntities dc = new EacademyEntities();

        public List<M_TechEmployeeProfessional> GetEmployeeProfessionalDetails(int EmployeeId)
        {
            var res = (from a in dc.TechEmployeeProfessionals
                       where a.EmployeeRegisterId == EmployeeId
                       select new M_TechEmployeeProfessional
                       {
                           ProfessionalDetailsId=a.TechEmployeeProfessionalId,
                           Course = a.Cours,
                           Certificate = a.CertificateName,
                           File = a.CertificateFile,
                           
                       }).ToList();
            return res;
        }

        //private bool disposed = false;

        //protected virtual void Dispose(bool disposing)
        //{
        //    if (!this.disposed)
        //    {
        //        if (disposing)
        //        {
        //            dc.Dispose();
        //        }
        //    }
        //}
        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}
    }
}
