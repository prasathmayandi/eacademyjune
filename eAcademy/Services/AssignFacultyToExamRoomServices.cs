﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class AssignFacultyToExamRoomServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public AssignFacultyToExamRoom IsFacultyAssignedToExamRoom(int YearId, int ExamId, DateTime date, int Schedule)
        {
            var GetRow = (from a in db.AssignFacultyToExamRooms where a.AcademicYearId == YearId && a.ExamId == ExamId && a.ExamDate == date && a.TimeScheduleId == Schedule select a).FirstOrDefault();
            return GetRow;
        }

        public List<ExamRoomList> GetExistList(int YearId, int ExamId, DateTime date, int Schedule)
        {
            var List = (from a in db.AssignFacultyToExamRooms
                        from b in db.ClassRooms
                        where a.ClassRoomId == b.ClassRoomId &&
                         a.AcademicYearId == YearId &&
                         a.ExamId == ExamId &&
                         a.ExamDate == date &&
                         a.TimeScheduleId == Schedule
                        select new 
                        {
                            RoomNumber = b.RoomNumber,
                            RoomId = a.ClassRoomId,
                            FacultyId = a.EmployeeRegisterId,
                            id = a.Id
                        }).ToList().Select(a => new ExamRoomList
                        {
                            RoomNumber = a.RoomNumber,
                            RoomId = QSCrypt.Encrypt(a.RoomId.ToString()),
                            FacultyId = a.FacultyId,
                            id = QSCrypt.Encrypt(a.id.ToString())
                        }).ToList();
            return List;
        }

        public void addInvigilator(int year, int ExamId, DateTime date, int schedule, int RoomId, int FacultyId)
        {
            AssignFacultyToExamRoom add = new AssignFacultyToExamRoom()
            {
                AcademicYearId = year,
                ExamId = ExamId,
                ExamDate = date,
                TimeScheduleId = schedule,
                ClassRoomId = RoomId,
                EmployeeRegisterId = FacultyId
            };
            db.AssignFacultyToExamRooms.Add(add);
            db.SaveChanges();
        }

        public void UpdateInvigilator(int id, int year, int ExamId, DateTime date, int schedule, int RoomId, int FacultyId)
        {
            var getRow = (from a in db.AssignFacultyToExamRooms where a.Id == id && a.AcademicYearId == year && a.ExamId == ExamId && a.ExamDate == date && a.TimeScheduleId == schedule select a).FirstOrDefault();
            getRow.ClassRoomId = RoomId;
            getRow.EmployeeRegisterId = FacultyId;
            db.SaveChanges();
        }


        public IEnumerable<Object> GetRoom(int YearId,int ExamId,DateTime Date,int Schedule,int FacultyId)
        {
            var RoomList = (from a in db.AssignFacultyToExamRooms
                            from b in db.ClassRooms
                            where a.ClassRoomId == b.ClassRoomId &&
                                  a.AcademicYearId == YearId &&
                                  a.ExamId == ExamId &&
                                  a.ExamDate == Date &&
                                  a.TimeScheduleId == Schedule &&
                                  a.EmployeeRegisterId == FacultyId 
                                    select new
                                    {
                                        RoomNumber = b.RoomNumber,
                                        RoomId = a.ClassRoomId
                                    }).ToList().AsEnumerable();
            return RoomList;
        }

        public AssignFacultyToExamRoom checkInvigilatorIsFree(int PassYearId,int PassExamId,DateTime passExamDate,int passEmpRegId)
        {
            var row = (from a in db.AssignFacultyToExamRooms where a.AcademicYearId == PassYearId && a.ExamId == PassExamId && a.ExamDate == passExamDate && a.EmployeeRegisterId == passEmpRegId select a).FirstOrDefault();
            return row;
        }

        public List<InvigilatorScheduleList> getInvigilatorScheduleList(int PassYearId,int PassExamId,DateTime passExamDate,int passEmpRegId)
        {
            var list = (from a in db.AssignFacultyToExamRooms
                        where a.AcademicYearId == PassYearId &&
                        a.ExamId == PassExamId &&
                        a.ExamDate == passExamDate &&
                        a.EmployeeRegisterId == passEmpRegId
                        select new InvigilatorScheduleList
                        {
                            TimeScheduleId = a.TimeScheduleId
                        }).ToList();
            return list;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}