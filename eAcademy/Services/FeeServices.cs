﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class FeeServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public List<FeeStructure> getAnnualFee(int yearId, int classId)
        {
            var getAnnualFeeStructure = (from a in dc.Fees
                                         from b in dc.FeeCategories
                                         where a.AcademicYearId == yearId &&
                                             a.ClassId == classId &&
                                             a.FeeCategoryId == b.FeeCategoryId
                                         select new FeeStructure
                                         {
                                             FeeName = b.FeeCategoryName,
                                             Amount = a.Amount,
                                             Last_Date = SqlFunctions.DateName("day", a.LastDate).Trim() + " " + SqlFunctions.DateName("month", a.LastDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.LastDate)
                                         }).ToList();
            return getAnnualFeeStructure;
        }

        public FeeInformation getFeeByID(int acid, int fid, int cid)
        {
            var ans = (from t1 in dc.Fees
                       where t1.AcademicYearId == acid &&
                       t1.FeeCategoryId == fid &&
                       t1.ClassId == cid
                       select new FeeInformation { FeeAmount = t1.Amount }).SingleOrDefault();

            return ans;
        }

        public IEnumerable<object> getFeeByID(int acid, int cid)
        {
            var ans = from t1 in dc.Fees
                      from t2 in dc.FeeCategories
                      where t1.AcademicYearId == acid && t1.ClassId == cid && t1.FeeCategoryId == t2.FeeCategoryId
                      select new
                      {
                          fid = t2.FeeCategoryName,
                          fee = t1.Amount,
                          ldate = SqlFunctions.DateName("day", t1.LastDate).Trim() + " " + SqlFunctions.DateName("month", t1.LastDate).Remove(3) + ", " + SqlFunctions.DateName("year", t1.LastDate),
                      };
            return ans;
        }

        public ConfigFee getConfigFee(int feeid)
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.FeeCategories
                       from t4 in dc.Fees
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.FeeCategoryId == t4.FeeCategoryId && t4.FeeId == feeid
                       select new ConfigFee
                       {
                           AcYearId = t1.AcademicYearId,
                           AcYear = t1.AcademicYear1,
                           ClassId = t2.ClassId,
                           Class = t2.ClassType,
                           FeeCategoryId = t3.FeeCategoryId,
                           FeeCategory = t3.FeeCategoryName,
                           FeeId=t4.FeeId,
                           Amount = t4.Amount,
                           Tax=t4.Tax,
                           Total=t4.Total,
                           Date = SqlFunctions.DateName("day", t4.LastDate).Trim() + " " + SqlFunctions.DateName("month", t4.LastDate).Remove(3) + ", " + SqlFunctions.DateName("year", t4.LastDate),
                           LastDate = t4.LastDate.Value
                       }).First();
            return ans;
        }

        public ConfigFee getTermConfigFee(int feeid)
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.FeeCategories
                       from t4 in dc.TermFees
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.FeeCategoryId == t4.FeeCategoryId && t4.TermFeeID == feeid
                       select new ConfigFee { AcYearId = t1.AcademicYearId, AcYear = t1.AcademicYear1, ClassId = t2.ClassId, Class = t2.ClassType, FeeCategoryId = t3.FeeCategoryId, FeeCategory = t3.FeeCategoryName, FeeId = t4.FeeId.Value, Amount = t4.Amount,
                                              Date = SqlFunctions.DateName("day", t4.LastDate).Trim() + " " + SqlFunctions.DateName("month", t4.LastDate).Remove(3) + ", " + SqlFunctions.DateName("year", t4.LastDate),
                                              LastDate=t4.LastDate.Value,
                                              Tax = t4.Tax,
                                              Total = t4.Total,
                                              TermId =t4.TermId
                       }).First();
            return ans;
        }

        public eAcademy.Models.FeeDetails getFeeIdByID(int acid, int id, int fid)
        {
            var getfeeid = dc.Fees.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(id) && q.FeeCategoryId.Value.Equals(fid)).Select(s => new eAcademy.Models.FeeDetails { FeeId = s.FeeId }).First();
            return getfeeid;
        }
        public bool Checkfeesalreadypaid(int fid)
        {
            var checkpaid = (from a in dc.Fees
                             from b in dc.FeeCollectionCategories
                             where a.FeeId == fid && b.AcademicYearId == a.AcademicYearId && b.ClassId == a.ClassId && b.FeeCategoryId == a.FeeCategoryId
                             select a).ToList();
            if(checkpaid.Count >0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool deleteAcademicYear(int acid)
        {
            var ans = (from c in dc.FeeCollectionCategories
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {              
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public bool checkFeeConfig(int acid, int fid, int id)
        {
            var ans = dc.Fees.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(id) && q.FeeCategoryId.Value.Equals(fid)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkFeeConfig(int acid, int fid, int cid,Decimal fee,Decimal ServicesTax,Decimal Total, DateTime ldate)
        {
            var ans = dc.Fees.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.FeeCategoryId.Value.Equals(fid) && q.Amount.Value.Equals(fee) && q.LastDate.Value.Equals(ldate) && q.Tax.Value.Equals(ServicesTax) && q.Total.Value.Equals(Total)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
     
        public List<ConfigFee> getFeeConfig()
        {
            var ans = ((from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.FeeCategories
                       from t4 in dc.Fees
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.FeeCategoryId == t4.FeeCategoryId
                       select new ConfigFee 
                       { 
                           AcYearId = t1.AcademicYearId, 
                           AcYear = t1.AcademicYear1,
                           ClassId = t2.ClassId, Class = t2.ClassType, 
                           FeeCategoryId = t3.FeeCategoryId, FeeCategory = t3.FeeCategoryName, 
                           FeeId = t4.FeeId, Amount = t4.Amount, 
                           Tax=t4.Tax,
                           ReFund=t4.ReFundAmount,
                           Total=t4.Total,
                           LastDate = t4.LastDate.Value,
                           Date = SqlFunctions.DateName("day", t4.LastDate).Trim() + " " + SqlFunctions.DateName("month", t4.LastDate).Remove(3) + ", " + SqlFunctions.DateName("year", t4.LastDate),                                            
                       }).ToList()).
                       Select(q=>new ConfigFee

                       {
                           FeeConfigId=QSCrypt.Encrypt(q.FeeId.ToString()),
                           AcYearId = q.AcYearId,
                           AcYear = q.AcYear,
                           ClassId = q.ClassId,
                           Class = q.Class,
                           FeeCategoryId = q.FeeCategoryId,
                           FeeCategory = q.FeeCategory,
                           FeeId = q.FeeId,
                           Amount = q.Amount,
                           Tax=q.Tax,
                           ReFund=Convert.ToDecimal(q.ReFund),
                           Total=q.Total,
                           Term="Click",
                           Date = q.Date,
                           AcademicYear=q.AcYear,
                           LastDates=q.Date
                       }
                       ) .ToList() ;
            return ans;
        }

        public List<ConfigFee> getRefundFeeConfig()
        {
            var ans = ((from t1 in dc.AcademicYears
                        from t2 in dc.Classes
                        from t3 in dc.FeeCategories
                        from t4 in dc.Fees
                        from t5 in dc.FeesRefunds
                        from t6 in dc.TechEmployees
                        where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.FeeCategoryId == t4.FeeCategoryId && t5.FeeId == t4.FeeId && t6.EmployeeRegisterId == t5.EmployeeRegisterId && t5.Status == true
                        select new ConfigFee
                        {
                            AcYearId = t1.AcademicYearId,
                            AcYear = t1.AcademicYear1,
                            ClassId = t2.ClassId,
                            Class = t2.ClassType,
                            FeeCategoryId = t3.FeeCategoryId,
                            FeeCategory = t3.FeeCategoryName,
                            FeeId = t4.FeeId,
                            Amount = t4.Amount,
                            Tax = t4.Tax,
                            ReFund = t5.RefundAmount,
                            Total = t4.Total,
                            LastDate = t4.LastDate.Value,
                            Reason=t5.Reason,
                            TotalAmount = t5.LastAmount,
                            NetAmount=t5.NetAmount,
                            EmployeeRegId=t5.EmployeeRegisterId,
                            EmployeeId=t6.EmployeeId,
                            EmployeeName=t6.EmployeeName +"("+t6.EmployeeId+")",
                            dateofmadified=t5.DateOfModified.Value,
                            Date = SqlFunctions.DateName("day", t5.DateOfModified).Trim() + " " + SqlFunctions.DateName("month", t5.DateOfModified).Remove(3) + ", " + SqlFunctions.DateName("year", t5.DateOfModified),
                        }).ToList()).
                       Select(q => new ConfigFee

                       {
                           FeeConfigId = QSCrypt.Encrypt(q.FeeId.ToString()),
                           AcYearId = q.AcYearId,
                           AcYear = q.AcYear,
                           ClassId = q.ClassId,
                           Class = q.Class,
                           FeeCategoryId = q.FeeCategoryId,
                           FeeCategory = q.FeeCategory,
                           FeeId = q.FeeId,
                           Amount = q.Amount,
                           Tax = q.Tax,
                           ReFund = Convert.ToDecimal(q.ReFund),
                           Total = q.Total,
                           Date = q.dateofmadified.ToString("dd MMM, yyyy"),
                             Reason=q.Reason,
                           TotalAmount = q.TotalAmount,
                            NetAmount=q.NetAmount,
                            EmployeeName=q.EmployeeName,
                            EmployeeId=q.EmployeeId,
                            EmployeeRegId=q.EmployeeRegId,
                           PayableAmount=q.NetAmount,
                           AcademicYear = q.AcYear
                       }
                       ).ToList();
            return ans;
        }

        public void addFeeConfig(int acid, int fid, int cid, Decimal amount, DateTime ldate, Decimal tax, Decimal total)
        {
            int type = 1;
            Fee add = new Fee()
            {
                AcademicYearId = acid,
                FeeCategoryId = fid,
                ClassId = cid,
                PaymentTypeId = type,
                Amount = amount,
                LastDate = ldate,
                Tax = tax,
                Total = total
            };

            dc.Fees.Add(add);
            dc.SaveChanges();
        }

        public void updateFeeConfig(int feeid, int amount)
        {

            var update = dc.Fees.Where(query => query.FeeId.Equals(feeid)).First();
            update.Amount = amount;
            dc.SaveChanges();
        }

        public bool updateFeeConfig(int acid, int cid, int fid, int feeid, Decimal fee, Decimal ServicesTax, Decimal Total, DateTime ldate)
        {

            var checkFeespaid = dc.FeeCollectionCategories.Where(q => q.AcademicYearId == acid && q.ClassId == cid && q.FeeCategoryId == fid).ToList();
            if (checkFeespaid.Count < 1)
            {
                var update = dc.Fees.Where(q => q.FeeId.Equals(feeid)).First();
                update.Amount = fee;
                update.LastDate = ldate;
                update.Tax = ServicesTax;
                update.Total = Total;
                dc.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool checkRefundFeeConfig(int fid, Decimal Total, Decimal refund, Decimal netamt)
        {
            var ans = dc.FeesRefunds.Where(q => q.FeeId.Value.Equals(fid) && q.LastAmount.Value.Equals(Total) && q.RefundAmount.Value.Equals(refund) && q.NetAmount.Value.Equals(netamt)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool updateRefundFeeConfig(int acid, int cid, int fid, int feeid, Decimal Total,Decimal refundamt,Decimal Netamount,string description, int empregid)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var checkFeespaid = dc.Fees.Where(q => q.AcademicYearId == acid && q.ClassId == cid && q.FeeCategoryId == fid && q.FeeId == feeid).FirstOrDefault();
            if (checkFeespaid !=null)
            {
                Decimal amt =Convert.ToDecimal( checkFeespaid.ReFundAmount);
                checkFeespaid.ReFundAmount = amt + refundamt;
                checkFeespaid.Total = Netamount;
                dc.SaveChanges();

                FeesRefund refund = new FeesRefund()
                {
                    FeeId = feeid,
                    LastAmount = Total,
                    RefundAmount = refundamt,
                    NetAmount = Netamount,
                    DateOfModified = date,
                    Reason = description,
                    EmployeeRegisterId=empregid,
                    Status = true
                };
                dc.FeesRefunds.Add(refund);
                dc.SaveChanges();


                return true;
            }
            else
            {
                return false;
            }
        }


        public bool deleteFeeConfig(int feeid)
        {
            var ans = (from a in dc.Fees
                       from b in dc.FeeCollectionCategories
                       where a.FeeId == feeid && b.AcademicYearId == a.AcademicYearId && b.ClassId == a.ClassId && b.FeeCategoryId == a.FeeCategoryId
                       select b).ToList();
            if (ans.Count == 0)
            {
               

                    var delete = dc.Fees.Where(query => query.FeeId == feeid ).FirstOrDefault();
                    if (delete != null)
                    {
                        var list = dc.TermFees.Where(q => q.FeeId == feeid).ToList();

                        if (list.Count > 0)
                        {
                            foreach (var v in list)
                            {
                                var delete1 = dc.TermFees.Where(query => query.FeeId == feeid).FirstOrDefault();
                                dc.TermFees.Remove(delete1);
                                dc.SaveChanges();
                            }
                            dc.Fees.Remove(delete);
                            dc.SaveChanges();
                            return true;
                        }
                        else
                        {
                            dc.Fees.Remove(delete);
                            dc.SaveChanges();
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
               
            }
            else
            {
                return false;
            }
        }

        internal object getallFeesByID(int acid, int cid)
        {

            var ans = (
                  (from t1 in dc.AcademicYears
                   from t2 in dc.Classes
                   from t3 in dc.FeeCategories
                   from t4 in dc.Fees

                   where t1.AcademicYearId == t4.AcademicYearId &&
                         t2.ClassId == cid &&
                         t3.FeeCategoryId == t4.FeeCategoryId &&
                         t4.AcademicYearId == acid && t4.ClassId == cid
                   select new FeeStructure
                   {
                       FeeCategory = t3.FeeCategoryName,
                       FeeCategoryId = t3.FeeCategoryId,
                       PaymentTypeId = t4.PaymentTypeId.Value,
                       TermId = 0,
                       Amount = t4.Amount,
                       Ldate = SqlFunctions.DateName("day", t4.LastDate).Trim() + " " + SqlFunctions.DateName("month", t4.LastDate).Remove(3) + ", " + SqlFunctions.DateName("year", t4.LastDate),
                       PaymentType = "Annual",
                       FeeId = t4.FeeId,
                       TermFeeId = 0,
                       ServicesTax = t4.Tax,
                       Total = t4.Total
                   })
                .Union
                (from t1 in dc.AcademicYears
                 from t2 in dc.Classes
                 from t3 in dc.FeeCategories
                 from t4 in dc.TermFees
                 from t5 in dc.Terms

                 where t1.AcademicYearId == t4.AcademicYearId &&
                         t2.ClassId == cid &&
                         t3.FeeCategoryId == t4.FeeCategoryId &&
                         t4.AcademicYearId == acid && t4.ClassId == cid &&
                         t4.TermId == t5.TermId

                 select new FeeStructure
                 {
                     FeeCategory = t3.FeeCategoryName,
                     FeeCategoryId = t3.FeeCategoryId,
                     PaymentTypeId = t4.PaymentTypeId.Value,
                     TermId = t4.TermId,
                     Amount = t4.Amount,
                     Ldate = SqlFunctions.DateName("day", t4.LastDate).Trim() + " " + SqlFunctions.DateName("month", t4.LastDate).Remove(3) + ", " + SqlFunctions.DateName("year", t4.LastDate),
                     PaymentType = t5.TermName,
                     FeeId = t4.FeeId.Value,
                     TermFeeId = t4.TermFeeID,
                     ServicesTax = t4.Tax,
                     Total = t4.Total
                 })

                    ).OrderBy(q => q.FeeCategoryId).ToList();
            return ans;
        }

        internal List<FeeStructure> getPaidFeesByID(int acid,int cid, int secid,int stdid)
        {
            var ans = (
                    (from t1 in dc.FeeCollectionCategories
                     where t1.AcademicYearId==acid && t1.ClassId==cid && t1.SectionId==secid &&  t1.StudentRegId == stdid && t1.PaymentTypeId == 0
                     select new FeeStructure
                     {
                         FeeCategoryId = t1.FeeCategoryId.Value,
                         PaymentTypeId = t1.PaymentTypeId.Value,
                         CollectedId = t1.CollectionId
                     })
                .Union
                    (from t1 in dc.FeeCollectionCategories
                     from t2 in dc.Terms
                     where   t1.AcademicYearId==acid && t1.ClassId==cid && t1.SectionId==secid && t1.StudentRegId == stdid && t1.PaymentTypeId == t2.TermId
                     select new FeeStructure
                     {
                        FeeCategoryId = t1.FeeCategoryId.Value,
                        PaymentTypeId = t1.PaymentTypeId.Value,
                        CollectedId = t1.CollectionId
                     })
                ).ToList();
            return ans;
        }
        //jegadeesh

        public List<FeeInformation> Feedetails(int yid, int cid)
        {
            var ans = (from a in dc.Fees
                       from b in dc.FeeCategories
                       where a.AcademicYearId == yid && a.ClassId == cid && a.FeeCategoryId == b.FeeCategoryId
                       select new FeeInformation
                       {
                           FeeCategory = b.FeeCategoryName,
                           FeeAmount = a.Amount
                       }).ToList();
            return ans;

        }
        public List<Fee> GetJsonAnnualFee(int year, int clas)
        {
            var ans = (from a in dc.Fees
                       where a.AcademicYearId == year && a.ClassId == clas
                       select a).ToList();
            return ans;

        }

        public IEnumerable<object> GetRefundfeeCategory(int aid,int cid)
        {
            var ans = (from a in dc.Fees
                       from b in dc.FeeCategories
                       where a.AcademicYearId == aid && a.ClassId == cid && a.ReFundAmount != null && b.FeeCategoryId == a.FeeCategoryId
                       select new
                       {
                           FeeCategoryId = b.FeeCategoryId,
                           FeecategoryName = b.FeeCategoryName
                       }).ToList();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        internal bool checkTermFeeConfig(int acid, int fid, int cid, Decimal fee,Decimal tax,Decimal total, DateTime ldate)
        {
            var ans = dc.TermFees.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.FeeCategoryId.Value.Equals(fid) && q.Amount.Value.Equals(fee) && q.Tax.Value.Equals(tax) && q.Total.Value.Equals(total) && q.LastDate.Value.Equals(ldate)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal bool updateTermFeeConfig(int acid, int cid, int fid, int feeid, Decimal fee, Decimal tax, Decimal total, DateTime ldate)
        {
            var checkFeespaid = dc.FeeCollectionCategories.Where(q => q.AcademicYearId == acid && q.ClassId == cid && q.FeeCategoryId == fid).ToList();
            if (checkFeespaid.Count < 1)
            {
                var update = dc.TermFees.Where(q => q.TermFeeID.Equals(feeid)).First();
                update.Amount = fee;
                update.Tax = tax;
                update.Total = total;
                update.LastDate = ldate;
                dc.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}