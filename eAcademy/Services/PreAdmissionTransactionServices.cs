﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace eAcademy.Services
{
    public class PreAdmissionTransactionServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public int GetNoOfStudent(string email)
        {
            var ans = (from a in dc.PreAdmissionPrimaryUserRegisters
                       from b in dc.PreAdmissionTransactions
                       where a.PrimaryUserEmail.Equals(email) && b.PrimaryUserAdmissionId == a.PrimaryUserAdmissionId
                       select b).ToList();
            return ans.Count;
        }

        public PreAdmissionTransaction GetPrimaryUser(int PrimaryUserAdmissionId, int StudentId)
        {
            var ans = dc.PreAdmissionTransactions.Where(q => q.PrimaryUserAdmissionId == PrimaryUserAdmissionId && q.StudentAdmissionId == StudentId).FirstOrDefault();
            return ans;
        }

        public int? AddEmailExistDetailsToNewtable(string uname)
        {
            List<M_ParentSibling> si = new List<M_ParentSibling>();
            int? OnLineRegId = 0; int? PrimaryUserRegId = 0; int? FatherRegId = 0; int? MotherRegId = 0; int? GuardianRegId = 0;

            var old = (from a in dc.PrimaryUserRegisters
                       from b in dc.AdmissionTransactions
                       where a.PrimaryUserEmail.Equals(uname) && b.PrimaryUserRegisterId == a.PrimaryUserRegisterId
                       select b).OrderByDescending(q => q.StudentRegisterId).FirstOrDefault();
            if (old.PrimaryUserRegisterId != null)
            {
                var getrow = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserRegisterId == old.PrimaryUserRegisterId).FirstOrDefault();
                if (getrow != null)
                {
                    PreAdmissionOnLineRegister OR = new PreAdmissionOnLineRegister()
                    {
                        Username = getrow.PrimaryUserEmail,
                        Passwords = getrow.PrimaryUserPwd
                    };
                    dc.PreAdmissionOnLineRegisters.Add(OR);
                    dc.SaveChanges();
                    OnLineRegId = OR.OnlineRegisterId;
                    AddOldStudentPrimaryuserDetailsToNewStudent(OnLineRegId, getrow, old);                    
                }
            }
            return OnLineRegId;
        }
        public int? AddMobileExistDetailsToNewtable(long? uname)
        {
            
            List<M_ParentSibling> si = new List<M_ParentSibling>();
            int? OnLineRegId = 0; int? PrimaryUserRegId = 0; int? FatherRegId = 0; int? MotherRegId = 0; int? GuardianRegId = 0;

            var old = (from a in dc.PrimaryUserRegisters
                       from b in dc.AdmissionTransactions
                       where a.PrimaryUserContactNo.Value.Equals(uname) && b.PrimaryUserRegisterId == a.PrimaryUserRegisterId
                       select b).OrderByDescending(q => q.StudentRegisterId).FirstOrDefault();
            if (old.PrimaryUserRegisterId != null)
            {
                var getrow = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserRegisterId == old.PrimaryUserRegisterId).FirstOrDefault();
                if (getrow != null)
                {
                    string contact = getrow.PrimaryUserContactNo.ToString();
                    PreAdmissionOnLineRegister OR = new PreAdmissionOnLineRegister()
                    {
                        Username = contact,
                        Passwords = getrow.PrimaryUserPwd
                    };
                    dc.PreAdmissionOnLineRegisters.Add(OR);
                    dc.SaveChanges();
                    OnLineRegId = OR.OnlineRegisterId;
                    AddOldStudentPrimaryuserDetailsToNewStudent(OnLineRegId, getrow, old);
                }
            }
            return OnLineRegId;
        }

        public void AddOldStudentPrimaryuserDetailsToNewStudent(int? OnLineRegId,PrimaryUserRegister getrow,AdmissionTransaction old)
        {
            List<M_ParentSibling> si = new List<M_ParentSibling>();
            int? PrimaryUserRegId = 0; int? FatherRegId = 0; int? MotherRegId = 0; int? GuardianRegId = 0;
            if (OnLineRegId != null)
            {
                PreAdmissionPrimaryUserRegister PU = new PreAdmissionPrimaryUserRegister()
                {
                    OnlineRegisterId = OnLineRegId,
                    PrimaryUser = getrow.PrimaryUser,
                    PrimaryUserName = getrow.PrimaryUserName,
                    PrimaryUserEmail = getrow.PrimaryUserEmail,
                    CompanyAddress1 = getrow.CompanyAddress1,
                    CompanyAddress2 = getrow.CompanyAddress2,
                    CompanyCity = getrow.CompanyCity,
                    CompanyState = getrow.CompanyState,
                    CompanyCountry = getrow.CompanyCountry,
                    CompanyPostelcode = getrow.CompanyPostelcode,
                    CompanyContact = getrow.CompanyContact,
                    PrimaryUserPwd = getrow.PrimaryUserPwd,
                    PrimaryUserContactNo=getrow.PrimaryUserContactNo
                };
                dc.PreAdmissionPrimaryUserRegisters.Add(PU);
                dc.SaveChanges();
                PrimaryUserRegId = PU.PrimaryUserAdmissionId;

                if (PrimaryUserRegId != null)
                {

                    if (old.FatherRegisterId != null)
                    {
                        var fatherrow = dc.FatherRegisters.Where(q => q.FatherRegisterId == old.FatherRegisterId).FirstOrDefault();

                        PreAdmissionFatherRegister PFR = new PreAdmissionFatherRegister()
                        {
                            FatherFirstName = fatherrow.FatherFirstName,
                            FatherMiddleName = fatherrow.FatherMiddleName,
                            FatherLastName = fatherrow.FatherLastName,
                            FatherDOB = fatherrow.FatherDOB,
                            FatherQualification = fatherrow.FatherQualification,
                            FatherOccupation = fatherrow.FatherOccupation,
                            FatherMobileNo = fatherrow.FatherMobileNo,
                            FatherEmail = fatherrow.FatherEmail
                        };
                        dc.PreAdmissionFatherRegisters.Add(PFR);
                        dc.SaveChanges();
                        FatherRegId = PFR.FatherAdmissionId;
                    }
                    if (old.MotherRegisterId != null)
                    {
                        var motherrow = dc.MotherRegisters.Where(q => q.MotherRegisterId == old.MotherRegisterId).FirstOrDefault();

                        PreAdmissionMotherRegister PMR = new PreAdmissionMotherRegister()
                        {
                            MotherFirstname = motherrow.MotherFirstname,
                            MotherMiddleName = motherrow.MotherMiddleName,
                            MotherLastName = motherrow.MotherLastName,
                            MotherDOB = motherrow.MotherDOB,
                            MotherQualification = motherrow.MotherQualification,
                            MotherOccupation = motherrow.MotherOccupation,
                            MotherMobileNo = motherrow.MotherMobileNo,
                            MotherEmail = motherrow.MotherEmail
                        };
                        dc.PreAdmissionMotherRegisters.Add(PMR);
                        dc.SaveChanges();
                        MotherRegId = PMR.MotherAdmissionId;
                    }
                    if (old.GuardianRegisterId != null)
                    {
                        var guardianrow = dc.GuardianRegisters.Where(q => q.GuardianRegisterId == old.GuardianRegisterId).FirstOrDefault();

                        PreAdmissionGuardianRegister PGR = new PreAdmissionGuardianRegister()
                        {
                            GuardianFirstname = guardianrow.GuardianFirstname,
                            GuardianMiddleName = guardianrow.GuardianMiddleName,
                            GuardianLastName = guardianrow.GuardianLastName,
                            GuardianDOB = guardianrow.GuardianDOB,
                            GuardianQualification = guardianrow.GuardianQualification,
                            GuardianOccupation = guardianrow.GuardianOccupation,
                            GuardianMobileNo = guardianrow.GuardianMobileNo,
                            GuardianEmail = guardianrow.GuardianEmail
                        };
                        dc.PreAdmissionGuardianRegisters.Add(PGR);
                        dc.SaveChanges();
                        GuardianRegId = PGR.GuardianAdmissionId;
                    }
                    var oldPrimaryUser = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserRegisterId == old.PrimaryUserRegisterId).FirstOrDefault().PrimaryUser;

                    if (oldPrimaryUser == "Father")
                    {
                        var Sbling = (from a in dc.AdmissionTransactions
                                      from f in dc.Students
                                      where a.PrimaryUserRegisterId == old.PrimaryUserRegisterId && a.FatherRegisterId == old.FatherRegisterId &&
                                      f.StudentRegisterId == a.StudentRegisterId
                                      select new M_ParentSibling
                                      {
                                          Studentid = f.StudentId,
                                          StudentRegId = f.StudentRegisterId
                                      }).Distinct().ToList();
                        si = Sbling;

                    }
                    else if (oldPrimaryUser == "Mother")
                    {
                        var Sbling = (from a in dc.AdmissionTransactions
                                      from f in dc.Students
                                      where a.PrimaryUserRegisterId == old.PrimaryUserRegisterId && a.MotherRegisterId == old.MotherRegisterId &&
                                      f.StudentRegisterId == a.StudentRegisterId
                                      select new M_ParentSibling
                                      {
                                          Studentid = f.StudentId,
                                          StudentRegId = f.StudentRegisterId
                                      }).Distinct().ToList();
                        si = Sbling;
                    }
                    else if (oldPrimaryUser == "Guardian")
                    {

                        var Sbling = (from a in dc.AdmissionTransactions
                                      from f in dc.Students
                                      where a.PrimaryUserRegisterId == old.PrimaryUserRegisterId && a.GuardianRegisterId == old.GuardianRegisterId &&
                                      f.StudentRegisterId == a.StudentRegisterId
                                      select new M_ParentSibling
                                      {
                                          Studentid = f.StudentId,
                                          StudentRegId = f.StudentRegisterId
                                      }).Distinct().ToList();
                        si = Sbling;
                    }

                    foreach (var v in si)
                    {
                        PreAdmissionParentSibling sibling = new PreAdmissionParentSibling()
                        {
                            OnlineRegisterId = OnLineRegId,
                            SiblingStudentId = v.Studentid,
                            SblingStudentRegId = v.StudentRegId
                        };
                        dc.PreAdmissionParentSiblings.Add(sibling);
                        dc.SaveChanges();
                    }

                    string primaryuser = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserAdmissionId == PrimaryUserRegId).FirstOrDefault().PrimaryUser;
                    if (primaryuser == "Father")
                    {
                        if (MotherRegId == 0 && GuardianRegId == 0)
                        {
                            PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                            {
                                PrimaryUserAdmissionId = PrimaryUserRegId,
                                FatherAdmissionId = FatherRegId,
                            };
                            dc.PreAdmissionTransactions.Add(PAT);
                            dc.SaveChanges();
                        }
                        else if (MotherRegId != 0 && GuardianRegId == 0)
                        {
                            PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                            {
                                PrimaryUserAdmissionId = PrimaryUserRegId,
                                FatherAdmissionId = FatherRegId,
                                MotherAdmissionId = MotherRegId,
                            };
                            dc.PreAdmissionTransactions.Add(PAT);
                            dc.SaveChanges();
                        }
                        else if (MotherRegId == 0 && GuardianRegId != 0)
                        {
                            PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                            {
                                PrimaryUserAdmissionId = PrimaryUserRegId,
                                FatherAdmissionId = FatherRegId,
                                GuardianAdmissionId = GuardianRegId,
                            };
                            dc.PreAdmissionTransactions.Add(PAT);
                            dc.SaveChanges();
                        }
                        else
                        {
                            PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                            {
                                PrimaryUserAdmissionId = PrimaryUserRegId,
                                FatherAdmissionId = FatherRegId,
                                MotherAdmissionId = MotherRegId,
                                GuardianAdmissionId = GuardianRegId
                            };
                            dc.PreAdmissionTransactions.Add(PAT);
                            dc.SaveChanges();
                        }
                    }

                    else if (primaryuser == "Mother")
                    {
                        if (FatherRegId == 0 && GuardianRegId == 0)
                        {
                            PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                            {
                                PrimaryUserAdmissionId = PrimaryUserRegId,
                                MotherAdmissionId = MotherRegId,
                            };
                            dc.PreAdmissionTransactions.Add(PAT);
                            dc.SaveChanges();
                        }
                        else if (FatherRegId != 0 && GuardianRegId == 0)
                        {
                            PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                            {
                                PrimaryUserAdmissionId = PrimaryUserRegId,
                                FatherAdmissionId = FatherRegId,
                                MotherAdmissionId = MotherRegId,
                            };
                            dc.PreAdmissionTransactions.Add(PAT);
                            dc.SaveChanges();
                        }
                        else if (FatherRegId == 0 && GuardianRegId != 0)
                        {
                            PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                            {
                                PrimaryUserAdmissionId = PrimaryUserRegId,
                                MotherAdmissionId = MotherRegId,
                                GuardianAdmissionId = GuardianRegId,
                            };
                            dc.PreAdmissionTransactions.Add(PAT);
                            dc.SaveChanges();
                        }
                        else
                        {
                            PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                            {
                                PrimaryUserAdmissionId = PrimaryUserRegId,
                                FatherAdmissionId = FatherRegId,
                                MotherAdmissionId = MotherRegId,
                                GuardianAdmissionId = GuardianRegId
                            };
                            dc.PreAdmissionTransactions.Add(PAT);
                            dc.SaveChanges();
                        }
                    }

                    else if (primaryuser == "Guardian")
                    {
                        if (FatherRegId == 0 && MotherRegId == 0)
                        {
                            PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                            {
                                PrimaryUserAdmissionId = PrimaryUserRegId,
                                GuardianAdmissionId = GuardianRegId,

                            };
                            dc.PreAdmissionTransactions.Add(PAT);
                            dc.SaveChanges();
                        }
                        else if (FatherRegId != 0 && MotherRegId == 0)
                        {
                            PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                            {
                                PrimaryUserAdmissionId = PrimaryUserRegId,
                                FatherAdmissionId = FatherRegId,
                                GuardianAdmissionId = GuardianRegId,
                            };
                            dc.PreAdmissionTransactions.Add(PAT);
                            dc.SaveChanges();
                        }
                        else if (FatherRegId == 0 && MotherRegId != 0)
                        {
                            PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                            {
                                PrimaryUserAdmissionId = PrimaryUserRegId,
                                MotherAdmissionId = MotherRegId,
                                GuardianAdmissionId = GuardianRegId,

                            };
                            dc.PreAdmissionTransactions.Add(PAT);
                            dc.SaveChanges();
                        }
                        else
                        {
                            PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                            {
                                PrimaryUserAdmissionId = PrimaryUserRegId,
                                FatherAdmissionId = FatherRegId,
                                MotherAdmissionId = MotherRegId,
                                GuardianAdmissionId = GuardianRegId
                            };
                            dc.PreAdmissionTransactions.Add(PAT);
                            dc.SaveChanges();
                        }
                    }
                }
            }
        }

        public void AddPrimaryUserandStudentId(int PrimaryUserAdmissionId, int StudentId)
        {
            PreAdmissionTransaction trans = new PreAdmissionTransaction()
            {
                PrimaryUserAdmissionId = PrimaryUserAdmissionId,
                StudentAdmissionId = StudentId

            };
            dc.PreAdmissionTransactions.Add(trans);
            dc.SaveChanges();
        }
        public void UpdateStudentId(int PrimaryUserAdmissionId, int StudentId)
        {
            var get = dc.PreAdmissionTransactions.Where(q => q.PrimaryUserAdmissionId == PrimaryUserAdmissionId).FirstOrDefault();
            if (get != null)
            {
                if (get.StudentAdmissionId == null)
                {
                    get.StudentAdmissionId = StudentId;
                    dc.SaveChanges();
                }
            }
        }

        public void UpdateFathertId(int PrimaryUserAdmissionId, int StudentId, int fatherid)
        {
            var get = dc.PreAdmissionTransactions.Where(q => q.PrimaryUserAdmissionId == PrimaryUserAdmissionId && q.StudentAdmissionId == StudentId).FirstOrDefault();
            if (get != null)
            {
                get.FatherAdmissionId = fatherid;
                dc.SaveChanges();
            }
        }
        public void UpdateMothertId(int PrimaryUserAdmissionId, int StudentId, int motherid)
        {
            var get = dc.PreAdmissionTransactions.Where(q => q.PrimaryUserAdmissionId == PrimaryUserAdmissionId && q.StudentAdmissionId == StudentId).FirstOrDefault();
            if (get != null)
            {
                get.MotherAdmissionId = motherid;
                dc.SaveChanges();
            }
        }
        public void UpdateGuardiantId(int PrimaryUserAdmissionId, int StudentId, int guardianid)
        {
            var get = dc.PreAdmissionTransactions.Where(q => q.PrimaryUserAdmissionId == PrimaryUserAdmissionId && q.StudentAdmissionId == StudentId).FirstOrDefault();
            if (get != null)
            {
                get.GuardianAdmissionId = guardianid;
                dc.SaveChanges();
            }
        }

        public List<PreAdmissionTransaction> GetRemainingStudent(int PrimaryUserId, int StudentId)
        {
            var ans = dc.PreAdmissionTransactions.Where(q => q.PrimaryUserAdmissionId == PrimaryUserId && q.StudentAdmissionId != StudentId).Distinct().ToList();
            return ans;
        }

        public PreAdmissionTransaction FindPrimaryuser(int PrimaryUserAdmissionId)
        {
            var ans = dc.PreAdmissionTransactions.Where(q => q.PrimaryUserAdmissionId == PrimaryUserAdmissionId).OrderByDescending(q=>q.PreAdmissionTransactionId).FirstOrDefault();
            return ans;
        }
        public PreAdmissionTransaction FindPrimaryUserId(int studentid)
        {
            var ans = dc.PreAdmissionTransactions.Where(q => q.StudentAdmissionId == studentid).FirstOrDefault();
            return ans;
        }

        public PreAdmissionStudentRegister CheckEmailuserExit(string Offlineid, string email)
        {
        var ans= (from a in dc.PreAdmissionStudentRegisters
                      from b in dc.PreAdmissionPrimaryUserRegisters
                      from c in dc.PreAdmissionTransactions
                      where a.OfflineApplicationID == Offlineid && b.PrimaryUserEmail == email && c.StudentAdmissionId == a.StudentAdmissionId && c.PrimaryUserAdmissionId ==b.PrimaryUserAdmissionId
                      select a).FirstOrDefault();
        return ans;
        }
        public PreAdmissionStudentRegister CheckMobileluserExit(string Offlineid, string mno)
        {
            long contact = Convert.ToInt64(mno);
            var ans = (from a in dc.PreAdmissionStudentRegisters
                       from b in dc.PreAdmissionPrimaryUserRegisters
                       from c in dc.PreAdmissionTransactions
                       where a.OfflineApplicationID == Offlineid && b.PrimaryUserContactNo == contact && c.StudentAdmissionId == a.StudentAdmissionId && c.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId
                       select a).FirstOrDefault();
            return ans;
        }
        //offline
        public string AddExistDetailsToNewtableOffline(string uname, string OfflineApplicationID)
        {
            var old = new AdmissionTransaction();
            string MatchEmailPattern =
                       @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
            if (Regex.IsMatch(uname, MatchEmailPattern))
            {
                old = (from a in dc.PrimaryUserRegisters
                           from b in dc.AdmissionTransactions
                           where a.PrimaryUserEmail.Equals(uname) && b.PrimaryUserRegisterId == a.PrimaryUserRegisterId
                           select b).OrderByDescending(q => q.StudentRegisterId).FirstOrDefault();
            }
            else
            {
                long contact = Convert.ToInt64(uname);
                 old = (from a in dc.PrimaryUserRegisters
                           from b in dc.AdmissionTransactions
                        where a.PrimaryUserContactNo.Value.Equals(contact) && b.PrimaryUserRegisterId == a.PrimaryUserRegisterId
                           select b).OrderByDescending(q => q.StudentRegisterId).FirstOrDefault();
            }
            List<M_ParentSibling> si = new List<M_ParentSibling>();
           int? PrimaryUserRegId = 0; int? FatherRegId = 0; int? MotherRegId = 0; int? GuardianRegId = 0;

           
            if (old.PrimaryUserRegisterId != null)
            {
                var getrow = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserRegisterId == old.PrimaryUserRegisterId).FirstOrDefault();
                if (getrow != null)
                {
                        PreAdmissionPrimaryUserRegister PU = new PreAdmissionPrimaryUserRegister()
                        {
                            PrimaryUser = getrow.PrimaryUser,
                            PrimaryUserName = getrow.PrimaryUserName,
                            PrimaryUserEmail = getrow.PrimaryUserEmail,
                            CompanyAddress1 = getrow.CompanyAddress1,
                            CompanyAddress2 = getrow.CompanyAddress2,
                            CompanyCity = getrow.CompanyCity,
                            CompanyState = getrow.CompanyState,
                            CompanyCountry = getrow.CompanyCountry,
                            CompanyPostelcode = getrow.CompanyPostelcode,
                            CompanyContact = getrow.CompanyContact,
                            EmergencyContactPersonName = getrow.EmergencyContactPersonName,
                            EmergencyContactNumber = getrow.EmergencyContactNumber,
                            ContactPersonRelationship = getrow.ContactPersonRelationship,
                            PrimaryUserPwd = getrow.PrimaryUserPwd,
                            PrimaryUserContactNo=getrow.PrimaryUserContactNo
                        };
                        dc.PreAdmissionPrimaryUserRegisters.Add(PU);
                        dc.SaveChanges();
                        PrimaryUserRegId = PU.PrimaryUserAdmissionId;

                        if (PrimaryUserRegId != null)
                        {
                            if (old.FatherRegisterId != null)
                            {
                                var fatherrow = dc.FatherRegisters.Where(q => q.FatherRegisterId == old.FatherRegisterId).FirstOrDefault();
                                PreAdmissionFatherRegister PFR = new PreAdmissionFatherRegister()
                                {
                                    FatherFirstName = fatherrow.FatherFirstName,
                                    FatherMiddleName = fatherrow.FatherMiddleName,
                                    FatherLastName = fatherrow.FatherLastName,
                                    FatherDOB = fatherrow.FatherDOB,
                                    FatherQualification = fatherrow.FatherQualification,
                                    FatherOccupation = fatherrow.FatherOccupation,
                                    FatherMobileNo = fatherrow.FatherMobileNo,
                                    FatherEmail = fatherrow.FatherEmail
                                };
                                dc.PreAdmissionFatherRegisters.Add(PFR);
                                dc.SaveChanges();
                                FatherRegId = PFR.FatherAdmissionId;
                            }
                            if (old.MotherRegisterId != null)
                            {
                                var motherrow = dc.MotherRegisters.Where(q => q.MotherRegisterId == old.MotherRegisterId).FirstOrDefault();
                                PreAdmissionMotherRegister PMR = new PreAdmissionMotherRegister()
                                {
                                    MotherFirstname = motherrow.MotherFirstname,
                                    MotherMiddleName = motherrow.MotherMiddleName,
                                    MotherLastName = motherrow.MotherLastName,
                                    MotherDOB = motherrow.MotherDOB,
                                    MotherQualification = motherrow.MotherQualification,
                                    MotherOccupation = motherrow.MotherOccupation,
                                    MotherMobileNo = motherrow.MotherMobileNo,
                                    MotherEmail = motherrow.MotherEmail
                                };
                                dc.PreAdmissionMotherRegisters.Add(PMR);
                                dc.SaveChanges();
                                MotherRegId = PMR.MotherAdmissionId;
                            }
                            if (old.GuardianRegisterId != null)
                            {
                                var guardianrow = dc.GuardianRegisters.Where(q => q.GuardianRegisterId == old.GuardianRegisterId).FirstOrDefault();
                                PreAdmissionGuardianRegister PGR = new PreAdmissionGuardianRegister()
                                {
                                    GuardianFirstname = guardianrow.GuardianFirstname,
                                    GuardianMiddleName = guardianrow.GuardianMiddleName,
                                    GuardianLastName = guardianrow.GuardianLastName,
                                    GuardianDOB = guardianrow.GuardianDOB,
                                    GuardianQualification = guardianrow.GuardianQualification,
                                    GuardianOccupation = guardianrow.GuardianOccupation,
                                    GuardianMobileNo = guardianrow.GuardianMobileNo,
                                    GuardianEmail = guardianrow.GuardianEmail
                                };
                                dc.PreAdmissionGuardianRegisters.Add(PGR);
                                dc.SaveChanges();
                                GuardianRegId = PGR.GuardianAdmissionId;
                            }
                            var oldPrimaryUser = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserRegisterId == old.PrimaryUserRegisterId).FirstOrDefault().PrimaryUser;

                            if (oldPrimaryUser == "Father")
                            {
                                var Sbling = (from a in dc.AdmissionTransactions

                                              from f in dc.Students

                                              where a.PrimaryUserRegisterId == old.PrimaryUserRegisterId && a.FatherRegisterId == old.FatherRegisterId &&
                                              f.StudentRegisterId == a.StudentRegisterId
                                              select new M_ParentSibling
                                              {
                                                  Studentid = f.StudentId,
                                                  StudentRegId = f.StudentRegisterId
                                              }).Distinct().ToList();
                                si = Sbling;
                            }
                            else if (oldPrimaryUser == "Mother")
                            {
                                var Sbling = (from a in dc.AdmissionTransactions

                                              from f in dc.Students

                                              where a.PrimaryUserRegisterId == old.PrimaryUserRegisterId && a.MotherRegisterId == old.MotherRegisterId &&
                                              f.StudentRegisterId == a.StudentRegisterId
                                              select new M_ParentSibling
                                              {
                                                  Studentid = f.StudentId,
                                                  StudentRegId = f.StudentRegisterId
                                              }).Distinct().ToList();
                                si = Sbling;
                            }
                            else if (oldPrimaryUser == "Guardian")
                            {
                                var Sbling = (from a in dc.AdmissionTransactions

                                              from f in dc.Students

                                              where a.PrimaryUserRegisterId == old.PrimaryUserRegisterId && a.GuardianRegisterId == old.GuardianRegisterId &&
                                              f.StudentRegisterId == a.StudentRegisterId
                                              select new M_ParentSibling
                                              {
                                                  Studentid = f.StudentId,
                                                  StudentRegId = f.StudentRegisterId
                                              }).Distinct().ToList();
                                si = Sbling;
                            }

                            foreach (var v in si)
                            {
                                PreAdmissionParentSibling sibling = new PreAdmissionParentSibling()
                                {
                                    OfflineApplicationID = OfflineApplicationID,
                                    SiblingStudentId = v.Studentid,
                                    SblingStudentRegId = v.StudentRegId
                                };
                                dc.PreAdmissionParentSiblings.Add(sibling);
                                dc.SaveChanges();
                            }

                            string primaryuser = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserAdmissionId == PrimaryUserRegId).FirstOrDefault().PrimaryUser;
                            if (primaryuser == "Father")
                            {
                                if (MotherRegId == 0 && GuardianRegId == 0)
                                {
                                    PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                    {
                                        PrimaryUserAdmissionId = PrimaryUserRegId,
                                        FatherAdmissionId = FatherRegId,

                                    };
                                    dc.PreAdmissionTransactions.Add(PAT);
                                    dc.SaveChanges();
                                }
                                else if (MotherRegId != 0 && GuardianRegId == 0)
                                {
                                    PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                    {
                                        PrimaryUserAdmissionId = PrimaryUserRegId,
                                        FatherAdmissionId = FatherRegId,
                                        MotherAdmissionId = MotherRegId,
                                    };
                                    dc.PreAdmissionTransactions.Add(PAT);
                                    dc.SaveChanges();
                                }
                                else if (MotherRegId == 0 && GuardianRegId != 0)
                                {
                                    PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                    {
                                        PrimaryUserAdmissionId = PrimaryUserRegId,
                                        FatherAdmissionId = FatherRegId,
                                        GuardianAdmissionId = GuardianRegId,

                                    };
                                    dc.PreAdmissionTransactions.Add(PAT);
                                    dc.SaveChanges();
                                }
                                else
                                {
                                    PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                    {
                                        PrimaryUserAdmissionId = PrimaryUserRegId,
                                        FatherAdmissionId = FatherRegId,
                                        MotherAdmissionId = MotherRegId,
                                        GuardianAdmissionId = GuardianRegId
                                    };
                                    dc.PreAdmissionTransactions.Add(PAT);
                                    dc.SaveChanges();
                                }
                            }

                            else if (primaryuser == "Mother")
                            {
                                if (FatherRegId == 0 && GuardianRegId == 0)
                                {
                                    PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                    {
                                        PrimaryUserAdmissionId = PrimaryUserRegId,
                                        MotherAdmissionId = MotherRegId,
                                    };
                                    dc.PreAdmissionTransactions.Add(PAT);
                                    dc.SaveChanges();
                                }
                                else if (FatherRegId != 0 && GuardianRegId == 0)
                                {
                                    PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                    {
                                        PrimaryUserAdmissionId = PrimaryUserRegId,
                                        FatherAdmissionId = FatherRegId,
                                        MotherAdmissionId = MotherRegId,
                                    };
                                    dc.PreAdmissionTransactions.Add(PAT);
                                    dc.SaveChanges();
                                }
                                else if (FatherRegId == 0 && GuardianRegId != 0)
                                {
                                    PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                    {
                                        PrimaryUserAdmissionId = PrimaryUserRegId,
                                        MotherAdmissionId = MotherRegId,
                                        GuardianAdmissionId = GuardianRegId,
                                    };
                                    dc.PreAdmissionTransactions.Add(PAT);
                                    dc.SaveChanges();
                                }
                                else
                                {
                                    PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                    {
                                        PrimaryUserAdmissionId = PrimaryUserRegId,
                                        FatherAdmissionId = FatherRegId,
                                        MotherAdmissionId = MotherRegId,
                                        GuardianAdmissionId = GuardianRegId
                                    };
                                    dc.PreAdmissionTransactions.Add(PAT);
                                    dc.SaveChanges();
                                }
                            }

                            else if (primaryuser == "Guardian")
                            {
                                if (FatherRegId == 0 && MotherRegId == 0)
                                {
                                    PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                    {
                                        PrimaryUserAdmissionId = PrimaryUserRegId,
                                        GuardianAdmissionId = GuardianRegId,
                                    };
                                    dc.PreAdmissionTransactions.Add(PAT);
                                    dc.SaveChanges();
                                }
                                else if (FatherRegId != 0 && MotherRegId == 0)
                                {
                                    PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                    {
                                        PrimaryUserAdmissionId = PrimaryUserRegId,
                                        FatherAdmissionId = FatherRegId,
                                        GuardianAdmissionId = GuardianRegId,
                                    };
                                    dc.PreAdmissionTransactions.Add(PAT);
                                    dc.SaveChanges();
                                }
                                else if (FatherRegId == 0 && MotherRegId != 0)
                                {
                                    PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                    {
                                        PrimaryUserAdmissionId = PrimaryUserRegId,
                                        MotherAdmissionId = MotherRegId,
                                        GuardianAdmissionId = GuardianRegId,
                                    };
                                    dc.PreAdmissionTransactions.Add(PAT);
                                    dc.SaveChanges();
                                }
                                else
                                {
                                    PreAdmissionTransaction PAT = new PreAdmissionTransaction()
                                    {
                                        PrimaryUserAdmissionId = PrimaryUserRegId,
                                        FatherAdmissionId = FatherRegId,
                                        MotherAdmissionId = MotherRegId,
                                        GuardianAdmissionId = GuardianRegId
                                    };
                                    dc.PreAdmissionTransactions.Add(PAT);
                                    dc.SaveChanges();
                                }
                            }
                        }
                }
            }
            return OfflineApplicationID;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}