﻿using eAcademy.DataModel;
using System;
using System.Web;
using System.Web.Mvc;

namespace eAcademy.Services
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class Guardian_CheckSessionAttribute : ActionFilterAttribute
    {

        Data db = new Data();
        EacademyEntities ee = new EacademyEntities();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower();
            if (!controllerName.Contains("home"))
            {
                HttpSessionStateBase session = filterContext.HttpContext.Session;
                var userG = session["GuardianRegId_SessionAttribute"];
                if (((userG == null) && (!session.IsNewSession)) || (session.IsNewSession) || (null == filterContext.HttpContext.Session))
                {
                    var url = new UrlHelper(filterContext.RequestContext);
                    var loginUrl = url.Content("~/Home/Login");
                    filterContext.Result = new RedirectResult(loginUrl);
                    return;
                }

            }
        }
    }
}