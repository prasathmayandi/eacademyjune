﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class TimeScheduleServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public List<TimePeriodSchedule> getTimeScheduleList()
        {
            var ans = (from t1 in dc.TimeSchedules
                       where t1.Status==true
                       select new TimePeriodSchedule { ScheduleID = t1.TimeScheduleId, txt_Schedule = t1.Schedule}).ToList();
            return ans;
        }

        public List<TimePeriodSchedule> getTimeSchedule()
        {
            var ans = (from t1 in dc.TimeSchedules
                       select new TimePeriodSchedule { ScheduleID=t1.TimeScheduleId,txt_Schedule=t1.Schedule,NumberPeriod=t1.NoPeriods.Value,Status=t1.Status.Value }).ToList();
            return ans;
        }
        internal TimePeriodSchedule getTimeSchedule(int ScheduleId)
        {
            var ans = (from t1 in dc.TimeSchedules
                       where t1.TimeScheduleId == ScheduleId
                       select new TimePeriodSchedule { ScheduleID = t1.TimeScheduleId, txt_Schedule = t1.Schedule, NumberPeriod = t1.NoPeriods.Value, Status = t1.Status.Value }).FirstOrDefault();
            return ans;
        }
        internal TimeSchedule getTimeSchedule(string Schedule,int NumberPeriod, Boolean Status)
        {
            var ans = dc.TimeSchedules.Where(q => q.Schedule.Equals(Schedule) && q.NoPeriods.Value.Equals(NumberPeriod) && q.Status.Value.Equals(Status)).FirstOrDefault();
            return ans;
        }
        internal void addTimeSchedule(string Schedule, int NumberPeriod, Boolean Status)
        {
            TimeSchedule add = new TimeSchedule()
            {
                Schedule = Schedule,
                NoPeriods = NumberPeriod,
                Status = Status
            };
            dc.TimeSchedules.Add(add);
            dc.SaveChanges();
        }

        internal bool checkTimeSchedule(string Schedule)
        {
            var ans = dc.TimeSchedules.Where(q => q.Schedule.Equals(Schedule)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
      
        internal void updateTimeSchedule(int TimeScheduleId, string Schedule, int NumberPeriod, bool Status)
        {
            var update = dc.TimeSchedules.Where(q => q.TimeScheduleId.Equals(TimeScheduleId)).FirstOrDefault();
            update.Schedule = Schedule;
            update.NoPeriods = NumberPeriod;
            update.Status = Status;
            dc.SaveChanges();
        }

        //public int? GetClassPeriods(int cid)
        //{
        //    var res = (from a in dc.Classes
        //               from b in dc.TimeSchedules
        //               from c in dc.PeriodsSchedules
        //               where a.ClassId == cid && a.TimeScheduleId == c.TimeScheduleId
        //               select b).FirstOrDefault();
        //    int? x = 0;
        //    if (res != null)
        //    {
        //        x = res.NoPeriods;
        //    }
        //    return x;
        //}

        public int? ClassPeriodCount(int cid)
        {
            var res = (from a in dc.Classes
                       where a.ClassId == cid
                       select a).FirstOrDefault();
            int? x = 0;
            int? count = 0;
            if (res != null)
            {
                x = res.TimeScheduleId;

                if (x != 0)
                {
                    var ter = (from a in dc.TimeSchedules
                               where a.TimeScheduleId == x
                               select a).FirstOrDefault();
                    if (ter != null)
                    {
                        count = ter.NoPeriods;
                    }
                }
            }
            return count;
        }
        ////my code//
        public int GetClassPeriodCount(int cid)
        {
            int Count;
            var res = (from a in dc.Classes
                       from c in dc.PeriodsSchedules
                       where a.ClassId == cid && a.TimeScheduleId == c.TimeScheduleId
                       select new
                       {
                           PeriodsScheduleId = c.PeriodScheduleId,
                           PeriodType = c.Type
                       }).ToList();
            Count = res.Count;
            return Count;

        }
        public List<TimePeriodSchedule> GetClassPeriodTimeTable(int cid)
        {
            var ans = (from a in dc.Classes
                       from c in dc.PeriodsSchedules
                       where a.ClassId == cid && a.TimeScheduleId == c.TimeScheduleId
                       select new
                       {
                           PeriodsScheduleId=c.PeriodScheduleId,
                           PeriodType=c.Type,
                           PeriodName=c.Periods
                           
                       }).ToList().Select(q => new TimePeriodSchedule
                       {
                           PeriodS_Id=q.PeriodsScheduleId,
                           PeriodType=q.PeriodType,
                           PeriodName=q.PeriodName,
                           txt_PeriodTime =GetPeriodTime(q.PeriodsScheduleId),

                       }).ToList();

            return ans;

        }
        public string GetPeriodTime(int PeriodsScheduleId)
        {
            var File = (from a in dc.PeriodsSchedules

                        where a.PeriodScheduleId == PeriodsScheduleId 
                        select new
                        {
                            Periods = a.Periods,
                            StTime=a.StartTime.Value,
                            EdTime=a.EndTime.Value

                        }).Distinct().ToList().Select(x => new TimePeriodSchedule
                         {
                             txt_PeriodTime ="(" + x.StTime.ToString(@"hh\:mm")+ " - " + x.EdTime.ToString(@"hh\:mm") + ")"
                         }).ToList();

            string[] txt_PeriodTime = new string[File.Count];
            for (int i = 0; i < File.Count; i++)
            {
                txt_PeriodTime[i] = File[i].txt_PeriodTime;
            }
            string txt_PeriodTimeList = string.Join(",", txt_PeriodTime);
            return txt_PeriodTimeList;
        }

        public int? GetMaximumPeriod()
        {
            var row = (from a in dc.TimeSchedules orderby a.NoPeriods descending select a).ToList().FirstOrDefault().NoPeriods;
            return row;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}