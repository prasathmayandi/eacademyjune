﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class AssignmentServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public List<ParentAssignment> getParentAssignment(int yearId, int classId, int secId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in db.Assignments
                        from b in db.AssignSubjectToSections
                        from c in db.Subjects
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.AssignmentPostedDate >= fdate &&
                        a.AssignmentPostedDate <= toDate &&
                        a.SubjectId == c.SubjectId
                        select new ParentAssignment
                        {
                            AssignmentId = a.AssignmentId,
                            AssignmentPostedDate = a.AssignmentPostedDate,
                            Subject = c.SubjectName,
                            Assignment1 = a.Assignment1,
                            Descriptions = a.Descriptions,
                            AssignmentFileName = a.AssignmentFileName,
                            DateOfSubmission = a.DateOfSubmission
                        }).ToList();
            return list;
        }

        public Assignment getStudentAssignmentFile(int fid)
        {
            var getAssignmentFile = (from a in db.Assignments where a.AssignmentId == fid select a).FirstOrDefault();
            return getAssignmentFile;
        }

        public void addAssignment(int yearId, int classId, int secId, int subId, DateTime submissionDate, string Assignment, string desc, string fileName, int FacultyId)
        {
            DateTime date = DateTimeByZone.getCurrentDate();

            Assignment add = new Assignment()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                SubjectId = subId,
                AssignmentPostedDate = date,
                DateOfSubmission = submissionDate,
                Assignment1 = Assignment,
                Descriptions = desc,
                AssignmentFileName = fileName,
                AssignmentStatus = true,
                EmployeeRegisterId = FacultyId
            };
            db.Assignments.Add(add);
            db.SaveChanges();
        }

        public List<HomeworkList> getAssignmentList(int yearId, int classId, int secId, int subId, DateTime fromDate, DateTime toDate, int FacultyId)
        {
            var list = (from a in db.Assignments
                        where a.AcademicYearId == yearId &&
                                 a.ClassId == classId &&
                                 a.SectionId == secId &&
                                 a.SubjectId == subId &&
                                 a.EmployeeRegisterId == FacultyId &&
                                 a.DateOfSubmission >= fromDate &&
                                 a.DateOfSubmission <= toDate
                        select new HomeworkList
                        {
                            HomeWorkId = a.AssignmentId,
                            DateOfWorkPosted = a.AssignmentPostedDate,
                            DateToCompleteWork = a.DateOfSubmission,
                            HomeWork1 = a.Assignment1,
                            Descriptions = a.Descriptions,
                            HomeWorkStatus = a.AssignmentStatus,
                            HomeWorkFileName = a.AssignmentFileName
                        }).ToList();

            return list;
        }
        public void updateAssignmentFile(int eid, string fileName1)
        {
            var getRow = (from a in db.Assignments where a.AssignmentId == eid select a).FirstOrDefault();
            getRow.AssignmentFileName = fileName1;
            db.SaveChanges();
        }

        public void updateAssignment(DateTime Date, string Assignment, string desc, Boolean status, int eid)
        {
            var getRow = (from a in db.Assignments where a.AssignmentId == eid select a).FirstOrDefault();
            getRow.Assignment1 = Assignment;
            getRow.DateOfSubmission = Date;
            getRow.Descriptions = desc;
            getRow.AssignmentStatus = status;
            db.SaveChanges();
        }
        public void delAssignment(int id)
        {
            var getRow = (from a in db.Assignments where a.AssignmentId == id select a).FirstOrDefault();
            db.Assignments.Remove(getRow);
            db.SaveChanges();
        }

        public List<StudentAssignment> getStudentAssignment(int yearId, int classId, int secId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in db.Assignments
                        from b in db.Subjects
                        where a.SubjectId == b.SubjectId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.AssignmentPostedDate >= fdate &&
                        a.AssignmentPostedDate <= toDate
                        select new
                        {
                            AssignmentId = a.AssignmentId,
                            Subject = b.SubjectName,
                            AssignmentPostedDate = a.AssignmentPostedDate,
                            Assignment1 = a.Assignment1,
                            Descriptions = a.Descriptions,
                            AssignmentFileName = a.AssignmentFileName,
                            DateOfSubmission = a.DateOfSubmission
                        }).ToList().ToList().Select(a => new StudentAssignment
                        {
                            Assignment_Id = QSCrypt.Encrypt(a.AssignmentId.ToString()),
                            Subject = a.Subject,
                            PostedDate = a.AssignmentPostedDate.Value.ToString("dd MMM, yyyy"),
                            Assignment = a.Assignment1,
                            Description = a.Descriptions,
                            AssignmentFileName = a.AssignmentFileName,
                            SubmissionDate = a.DateOfSubmission.Value.ToString("dd MMM, yyyy"),
                            parentAssignmentfilename = getParentAssignmentsWithFile(a.AssignmentId),
                        }).ToList();
            return list;
        }

        public List<StudentAssignment> getStudentAssignment(int yearId, int classId, int secId, int subId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in db.Assignments
                        from b in db.Subjects
                        where a.SubjectId == b.SubjectId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.SubjectId == subId &&
                        a.AssignmentPostedDate >= fdate &&
                        a.AssignmentPostedDate <= toDate
                        select new 
                        {
                            AssignmentId = a.AssignmentId,
                            Subject = b.SubjectName,
                            AssignmentPostedDate = a.AssignmentPostedDate,
                            Assignment1 = a.Assignment1,
                            Descriptions = a.Descriptions,
                            AssignmentFileName = a.AssignmentFileName,
                            DateOfSubmission = a.DateOfSubmission
                        }).ToList().Select(a => new StudentAssignment
                        {
                            Assignment_Id = QSCrypt.Encrypt(a.AssignmentId.ToString()),
                            Subject = a.Subject,
                            PostedDate = a.AssignmentPostedDate.Value.ToString("MMM dd yyyy"),
                            Assignment = a.Assignment1,
                            Description = a.Descriptions,
                            AssignmentFileName = a.AssignmentFileName,
                            SubmissionDate = a.DateOfSubmission.Value.ToString("MMM dd yyyy"),
                            parentAssignmentfilename = getParentAssignmentsWithFile(a.AssignmentId)
                        }).ToList();
            return list;
        }

        public List<StudentAssignment> getStudentAssignment1(int yearId, int classId, int secId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in db.Assignments
                        from b in db.Subjects
                        where a.SubjectId == b.SubjectId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.AssignmentPostedDate >= fdate &&
                        a.AssignmentPostedDate <= toDate
                        select new
                        {
                            AssignmentId = a.AssignmentId,
                            Subject = b.SubjectName,
                            AssignmentPostedDate = a.AssignmentPostedDate,
                            Assignment1 = a.Assignment1,
                            Descriptions = a.Descriptions,
                            AssignmentFileName = a.AssignmentFileName,
                            DateOfSubmission = a.DateOfSubmission
                        }).ToList().ToList().Select(a => new StudentAssignment
                        {
                            Assignment_Id = QSCrypt.Encrypt(a.AssignmentId.ToString()),
                            Subject = a.Subject,
                            PostedDate = a.AssignmentPostedDate.Value.ToString("MMM dd yyyy"),
                            Assignment = a.Assignment1,
                            Description = a.Descriptions,
                            AssignmentFileName = a.AssignmentFileName,
                            SubmissionDate = a.DateOfSubmission.Value.ToString("MMM dd yyyy"),
                            parentAssignmentfilename = getStudentAssignmentsWithFile(a.AssignmentId),
                        }).ToList();
            return list;
        }

        public List<StudentAssignment> getStudentAssignment1(int yearId, int classId, int secId, int subId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in db.Assignments
                        from b in db.Subjects
                        where a.SubjectId == b.SubjectId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.SubjectId == subId &&
                        a.AssignmentPostedDate >= fdate &&
                        a.AssignmentPostedDate <= toDate
                        select new 
                        {
                            AssignmentId = a.AssignmentId,
                            Subject = b.SubjectName,
                            AssignmentPostedDate = a.AssignmentPostedDate,
                            Assignment1 = a.Assignment1,
                            Descriptions = a.Descriptions,
                            AssignmentFileName = a.AssignmentFileName,
                            DateOfSubmission = a.DateOfSubmission
                        }).ToList().Select(a => new StudentAssignment
                        {
                            Assignment_Id = QSCrypt.Encrypt(a.AssignmentId.ToString()),
                            Subject = a.Subject,
                            PostedDate = a.AssignmentPostedDate.Value.ToString("MMM dd yyyy"),
                            Assignment = a.Assignment1,
                            Description = a.Descriptions,
                            AssignmentFileName = a.AssignmentFileName,
                            SubmissionDate = a.DateOfSubmission.Value.ToString("MMM dd yyyy"),
                            parentAssignmentfilename = getStudentAssignmentsWithFile(a.AssignmentId)
                        }).ToList();
            return list;
        }




        public List<AssignmentList> getAssignmentList(int? yearId, int? classId, int? secId, int? subId, DateTime? fromDate, DateTime? toDate, int? FacultyId)
        {
            var list = (from a in db.Assignments
                        where a.AcademicYearId == yearId &&
                                 a.ClassId == classId &&
                                 a.SectionId == secId &&
                                 a.SubjectId == subId &&
                                 a.EmployeeRegisterId == FacultyId &&
                                 a.AssignmentPostedDate >= fromDate &&
                                 a.AssignmentPostedDate <= toDate
                        select new
                        {
                            HomeWorkId = a.AssignmentId,
                            DateOfWorkPosted = a.AssignmentPostedDate,
                            DateToCompleteWork = a.DateOfSubmission,
                            HomeWork1 = a.Assignment1,
                            Descriptions = a.Descriptions,
                            HomeWorkStatus = a.AssignmentStatus,
                            HomeWorkFileName = a.AssignmentFileName,
                            MI_HW_List_txt_fromDate = a.AssignmentPostedDate,
                            MI_HW_List_txt_toDate = a.AssignmentPostedDate
                        }).ToList().Select(a => new AssignmentList
                        {
                            Assignment_Id = QSCrypt.Encrypt(a.HomeWorkId.ToString()),
                            PostedtDate = a.DateOfWorkPosted.Value.ToString("dd MMM, yyyy"),
                            LastDate = a.DateToCompleteWork.Value.ToString("dd MMM, yyyy"),
                            Assignment = a.HomeWork1,
                            Descriptions = a.Descriptions,
                            Status = a.HomeWorkStatus,
                            AssignmentFileName = a.HomeWorkFileName,
                            fromDate = a.DateOfWorkPosted,
                            toDate = a.DateOfWorkPosted,
                            AssignmentAttachFileName = getAssignmentsWithFile(a.HomeWorkId),
                        }).ToList();

            return list;
        }

        public string getAssignmentsWithFile(int AssignmentId)
        {
            string sn;
            var File = (from a in db.Assignments where a.AssignmentId == AssignmentId select a).FirstOrDefault().AssignmentFileName;
            if (File != null)
            {
                var SN = (from a in db.Assignments where a.AssignmentId == AssignmentId select a).FirstOrDefault().Assignment1;
                string assignmentid = QSCrypt.Encrypt(AssignmentId.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    sn = "<a href=/ModulesInfo/OpenAssignmentFile/" + assignmentid + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + SN;
                }
                else
                {
                    sn = "<a href=/ModulesInfo/OpenAssignmentFile/" + assignmentid + "&name=student><i class='fa fa-file-word-o'></i></a>" + SN;
                }
            }
            else
            {
                var SN = (from a in db.Assignments where a.AssignmentId == AssignmentId select a).FirstOrDefault().Assignment1;
                sn = SN;
            }
            return sn;
        }

        public string getParentAssignmentsWithFile(int AssignmentId)
        {
            string sn;
            var File = (from a in db.Assignments where a.AssignmentId == AssignmentId select a).FirstOrDefault().AssignmentFileName;
            if (File != null)
            {
                var SN = (from a in db.Assignments where a.AssignmentId == AssignmentId select a).FirstOrDefault().Assignment1;
                string assignmentid = QSCrypt.Encrypt(AssignmentId.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    sn = "<a href=/ParentModuleAssigned/OpenAssignmentFile/" + assignmentid + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + SN;
                }
                else
                {
                    sn = "<a href=/ParentModuleAssigned/OpenAssignmentFile/" + assignmentid + "&name=student><i class='fa fa-file-word-o'></i></a>" + SN;
                }
            }
            else
            {
                var SN = (from a in db.Assignments where a.AssignmentId == AssignmentId select a).FirstOrDefault().Assignment1;
                sn = SN;
            }
            return sn;
        }

        public string getStudentAssignmentsWithFile(int AssignmentId)
        {
            string sn;
            var File = (from a in db.Assignments where a.AssignmentId == AssignmentId select a).FirstOrDefault().AssignmentFileName;
            if (File != null)
            {
                var SN = (from a in db.Assignments where a.AssignmentId == AssignmentId select a).FirstOrDefault().Assignment1;
                string assignmentid = QSCrypt.Encrypt(AssignmentId.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    sn = "<a href=/StudentModuleAssigned/OpenAssignmentFile/" + assignmentid + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + SN;
                }
                else
                {
                    sn = "<a href=/StudentModuleAssigned/OpenAssignmentFile/" + assignmentid + "&name=student><i class='fa fa-file-word-o'></i></a>" + SN;
                }
            }
            else
            {
                var SN = (from a in db.Assignments where a.AssignmentId == AssignmentId select a).FirstOrDefault().Assignment1;
                sn = SN;
            }
            return sn;
        }



        public void updateAssignment(DateTime Date, string Assignment, string desc, Boolean status, string fileName, int eid)
        {
            var getRow = (from a in db.Assignments where a.AssignmentId == eid select a).FirstOrDefault();
            getRow.Assignment1 = Assignment;
            getRow.DateOfSubmission = Date;
            getRow.Descriptions = desc;
            getRow.AssignmentStatus = status;
            getRow.AssignmentFileName = fileName;
            db.SaveChanges();
        }

        public HomeworkReplyList getAssignmentDetailById(int AssignmentId)
        {
            var getRow = (from a in db.Assignments
                          from b in db.Subjects
                          where a.SubjectId == b.SubjectId &&
                           a.AssignmentId == AssignmentId
                          select new HomeworkReplyList
                          {
                              Subject = b.SubjectName,
                              Title = a.Assignment1,
                              Description = a.Descriptions,
                              SubmissionDate = SqlFunctions.DateName("year", a.DateOfSubmission) + "/" + SqlFunctions.StringConvert((double)a.DateOfSubmission.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("day", a.DateOfSubmission).Trim(),
                          }).FirstOrDefault();
            return getRow;
        }

        public Assignment GetClassDetails(int AssignmentId)
        {
            var row = (from a in db.Assignments where a.AssignmentId == AssignmentId select a).FirstOrDefault();
            return row;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}