﻿using eAcademy.DataModel;
using System;
using System.Linq;

namespace eAcademy.Services
{
    public class PreAdmissionFatherRegisterServices:IDisposable
    {

        EacademyEntities dc = new EacademyEntities();
        public int AddFatherdetails(string fafirstname, string falastname, DateTime? fadob, string faqual, string faoccu, long? famobile, string faemail, long? income)
        {

            PreAdmissionFatherRegister father = new PreAdmissionFatherRegister()
            {
                FatherFirstName = fafirstname,
                FatherLastName = falastname,
                FatherDOB = fadob,
                FatherQualification = faqual,
                FatherOccupation = faoccu,
                FatherMobileNo = famobile,
                FatherEmail = faemail,
                 FatherMotherTotalIncome=income,
            };
            dc.PreAdmissionFatherRegisters.Add(father);
            dc.SaveChanges();
            return father.FatherAdmissionId;
        }

        public int UpdatefatherDetails1(int? fatherid, string fafirstname, string falastname, DateTime? fadob, string faqual, string faoccu, long? famobile, string faemail, long? income) //, 
        {
            var get = dc.PreAdmissionFatherRegisters.Where(q => q.FatherAdmissionId == fatherid).FirstOrDefault();
            if (get != null)
            {
                get.FatherFirstName = fafirstname;
                get.FatherLastName = falastname;
                get.FatherDOB = fadob;
                get.FatherEmail = faemail;
                get.FatherQualification = faqual;
                get.FatherOccupation = faoccu;
                get.FatherMobileNo = famobile;
                get.FatherMotherTotalIncome = income;
                dc.SaveChanges();
            }
            return get.FatherAdmissionId;
        }

        public int UpdatefatherDetails(int? fatherid, string faqual, string faoccu,string femail, long? famobile) //, 
        { 
            var get = dc.PreAdmissionFatherRegisters.Where(q => q.FatherAdmissionId == fatherid).FirstOrDefault();

            if(get!=null)
            {
                get.FatherEmail = femail;
                get.FatherQualification = faqual;
                get.FatherOccupation = faoccu;
                get.FatherMobileNo = famobile;
                dc.SaveChanges();
            }
            return get.FatherAdmissionId;
        }

        public int CheckFatherExit(int primaryuserid, int Newstudentid, string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email,long? totalincome)
        {

            int Fatherid = 0;

            var GetMother = (from a in dc.PreAdmissionTransactions
                             from b in dc.PreAdmissionFatherRegisters
                             where a.PrimaryUserAdmissionId == primaryuserid && b.FatherAdmissionId == a.FatherAdmissionId && b.FatherFirstName == firstname && b.FatherLastName == lastname && b.FatherDOB == dob
                             select b).FirstOrDefault();
            if (GetMother != null)
            {
                Fatherid = GetMother.FatherAdmissionId;

                UpdatefatherDetails(Fatherid, qual, occu, email,mobile);
            }
            else
            {
                Fatherid = AddFatherdetails(firstname, lastname, dob, qual, occu, mobile, email,totalincome);
            }
            return Fatherid;
        }

        public PreAdmissionFatherRegister GetEnrolementFatherDetails(int StudentAdmissionid)
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters
                       from b in dc.PreAdmissionTransactions
                       from c in dc.PreAdmissionFatherRegisters
                       where a.StudentAdmissionId == StudentAdmissionid && b.StudentAdmissionId == a.StudentAdmissionId && c.FatherAdmissionId == b.FatherAdmissionId
                       select c).FirstOrDefault();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}