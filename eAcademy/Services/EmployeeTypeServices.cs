﻿using System;
using System.Collections.Generic;
using System.Linq;
using eAcademy.DataModel;
using System.Collections;

namespace eAcademy.Services
{
    public class EmployeeTypeServices:IDisposable
    {
        EacademyEntities ee = new EacademyEntities();

        public IEnumerable GetEmployeeType()
        {
            var ans = ee.EmployeeTypes.Select(q => q);
                return ans;
        }
        public IEnumerable<Object> ShowEmployeeType()
        {
            var AcademicYearList = (from a in ee.EmployeeTypes
                                    select new
                                    {
                                        EmployeeType = a.EmployeeTypes,
                                        EmployeeTypeId = a.EmployeeTypeId
                                    }).ToList().AsEnumerable();
            return AcademicYearList;
        }
        
        public List<int> getAllEmployeeTypeId()
        {
            var ans = ee.EmployeeTypes.Select(q => q.EmployeeTypeId).ToList();
            return ans;
        }

        public int GetTypeId(string empType)
        {
            var record = ee.EmployeeTypes.Where(q => q.EmployeeTypes == empType).FirstOrDefault();
            if(record!=null)
            {
                return record.EmployeeTypeId;
            }
            else
            {
                return 0;
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ee.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}