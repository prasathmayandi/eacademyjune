﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eAcademy.Services
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class Resource_CheckSessionAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower();
            if (!controllerName.Contains("home"))
            {
                HttpSessionStateBase session = filterContext.HttpContext.Session;


                var user = ConfigurationManager.AppSettings["EnableResource_SuperAdmin"];
                if (((user != "true") && (!session.IsNewSession)) || (session.IsNewSession) || (null == filterContext.HttpContext.Session))
                {
                    //send them off to the login page
                    var url = new UrlHelper(filterContext.RequestContext);
                    var loginUrl = url.Content("~/Home/Login");
                    filterContext.Result = new RedirectResult(loginUrl);
                    return;
                }
            }
        }
    }

}