﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class ClassServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public IEnumerable<Object> ShowClasses()
        {
            var ClassList = (from a in dc.Classes
                             where a.Status == true
                             select new
                             {
                                 ClassName = a.ClassType,
                                 ClassId = a.ClassId
                             }).ToList().AsEnumerable();
            return ClassList;
        }

        public IEnumerable<Object> ShowClassesBySession(int YearId)
        {
            var ClassList = (from a in dc.Classes
                             where a.Status == true
                             select new
                             {
                                 ClassName = a.ClassType,
                                 ClassId = a.ClassId
                             }).ToList().AsEnumerable();
            return ClassList;
        }

        public IEnumerable<Object> ShowClasses(int ClassInchargeId)
        {
            var ClassList = (from a in dc.Classes
                             from b in dc.AssignFacultyToSubjects
                             where a.ClassId == b.ClassId &&
                             b.EmployeeRegisterId == ClassInchargeId
                             select new
                             {
                                 ClassName = a.ClassType,
                                 ClassId = a.ClassId
                             }).ToList().Distinct().AsEnumerable();
            return ClassList;
        }

        public Class getClassName(int? classId)
        {
            var getRecord = (from a in dc.Classes where a.ClassId == classId select a).FirstOrDefault();
            return getRecord;
        }

        public List<ClassSection> Classes()
        {
            var ans = (from t1 in dc.Classes
                       where t1.Status==true
                       select new ClassSection { ClassId = t1.ClassId, ClassType = t1.ClassType }).OrderBy(q=>q.ClassId).ToList();
            return ans;
        }

        public List<int> getAllClassId()
        {
            var ans = dc.Classes.Where(q=>q.Status.Value.Equals(true)).Select(q => q.ClassId).ToList();
            return ans;
        }

        public Class getClassIdByClassName(string classname)
        {
            var ans = dc.Classes.Where(q => q.ClassType.Equals(classname)).SingleOrDefault();
            return ans;
        }

        public Class getClassNameByClassID(int cid)
        {
            var ans = dc.Classes.Where(query => query.ClassId.Equals(cid)).First();
            return ans;
        }

        public bool checkClass(string classname)
        {
            var ans = dc.Classes.Where(q => q.ClassType.Equals(classname)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<Class> getClass()
        {
            var ans = dc.Classes.Select(query => query).ToList();
            return ans;
        }

        public IEnumerable<object> getClass1()
        {
            var ans = dc.Classes.Select(query => query).ToList();
            return ans;
        }

        public void addClass(string className)
        {
            Class cls = new Class()
            {
                ClassType = className
            };

            dc.Classes.Add(cls);
            dc.SaveChanges();
        }

        public void updateClass(int cid, string classType)
        {
            var update = dc.Classes.Where(q => q.ClassId.Equals(cid)).First();
            update.ClassType = classType;
            dc.SaveChanges();
        }

        internal List<ClassSection> getClasswithSchedule()
        {
            var ans = (from t1 in dc.Classes
                     
                       select new ClassSection { ClassId = t1.ClassId, ClassType = t1.ClassType, Status = t1.Status, ScheduleID=t1.TimeScheduleId }).ToList();
            return ans;
        }

        internal void updateClassSchedule(int p1, int p2, bool p3)
        {
            var update = dc.Classes.Where(q => q.ClassId.Equals(p1)).FirstOrDefault();
            update.TimeScheduleId = p2;
            update.Status = p3;
            dc.SaveChanges();
        }

        internal List<ClassSection> Classes(int cid)
        {
            var ans = (from t1 in dc.Classes
                       where t1.ClassId >= cid && t1.Status == true
                       select new ClassSection { ClassId = t1.ClassId, ClassType = t1.ClassType }).ToList();

            return ans;
        }

        public List<M_ClassVacancy> GetClassIdandName()
        {
            var ans = (from a in dc.Classes
                       where a.Status == true
                       select new M_ClassVacancy
                       {
                           classid = a.ClassId,
                           txt_class = a.ClassType
                       }).ToList();
            return ans;
        }
        public IEnumerable GetClass()
        {
            var ans = dc.Classes.Where(q=>q.Status.Value.Equals(true)).Select(q => q);
            return ans;
        }
        public IEnumerable GetClassvacauncy()
        {
            var ans = (from a in dc.AsignClassVacancies
                       from b in dc.Classes
                       where a.Status == true && b.ClassId == a.ClassId && b.Status == true
                       select b).Distinct().ToList();
            return ans;
        }
        public IEnumerable GetParticularClass(int classid)
        {
            var ans = dc.Classes.Where(q => q.ClassId.Equals(classid)).FirstOrDefault().ClassType;
            return ans;
        }
        public Class getPreviousClass(int classId)
        {
            int clasid = (from a in dc.Classes where a.ClassId < classId select a.ClassId).ToList().Max();
            var clas = (from a in dc.Classes where a.ClassId == clasid select a).FirstOrDefault();
            return clas;
        }
        public List<getclassandsection> GetAllClassandSection()
        {
            var ans = (from b in dc.Classes
                       from c in dc.Sections
                       where
                       b.Status==true && c.ClassId==b.ClassId
                       select new getclassandsection
                       {
                           ClassId=b.ClassId,
                           ClassName = b.ClassType,
                           SectionName = c.SectionName
                       }).Distinct().OrderBy(q => q.ClassId).ToList();

            return ans;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
      
    }
}