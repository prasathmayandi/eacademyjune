﻿using eAcademy.DataModel;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eAcademy.Services
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class CheckSessionOutAttribute : ActionFilterAttribute
    {

        Data db = new Data();
        EacademyEntities ee = new EacademyEntities();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower();
            if (!controllerName.Contains("home"))
            {
                HttpSessionStateBase session = filterContext.HttpContext.Session;
                var user = session["username"];
                if (((user == null) && (!session.IsNewSession)) || (session.IsNewSession) || (null == filterContext.HttpContext.Session))
                {
                    var url = new UrlHelper(filterContext.RequestContext);
                    var loginUrl = url.Content("~/Home/Login");
                    filterContext.Result = new RedirectResult(loginUrl);
                    return;
                }
                else
                {

                    if (!controllerName.Contains("dashboard"))
                    {
                        string controllerName1 = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
                        string actionName = filterContext.ActionDescriptor.ActionName;
                        System.Collections.ArrayList link = (System.Collections.ArrayList) session["ControllerName"];
                     
                        string[] controller = (string[])link.ToArray(typeof(string));
                        if (controller.Contains(controllerName1))
                        {
                            var ans = ee.Features.Where(q => q.ControllerName.Equals(controllerName1)).Select(s => new {id=s.FeatureId,cname=s.FaetureType,gname=s.GroupName }).FirstOrDefault();
                            session["UrlId"] = ans.id;
                            session["UrlControllerName"] = ans.cname;
                            session["UrlGroupName"] = ans.gname;
                            session["UrlActionName"] = actionName;
                        }
                        else
                        {
                            var url = new UrlHelper(filterContext.RequestContext);
                            var loginUrl = url.Content("~/DashBoard/Error");
                            filterContext.Result = new RedirectResult(loginUrl);
                            return;
                        }
                       
                    }
                   
                           
                }
            }
        }
    }
}