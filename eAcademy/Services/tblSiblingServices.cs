﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class tblSiblingServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public void CopysiblingfromPreAdmissiontoMaster(int Primaryuserid, int studentregid, int fatherid, int motherid, int guardianid)
        {
            var PUser = db.PrimaryUserRegisters.Where(q => q.PrimaryUserRegisterId == Primaryuserid).FirstOrDefault().PrimaryUser;
            var RemaingingStudent = db.AdmissionTransactions.Where(q => q.PrimaryUserRegisterId == Primaryuserid && q.StudentRegisterId != studentregid).ToList();
            if (RemaingingStudent.Count != 0)
            {
                if (PUser == "Father")
                {
                    foreach (var v in RemaingingStudent)
                    {
                        if (studentregid != v.StudentRegisterId)
                        {
                            tblSibling sibling = new tblSibling()
                            {
                                StudentRegisterId = v.StudentRegisterId,
                                SiblingStudentRegId = studentregid,
                                PrimaryUserRegId = v.PrimaryUserRegisterId,
                                StatusFlag = "True"
                            };
                            db.tblSiblings.Add(sibling);
                            db.SaveChanges();
                            var getstudent = db.Students.Where(q => q.StudentRegisterId == v.StudentRegisterId).FirstOrDefault();
                            if (getstudent != null)
                            {
                                tblSibling si = new tblSibling()
                                {
                                    StudentRegisterId = studentregid,
                                    SiblingStudentRegId = v.StudentRegisterId,

                                    PrimaryUserRegId = v.PrimaryUserRegisterId,
                                    StatusFlag = "True"
                                };
                                db.tblSiblings.Add(si);
                                db.SaveChanges();
                                getstudent.HaveSiblings = "Yes";
                                db.SaveChanges();
                            }
                        }

                    }
                    var currentstudent = db.Students.Where(q => q.StudentRegisterId == studentregid).FirstOrDefault();
                    if (currentstudent != null)
                    {
                        currentstudent.HaveSiblings = "Yes";
                        db.SaveChanges();

                    }
                }
                else if (PUser == "Mother")
                {
                    foreach (var v in RemaingingStudent)
                    {
                        if (studentregid != v.StudentRegisterId)
                        {
                            tblSibling sibling = new tblSibling()
                            {
                                StudentRegisterId = v.StudentRegisterId,
                                SiblingStudentRegId = studentregid,
                                PrimaryUserRegId = v.PrimaryUserRegisterId,
                                StatusFlag = "True"
                            };
                            db.tblSiblings.Add(sibling);
                            db.SaveChanges();
                            var getstudent = db.Students.Where(q => q.StudentRegisterId == v.StudentRegisterId).FirstOrDefault();
                            if (getstudent != null)
                            {
                                tblSibling si = new tblSibling()
                                {
                                    StudentRegisterId = studentregid,
                                    SiblingStudentRegId = v.StudentRegisterId,

                                    PrimaryUserRegId = v.PrimaryUserRegisterId,
                                    StatusFlag = "True"
                                };
                                db.tblSiblings.Add(si);
                                db.SaveChanges();
                                getstudent.HaveSiblings = "Yes";
                                db.SaveChanges();
                            }
                        }
                    }
                    var currentstudent = db.Students.Where(q => q.StudentRegisterId == studentregid).FirstOrDefault();
                    if (currentstudent != null)
                    {
                        currentstudent.HaveSiblings = "Yes";
                        db.SaveChanges();

                    }
                }
                else if (PUser == "Guardian")
                {
                    if (fatherid != 0 && motherid != 0)
                    {
                        var checkfathermothersame = db.AdmissionTransactions.Where(q => q.PrimaryUserRegisterId == Primaryuserid && q.StudentRegisterId != studentregid && q.FatherRegisterId == fatherid && q.MotherRegisterId == motherid).ToList();
                        if (checkfathermothersame.Count != 0)
                        {
                            foreach (var v in checkfathermothersame)
                            {
                                if (studentregid != v.StudentRegisterId)
                                {
                                    tblSibling sibling = new tblSibling()
                                    {
                                        StudentRegisterId = v.StudentRegisterId,
                                        SiblingStudentRegId = studentregid,
                                        PrimaryUserRegId = v.PrimaryUserRegisterId,
                                        StatusFlag = "True"
                                    };
                                    db.tblSiblings.Add(sibling);
                                    db.SaveChanges();
                                    var getstudent = db.Students.Where(q => q.StudentRegisterId == v.StudentRegisterId).FirstOrDefault();
                                    if (getstudent != null)
                                    {
                                        tblSibling si = new tblSibling()
                                        {
                                            StudentRegisterId = studentregid,
                                            SiblingStudentRegId = v.StudentRegisterId,
                                            PrimaryUserRegId = v.PrimaryUserRegisterId,
                                            StatusFlag = "True"
                                        };
                                        db.tblSiblings.Add(si);
                                        db.SaveChanges();
                                        getstudent.HaveSiblings = "Yes";
                                        db.SaveChanges();
                                    }
                                }
                            }
                            var currentstudent = db.Students.Where(q => q.StudentRegisterId == studentregid).FirstOrDefault();
                            if (currentstudent != null)
                            {
                                currentstudent.HaveSiblings = "Yes";
                                db.SaveChanges();
                            }
                        }
                    }

                }
            }
        }

        public void CopyFromParentSiblingToSibling(PreAdmissionParentSibling row, int studentRegId)
        {
            tblSibling add = new tblSibling()
            {

                StudentRegisterId = studentRegId,
                SiblingAcademicYear = row.SiblingAcademicYear,
                SiblingClass = row.SiblingClass,
                SiblingSection = row.SiblingSection,
                SiblingRollNumbers = row.SiblingRollNumbers,
                PersonName = row.PersonName,
                PersonDepartment = row.PersonDepartment
            };
            db.tblSiblings.Add(add);
            db.SaveChanges();

        }

        public void ChangeOldsiblingstatus(int primaryuserid, int studentid)
        {
            var GetChagedstudentList = db.tblSiblings.Where(q => q.StudentRegisterId == studentid && q.StatusFlag == "True").ToList();
            if (GetChagedstudentList.Count != 0)
            {
                foreach (var v in GetChagedstudentList)
                {
                    var getcurrentrow = db.tblSiblings.Where(q => q.StudentRegisterId == v.StudentRegisterId).FirstOrDefault();
                    if (getcurrentrow != null)
                    {
                        db.tblSiblings.Remove(getcurrentrow);
                        db.SaveChanges();
                    }
                   
                }
            }
            var GetChagedstudentList1 = db.tblSiblings.Where(q => q.SiblingStudentRegId == studentid && q.StatusFlag == "True").ToList();
            if (GetChagedstudentList1.Count != 0)
            {
                foreach (var v in GetChagedstudentList1)
                {
                   
                    var getcurrentrow1 = db.tblSiblings.Where(q => q.SiblingStudentRegId == v.SiblingStudentRegId).FirstOrDefault();
                    if (getcurrentrow1 != null)
                    {
                        db.tblSiblings.Remove(getcurrentrow1);
                        db.SaveChanges();
                    }
                }
            }
            //var ans = db.tblSiblings.Where(q => q.StudentRegisterId == studentid && q.PrimaryUserRegId == primaryuserid && q.StatusFlag == "True").ToList();

            //if (ans.Count != 0)
            //{
            //    foreach (var v in ans)
            //    {
            //        var get = db.tblSiblings.Where(q => q.StudentRegisterId == v.SiblingStudentRegId && q.PrimaryUserRegId == v.PrimaryUserRegId && q.StatusFlag == "True").FirstOrDefault();
            //        if (get != null)
            //        {
            //            get.StatusFlag = "False";
            //            db.SaveChanges();
            //        }
            //        var set = db.tblSiblings.Where(q => q.StudentRegisterId == v.SiblingStudentRegId && q.SiblingStudentRegId == v.StudentRegisterId && q.PrimaryUserRegId == v.PrimaryUserRegId && q.StatusFlag == "True").FirstOrDefault();
            //        if (set != null)
            //        {
            //            set.StatusFlag = "False";
            //            db.SaveChanges();
            //        }
            //    }
            //}
        }

        public void ChangeSiblingfromNewusertoExistUser(int primaryuserid, int studentregid, string studentid)
        {
            var getexistuser = db.tblSiblings.Where(q => q.PrimaryUserRegId == primaryuserid && q.StatusFlag == "True").ToList();
            if (getexistuser.Count == 0)
            {

                var ans = (from a in db.AdmissionTransactions
                           from b in db.Students
                           where a.PrimaryUserRegisterId == primaryuserid && b.StudentRegisterId == a.StudentRegisterId
                           select b).ToList();
                if (ans.Count != 0)
                {
                    foreach (var v in ans)
                    {
                        if (studentregid != v.StudentRegisterId)
                        {
                            tblSibling sibling = new tblSibling()
                            {
                                StudentRegisterId = v.StudentRegisterId,
                                SiblingStudentRegId = studentregid,

                                PrimaryUserRegId = primaryuserid,
                                StatusFlag = "True"
                            };
                            db.tblSiblings.Add(sibling);
                            db.SaveChanges();
                            tblSibling si = new tblSibling()
                            {
                                StudentRegisterId = studentregid,
                                SiblingStudentRegId = v.StudentRegisterId,

                                PrimaryUserRegId = primaryuserid,
                                StatusFlag = "True"
                            };
                            db.tblSiblings.Add(si);
                            db.SaveChanges();
                        }
                    }

                }
            }
            else
            {


                foreach (var v in getexistuser)
                {
                    if (studentregid != v.StudentRegisterId)
                    {
                        tblSibling sibling = new tblSibling()
                        {
                            StudentRegisterId = v.StudentRegisterId,
                            SiblingStudentRegId = studentregid,

                            PrimaryUserRegId = primaryuserid,
                            StatusFlag = "True"
                        };
                        db.tblSiblings.Add(sibling);
                        db.SaveChanges();
                        tblSibling si = new tblSibling()
                        {
                            StudentRegisterId = studentregid,
                            SiblingStudentRegId = v.StudentRegisterId,

                            PrimaryUserRegId = primaryuserid,
                            StatusFlag = "True"
                        };
                        db.tblSiblings.Add(si);
                        db.SaveChanges();
                    }
                }
            }
        }

        public void CheckFatherMotherSameinSibling(int primaryuserid, int studentRegisterId, string studentid, string fafname, string falname, DateTime? fadob, string mofname, string molanme, DateTime? modob)
        {
            var ans = (from a in db.AdmissionTransactions
                       from b in db.FatherRegisters
                       from c in db.MotherRegisters
                       where a.PrimaryUserRegisterId == primaryuserid && b.FatherRegisterId == a.FatherRegisterId && c.MotherRegisterId == a.MotherRegisterId && b.FatherFirstName == fafname &&
                             b.FatherLastName == falname && b.FatherDOB == fadob && c.MotherFirstname == mofname && c.MotherLastName == molanme && c.MotherDOB == modob
                       select a).ToList();
            if (ans.Count != 0)
            {
                foreach (var v in ans)
                {
                    tblSibling sibling = new tblSibling()
                    {
                        StudentRegisterId = v.StudentRegisterId,
                        SiblingStudentRegId = studentRegisterId,

                        PrimaryUserRegId = primaryuserid,
                        StatusFlag = "True"
                    };
                    db.tblSiblings.Add(sibling);
                    db.SaveChanges();
                    var getstudent = db.tblSiblings.Where(q => q.SiblingStudentRegId == v.StudentRegisterId).FirstOrDefault();
                    if (getstudent != null)
                    {
                        tblSibling si = new tblSibling()
                        {
                            StudentRegisterId = studentRegisterId,
                            SiblingStudentRegId = v.StudentRegisterId,

                            PrimaryUserRegId = primaryuserid,
                            StatusFlag = "True"
                        };
                        db.tblSiblings.Add(si);
                        db.SaveChanges();
                    }
                }
            }
        }

        public void CheckMotherSameinSibling(int primaryuserid, int studentRegisterId, string studentid, string mofname, string molanme, DateTime? modob)
        {
            var ans = (from a in db.AdmissionTransactions
                       from c in db.MotherRegisters
                       where a.PrimaryUserRegisterId == primaryuserid && c.MotherRegisterId == a.MotherRegisterId &&
                              c.MotherFirstname == mofname && c.MotherLastName == molanme && c.MotherDOB == modob
                       select a).ToList();
            if (ans.Count != 0)
            {
                foreach (var v in ans)
                {
                    tblSibling sibling = new tblSibling()
                    {
                        StudentRegisterId = v.StudentRegisterId,
                        SiblingStudentRegId = studentRegisterId,

                        PrimaryUserRegId = primaryuserid,
                        StatusFlag = "True"
                    };
                    db.tblSiblings.Add(sibling);
                    db.SaveChanges();
                    var getstudent = db.tblSiblings.Where(q => q.SiblingStudentRegId == v.StudentRegisterId).FirstOrDefault();
                    if (getstudent != null)
                    {
                        tblSibling si = new tblSibling()
                        {
                            StudentRegisterId = studentRegisterId,
                            SiblingStudentRegId = v.StudentRegisterId,

                            PrimaryUserRegId = primaryuserid,
                            StatusFlag = "True"
                        };
                        db.tblSiblings.Add(si);
                        db.SaveChanges();
                    }
                }
            }
        }

        public void CheckFatherSameinSibling(int primaryuserid, int studentRegisterId, string studentid, string fafname, string falname, DateTime? fadob)
        {
            var ans = (from a in db.AdmissionTransactions
                       from b in db.FatherRegisters
                       where a.PrimaryUserRegisterId == primaryuserid && b.FatherRegisterId == a.FatherRegisterId && b.FatherFirstName == fafname &&
                             b.FatherLastName == falname && b.FatherDOB == fadob
                       select a).ToList();
            if (ans.Count != 0)
            {
                foreach (var v in ans)
                {
                    tblSibling sibling = new tblSibling()
                    {
                        StudentRegisterId = v.StudentRegisterId,
                        SiblingStudentRegId = studentRegisterId,

                        PrimaryUserRegId = primaryuserid,
                        StatusFlag = "True"
                    };
                    db.tblSiblings.Add(sibling);
                    db.SaveChanges();
                    var getstudent = db.tblSiblings.Where(q => q.SiblingStudentRegId == v.StudentRegisterId).FirstOrDefault();
                    if (getstudent != null)
                    {
                        tblSibling si = new tblSibling()
                        {
                            StudentRegisterId = studentRegisterId,
                            SiblingStudentRegId = v.StudentRegisterId,

                            PrimaryUserRegId = primaryuserid,
                            StatusFlag = "True"
                        };
                        db.tblSiblings.Add(si);
                        db.SaveChanges();
                    }
                }
            }
        }

        public IEnumerable<Object> GetJsonSibling(int sturegid)
        {
            var ans = (from a in db.tblSiblings
                       from b in db.Students
                       where a.StudentRegisterId.Value.Equals(sturegid) && a.StatusFlag == "True" && b.StudentRegisterId == a.SiblingStudentRegId
                       select new
                       {
                           StudentID = b.StudentId,
                           StudentRegId = a.SiblingStudentRegId

                       }).ToList();

            return ans;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}