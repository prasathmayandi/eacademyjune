﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class HomeworkReplyFromStudentServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public List<HomeworkReplyList> getHomeworkReplyCount(int HomeworkId, int StudentRegId)
        {
            var list = (from a in db.HomeworkReplyFromStudents
                        where a.HomeWorkId == HomeworkId &&
                        a.StudentRegisterId == StudentRegId
                        select new HomeworkReplyList
                        { Description = a.Description
                        }).ToList();
            return list;
        }

        public void addAnswerForHomework(int HId,string reply,string ReplyFileName,int StudentRegId)
        {
            HomeworkReplyFromStudent add = new HomeworkReplyFromStudent()
            {
                HomeWorkId = HId,
                Description = reply,
                AnswerFileName = ReplyFileName,
                StudentRegisterId = StudentRegId,
            };
            db.HomeworkReplyFromStudents.Add(add);
            db.SaveChanges();
        }

        public HomeworkReplyFromStudent getHomeworkAnswerFile(int id)
        {
            var row = (from a in db.HomeworkReplyFromStudents where a.Id == id select a).FirstOrDefault();
            return row;
        }

        public void UpdateRemark(int homeworkAnswerId, string comment, int FacultyId)
        {
            DateTime date = DateTimeByZone.getCurrentDate();

            var row = (from a in db.HomeworkReplyFromStudents where a.Id == homeworkAnswerId select a).FirstOrDefault();
            row.Remark = comment;
            row.RemarkGivenBy = FacultyId;
            row.RemarkDate = date;
            db.SaveChanges();
        }

        public List<HomeworkReplyStudentList> getStudentList(int HomeworkId, int yearId, int classId, int secId)
        {
            var List = (from a in db.HomeworkReplyFromStudents
                        from b in db.StudentRollNumbers 
                        from c in db.Students
                        where a.StudentRegisterId == b.StudentRegisterId &&
                        a.StudentRegisterId == c.StudentRegisterId &&
                        a.HomeWorkId == HomeworkId
                        orderby b.RollNumber
                        select new
                        {
                            StudentRegId= a.StudentRegisterId,
                            StudentRollNumber = b.RollNumber,
                            FName = c.FirstName,
                            LName = c.LastName,
                            Answer = a.Description,
                            HomeworkAnswerId = a.Id,
                            Remark = a.Remark
                        }).ToList().Select(a => new HomeworkReplyStudentList
                            {
                                StudentReg_Id = QSCrypt.Encrypt(a.StudentRegId.ToString()),
                                StudentRollNumber = a.StudentRollNumber,
                                Name = a.FName+" "+a.LName,
                                Answer = a.Answer,
                                HomeworkAnswer_Id = QSCrypt.Encrypt(a.HomeworkAnswerId.ToString()),
                                Remark = a.Remark
                            }).ToList();
            return List;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}