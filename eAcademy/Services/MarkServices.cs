﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class MarkServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public Mark getMaxMark(int passYearId, int passClassId, int secId, int passSubId)
        {
            var getMaxMark = (from a in dc.Marks where a.AcademicYearId == passYearId && a.ClassId == passClassId && a.SectionId == secId && a.SubjectId == passSubId select a).FirstOrDefault();
            return getMaxMark;
        }

        public MarkDetails getMarkDetails(int markId)
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.Subjects
                       from t4 in dc.Marks
                       from t5 in dc.Sections
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.SubjectId == t4.SubjectId && t4.MarkId == markId && t5.SectionId == t4.SectionId
                       select new MarkDetails { AcYearId = t1.AcademicYearId, AcYear = t1.AcademicYear1, ClassId = t2.ClassId, Class = t2.ClassType, SubjectId = t3.SubjectId, Subject = t3.SubjectName, MaxMark = t4.MaxMark, MinMark = t4.MinMark, Section = t5.SectionName, SectionId = t5.SectionId }).First();
            return ans;
        }

        public List<ClassStudent> getSubjectMark(int acid, int cid, int sec_id, int std_id, int examId)
        {
            var ans = (from t1 in dc.StudentMarks
                       from t3 in dc.Subjects
                       from t4 in dc.Marks
                       where t1.AcademicYearId == t4.AcademicYearId && t1.ClassId == t4.ClassId && t3.SubjectId == t1.SubjectId && t4.SubjectId == t1.SubjectId && t1.StudentRegisterId == std_id && t1.AcademicYearId == acid && t1.ClassId == cid && t1.SectionId == sec_id && t1.ExamId == examId
                       select new ClassStudent { Subject = t3.SubjectName, SubjectId = t3.SubjectId, Mark = t1.Mark.Value, MarkId = t1.StudentMarkId, MaxMark = t4.MaxMark.Value, MinMark = t4.MinMark.Value }).ToList();
            return ans;
        }

        public bool checkMark(int acid, int cid, int sec_id, int sid, int min, int max)
        {
            var ans = dc.Marks.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) && q.SubjectId.Value.Equals(sid) && q.MinMark.Value.Equals(min) && q.MaxMark.Value.Equals(max)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool checkMark(int acid, int cid, int sec_id, int sid)
        {
            var ans = dc.Marks.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) && q.SubjectId.Value.Equals(sid)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<MarkDetails> getMark()
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.Subjects
                       from t4 in dc.Marks
                       from t5 in dc.Sections
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.SubjectId == t4.SubjectId && t5.SectionId == t4.SectionId

                       select new {acid=t1.AcademicYearId, AcYear = t1.AcademicYear1, cid=t2.ClassId, Class = t2.ClassType, Subject = t3.SubjectName, MarkId = t4.MarkId, MaxMark = t4.MaxMark, MinMark = t4.MinMark, Section = t5.SectionName, SectionId = t5.SectionId }).ToList().
                       Select(q => new MarkDetails { AcYearId = q.acid, AcademicYear = q.AcYear, ClassId = q.cid, Class = q.Class, Subject = q.Subject, MId = QSCrypt.Encrypt(q.MarkId.ToString()), MaximumMark = q.MaxMark, MinimumMark = q.MinMark, Section = q.Section, SectionId = q.SectionId }).ToList();
            return ans;
        }

        public void addMark(int acid, int cid, int sec_id, int sid, int min, int max)
        {
            Mark add = new Mark()
            {
                AcademicYearId = acid,
                ClassId = cid,
                SectionId = sec_id,
                SubjectId = sid,
                MinMark = min,
                MaxMark = max
            };
            dc.Marks.Add(add);
            dc.SaveChanges();
        }

        public void updateMark(int markId, int max, int min)
        {
            var update = dc.Marks.Where(q => q.MarkId.Equals(markId)).First();
            update.MaxMark = max;
            update.MinMark = min;
            dc.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}