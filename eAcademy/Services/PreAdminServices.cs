﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class PreAdminServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        
        public List<AdminSelectionList> GetAllList(int? acyear, int? Class)
        {
            var ans = 
                     (from a in dc.PreAdmissionStudentRegisters
                     from b in dc.PreAdmissionTransactions
                     from c in dc.PreAdmissionPrimaryUserRegisters
                     from d in dc.Classes
                     where a.AcademicyearId == acyear && b.StudentAdmissionId == a.StudentAdmissionId &&   c.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId && d.ClassId == Class && a.AdmissionClass == d.ClassId && a.Statusflag == "Enrolled"
                     select new 
                     {
                         StudentAdmissionId = a.StudentAdmissionId,

                         PrimaryUserName = c.PrimaryUserName,
                         StuFirstName = a.StuFirstName,
                         txt_AdmissionClass = d.ClassType,
                         Distance = a.Distance,
                         SiblingStatus = a.SiblingStatus,
                         WorkSameSchool = c.WorkSameSchool,
                         statusFlag=a.Statusflag,
                         ApplicationSource=a.ApplicationSource
                     }).ToList().Select(a => new AdminSelectionList
                     {
                         StudentAdmissionId = a.StudentAdmissionId,
                         ApplicationId = QSCrypt.Encrypt(a.StudentAdmissionId.ToString()),
                         PrimaryUserName = a.PrimaryUserName,
                         StuFirstName = a.StuFirstName,
                         txt_AdmissionClass = a.txt_AdmissionClass,
                         Distance = a.Distance+ "Km",
                         SiblingStatus = a.SiblingStatus,
                         WorkSameSchool = a.WorkSameSchool,
                         statusFlag = a.statusFlag,
                         ApplicationSource = a.ApplicationSource,
                         StudentName=a.StuFirstName,
                         ApplyClass=a.txt_AdmissionClass

                     }).ToList().Union((from a in dc.PreAdmissionStudentRegisters
                  from b in dc.PreAdmissionTransactions
                  from c in dc.PreAdmissionPrimaryUserRegisters
                  from d in dc.Classes
                  where a.AcademicyearId == acyear && b.StudentAdmissionId == a.StudentAdmissionId &&
                  c.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId &&
                  d.ClassId == Class && a.AdmissionClass == d.ClassId && a.Statusflag == "Pending"
                  select new 
                     {
                         StudentAdmissionId = a.StudentAdmissionId,
                         PrimaryUserName = c.PrimaryUserName,
                         StuFirstName = a.StuFirstName,
                         txt_AdmissionClass = d.ClassType,
                         Distance= a.Distance,
                         SiblingStatus = a.SiblingStatus,
                         WorkSameSchool = c.WorkSameSchool,
                         statusFlag = a.Statusflag,
                         ApplicationSource = a.ApplicationSource

                     }).ToList().Select(a => new AdminSelectionList
                     {
                         StudentAdmissionId = a.StudentAdmissionId,
                         ApplicationId = QSCrypt.Encrypt(a.StudentAdmissionId.ToString()),
                         PrimaryUserName = a.PrimaryUserName,
                         StuFirstName = a.StuFirstName,
                         txt_AdmissionClass = a.txt_AdmissionClass,
                         Distance = a.Distance+ "Km",
                         SiblingStatus = a.SiblingStatus,
                         WorkSameSchool = a.WorkSameSchool,
                         statusFlag = a.statusFlag,
                         StudentName = a.StuFirstName,
                         ApplyClass = a.txt_AdmissionClass,
                         ApplicationSource = a.ApplicationSource
                     }
                     ).ToList()).ToList();
            return ans;
                     
                //var s=  (from a in dc.PreAdmissionStudentRegisters
                //  from b in dc.PreAdmissionTransactions
                //  from c in dc.PreAdmissionPrimaryUserRegisters
                //  from d in dc.Classes
                //  where a.AcademicyearId == acyear && b.StudentAdmissionId == a.StudentAdmissionId &&
                //  c.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId &&
                //  d.ClassId == Class && a.AdmissionClass == d.ClassId && a.Statusflag == "Pending"
                //  select new 
                //     {
                //         StudentAdmissionId = a.StudentAdmissionId,
                //         PrimaryUserName = c.PrimaryUserName,
                //         StuFirstName = a.StuFirstName,
                //         txt_AdmissionClass = d.ClassType,
                //         Distance= a.Distance,
                //         SiblingStatus = a.SiblingStatus,
                //         WorkSameSchool = c.WorkSameSchool,
                //         statusFlag = a.Statusflag,
                //         ApplicationSource = a.ApplicationSource

                //     }).ToList().Select(a => new AdminSelectionList
                //     {
                //         StudentAdmissionId = a.StudentAdmissionId,
                //         ApplicationId = QSCrypt.Encrypt(a.StudentAdmissionId.ToString()),
                //         PrimaryUserName = a.PrimaryUserName,
                //         StuFirstName = a.StuFirstName,
                //         txt_AdmissionClass = a.txt_AdmissionClass,
                //         Distance = a.Distance+ "Km",
                //         SiblingStatus = a.SiblingStatus,
                //         WorkSameSchool = a.WorkSameSchool,
                //         statusFlag = a.statusFlag,
                //         StudentName = a.StuFirstName,
                //         ApplyClass = a.txt_AdmissionClass,
                //         ApplicationSource = a.ApplicationSource
                //     }
                //     ).ToList();
           

        }
        public List<AdminSelectionList> GetDistanceList(int? acyear,int? Class,int Distance)
        {
            var ans = (
                      from a in dc.PreAdmissionStudentRegisters
                      from b in dc.PreAdmissionTransactions
                      from c in dc.PreAdmissionPrimaryUserRegisters
                      from d in dc.Classes
                      where a.AcademicyearId == acyear && b.StudentAdmissionId == a.StudentAdmissionId && c.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId && d.ClassId == Class && a.AdmissionClass == d.ClassId && a.Distance <= Distance && a.Statusflag == "Enrolled"
                     select new 
                     {
                         StudentAdmissionId = a.StudentAdmissionId,
                         PrimaryUserName = c.PrimaryUserName,
                         StuFirstName = a.StuFirstName,
                         txt_AdmissionClass = d.ClassType,
                         Distance = a.Distance,
                         SiblingStatus = a.SiblingStatus,
                         WorkSameSchool = c.WorkSameSchool,
                         statusFlag = a.Statusflag,
                         ApplicationSource = a.ApplicationSource
                     }).ToList().Select(a => new AdminSelectionList {
                         StudentAdmissionId = a.StudentAdmissionId,
                         ApplicationId = QSCrypt.Encrypt(a.StudentAdmissionId.ToString()),
                         PrimaryUserName = a.PrimaryUserName,
                         StuFirstName = a.StuFirstName,
                         txt_AdmissionClass = a.txt_AdmissionClass,
                         Distance = a.Distance+"Km",
                         SiblingStatus = a.SiblingStatus,
                         WorkSameSchool = a.WorkSameSchool,
                         statusFlag = a.statusFlag,
                          StudentName=a.StuFirstName,
                         ApplyClass=a.txt_AdmissionClass,
                       ApplicationSource=a.ApplicationSource
                     
                     }).ToList();
            return ans;

        }
        public List<AdminSelectionList> GetSiblingList(int? acyear, int? Class, string sibling)
        {
            var ans = (
                     from a in dc.PreAdmissionStudentRegisters
                     from b in dc.PreAdmissionTransactions
                     from c in dc.PreAdmissionPrimaryUserRegisters
                     from d in dc.Classes
                     where a.AcademicyearId == acyear && b.StudentAdmissionId == a.StudentAdmissionId && c.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId && d.ClassId == Class && a.AdmissionClass == d.ClassId && a.SiblingStatus == sibling && a.Statusflag == "Enrolled"
                      select new 
                      {
                          StudentAdmissionId = a.StudentAdmissionId,
                          PrimaryUserName = c.PrimaryUserName,
                          StuFirstName = a.StuFirstName,
                          txt_AdmissionClass = d.ClassType,
                          Distance = a.Distance,
                          SiblingStatus = a.SiblingStatus,
                          WorkSameSchool = c.WorkSameSchool,
                          statusFlag = a.Statusflag,
                          ApplicationSource = a.ApplicationSource
                      }).ToList().Select(a => new AdminSelectionList
                      {
                          StudentAdmissionId = a.StudentAdmissionId,
                          ApplicationId = QSCrypt.Encrypt(a.StudentAdmissionId.ToString()),
                          PrimaryUserName = a.PrimaryUserName,
                          StuFirstName = a.StuFirstName,
                          txt_AdmissionClass = a.txt_AdmissionClass,
                          Distance = a.Distance + "Km",
                          SiblingStatus = a.SiblingStatus,
                          WorkSameSchool = a.WorkSameSchool,
                          statusFlag = a.statusFlag,
                           StudentName=a.StuFirstName,
                         ApplyClass=a.txt_AdmissionClass,
                          ApplicationSource = a.ApplicationSource
                      }).ToList();
            return ans;



        }
        public List<AdminSelectionList> GetWorkSameSchoolList(int? acyear, int? Class, string Work)
        {
            var ans = (
                    from a in dc.PreAdmissionStudentRegisters
                    from b in dc.PreAdmissionTransactions
                    from c in dc.PreAdmissionPrimaryUserRegisters
                    from d in dc.Classes
                    where a.AcademicyearId == acyear && b.StudentAdmissionId == a.StudentAdmissionId && c.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId && d.ClassId == Class && a.AdmissionClass == d.ClassId && c.WorkSameSchool == Work && a.Statusflag == "Enrolled"
                    select new 
                    {
                        StudentAdmissionId = a.StudentAdmissionId,
                        PrimaryUserName = c.PrimaryUserName,
                        StuFirstName = a.StuFirstName,
                        txt_AdmissionClass = d.ClassType,
                        Distance = a.Distance,
                        SiblingStatus = a.SiblingStatus,
                        WorkSameSchool = c.WorkSameSchool,
                        statusFlag = a.Statusflag,
                        ApplicationSource = a.ApplicationSource
                    }).ToList().Select(a => new AdminSelectionList
                    {
                        StudentAdmissionId = a.StudentAdmissionId,
                        ApplicationId = QSCrypt.Encrypt(a.StudentAdmissionId.ToString()),
                        PrimaryUserName = a.PrimaryUserName,
                        StuFirstName = a.StuFirstName,
                        txt_AdmissionClass = a.txt_AdmissionClass,
                        Distance = a.Distance + "Km",
                        SiblingStatus = a.SiblingStatus,
                        WorkSameSchool = a.WorkSameSchool,
                        statusFlag = a.statusFlag,
                         StudentName=a.StuFirstName,
                         ApplyClass=a.txt_AdmissionClass,
                        ApplicationSource = a.ApplicationSource
                    }).ToList();
            return ans;
        }
        public List<AdminSelectionList> GetPendingList(int? acyear, int? Class, string pending)
        {
            var ans = (
                   from a in dc.PreAdmissionStudentRegisters
                   from b in dc.PreAdmissionTransactions
                   from c in dc.PreAdmissionPrimaryUserRegisters
                   from d in dc.Classes
                   where a.AcademicyearId == acyear && b.StudentAdmissionId == a.StudentAdmissionId && c.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId && d.ClassId == Class && a.AdmissionClass == d.ClassId && a.Statusflag == pending
                    select new 
                    {
                        StudentAdmissionId = a.StudentAdmissionId,
                        PrimaryUserName = c.PrimaryUserName,
                        StuFirstName = a.StuFirstName,
                        txt_AdmissionClass = d.ClassType,
                        Distance = a.Distance,
                        SiblingStatus = a.SiblingStatus,
                        WorkSameSchool = c.WorkSameSchool,
                        statusFlag = a.Statusflag,
                        ApplicationSource = a.ApplicationSource
                    }).ToList().Select(a => new AdminSelectionList
                    {
                        StudentAdmissionId = a.StudentAdmissionId,
                        ApplicationId = QSCrypt.Encrypt(a.StudentAdmissionId.ToString()),
                        PrimaryUserName = a.PrimaryUserName,
                        StuFirstName = a.StuFirstName,
                        txt_AdmissionClass = a.txt_AdmissionClass,
                        Distance = a.Distance+"Km",
                        SiblingStatus = a.SiblingStatus,
                        WorkSameSchool = a.WorkSameSchool,
                        statusFlag = a.statusFlag,
                         StudentName=a.StuFirstName,
                         ApplyClass=a.txt_AdmissionClass,
                        ApplicationSource = a.ApplicationSource
                    }).ToList();
            return ans;

        }
        public List<AdminSelectionList> GetParticularStatusflagList(int? acyear, int? Class, string status)
        {
            var ans = (
                    from a in dc.PreAdmissionStudentRegisters
                    from b in dc.PreAdmissionTransactions
                    from c in dc.PreAdmissionPrimaryUserRegisters
                    from d in dc.Classes
                    where a.AcademicyearId == acyear && b.StudentAdmissionId == a.StudentAdmissionId && c.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId && d.ClassId == Class && a.AdmissionClass == d.ClassId && a.Statusflag == status
                    //&& a.SendEmailStatus == "No"
                    select new 
                    {
                        StudentAdmissionId = a.StudentAdmissionId,
                        PrimaryUserName = c.PrimaryUserName,
                        StuFirstName = a.StuFirstName,
                        txt_AdmissionClass = d.ClassType,
                        Distance = a.Distance,
                        SiblingStatus = a.SiblingStatus,
                        WorkSameSchool = c.WorkSameSchool,
                        statusFlag = a.Statusflag,
                        ApplicationSource = a.ApplicationSource
                    }).ToList().Select(a => new AdminSelectionList
                    {
                        StudentAdmissionId = a.StudentAdmissionId,
                        ApplicationId = QSCrypt.Encrypt(a.StudentAdmissionId.ToString()),
                        PrimaryUserName = a.PrimaryUserName,
                        StuFirstName = a.StuFirstName,
                        txt_AdmissionClass = a.txt_AdmissionClass,
                        Distance = a.Distance + "Km",
                        SiblingStatus = a.SiblingStatus,
                        WorkSameSchool = a.WorkSameSchool,
                        statusFlag = a.statusFlag,
                         StudentName=a.StuFirstName,
                         ApplyClass=a.txt_AdmissionClass,
                        ApplicationSource = a.ApplicationSource
                    }).ToList();
            return ans;
        }
        public List<AdminSelectionList> GetInterViewStatusflagList(int? acyear, int? Class, string status)
        {
            var ans = (
                    from a in dc.PreAdmissionStudentRegisters
                    from b in dc.PreAdmissionTransactions
                    from c in dc.PreAdmissionPrimaryUserRegisters
                    from d in dc.Classes
                    where a.AcademicyearId == acyear && b.StudentAdmissionId == a.StudentAdmissionId && c.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId && d.ClassId == Class && a.AdmissionClass == d.ClassId && a.Statusflag == status
                    select new 
                    {
                        StudentAdmissionId = a.StudentAdmissionId,

                        PrimaryUserName = c.PrimaryUserName,
                        StuFirstName = a.StuFirstName,
                        txt_AdmissionClass = d.ClassType,
                        Distance = a.Distance,
                        SiblingStatus = a.SiblingStatus,
                        WorkSameSchool = c.WorkSameSchool,
                        statusFlag = a.Statusflag,
                        ApplicationSource = a.ApplicationSource
                    }).ToList().Select(a => new AdminSelectionList
                    {
                        StudentAdmissionId = a.StudentAdmissionId,
                        ApplicationId = QSCrypt.Encrypt(a.StudentAdmissionId.ToString()),
                        PrimaryUserName = a.PrimaryUserName,
                        StuFirstName = a.StuFirstName,
                        txt_AdmissionClass = a.txt_AdmissionClass,
                        Distance = a.Distance + "Km",
                        SiblingStatus = a.SiblingStatus,
                        WorkSameSchool = a.WorkSameSchool,
                        statusFlag = a.statusFlag,
                         StudentName=a.StuFirstName,
                         ApplyClass=a.txt_AdmissionClass,
                        ApplicationSource = a.ApplicationSource
                    }).ToList();
            return ans;
        }



        public List<AdminSelectionList> GetNotFeePaidList(int? acyear, int? Class, string status)
        {
            var date = DateTimeByZone.getCurrentDateTime().Date;
            var ans = (
                    from a in dc.PreAdmissionStudentRegisters
                    from b in dc.PreAdmissionTransactions
                    from c in dc.PreAdmissionPrimaryUserRegisters
                    from d in dc.Classes
                    where a.AcademicyearId == acyear 
                    && b.StudentAdmissionId == a.StudentAdmissionId 
                    && c.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId
                    && d.ClassId == Class 
                    && a.AdmissionClass == d.ClassId 
                    && a.Statusflag == "Conditionaloffer" 
                    && a.FeePayLastDate<date
                    select new 
                    {
                        StudentAdmissionId = a.StudentAdmissionId,
                        PrimaryUserName = c.PrimaryUserName,
                        StuFirstName = a.StuFirstName,
                        txt_AdmissionClass = d.ClassType,
                        Distance = a.Distance,
                        SiblingStatus = a.SiblingStatus,
                        WorkSameSchool = c.WorkSameSchool,
                        statusFlag = a.Statusflag,
                        ApplicationSource = a.ApplicationSource
                    }).ToList().Select(a => new AdminSelectionList
                    {
                        StudentAdmissionId = a.StudentAdmissionId,
                        ApplicationId = QSCrypt.Encrypt(a.StudentAdmissionId.ToString()),
                        PrimaryUserName = a.PrimaryUserName,
                        StuFirstName = a.StuFirstName,
                        txt_AdmissionClass = a.txt_AdmissionClass,
                        Distance = a.Distance+"Km",
                        SiblingStatus = a.SiblingStatus,
                        WorkSameSchool = a.WorkSameSchool,
                        statusFlag = a.statusFlag,
                        StudentName = a.StuFirstName,
                        ApplyClass = a.txt_AdmissionClass,
                        ApplicationSource = a.ApplicationSource
                    }).ToList();
            return ans;
        }

        public List<M_FeeStructure> getallFeesByID(int acid, int cid)
        {
            int termid = 0;
            var ans = (
                  (from t1 in dc.AcademicYears
                   from t2 in dc.Classes
                   from t3 in dc.FeeCategories
                   from t4 in dc.Fees
                   where t4.AcademicYearId == acid && t4.ClassId == cid && t1.AcademicYearId == t4.AcademicYearId &&
                         t2.ClassId == cid &&
                         t3.FeeCategoryId == t4.FeeCategoryId
                   select new M_FeeStructure
                   {
                       FeeCategory = t3.FeeCategoryName,
                       FeeCategoryId = t3.FeeCategoryId,
                       PaymentTypeId = t4.PaymentTypeId.Value,
                       TermId = termid,
                       Amount = t4.Amount,
                       Ldate = SqlFunctions.DateName("day", t4.LastDate).Trim() + "-" + SqlFunctions.StringConvert((double)t4.LastDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", t4.LastDate),
                       PaymentType = "Annual",
                       FeeId = t4.FeeId,
                       TermFeeId = termid,
                       ServicesTax = t4.Tax,
                       Total = t4.Total
                   })
                .Union
                (from t1 in dc.AcademicYears
                 from t2 in dc.Classes
                 from t3 in dc.FeeCategories
                 from t4 in dc.TermFees
                 from t5 in dc.Terms
                 where t4.AcademicYearId == acid && t4.ClassId == cid && t1.AcademicYearId == t4.AcademicYearId &&
                         t2.ClassId == cid &&
                         t3.FeeCategoryId == t4.FeeCategoryId &&
                         t4.TermId == t5.TermId

                 select new M_FeeStructure
                 {
                     FeeCategory = t3.FeeCategoryName,
                     FeeCategoryId = t3.FeeCategoryId,
                     PaymentTypeId = t4.PaymentTypeId.Value,
                     TermId = t4.TermId,
                     Amount = t4.Amount,
                     Ldate = SqlFunctions.DateName("day", t4.LastDate).Trim() + "-" + SqlFunctions.StringConvert((double)t4.LastDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", t4.LastDate),
                     PaymentType = t5.TermName,
                     FeeId = t4.FeeId.Value,
                     TermFeeId = t4.TermFeeID,
                     ServicesTax = t4.Tax,
                     Total = t4.Total
                 })
                    ).OrderBy(q => q.FeeCategoryId).ToList();
            return ans;
        }


      


        public List<M_FeeStructure> getallFeesPaidByID(int acid, int cid,int sid,int Stuid)
        {
            var ans = (
                  (from t1 in dc.AcademicYears
                   from t2 in dc.Classes
                   from t3 in dc.FeeCategories
                   from t4 in dc.Fees
                   from t5 in dc.FeeCollectionCategories
                   from t6 in dc.FeeCollections
                   where t5.AcademicYearId == acid && t5.ClassId == cid && t5.SectionId == sid && t5.StudentRegId == Stuid && t1.AcademicYearId == t4.AcademicYearId &&
                         t2.ClassId == cid &&
                         t3.FeeCategoryId == t4.FeeCategoryId &&
                         t4.AcademicYearId == t5.AcademicYearId && t4.ClassId == t5.ClassId && t4.Total == t5.TotalAmount && t6.FeeCollectionId == t5.FeeCollectionId
                   select new M_FeeStructure
                   {
                       FeeCategory = t3.FeeCategoryName,
                       FeeCategoryId = t3.FeeCategoryId,
                       PaymentTypeId = t4.PaymentTypeId.Value,
                       TermId = 0,
                       Amount = t4.Amount,
                       FeespaidDate = SqlFunctions.DateName("day", t6.CollectionDate).Trim() + "-" + SqlFunctions.StringConvert((double)t6.CollectionDate.Month).TrimStart() + "-" + SqlFunctions.DateName("year", t6.CollectionDate),
                       PaymentType = "Annual",
                       FeeId = t4.FeeId,
                       TermFeeId = 0,
                       ServicesTax = t4.Tax,
                       Total = t4.Total
                   })
                .Union
       (from t1 in dc.AcademicYears
        from t2 in dc.Classes
        from t3 in dc.FeeCategories
        from t4 in dc.TermFees
        from t5 in dc.Terms
        from t6 in dc.FeeCollectionCategories
         from t7 in dc.FeeCollections
        where t6.AcademicYearId == acid && t6.ClassId == cid && t6.SectionId == sid && t6.StudentRegId == Stuid && t1.AcademicYearId == t4.AcademicYearId &&
                t2.ClassId == cid && t4.FeeCategoryId == t6.FeeCategoryId &&
                t3.FeeCategoryId == t4.FeeCategoryId &&
                t4.AcademicYearId == t6.AcademicYearId && t4.ClassId == t6.ClassId && t4.Total == t6.TotalAmount && t4.TermFeeID == t6.PaymentTypeId && t7.FeeCollectionId==t6.FeeCollectionId &&
                t4.TermId == t5.TermId

        select new M_FeeStructure
        {
            FeeCategory = t3.FeeCategoryName,
            FeeCategoryId = t3.FeeCategoryId,
            PaymentTypeId = t4.PaymentTypeId.Value,
            TermId = t5.TermId,
            Amount = t6.Amount,
            FeespaidDate = SqlFunctions.DateName("day", t7.CollectionDate).Trim() + "-" + SqlFunctions.StringConvert((double)t7.CollectionDate.Month).TrimStart() + "-" + SqlFunctions.DateName("year", t7.CollectionDate),
            PaymentType = t5.TermName,
            FeeId = t4.FeeId.Value,
            TermFeeId = t4.TermFeeID,
            ServicesTax = t6.ServicesTax,
            Total = t6.TotalAmount
        })

                    ).OrderBy(q => q.FeeCategoryId).ToList();
            return ans;
        }
        public List<M_FeeStructure> getallFeesDueByID(int acid, int cid, int sid, int Stuid)
        {
            var ans = (from a in dc.FeeCollectionCategories
                       from b in dc.Fees
                       where a.AcademicYearId == acid && a.ClassId == cid && a.SectionId == sid && a.StudentRegId == Stuid &&b.AcademicYearId==a.AcademicYearId && b.ClassId==a.ClassId
                       && b.FeeCategoryId !=a.FeeCategoryId
                       select new M_FeeStructure {
                       FeeCategoryId=b.FeeCategoryId
                       }).Distinct().OrderBy(q=>q.FeeCategoryId).ToList();
            return ans;
        }


        public List<Term> GetAcademictermId(int acyearid)
        {
            var ans = (from a in dc.AcademicYears
                       from b in dc.Terms
                       where a.AcademicYearId == acyearid && b.AcademicYearId == a.AcademicYearId
                       select b).OrderBy(s => s.TermId).ToList();
            return ans;
        }


        public int GetAcademicfirsttermId(int acyearid)
        {
            var ans = (from a in dc.AcademicYears
                       from b in dc.Terms
                       where a.AcademicYearId == acyearid && b.AcademicYearId == a.AcademicYearId 
                       select b).OrderBy(s => s.TermId).ToList();
            //return ans;
            var e=0;
            if (ans.Count==0)
            {
                e = 0;
            }
            else
            {
             e= (from a in dc.AcademicYears
                           from b in dc.Terms
                           where a.AcademicYearId == acyearid && b.AcademicYearId == a.AcademicYearId
                           select b).OrderBy(s => s.TermId).FirstOrDefault().TermId;
            }
            return e;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}