﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class FacultyleaverequestServices : IDisposable
    {
        EacademyEntities ee = new EacademyEntities();

        public void AddFacultyLeaveRequest(int FacultyId, DateTime fdate, DateTime toDate, int NoOfDays, string reason, int ApproverId)
        {
            FacultyLeaveRequest add = new FacultyLeaveRequest()
            {
                EmployeeRegisterId = FacultyId,
                FromDate = fdate,
                ToDate = toDate,
                NumberOfDays = NoOfDays,
                LeaveReason = reason,
                Status = "Pending",
                ApproverId = ApproverId
            };
            ee.FacultyLeaveRequests.Add(add);
            ee.SaveChanges();
        }
        public List<LeaveRequestStatus> getFacultyLeaveRequest(int FacultyId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in ee.FacultyLeaveRequests
                        where a.EmployeeRegisterId == FacultyId &&
                        a.FromDate >= fdate &&
                          a.FromDate <= toDate

                        select new LeaveRequestStatus
                        {
                            FromDate = a.FromDate,
                            ToDate = a.ToDate,
                            NumberOfDays = a.NumberOfDays,
                            LeaveReason = a.LeaveReason,
                            Status = a.Status
                        }).ToList();
            return list;
        }
        
        public void updateFacultyLeaveReuest(int Req_id, int ApproverId, string decision)
        {           
            var update = (from a in ee.FacultyLeaveRequests where a.RequestId == Req_id select a).First();
            update.Status = decision;
            update.ApproverId = ApproverId;
            ee.SaveChanges();
        }

        public FacultyLeaveRequest getLeaveDates(int Req_id, string status)
        {
            var getRow = (from a in ee.FacultyLeaveRequests where a.RequestId == Req_id && a.Status == status select a).FirstOrDefault();
            return getRow;
        }

        public List<LeaveRequestStatus> getFacultyLeaveRequest(int FacultyId, DateTime? fdate, DateTime? toDate)
        {
            var list = (from a in ee.FacultyLeaveRequests
                        where a.EmployeeRegisterId == FacultyId &&
                        a.FromDate >= fdate &&
                         a.FromDate <= toDate
                        select new
                        {
                            FromDate = a.FromDate,
                            ToDate = a.ToDate,
                            NumberOfDays = a.NumberOfDays,
                            LeaveReason = a.LeaveReason,
                            Status = a.Status,
                            requestId = a.RequestId     
                        }).ToList().Select(a => new LeaveRequestStatus
                        {
                            From_Date = a.FromDate.Value.ToString("dd MMM, yyyy"),
                            To_Date = a.ToDate.Value.ToString("dd MMM, yyyy"),
                            NumberOfDays = a.NumberOfDays,
                            LeaveReason = a.LeaveReason,
                            Status = a.Status,
                            Request_Id = QSCrypt.Encrypt(a.requestId.ToString())
                        }).ToList();    
            return list;
        }

        public FacultyLeaveRequest FacultyLeaveReq(int id)
        {
            var getRow = (from a in ee.FacultyLeaveRequests where a.RequestId == id select a).FirstOrDefault();
            return getRow;
        }

        public string IsLeaveReqExist(DateTime fdate, DateTime todate, int facultyId)
        {
           // var row = (from a in ee.FacultyLeaveRequests where a.FromDate == fdate && a.ToDate == todate select a).FirstOrDefault();

            var LeaveReqList = (from a in ee.FacultyLeaveRequests
                                where a.FromDate >= fdate.Date &&
                                a.FromDate <= todate.Date &&
                                a.EmployeeRegisterId == facultyId
                                select new
                                {
                                    id = a.RequestId,
                                    fdate = a.FromDate,
                                    tdate = a.ToDate
                                }).ToList();

            for (int i = 0; i < LeaveReqList.Count; i++)
            {
                DateTime? fd = LeaveReqList[i].fdate;
                DateTime? td = LeaveReqList[i].tdate;

                if(fdate >= fd && fdate <= td)
                {
                    return "dateExist";
                }
                if (todate >= fd && todate <= td)
                {
                    return "dateExist";
                }
            }

            var LeaveReqList1 = (from a in ee.FacultyLeaveRequests
                                where a.ToDate >= fdate.Date &&
                                a.ToDate <= todate.Date &&
                                a.EmployeeRegisterId == facultyId
                                select new
                                {
                                    id = a.RequestId,
                                    fdate = a.FromDate,
                                    tdate = a.ToDate
                                }).ToList();

            for (int j = 0; j < LeaveReqList1.Count; j++)
            {
                DateTime? fd1 = LeaveReqList1[j].fdate;
                DateTime? td1 = LeaveReqList1[j].tdate;

                if (fdate >= fd1 && fdate <= td1)
                {
                    return "dateExist";
                }
                if (todate >= fd1 && todate <= td1)
                {
                    return "dateExist";
                }
            }

            var LeaveReqList2 = (from a in ee.FacultyLeaveRequests
                                 where a.ToDate >= todate.Date &&
                                 a.EmployeeRegisterId == facultyId
                                 select new
                                 {
                                     id = a.RequestId,
                                     fdate = a.FromDate,
                                     tdate = a.ToDate
                                 }).ToList();
            for (int j = 0; j < LeaveReqList2.Count; j++)
            {
                DateTime? fd2 = LeaveReqList2[j].fdate;
                DateTime? td2 = LeaveReqList2[j].tdate;
                if (fdate >= fd2 && fdate <= td2)
                {
                    return "dateExist";
                }
                if (todate >= fd2 && todate <= td2)
                {
                    return "dateExist";
                }
            }

            return "dateFree";
        }

        public string IsLeaveReqExist(DateTime fdate, DateTime todate, int facultyId, int RequestId)
        {
            // var row = (from a in ee.FacultyLeaveRequests where a.FromDate == fdate && a.ToDate == todate select a).FirstOrDefault();

            var LeaveReqList = (from a in ee.FacultyLeaveRequests
                                where a.FromDate >= fdate.Date &&
                                a.FromDate <= todate.Date &&
                                a.EmployeeRegisterId == facultyId &&
                                a.RequestId != RequestId
                                select new
                                {
                                    id = a.RequestId,
                                    fdate = a.FromDate,
                                    tdate = a.ToDate
                                }).ToList();

            for (int i = 0; i < LeaveReqList.Count; i++)
            {
                DateTime? fd = LeaveReqList[i].fdate;
                DateTime? td = LeaveReqList[i].tdate;

                if (fdate >= fd && fdate <= td)
                {
                    return "dateExist";
                }
                if (todate >= fd && todate <= td)
                {
                    return "dateExist";
                }
            }

            var LeaveReqList1 = (from a in ee.FacultyLeaveRequests
                                 where a.ToDate >= fdate.Date &&
                                 a.ToDate <= todate.Date &&
                                 a.RequestId != RequestId
                                 select new
                                 {
                                     id = a.RequestId,
                                     fdate = a.FromDate,
                                     tdate = a.ToDate
                                 }).ToList();

            for (int j = 0; j < LeaveReqList1.Count; j++)
            {
                DateTime? fd1 = LeaveReqList1[j].fdate;
                DateTime? td1 = LeaveReqList1[j].tdate;

                if (fdate >= fd1 && fdate <= td1)
                {
                    return "dateExist";
                }
                if (todate >= fd1 && todate <= td1)
                {
                    return "dateExist";
                }
            }

            var LeaveReqList2 = (from a in ee.FacultyLeaveRequests
                                 where a.ToDate >= todate.Date &&
                                 a.RequestId != RequestId
                                 select new
                                 {
                                     id = a.RequestId,
                                     fdate = a.FromDate,
                                     tdate = a.ToDate
                                 }).ToList();
            for (int j = 0; j < LeaveReqList2.Count; j++)
            {
                DateTime? fd2 = LeaveReqList2[j].fdate;
                DateTime? td2 = LeaveReqList2[j].tdate;
                if (fdate >= fd2 && fdate <= td2)
                {
                    return "dateExist";
                }
                if (todate >= fd2 && todate <= td2)
                {
                    return "dateExist";
                }
            }

            return "dateFree";
        }

        //public FacultyLeaveRequest IsLeaveReqExist(DateTime fdate, DateTime todate, int eid)
        //{
        //    var row = (from a in ee.FacultyLeaveRequests where a.FromDate == fdate && a.ToDate == todate && a.RequestId != eid select a).FirstOrDefault();
        //    return row;
        //}

        public void EditLeaveReq(int Req_id, int facultyId, DateTime fdate, DateTime toDate, int NoOfDays, string reason)
        {
            var update = (from a in ee.FacultyLeaveRequests where a.RequestId == Req_id && a.EmployeeRegisterId == facultyId select a).First();
            update.FromDate = fdate;
            update.ToDate = toDate;
            update.NumberOfDays = NoOfDays;
            update.LeaveReason = reason;
            ee.SaveChanges();
        }
        public void AddFacultyLeaveRequest(int FacultyId, DateTime fdate, DateTime toDate, int NoOfDays, string reason)
        {
            FacultyLeaveRequest add = new FacultyLeaveRequest()
            {
                EmployeeRegisterId = FacultyId,
                FromDate = fdate,
                ToDate = toDate,
                NumberOfDays = NoOfDays,
                LeaveReason = reason,
                Status = "Pending"
            };
            ee.FacultyLeaveRequests.Add(add);
            ee.SaveChanges();
        }

        public FacultyLeaveRequest getLastLeavReq(int EmployeeRegId)
        {
            var LeaveRequestStatus1 = (from a in ee.FacultyLeaveRequests where a.EmployeeRegisterId == EmployeeRegId select a).OrderByDescending(x => x.FromDate).FirstOrDefault();
            return LeaveRequestStatus1;
        }

        public void delLeaveReq(int Did)
        {
            var getRow = (from a in ee.FacultyLeaveRequests where a.RequestId == Did select a).FirstOrDefault();
            ee.FacultyLeaveRequests.Remove(getRow);
            ee.SaveChanges();
        }

        //jegadeesh
        public List<Ranklist> GetEmployeeLeave(int yid, DateTime from1, DateTime to)
        {
            var ans = (from a in ee.FacultyLeaveRequests
                       from b in ee.TechEmployees

                       where a.AcademicYearId == yid && a.FromDate >= from1 && a.ToDate <= to && a.EmployeeRegisterId == b.EmployeeRegisterId
                       select new Ranklist
                       {
                           rollnumber = b.EmployeeId,
                           stuname = b.EmployeeName,
                           from1 = a.FromDate,
                           to1 = a.ToDate,
                           status = a.Status,
                           totalleave = a.NumberOfDays


                       }).ToList();
            return ans;
        }
        public List<M_EmployeeLeaveRequest> GetEmployeeLeaveRequest(DateTime? from1, DateTime? to)
        {
            var ans = (from a in ee.FacultyLeaveRequests
                       from b in ee.TechEmployees

                       where  a.FromDate >= from1 && a.ToDate <= to && a.EmployeeRegisterId == b.EmployeeRegisterId
                       select new M_EmployeeLeaveRequest
                       {
                           EmployeeId = b.EmployeeId,
                           EmployeeName = b.EmployeeName,
                           sdate=a.FromDate,
                           Edate=a.ToDate,
                           //FromDate = SqlFunctions.DateName("day", a.FromDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.FromDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.FromDate),
                          // ToDate = SqlFunctions.DateName("day", a.ToDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.ToDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.ToDate),
                           status = a.Status,
                           TotalLeave = a.NumberOfDays


                       }).ToList().Select(a=>new M_EmployeeLeaveRequest{
                       
                       EmployeeId=a.EmployeeId,
                       EmployeeName=a.EmployeeName,
                       FromDate=a.sdate.Value.ToString("MMM dd, yyyy"),
                       ToDate=a.Edate.Value.ToString("MMM dd, yyyy"),
                       status=a.status,
                       TotalLeave=a.TotalLeave
                       
                       }).ToList();
            return ans;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ee.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}