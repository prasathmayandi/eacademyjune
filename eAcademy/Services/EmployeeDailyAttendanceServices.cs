﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class EmployeeDailyAttendanceServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public EmployeeDailyAttendance AttendanceExist(int yearId, DateTime date)
        {
            var getRow = (from a in db.EmployeeDailyAttendances where a.AcademicYearId == yearId && a.DateOfAttendance == date select a).FirstOrDefault();
            return getRow;
        }

        public void updateEmployeeDailyAttendance(int RegId, int year, DateTime date, string status, string comment, DateTime updatedDate)
        {
            var getRow = (from a in db.EmployeeDailyAttendances where a.EmployeeRegisterId == RegId && a.AcademicYearId == year && a.DateOfAttendance == date select a).FirstOrDefault();
            getRow.AttendanceStatus = status;
            getRow.LeaveReason = comment;
            getRow.AttendanceUpdatedDate = updatedDate;
            db.SaveChanges();
        }

        public List<AttendanceExistList> getAttendanceExistList(int yearId, DateTime date)
        {
            var getRow = (from a in db.EmployeeDailyAttendances
                          from b in db.OtherEmployees
                          from c in db.tblAttendanceStatus
                          where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                          a.AttendanceStatus == c.AttendanceStatusValue &&
                          a.AcademicYearId == yearId &&
                          a.DateOfAttendance == date 
                          orderby b.EmployeeId
                          select new
                          {
                              EmployeeRegId = a.EmployeeRegisterId,
                              EmployeeId = b.EmployeeId,
                              EmployeeName = b.EmployeeName,
                              status = a.AttendanceStatus,
                              comment = a.LeaveReason,
                              AttendanceStatusName = c.AttendanceStatusName,
                              AttendanceId = a.Id
                          }).OrderBy(x=>x.EmployeeRegId).ToList().Select(c => new AttendanceExistList
                          {
                              EmployeeRegId = QSCrypt.Encrypt(c.EmployeeRegId.ToString()),
                              EmployeeId = c.EmployeeId,
                              EmployeeName = c.EmployeeName,
                              status = c.status,
                              comment = c.comment,
                              AttendanceStatusName = c.AttendanceStatusName
                          }).ToList();
            return getRow;
        }

        public void addEmployeeDailyAttendance(int AcademicYearId, int EmployeeRegisterId, DateTime CurrentDate, string AttendanceStatus, string LeaveReason, DateTime AttendanceMarkedDate)
        {
            EmployeeDailyAttendance add = new EmployeeDailyAttendance()
            {
                AcademicYearId = AcademicYearId,
                EmployeeRegisterId = EmployeeRegisterId,
                DateOfAttendance = CurrentDate,
                AttendanceStatus = AttendanceStatus,
                LeaveReason = LeaveReason,
                AttendanceMarkedDate = AttendanceMarkedDate
            };
            db.EmployeeDailyAttendances.Add(add);
            db.SaveChanges();
        }

        public List<FacultyAttendanceReport> EmployeeAttendanceList(int? empRegisterId, DateTime? sDate, DateTime? eDate)
        {
            var list = (from a in db.EmployeeDailyAttendances
                        where a.EmployeeRegisterId == empRegisterId &&
                              a.DateOfAttendance >= sDate &&
                              a.DateOfAttendance <= eDate
                        select new
                        {
                            Date = a.DateOfAttendance,
                            Status = a.AttendanceStatus,
                            Reason = a.LeaveReason,
                            fdate_search = a.DateOfAttendance,
                            toDate_search = a.DateOfAttendance,
                            empId_search = a.EmployeeRegisterId
                        }).ToList().Select(c => new FacultyAttendanceReport
                        {
                            Date1 = c.Date.Value.ToString("MMM dd,yyyy"),
                            Status = c.Status,
                            Reason = c.Reason,
                            Att_SectionStudent = c.empId_search,
                            txt_FromDate = c.fdate_search,
                            txt_ToDate = c.toDate_search
                        }).ToList();
            return list;

        }

        public List<Ranklist> GetOtherEmployeeDailyAttendance(int yid, DateTime? da,int designationid)
        {
            var ans = (from a in db.EmployeeDailyAttendances
                       from b in db.OtherEmployees

                       where a.AcademicYearId == yid && a.DateOfAttendance == da && b.EmployeeDesignationId == designationid && a.EmployeeRegisterId == b.EmployeeRegisterId
                       select new Ranklist
                       {
                           rollnumber = b.EmployeeId,
                           stuname = b.EmployeeName ,
                           status = a.AttendanceStatus
                       }).ToList();
            return ans;
        }
        public List<EmployeeDailyAttendance> OtherEmployeeDailyPresentAttendance(int yid, DateTime da)
        {
            var ans = (from a in db.EmployeeDailyAttendances

                       where a.AcademicYearId == yid && a.DateOfAttendance == da && a.AttendanceStatus == "P"
                       select a).ToList();
            return ans;
        }
        public List<EmployeeDailyAttendance> OtherEmployeeDailyAbsentAttendance(int yid, DateTime da)
        {
            var ans = (from a in db.EmployeeDailyAttendances

                       where a.AcademicYearId == yid && a.DateOfAttendance == da && a.AttendanceStatus == "A"
                       select a).ToList();
            return ans;
        }
        public List<Ranklist> GetOtherEmployeeFromToAttendance(int yid, DateTime? from1, DateTime? to,int designationid)
        {
            var ans = (from a in db.EmployeeDailyAttendances
                       from b in db.OtherEmployees

                       where a.AcademicYearId == yid && a.DateOfAttendance >= from1 && a.DateOfAttendance <= to && b.EmployeeDesignationId == designationid && a.EmployeeRegisterId == b.EmployeeRegisterId
                       select new Ranklist
                       {
                           rollnumber = b.EmployeeId,
                           stuname = b.EmployeeName,
                           date = a.DateOfAttendance.Value,
                           status = a.AttendanceStatus
                       }).Distinct().OrderBy(q => q.date).ToList();
            return ans;
        }
        public List<EmployeeDailyAttendance> OtherEmployeeFromToPresentAttendance(int yid, DateTime from1, DateTime to, string r)
        {
            var ans = (from a in db.EmployeeDailyAttendances
                       from b in db.OtherEmployees
                       where a.AcademicYearId == yid && b.EmployeeId == r && a.EmployeeRegisterId == b.EmployeeRegisterId && a.AttendanceStatus == "P" && a.DateOfAttendance >= from1 && a.DateOfAttendance <= to
                       select a).ToList();
            return ans;
        }

        public List<Ranklist> GetOtherEmployeeMonthAttendance(int yid, int? id,int designationid)
        {
            var ans = (from a in db.EmployeeDailyAttendances
                       from b in db.OtherEmployees

                       where a.AcademicYearId == yid && a.DateOfAttendance.Value.Month == id && b.EmployeeDesignationId == designationid && a.EmployeeRegisterId == b.EmployeeRegisterId
                       select new Ranklist
                       {
                           rollnumber = b.EmployeeId,
                           stuname = b.EmployeeName,
                           date = a.DateOfAttendance.Value,
                           status = a.AttendanceStatus,
                           d = SqlFunctions.DateName("day", a.DateOfAttendance.Value).Trim()
                       }).OrderBy(q => q.date).ToList();
            return ans;
        }
        public List<EmployeeDailyAttendance> OtherEmployeeMonthPresentAttendance(int yid, int id, string r)
        {
            var ans = (from a in db.EmployeeDailyAttendances
                       from b in db.OtherEmployees
                       where a.AcademicYearId == yid && b.EmployeeId == r && a.EmployeeRegisterId == b.EmployeeRegisterId && a.AttendanceStatus == "P" && a.DateOfAttendance.Value.Month == id
                       select a).ToList();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}