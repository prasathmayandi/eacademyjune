﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class GradeTypeServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public IEnumerable<object> getGradeType()
        {
            var ans = dc.GradeTypes.Select(q => q).Select(s => new { GradeTypeId = s.GradeTypeId, GradeTypeName =s.GradeTypeName}).ToList();
            return ans;
        }

        public List<GradeTypeList> getGradeTypeList()
        {
            var ans =( dc.GradeTypes.Select(q => q).

                Select(s => new GradeTypeList { GradeTypeId = s.GradeTypeId, GradeType = s.GradeTypeName, Status =s.GradeTypeStatus.Value}).ToList()).
                Select(s1 => new GradeTypeList { IdGradeType = QSCrypt.Encrypt(s1.GradeTypeId.ToString()), GradeTypeId = s1.GradeTypeId, GradeType = s1.GradeType, Status = s1.Status, addview ="Add/View"}).ToList();
            return ans;
        }

        public bool checkGradeType(string gradeType)
        {
            var ans = dc.GradeTypes.Where(q => q.GradeTypeName.Equals(gradeType)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool checkGradeTypeexist(int gid,string gradeType)
        {
            var ans = dc.GradeTypes.Where(q => q.GradeTypeName.Equals(gradeType) && q.GradeTypeId != gid).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int addGradeType(string GradeTypeName)
        {
            GradeType gtype = new GradeType()
            {
                GradeTypeName = GradeTypeName,
                GradeTypeStatus = true
            };
            dc.GradeTypes.Add(gtype);
            dc.SaveChanges();
            int id = gtype.GradeTypeId;
            return id;         
        }

        //public IEnumerable<object> getGradeTypeId()
        //{
        //    var ans1 = dc.GradeTypes.Select(q => q).Select(s => new { GradeTypeId = s.GradeTypeId, GradeTypeName = s.GradeTypeName }).ToList();
        //    return ans1;
        //}



        public void UpdateGradeType(int gid, string GradeTypeName,bool Status)
        {
            var ans = dc.GradeTypes.Where(q => q.GradeTypeId == gid).FirstOrDefault();
            if (ans != null)
            {
                ans.GradeTypeName = GradeTypeName;
                ans.GradeTypeStatus = Status;
                dc.SaveChanges();
            }
        }      

      
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}