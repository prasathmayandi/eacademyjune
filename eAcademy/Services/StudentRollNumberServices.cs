﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class StudentRollNumberServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public IEnumerable<Object> GetRollNumber(int passYearId, int passClassId, int passSectionId)
        {
            var RollNumberList = (from a in db.StudentRollNumbers
                                  from b in db.Students
                                  from c in db.AssignClassToStudents
                                  where a.StudentRegisterId == b.StudentRegisterId &&
                                        a.StudentRegisterId == c.StudentRegisterId &&
                                        a.AcademicYearId == c.AcademicYearId &&
                                        a.ClassId == c.ClassId &&
                                        a.SectionId == c.SectionId &&
                                        a.AcademicYearId == passYearId &&
                                        a.ClassId == passClassId &&
                                        a.SectionId == passSectionId &&
                                        b.StudentStatus==true &&
                                        c.Status == true
                                  select new
                                  {
                                      RollNumberId = a.RollNumberId,
                                      StudentRollNumber = a.RollNumber
                                  }).ToList();
            return RollNumberList;
        }

        public IEnumerable<Object> GetRollNumber(int yId, int cId, int sId,int passStartRollNumberId)
        {
            var RollNumberList = (from a in db.StudentRollNumbers
                                  from b in db.Students
                                  from c in db.AssignClassToStudents
                                  where a.StudentRegisterId == b.StudentRegisterId &&
                                        a.StudentRegisterId == c.StudentRegisterId &&
                                        a.AcademicYearId == c.AcademicYearId &&
                                        a.ClassId == c.ClassId &&
                                        a.SectionId == c.SectionId &&
                                        a.AcademicYearId == yId &&
                                        a.ClassId == cId &&
                                        a.SectionId == sId &&
                                        a.RollNumberId >= passStartRollNumberId &&
                                        b.StudentStatus == true &&
                                        c.Status == true
                                  select new
                                  {
                                      RollNumberId = a.RollNumberId,
                                      StudentRollNumber = a.RollNumber
                                  }).ToList();
            return RollNumberList;
        }

        public IEnumerable<Object> GetRollNumber1(int passYearId, int passClassId, int passSecId)
        {
            var RollNumberList = (from a in db.StudentRollNumbers
                                  where a.AcademicYearId == passYearId &&
                                     a.ClassId == passClassId &&
                                     a.SectionId == passSecId
                                  select new
                                  {
                                      RollNumber = a.RollNumber,
                                      RegId = a.StudentRegisterId
                                  }).ToList();
            return RollNumberList;
        }

        public IEnumerable<Object> ShowRollNumber(int? yearId, int? classId, int? secId)
        {
            var RollNumberList = (from a in db.StudentRollNumbers
                                  where a.AcademicYearId == yearId &&
                                  a.ClassId == classId &&
                                  a.SectionId == secId
                                  select new
                                  {
                                      rollNumber = a.RollNumber,
                                      RegId = a.StudentRegisterId
                                  }).ToList().AsEnumerable();
            return RollNumberList;
        }

        public IEnumerable<Object> ShowRollNumberEnd(int? yearId, int? classId, int? secId, int? startRegId)
        {
            var RollNumberList = (from a in db.StudentRollNumbers
                                  where a.AcademicYearId == yearId &&
                                  a.ClassId == classId &&
                                  a.SectionId == secId &&
                                  a.StudentRegisterId > startRegId
                                  select new
                                  {
                                      rollNumber = a.RollNumber,
                                      RegId = a.StudentRegisterId
                                  }).ToList().AsEnumerable();
            return RollNumberList;
        }

        public StudentRollNumber RollNumber(int RollNumberId)
        {
            var getRow = (from a in db.StudentRollNumbers where a.RollNumberId == RollNumberId select a).FirstOrDefault();
            return getRow;
        }

        public RollNumberList getRollNumberOrderByID(int acid, int cid, int sec_id)
        {
            var rollNumber = db.StudentRollNumbers.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id)).Select(s => new RollNumberList { RollNumberFormat = s.RollNumber, RollNumberFormatId = s.RollNumberId }).OrderByDescending(q => q.RollNumberFormatId).FirstOrDefault();
            return rollNumber;
        }

        public void addStudentRollNumber(int acid, int cid, int sec_id, int p, string roll_number)
        {
            StudentRollNumber number = new StudentRollNumber()
            {
                AcademicYearId = acid,
                ClassId = cid,
                SectionId = sec_id,
                StudentRegisterId = p,
                RollNumber = roll_number,
                Status = true
            };
            db.StudentRollNumbers.Add(number);
            db.SaveChanges();
            var update = db.AssignClassToStudents.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) && q.StudentRegisterId.Value.Equals(p)).First();
            update.HaveRollNumber = true;
            db.SaveChanges();
        }

        public StudentRollNumber getRollNumberDetail(int rollnumberId)
        {
            var getStartRollNumber = (from a in db.StudentRollNumbers where a.RollNumberId == rollnumberId select a).FirstOrDefault();
            return getStartRollNumber;
        }

        public StudentRollNumber checkRollNumberExist(int year, int classId, int studentRegisterId)
        {
            var getRow = (from a in db.StudentRollNumbers where a.AcademicYearId == year && a.ClassId == classId && a.StudentRegisterId == studentRegisterId && a.Status == true select a).FirstOrDefault();
            return getRow;
        }

        public void updateRollNumberStatus(int year, int classId, int studentRegisterId)
        {
            var getRow = (from a in db.StudentRollNumbers where a.AcademicYearId == year && a.ClassId == classId && a.StudentRegisterId == studentRegisterId && a.Status == true select a).FirstOrDefault();
            getRow.Status = false;
            db.SaveChanges();
        }

        public List<ExamAttendanceStudentList> GetStudentList(int YearId,int classId,int secId,int StartRollNumberId,int EndRollNumberId)
        {
            var list = (from a in db.StudentRollNumbers
                        from b in db.Students
                        orderby a.RollNumberId
                        where a.StudentRegisterId == b.StudentRegisterId &&
                        a.AcademicYearId == YearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.RollNumberId >= StartRollNumberId &&
                        a.RollNumberId <= EndRollNumberId &&
                        a.Status == true &&
                        b.StudentStatus == true
                        select new
                        {
                            StudentRegId = a.StudentRegisterId,
                            RollNumber = a.RollNumber,
                            RollNumberId = a.RollNumberId,
                            FirstName = b.FirstName,
                            LastName = b.LastName
                        }).ToList().Select(a => new ExamAttendanceStudentList
                        {
                            StudentRegId = QSCrypt.Encrypt(a.StudentRegId.ToString()),
                            RollNumberId = QSCrypt.Encrypt(a.RollNumberId.ToString()),
                            RollNumber = a.RollNumber,
                            StudentName = a.FirstName+" "+a.LastName
                        }).ToList();
            return list;
        }
        
        public StudentRollNumber get_sturollnumber(int std_id)
        {
            var stclass = (from a in db.StudentRollNumbers where std_id == a.StudentRegisterId select a).FirstOrDefault();
            return stclass;
        }

        public void ChangeStudentRollNumberstatus(int acyearid,int classid,int sectionid,int studentregisterid)
        {
            var update = db.StudentRollNumbers.Where(q => q.AcademicYearId == acyearid && q.ClassId == classid && q.SectionId == sectionid && q.StudentRegisterId == studentregisterid).FirstOrDefault();
            if(update !=null)
            {
                update.Status = false;
                db.SaveChanges();
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}