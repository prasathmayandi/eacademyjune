﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class ParentSiblingServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public PreAdmissionParentSibling getRow(int onlineRegId, int admissionId)
        {
            var getRow = (from a in dc.PreAdmissionParentSiblings where a.OnlineRegisterId == onlineRegId && a.StudentAdmissionId == admissionId select a).FirstOrDefault();
            return getRow;
        }
        public void AddSibling(int stuid, int regid, string years, string rolls, string classes, string sections)
        {
            PreAdmissionParentSibling add = new PreAdmissionParentSibling()
            {
                StudentAdmissionId = stuid,
                OnlineRegisterId = regid,
                SiblingAcademicYear = years,
                SiblingRollNumbers = rolls,
                SiblingClass = classes,
                SiblingSection = sections
            };
            dc.PreAdmissionParentSiblings.Add(add);
            dc.SaveChanges();

        }
        public void UpdateSibling(int stuid, int regid, string years, string rolls, string classes, string sections)
        {
            var getrow = (from a in dc.PreAdmissionParentSiblings
                          where a.StudentAdmissionId.Value.Equals(stuid) && a.OnlineRegisterId.Value.Equals(regid) && a.SiblingRollNumbers == rolls
                          select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.SiblingAcademicYear = years;
                getrow.SiblingRollNumbers = rolls;
                getrow.SiblingClass = classes;
                getrow.SiblingSection = sections;
                dc.SaveChanges();
            }
        }

        public void deleteSibling(int regid, int stuid)
        {
            var get = (from a in dc.PreAdmissionParentSiblings
                       where a.OnlineRegisterId.Value.Equals(regid) && a.StudentAdmissionId.Value.Equals(stuid)
                       select a).ToList();
            if (get.Count != 0)
            {
                foreach (var v in get)
                {
                    var del = (from a in dc.PreAdmissionParentSiblings
                               where a.OnlineRegisterId.Value.Equals(regid) && a.StudentAdmissionId.Value.Equals(stuid)
                               select a).FirstOrDefault();
                    dc.PreAdmissionParentSiblings.Remove(del);
                    dc.SaveChanges();
                }
            }

        }
        public List<PreAdmissionParentSibling> GetSibling(int stuid, int regid)
        {
            var res = dc.PreAdmissionParentSiblings.Where(q => q.OnlineRegisterId.Value.Equals(regid) && q.StudentAdmissionId.Value.Equals(stuid)).ToList();
            return res;
        }

        //sibling Details
        public IEnumerable<Object> GetJsonSibling(int stuid)
        {
            var ans = (from a in dc.PreAdmissionParentSiblings
                       where  a.OnlineRegisterId.Value.Equals(stuid)
                       select new
                       {
                           StudentID = a.SiblingStudentId,
                           StudentRegId=a.SblingStudentRegId
                           
                       }).ToList();
            return ans;
        }

        //Offline

        public List<PreAdmissionParentSibling> GetSiblingOffline(int stuid, string id)
        {
            var res = dc.PreAdmissionParentSiblings.Where(q => q.OfflineApplicationID.Equals(id) && q.StudentAdmissionId.Value.Equals(stuid)).ToList();
            return res;
        }
        public void AddSiblingOffline(int stuid, string id, string years, string rolls, string classes, string sections)
        {
            PreAdmissionParentSibling add = new PreAdmissionParentSibling()
            {
                StudentAdmissionId = stuid,
                OfflineApplicationID = id,
                SiblingAcademicYear = years,
                SiblingRollNumbers = rolls,
                SiblingClass = classes,
                SiblingSection = sections
            };
            dc.PreAdmissionParentSiblings.Add(add);
            dc.SaveChanges();
        }

        public void UpdateSiblingOffline(int stuid, string id, string years, string rolls, string classes, string sections)
        {
            var getrow = (from a in dc.PreAdmissionParentSiblings
                          where a.StudentAdmissionId.Value.Equals(stuid) && a.OfflineApplicationID.Equals(id) && a.SiblingRollNumbers == rolls
                          select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.SiblingAcademicYear = years;
                getrow.SiblingRollNumbers = rolls;
                getrow.SiblingClass = classes;
                getrow.SiblingSection = sections;
                dc.SaveChanges();
            }
        }
        public void deleteSiblingOffline(string id, int stuid)
        {
            var get = (from a in dc.PreAdmissionParentSiblings
                       where a.OfflineApplicationID.Equals(id) && a.StudentAdmissionId.Value.Equals(stuid)
                       select a).ToList();
            if (get.Count != 0)
            {
                foreach (var v in get)
                {
                    var del = (from a in dc.PreAdmissionParentSiblings
                               where a.OfflineApplicationID.Equals(id) && a.StudentAdmissionId.Value.Equals(stuid)
                               select a).FirstOrDefault();
                    dc.PreAdmissionParentSiblings.Remove(del);
                    dc.SaveChanges();
                }
            }

        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}