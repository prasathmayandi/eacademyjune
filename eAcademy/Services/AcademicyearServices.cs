﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class AcademicyearServices: IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
      
        public IEnumerable<Object> ShowAcademicYears()
        {
            var AcademicYearList = (from a in dc.AcademicYears
                                    where a.Status==true
                                    select new
                                    {
                                        AcademicYear = a.AcademicYear1,
                                        AcademicYearId = a.AcademicYearId
                                    }).ToList().AsEnumerable();
            return AcademicYearList;
        }
        public AcademicYear date(int acid)
        {
           
            var getRow = (from a in dc.AcademicYears where a.AcademicYearId == acid select a).FirstOrDefault();
            return getRow;
        }
        public int GetmonthDates(int monthid,int acyearid)
        {
            int dd = 0;
            var getrow = dc.AcademicYears.Where(q => q.AcademicYearId==acyearid && q.StartDate.Month <= monthid).FirstOrDefault();
            if(getrow!=null)
            {
                dd = getrow.StartDate.Year;
            }
            var getrow1 = dc.AcademicYears.Where(q => q.AcademicYearId == acyearid && q.EndDate.Month >= monthid).FirstOrDefault();
            if(getrow1!=null)
            {
                dd = getrow1.EndDate.Year;
            }
            return dd;
        }

        public List<AcademicYear> AllList_AcademicYear()
        {
            var ayear = dc.AcademicYears.Select(q => q).ToList();
            return ayear;
        }


        public List<AcademicYear> Add_formAcademicYear()
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var ayear = (from a in dc.AcademicYears
                         where a.EndDate >= date && a.Status == true
                         select a).ToList();
            return ayear;
        }

        public IEnumerable<object> AddFormAcademicYear()
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var ayear = (from a in dc.AcademicYears
                         where a.EndDate >= date && a.Status == true
                         select new 
                         {
                             AcademicYear = a.AcademicYear1,
                             AcademicYearId = a.AcademicYearId
                         }).ToList();
            return ayear;
        }

        public List<AcademicYear> getCurrentAcYear()
        {
            var acYearList = (from a in dc.AcademicYears select a).ToList();
            return acYearList;
        }

        public AcademicYear AcademicYear(int acid)
        {
            var ayear = dc.AcademicYears.Where(query => query.AcademicYearId.Equals(acid)).SingleOrDefault();
            return ayear;
        }

        public Date AcademicYears(int acid)
        {
            var ans = (from t1 in dc.AcademicYears
                       where t1.AcademicYearId == acid
                       select new Date { StartDate = t1.StartDate, EndDate = t1.EndDate,
                                         SDate = SqlFunctions.DateName("day", t1.StartDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.StartDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.StartDate),
                                         EDate = SqlFunctions.DateName("day", t1.EndDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.EndDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.EndDate),
                                         academicstatus=t1.Status
                       }).SingleOrDefault();
            return ans;
        }

        public Date Termdate(int acid,int tid)
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Terms
                       where t1.AcademicYearId == acid && t2.TermId == tid && t2.AcademicYearId == t1.AcademicYearId 
                       select new Date
                       {
                           StartDate = t2.StartDate,
                           EndDate = t2.EndDate,
                           SDate = SqlFunctions.DateName("day", t1.StartDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.StartDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.StartDate),
                           EDate = SqlFunctions.DateName("day", t1.EndDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.EndDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.EndDate)
                       }).SingleOrDefault();
            return ans;
        }

        public AcademicYear AcademicYr(int acid)
        {
            var ayear = dc.AcademicYears.Where(q => q.AcademicYearId.Equals(acid)).First();
            return ayear;
        }
        

       public bool AcademicYearcheck( DateTime sYear)
        {         
            var ans = (from t1 in dc.AcademicYears
                       where (t1.StartDate > sYear)                                          
                       select t1).ToList();
                    
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkAcademicYear(DateTime sdate, DateTime edate)
        {
         
            var ans = (from t1 in dc.AcademicYears
                       where (t1.StartDate >= sdate && t1.StartDate <= edate) ||
                       (t1.EndDate <= edate && t1.EndDate >= sdate)
                       select t1).ToList();
                    
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

       public bool checkAcademicYear(int acid, DateTime sdate, DateTime edate)
        {
            
            var ans = (from t1 in dc.AcademicYears
                       where t1.AcademicYearId!=acid && ( (t1.StartDate >= sdate && t1.StartDate <= edate) ||
                       (t1.EndDate <= edate && t1.EndDate >= sdate) )
                       select t1).ToList();

            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public List<AcademicYear> getAcademicYear()
        {
            var ans = dc.AcademicYears.Select(query => query).ToList();
            return ans;
        }
        internal  List<AcademicYearValidation> getAcademicYear(int AcademicYearId)
        {
            var EndDate = dc.AcademicYears.Where(q => q.AcademicYearId.Equals(AcademicYearId)).Select(q => new { edate = q.EndDate }).FirstOrDefault();

            var ans = (from t1 in dc.AcademicYears
                       where t1.StartDate > EndDate.edate
                       select new AcademicYearValidation { AcYear = t1.AcademicYear1, AcYearId = t1.AcademicYearId }).ToList();
            return ans;
        }
        public List<AcademicYearValidation> getAcademicYear1()
        {
            var ans = (from t1 in dc.AcademicYears
                       select new AcademicYearValidation
                       {
                           AcYearId = t1.AcademicYearId,
                           AcYear = t1.AcademicYear1,
                           AcademicYearStartDate = t1.StartDate,
                           AcademicYearEndState = t1.EndDate,
                           AcademicYearStatus = t1.Status
                           //StartDate = SqlFunctions.DateName("day", t1.StartDate).Trim() + "-" + SqlFunctions.StringConvert((double)t1.StartDate.Month).TrimStart() + "-" + SqlFunctions.DateName("year", t1.StartDate),
                           //EndDate = SqlFunctions.DateName("day", t1.EndDate).Trim() + "-" + SqlFunctions.StringConvert((double)t1.EndDate.Month).TrimStart() + "-" + SqlFunctions.DateName("year", t1.EndDate)
                       }).ToList().Select(a => new AcademicYearValidation {
                           StartDate = a.AcademicYearStartDate.ToString("MMM dd, yyyy"),
                           EndDate = a.AcademicYearEndState.ToString("MMM dd, yyyy"),
                           AcYearId=a.AcYearId,
                           AcademicYear=a.AcYear,
                           AcademicYearStatus = a.AcademicYearStatus
                       }).ToList();
            return ans;
        }

        public void addAcademicYear(string acyear, DateTime sdate, DateTime edate)
        {
            AcademicYear add = new AcademicYear()
            {
                AcademicYear1 = acyear,
                StartDate = sdate,
                EndDate = edate,
                Status=true
            };
            dc.AcademicYears.Add(add);
            dc.SaveChanges();
        }

        public void updateAcademicYear(int acid, string acyear, DateTime sdate, DateTime edate, bool? status)
        {
            var update = dc.AcademicYears.Where(query => query.AcademicYearId.Equals(acid)).First();
            update.AcademicYear1 = acyear;
            update.StartDate = sdate;
            update.EndDate = edate;
            update.Status = status;
            dc.SaveChanges();
        }

        public bool deleteAcademicYear(int acid)
        {
            var ans = (from c in dc.AssignClassToStudents
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {
              
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool deleteAcademicYearGameLevel(int acid)
        {
            var ans = (from c in dc.SportsGameLevels
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {
              
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool deleteAcademicYearAssignGametoStud(int acid)
        {
            var ans = (from c in dc.SportsAssignGametoStudents
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {
              
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool deleteAcademicYearstudActicity(int acid)
        {
            var ans = (from c in dc.AssignActivityToStudents
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }
        public bool deleteAcademicYearActIncharge(int acid)
        {
            var ans = (from c in dc.ActivityIncharges
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }
        public bool deleteAcademicYearFincharge(int acid)
        {
            var ans = (from c in dc.FacilityIncharges
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }


        public bool deleteAcademicYearEAchieve(int acid)
        {
            var ans = (from c in dc.EmployeeAchievements
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool deleteAcademicYearSAchieve(int acid)
        {
            var ans = (from c in dc.SchoolAchievements
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }


        public bool deleteAcademicYearStudAchieve(int acid)
        {
            var ans = (from c in dc.StudentAchievements
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }


        public bool deleteAcademicYearTA(int acid)
        {
            var ans = (from c in dc.TransportAllotments
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool deleteAcademicYearHF(int acid)
        {
            var ans = (from c in dc.HostelFeesCategories
                       where c.AcademicYear == acid
                       select c).ToList();
            if (ans.Count == 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }


        public bool deleteAcademicYearET(int acid)
        {
            var ans = (from c in dc.ExamTimetables
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }
        public bool deleteAcademicYearAECR(int acid)
        {
            var ans = (from c in dc.ExamTimetables
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }


        public bool deleteAcademicYearRollno(int acid)
        {
            var ans = (from c in dc.RollNumberFormats
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool deleteAcademicYearACT(int acid)
        {
            var ans = (from c in dc.AssignClassTeachers
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }
        public bool deleteAcademicYearASS(int acid)
        {
            var ans = (from c in dc.AssignSubjectToSections
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }    
        public void deleteACYear(int acid)
        {
            var delete = dc.AcademicYears.Where(query => query.AcademicYearId.Equals(acid)).First();
            dc.AcademicYears.Remove(delete);
            dc.SaveChanges();

        }
         

        public IEnumerable GetAcademicYear()
        {
            //var ans = dc.AcademicYears.Select(q => q).ToList();
            var ans = (from a in dc.AcademicYears where a.Status == true select a).ToList();
            return ans;
        }
        public IEnumerable GetParticularYear(int id)
        {
            var ans = dc.AcademicYears.Where(q => q.AcademicYearId.Equals(id)).FirstOrDefault().AcademicYear1;
            return ans;
        }
        public IEnumerable GetParticularAcdemicYear(int? id)
        {
            var ans = (from a in dc.AcademicYears
                       where a.AcademicYearId == id
                       select a).FirstOrDefault().AcademicYear1;
            return ans;
        }

        public AcademicYear getCurrentAcademicYearId(DateTime date1)
        {
          //  var dat = date1.AddDays(1);
            var ans = (from a in dc.AcademicYears
                       where a.StartDate <= date1 && a.EndDate >= date1
                       select a).FirstOrDefault();
            return ans;
        }
        public IEnumerable getvacancyAcademicYear()
        {
            var ans = (from a in dc.AcademicYears
                       from b in dc.AsignClassVacancies
                       where b.AcademicYearId==a.AcademicYearId && b.Status ==true
                       select a).Distinct().ToList();
            return ans;
        }

        public AcademicYear getPreviousAcYear(int classId,int studentRegId)
        {
            var PrevYearId = (from a in dc.AssignClassToStudents where a.ClassId < classId && a.StudentRegisterId == studentRegId select a.AcademicYearId).ToList().Max();
            if(PrevYearId != null)
            {
                var PrevYear = (from a in dc.AcademicYears where a.AcademicYearId == PrevYearId select a).FirstOrDefault();
                return PrevYear;
            }
            return null;
        }

        public IEnumerable getvacancyAcademicYearList()
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var ans = (from a in dc.AcademicYears
                       from b in dc.AsignClassVacancies
                       where b.AcademicYearId == a.AcademicYearId && a.Status== true && b.Status == true && a.EndDate >= date
                       select a).Distinct().ToList();
            return ans;
        }
        public IEnumerable CurrentFutureAcYear()
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var ans = (from a in dc.AcademicYears
                       from b in dc.AsignClassVacancies
                       where b.AcademicYearId == a.AcademicYearId && a.Status == true && b.Status == true && a.EndDate >= date
                       select a).Distinct().ToList();
            return ans;
        }
        public bool PastAcademicyear(int? acyear)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var res = (from a in dc.AcademicYears where a.AcademicYearId == acyear && a.EndDate < date select a).FirstOrDefault();
            if (res == null)
            {
                return true;// 

            }
            else
            {
                return false;// complete academicyear
            }
        }

        public int getAcademicyearId(string acyear)
        {
            var record = dc.AcademicYears.Where(q => q.AcademicYear1 == acyear).FirstOrDefault();
            if(record != null)
            {
                return record.AcademicYearId;
            }
            else
            {
                return 1;
            }
        }
       

     internal bool checkAcademicYearAssigned(int acid)
        {
            //var ans = dc.Terms.Where(q => q.AcademicYearId.Value.Equals(acid)).ToList();

            var ans = (from a in dc.Terms
                       from b in dc.AssignClassToStudents
                       where a.AcademicYearId == acid && b.AcademicYearId == acid
                       select a).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal Date AcademicYearsWithTerm(int acid)
        {
            var term = dc.Terms.Where(q => q.AcademicYearId.Value.Equals(acid)).ToList();

            int count = term.Count;
            DateTime d=new DateTime();

            if (count != 0)
            {
                for (int i = 0; i < count; i++)
                {
                    DateTime loc = term[i].EndDate;
                    if (loc > d)
                    {
                        d = loc;
                    }

                }
                var ans = (from t1 in dc.AcademicYears
                           where t1.AcademicYearId == acid
                           select new Date
                           {
                               StartDate = d,
                               EndDate = t1.EndDate,
                               SDate = SqlFunctions.DateName("day", t1.StartDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.StartDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.StartDate),
                               EDate = SqlFunctions.DateName("day", t1.EndDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.EndDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.EndDate)
                           }).SingleOrDefault();
                return ans;
            }
            else
            {

                var ans = (from t1 in dc.AcademicYears
                           where t1.AcademicYearId == acid
                           select new Date
                           {
                               StartDate = t1.StartDate,
                               EndDate = t1.EndDate,
                               SDate = SqlFunctions.DateName("day", t1.StartDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.StartDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.StartDate),
                               EDate = SqlFunctions.DateName("day", t1.EndDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.EndDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.EndDate)
                           }).SingleOrDefault();
                return ans;
            }
        }

        public IEnumerable<object> CurrentDateAcademicYear()
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var ayear = (from a in dc.AcademicYears
                         where a.StartDate <= date && a.EndDate >= date && a.Status == true
                         select new
                         {
                             AcademicYear = a.AcademicYear1,
                             AcademicYearId = a.AcademicYearId
                         }).ToList();
            return ayear;
        }

        public int IsDateWithInCurrentAcYear()
        {
            int yearId = 0;
            DateTime TodayDate = DateTimeByZone.getCurrentDate();
            var year = (from a in dc.AcademicYears
                        where a.StartDate <= TodayDate && a.EndDate >= TodayDate
                       select a).FirstOrDefault();
            if (year != null)
            {
                yearId = year.AcademicYearId;
               
            }
            else
            {
                yearId = 0;
            }
            return yearId;
        }
        
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}