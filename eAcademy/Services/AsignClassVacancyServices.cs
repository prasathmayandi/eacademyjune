﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class AsignClassVacancyServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public IEnumerable ShowVacancyClassList(int yearid)
        {
            var ans = (from a in dc.AsignClassVacancies
                       from b in dc.Classes
                       where a.Vacancy != null && a.ClassId == b.ClassId && a.Status==true && a.AcademicYearId == yearid
                       select new
                       {
                           ClassId = b.ClassId,
                           ClassType = b.ClassType
                       }).ToList();
            return ans;
        }
        public IEnumerable Showpreclass(int classid)
        {
            var ans = (from b in dc.Classes
                       where b.ClassId <= classid
                       orderby b.ClassId
                       select new 
                       {
                           ClassId = b.ClassId,
                           ClassType = b.ClassType
                       }).OrderByDescending(x=> x.ClassId).Take(2).ToList();
            return ans;
        }
        public List<M_ClassVacancy> GetClassVacancy(int yearid)
        {
            var ans1 = (from a in dc.Classes
                        where a.Status == true && a.TimeScheduleId !=null &&  !(from b in dc.AsignClassVacancies
                                                    where b.ClassId == a.ClassId &&
                                                    b.AcademicYearId == yearid
                                                    select b.ClassId)
                                  .Contains(a.ClassId)
                        select new  { Classid = a.ClassId}).ToList();
            if(ans1.Count !=0) 
            {
                foreach (var v in ans1)
                {
                    AsignClassVacancy add = new AsignClassVacancy()
                    {
                        ClassId = v.Classid,
                        Vacancy = 0,
                        AcademicYearId = yearid
                    };
                    dc.AsignClassVacancies.Add(add);
                    dc.SaveChanges();
                }
            }
            var ans = (from a in dc.AsignClassVacancies
                       from b in dc.Classes
                       where a.AcademicYearId == yearid && a.ClassId == b.ClassId 
                       select new M_ClassVacancy
                       {
                           classid = a.ClassId,
                           Vacancy = a.Vacancy,
                           txt_class = b.ClassType,
                           status=a.Status,
                           AgeLimit = SqlFunctions.DateName("day", a.DateofAgeLimit).Trim() + "/" + SqlFunctions.StringConvert((double)a.DateofAgeLimit.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DateofAgeLimit)
                       }).ToList();
            return ans;
        }
   
        public List<M_ClassVacancy> GetonlyActiveClassVacancy(int yearid)
        {
           
            var ans = (from a in dc.AsignClassVacancies
                       from b in dc.Classes
                       where a.AcademicYearId == yearid && a.ClassId == b.ClassId && a.Status ==true
                       select new M_ClassVacancy
                       {
                           classid = a.ClassId,
                           Vacancy = a.Vacancy,
                           txt_class = b.ClassType,
                           status = a.Status
                       }).ToList();
            return ans;
        }
        public void SaveClassVacancy(int year, int cid, int vacancy)
        {
            AsignClassVacancy add = new AsignClassVacancy()
            {
                ClassId = cid,
                Vacancy = vacancy,
                AcademicYearId = year
            };
            dc.AsignClassVacancies.Add(add);
            dc.SaveChanges();
        }
        public Object getParticularClassVacancy(int? year, int? classid)
        {
            var ans = (from a in dc.AsignClassVacancies
                       where a.AcademicYearId == year && a.ClassId == classid && a.Status == true
                       select a).FirstOrDefault().Vacancy;
            return ans;
        }
        public AsignClassVacancy getdate(int? AdminssionClassId, int? AcademicyearId)
        {        
            var ans = (from a in dc.AsignClassVacancies
                       where a.AcademicYearId == AcademicyearId && a.ClassId == AdminssionClassId && a.Status == true
                       select a).FirstOrDefault();
            return ans;
        }
        public void UpdateClassVacancy(int year, int cid, int vacancy,bool sta, DateTime? date)
        {
            var getrow = dc.AsignClassVacancies.Where(q => q.AcademicYearId.Value.Equals(year) && q.ClassId.Value.Equals(cid)).FirstOrDefault();
           
            if (getrow != null)
            {
                getrow.AcademicYearId = year;
                getrow.ClassId = cid;
                getrow.Vacancy = vacancy;
                getrow.Status = sta;
                getrow.DateofAgeLimit = date;
                dc.SaveChanges();
            }
        }


       public  int? checkexistvacuncy(int year,int cid){
           var num = (from a in dc.AsignClassVacancies where a.ClassId == cid && a.AcademicYearId == year select a.Vacancy).FirstOrDefault();
           return num;
       }
        public AsignClassVacancy GetAnyOneYearvacancy()
        {
            var ans = (from a in dc.AsignClassVacancies
                       where a.Status == true
                       select a).FirstOrDefault();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}