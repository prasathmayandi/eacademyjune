﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class CountriesServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public IEnumerable<Object> ShowCountries()
        {
            var CountryList = (from a in db.tblCountries
                               select new
                               {
                                   CountryName = a.CountryName,
                                   CountryCode = a.ISO3166_2LetterCode
                               }).ToList().AsEnumerable();
            return CountryList;
        }
        public List<tblCountry> GetCountry()
        {
            var co = db.tblCountries.Select(q => q).ToList();
            return co;
        }
        public List<tblCountry> GetCountry_Admission()
        {
            var co = db.tblCountries.Select(q => q).ToList();
            return co;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}