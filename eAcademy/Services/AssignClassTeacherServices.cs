﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class AssignClassTeacherServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public AssignClassTeacher getClassTeacher(int yearId, int classId, int secId)
        {
            var getClassIncharge = (from a in db.AssignClassTeachers where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.IsClassTeacher == "Yes" select a).FirstOrDefault();
            return getClassIncharge;
        }

        public AssignClassTeacher getClassTeacherSection(int ClassInchargeId, int passYearId)
        {
            var getRow = (from a in db.AssignClassTeachers where a.EmployeeRegisterId == ClassInchargeId && a.AcademicYearId == passYearId select a).FirstOrDefault();
            return getRow;
        }

        public AssignClassTeacher isClassTeacher(int EmployeeRegId)
        {
            var IsClassTeacher = (from a in db.AssignClassTeachers where a.EmployeeRegisterId == EmployeeRegId select a).FirstOrDefault();
            return IsClassTeacher;
        }

        public GetClassTeacher getClassTeacher(int id)
        {
            var getRow = (from a in db.AssignClassTeachers
                          from b in db.AcademicYears
                          from c in db.Classes
                          from d in db.Sections
                          where a.Id == id &&
                           a.AcademicYearId == b.AcademicYearId &&
                           a.ClassId == c.ClassId &&
                           a.SectionId == d.SectionId
                          select new GetClassTeacher
                          {
                              year = b.AcademicYear1,
                              clas = c.ClassType,
                              sec = d.SectionName,
                              empId = a.EmployeeRegisterId
                          }).FirstOrDefault();

            return getRow;
        }

        public AssignClassTeacher checkRecord(int? AcademicYearId, int? ClassId, int? SectionId)
        {
            var CheckRecordExist = (from a in db.AssignClassTeachers where a.AcademicYearId == AcademicYearId && a.ClassId == ClassId && a.SectionId == SectionId select a).FirstOrDefault();
            return CheckRecordExist;
        }


        public void addClassTeacher(int yearId, int classId, int secId, int FacultyId)
        {
            AssignClassTeacher add = new AssignClassTeacher()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                EmployeeRegisterId = FacultyId
            };
            db.AssignClassTeachers.Add(add);
            db.SaveChanges();
        }

        public List<ClassList> getClassTeacherList(int yearId)
        {
            var ClassTeacherList = (from a in db.AssignClassTeachers
                                    from b in db.Classes
                                    from c in db.Sections
                                    from d in db.TechEmployees
                                    where a.AcademicYearId == yearId &&
                                    a.ClassId == b.ClassId &&
                                    a.SectionId == c.SectionId &&
                                    a.EmployeeRegisterId == d.EmployeeRegisterId
                                    select new ClassList
                                    {
                                        id = a.Id,
                                        ClassName = b.ClassType,
                                        SectionName = c.SectionName,
                                        ClassTeacher = d.EmployeeName
                                    }).ToList();

            return ClassTeacherList;
        }

        public AssignClassTeacher checkRecord(int AssignClassTeacherId)
        {
            var getRecord = (from a in db.AssignClassTeachers where a.Id == AssignClassTeacherId select a).FirstOrDefault();
            return getRecord;
        }

        public void updateClassTeacher(int AssignClassTeacherId, int empId)
        {
            var getRecord = (from a in db.AssignClassTeachers where a.Id == AssignClassTeacherId select a).First();
            getRecord.EmployeeRegisterId = empId;
            db.SaveChanges();
        }

        public void updateFaculty(int yearId,int classId,int secId,int subId,int Edit_AssignFaculty)
        {
            var row = (from a in db.AssignClassTeachers where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.SubjectId == subId select a).FirstOrDefault();
            row.EmployeeRegisterId = Edit_AssignFaculty;
            db.SaveChanges();
        }

        public AssignClassTeacher checkClassAssigned(int FacultyId, int yearId)
        {
            var getRecord = (from a in db.AssignClassTeachers where a.EmployeeRegisterId == FacultyId && a.AcademicYearId == yearId select a).FirstOrDefault();
            return getRecord;
        }

        public AssignClassTeacher checkClassAssigned_ClassTeacher(int FacultyId, int yearId)
        {
            var getRecord = (from a in db.AssignClassTeachers where a.EmployeeRegisterId == FacultyId && a.AcademicYearId == yearId && a.IsClassTeacher == "Yes" select a).FirstOrDefault();
            return getRecord;
        }

        public IEnumerable<object> getClassWithSection(int ClassInchargeId, int passYearId)
        {
            var getList = (from a in db.AssignClassTeachers
                           from b in db.Classes
                           from c in db.Sections
                           where a.ClassId == b.ClassId &&
                           a.SectionId == c.SectionId &&
                           a.IsClassTeacher == "Yes" &&
                           a.EmployeeRegisterId == ClassInchargeId &&
                           a.AcademicYearId == passYearId
                           select new
                           {
                               ClassName = b.ClassType,
                               SecName = c.SectionName,
                               ClassId = a.ClassId,
                               SecId = a.SectionId
                           }).ToList().Select(c => new
                           {
                               ClassNameSecName = c.ClassName + " " + c.SecName,
                               ClassIdSecId = c.ClassId + " " + c.SecId
                           }).ToList();
            return getList;
        }


        public AssignClassTeacher checkRecord(int? AcademicYearId, int? ClassId, int? SectionId, string IsClassTeacher)
        {
            var CheckRecordExist = (from a in db.AssignClassTeachers where a.AcademicYearId == AcademicYearId && a.ClassId == ClassId && a.SectionId == SectionId && a.IsClassTeacher == "Yes" select a).FirstOrDefault();
            return CheckRecordExist;
        }
        public AssignClassTeacher IsClassTeacher(int yearId, int classId, int secId, int empRegId, int subId)
        {
            var getClassIncharge = (from a in db.AssignClassTeachers where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.EmployeeRegisterId == empRegId && a.SubjectId == subId select a).FirstOrDefault();
            return getClassIncharge;
        }
        public void addClassTeacher(int yearId, int classId, int secId, int FacultyId, string IsClassTeacher, int subId)
        {
            AssignClassTeacher add = new AssignClassTeacher()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                EmployeeRegisterId = FacultyId,
                IsClassTeacher = IsClassTeacher,
                SubjectId = subId
            };
            db.AssignClassTeachers.Add(add);
            db.SaveChanges();
        }

        public void UpdateClassTeacher(int yearId, int classId, int secId, int oldEmpId, int newEmpId, string IsClassIncharge, int subId)
        {
            var getRow = (from a in db.AssignClassTeachers where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.EmployeeRegisterId == oldEmpId && a.SubjectId == subId select a).FirstOrDefault();
            getRow.EmployeeRegisterId = newEmpId;
            getRow.IsClassTeacher = IsClassIncharge;
            db.SaveChanges();
        }
        public AssignClassTeacher getClass(int AssignClassTeacherId)
        {
            var getRecord = (from a in db.AssignClassTeachers where a.EmployeeRegisterId == AssignClassTeacherId && a.IsClassTeacher == "Yes" select a).FirstOrDefault();
            return getRecord;
        }

        public AssignClassTeacher CheckClassTeacher(int EmployeeRegId,int yearId,int classId,int secId)
        {
            var row = (from a in db.AssignClassTeachers where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.IsClassTeacher == "Yes" select a).FirstOrDefault();
            return row;
        }

        public IEnumerable<FI_ClassAssigned> getClassAssigned(int yearId, int facultyId)
        {
            var list = (from a in db.AssignClassTeachers
                        from b in db.Classes
                        from c in db.Sections
                        from d in db.Subjects
                        where a.ClassId == b.ClassId &&
                        a.SectionId == c.SectionId &&
                        a.SubjectId == d.SubjectId &&
                        a.AcademicYearId == yearId &&
                        a.EmployeeRegisterId == facultyId &&
                        a.IsClassTeacher == "Yes"
                        select new FI_ClassAssigned
                        {
                            className = b.ClassType,
                            sectionName = c.SectionName,
                            subjectName = d.SubjectName
                        }).ToList();
            return list;
        }

        public List<getclassandsection> SearchClassIncharge(int id)
        {
            var ans = (from b in db.Classes
                       from c in db.Sections
                       where
                       b.Status == true && c.ClassId == b.ClassId
                       select new getclassandsection
                       {
                           ClassId = b.ClassId,
                           ClassName = b.ClassType,
                           SectionName = c.SectionName,
                           SectionId=c.SectionId
                       }).Distinct().OrderBy(q => q.ClassId).ToList().Select(p => new getclassandsection {
                       ClassId=p.ClassId,                     
                       ClassName=p.ClassName,
                       SectionName=p.SectionName,
                       ClassTeacher = GetClassincharge(id,p.ClassId,p.SectionId)
                       }).ToList();

            //var ans = (from a in db.AssignClassTeachers
            //           from b in db.Classes
            //           from c in db.Sections
            //           from d in db.TechEmployees
            //           where a.AcademicYearId == id &&
            //           b.Status.Value.Equals(true) &&
            //           a.ClassId == b.ClassId &&
            //           a.SectionId == c.SectionId &&
            //           a.EmployeeRegisterId == d.EmployeeRegisterId && a.IsClassTeacher == "Yes"

            //           select new getclassandsection
            //           {
            //               ClassId=b.ClassId,
            //               ClassName = b.ClassType,
            //               SectionName = c.SectionName,
            //               ClassTeacher = d.EmployeeName +"("+d.EmployeeId+")"
            //           }).Distinct().OrderBy(q=>q.ClassId).ToList();

            return ans;
        }
        public string GetClassincharge(int yid,int cid,int secid)
        {
            var check = (from a in db.AssignClassTeachers
                         from b in db.TechEmployees
                         where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == secid && a.EmployeeRegisterId == b.EmployeeRegisterId && a.IsClassTeacher == "Yes"
                         select new
                         {
                             ClassTeacher = b.EmployeeName + "(" + b.EmployeeId + ")"
                         }).FirstOrDefault();
        if(check!=null)
        {
            return check.ClassTeacher;
        }
        else
        {
            return "";
        }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}