﻿using eAcademy.DataModel;

namespace eAcademy.Services
{
    public class ParentDetailServices
    {
        EacademyEntities db = new EacademyEntities();

        //public ParentDetail getFather(string ParentUserName)
        //{
        //    var getFatherRecord = (from a in db.ParentDetails where a.FatherUserName == ParentUserName select a).FirstOrDefault();
        //    return getFatherRecord;
        //}
             
        //public ParentProfile getFatherDetails(string ParentUserName)
        //{
        //    var list = (from a in db.ParentDetails
        //                where a.FatherUserName == ParentUserName
        //                select new ParentProfile
        //                {
        //                    Name = a.FatherName,
        //                    Dob = a.FatherDOB,
        //                    Gender = "Male",
        //                    Email = a.FartherEmail,
        //                    Mobile = a.FatherContact,
        //                    Occupation = a.FatherOccupation,
        //                    Qualification = a.FatherQualification,
        //                    WorkNumber = a.FatherWorkNumber,
        //                    YearlyIncome = a.FamilyYeralyIncome,
        //                    Photo = a.FatherPhoto,
        //                    Religion = a.Religion,
        //                    Nationality = a.Nationality,
        //                    Phone = a.Phone,
        //                    Address = a.Address,
        //                    City = a.City,
        //                    State = a.State,
        //                    Country = a.Country,
        //                    UserId = a.FatherUserName
        //                }).ToList().FirstOrDefault();
        //    return list;
        //}

        //public ParentDetail getMother(string ParentUserName)
        //{
        //    var getMotherRecord = (from a in db.ParentDetails where a.MotherUserName == ParentUserName select a).FirstOrDefault();
        //    return getMotherRecord;
        //}

        //public ParentProfile getMotherDetails(string ParentUserName)
        //{
        //    var list = (from a in db.ParentDetails
        //                where a.MotherUserName == ParentUserName
        //                select new ParentProfile
        //                {
        //                    Name = a.MotherName,
        //                    Dob = a.MotherDOB,
        //                    Gender = "Female",
        //                    Email = a.MotherEmail,
        //                    Mobile = a.MotherContact,
        //                    Qualification = a.MotherQualification,
        //                    Occupation = a.MotherOccupation,
        //                    WorkNumber = a.MotherWorkNumber,
        //                    YearlyIncome = a.FamilyYeralyIncome,
        //                    Photo = a.MotherPhoto,
        //                    Religion = a.Religion,
        //                    Nationality = a.Nationality,
        //                    Phone = a.Phone,
        //                    Address = a.Address,
        //                    City = a.City,
        //                    State = a.State,
        //                    Country = a.Country,
        //                    UserId = a.MotherUserName
        //                }).ToList().FirstOrDefault();
        //    return list;
        //}

        //public ParentDetail getGuardian(string ParentUserName)
        //{
        //    var getGuardianRecord = (from a in db.ParentDetails where a.GuardianEmail == ParentUserName select a).FirstOrDefault();
        //    return getGuardianRecord;
        //}
        //public ParentProfile getGuardianDetails(string ParentUserName)
        //{
        //    var list = (from a in db.ParentDetails
        //                where a.GuardianUserName == ParentUserName
        //                select new ParentProfile
        //                {
        //                    Name = a.GuardianName,
        //                    Dob = a.GuardianDOB,
        //                    Gender = a.GuardianGender,
        //                    Email = a.GuardianEmail,
        //                    Mobile = a.GuardianContact,
        //                    Qualification = a.GuardianQualification,
        //                    Occupation = a.GuardianOccupation,
        //                    WorkNumber = a.GuardianWorkNumber,
        //                    YearlyIncome = a.FamilyYeralyIncome,
        //                    Photo = a.GuardianPhoto,
        //                    Religion = a.Religion,
        //                    Nationality = a.Nationality,
        //                    Phone = a.Phone,
        //                    Address = a.Address,
        //                    City = a.City,
        //                    State = a.State,
        //                    Country = a.Country,
        //                    UserId = a.MotherUserName
        //                }).ToList().FirstOrDefault();
        //    return list;
        //}


        //public void updateFatherDetail(string ParentUserName,string Email, long? Mobile, long? WorkNumber, string Qualification, string Occupation, int? YearlyIncome, string Religion, string Nationality, long? Phone, string Address, string City, string State, string Parent_allCountries)
        //{
        //    var s = (from a in db.ParentDetails where a.FatherUserName == ParentUserName select a).First();
        //    if(s != null)
        //    {
        //        s.FartherEmail = Email;
        //        s.FatherContact = Mobile;
        //        s.FatherWorkNumber = WorkNumber;
        //        s.FatherQualification = Qualification;
        //        s.FatherOccupation = Occupation;
        //        s.FamilyYeralyIncome = YearlyIncome;
        //        s.Religion = Religion;
        //        s.Nationality = Nationality;
        //        s.Phone = Phone;
        //        s.Address = Address;
        //        s.City = City;
        //        s.State = State;
        //        s.Country = Parent_allCountries;
        //        db.SaveChanges();
        //    }
        //}

        //public void updateMotherDetail(string ParentUserName, string Email, long? Mobile, long? WorkNumber, string Qualification, string Occupation, int? YearlyIncome, string Religion, string Nationality, long? Phone, string Address, string City, string State, string Parent_allCountries)
        //{
        //    var ss = (from a in db.ParentDetails where a.MotherUserName == ParentUserName select a).First();
        //    if (ss != null)
        //    {
        //        ss.MotherEmail = Email;
        //        ss.MotherContact = Mobile;
        //        ss.MotherWorkNumber = WorkNumber;
        //        ss.MotherQualification = Qualification;
        //        ss.MotherOccupation = Occupation;
        //        ss.FamilyYeralyIncome = YearlyIncome;
        //        ss.Religion = Religion;
        //        ss.Nationality = Nationality;
        //        ss.Phone = Phone;
        //        ss.Address = Address;
        //        ss.City = City;
        //        ss.State = State;
        //        ss.Country = Parent_allCountries;
        //        db.SaveChanges();
        //    }
        //}

        //public void updateGuardianDetail(string ParentUserName, string Email, long? Mobile, long? WorkNumber, string Qualification, string Occupation, int? YearlyIncome, string Religion, string Nationality, long? Phone, string Address, string City, string State, string Parent_allCountries)
        //{
        //    var sss = (from a in db.ParentDetails where a.GuardianUserName == ParentUserName select a).First();
        //    if (sss != null)
        //    {
        //        sss.GuardianEmail = Email;
        //        sss.GuardianContact = Mobile;
        //        sss.GuardianWorkNumber = WorkNumber;
        //        sss.GuardianQualification = Qualification;
        //        sss.GuardianOccupation = Occupation;
        //        sss.FamilyYeralyIncome = YearlyIncome;
        //        sss.Religion = Religion;
        //        sss.Nationality = Nationality;
        //        sss.Phone = Phone;
        //        sss.Address = Address;
        //        sss.City = City;
        //        sss.State = State;
        //        sss.Country = Parent_allCountries;
        //        db.SaveChanges();
        //    }
        //}

        //public ParentDetail CheckFatherEMailExist(string FatherEmail)
        //{
        //    var CheckFatherEMailExist = (from bc in db.ParentDetails where bc.FartherEmail == FatherEmail select bc).FirstOrDefault();
        //    return CheckFatherEMailExist;
        //}

        //public ParentDetail CheckMotherEmailExist(string MotherEmail)
        //{
        //    var CheckMotherEmailExist = (from bc in db.ParentDetails where bc.MotherEmail == MotherEmail select bc).FirstOrDefault();
        //    return CheckMotherEmailExist;
        //}

        //public ParentDetail checkGuardianEmailExist(string GuardianEmail)
        //{
        //    var checkGuardianEmailExist = (from bc in db.ParentDetails where bc.GuardianEmail == GuardianEmail select bc).FirstOrDefault();
        //    return checkGuardianEmailExist;
        //}

        //public void addParent(string FartherEmail1,int StudentRegisterId,string FatherName,string FatherSalt,DateTime FatherDOB1,long FatherContact1,long FatherWorkNumber1,string FatherOccupation,
        //               string FatherQualification,string FatherPhoto1,string MotherEmail1,string MotherName,string MotherSalt,DateTime MotherDOB1,long MotherContact1,long MotherWorkNumber1,
        //               string MotherOccupation,string MotherQualification,string MotherPhoto1,string Religion1,string Nationality1,long  Phone1,string Address1,string City1,string State1,string Country1,
        //               int FamilyYeralyIncome,DateTime DOR)
        //{
        //    ParentDetail add = new ParentDetail()
        //    {
        //        StudentRegisterId = StudentRegisterId,

        //        FartherEmail = FartherEmail1,
        //        FatherName = FatherName,
        //        FatherSalt = FatherSalt,
        //        FatherDOB = FatherDOB1,
        //        FatherContact = FatherContact1,
        //        FatherWorkNumber = FatherWorkNumber1,
        //        FatherOccupation = FatherOccupation,
        //        FatherQualification = FatherQualification,
        //        FatherPhoto = FatherPhoto1,

        //        MotherEmail = MotherEmail1,
        //        MotherName = MotherName,
        //        MotherSalt = MotherSalt,
        //        MotherDOB = MotherDOB1,
        //        MotherContact = MotherContact1,
        //        MotherWorkNumber = MotherWorkNumber1,
        //        MotherOccupation = MotherOccupation,
        //        MotherQualification = MotherQualification,
        //        MotherPhoto = MotherPhoto1,

        //        Religion = Religion1,
        //        Nationality = Nationality1,
        //        Phone = Phone1,
        //        Address = Address1,
        //        City = City1,
        //        State = State1,
        //        Country = Country1,
        //        FamilyYeralyIncome = FamilyYeralyIncome,
        //        DOR = DOR
        //    };
        //    db.ParentDetails.Add(add);
        //    db.SaveChanges();
        //}

        //public void CopyFromSParentRegTolParent(ParentRegister row)
        //{
        //    ParentDetail add = new ParentDetail()
        //    {
        //        ParentAdmissionId = row.ParentAdmissionId,
        //        StudentAdmissionId = row.StudentAdmissionId,
        //        OnlineRegisterId = row.OnlineRegisterId,

        //        FatherFirstName = row.FatherFirstName,
        //        FatherLastName = row.FatherLastName,
        //        FatherDOB = row.FatherDOB,
        //        FatherQualification = row.FatherQualification,
        //        FatherOccupation = row.FatherOccupation,
        //        FatherContact = row.FatherMobileNo,
        //        FartherEmail = row.FatherEmail,

        //        MotherFirstName = row.MotherFirstname,
        //        MotherLastName = row.MotherLastName,
        //        MotherDOB = row.MotherDOB,
        //        MotherQualification = row.MotherQualification,
        //        MotherOccupation = row.MotherOccupation,
        //        MotherContact = row.MotherMobileNo,
        //        MotherEmail = row.MotherEmail,
        //        //    FamilyYeralyIncome = row.TotalIncome,
        //        CompanyName = row.UserCompanyname,
        //        CompanyAddress1 = row.CompanyAddress1,
        //        CompanyAddress2 = row.CompanyAddress2,
        //        Companycity = row.CompanyCity,
        //        CompanyState = row.CompanyState,
        //        CompanyCountry = row.CompanyCountry,
        //        CompanyPostalCode = row.CompanyPostelcode,
        //        CompanyContact = row.CompanyContact,

        //        GuardianRequired = row.GuardianRequried,
        //        GuardianRelation = row.GuRelationshipToChild,
        //        GuardianFirstName = row.GuardianFirstname,
        //        GuardianLastName = row.GuardianLastName,
        //        GuardianDOB = row.GuardianDOB,
        //        GuardianGender = row.GuardianGender,
        //        GuardianQualification = row.GuardianQualification,
        //        GuardianOccupation = row.GuardianOccupation,
        //        GuardianContact = row.GuardianMobileNo,
        //        GuardianEmail = row.GuardianEmail,
        //        GuardianIncome = row.GuardianIncome,
        //        GuardianCompanyName = row.GuardianCompanyName,
        //        EmergencyContactPersonName = row.EmergencyContactPersonName,
        //        EmergencyContactNumber = row.EmergencyContactNumber,
        //        ContactPersonRelationship = row.ContactPersonRelationship,

        //        //   PrimaryUserName = row.PrimaryUserName,
        //        PrimaryUser = row.PrimaryUser,
        //        PrimaryUserEmail = row.PrimaryUserEmail,
        //        PrimaryUserContact = row.PrimaryUserContact,
        //        ParentWorkSameSchool = row.WorkSameSchool,
        //        SendEmailStatus = row.SendEmailStatus

        //    };
        //    db.ParentDetails.Add(add);
        //    db.SaveChanges();
        //}

        //public ParentDetail getParentRow(int stuId)
        //{
        //    var getRow = (from ac in db.ParentDetails where ac.StudentRegisterId == stuId select ac).First();
        //    return getRow;
        //}

        //public void UpdateFatherUserName(int stuId,string FatherUserName,string FatherPassword,string MotherUserName,string MotherPassword)
        //{
        //    var getRow = (from ac in db.ParentDetails where ac.StudentRegisterId == stuId select ac).First();
        //    getRow.FatherUserName = FatherUserName;
        //    getRow.FatherPassword = FatherPassword;
        //    getRow.MotherUserName = MotherUserName;
        //    getRow.MotherPassword = MotherPassword;
        //    db.SaveChanges();
        //}



        //public void addGuardian(int StudentRegisterId,string GuardianName,string GuardianSalt,string GuardianRelation,string GuardianGender,DateTime GuardianDOB1,string GuardianEmail,long GuardianContact,
        //      long GuardianWorkNumber1,string GuardianOccupation,string GuardianQualification,string GuardianPhoto1,string Religion3,string Nationality3,long phone3,string Address3,string City3,
        //    string State3,string Country3,int FamilyYeralyIncome3,DateTime DOR)
        //{
        //    ParentDetail add = new ParentDetail()
        //    {
        //        StudentRegisterId = StudentRegisterId,
        //        GuardianName = GuardianName,
        //        GuardianSalt = GuardianSalt,
        //        GuardianRelation = GuardianRelation,
        //        GuardianGender = GuardianGender,
        //        GuardianDOB = GuardianDOB1,
        //        GuardianEmail = GuardianEmail,
        //        GuardianContact = GuardianContact,
        //        GuardianWorkNumber = GuardianWorkNumber1,
        //        GuardianOccupation = GuardianOccupation,
        //        GuardianQualification = GuardianQualification,
        //        GuardianPhoto = GuardianPhoto1,
        //        Religion = Religion3,
        //        Nationality = Nationality3,
        //        Phone = phone3,
        //        Address = Address3,
        //        City = City3,
        //        State = State3,
        //        Country = Country3,
        //        FamilyYeralyIncome = FamilyYeralyIncome3,
        //        DOR = DOR
        //    };
        //    db.ParentDetails.Add(add);
        //    db.SaveChanges();
        //}

        //public void UpdateGuardianUserName(int stuId, string GuardianUserName, string GuardianPassword)
        //{
        //    var getRow = (from ac in db.ParentDetails where ac.StudentRegisterId == stuId select ac).First();
        //    getRow.GuardianUserName = GuardianUserName;
        //    getRow.GuardianPassword = GuardianPassword;
        //    db.SaveChanges();
        //}

        //public void SiblingParentAdd(ParentDetail row)
        //{
        //    db.ParentDetails.Add(row);
        //    db.SaveChanges();
        //}

        //public void UpdateStudentId(int SiblingRegId, int StudentRegId)
        //{
        //    var UpdateRow = (from ac in db.ParentDetails where ac.StudentRegisterId == SiblingRegId select ac).First();
        //    UpdateRow.StudentRegisterId = StudentRegId;
        //    db.SaveChanges();
        //}

        //public void updateParentPassword(int onlineRegId, int admissionId, string userName, string Salt, string Password)
        //{
        //    var getRow = (from a in db.ParentDetails where a.OnlineRegisterId == onlineRegId && a.StudentAdmissionId == admissionId select a).FirstOrDefault();
        //    getRow.PrimaryUserName = userName;
        //    getRow.PrimaryUserSalt = Salt;
        //    getRow.PrimaryUserPassword = Password;
        //    db.SaveChanges();
        //}
        //public ParentDetail getRecordToUpadatePassword(int onlineRegId, int admissionId)
        //{
        //    var getRow = (from a in db.ParentDetails where a.OnlineRegisterId == onlineRegId && a.StudentAdmissionId == admissionId select a).FirstOrDefault();
        //    return getRow;
        //}


        ////jegadeesh

        //public List<ParentDetail> SearchParentProfile(int id)
        //{
        //    var q1 = db.ParentDetails.Where(q => q.StudentRegisterId.Value.Equals(id)).ToList();
        //    //var q1 = (from a in dc.ParentDetails where a.StudentRegisterId == id select a).ToList();
        //    return q1;

        //}

        //public List<StudentDetails> getPsrentdetails(int id)
        //{
            
         
        //    var res = (from a in db.ParentDetails
        //               where a.StudentRegisterId == id
        //               select new StudentDetails
        //               {
        //                   StudentRegId=a.StudentRegisterId,
        //                   FatherName=a.FatherFirstName +" "+a.FatherLastName,
        //                   FatherDob = SqlFunctions.DateName("day", a.FatherDOB).Trim() + "-" + SqlFunctions.StringConvert((double)a.FatherDOB.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.FatherDOB),
        //                   FatherContact=a.FatherContact,
        //                   FatherOccupation=a.FatherOccupation,
        //                   FatherQualification=a.FatherQualification,
        //                   FatherEmail=a.FartherEmail,
        //                   MotherName=a.MotherFirstName+" "+a.MotherLastName,
        //                   MotherDob = SqlFunctions.DateName("day", a.MotherDOB).Trim() + "-" + SqlFunctions.StringConvert((double)a.MotherDOB.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.MotherDOB),
        //                   MotherContact=a.MotherContact,
        //                   MotherOccupation=a.MotherOccupation,
        //                   MotherQualification=a.MotherQualification,
        //                   MotherEmail=a.MotherEmail,
        //                   GuardianName=a.GuardianFirstName+" "+a.GuardianLastName,
        //                   GuardiaContact=a.GuardianContact,
        //                   GuardianDob = SqlFunctions.DateName("day", a.GuardianDOB).Trim() + "-" + SqlFunctions.StringConvert((double)a.GuardianDOB.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.GuardianDOB),
        //                   GuardianOccupation=a.GuardianOccupation,
        //                   GuardianQualification=a.GuardianQualification,
        //                   GuardianEmail=a.GuardianEmail,

                          
                           
        //               }).ToList();
        //    return res;

        //}
    }
}