﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eAcademy.Services
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class Student_CheckSessionAttribute : ActionFilterAttribute
    {

        Data db = new Data();
        EacademyEntities ee = new EacademyEntities();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower();
            if (!controllerName.Contains("home"))
            {
                HttpSessionStateBase session = filterContext.HttpContext.Session;
                var user = session["StudentRegId_SessionAttribute"];
                if (((user == null) && (!session.IsNewSession)) || (session.IsNewSession) || (null == filterContext.HttpContext.Session))
                {
                    //send them off to the login page
                    var url = new UrlHelper(filterContext.RequestContext);
                    var loginUrl = url.Content("~/Home/ParentLogout");
                    filterContext.Result = new RedirectResult(loginUrl);
                    return;
                }             
            }
        }
    }
}