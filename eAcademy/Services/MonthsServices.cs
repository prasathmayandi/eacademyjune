﻿using System;
using System.Linq;
using eAcademy.DataModel;
using System.Collections;
using eAcademy.HelperClass;
using eAcademy.Models;
using System.Collections.Generic;
namespace eAcademy.Services
{
    public class MonthsServices:IDisposable
    {
        EacademyEntities ee = new EacademyEntities();
        public IEnumerable GetMonths()
        {
            var ans = ee.Months.Select(q => q);

            return ans;
        }
        public IEnumerable GetParticularMonth(int monthid)
        {
            var ans = ee.Months.Where(q => q.MonthsId.Equals(monthid)).FirstOrDefault().MonthsName;
            return ans;
        }
        public IEnumerable GetPreviousMonth()
        {
            var date = DateTimeByZone.getCurrentDateTime();
            int mon = date.Month;
            mon--;
            if (mon > 0)
            {
                var ans = ee.Months.Where(q=>q.MonthsId == mon).Select(q => q);
                return ans;
            }
            else
            {
                var ans = ee.Months.Where(q => q.MonthsId == 12).Select(q => q);
                return ans;
            }
        }
        public int GetPreviousMonthYear()
        {
            var date = DateTimeByZone.getCurrentDateTime();
            int mon = date.Month;
            mon--;
            if (mon > 0)
            {
                return date.Year;
            }
            else
            {
                int year = date.Year;
                year--;
                return year;
            }
        }

        public List<M_AcademicYearMonth> getAcademicyesrMonth(int yearid)
        {
            var list = ee.AcademicYears.Where(q => q.AcademicYearId == yearid).FirstOrDefault();            
                var startmonth = list.StartDate.Month;
                var endmonth = list.EndDate.Month;

                var record = ((from a in ee.Months
                               where a.MonthsId >= startmonth
                               select new M_AcademicYearMonth
                               {
                                   MonthId = a.MonthsId,
                                   MonthName = a.MonthsName,
                                   order = "begin"
                               }).Distinct().ToList().Union(from a in ee.Months
                                                            where a.MonthsId <= endmonth
                                                            select new M_AcademicYearMonth
                                                            {
                                                                MonthId = a.MonthsId,
                                                                MonthName = a.MonthsName,
                                                                order = "end"
                                                            }).Distinct().ToList()).ToList();
                return record;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ee.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}