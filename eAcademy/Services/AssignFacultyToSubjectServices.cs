﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class AssignFacultyToSubjectServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public List<StudentSubjectList>SubjectList(int yearId, int classId, int secId)
        {
            var list = (from a in db.AssignFacultyToSubjects
                        from b in db.Subjects
                        from c in db.TechEmployees
                        from d in db.AssignClassTeachers
                        where a.AcademicYearId == yearId &&
                            a.ClassId == classId &&
                            a.SectionId == secId &&
                            a.SubjectId == b.SubjectId &&
                            a.EmployeeRegisterId == c.EmployeeRegisterId &&
                            a.AcademicYearId == d.AcademicYearId &&
                            a.ClassId == d.ClassId &&
                            a.SectionId == d.SectionId &&
                            a.EmployeeRegisterId == d.EmployeeRegisterId &&
                            a.SubjectId == d.SubjectId
                        select new
                        {
                            FacultyId = c.EmployeeRegisterId,
                            Subject = b.SubjectName,
                            Faculty = c.EmployeeName,
                            id= a.Id,
                            classTeacher = d.IsClassTeacher
                        }).ToList().Select ( a => new StudentSubjectList
                        {
                            FacultyId = a.FacultyId,
                            Subject = a.Subject,
                            Faculty = a.Faculty,
                            classTeacher = a.classTeacher,
                            RowId = QSCrypt.Encrypt(a.id.ToString())
                        }).ToList();
            return list;
        }

        public getSubject getSubName(int rowId)
        {
            var sub = (from a in db.AssignFacultyToSubjects
                       from b in db.Subjects
                       from c in db.TechEmployees
                       where a.EmployeeRegisterId == c.EmployeeRegisterId &&
                       a.SubjectId == b.SubjectId &&
                       a.Id == rowId
                       select new getSubject
                       {
                           Subject = b.SubjectName,
                           Faculty = c.EmployeeName
                       }).FirstOrDefault();
            return sub;
        }

        public AssignFacultyToSubject getFacultyId(int rowid)
        {
            var row = (from a in db.AssignFacultyToSubjects where a.Id == rowid select a).FirstOrDefault();
            return row;
        }

        public List<SubjectTeacherList> TeacherSubjectList(int yearId, int classId, int secId)
        {
            var subList = (from a in db.AssignFacultyToSubjects
                           from f in db.TechEmployees
                           from g in db.Subjects
                           where a.AcademicYearId == yearId &&
                                 a.ClassId == classId &&
                                 a.SectionId == secId &&
                                 a.EmployeeRegisterId == f.EmployeeRegisterId &&
                                 a.SubjectId == g.SubjectId
                           select new SubjectTeacherList
                           {
                               Id = a.Id,
                               SubjectName = g.SubjectName,
                               FacultyName = f.EmployeeName
                           }).ToList();
            return subList;
        }

        public AssignFacultyToSubject getFaculySubject(int yearId, int classId, int secId, int FacultyId)
        {
            var getSubRow = (from a in db.AssignFacultyToSubjects where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.EmployeeRegisterId == FacultyId select a).FirstOrDefault();
            return getSubRow;
        }

        public AssignFacultyToSubject getSubjectFaculty(int yearId, int classId, int secId, int subId)
        {
            var getSubRow = (from a in db.AssignFacultyToSubjects where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.SubjectId == subId select a).FirstOrDefault();
            return getSubRow;
        }

        public IEnumerable<Object> ShowSections(int ClassInchargeId)
        {
            var SecList = (from a in db.Sections
                           from b in db.AssignFacultyToSubjects
                           where a.SectionId == b.SectionId &&
                           b.EmployeeRegisterId == ClassInchargeId
                           select new
                           {
                               SecName = a.SectionName,
                               SecId = a.SectionId
                           }).ToList().AsEnumerable();          
            return SecList;
        }

        public AssignFacultyToSubject checkFacultyAssigned(int id)
        {
            var getRow = (from a in db.AssignFacultyToSubjects where a.Id == id select a).FirstOrDefault();
            return getRow;
        }

        public GetAssignedFaculty getAssignedFacultyDetail(int id)
        {
            var getValues = (from a in db.AssignFacultyToSubjects
                             from b in db.AcademicYears
                             from c in db.Classes
                             from d in db.Sections
                             from e in db.Subjects
                             where a.Id == id &&
                             a.AcademicYearId == b.AcademicYearId &&
                             a.ClassId == c.ClassId &&
                             a.SectionId == d.SectionId &&
                             a.SubjectId == e.SubjectId
                             select new GetAssignedFaculty
                             {
                                 year = b.AcademicYear1,
                                 classs = c.ClassType,
                                 sec = d.SectionName,
                                 sub = e.SubjectName
                             }).FirstOrDefault();

            return getValues;
        }

        public void addFacultyToSubject(int AcademicYearId,int ClassId,int SectionId,int SubjectId,int EmployeeRegisterId)
        {
            AssignFacultyToSubject add = new AssignFacultyToSubject()
            {
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                SubjectId = SubjectId,
                EmployeeRegisterId = EmployeeRegisterId
            };
            db.AssignFacultyToSubjects.Add(add);
            db.SaveChanges();
        }

        public List<ModulesAssignedList> getModulesAssigned(int yearId, int FacultyId)
        {
            var moduleList = (from a in db.AssignFacultyToSubjects
                              from b in db.Classes
                              from c in db.Sections
                              from d in db.Subjects
                              where a.ClassId == b.ClassId &&
                                    a.SectionId == c.SectionId &&
                                    a.SubjectId == d.SubjectId &&
                                    a.AcademicYearId == yearId &&
                                    a.EmployeeRegisterId == FacultyId
                              select new ModulesAssignedList
                              {
                                  ClassName = b.ClassType,
                                  SectionName = c.SectionName,
                                  SubjectName = d.SubjectName
                              }).ToList();
            return moduleList;
        }

        public List<StaffTimetableList> getTimetable(int yearId, int FacultyId, int dayId)
        {
            var list = (from a in db.AssignFacultyToSubjects
                        from b in db.AssignSubjectToPeriods
                        from c in db.Classes
                        from d in db.Sections
                        from e in db.Subjects
                        where a.SubjectId == b.SubjectId &&
                              a.EmployeeRegisterId == FacultyId &&
                              b.Day == dayId &&
                              a.AcademicYearId == yearId &&
                              a.ClassId == c.ClassId &&
                              a.SectionId == d.SectionId &&
                              a.SubjectId == e.SubjectId
                        select new StaffTimetableList
                        {
                            Period = b.Period,
                            Class = c.ClassType,
                            Section = d.SectionName,
                            Subject = e.SubjectName
                        }).ToList();

            return list;
        }

        public IEnumerable<Object> ShowSubjects(int passSecId, int FacultyId)
        {
            var SubjectList = (from a in db.Subjects
                               from b in db.AssignFacultyToSubjects
                               where a.SubjectId == b.SubjectId &&
                               b.SectionId == passSecId &&
                               b.EmployeeRegisterId == FacultyId
                               select new
                               {
                                   SubName = a.SubjectName,
                                   SubId = a.SubjectId
                               }).ToList().AsEnumerable();



            return SubjectList;
        }

        public IEnumerable<Object> ShowSection(int passClassId, int FacultyId)
        {
            var SecList = (from a in db.Sections
                           from b in db.AssignFacultyToSubjects
                           where a.SectionId == b.SectionId &&
                           b.ClassId == passClassId &&
                           b.EmployeeRegisterId == FacultyId
                           select new
                           {
                               SecName = a.SectionName,
                               SecId = a.SectionId
                           }).ToList().AsEnumerable();


            return SecList;
        }

        public void updateFaculty(int id, int facultyId)
        {
            var getRow = (from a in db.AssignFacultyToSubjects where a.Id == id select a).First();
            getRow.EmployeeRegisterId = facultyId;
            db.SaveChanges();
        }


        public void updateFaculty1(int yearId, int classId, int secId, int subId, int facultyId)
        {
            var getRow = (from a in db.AssignFacultyToSubjects where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.SubjectId == subId select a).FirstOrDefault();
            getRow.EmployeeRegisterId = facultyId;
            db.SaveChanges();
        }

        public List<ListSubjectWithFaculty> getSubjectFacultyList(int? yearId, int? classId, int? secId)
        {
            var list = (from a in db.AssignSubjectToSections
                        from b in db.AssignFacultyToSubjects
                        from c in db.AssignClassTeachers
                        from d in db.Subjects
                        from e in db.TechEmployees
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        b.AcademicYearId == yearId &&
                        b.ClassId == classId &&
                        b.SectionId == secId &&
                        c.AcademicYearId == yearId &&
                        c.ClassId == classId &&
                        c.SectionId == secId &&
                        a.AcademicYearId == b.AcademicYearId &&
                        a.ClassId == b.ClassId &&
                        a.SectionId == b.SectionId &&
                        a.SubjectId == b.SubjectId &&
                        a.AcademicYearId == c.AcademicYearId &&
                        a.ClassId == c.ClassId &&
                        a.SectionId == c.SectionId &&
                        a.SubjectId == c.SubjectId &&
                        a.SubjectId == d.SubjectId &&
                        b.EmployeeRegisterId == e.EmployeeRegisterId &&
                        b.EmployeeRegisterId == c.EmployeeRegisterId
                        select new ListSubjectWithFaculty
                        {
                            Subject = d.SubjectName,
                            Faculty = e.EmployeeName,
                            ClassIncharge = c.IsClassTeacher,
                            AsubId = a.AssignSubjectToSectionId,
                            AFacultyId = b.Id,
                            ACI_Id = c.Id,
                            SubjectStatus = a.Status
                        }).OrderBy(x=>x.ACI_Id).ToList();
            return list;
        }


        public List<StaffTimetableList> getTimetable1(int yearId, int FacultyId, int dayId)
        {
            var list = (from a in db.AssignFacultyToSubjects
                        from b in db.AssignSubjectToPeriods
                        from c in db.Classes
                        from d in db.Sections
                        from e in db.Subjects
                        where b.AcademicYearId == yearId &&
                        b.Day == dayId &&
                         a.EmployeeRegisterId == FacultyId &&
                        a.AcademicYearId == b.AcademicYearId &&
                        a.ClassId == b.ClassId &&
                        a.SectionId == b.SectionId &&
                        a.SubjectId == b.SubjectId &&
                              a.ClassId == c.ClassId &&
                              a.SectionId == d.SectionId &&
                              a.SubjectId == e.SubjectId
                        select new
                        {
                            Period = b.Period,
                            Class = c.ClassType,
                            Section = d.SectionName,
                            Subject = e.SubjectName,
                            ClassId = b.ClassId,
                            SectionId = b.SectionId,
                            SubjectId = b.SubjectId
                        }).ToList().Select(a => new StaffTimetableList
                        {
                            Period = a.Period,
                            Class = a.Class,
                            Section = a.Section,
                            Subject = a.Subject,
                            ClassId1 = QSCrypt.Encrypt(a.ClassId.ToString()),
                            SectionId1 = QSCrypt.Encrypt(a.SectionId.ToString()),
                            SubjectId1 = QSCrypt.Encrypt(a.SubjectId.ToString()),
                            Period1 = QSCrypt.Encrypt(a.Period.ToString())
                        }).ToList();

            return list;
        }

        public List<GetAssignedFaculty> getEmployeesSub(int empId, int yearId)
        {
            var getSubRow = (from a in db.AssignFacultyToSubjects
                             where a.EmployeeRegisterId == empId &&
                             a.AcademicYearId == yearId
                             select new GetAssignedFaculty
                             {
                                 subId = a.SubjectId,
                                 classId = a.ClassId,
                                 secId = a.SectionId
                             }).ToList();
            return getSubRow;
        }


        public IEnumerable<object> getClassWithSection(int facultyId, int passYearId)
        {
            var getList = (from a in db.AssignFacultyToSubjects
                           from b in db.Classes
                           from c in db.Sections
                           where a.ClassId == b.ClassId &&
                           a.SectionId == c.SectionId &&
                           b.Status == true &&
                           a.EmployeeRegisterId == facultyId &&
                           a.AcademicYearId == passYearId
                           select new
                           {
                               ClassName = b.ClassType,
                               SecName = c.SectionName,
                               ClassId = a.ClassId,
                               SecId = a.SectionId
                           }).ToList().Select(c => new
                           {
                               ClassNameSecName = c.ClassName + " " + c.SecName,
                               ClassIdSecId = c.ClassId + " " + c.SecId
                           }).Distinct().ToList();
            return getList;
        }

        public IEnumerable<object> ShowSubjects(int yearId, int classId, int secId, int facultyId)
        {
            var getList = (from a in db.AssignFacultyToSubjects
                           from b in db.AcademicYears
                           from c in db.Classes
                           from d in db.Sections
                           from e in db.Subjects
                           where a.AcademicYearId == b.AcademicYearId &&
                           a.ClassId == c.ClassId &&
                           a.SectionId == d.SectionId &&
                           a.SubjectId == e.SubjectId &&
                           a.EmployeeRegisterId == facultyId &&
                           a.AcademicYearId == yearId &&
                           a.ClassId == classId &&
                           a.SectionId == secId &&
                           a.EmployeeRegisterId == facultyId
                           select new
                           {
                               subjectName = e.SubjectName,
                               subjectId = a.SubjectId
                           }).ToList();
            return getList;
        }

        public MI_Queries GetSubFacultyName(int yearId, int classId, int secId, int subId)
        {
            var FacultyName = (from a in db.AssignFacultyToSubjects
                               from b in db.TechEmployees
                               where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                               a.AcademicYearId == yearId &&
                               a.ClassId == classId &&
                               a.SectionId == secId &&
                               a.SubjectId == subId
                               select new MI_Queries
                               {
                                   FacultyName = b.EmployeeName,
                                   LastName = b.LastName,
                                   FacultyId = a.EmployeeRegisterId.Value
                               }).FirstOrDefault();
            return FacultyName;
        }

        public List<getclassandsection> SearchSubjectTeacher(int yid, int cid, int sid)
        {
            var ans = (from a in db.AssignFacultyToSubjects
                       from b in db.Classes
                       from c in db.Sections
                       from d in db.Subjects
                       from e in db.TechEmployees
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && b.Status.Value.Equals(true) && b.ClassId == cid && c.ClassId == cid && c.SectionId == sid && a.SubjectId == d.SubjectId && a.EmployeeRegisterId == e.EmployeeRegisterId
                       select new getclassandsection
                       {
                           ClassName = b.ClassType,
                           SectionName = c.SectionName,
                           subjectName = d.SubjectName,
                           ClassTeacher = e.EmployeeName,
                           EmpRegId = e.EmployeeRegisterId
                       }).Distinct().ToList().Select(q => new getclassandsection { 
                       
                       ClassName=q.ClassName,
                       SectionName=q.SectionName,
                       subjectName=q.subjectName,
                       ClassTeacher = GetEmployeeincharge(q.ClassTeacher,q.EmpRegId,yid,cid,sid)         
                       }).ToList();
            return ans;
        }
        public string GetEmployeeincharge(string Empname,int empid,int yid,int cid,int sid)
        {
            var check = db.AssignClassTeachers.Where(q => q.AcademicYearId == yid && q.ClassId == cid && q.SectionId == sid && q.EmployeeRegisterId == empid && q.IsClassTeacher == "Yes").FirstOrDefault();
            if(check!=null)
            {
                return Empname + "(Class Incharge)";
            }
            else
            {
                return Empname;
            }
        }
        public List<TimeTable> GetTimeTable(int yid, int eid)
        {
            var ans = (from a in db.AssignSubjectToPeriods
                       from b in db.AssignFacultyToSubjects
                       from c in db.Subjects
                       from d in db.Classes
                       from e in db.Sections
                       where
                       a.AcademicYearId == b.AcademicYearId &&
                       b.AcademicYearId == yid &&
                       a.AcademicYearId==yid &&
                       b.EmployeeRegisterId == eid &&
                       

                       a.SubjectId == b.SubjectId&&
                       a.ClassId == b.ClassId && 
                       a.SectionId == b.SectionId && 
                      

                       c.SubjectId == b.SubjectId && 
                       d.ClassId == b.ClassId && 
                       e.SectionId==b.SectionId &&

                       c.SubjectId == a.SubjectId &&
                       d.ClassId == a.ClassId &&
                       e.SectionId == a.SectionId
                       select new TimeTable
                       {
                           day = a.Day,
                           period = a.Period,
                           //subjectID=a.SubjectId
                           subject = c.SubjectName +"("+d.ClassType+ "-"+e.SectionName+")"
                       }).Distinct().ToList();
            return ans;
        }

        public int GetgreaterClass(int yid, int eid)
        {
            var res = (from a in db.AssignFacultyToSubjects

                       where a.AcademicYearId == yid && a.EmployeeRegisterId == eid
                       select new TimeTable
                       {
                           classid = a.ClassId
                       }).Distinct().Max(q => q.classid).Value;
            return res;

        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}