﻿using eAcademy.DataModel;
using System;
using System.Linq;

namespace eAcademy.Services
{
    public class SchoolSettingServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public SchoolSetting getSchoolSettings()
        {
            var getSchoolSettings = (from a in db.SchoolSettings select a).FirstOrDefault();
            return getSchoolSettings;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}