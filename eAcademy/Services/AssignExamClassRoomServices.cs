﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class AssignExamClassRoomServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public GetExamAttendance getExamAttendance(int id)
        {
            var getRecord = (from a in db.AssignExamClassRooms
                             from b in db.AcademicYears
                             from c in db.Classes
                             from d in db.Sections
                             from e in db.Subjects
                             from f in db.Exams
                             where a.AssignExamClassRoomId == id &&
                             a.AcademicYearId == b.AcademicYearId &&
                             a.ClassId == c.ClassId &&
                             a.SectionId == d.SectionId &&
                             a.SubjectId == e.SubjectId &&
                             a.ExamId == f.ExamId
                             select new GetExamAttendance
                             {
                                 year = b.AcademicYear1,
                                 classs = c.ClassType,
                                 section = d.SectionName,
                                 subject = e.SubjectName,
                                 exam = f.ExamName,
                                 date = a.ExamDate,
                                 sRegId = a.StartRegisterId,
                                 eRegId = a.EndRegisterId,
                                 room = a.RoomNumber
                             }).FirstOrDefault();
            return getRecord;
        }

        public AssignExamClassRoom IsExamClassRoomAssigned(int CRid,int AcYearId)
        {
            DateTime todayDate = DateTimeByZone.getCurrentDate();
            var row = (from a in db.AssignExamClassRooms where a.AcademicYearId == AcYearId && a.ClassRoomId == CRid && a.ExamDate >= todayDate select a).FirstOrDefault();
            return row;
        }

        public AssignExamClassRoom getExamClaasssRoom(int id)
        {
            var getRow = (from a in db.AssignExamClassRooms where a.AssignExamClassRoomId == id select a).FirstOrDefault();
            return getRow;
        }

        public void addAllotExamClassRoom(int? AcademicYearId,int? ClassId,int SectionId,int SubjectId,int ExamId,DateTime ExamDate,string StartRollNumber,string EndRollNumber,int? StartRegisterId,int? EndRegisterId,int startRollNumberId,int endRollNumberId,int NumberOfStudents,string RoomNumber)
        {
            AssignExamClassRoom add = new AssignExamClassRoom()
            {
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                SubjectId = SubjectId,
                ExamId = ExamId,
                ExamDate = ExamDate,
                StartRollNumber = StartRollNumber,
                StartRegisterId = StartRegisterId,
                EndRegisterId = EndRegisterId,
                RoomNumber = RoomNumber,
                status = true
            };
            db.AssignExamClassRooms.Add(add);
            db.SaveChanges();
        }

        public List<EM_ExamClassRoomList> getExamClassRoomList(int? EM_allAcademicYears_List,int? EM_allClass_List,int secID,int EM_allExams_List,int EM_allSubjects_List)
        {
            var List = (from a in db.AssignExamClassRooms
                        from b in db.ClassRooms
                        where a.ClassRoomId == b.ClassRoomId &&
                        a.AcademicYearId == EM_allAcademicYears_List &&
                        a.ClassId == EM_allClass_List &&
                        a.SectionId == secID &&
                        a.ExamId == EM_allExams_List &&
                        a.SubjectId == EM_allSubjects_List
                        select new
                        {
                            ClassRoomId = a.AssignExamClassRoomId,
                            startRollNumber = a.StartRollNumber,
                            endRollNumber = a.EndRollNumber,
                            RoomNumber = b.RoomNumber,
                            ExamDate = a.ExamDate
                        }).ToList().Select(a => new EM_ExamClassRoomList 
                        { 
                            ClassRoomId = QSCrypt.Encrypt(a.ClassRoomId.ToString()),
                            StartRollNumber = a.startRollNumber,
                            EndRollNumber = a.endRollNumber,
                            RoomNumber = a.RoomNumber,
                            ExamDate = a.ExamDate.Value.ToString("dd MMM, yyyy")
                        }).ToList();
            return List;
        }

        public AssignExamClassRoom getRecord(int Edit_CR_Years, int Edit_CR_Class, int secId, int Edit_CR_Exam, int Edit_CR_Subject)
        {

            var getRecord = (from a in db.AssignExamClassRooms
                             where a.AcademicYearId == Edit_CR_Years &&
                                 a.ClassId == Edit_CR_Class &&
                                 a.SectionId == secId &&
                                 a.ExamId == Edit_CR_Exam &&
                                 a.SubjectId == Edit_CR_Subject
                             select a).First();
            return getRecord;
        }

        public void updateExamClassRoom(int Edit_CR_Years,int Edit_CR_Class,int secId,int Edit_CR_Exam,int Edit_CR_Subject,int StartRegisterId,int EndRegisterId,string getStartRollNumber,string getEndRollNumber,string Edit_CR_txt_RoomNumber)
        {
            var getRecord = (from a in db.AssignExamClassRooms
                             where a.AcademicYearId == Edit_CR_Years &&
                                 a.ClassId == Edit_CR_Class &&
                                 a.SectionId == secId &&
                                 a.ExamId == Edit_CR_Exam &&
                                 a.SubjectId == Edit_CR_Subject
                             select a).First();
            if (getRecord != null)
            {
                getRecord.StartRegisterId = StartRegisterId;
                getRecord.EndRegisterId = EndRegisterId;
                getRecord.StartRollNumber = getStartRollNumber;
                getRecord.EndRollNumber = getEndRollNumber;
                getRecord.RoomNumber = Edit_CR_txt_RoomNumber;
            } 
            db.SaveChanges();
        }

        public void DeleteExamClassRoom(int id)
        {
            var getRow = (from a in db.AssignExamClassRooms where a.AssignExamClassRoomId == id select a).FirstOrDefault();
            db.AssignExamClassRooms.Remove(getRow);
            db.SaveChanges();
        }

        public List<CheckRollNumberExist> checkRollNumberExist(int? allAcademicYears, int? allClass, int secId, int allSubjects, int allExams)
        {
            var getRow = (from a in db.AssignExamClassRooms
                          where a.AcademicYearId == allAcademicYears &&
                          a.ClassId == allClass &&
                          a.SectionId == secId &&
                          a.SubjectId == allSubjects &&
                          a.ExamId == allExams
                          select new CheckRollNumberExist
                          {
                              StartRollNumberId = a.StartRollNumberId.Value,
                              EndRollNumberId = a.EndRollNumberId.Value
                          }).ToList();
            return getRow;
        }

        public List<CheckExamExist> checkExamExist(int? allAcademicYears, int? allClass, int secId, int allSubjects, int allExams)
        {
            var getRow = (from a in db.ExamAttendances
                          where a.AcademicYearId == allAcademicYears &&
                          a.ClassId == allClass &&
                          a.SectionId == secId &&
                          a.SubjectId == allSubjects &&
                          a.ExamId == allExams 
                         // a.AttendanceStatus=="P"

                          select new CheckExamExist
                          {
                              StartRollNumberId = a.StudentRegisterId
                             
                          }).ToList();
            return getRow;
        }
        public List<CheckExamExist> checkExamStatus(int? allAcademicYears, int? allClass, int secId, int allSubjects, int allExams)
        {
            var getRow = (from a in db.ExamAttendances
                          where a.AcademicYearId == allAcademicYears &&
                          a.ClassId == allClass &&
                          a.SectionId == secId &&
                          a.SubjectId == allSubjects &&
                          a.ExamId == allExams &&
                          a.AttendanceStatus=="P"

                          select new CheckExamExist
                          {
                              StartRollNumberId = a.StudentRegisterId

                          }).ToList();
            return getRow;
        }
       
        public void addAllotExamClassRoom(int? AcademicYearId, int? ClassId, int SectionId, int SubjectId, int ExamId, DateTime ExamDate, string StartRollNumber, string EndRollNumber, int? StartRegisterId, int? EndRegisterId, int startRollNumberId, int endRollNumberId, int NumberOfStudents, int RoomNumberId, int schedule)
        {
            AssignExamClassRoom add = new AssignExamClassRoom()
            {
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                SubjectId = SubjectId,
                ExamId = ExamId,
                ExamDate = ExamDate,
                StartRollNumber = StartRollNumber,
                EndRollNumber= EndRollNumber,
                StartRegisterId = StartRegisterId,
                EndRegisterId = EndRegisterId,
                StartRollNumberId = startRollNumberId,
                EndRollNumberId = endRollNumberId,
                NumberOfStudents = NumberOfStudents,
                ClassRoomId = RoomNumberId,
                TimeScheduleId = schedule,
                status = true
            };
            db.AssignExamClassRooms.Add(add);
            db.SaveChanges();
        }

        public EditExamClassRoom examClassRoomDetails(int id)
        {
            var getRow = (from a in db.AssignExamClassRooms
                          from b in db.AcademicYears
                          from c in db.Classes
                          from d in db.Sections
                          from e in db.Exams
                          from f in db.Subjects
                          where a.AcademicYearId == b.AcademicYearId &&
                          a.ClassId == c.ClassId &&
                          a.SectionId == d.SectionId &&
                          a.ExamId == e.ExamId &&
                          a.SubjectId == f.SubjectId &&
                          a.AssignExamClassRoomId == id
                          select new
                          {
                              year = b.AcademicYear1,
                              clas = c.ClassType,
                              sec = d.SectionName,
                              exam = e.ExamName,
                              sub = f.SubjectName,
                              yearId = a.AcademicYearId.Value,
                              ClassId = a.ClassId.Value,
                              SectionId = a.SectionId.Value,
                              StartRollNumId = a.StartRollNumberId.Value,
                              EndRollNumId = a.EndRollNumberId.Value,
                              classRoomId = a.ClassRoomId.Value,
                              RowId = a.AssignExamClassRoomId
                          }).ToList().Select(a => new EditExamClassRoom
                          {
                              year = a.year,
                              clas = a.clas,
                              sec = a.sec,
                              exam = a.exam,
                              sub = a.sub,
                              yearId = a.yearId,
                              ClassId = a.ClassId,
                              SectionId = a.SectionId,
                              StartRollNumId = a.StartRollNumId,
                              EndRollNumId = a.EndRollNumId,
                              ClassRoomId = a.classRoomId,
                              RowId = QSCrypt.Encrypt(a.RowId.ToString())
                          }).FirstOrDefault();
            return getRow;
        }

        public void updateExamClassRoom(int RowId, string StartRollNumber, string EndRollNumber, int? StartRegisterId, int? EndRegisterId, int startRollNumberId, int endRollNumberId, int NumberOfStudents, int RoomNumberId)
        {
            var getRow = (from a in db.AssignExamClassRooms where a.AssignExamClassRoomId == RowId select a).FirstOrDefault();
            getRow.StartRollNumber = StartRollNumber;
            getRow.EndRollNumber = EndRollNumber;
            getRow.StartRegisterId = StartRegisterId;
            getRow.EndRegisterId = EndRegisterId;
            getRow.StartRollNumberId = startRollNumberId;
            getRow.EndRollNumberId = endRollNumberId;
            getRow.NumberOfStudents = NumberOfStudents;
            getRow.ClassRoomId = RoomNumberId;
            db.SaveChanges();
        }

        public List<ExamRoomList> getExamRoomList(int YearId, int ExamId, DateTime date, int Schedule)
        {
            var List = (from a in db.AssignExamClassRooms
                        from b in db.ClassRooms
                        where a.ClassRoomId == b.ClassRoomId &&
                           a.AcademicYearId == YearId &&
                           a.ExamId == ExamId &&
                           a.ExamDate == date &&
                           a.TimeScheduleId == Schedule &&
                           a.status == true
                        select new ExamRoomList
                        {
                            RoomNumber = b.RoomNumber,
                            ClassRoomId = a.ClassRoomId
                        }).Distinct().ToList().Select(a => new ExamRoomList
                        {
                            RoomNumber = a.RoomNumber,
                            RoomId = QSCrypt.Encrypt(a.ClassRoomId.ToString())
                        }).ToList();
            return List;
        }

        public IEnumerable<Object> GetClassList(int YearId, int ExamId,DateTime PassDate, int Schedule, int RoomId)
        {
            var RoomList = (from a in db.AssignExamClassRooms
                            from b in db.Classes
                            from c in db.Sections
                            where a.ClassId == b.ClassId &&
                            a.SectionId == c.SectionId &&
                            a.AcademicYearId == YearId &&
                            a.ExamId == ExamId &&
                            a.ExamDate == PassDate &&
                            a.TimeScheduleId == Schedule &&
                            a.ClassRoomId == RoomId
                            select new
                            {
                                classId = a.ClassId,
                                className = b.ClassType,
                                sectionId = a.SectionId,
                                sectionName = c.SectionName,
                            }).ToList().Select(a => new
                            {
                                ClassWithSectionName = a.className + " " + a.sectionName,
                                ClassWithSectionId = a.classId + " " + a.sectionId
                            }).Distinct().ToList();
            return RoomList;
        }

        public AssignExamClassRoom GetRollNumber(int YearId, int ExamId, DateTime date, int schedule, int RoomId, int classId, int secId)
        {
            var getRow = (from a in db.AssignExamClassRooms
                          where a.AcademicYearId == YearId &&
                          a.ClassId == classId &&
                          a.SectionId == secId &&
                          a.ExamId == ExamId &&
                          a.ExamDate == date &&
                          a.TimeScheduleId == schedule &&
                          a.ClassRoomId == RoomId
                          select a).FirstOrDefault();
            return getRow;
        }

        public List<ExamRollNo> GetRollNumber1(int YearId, int ExamId, DateTime date, int schedule, int RoomId, int classId, int secId)
        {
            var list = (from a in db.AssignExamClassRooms
                        where a.AcademicYearId == YearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.ExamId == ExamId &&
                        a.ExamDate == date &&
                        a.TimeScheduleId == schedule &&
                        a.ClassRoomId == RoomId
                        select new ExamRollNo
                        {
                            StartRoolNo = a.StartRollNumberId,
                            EndRoolNo = a.EndRollNumberId,
                            SubjectId = a.SubjectId,
                            StartRegisterId = a.StartRegisterId
                        }).ToList();

            return list;
        }

        public AssignExamClassRoom getRow (int id)
        {
            var Row = (from a in db.AssignExamClassRooms where a.AssignExamClassRoomId == id select a).FirstOrDefault();
            return Row;
        }

        public AssignExamClassRoom checkExamRoomAssigned(int YearId, int ExamId, DateTime date, int schedule)
        {
            var getRow = (from a in db.AssignExamClassRooms
                          where a.AcademicYearId == YearId &&
                          a.ExamId == ExamId &&
                          a.ExamDate == date &&
                          a.TimeScheduleId == schedule
                          select a).FirstOrDefault();
            return getRow;
        }

        public List<ExamRoomStudentCount> GetStudentCount(int? passYearId, int passExamId, DateTime ExamDate, int ScId, int passRoomNo)
        {
            var list = (from a in db.AssignExamClassRooms
                        where a.AcademicYearId == passYearId &&
                        a.ExamId == passExamId &&
                        a.ExamDate == ExamDate &&
                        a.TimeScheduleId == ScId &&
                        a.ClassRoomId == passRoomNo
                        select new ExamRoomStudentCount
                        {
                            AssignExamClassRoomId = a.AssignExamClassRoomId,
                            NumberOfStudents = a.NumberOfStudents
                        }).ToList();
          
            return list;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}