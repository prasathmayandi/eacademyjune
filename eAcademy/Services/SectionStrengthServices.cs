﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System.Collections.Generic;
using System.Linq;
using System;

namespace eAcademy.Services
{
    public class SectionStrengthServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        internal List<StrengthList> getStrength()
        {
            var ans = (from t1 in dc.SectionStrengths
                       from t2 in dc.AcademicYears
                       from t3 in dc.Classes
                       from t4 in dc.Sections
                       where t1.AcademicYearId == t2.AcademicYearId && t1.ClassId == t3.ClassId && t1.SectionId == t4.SectionId
                       select new {acid=t2.AcademicYearId,cid=t3.ClassId,secid=t4.SectionId, acyr = t2.AcademicYear1, className = t3.ClassType, section = t4.SectionName, strength = t1.Strength, status = t1.Status, id = t1.SectionStrengthId }).ToList().
                     Select(q => new StrengthList { Acid = q.acid, Classid = q.cid, Secid = q.secid, AcademicYear = q.acyr, ClassSection = q.className+" / "+q.section, Strength = q.strength, Status = q.status.Value, Strength_Id = QSCrypt.Encrypt(q.id.ToString()) }).ToList();
            return ans;
        }
        internal StrengthList getStrength(int cs_Id)
        {
            var ans = (from t1 in dc.SectionStrengths
                       from t2 in dc.AcademicYears
                       from t3 in dc.Classes
                       from t4 in dc.Sections
                       where t1.AcademicYearId == t2.AcademicYearId && t1.ClassId == t3.ClassId && t1.SectionId == t4.SectionId && t1.SectionStrengthId == cs_Id
                       select new StrengthList { 
                           Acid = t2.AcademicYearId,
                           Classid = t3.ClassId,
                           Secid = t4.SectionId,
                           AcademicYear = t2.AcademicYear1, 
                           Class = t3.ClassType, 
                           Section = t4.SectionName,
                           Strength = t1.Strength, 
                           Status = t1.Status,
                           StrengthId = t1.SectionStrengthId }).FirstOrDefault();
            return ans;
        }
        internal bool checkStrength(int acid, int cid, int secid)
        {
            var ans = dc.SectionStrengths.Where(q => q.AcademicYearId.Value.Equals(acid)  && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(secid)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal void addStrength(int acid, int cid, int secid, int strength)
        {
            SectionStrength add = new SectionStrength()
            {
                AcademicYearId = acid,
                ClassId = cid,
                SectionId = secid,
                Strength = strength,
                Status = true
            };

            dc.SectionStrengths.Add(add);
            dc.SaveChanges();
        }

        internal void updatClassStrength(int CSid, int Strength, string status)
        {
            var update = dc.SectionStrengths.Where(q => q.SectionStrengthId.Equals(CSid)).First();
            update.Strength = Strength;
            if (status == "Active")
            {
                update.Status = true;
            }
            else 
            {
                update.Status = false;
            }
            dc.SaveChanges();
        }

        public int checkstudentstrength(int CSid, int academicyearid)
        {
            var count = (from a in dc.AssignClassToStudents where a.SectionId == CSid && a.AcademicYearId == academicyearid select a).ToList().Count();
            return count;
        }
        internal void allocateCRtoSection(int SSid, int CRid)
        {
            var update = dc.SectionStrengths.Where(q => q.SectionStrengthId.Equals(SSid)).First();
            update.ClassRoomId = CRid;
            dc.SaveChanges();
        }

        internal bool checkCRtoSection(int SSid, int CRid)
        {
            var acYear = dc.SectionStrengths.Where(q => q.SectionStrengthId.Equals(SSid)).Select(q => new { acid = q.AcademicYearId }).FirstOrDefault();
            int id = acYear.acid.Value;

            var ans =(from t1 in dc.SectionStrengths
                     where t1.SectionStrengthId != SSid && t1.ClassRoomId.Value==CRid && t1.AcademicYearId.Value==id && t1.Status == true
                      select t1    
                          ).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public SectionStrength sectioncheckusing(int sec_id)
        {
            var ans = (from a in dc.SectionStrengths where a.SectionId == sec_id select a).FirstOrDefault();
            //var ans = dc.AssignClassToStudents.Where(query => query.SectionId.Equals(sec_id)).FirstOrDefault();
            return ans;
        }

        public SectionStrength TotalStrength(int yearId, int classId, int secId)
        {
            var getRow = (from a in dc.SectionStrengths where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId select a).FirstOrDefault();
            return getRow;
        }

        public SectionStrength IsClassRoomAssigned(int crId, int yearId)
        {
            var row = (from a in dc.SectionStrengths where a.ClassRoomId == crId && a.AcademicYearId == yearId && a.Status == true select a).FirstOrDefault();
            return row;
        }

        public bool deleteAcademicYear(int acid)
        {
            var ans = (from c in dc.SectionStrengths
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {              
                return true;
            }
            else
            {
                return false;
            }
        }  

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}