﻿using eAcademy.DataModel;
using System;
using System.Linq;

namespace eAcademy.Services
{
    public class ParentRegisterServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public ParentRegister getParentRegisterRow(int onlineRegId, int admissionId)
        {
            var getRow = (from ac in dc.ParentRegisters where ac.OnlineRegisterId == onlineRegId && ac.StudentAdmissionId == admissionId select ac).First();
            return getRow;
        }
        public ParentRegister findParent(int id)
        {
            var a = dc.ParentRegisters.Where(q => q.OnlineRegisterId.Value.Equals(id)).FirstOrDefault();
            return a;

        }

        public void AddParentDetailsForm1(int regid, int? stuid, string primaryuser, string faName, string faOccu, long? faContact, string faEmail, string moName, string MoOccu, long? MoContact, string MoEmail, string CompanyName, string Offaddr1, string offaddr2,
         string offcity, string offstate, int? offcountry, long? offpostel, long? offcontact, string guname, string gurelation, string gugender, string guQual, long? guMobile, string guemail, string guOccu, long? guAnnual, string username, string userpwd,
         string contactpersonName, long? ContactPersoncontact, string contactPersonRelation, string guardianReq, string worksameschool)
        {
            ParentRegister add = new ParentRegister()
            {
                StudentAdmissionId = stuid,
                OnlineRegisterId = regid,
                FatherFirstName = faName,
                FatherOccupation = faOccu,
                FatherMobileNo = faContact,
                FatherEmail = faEmail,
                MotherFirstname = moName,
                MotherOccupation = MoOccu,
                MotherMobileNo = MoContact,
                MotherEmail = MoEmail,
                UserCompanyname = CompanyName,
                CompanyAddress1 = Offaddr1,
                CompanyAddress2 = offaddr2,
                CompanyCity = offcity,
                CompanyState = offstate,
                CompanyCountry = offcountry,
                CompanyPostelcode = offpostel,
                CompanyContact = offcontact,
                GuardianFirstname = guname,
                GuRelationshipToChild = gurelation,
                GuardianGender = gugender,
                GuardianQualification = guQual,
                GuardianMobileNo = guMobile,
                GuardianEmail = guemail,
                GuardianOccupation = guOccu,
                GuardianIncome = guAnnual,
                UserName = username,
                UserPassword = userpwd,
                EmergencyContactPersonName = contactpersonName,
                EmergencyContactNumber = ContactPersoncontact,
                ContactPersonRelationship = contactPersonRelation,
                GuardianRequried = guardianReq,
                WorkSameSchool = worksameschool,
                PrimaryUser = primaryuser,

            };
            dc.ParentRegisters.Add(add);
            dc.SaveChanges();
        }

        public void UpdateParentDetailsForm1(int regid, int? stuid, string primaryuser, string faName, string faOccu, long? faContact, string faEmail, string moName, string MoOccu, long? MoContact, string MoEmail, string CompanyName, string Offaddr1, string offaddr2,
     string offcity, string offstate, int? offcountry, long? offpostel, long? offcontact, string guname, string gurelation, string gugender, string guQual, long? guMobile, string guemail, string guOccu, long? guAnnual, string userpwd,
         string contactpersonName, long? ContactPersoncontact, string contactPersonRelation, string guardianReq, string worksameschool)
        {
            var getrow = (from a in dc.ParentRegisters where a.OnlineRegisterId == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {

                getrow.FatherFirstName = faName;
                getrow.FatherOccupation = faOccu;
                getrow.FatherMobileNo = faContact;
                getrow.FatherEmail = faEmail;
                getrow.MotherFirstname = moName;
                getrow.MotherOccupation = MoOccu;
                getrow.MotherMobileNo = MoContact;
                getrow.MotherEmail = MoEmail;
                getrow.UserCompanyname = CompanyName;
                getrow.CompanyAddress1 = Offaddr1;
                getrow.CompanyAddress2 = offaddr2;
                getrow.CompanyCity = offcity;
                getrow.CompanyState = offstate;
                getrow.CompanyCountry = offcountry;
                getrow.CompanyPostelcode = offpostel;
                getrow.CompanyContact = offcontact;
                getrow.GuardianFirstname = guname;
                getrow.GuRelationshipToChild = gurelation;
                getrow.GuardianGender = gugender;
                getrow.GuardianQualification = guQual;
                getrow.GuardianMobileNo = guMobile;
                getrow.GuardianEmail = guemail;
                getrow.GuardianOccupation = guOccu;
                getrow.GuardianIncome = guAnnual;
                getrow.UserPassword = userpwd;
                getrow.EmergencyContactPersonName = contactpersonName;
                getrow.EmergencyContactNumber = ContactPersoncontact;
                getrow.ContactPersonRelationship = contactPersonRelation;
                getrow.GuardianRequried = guardianReq;
                getrow.WorkSameSchool = worksameschool;
                getrow.PrimaryUser = primaryuser;
                dc.SaveChanges();
            }
        }

        public void UpdateParentDetailsForm4(int regid, int stuid, string primaryuser, string faName, string faOccu, long? faContact, string faEmail, string moName, string MoOccu, long? MoContact, string MoEmail, string CompanyName, string Offaddr1, string offaddr2,
      string offcity, string offstate, int? offcountry, long? offpostel, long? offcontact, string guname, string gurelation, string gugender, string guQual, long? guMobile, string guemail, string guOccu, long? guAnnual, string uname, string userpwd,
          string contactpersonName, long? ContactPersoncontact, string contactPersonRelation, string guardianReq, string fatherlastname, DateTime? fatherdob, string fatherquali, string motherlastname, DateTime? motherdob, string motherquali, long? totalincome, string work)
        {
            var getrow = (from a in dc.ParentRegisters where a.OnlineRegisterId == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {

                getrow.FatherFirstName = faName;
                getrow.FatherOccupation = faOccu;
                getrow.FatherMobileNo = faContact;
                getrow.FatherEmail = faEmail;
                getrow.MotherFirstname = moName;
                getrow.MotherOccupation = MoOccu;
                getrow.MotherMobileNo = MoContact;
                getrow.MotherEmail = MoEmail;
                getrow.UserCompanyname = CompanyName;
                getrow.CompanyAddress1 = Offaddr1;
                getrow.CompanyAddress2 = offaddr2;
                getrow.CompanyCity = offcity;
                getrow.CompanyState = offstate;
                getrow.CompanyCountry = offcountry;
                getrow.CompanyPostelcode = offpostel;
                getrow.CompanyContact = offcontact;
                getrow.GuardianFirstname = guname;
                getrow.GuRelationshipToChild = gurelation;
                getrow.GuardianGender = gugender;
                getrow.GuardianQualification = guQual;
                getrow.GuardianMobileNo = guMobile;
                getrow.GuardianEmail = guemail;
                getrow.GuardianOccupation = guOccu;
                getrow.GuardianIncome = guAnnual;
                getrow.UserName = uname;
                getrow.UserPassword = userpwd;
                getrow.EmergencyContactPersonName = contactpersonName;
                getrow.EmergencyContactNumber = ContactPersoncontact;
                getrow.ContactPersonRelationship = contactPersonRelation;
                getrow.GuardianRequried = guardianReq;
                getrow.FatherLastName = fatherlastname;
                getrow.FatherDOB = fatherdob;
                getrow.FatherQualification = fatherquali;
                getrow.MotherLastName = motherlastname;
                getrow.MotherDOB = motherdob;
                getrow.MotherQualification = motherquali;
                getrow.TotalIncome = totalincome;
                getrow.WorkSameSchool = work;
                getrow.PrimaryUser = primaryuser;
                dc.SaveChanges();
            }
        }

        public void UpdatePrimaryUser(int regid, int stuid, string uname, string useremail, long? usercontact)
        {
            var getrow = (from a in dc.ParentRegisters where a.OnlineRegisterId == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.PrimaryUserName = uname;
                getrow.PrimaryUserEmail = useremail;
                getrow.PrimaryUserContact = usercontact;
                dc.SaveChanges();
            }

        }

        public void AddParentDetailsForm4(int regid, int stuid)
        {
            ParentRegister add = new ParentRegister()
            {
                StudentAdmissionId = stuid,
                OnlineRegisterId = regid
            };
            dc.ParentRegisters.Add(add);
            dc.SaveChanges();
        }

        //Offline
        public ParentRegister findParentOffline(string id)
        {
            var a = dc.ParentRegisters.Where(q => q.OfflineApplicationID.Equals(id)).FirstOrDefault();
            return a;

        }
        public void AddParentDetailsForm4Offline(string id, int stuid)
        {
            ParentRegister add = new ParentRegister()
            {
                StudentAdmissionId = stuid,
                OfflineApplicationID = id
            };
            dc.ParentRegisters.Add(add);
            dc.SaveChanges();

        }

        public void UpdateParentDetailsForm4Offline(string id, int stuid, string primaryuser, string faName, string faOccu, long? faContact, string faEmail, string moName, string MoOccu, long? MoContact, string MoEmail, string CompanyName, string Offaddr1, string offaddr2,
    string offcity, string offstate, int? offcountry, long? offpostel, long? offcontact, string guname, string gurelation, string gugender, string guQual, long? guMobile, string guemail, string guOccu, long? guAnnual,
        string contactpersonName, long? ContactPersoncontact, string contactPersonRelation, string guardianReq, string fatherlastname, DateTime? fatherdob, string fatherquali, string motherlastname, DateTime? motherdob, string motherquali, long? totalincome, string work)
        {
            var getrow = (from a in dc.ParentRegisters where a.OfflineApplicationID == id && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.FatherFirstName = faName;
                getrow.FatherOccupation = faOccu;
                getrow.FatherMobileNo = faContact;
                getrow.FatherEmail = faEmail;
                getrow.MotherFirstname = moName;
                getrow.MotherOccupation = MoOccu;
                getrow.MotherMobileNo = MoContact;
                getrow.MotherEmail = MoEmail;
                getrow.UserCompanyname = CompanyName;
                getrow.CompanyAddress1 = Offaddr1;
                getrow.CompanyAddress2 = offaddr2;
                getrow.CompanyCity = offcity;
                getrow.CompanyState = offstate;
                getrow.CompanyCountry = offcountry;
                getrow.CompanyPostelcode = offpostel;
                getrow.CompanyContact = offcontact;
                getrow.GuardianFirstname = guname;
                getrow.GuRelationshipToChild = gurelation;
                getrow.GuardianGender = gugender;
                getrow.GuardianQualification = guQual;
                getrow.GuardianMobileNo = guMobile;
                getrow.GuardianEmail = guemail;
                getrow.GuardianOccupation = guOccu;
                getrow.GuardianIncome = guAnnual;
                getrow.EmergencyContactPersonName = contactpersonName;
                getrow.EmergencyContactNumber = ContactPersoncontact;
                getrow.ContactPersonRelationship = contactPersonRelation;
                getrow.GuardianRequried = guardianReq;
                getrow.FatherLastName = fatherlastname;
                getrow.FatherDOB = fatherdob;
                getrow.FatherQualification = fatherquali;
                getrow.MotherLastName = motherlastname;
                getrow.MotherDOB = motherdob;
                getrow.MotherQualification = motherquali;
                getrow.TotalIncome = totalincome;
                getrow.WorkSameSchool = work;
                getrow.PrimaryUser = primaryuser;
                dc.SaveChanges();
            }
        }
        public void UpdatePrimaryUserOffline(string regid, int stuid, string uname, string useremail, long? usercontact)
        {
            var getrow = (from a in dc.ParentRegisters where a.OfflineApplicationID == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.PrimaryUserName = uname;
                getrow.PrimaryUserEmail = useremail;
                getrow.PrimaryUserContact = usercontact;
                dc.SaveChanges();
            }

        }

        public void AddParentDetailsForm1Offline(string regid, int stuid, string primaryuser, string faName, string faOccu, long? faContact, string faEmail, string moName, string MoOccu, long? MoContact, string MoEmail, string CompanyName, string Offaddr1, string offaddr2,
       string offcity, string offstate, int? offcountry, long? offpostel, long? offcontact, string guname, string gurelation, string gugender, string guQual, long? guMobile, string guemail, string guOccu, long? guAnnual,
       string contactpersonName, long? ContactPersoncontact, string contactPersonRelation, string guardianReq, string worksameschool)
        {
            ParentRegister add = new ParentRegister()
            {
                StudentAdmissionId = stuid,
                OfflineApplicationID = regid,

                FatherFirstName = faName,
                FatherOccupation = faOccu,
                FatherMobileNo = faContact,
                FatherEmail = faEmail,
                MotherFirstname = moName,
                MotherOccupation = MoOccu,
                MotherMobileNo = MoContact,
                MotherEmail = MoEmail,
                UserCompanyname = CompanyName,
                CompanyAddress1 = Offaddr1,
                CompanyAddress2 = offaddr2,
                CompanyCity = offcity,
                CompanyState = offstate,
                CompanyCountry = offcountry,
                CompanyPostelcode = offpostel,
                CompanyContact = offcontact,
                GuardianFirstname = guname,
                GuRelationshipToChild = gurelation,
                GuardianGender = gugender,
                GuardianQualification = guQual,
                GuardianMobileNo = guMobile,
                GuardianEmail = guemail,
                GuardianOccupation = guOccu,
                GuardianIncome = guAnnual,
                EmergencyContactPersonName = contactpersonName,
                EmergencyContactNumber = ContactPersoncontact,
                ContactPersonRelationship = contactPersonRelation,
                GuardianRequried = guardianReq,
                WorkSameSchool = worksameschool,
                PrimaryUser = primaryuser,
            };
            dc.ParentRegisters.Add(add);
            dc.SaveChanges();
        }

        public void UpdateParentDetailsForm1Offline(string regid, int stuid, string primaryuser, string faName, string faOccu, long? faContact, string faEmail, string moName, string MoOccu, long? MoContact, string MoEmail, string CompanyName, string Offaddr1, string offaddr2,
     string offcity, string offstate, int? offcountry, long? offpostel, long? offcontact, string guname, string gurelation, string gugender, string guQual, long? guMobile, string guemail, string guOccu, long? guAnnual,
         string contactpersonName, long? ContactPersoncontact, string contactPersonRelation, string guardianReq, string worksameschool)
        {
            var getrow = (from a in dc.ParentRegisters where a.OfflineApplicationID == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {

                getrow.FatherFirstName = faName;
                getrow.FatherOccupation = faOccu;
                getrow.FatherMobileNo = faContact;
                getrow.FatherEmail = faEmail;
                getrow.MotherFirstname = moName;
                getrow.MotherOccupation = MoOccu;
                getrow.MotherMobileNo = MoContact;
                getrow.MotherEmail = MoEmail;
                getrow.UserCompanyname = CompanyName;
                getrow.CompanyAddress1 = Offaddr1;
                getrow.CompanyAddress2 = offaddr2;
                getrow.CompanyCity = offcity;
                getrow.CompanyState = offstate;
                getrow.CompanyCountry = offcountry;
                getrow.CompanyPostelcode = offpostel;
                getrow.CompanyContact = offcontact;
                getrow.GuardianFirstname = guname;
                getrow.GuRelationshipToChild = gurelation;
                getrow.GuardianGender = gugender;
                getrow.GuardianQualification = guQual;
                getrow.GuardianMobileNo = guMobile;
                getrow.GuardianEmail = guemail;
                getrow.GuardianOccupation = guOccu;
                getrow.GuardianIncome = guAnnual;
                getrow.EmergencyContactPersonName = contactpersonName;
                getrow.EmergencyContactNumber = ContactPersoncontact;
                getrow.ContactPersonRelationship = contactPersonRelation;
                getrow.GuardianRequried = guardianReq;
                getrow.WorkSameSchool = worksameschool;
                getrow.PrimaryUser = primaryuser;
                dc.SaveChanges();
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}