﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace eAcademy.Services
{
    public class MapRoleFeatureServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public MapFeatureRole getMapFeature(int mapId)
        {
            var ans = (from t1 in dc.Features
                       from t2 in dc.MapRoleFeatures
                       from t3 in dc.Roles
                       where t1.FeatureId == t2.FeatureId && t2.RoleId == t3.RoleId && t2.MapId == mapId
                       select new MapFeatureRole { FeatureId = t1.FeatureId, FeatureType = t1.FaetureType, MapRoleFeatureId = t2.MapId, RoleId = t3.RoleId, RoleType = t3.RoleType, FeatureDetails = t1.FeatureDetails, Status = t2.status.Value }).First();
            return ans;
        }

        public bool checkMapRoleFeatures(int rid, int fid)
        {
            var ans = dc.MapRoleFeatures.Where(q => q.RoleId.Value.Equals(rid) && q.FeatureId.Value.Equals(fid)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<MapFeatureRole> getMapRole()
        {
            var ans = (from t1 in dc.Features
                       from t2 in dc.MapRoleFeatures
                       from t3 in dc.Roles
                       where t1.FeatureId == t2.FeatureId && t2.RoleId == t3.RoleId
                       select new { FeatureId = t1.FeatureId,  FeatureType = t1.FaetureType, MapRoleFeatureId = t2.MapId, RoleId = t3.RoleId, RoleType = t3.RoleType, FeatureDetails = t1.FeatureDetails, Status = t2.status.Value }).OrderBy(q => q.RoleType).OrderBy(q => q.RoleType).ToList().
                       Select(q => new MapFeatureRole { FeatureId = q.FeatureId, FeatureType = q.FeatureType, MapRoleFeatureId = q.MapRoleFeatureId,MRid = QSCrypt.Encrypt(q.MapRoleFeatureId.ToString()), RoleId = q.RoleId, RoleType = q.RoleType, FeatureDetails = q.FeatureDetails, Status = q.Status }).ToList();
            return ans;
        }

        public List<MapRoleFeature> getMapRoleFeatures()
        {
            var ans = dc.MapRoleFeatures.Select(q => q).OrderBy(d => d.RoleId).ToList();
            return ans;
        }

        public void addMapRoleFeatures(int rid, int fid)
        {
            MapRoleFeature map = new MapRoleFeature()
            {
                FeatureId = fid,
                RoleId = rid,
                status = true
            };

            dc.MapRoleFeatures.Add(map);
            dc.SaveChanges();
        }
        public void updateMapRole(int mapId, string status)
        {
            var update = dc.MapRoleFeatures.Where(q => q.MapId.Equals(mapId)).First();
            if (status == "Active")
            {
                update.status = true;
            }
            else
            {
                update.status = false;
            }
            dc.SaveChanges();
        }
        public IEnumerable GetRemainFeaturelist(int roleid)
        {
            var result1 = (from e in dc.Features
                           where e.FeatureStatus == true
                           select e).ToList();
            var result2 = (from e in dc.MapRoleFeatures
                           where e.RoleId == roleid
                           select e).ToList();
            var list1 = (from e in result1
                         where !(from m in result2
                                 select m.FeatureId).Contains(e.FeatureId)
                         select new
                         {
                             FeaturedId = e.FeatureId,
                             Feature = e.FaetureType
                         }).OrderBy(q => q.Feature).ToList();

            var list = (from a in dc.Features
                        where a.FeatureStatus == true && !dc.MapRoleFeatures.Any(f => f.FeatureId == a.FeatureId && f.RoleId == roleid)
                        select new
                        {
                            FeaturedId = a.FeatureId,
                            Feature = a.FaetureType
                        }).ToList();
            return list1;
        }
        public bool checkMapRoleFeatures(int rid, int fid, string status)
        {
            int count;
            if (status == "Active")
            {

                var ans = dc.MapRoleFeatures.Where(q => q.RoleId.Value.Equals(rid) && q.FeatureId.Value.Equals(fid) && q.status.Value.Equals(true)).ToList();
                count = ans.Count;
            }

            else
            {
                var ans = dc.MapRoleFeatures.Where(q => q.RoleId.Value.Equals(rid) && q.FeatureId.Value.Equals(fid) && q.status.Value.Equals(false)).ToList();
                count = ans.Count;
            }

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}