﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class FacultyDailyAttendanceServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        public FacultyDailyAttendance AttendanceExist(int yearId, DateTime date)
        {
            var getRow = (from a in db.FacultyDailyAttendances where a.AcademicYearId == yearId && a.CurrentDate == date select a).FirstOrDefault();
            return getRow;
        }

        public List<AttendanceExistList> getAttendanceExistList(int yearId, DateTime date)
        {
            var getRow = (from a in db.FacultyDailyAttendances
                          from b in db.TechEmployees
                          from c in db.tblAttendanceStatus
                          where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                          a.AttendanceStatus == c.AttendanceStatusValue &&
                          a.AcademicYearId == yearId &&
                          a.CurrentDate == date
                          orderby b.EmployeeId
                          select new
                          {
                              EmployeeRegId = a.EmployeeRegisterId,
                              EmployeeId = b.EmployeeId,
                              EmployeeName = b.EmployeeName,
                              status = a.AttendanceStatus,
                              comment = a.LeaveReason,
                              AttendanceStatusName = c.AttendanceStatusName,
                              AttendanceId = a.Id
                          }).OrderBy(x=>x.AttendanceId).ToList().Select(c => new AttendanceExistList
                          {
                              EmployeeRegId = QSCrypt.Encrypt(c.EmployeeRegId.ToString()),
                              EmployeeId = c.EmployeeId,
                              EmployeeName = c.EmployeeName,
                              status = c.status,
                              comment = c.comment,
                              AttendanceStatusName = c.AttendanceStatusName,
                          }).ToList();
            return getRow;
        }

        public void addFacultyDailyAttendance(int AcademicYearId,int EmployeeRegisterId,DateTime CurrentDate,string AttendanceStatus,string LeaveReason)
        {
            FacultyDailyAttendance add = new FacultyDailyAttendance()
            {
                AcademicYearId = AcademicYearId,
                EmployeeRegisterId = EmployeeRegisterId,
                CurrentDate = CurrentDate,
                AttendanceStatus = AttendanceStatus,
                LeaveReason = LeaveReason
            };
            db.FacultyDailyAttendances.Add(add);
            db.SaveChanges();
        }

        public List<FacultyAttendanceReport>FacultyAttendanceList(int empRegisterId, DateTime sDate, DateTime eDate)
        {
            var list = (from a in db.FacultyDailyAttendances
                         where a.EmployeeRegisterId == empRegisterId &&
                               a.CurrentDate >= sDate &&
                               a.CurrentDate <= eDate
                         select new FacultyAttendanceReport
                         {
                             Date = a.CurrentDate,
                             Status = a.AttendanceStatus,
                             Reason = a.LeaveReason
                         }).ToList();
            return list;

        }

        public void updateFacultyDailyAttendance(int RegId, int year, DateTime date, string status, string comment, DateTime updatedDate)
        {
            var getRow = (from a in db.FacultyDailyAttendances where a.EmployeeRegisterId == RegId && a.AcademicYearId == year && a.CurrentDate == date select a).FirstOrDefault();
            getRow.AttendanceStatus = status;
            getRow.LeaveReason = comment;
            getRow.AttendanceUpdatedDate = updatedDate;
            db.SaveChanges();
        }

        public void addFacultyDailyAttendance(int AcademicYearId, int EmployeeRegisterId, DateTime CurrentDate, string AttendanceStatus, string LeaveReason, DateTime markedDate)
        {
            FacultyDailyAttendance add = new FacultyDailyAttendance()
            {
                AcademicYearId = AcademicYearId,
                EmployeeRegisterId = EmployeeRegisterId,
                CurrentDate = CurrentDate,
                AttendanceStatus = AttendanceStatus,
                LeaveReason = LeaveReason,
                AttendanceMarkedDate = markedDate
            };
            db.FacultyDailyAttendances.Add(add);
            db.SaveChanges();
        }

        public void addFacultyDailyAttendanceOnLeaveAccept(int? empId, DateTime? fdate, string AttStatus, string Reason, DateTime? markedDate)
        {
            FacultyDailyAttendance add = new FacultyDailyAttendance()
            {
                EmployeeRegisterId = empId,
                CurrentDate = fdate,
                AttendanceStatus = AttStatus,
                LeaveReason = Reason,
                AttendanceMarkedDate = markedDate
            };
            db.FacultyDailyAttendances.Add(add);
            db.SaveChanges();
        }

        public List<FacultyAttendanceReport> FacultyAttendanceList(int? empRegisterId, DateTime? sDate, DateTime? eDate)
        {
            var list = (from a in db.FacultyDailyAttendances
                        where a.EmployeeRegisterId == empRegisterId &&
                              a.CurrentDate >= sDate &&
                              a.CurrentDate <= eDate
                        select new
                        {
                            Date = a.CurrentDate,
                            Status = a.AttendanceStatus,
                            Reason = a.LeaveReason,
                            fdate_search = a.CurrentDate,
                            toDate_search = a.CurrentDate,
                            empId_search = a.EmployeeRegisterId
                        }).ToList().Select(c => new FacultyAttendanceReport
                        {
                            Date1 = c.Date.Value.ToString("MMM dd,yyyy"),
                            Status = c.Status,
                            Reason = c.Reason,
                            Att_SectionStudent = c.empId_search,
                            txt_FromDate = c.fdate_search,
                            txt_ToDate = c.toDate_search
                        }).ToList();
            return list;

        }

        public IEnumerable<Object> getLeaveFaculty(DateTime date)
        {
            var FacultyList = (from a in db.FacultyDailyAttendances
                               from b in db.TechEmployees
                               where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                               a.CurrentDate == date &&
                               a.AttendanceStatus == "L"
                               select new
                               {
                                   EmployeeId = b.EmployeeId,
                                   EmployeeName = b.EmployeeName,
                                   EmployeeRegId = a.EmployeeRegisterId
                               }).Distinct().ToList().Select(a => new
                               {
                                   EmployeeId = a.EmployeeId,
                                   EmployeeName = a.EmployeeName,
                                   EmployeeRegId = QSCrypt.Encrypt(a.EmployeeRegId.ToString())
                               }).ToList();
            return FacultyList;
        }

        public FacultyDailyAttendance IsFacultyLeaveOnDate(int? empRegId, DateTime date)
        {
            var getRow = (from a in db.FacultyDailyAttendances where a.EmployeeRegisterId == empRegId && a.CurrentDate == date && a.AttendanceStatus == "L" select a).FirstOrDefault();
            return getRow;
        }
        //jegadeesh

        public List<Ranklist> GetTechEmployeeDailyAttendance(int yid, DateTime? da,int designationid)
        {
            var ans = (from a in db.FacultyDailyAttendances
                       from b in db.TechEmployees

                       where a.AcademicYearId == yid && a.CurrentDate == da && b.EmployeeDesignationId == designationid && a.EmployeeRegisterId == b.EmployeeRegisterId 
                       select new Ranklist
                       {
                           rollnumber = b.EmployeeId,
                           stuname = b.EmployeeName + " " + b.LastName,
                           status = a.AttendanceStatus
                       }).OrderBy(q=>q.rollnumber).ToList();
            return ans;
        }
        public List<FacultyDailyAttendance> TechEmployeeDailyPresentAttendance(int yid, DateTime da)
        {
            var ans = (from a in db.FacultyDailyAttendances

                       where a.AcademicYearId == yid && a.CurrentDate == da && a.AttendanceStatus == "P"
                       select a).ToList();
            return ans;
        }
        public List<FacultyDailyAttendance> TechEmployeeDailyAbsentAttendance(int yid, DateTime da)
        {
            var ans = (from a in db.FacultyDailyAttendances

                       where a.AcademicYearId == yid && a.CurrentDate == da && a.AttendanceStatus == "A"
                       select a).ToList();
            return ans;
        }
        public List<Ranklist> GetTechEmployeeFromToAttendance(int yid, DateTime? from1, DateTime? to ,int designationid)
        {
            var ans = (from a in db.FacultyDailyAttendances
                       from b in db.TechEmployees

                       where a.AcademicYearId == yid && a.CurrentDate >= from1 && a.CurrentDate <= to && b.EmployeeDesignationId == designationid && a.EmployeeRegisterId == b.EmployeeRegisterId
                       select new Ranklist
                       {
                           rollnumber = b.EmployeeId,
                           stuname = b.EmployeeName+""+b.LastName,
                           date = a.CurrentDate.Value,
                           status = a.AttendanceStatus
                       }).Distinct().OrderBy(q => q.date).ToList();
            return ans;
        }
        public List<FacultyDailyAttendance> TechEmployeeFromToPresentAttendance(int yid, DateTime from1, DateTime to, string r)
        {
            var ans = (from a in db.FacultyDailyAttendances
                       from b in db.TechEmployees
                       where a.AcademicYearId == yid && b.EmployeeId == r && a.EmployeeRegisterId == b.EmployeeRegisterId && a.AttendanceStatus == "P" && a.CurrentDate >= from1 && a.CurrentDate <= to
                       select a).ToList();
            return ans;
        }
        public List<Ranklist> GetTechEmployeeMonthAttendance(int yid, int? id,int designationid)
        {
            var ans = (from a in db.FacultyDailyAttendances
                       from b in db.TechEmployees

                       where a.AcademicYearId == yid && a.CurrentDate.Value.Month == id && b.EmployeeDesignationId == designationid && a.EmployeeRegisterId == b.EmployeeRegisterId
                       select new Ranklist
                       {
                           rollnumber = b.EmployeeId,
                           stuname = b.EmployeeName + " " + b.LastName,
                           date = a.CurrentDate.Value,
                           status = a.AttendanceStatus,
                           d = SqlFunctions.DateName("day", a.CurrentDate.Value).Trim()
                       }).OrderBy(q => q.date).ToList();
            return ans;
        }
        public List<FacultyDailyAttendance> TechEmployeeMonthPresentAttendance(int yid, int id, string r)
        {
            var ans = (from a in db.FacultyDailyAttendances
                       from b in db.TechEmployees
                       where a.AcademicYearId == yid && b.EmployeeId == r && a.EmployeeRegisterId == b.EmployeeRegisterId && a.AttendanceStatus == "P" && a.CurrentDate.Value.Month == id
                       select a).ToList();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}