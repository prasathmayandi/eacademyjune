﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class MotherRegisterServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        
        public int MotherdetailsCopyfromPreAdmissiontomother(PreAdmissionMotherRegister old, int primaryuserid, int studentregid)
        {
            var checkmotherexist = (from a in dc.AdmissionTransactions
                                    from b in dc.MotherRegisters
                                    where a.PrimaryUserRegisterId == primaryuserid && a.StudentRegisterId != studentregid && b.MotherRegisterId == a.MotherRegisterId && b.MotherFirstname == old.MotherFirstname &&
                                       b.MotherLastName == old.MotherLastName //&& b.MotherDOB == old.MotherDOB
                                    select b).FirstOrDefault();
            if (checkmotherexist != null)
            {
                return checkmotherexist.MotherRegisterId;
            }
            else
            {
                MotherRegister user = new MotherRegister()
                {
                    MotherFirstname = old.MotherFirstname,
                    MotherLastName = old.MotherLastName,
                    MotherDOB = old.MotherDOB,
                    MotherQualification = old.MotherQualification,
                    MotherOccupation = old.MotherOccupation,
                    MotherMobileNo = old.MotherMobileNo,
                    MotherEmail = old.MotherEmail
                };
                dc.MotherRegisters.Add(user);
                dc.SaveChanges();
                int motherregid = user.MotherRegisterId;

                return motherregid;
            }
        }

        public int EditAddMotherdetails(string mofirstname, string molastname, DateTime? modob, string moqual, string mooccu, long? momobile, string moemail)
        {
            MotherRegister mother = new MotherRegister()
            {
                MotherFirstname = mofirstname,
                MotherLastName = molastname,
                MotherDOB = modob,
                MotherQualification = moqual,
                MotherOccupation = mooccu,
                MotherMobileNo = momobile,
                MotherEmail = moemail
            };
            dc.MotherRegisters.Add(mother);
            dc.SaveChanges();

            return mother.MotherRegisterId;
        }

        public int EditmotherDetails1(int? motherid, string mofirstname, string molastname, DateTime? modob, string moqual, string mooccu, long? momobile, string moemail) //, 
        {
            var get = dc.MotherRegisters.Where(q => q.MotherRegisterId == motherid).FirstOrDefault();

            if (get != null)
            {
                get.MotherFirstname = mofirstname;
                get.MotherLastName = molastname;
                get.MotherDOB = modob;
                get.MotherEmail = moemail;
                get.MotherQualification = moqual;
                get.MotherOccupation = mooccu;
                get.MotherMobileNo = momobile;
                dc.SaveChanges();
            }
            return get.MotherRegisterId;

        }
        public int UpdatemotherDetails(int? motherid, string moqual, string mooccu, long? momobile) //, 
        {
            var get = dc.MotherRegisters.Where(q => q.MotherRegisterId == motherid).FirstOrDefault();

            if (get != null)
            {


                get.MotherQualification = moqual;
                get.MotherOccupation = mooccu;
                get.MotherMobileNo = momobile;
                dc.SaveChanges();
            }
            return get.MotherRegisterId;
        }

        public int CheckMotherExit(int primaryuserid, int Newstudentid, string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email,long? totalincome)
        {

            int Motherid = 0;

            var GetMother = (from a in dc.AdmissionTransactions
                             from b in dc.MotherRegisters
                             where a.PrimaryUserRegisterId == primaryuserid && b.MotherRegisterId == a.MotherRegisterId && b.MotherFirstname == firstname && b.MotherLastName == lastname && b.MotherDOB == dob
                             select b).FirstOrDefault();
            if (GetMother != null)
            {
                Motherid = GetMother.MotherRegisterId;
                UpdatemotherDetails(Motherid, qual, occu, mobile);
            }
            else
            {
                Motherid = EditAddMotherdetails(firstname, lastname, dob, qual, occu, mobile, email);
            }
            return Motherid;
        }

        public int CheckEditMotherdetailsExit(int primaryuserid, int Newstudentid,int mid, string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email)
        {
            int Motherid = 0;
            var GetMother = (from a in dc.AdmissionTransactions
                             from b in dc.MotherRegisters
                             where a.PrimaryUserRegisterId == primaryuserid && b.MotherRegisterId == a.MotherRegisterId && b.MotherFirstname == firstname && b.MotherLastName == lastname && b.MotherDOB == dob
                             select b).FirstOrDefault();
            if (GetMother != null)
            {
                Motherid = GetMother.MotherRegisterId;

                UpdatemotherDetails(Motherid, qual, occu, mobile);
                ChangeMotherStatusFlag(mid, Newstudentid);
            }
            else
            {
                Motherid = EditmotherDetails1(mid,firstname, lastname, dob, qual, occu, mobile, email);
            }
            return Motherid;
        }

        public void ChangeMotherStatusFlag(int motherid,int studentid)
        {
            var get = dc.MotherRegisters.Where(q => q.MotherRegisterId == motherid).FirstOrDefault();
            if(get !=null)
            {
                get.MotherStatusFlag = "False";
                get.StudentRegisterId = studentid;
                dc.SaveChanges();
            }
        }

        public bool CheckMotherSameinRemaingstudent(int primaryuserid, int studentid, List<AdmissionTransaction> old, string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email)
        {
            int flag = 0;
            int count = Convert.ToInt16(old.Count);
            foreach (var v in old)
            {
                var ans = (from a in dc.AdmissionTransactions
                           from b in dc.MotherRegisters
                           where a.PrimaryUserRegisterId == v.PrimaryUserRegisterId && a.StudentRegisterId == v.StudentRegisterId && b.MotherRegisterId == a.MotherRegisterId && b.MotherFirstname == firstname && b.MotherLastName == lastname && b.MotherDOB == dob
                           select b).FirstOrDefault();
                if (ans != null)
                {
                    flag++;
                }
            }
            if (flag == count)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}