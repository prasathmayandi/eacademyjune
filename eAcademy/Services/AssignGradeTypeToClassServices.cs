﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class AssignGradeTypeToClassServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public bool checkAssignGradeClass(int acid, int cid, int gid)
        {
            var ans = dc.AssignGradeTypeToClasses.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void addAssignGradeClass(int acid, int cid, int gtypeid)
        {
            AssignGradeTypeToClass add = new AssignGradeTypeToClass()
            {
                AcademicYearId = acid,
                ClassId = cid,
                GradeTypeId = gtypeid
            };
            dc.AssignGradeTypeToClasses.Add(add);
            dc.SaveChanges();
        }

        public List<GradeClass> getAssignGrade()
        {
            var ans = ((from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.GradeTypes
                       from t4 in dc.AssignGradeTypeToClasses
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.GradeTypeId == t4.GradeTypeId
                       select new GradeClass { AcYaerId=t1.AcademicYearId, AcYear = t1.AcademicYear1, Class = t2.ClassType, GradeType = t3.GradeTypeName, GradeClassId = t4.GradeClassId,ClassId=t2.ClassId }).ToList().
                       Select(s => new GradeClass {
                           AcYaerId=s.AcYaerId,
                           AcYear=s.AcYear,
                           Class=s.Class,
                           GradeType=s.GradeType,
                           gradesysid=QSCrypt.Encrypt(s.GradeClassId.ToString()),
                           ClassId=s.ClassId,
                           AcademicYear=s.AcYear
                       })).ToList();
            return ans;
        }


        public AssignGradeTypeToClass getgradetypeclass(int gradeclass)
        {
            var ans = (from a in dc.AssignGradeTypeToClasses where a.GradeClassId == gradeclass select a).FirstOrDefault();
            return ans;
        }

        public bool DeleteGrade(int id)
        {
            var ans = (from a in dc.AssignGradeTypeToClasses
                       from b in dc.StudentGrades
                       where a.GradeClassId == id && b.AcademicYearId == a.AcademicYearId && b.ClassId == a.ClassId
                       select a).FirstOrDefault();
            if(ans == null)
            {
                var deletegradesystem = dc.AssignGradeTypeToClasses.Where(q => q.GradeClassId == id).FirstOrDefault();
                if(deletegradesystem!=null)
                {
                    dc.AssignGradeTypeToClasses.Remove(deletegradesystem);
                    dc.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}