﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class PreAdmissionParentsSblingServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public void UpdateSblingdetails(int onlineregid,int studentid)
        {
            var ans = dc.PreAdmissionParentSiblings.Where(q => q.OnlineRegisterId == onlineregid).FirstOrDefault();
            if(ans!=null)
            {
                ans.StudentAdmissionId = studentid;
                dc.SaveChanges();
            }
        }
        public IEnumerable<Object> GetJsonSiblingonline(int onlineid)
        {
            var ans = (from a in dc.PreAdmissionParentSiblings
                       where a.OnlineRegisterId.Value.Equals(onlineid)
                       select new
                       {
                           StudentID = a.SiblingStudentId,
                           StudentRegId = a.SblingStudentRegId
                       }).ToList();
            
            return ans;
        }
        public IEnumerable<Object> GetJsonSiblingStudentId(int stuid)
        {
            var ans = (from a in dc.PreAdmissionParentSiblings
                       where a.StudentAdmissionId.Value.Equals(stuid)
                       select new
                       {
                           StudentID = a.SiblingStudentId,
                           StudentRegId = a.SblingStudentRegId
                       }).ToList();
            return ans;
        }

        //offline

        public void UpdateSblingdetailsOffline(string OfflineId, int studentid)
        {
            var ans = dc.PreAdmissionParentSiblings.Where(q => q.OfflineApplicationID == OfflineId).FirstOrDefault();
            if (ans != null)
            {
                ans.StudentAdmissionId = studentid;
                dc.SaveChanges();
            }
        }
        public IEnumerable<Object> GetJsonSiblingOffline(int offlineid)
        {
            var ans = (from a in dc.PreAdmissionParentSiblings
                       where a.OfflineApplicationID.Equals(offlineid)
                       select new
                       {
                           StudentID = a.SiblingStudentId,
                           StudentRegId = a.SblingStudentRegId

                       }).ToList();
            return ans;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}