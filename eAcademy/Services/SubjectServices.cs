﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class SubjectServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
       
        public IEnumerable<Object> ShowSubjects()
        {
            var SubjectList = (from a in dc.Subjects
                               where a.Status==true
                               select new
                               {
                                   SubjectName = a.SubjectName,
                                   SubjectId = a.SubjectId
                               }).ToList().AsEnumerable();
            return SubjectList;
        }
        
        public List<GroupSubjectList> GroupSubject()
        {
            var GroupSubjectList = (from a in dc.Subjects
                               where a.GroupSubject == true
                                    select new GroupSubjectList
                               {
                                   Subject = a.SubjectName,
                                   SubjectId = a.SubjectId
                               }).ToList();
            return GroupSubjectList;
        }
        
        public SubjectList getSubjectBySubjetID(int sub_id)
        {
            var ans = (from t1 in dc.Subjects
                       where t1.SubjectId == sub_id
                       select new SubjectList
                       {
                          
                           SubjectId = t1.SubjectId,
                           Subject = t1.SubjectName,
                           Non_Subject = t1.NonAcademicSubject,
                           GroupSubjectStatus=t1.GroupSubject,
                           Status =t1.Status
                       }).FirstOrDefault();
                
            return ans;
        }
        public AssignSubjectToSection CheckSubjectAssgin(int sub_id,int year)
        {
            //int count;
            
            var row = (from a in dc.AssignSubjectToSections

                       where a.SubjectId == sub_id && a.AcademicYearId == year && a.Status==true
                       select a).FirstOrDefault();
            return row;
            
        }
        public AssignSubjectToSection CheckGroupSubjectAssgin(int Groupid, int year)
        {
            //int count;

            var row = (from a in dc.AssignSubjectToSections

                       where a.GroupSubjectId == Groupid && a.AcademicYearId == year && a.Status == true
                       select a).FirstOrDefault();
            return row;

        }
        public Subject getSubjectIdBySubjet(string subject)
        {
            var ans = (from a in dc.Subjects where a.SubjectName == subject select a).FirstOrDefault();
            return ans;
        }
        public IEnumerable<object> getSubject(int acid, int cid, int sec_id)
        {
            var ans = (from t1 in dc.Subjects
                       from t2 in dc.AssignSubjectToSections
                       where t1.SubjectId == t2.SubjectId && t2.ClassId == cid && t2.SectionId == sec_id && t2.AcademicYearId.Value == acid
                       select new { sub_id = t1.SubjectId, sub_name = t1.SubjectName }).ToList();
            return ans;
        }

        public bool checkSubject(string subjectName)
        {
            var ans = dc.Subjects.Where(q => q.SubjectName.Equals(subjectName)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public bool checkSubject(string subjectName, string status)
        {
            int count;
            if (status == "Active")
            {
                var ans = dc.Subjects.Where(q => q.SubjectName.Equals(subjectName) && q.Status.Value.Equals(true)).ToList();
                count = ans.Count;
            }

            else
            {
                var ans = dc.Subjects.Where(q => q.SubjectName.Equals(subjectName) && q.Status.Value.Equals(false)).ToList();
                count = ans.Count;
            }

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool checkExistSubject(string subjectName, int sub_id)
        {
                //var ans = dc.Subjects.Where(q => q.SubjectName.Equals(subjectName) && q.Status.Value.Equals(true)).ToList();
            var ans = (from a in dc.Subjects where subjectName == a.SubjectName && a.SubjectId != sub_id select a).FirstOrDefault();
            if (ans == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public List<SubjectList> getSubject()
        {
            var ans = (from t1 in dc.Subjects
                       select new
                       {
                           subjectid=t1.SubjectId,
                           subject = t1.SubjectName,
                           nonAcademic = t1.NonAcademicSubject,
                           status = t1.Status,
                           groupSubjectStatus=t1.GroupSubject,
                       }).ToList().Select(q => new SubjectList
                       {
                         SubId=QSCrypt.Encrypt(q.subjectid.ToString()),
                           SubjectId=q.subjectid,
                          Subject=q.subject,
                          Non_Subject=q.nonAcademic,                        
                          Status=q.status,
                          GroupSubjectStatus=q.groupSubjectStatus
                       }).ToList();

            return ans;
        }


        public void addSubject(string subName,bool  GroupSubStatus)
        {
            Subject sub = new Subject()
            {
                SubjectName = subName,
                GroupSubject=GroupSubStatus,
                Status=true
            };

            dc.Subjects.Add(sub);
            dc.SaveChanges();
        }

        public void updateSubject(int sub_id, string subjectName,string status,string GroupSubStatus)
        {
            var update = dc.Subjects.Where(q => q.SubjectId.Equals(sub_id)).First();
            update.SubjectName = subjectName;

             if (status == "Active")
            {
                update.Status = true;
            }
            else
            {
                update.Status = false;
            }
             if (GroupSubStatus == "Active")
             {
                 update.GroupSubject = true;
             }
             else
             {
                 update.GroupSubject = false;
             }
            dc.SaveChanges();
        }


        public List<SubjectList> getSubjectList()
        {
            var ans = (from t1 in dc.Subjects
                       where t1.Status==true
                      select new SubjectList
                       {
                           SubjectId =t1.SubjectId,
                           Subject = t1.SubjectName,
                         
                       }).ToList();
            return ans;
        }

        public Subject getSubjectName(int subId)
        {
            var row = (from a in dc.Subjects where a.SubjectId == subId select a).FirstOrDefault();
            return row;
        }
        //jegadeesh
        public IEnumerable GetParticularSubject(int subid)
        {
            var ans = dc.Subjects.Where(q => q.SubjectId.Equals(subid)).FirstOrDefault().SubjectName;
            return ans;
        }
        public IEnumerable<Object> GetJsonExamSubject(int yid, int cid, int sid)
        {
            var ans = from t1 in dc.AssignSubjectToSections
                      from t2 in dc.Subjects
                      where t1.SectionId == sid && t1.ClassId == cid && t1.AcademicYearId == yid && t1.SubjectId == t2.SubjectId
                      select new { sname = t2.SubjectName, reg_id = t1.SubjectId };
            return ans;

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}