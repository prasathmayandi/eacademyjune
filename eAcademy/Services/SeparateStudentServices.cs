﻿using eAcademy.DataModel;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class SeparateStudentServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public void addSeparateStudent(int StudentRegisterId,int AcademicYearId,int ClassId,int SectionId,DateTime DateOfIssue,string Reason,string Description,string TC,string CC)
        {
            SeparateStudent ss = new SeparateStudent()
            {
                StudentRegisterId = StudentRegisterId,
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                DateOfIssue = DateOfIssue,
                Reason = Reason,
                Description = Description,
                TC = TC,
                CC = CC
            };
            db.SeparateStudents.Add(ss);
            db.SaveChanges();
        }

        public List<OldStudents> getOldStudentList(int yearId, int classId, int secId)
        {

            var oldStudentList = (from a in db.Students
                                  from b in db.AssignClassToStudents
                                  from d in db.Classes
                                  from e in db.Sections
                                  where a.StudentRegisterId == b.StudentRegisterId &&
                                        b.ClassId == d.ClassId &&
                                        b.SectionId == e.SectionId &&
                                        a.StudentStatus == false &&
                                        b.SeparationStatus == "True" &&
                                        b.ClassId == d.ClassId &&
                                        b.SectionId == e.SectionId &&
                                        b.AcademicYearId == yearId &&
                                        b.ClassId == classId &&
                                        b.SectionId == secId 
                                  select new OldStudents
                                  {
                                      StudentRegisterId = a.StudentRegisterId,
                                      StudentId = a.StudentId,
                                      StudentName = a.FirstName + " " + a.LastName,
                                      Contact = a.Contact,
                                      Class = d.ClassType,
                                      Section = e.SectionName,
                                      Reason = b.TransferStatus,
                                      TC = b.TransferCertificate,
                                      CC = b.ConductCertificate
                                  }).ToList();

            //var oldStudentList = (from a in db.Students
            //                      from b in db.AssignClassToStudents
            //                      from c in db.SeparateStudents
            //                      from d in db.Classes
            //                      from e in db.Sections
            //                      where a.StudentRegisterId == b.StudentRegisterId &&
            //                            a.StudentRegisterId == c.StudentRegisterId &&
            //                            a.StudentStatus == false &&
            //                            c.ClassId == d.ClassId &&
            //                            c.SectionId == e.SectionId &&
            //                            c.AcademicYearId == yearId &&
            //                            c.ClassId == classId &&
            //                            c.SectionId == secId &&
            //                            b.AcademicYearId == c.AcademicYearId &&
            //                            b.ClassId == d.ClassId &&
            //                            b.SectionId == e.SectionId
            //                      select new OldStudents
            //                      {
            //                          StudentRegisterId = a.StudentRegisterId,
            //                          StudentId = a.StudentId,
            //                          StudentName = a.FirstName+" "+a.LastName,
            //                          Contact = a.Contact.Value,
            //                          Class = d.ClassType,
            //                          Section = e.SectionName,
            //                          Reason = c.Reason,
            //                          TC = c.TC,
            //                          CC = c.CC
            //                      }).ToList();
            return oldStudentList;

        }

        public SeparateStudent getTC(string fid)
        {
            var getTC = (from a in db.SeparateStudents where a.TC == fid select a).FirstOrDefault();
            return getTC;
        }

        public SeparateStudent getCC(string fid)
        {
            var getCC = (from a in db.SeparateStudents where a.CC == fid select a).FirstOrDefault();
            return getCC;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}