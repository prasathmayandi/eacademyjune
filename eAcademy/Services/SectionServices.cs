﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class SectionServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
       
        public IEnumerable<Object> getSection(int passClassId)
        {
            var List = dc.Sections.Where(d => d.ClassId == passClassId).Select(e => new { selSectionId = e.SectionId, selSectionName = e.SectionName }).ToList();
            return List;
        }

        public IEnumerable<Object> getSectionWithoutSelectedSection(int passClassId, int secId)
        {
            var List = dc.Sections.Where(d => d.ClassId == passClassId && d.SectionId != secId).Select(e => new { selSectionId = e.SectionId, selSectionName = e.SectionName }).ToList();
            return List;
        }

        public List<Section> getSections(int passClassId)
        {
            var List = dc.Sections.Where(d => d.ClassId == passClassId).ToList();
            return List;
        }

        public IEnumerable<Object> ShowSections(int classId)
        {
            var TermList = (from a in dc.Sections
                            select new
                            {
                                SecName = a.SectionName,
                                SecId = a.SectionId
                            }).ToList().AsEnumerable();
            return TermList;
        }

        public Section getSectionName(int classId, int secId)
        {
            var getSectionName = (from a in dc.Sections where a.ClassId == classId && a.SectionId == secId select a).FirstOrDefault();
            return getSectionName;
        }

        public IEnumerable<Object> ShowSection(int classId)
        {
            var SectionList = (from a in dc.Sections
                               where a.ClassId == classId
                               select new
                               {
                                   SectionName = a.SectionName,
                                   SectionId = a.SectionId
                               }).ToList().AsEnumerable();
            return SectionList;
        }

        public Section getSectionName(int? secId)
        {
            var getSecName = (from a in dc.Sections where a.SectionId == secId select a).FirstOrDefault();
            return getSecName;
        }

        public IEnumerable<Object> getSection(int passClassId, int FacultyId)
        {
            var SecList = (from a in dc.Sections
                           from b in dc.AssignFacultyToSubjects
                           where a.SectionId == b.SectionId &&
                           b.ClassId == passClassId &&
                           b.EmployeeRegisterId == FacultyId
                           select new
                           {
                               SecName = a.SectionName,
                               SecId = a.SectionId
                           }).ToList();
            return SecList;
        }

        public List<ClassSection> getSectionByClassId(int cid)
        {
            var ans1 = (from t1 in dc.Classes
                        from t2 in dc.Sections
                        
                        where t1.ClassId == t2.ClassId && t2.ClassId == cid 
                        select new ClassSection { ClassId = t1.ClassId, ClassType = t1.ClassType, Section = t2.SectionName, SectionId = t2.SectionId }).ToList();
            return ans1;
        }

        public List<ClassSection> getSectionByClassId(int acid, int cid)
        {
            var ans1 = (from a in dc.Sections
                           where a.ClassId== cid && !(from b in dc.SectionStrengths
                                   where b.AcademicYearId == acid &&
                                   b.ClassId == a.ClassId &&
                                  b.SectionId == a.SectionId &&
                                   b.ClassId == cid
                                   select b.SectionId)
                                     .Contains(a.SectionId)
                            select new ClassSection {  Section = a.SectionName, SectionId = a.SectionId }).ToList();
            return ans1;
        }

        public List<ClassSection> getSectionwithStrength(int acid,int cid)
        {
            var ans1 = (from t1 in dc.Classes
                        from t2 in dc.Sections
                        from t3 in dc.SectionStrengths 
                        from t4 in dc.AcademicYears
                        where t1.ClassId == t2.ClassId && t2.ClassId == cid && t2.SectionId==t3.SectionId && t3.AcademicYearId==t4.AcademicYearId && t3.AcademicYearId==acid
                        select new ClassSection { ClassId = t1.ClassId, ClassType = t1.ClassType, Section = t2.SectionName, SectionId = t2.SectionId, Strength = t3.Strength, ClassRoomId = t3.ClassRoomId, SectionStrengthId =t3.SectionStrengthId}).ToList();
            return ans1;
        }

        public Section getSectionBySectionId(int sec_id)
        {
            var ans = dc.Sections.Where(query => query.SectionId.Equals(sec_id)).First();
            return ans;
        }

        public AssignClassToStudent sectioncheckusing(int sec_id)
        {
            var ans = (from a in dc.AssignClassToStudents where a.SectionId == sec_id select a).FirstOrDefault();
            //var ans = dc.AssignClassToStudents.Where(query => query.SectionId.Equals(sec_id)).FirstOrDefault();
            return ans;
        }
        public void deletesection(int sec_id)
        {
            var ans = (from a in dc.Sections where a.SectionId == sec_id select a).FirstOrDefault();
            dc.Sections.Remove(ans);
            dc.SaveChanges();
            
        }
        public void addSection(string sectionName, int cid)
        {
            Section sec = new Section()
            {
                ClassId = cid,
                SectionName = sectionName
            };
            dc.Sections.Add(sec);
            dc.SaveChanges();
        }


        public List<Section> getSectionByCid(int cid)
        {
            var ans = dc.Sections.Where(query => query.ClassId.Value.Equals(cid)).ToList();
            return ans;
        }

        public bool checkSection(string secname, int cid)
        {
            
            var ans = dc.Sections.Where(q => q.ClassId == cid && q.SectionName.Equals(secname)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkSection(string secname, int cid,int sec_id)
        {
            var ans = dc.Sections.Where(q => q.ClassId == cid && q.SectionName.Equals(secname) && q.SectionId!=sec_id).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkSection(int cid)
        {
            var ans = dc.Sections.Where(q => q.ClassId.Value.Equals(cid)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void updateSection(int cid,int sec_id, string sectionName)
        {
            var update = dc.Sections.Where(q => q.ClassId.Value.Equals(cid) && q.SectionId.Equals(sec_id)).First();
            update.SectionName = sectionName;
            dc.SaveChanges();
        }
        //jegadeesh

        public IEnumerable GetSection(int clasid)
        {
            var ans = (from b in dc.Sections where b.ClassId == clasid select b).ToList();
            return ans;
        }
        public IEnumerable GetParticularSection(int sectionid)
        {
            var ans = dc.Sections.Where(q => q.SectionId.Equals(sectionid)).FirstOrDefault().SectionName;
            return ans;
        }
        public IEnumerable GetJsonsection(int classid)
        {
            var ans = dc.Sections.Where(q => q.ClassId.Value.Equals(classid)).Select(s => new { sec_id = s.SectionId, sec = s.SectionName }).ToList();
            return ans;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}