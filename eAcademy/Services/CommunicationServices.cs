﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class CommunicationServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public List<CommunicationList> getCommunicationdetails(DateTime? fdate,DateTime? tdate)
        {
            tdate = tdate.Value.AddDays(1);
            var result = ((
                from a in dc.ClassCommunications
                from b in dc.Classes
                where a.MarkedDate >= fdate && a.MarkedDate <= tdate && b.ClassId == a.ClassId
                select new 
                {
                    id = a.Id,
                    Title = a.Title,
                    Description = a.Description,
                    Date = SqlFunctions.DateName("day", a.MarkedDate).Trim() + " " + SqlFunctions.DateName("month", a.MarkedDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.MarkedDate),                    
                    Filename = a.FileName,
                    Usertype=b.ClassType,
                    Pdate=a.MarkedDate,
                    To = "Class"
                }).Distinct().OrderBy(q=>q.Pdate).ToList().Select(a => new CommunicationList
                {
                    id = a.id,
                    Title = getClassCommnFile(a.id)+" "+a.Title,
                    Description = a.Description,
                    Date = a.Date,
                    Pdate=a.Pdate,
                    Filename = getClassCommnFile(a.id),
                    Usertype=a.Usertype,
                    Titles=a.Title,
                    To = "Class"
                }).Distinct().ToList()).Union((
                from a in dc.StudentCommunications
                from b in dc.Students
                where a.MarkedDate >= fdate && a.MarkedDate <= tdate && b.StudentRegisterId == a.StudentRegisterId
                select new 
                {
                    id = a.Id,
                    Title = a.Title,
                    Description = a.Description,
                    Pdate = a.MarkedDate,
                    Date = SqlFunctions.DateName("day", a.MarkedDate).Trim() + " " + SqlFunctions.DateName("month", a.MarkedDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.MarkedDate),
                    Filename = a.FileName,
                    Usertype=b.FirstName +" "+b.LastName,
                    To = "Single Student"
                }).Distinct().OrderBy(q => q.Pdate).ToList().Select(a => new CommunicationList
                {
                    id = a.id,
                    Title =getStuCommnFile(a.id)+" "+ a.Title,
                    Description = a.Description,
                    Date = a.Date,
                    Pdate = a.Pdate,
                    Filename = getStuCommnFile(a.id),
                    Usertype = a.Usertype,
                    Titles = a.Title,
                    To = "Single Student"
                }).Distinct().ToList()).Union((
                from a in dc.StaffCategoryCommunications
                from b in dc.EmployeeTypes
                where a.MarkedDate >= fdate && a.MarkedDate <= tdate && b.EmployeeTypeId == a.EmployeeTypeId
                select new 
                {
                    id = a.Id,
                    Title = a.Title,
                    Pdate = a.MarkedDate,
                    Description = a.Description,
                    Date = SqlFunctions.DateName("day", a.MarkedDate).Trim() + " " + SqlFunctions.DateName("month", a.MarkedDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.MarkedDate),
                    Filename = a.FileName,
                    Usertype=b.EmployeeTypes,
                    To = "All Staff"
                }).Distinct().OrderBy(q => q.Pdate).ToList().Select(a => new CommunicationList
                {
                    id = a.id,
                    Title = getStaffCategoryCommnFile(a.id)+" "+a.Title,
                    Pdate = a.Pdate,
                    Description = a.Description,
                    Date = a.Date,
                    Filename = getStaffCategoryCommnFile(a.id),
                    Usertype = a.Usertype,
                    Titles = a.Title,
                    To = "All Staff"
                }).Distinct().ToList()).Union((
                from a in dc.SingleStaffCommunications
                from b in dc.TechEmployees
                where a.MarkedDate >= fdate && a.MarkedDate <= tdate && b.EmployeeRegisterId == a.EmployeeRegisterId
                select new 
                {
                    id = a.Id,
                    Title = a.Title,
                    Description = a.Description,
                    Pdate = a.MarkedDate,
                    Date = SqlFunctions.DateName("day", a.MarkedDate).Trim() + " " + SqlFunctions.DateName("month", a.MarkedDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.MarkedDate),
                    Filename = a.FileName,
                    Usertype=b.EmployeeName +" "+b.LastName,
                    To = "Single Staff"
                }).Distinct().OrderBy(q => q.Pdate).ToList().Select(a => new CommunicationList
                {
                    id = a.id,
                     Title = getSingleStaffCommnCommnFile(a.id)+" "+a.Title,
                    Pdate = a.Pdate,
                    Description = a.Description,
                    Date = a.Date,
                     Filename = getSingleStaffCommnCommnFile(a.id),
                     Usertype = a.Usertype,
                    Titles = a.Title,
                    To = "Single Staff"
                }).Distinct().ToList()).OrderByDescending(q => q.Pdate).ToList();            
            return result;
        }


        public string getClassCommnFile(int Id)
        {
            string hw;
            var File = (from a in dc.ClassCommunications where a.Id == Id select a).FirstOrDefault().FileName;
            if (File != null)
            {
                var HW = (from a in dc.ClassCommunications where a.Id == Id select a).FirstOrDefault().FileName;
                string HomeWork_Id = QSCrypt.Encrypt(Id.ToString());
                //HW = Path.GetFileNameWithoutExtension(HW);
                  string[] words = File.Split('.');
                  if (words[1] == "pdf")
                  {
                      //hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=class><i class='fa fa-file-pdf-o'></i>" + HW + "</a>";
                      hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=class><i class='fa fa-file-pdf-o'></i></a>";
                  }
                  else {
                      hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=class><i class='fa fa-file-word-o'></i></a>";
                      //hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=class><i class='fa fa-file-word-o'></i>" + HW + "</a>";
                  }
            }
            else
            {
                var HW = (from a in dc.ClassCommunications where a.Id == Id select a).FirstOrDefault().FileName;
                hw = HW;
            }
            return hw;
        }

        public string getStuCommnFile(int Id)
        {
            string hw;
            var File = (from a in dc.StudentCommunications where a.Id == Id select a).FirstOrDefault().FileName;
            if (File != null)
            {
                var HW = (from a in dc.StudentCommunications where a.Id == Id select a).FirstOrDefault().FileName;
                string HomeWork_Id = QSCrypt.Encrypt(Id.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf") {
                    //hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=student><i class='fa fa-file-pdf-o'></i>" + HW + "</a>"; //QSCrypt.Encrypt(Id.ToString())
                    hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=student><i class='fa fa-file-pdf-o'></i></a>"; //QSCrypt.Encrypt(Id.ToString())
                }
                else{
                    hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=student><i class='fa fa-file-word-o'></i></a>"; //QSCrypt.Encrypt(Id.ToString())
                    //hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=student><i class='fa fa-file-word-o'></i>" + HW + "</a>"; //QSCrypt.Encrypt(Id.ToString())
                }
            }
            else
            {
                var HW = (from a in dc.StudentCommunications where a.Id == Id select a).FirstOrDefault().FileName;
                hw = HW;
            }
            return hw;
        }

        public string getStaffCategoryCommnFile(int Id)
        {
            string hw;
            var File = (from a in dc.StaffCategoryCommunications where a.Id == Id select a).FirstOrDefault().FileName;
            if (File != null)
            {
                var HW = (from a in dc.StaffCategoryCommunications where a.Id == Id select a).FirstOrDefault().FileName;
                string HomeWork_Id = QSCrypt.Encrypt(Id.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    //hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=allstaff><i class='fa fa-file-pdf-o'></i>" + HW + "</a>";
                    hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=allstaff><i class='fa fa-file-pdf-o'></i></a>";
                }
                else { 
                    hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=allstaff><i class='fa fa-file-word-o'></i></a>";
                    //hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=allstaff><i class='fa fa-file-word-o'></i>" + HW + "</a>";
                }                
            }
            else
            {
                var HW = (from a in dc.StaffCategoryCommunications where a.Id == Id select a).FirstOrDefault().FileName;
                hw = HW;
            }
            return hw;
        }

        public string getSingleStaffCommnCommnFile(int Id)
        {
            string hw;
            var File = (from a in dc.SingleStaffCommunications where a.Id == Id select a).FirstOrDefault().FileName;
            if (File != null)
            {
                var HW = (from a in dc.SingleStaffCommunications where a.Id == Id select a).FirstOrDefault().FileName;
                string HomeWork_Id = QSCrypt.Encrypt(Id.ToString());
                 string[] words = File.Split('.');
                 if (words[1] == "pdf")
                 {
                     //hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=singlestaff><i class='fa fa-file-pdf-o'></i>" + HW + "</a>";
                     hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=singlestaff><i class='fa fa-file-pdf-o'></i></a>";
                 }
                 else{
                     hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=singlestaff><i class='fa fa-file-word-o'></i></a>";
                     //hw = "<a href=/AcademicReport/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=singlestaff><i class='fa fa-file-word-o'></i>" + HW + "</a>";
                 }
            }
            else
            {
                var HW = (from a in dc.SingleStaffCommunications where a.Id == Id select a).FirstOrDefault().FileName;
                hw = HW;
            }
            return hw;
        }

        public List<CommunicationList> getDashboardCommunication(DateTime? fdate, int yearId, int classId, int secId)
        {
            DateTime tdate = fdate.Value.AddDays(1);
            var result = ((
                from a in dc.ClassCommunications
                where a.MarkedDate >= fdate && a.MarkedDate <= tdate
                select new CommunicationList
                {
                    Title = a.Title,
                    Description = a.Description,
                    Date = SqlFunctions.DateName("year", a.MarkedDate) + "/" + SqlFunctions.StringConvert((double)a.MarkedDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("day", a.MarkedDate).Trim(),
                    Filename = a.FileName,
                    To = "Class"
                }
                ).Distinct().ToList()).Union((
                from a in dc.StudentCommunications
                where a.MarkedDate >= fdate && a.MarkedDate <= tdate && a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId
                select new CommunicationList
                {
                    Title = a.Title,
                    Description = a.Description,
                    Date = SqlFunctions.DateName("year", a.MarkedDate) + "/" + SqlFunctions.StringConvert((double)a.MarkedDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("day", a.MarkedDate).Trim(),
                    Filename = a.FileName,
                    To = "SingleStudent"
                }
                ).Distinct().ToList()).ToList();
            return result;
        }



        public ClassCommunication GetClassFile(int id)
        {
            var record = dc.ClassCommunications.Where(q => q.Id == id).FirstOrDefault();
            return record;
        }
        public StudentCommunication GetStudentFile(int id)
        {
            var record = dc.StudentCommunications.Where(q => q.Id == id).FirstOrDefault();
            return record;
        }
        public StaffCategoryCommunication getStaffCategory(int id)
        {
            var record = dc.StaffCategoryCommunications.Where(q => q.Id == id).FirstOrDefault();
            return record;
        }
        public SingleStaffCommunication getsingleStaff(int id)
        {
            var record = dc.SingleStaffCommunications.Where(q => q.Id == id).FirstOrDefault();
            return record;
        }
        public List<CommunicationList> GetParentCommunicationDetaions(int yid, int? cid, int StudentRegId)
        {
            var list = ((
                  from a in dc.ClassCommunications
                  where a.AcademicYearId == yid && a.ClassId == cid && a.Status == true
                  select new
                  {
                      id = a.Id,
                      Title = a.Title,
                      Description = a.Description,
                      EventDate = SqlFunctions.DateName("day", a.DateOfAnnouncement).Trim() + " " + SqlFunctions.DateName("month", a.DateOfAnnouncement).Remove(3) + ", " + SqlFunctions.DateName("year", a.DateOfAnnouncement),
                      PostedDate = SqlFunctions.DateName("day", a.MarkedDate).Trim() + " " + SqlFunctions.DateName("month", a.MarkedDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.MarkedDate),
                      Pdate = a.MarkedDate,
                      Filename = a.FileName,
                      To = "Class"
                  }).Distinct().OrderBy(q => q.Pdate).ToList()
                  .Select(a => new CommunicationList
                  {
                      id = a.id,
                      Cid = CheckFile(a.Filename, a.id),
                      Title = getClassCommunicationnWithFile(a.id),
                      Description = a.Description,
                      EventDate = a.EventDate,
                      PostedDate = a.PostedDate,
                      Filename = a.Filename,
                      Pdate = a.Pdate,
                      To = a.To
                  }).ToList())

                  .Union((
                  from a in dc.StudentCommunications
                  where a.AcademicYearId == yid && a.ClassId == cid && a.Status == true && a.StudentRegisterId == StudentRegId
                  select new CommunicationList
                  {
                      id = a.Id,
                      Title = a.Title,
                      Description = a.Description,
                      EventDate = SqlFunctions.DateName("day", a.DateOfAnnouncement).Trim() + " " + SqlFunctions.DateName("month", a.DateOfAnnouncement).Remove(3) + ", " + SqlFunctions.DateName("year", a.DateOfAnnouncement),
                      PostedDate = SqlFunctions.DateName("day", a.MarkedDate).Trim() + " " + SqlFunctions.DateName("month", a.MarkedDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.MarkedDate),
                      Pdate = a.MarkedDate,
                      Filename = a.FileName,
                      To = "SingleStudent"
                  }).Distinct().OrderBy(q => q.PostedDate).ToList()
                  .Select(a => new CommunicationList
                  {
                      id = a.id,
                      //Cid = QSCrypt.Encrypt(a.id.ToString()),
                      Cid = CheckFile(a.Filename, a.id),
                      Title = getStuCommunicationnWithFile(a.id),
                      Description = a.Description,
                      EventDate = a.EventDate,
                      PostedDate = a.PostedDate,
                      Pdate = a.Pdate,
                      Filename = a.Filename,
                      To = a.To
                  }).ToList()).OrderByDescending(q => q.Pdate).ToList();
            return list;
        }

        public List<CommunicationList> GetParentCommunicationDetaions(int yid, int? cid, DateTime fdate, DateTime tdate, int StudentRegId)
        {
            var list = ((
                  from a in dc.ClassCommunications
                  where a.AcademicYearId == yid && a.ClassId == cid && a.Status == true && 
                  a.DateOfAnnouncement >= fdate && a.DateOfAnnouncement <= tdate
                  select new
                  {
                      id = a.Id,
                      Title = a.Title,
                      Description = a.Description,
                      EventDate = SqlFunctions.DateName("day", a.DateOfAnnouncement).Trim() + " " + SqlFunctions.DateName("month", a.DateOfAnnouncement).Remove(3) + ", " + SqlFunctions.DateName("year", a.DateOfAnnouncement),
                      PostedDate = SqlFunctions.DateName("day", a.MarkedDate).Trim() + " " + SqlFunctions.DateName("month", a.MarkedDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.MarkedDate),
                      Pdate = a.MarkedDate,
                      Filename = a.FileName,
                      To = "Class"
                  }).Distinct().OrderBy(q => q.Pdate).ToList()
                  .Select(a => new CommunicationList
                  {
                      id = a.id,
                      Cid = CheckFile(a.Filename, a.id),
                      Title = getClassCommunicationnWithFile(a.id),
                      Description = a.Description,
                      EventDate = a.EventDate,
                      PostedDate = a.PostedDate,
                      Filename = a.Filename,
                      Pdate = a.Pdate,
                      To = a.To
                  }).ToList())

                  .Union((
                  from a in dc.StudentCommunications
                  where a.AcademicYearId == yid && a.ClassId == cid && a.Status == true && a.StudentRegisterId == StudentRegId &&
                  a.DateOfAnnouncement >= fdate && a.DateOfAnnouncement <= tdate
                  select new CommunicationList
                  {
                      id = a.Id,
                      Title = a.Title,
                      Description = a.Description,
                      EventDate = SqlFunctions.DateName("day", a.DateOfAnnouncement).Trim() + " " + SqlFunctions.DateName("month", a.DateOfAnnouncement).Remove(3) + ", " + SqlFunctions.DateName("year", a.DateOfAnnouncement),
                      PostedDate = SqlFunctions.DateName("day", a.MarkedDate).Trim() + " " + SqlFunctions.DateName("month", a.MarkedDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.MarkedDate),
                      Pdate = a.MarkedDate,
                      Filename = a.FileName,
                      To = "SingleStudent"
                  }).Distinct().OrderBy(q => q.PostedDate).ToList()
                  .Select(a => new CommunicationList
                  {
                      id = a.id,
                      //Cid = QSCrypt.Encrypt(a.id.ToString()),
                      Cid = CheckFile(a.Filename, a.id),
                      Title = getStuCommunicationnWithFile(a.id),
                      Description = a.Description,
                      EventDate = a.EventDate,
                      PostedDate = a.PostedDate,
                      Pdate = a.Pdate,
                      Filename = a.Filename,
                      To = a.To
                  }).ToList()).OrderByDescending(q => q.Pdate).ToList();
            return list;
        }

        public string CheckFile(string filename, int id)
        {
            if (filename != null)
            {
                return QSCrypt.Encrypt(id.ToString());
            }
            else
            {
                return "null";
            }
        }

        public string getStuCommunicationnWithFile(int id)
        {
            string hw;
            var File = (from a in dc.StudentCommunications where a.Id == id select a).FirstOrDefault().FileName;
            if (File != null)
            {
                var HW = (from a in dc.StudentCommunications where a.Id == id select a).FirstOrDefault().Title;
                string HomeWork_Id = QSCrypt.Encrypt(id.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    hw ="<a href=/Parent/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=student><i class='fa fa-file-pdf-o'></i></a>"+HW;
                }
                else {
                    hw ="<a href=/Parent/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=student><i class='fa fa-file-word-o'></i></a>"+HW;
                }
            }
            else
            {
                var HW = (from a in dc.StudentCommunications where a.Id == id select a).FirstOrDefault().Title;
                hw = HW;
            }
            return hw;
        }

        public string getClassCommunicationnWithFile(int id)
        {
            string hw;
            var File = (from a in dc.ClassCommunications where a.Id == id select a).FirstOrDefault().FileName;
            if (File != null)
            {
                var HW = (from a in dc.ClassCommunications where a.Id == id select a).FirstOrDefault().Title;
                string HomeWork_Id = QSCrypt.Encrypt(id.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    hw ="<a  href=/Parent/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=class><i class='fa fa-file-pdf-o'></i></a>"+HW;
                }
                else {
                    hw ="<a  href=/Parent/OpenCommunicationFile/?id=" + HomeWork_Id + "&name=class><i class='fa fa-file-word-o'></i></a>"+HW;
                }
            }
            else
            {
                var HW = (from a in dc.ClassCommunications where a.Id == id select a).FirstOrDefault().Title;
                hw = HW;
            }
            return hw;
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
      
    }
}