﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class DefaultResourceServices :IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public long AddDefaultResource(ResourceAdd m)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            DefaultResource add = new DefaultResource
            {
                Name = m.Resource_Name,
                DateCreated = date,
                DateUpdated = date,
                Deleted = false,
                ResourceTypeId = m.TypeId
            };
            dc.DefaultResources.Add(add);
            dc.SaveChanges();

            return add.Id;
        }

        public DefaultResource CheckResourceExist(string Resource_Name)
        {
            var checkResourceExist = (from a in dc.DefaultResources where a.Name == Resource_Name select a).FirstOrDefault();
            return checkResourceExist;
        }

        public void UpdateDefaultResource(ResourceAdd m)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var row = (from a in dc.DefaultResources
                       from b in dc.ResourceValues
                       where b.Id == m.Id && a.Id == b.ResourceId
                       select a).FirstOrDefault();
            if (row != null)
            {
                row.Name = m.Resource_Name;
                row.DateUpdated = date;
                dc.SaveChanges();
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

      

    }
}