﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class AssignSubjectToSectionServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public IEnumerable<Object> ShowSubjects(int yearId, int ClassId, int secId)
        {
            var SubjectList = (from a in db.AssignSubjectToSections
                               from b in db.Subjects
                               where a.AcademicYearId == yearId &&
                               a.ClassId == ClassId &&
                               a.SectionId == secId &&
                               a.SubjectId == b.SubjectId &&
                               a.Status == true
                               select new
                               {
                                   SubjectName = b.SubjectName,
                                   SubjectId = b.SubjectId
                               }).ToList().AsEnumerable();

            return SubjectList;
        }

        //public IEnumerable<Object> ShowSubjectsWithFacultyName(int yearId, int ClassId, int secId)
        //{
        //    var SubjectList = (from a in db.AssignSubjectToSections
        //                       from b in db.Subjects
        //                       from c in db.AssignFacultyToSubjects
        //                       from d in db.TechEmployees
        //                       where a.AcademicYearId == yearId &&
        //                       a.ClassId == ClassId &&
        //                       a.SectionId == secId &&
        //                       a.SubjectId == b.SubjectId &&
        //                       a.Status == true &&
        //                       c.AcademicYearId == yearId &&
        //                       c.ClassId == ClassId && 
        //                       c.SectionId == secId &&
        //                       a.AcademicYearId == c.AcademicYearId &&
        //                       a.ClassId == c.ClassId &&
        //                       a.SectionId == c.SectionId &&
        //                       a.SubjectId == c.SubjectId &&
        //                       b.SubjectId == c.SubjectId &&
        //                       c.EmployeeRegisterId == d.EmployeeRegisterId
        //                       select new
        //                       {
        //                           SubjectName = b.SubjectName,
        //                           SubjectId = b.SubjectId,
        //                           FacultyName = d.EmployeeName
        //                       }).ToList().AsEnumerable();

        //    return SubjectList;
        //}
        public AssignSubjectToSection checkSubjectAssigned(int AcademicYearId, int ClassId, int SectionId)
        {
            var getRow = (from a in db.AssignSubjectToSections where a.AcademicYearId == AcademicYearId && a.ClassId == ClassId && a.SectionId == SectionId select a).FirstOrDefault();
            return getRow;
        }

        public int? GetGroupSubject(int SubjectId)
        {
            var getRow = (from a in db.GroupSubjectConfigs where a.SubjectId == SubjectId && a.status == true select a.GroupSubjectId).FirstOrDefault();
            return getRow;
        }



        public IEnumerable<Object> getSubjectList(int yearId, int classId, int secId)
        {
            var subjectList = (from a in db.AssignSubjectToSections
                               from b in db.Subjects
                               where a.SubjectId == b.SubjectId && a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.Status == true
                               select new { sub = a.SubjectId, Subject_Id = a.SubjectId, Subject = b.SubjectName }).ToList();
            return subjectList;
        }

        public void addSubjectToSection(int AcademicYearId, int ClassId, int SectionId, int SubjectId, int? GroSubjectId)
        {
            AssignSubjectToSection add = new AssignSubjectToSection()
            {
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                SubjectId = SubjectId,
                GroupSubjectId = GroSubjectId,
                Status = true
            };
            db.AssignSubjectToSections.Add(add);
            db.SaveChanges();
        }

        public List<ListSubjectToSection>getAllSubjectToSection(int yearId, int classId, int secId)
        {
            var list = (from a in db.AssignSubjectToSections
                        from b in db.Subjects
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.Status == true &&
                        a.SubjectId == b.SubjectId
                        select new ListSubjectToSection
                        {
                            Subject = b.SubjectName,
                            AssignSubjectToSectionId = a.AssignSubjectToSectionId
                        }).ToList();

            return list;
        }

        public List<GradeProgressCard1>GetSectionSub(int yearId, int classId, int secId)
        {
            var list = (from a in db.AssignSubjectToSections
                        from b in db.Subjects
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.Status == true &&
                        a.SubjectId == b.SubjectId
                        select new GradeProgressCard1
                        {
                            Subject = b.SubjectName
                        }).ToList();

            return list;
        }

        public void updateSectionSubject(int id)
        {
            var getRecord = (from a in db.AssignSubjectToSections where a.AssignSubjectToSectionId == id select a).First();
            getRecord.Status = false;
            db.SaveChanges();
        }
        //public int (int AcademicYearId, int ClassId, int SectionId, int SubjectId)
        //{
        //    var getRow = (from a in db.AssignSubjectToSections where a.AcademicYearId == AcademicYearId && a.ClassId == ClassId && a.SectionId == SectionId && a.SubjectId == SubjectId select a).FirstOrDefault();
        //    return getRow;
        //int? GroupSubjectId
        //}

        public AssignSubjectToSection checkSubjectExist(int AcademicYearId, int ClassId, int SectionId, int SubjectId)
        {
            var getRow = (from a in db.AssignSubjectToSections where a.AcademicYearId == AcademicYearId && a.ClassId == ClassId && a.SectionId == SectionId && a.SubjectId == SubjectId select a).FirstOrDefault();
            return getRow;
        }
        public AssignSubjectToSection checkSubjectExist1(int AcademicYearId, int ClassId, int SectionId,  int? GSubjectId)
        {
            var getRow = (from a in db.AssignSubjectToSections where a.AcademicYearId == AcademicYearId && a.ClassId == ClassId && a.SectionId == SectionId && a.GroupSubjectId == GSubjectId select a).FirstOrDefault();
            return getRow;
        }
        public void updateSectionSubjectStatus(int yearId, int classId, int secId, int subId, Boolean status)
        {
            var getRecord = (from a in db.AssignSubjectToSections where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.SubjectId == subId select a).First();
            if (status == true)
            {
                getRecord.Status = true;
            }
            else
            {
                getRecord.Status = false;
            }
            db.SaveChanges();
        }

        public IEnumerable<Object> getClassList(int yearId, int ClassId)
        {
            var SubjectList = (from a in db.AssignSubjectToSections
                               from b in db.Subjects
                               where a.AcademicYearId == yearId &&
                               a.ClassId == ClassId &&
                               a.Status == true &&
                               a.SubjectId == b.SubjectId
                               select new
                               {
                                   SubjectName = b.SubjectName,
                                   SubjectId = b.SubjectId
                               }).Distinct().ToList().AsEnumerable();

            return SubjectList;
        }

        public List<ExistExamTimetableList> getClassSubList(int yearId, int ClassId, int examId)
        {
            var SubjectList = (from a in db.AssignSubjectToSections
                               from b in db.Subjects
                               where a.AcademicYearId == yearId &&
                               a.ClassId == ClassId &&
                               a.Status == true &&
                               a.SubjectId == b.SubjectId && !(from c in db.ExamTimetables where c.AcademicYearId == yearId && c.ClassId == ClassId && c.ExamId == examId select c.SubjectId).Contains(a.SubjectId)
                               select new ExistExamTimetableList
                               {
                                   SubjectName = b.SubjectName,
                                   SubjectId = b.SubjectId
                               }).Distinct().ToList();

            return SubjectList;
        }


        public List<getclassandsection> SearchClassSubject(int yid, int cid, int sid)
        {
            var ans = (from a in db.AssignSubjectToSections
                       from b in db.Classes
                       from c in db.Sections
                       from d in db.Subjects

                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && b.Status.Value.Equals(true) && b.ClassId == cid && c.ClassId == cid && c.SectionId == sid && a.SubjectId == d.SubjectId
                       select new getclassandsection
                       {
                           ClassName = b.ClassType,
                           SectionName = c.SectionName,
                           subjectName = d.SubjectName,
                           subjectId = a.SubjectId,
                           Id = a.AssignSubjectToSectionId
                       }
                          ).Distinct().OrderBy(x => x.Id).ToList();
            return ans;
        }
        public IEnumerable GetSubject(int yid, int cid, int sid)
        {
            var ans = (from a in db.AssignSubjectToSections
                       from b in db.Subjects
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.SubjectId == b.SubjectId
                       select b).ToList();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}