﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class FeeCategoryServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public FeeCategoryList getFeeCategoryByFeeCategoryID(int fid)
        {
            var ans = (from t1 in dc.FeeCategories
                       where t1.FeeCategoryId == fid
                       select new FeeCategoryList { FeeCatId = t1.FeeCategoryId, FeeType = t1.FeeCategoryName, Status = t1.Status }).FirstOrDefault();
            return ans;
        }

        public bool checkFeeCategory(string ftype)
        {
            var ans = dc.FeeCategories.Where(q => q.FeeCategoryName.Equals(ftype)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkFeeCategory(string ftype, string status,int fid)
        {
           
            if (status == "Active")
            {

                var ans = dc.FeeCategories.Where(q => q.FeeCategoryId != fid && q.FeeCategoryName == ftype).ToList();
                if(ans.Count>0)
                {
                    return false;
                }
                else
                {
                    var getrow = dc.FeeCategories.Where(q => q.FeeCategoryId == fid).FirstOrDefault();
                    if (getrow != null)
                    {
                        getrow.FeeCategoryName = ftype;
                        getrow.Status = true;
                        dc.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            else
            {
                var list = (from a in dc.Fees
                            where a.FeeCategoryId == fid
                            select a).ToList();
                if (list.Count == 0)
                {
                    var get = dc.FeeCategories.Where(q => q.FeeCategoryId != fid && q.FeeCategoryName == ftype).ToList();
                    if (get.Count > 0)
                    {
                        return false;
                    }
                    else
                    {
                        var ans = dc.FeeCategories.Where(q => q.FeeCategoryId == fid).FirstOrDefault();
                        if (ans != null)
                        {
                            ans.FeeCategoryName = ftype;
                            ans.Status = false;
                            dc.SaveChanges();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
        }
         
        public List<FeeCategoryList> getFeeCategory()
        {
            var ans =(from t1 in  dc.FeeCategories where t1.Status == true

                      select new FeeCategoryList { FeeCatId = t1.FeeCategoryId, FeeType = t1.FeeCategoryName, Status = t1.Status, FeeCategory =t1.FeeCategoryName}).ToList();
            return ans;
        }

        public void addFeeCategory(string feetype)
        {
            FeeCategory add = new FeeCategory()
            {
                FeeCategoryName = feetype,
                Status = true
            };

            dc.FeeCategories.Add(add);
            dc.SaveChanges();
        }

        public void updateFeeCategory(int fid, string feetype, string status)
        {

            var update = dc.FeeCategories.Where(query => query.FeeCategoryId.Equals(fid)).First();
            update.FeeCategoryName = feetype;
            if (status == "Active")
            {
                update.Status = true;
            }
            else
            {
                update.Status = false;
            }
            dc.SaveChanges();
        }

        public void deleteFeeCategory(int fid)
        {

            var delete = dc.FeeCategories.Where(query => query.FeeCategoryId.Equals(fid)).First();
            dc.FeeCategories.Remove(delete);
            dc.SaveChanges();
        }
        //jegadeesh
        public IEnumerable GetAnnualFeeCategory(int yearid, int classid, int payid)
        {
            var ans = (from a in dc.Fees
                       from b in dc.FeeCategories
                       where a.AcademicYearId == yearid && a.ClassId == classid && a.FeeCategoryId == b.FeeCategoryId && a.PaymentTypeId == payid
                       select b).Distinct().ToList();
            return ans;
        }
        public IEnumerable GetTermFeeCategory(int yearid, int classid, int payid)
        {
            var ans = (from a in dc.TermFees
                       from b in dc.FeeCategories
                       where a.AcademicYearId == yearid && a.ClassId == classid && a.FeeCategoryId == b.FeeCategoryId && a.PaymentTypeId == payid
                       select b).Distinct().ToList();
            return ans;
        }
        public IEnumerable GetParticularFee(int feeid)
        {
            var ans = dc.FeeCategories.Where(q => q.FeeCategoryId.Equals(feeid)).FirstOrDefault().FeeCategoryName;
            return ans;
        }
        public IEnumerable GetJsonFee(int yid, int cid, int pid)
        {
            var ans = (from a in dc.Fees
                       from b in dc.FeeCategories
                       from c in dc.TermFees
                       where a.AcademicYearId == yid && a.ClassId == cid && c.AcademicYearId == a.AcademicYearId && c.ClassId == a.ClassId && a.FeeId != c.FeeId && a.FeeCategoryId == b.FeeCategoryId && a.PaymentTypeId == pid
                       select new { fid = b.FeeCategoryId, fee = b.FeeCategoryName }).Distinct().ToList();
            return ans;
        }
        public IEnumerable GetJsonTermFee(int yid, int cid, int pid)
        {
            var ans = (from a in dc.TermFees
                       from b in dc.FeeCategories
                       where a.AcademicYearId == yid && a.ClassId == cid && a.FeeCategoryId == b.FeeCategoryId && a.PaymentTypeId == pid
                       select new { fid = b.FeeCategoryId, fee = b.FeeCategoryName }).Distinct().ToList();
            return ans;
        }
        public IEnumerable<Object> GetJsonAnnualFeeCategory(List<Fee> a2)
        {
            var ans = (from a in a2
                       from b in dc.FeeCategories
                       where a.FeeCategoryId == b.FeeCategoryId
                       select new { fcategory = b.FeeCategoryName, famount = a.Amount }).ToList();
            return ans;
        }
        public IEnumerable<Object> GetJsonTermFeeCategory(List<TermFee> a2)
        {
            var ans = (from a in a2
                       from b in dc.FeeCategories
                       where a.FeeCategoryId == b.FeeCategoryId
                       select new { fcategory = b.FeeCategoryName, famount = a.Amount }).ToList();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}