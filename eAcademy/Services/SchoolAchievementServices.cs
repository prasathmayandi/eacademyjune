﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class SchoolAchievementServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public List<AchievementList> getSchoolAchievement1()
        {
            var list = (from a in db.SchoolAchievements
                        select new
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = a.PeopleInvolved,
                            Ach_allAcademicYearsA1 =a.AcademicYearId
                        }).ToList().Select(a => new AchievementList
                        {
                            Date = a.DateOfAchievement.Value.ToString("MMM dd, yyyy"),
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = a.Name,
                            txt_doa_search = a.DateOfAchievement,
                            Ach_allAcademicYearsA1 = a.Ach_allAcademicYearsA1
                        }).ToList();
            return list;
        }

        public List<AchievementList> getSchoolAchievement(int yearId)
        {
            var list = (from a in db.SchoolAchievements
                        where a.AcademicYearId == yearId
                        select new AchievementList
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = a.PeopleInvolved
                        }).ToList();
            return list;
        }

        public SchoolAchievement getAchievementById(int ESchoolAchievementId)
        {
            var getRow = (from a in db.SchoolAchievements where a.Id == ESchoolAchievementId select a).FirstOrDefault();
            return getRow;
        }

        public List<AchievementList> getSchoolAchievement()
        {
            var list = (from a in db.SchoolAchievements
                        select new
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = a.PeopleInvolved,
                            AchievementId = a.Id,
                            AcademicYearId = a.AcademicYearId,
                            Id = a.Id
                        }).ToList().Select(c => new AchievementList
                        {
                            Date = c.DateOfAchievement.Value.ToString("dd MMM, yyyy"),
                            Achievement = c.Achievement,
                            Descriptions = c.Descriptions,
                            Name = c.Name,
                            AchievementId = c.AchievementId,
                            AchievementId1 = QSCrypt.Encrypt(c.AchievementId.ToString()),
                            txt_doa_search = c.DateOfAchievement,
                            Ach_allAcademicYearsA1 = c.AcademicYearId,
                            Id = c.Id
                        }).ToList();
            return list;
        }

        public List<AchievementList> sortSchoolAchievementByYear(int Yearid)
        {
            var list = (from a in db.SchoolAchievements
                        where a.AcademicYearId == Yearid
                        select new AchievementList
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = a.PeopleInvolved,
                            AchievementId = a.Id
                        }).ToList();
            return list;

        }

        public void addSchoolAchievement(int yearId, DateTime doa, string achievement, string desc, string peopleInvolved)
        {
            var getrow = db.SchoolAchievements.Where(q => q.DateOfAchievement == doa && q.Achievement == achievement && q.AcademicYearId == yearId).FirstOrDefault();
            if (getrow == null)
            {
                SchoolAchievement add = new SchoolAchievement()
                {
                    AcademicYearId = yearId,
                    DateOfAchievement = doa,
                    Achievement = achievement,
                    Descriptions = desc,
                    PeopleInvolved = peopleInvolved
                };
                db.SchoolAchievements.Add(add);
                db.SaveChanges();
            }
        }

        public void updateSchoolAchievement(DateTime doa, string achievement, string desc, int AchievementEId, string people)
        {
            var getRow = (from a in db.SchoolAchievements where a.Id == AchievementEId select a).FirstOrDefault();
            if (getRow != null)
            {
                getRow.DateOfAchievement = doa;
                getRow.Achievement = achievement;
                getRow.Descriptions = desc;
                getRow.PeopleInvolved = people;
                db.SaveChanges();
            }
        }

        public void delSchoolAchievement(int id)
        {
            var getRow = (from a in db.SchoolAchievements where a.Id == id select a).FirstOrDefault();
            if(getRow!= null)
            {
                db.SchoolAchievements.Remove(getRow);
                db.SaveChanges();
            }

        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}