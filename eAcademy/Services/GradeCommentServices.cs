﻿using eAcademy.DataModel;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using eAcademy.Models;
using eAcademy.HelperClass;

namespace eAcademy.Services
{
    public class GradeCommentServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public GradeComment GradeExist(int yearId,int classId,int secId,int exam)
        {
            var row = (from a in db.GradeComments where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.ExamId == exam select a).FirstOrDefault();
            return row;
        }
        public GradeComment checkCommentExist(int yearId, int classId, int examId)
        {
            var row = (from a in db.GradeComments where a.AcademicYearId == yearId && a.ClassId == classId && a.ExamId == examId select a).FirstOrDefault();
            return row;
        }
        public void addGradeComment(int yearId,int classId,int secId,int exam,int StuRegId,string grade_comment,int FacultyId)
        {
            DateTime date = DateTimeByZone.getCurrentDate();

            GradeComment add = new GradeComment()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                ExamId = exam,
                StudentRegisterId = StuRegId,
                Comment = grade_comment,
                EmployeeRegisterId = FacultyId,
                CommentMarkedDate = date
            };
            db.GradeComments.Add(add);
            db.SaveChanges();
        }

        public List<GradeProgressCard1>getExistComment(int yearId,int classId,int secId,int exam)
        {
            var List = (from a in db.GradeComments
                        from b in db.StudentRollNumbers
                        where a.StudentRegisterId == b.StudentRegisterId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.ExamId == exam
                        select new GradeProgressCard1
                        {
                            stuRollNum = b.RollNumber,
                            Comment = a.Comment
                        }).ToList();
            return List;
        }
        

        public GradeComment IsReportCardGenerated(int yearId,int classId,int secId,int examId)
        {
            var row = (from a in db.GradeComments where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.ExamId == examId select a).FirstOrDefault();
            return row;
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}