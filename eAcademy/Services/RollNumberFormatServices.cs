﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class RollNumberFormatServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public RollNumberList getRollNumberFormat(int acid, int cid, int sec_id)
        {
            var format = dc.RollNumberFormats.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id)).Select(s => new RollNumberList { RollNumberFormat = s.RollNumber }).FirstOrDefault();
            return format;
        }

        public List<RollNumberList> getRollNumberFormat(int acid)
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.Sections
                       from t4 in dc.RollNumberFormats
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.SectionId == t4.SectionId && t4.AcademicYearId == acid
                       select new RollNumberList { AcademicYear = t1.AcademicYear1, Class = t2.ClassType, Section = t3.SectionName, RollNumberFormatId = t4.RollNumberFormatId, RollNumberFormat = t4.RollNumber, Status = t4.Status.Value }).ToList();
            return ans;
        }

        public bool checkRollNumberFormat(int acid, int cid, int sec_id)
        {
            var ans = dc.RollNumberFormats.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool checkRollNumberFormatExit(string RollNo)
        {
            var ans = dc.RollNumberFormats.Where(q => q.RollNumber.Equals(RollNo)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void addRollNumberFormat(int acid, int cid, int sec_id, string rollNumber)
        {
            RollNumberFormat roll = new RollNumberFormat()
            {
                AcademicYearId = acid,
                ClassId = cid,
                SectionId = sec_id,
                RollNumber = rollNumber,
                Status = true

            };
            dc.RollNumberFormats.Add(roll);
            dc.SaveChanges();
        }

        public List<RollNumberList> getRollNumberFormatList()
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.Sections
                       from t4 in dc.RollNumberFormats
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.SectionId == t4.SectionId
                       select new RollNumberList { AcademicYearId = t1.AcademicYearId, AcademicYear = t1.AcademicYear1, Class = t2.ClassType, Section = t3.SectionName, RollNumberFormatId = t4.RollNumberFormatId, RollNumberFormat = t4.RollNumber, Status = t4.Status.Value }).ToList().
                       Select(s => new RollNumberList
                       {
                           FormatId=QSCrypt.Encrypt(s.RollNumberFormatId.ToString()),
                           AcademicYearId = s.AcademicYearId,
                           AcademicYear = s.AcademicYear,
                           ClassSection = s.Class + "-" + s.Section,
                           ClassId=s.ClassId,
                           RollNumberFormatId = s.RollNumberFormatId,
                           RollNumberFormat = s.RollNumberFormat,
                           Status = s.Status
                       }).ToList();

            return ans;
        }

        internal RollNumberList getRollNumberFormatById(int formatid)
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.Sections
                       from t4 in dc.RollNumberFormats
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.SectionId == t4.SectionId && t4.RollNumberFormatId==formatid
                       select new RollNumberList {AcademicYearId=t1.AcademicYearId, AcademicYear = t1.AcademicYear1, ClassId=t2.ClassId, Class = t2.ClassType, Section = t3.SectionName, SectionId=t3.SectionId, RollNumberFormatId = t4.RollNumberFormatId, RollNumberFormat = t4.RollNumber, Status = t4.Status.Value }).FirstOrDefault();
            return ans;
        }
      
        internal bool checkRollNumberFormat(int acid, int cid, int secid, string format, string status)
        {

            int count;

            if (status == "Active")
            {

                var ans = dc.RollNumberFormats.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(secid) && q.RollNumber.Equals(format) && q.Status.Value.Equals(true)).ToList();
                count = ans.Count;
            }

            else
            {
                var ans = dc.RollNumberFormats.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(secid) && q.RollNumber.Equals(format) && q.Status.Value.Equals(false)).ToList();
                count = ans.Count;
            }

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal bool checkFormatAssigned(int acid, int cid, int secid)
        {
            var ans = dc.StudentRollNumbers.Where(q =>q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(secid)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal void updateRollNumberFormat(int formatId, string format, string status)
        {
            var update = dc.RollNumberFormats.Where(q => q.RollNumberFormatId.Equals(formatId)).FirstOrDefault();
            update.RollNumber = format;

            if (status == "Active")
            {
                update.Status = true;
            }
            else
            {
                update.Status = false;
            }
            dc.SaveChanges();

        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}