﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class ClassRoomServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        internal List<ClassRoomsList> getClassRooms()
        {
            var ans = (from t1 in dc.ClassRooms
                       select new { id = t1.ClassRoomId, RoomNo = t1.RoomNumber, capacity = t1.Capacity, status = t1.Status }).ToList().
                    Select(q => new ClassRoomsList { CRid=q.id, ClassRoomId = QSCrypt.Encrypt(q.id.ToString()), RoomNumber = q.RoomNo, Capacity = q.capacity, Status = q.status  }).ToList();
            return ans;
        }
        internal List<ClassRoomsList> getClassRooms1(int strength)
        {
            var ans = (from t1 in dc.ClassRooms
                       where t1.Capacity >= strength
                       select new { id = t1.ClassRoomId, RoomNo = t1.RoomNumber, capacity = t1.Capacity, status = t1.Status }).ToList().
                    Select(q => new ClassRoomsList { CRid = q.id, ClassRoomId = QSCrypt.Encrypt(q.id.ToString()), RoomNumber = q.RoomNo, Capacity = q.capacity, Status = q.status }).ToList();
            return ans;
        }
        internal ClassRoomsList getClassRooms(int cr_Id)
        {
            var ans = dc.ClassRooms.Where(q => q.ClassRoomId.Equals(cr_Id)).Select(q => new ClassRoomsList { CRid = q.ClassRoomId, RoomNumber = q.RoomNumber, Capacity = q.Capacity, Status = q.Status }).FirstOrDefault();
            return ans;
        }
        internal bool checkClassRoom(string RoomNo)
        {
            var ans = dc.ClassRooms.Where(q =>q.RoomNumber.Equals(RoomNo) ).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        internal bool checkClassRoom(int id,string RoomNo, int capacity, string status)
        {
            if (status == "Active")
            {
                var ans = dc.ClassRooms.Where(q =>q.ClassRoomId !=id &&  q.RoomNumber.Equals(RoomNo) && q.Status==true).ToList();
                int count = ans.Count;
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                var ans = dc.ClassRooms.Where(q => q.ClassRoomId != id && q.RoomNumber.Equals(RoomNo)  && q.Status == false).ToList();
                int count = ans.Count;
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
          
        }

        internal eAcademy.DataModel.ClassRoom getClassRoom(int cid)
        {
            var row = (from a in dc.ClassRooms where a.ClassRoomId == cid select a).FirstOrDefault();
            return row;
        }

        internal void addClassRoom(string RoomNo, int Capacity)
        {
            eAcademy.DataModel.ClassRoom add = new DataModel.ClassRoom()
            {
                RoomNumber = RoomNo,
                Capacity = Capacity,
                Status=true
            };

            dc.ClassRooms.Add(add);
            dc.SaveChanges();
        }

        internal void updatClassRooms(int CRid,string RoomNo, int capacity, string status)
        {
            var update = dc.ClassRooms.Where(q => q.ClassRoomId.Equals(CRid)).First();
            update.RoomNumber = RoomNo;
            update.Capacity = capacity;
            if (status == "Active")
            {
                update.Status = true;
            }
            else
            {
                update.Status = false;
            }
            dc.SaveChanges();
        }

        public IEnumerable<Object> ShowClassRooms()
        {
            var ClassRoomList = (from a in dc.ClassRooms where a.Status == true select new { ClassRoomId = a.ClassRoomId, ClassRoomName = a.RoomNumber }).ToList();
            return ClassRoomList;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}