﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System.Collections.Generic;
using System.Linq;
using System;

namespace eAcademy.Services
{
    public class MapRoleUserServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public MapUserRole getMapFeature(int mapId)
        {
            var ans = (from t1 in dc.TechEmployees
                       from t2 in dc.MapRoleUsers
                       from t3 in dc.Roles
                       where t1.EmployeeRegisterId == t2.EmployeeRegisterId && t2.RoleId == t3.RoleId && t2.MapId == mapId
                       select new MapUserRole { UserId = t1.EmployeeRegisterId, UserName = t1.EmployeeName, MapRoleUserId = t2.MapId, RoleId = t3.RoleId, RoleType = t3.RoleType, Status = t2.status.Value }).First();
            return ans;
        }

        public List<MapUserRole> getMapUser()
        {
            var ans = (from t1 in dc.TechEmployees
                       from t2 in dc.MapRoleUsers
                       from t3 in dc.Roles
                       where t1.EmployeeRegisterId == t2.EmployeeRegisterId && t2.RoleId == t3.RoleId
                       select new { UserId = t1.EmployeeRegisterId, UserName = t1.EmployeeName, MapRoleUserId = t2.MapId, RoleId = t3.RoleId, RoleType = t3.RoleType, Status = t2.status.Value }).OrderBy(q => q.RoleType).ToList().
                       Select(q=>new  MapUserRole { UserId = q.UserId, UserName = q.UserName, MapRoleUserId = q.MapRoleUserId, MapRoleId=QSCrypt.Encrypt(q.MapRoleUserId.ToString()), RoleId = q.RoleId, RoleType = q.RoleType, Status = q.Status }).OrderBy(q => q.RoleType).ToList();
            return ans;
        }

        public bool checkMapRoleUser(int rid, int uid)
        {
            var ans = dc.MapRoleUsers.Where(q => q.EmployeeRegisterId.Value.Equals(uid) && q.RoleId.Value.Equals(rid)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void addMapRoleUser(int rid, int uid)
        {
            MapRoleUser map = new MapRoleUser()
            {
                EmployeeRegisterId = uid,
                RoleId = rid,
                status = true
            };
            dc.MapRoleUsers.Add(map);
            dc.SaveChanges();
        }

        public bool checkMapRoleUser(int rid, int uid, string status)
        {
            int count;
            if (status == "Active")
            {
                var ans = dc.MapRoleUsers.Where(q => q.RoleId.Value.Equals(rid) && q.EmployeeRegisterId.Value.Equals(uid) && q.status.Value.Equals(true)).ToList();
                count = ans.Count;
            }

            else
            {
                var ans = dc.MapRoleUsers.Where(q => q.RoleId.Value.Equals(rid) && q.EmployeeRegisterId.Value.Equals(uid) && q.status.Value.Equals(false)).ToList();
                count = ans.Count;
            }

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void updateMapRoleUser(int mapId, string status)
        {
            var update = dc.MapRoleUsers.Where(q => q.MapId.Equals(mapId)).First();
            if (status == "Active")
            {
                update.status = true;
            }
            else
            {
                update.status = false;
            }
            dc.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}