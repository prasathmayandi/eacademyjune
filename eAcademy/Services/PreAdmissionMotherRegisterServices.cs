﻿using eAcademy.DataModel;
using System;
using System.Linq;

namespace eAcademy.Services
{
    public class PreAdmissionMotherRegisterServices : IDisposable
    {

        EacademyEntities dc = new EacademyEntities();

        public int AddMotherdetails(string mofirstname, string molastname, DateTime? modob, string moqual, string mooccu, long? momobile, string moemail, long? income)
        {

            PreAdmissionMotherRegister mother = new PreAdmissionMotherRegister()
            {
                MotherFirstname = mofirstname,
                MotherLastName = molastname,
                MotherDOB=modob,
                MotherQualification=moqual,
                MotherOccupation =mooccu,
                MotherMobileNo =momobile,
                MotherEmail =moemail,
                FatherMotherTotalIncome=income,
            };
            dc.PreAdmissionMotherRegisters.Add(mother);
            dc.SaveChanges();
            return mother.MotherAdmissionId;
        }

        public int UpdatemotherDetails1(int? motherid, string mofirstname, string molastname, DateTime? modob, string moqual, string mooccu, long? momobile, string moemail, long? income) //, 
        {
            var get = dc.PreAdmissionMotherRegisters.Where(q => q.MotherAdmissionId == motherid).FirstOrDefault();

            if (get != null)
            {
                get.MotherFirstname = mofirstname;
                get.MotherLastName = molastname;
                get.MotherDOB = modob;
                get.MotherEmail = moemail;
                get.MotherQualification = moqual;
                get.MotherOccupation = mooccu;
                get.MotherMobileNo = momobile;
                get.FatherMotherTotalIncome = income;
                dc.SaveChanges();
            }
            return get.MotherAdmissionId;
        }
        public int UpdatemotherDetails(int? motherid, string moqual, string mooccu,string MEmail, long? momobile) //, 
        {
            var get = dc.PreAdmissionMotherRegisters.Where(q => q.MotherAdmissionId == motherid).FirstOrDefault();

            if (get != null)
            {
                get.MotherEmail = MEmail;
                get.MotherQualification = moqual;
                get.MotherOccupation = mooccu;
                get.MotherMobileNo = momobile;
                dc.SaveChanges();
            }
            return get.MotherAdmissionId;
        }

        public int CheckMotherExit(int primaryuserid,int Newstudentid,string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email,long? totalincome)
        {
            int Motherid = 0; 
            var GetMother = (from a in dc.PreAdmissionTransactions
                       from b in dc.PreAdmissionMotherRegisters
                       where a.PrimaryUserAdmissionId == primaryuserid && b.MotherAdmissionId == a.MotherAdmissionId && b.MotherFirstname == firstname && b.MotherLastName == lastname && b.MotherDOB == dob
                       select b).FirstOrDefault();
            if(GetMother !=null)
            {
                Motherid = GetMother.MotherAdmissionId;

                UpdatemotherDetails(Motherid, qual, occu,email, mobile);
            }
            else
            {
                Motherid = AddMotherdetails(firstname, lastname, dob, qual, occu, mobile, email, totalincome);
            }
            return Motherid;
        }

        public PreAdmissionMotherRegister GetEnrolementMotherdetails(int StudentAdmissionId)
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters
                       from b in dc.PreAdmissionTransactions
                       from c in dc.PreAdmissionMotherRegisters
                       where a.StudentAdmissionId == StudentAdmissionId && b.StudentAdmissionId == a.StudentAdmissionId && c.MotherAdmissionId == b.MotherAdmissionId
                       select c).FirstOrDefault();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}