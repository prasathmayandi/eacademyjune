﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class HomeworkServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public List<StudentHomework>getStudentHomework(int yearId, int classId, int secId, int subId, DateTime date)
        {
            var list = (from a in db.HomeWork
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.SubjectId == subId &&
                        a.DateOfWorkPosted == date
                        select new 
                        {
                            HomeWorkId = a.HomeWorkId,
                            PostedDate = a.DateOfWorkPosted,
                            Homework = a.HomeWork1,
                            Description = a.Descriptions,
                            HomeWorkFileName = a.HomeWorkFileName,
                            DateOfSubmission = a.DateToCompleteWork
                        }).ToList().Select(a=> new StudentHomework
                        {
                            HomeWorkId = a.HomeWorkId,
                            PostedDate = a.PostedDate.Value.ToString("dd MMM, yyyy"),
                            Homework = a.Homework,
                            Description = a.Description,
                            HomeWorkFileName = a.HomeWorkFileName,
                            DateOfSubmission = a.DateOfSubmission.Value.ToString("dd MMM, yyyy")
                        }).ToList();
            return list;
        }

        public List<ParentHomework> getParentHomework(int yearId, int classId, int secId, DateTime date)
        {
            var list = (from a in db.HomeWork
                        from b in db.AssignSubjectToSections
                        from c in db.Subjects
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.DateOfWorkPosted == date &&
                        a.SubjectId == b.SubjectId &&
                        a.SubjectId == c.SubjectId
                        select new ParentHomework
                        {
                            HomeWorkId = a.HomeWorkId,
                            DateOfWorkPosted = a.DateOfWorkPosted,
                            Subject = c.SubjectName,
                            HomeWork1 = a.HomeWork1,
                            Descriptions = a.Descriptions,
                            HomeWorkFileName = a.HomeWorkFileName,
                            DateToCompleteWork = a.DateToCompleteWork
                        }).ToList();
            return list;
        }

        public HomeWork getStudentHomeworkFile(int fid)
        {
            var getHomeworkFile = (from a in db.HomeWork where a.HomeWorkId == fid select a).FirstOrDefault();
            return getHomeworkFile;
        }

        public HomeWork getHomeworkDetail(DateTime currentDate)
        {
            var Homework = (from a in db.HomeWork where a.DateOfWorkPosted == currentDate select a).OrderByDescending(x => x.DateOfWorkPosted).FirstOrDefault();
            return Homework;
        }

        public void addHomework(int yearId, int classId, int secId, int subId, DateTime submissionDate, string Homework, string desc, string fileName, int FacultyId)
        {
            DateTime date = DateTimeByZone.getCurrentDate();

            HomeWork add = new HomeWork()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                SubjectId = subId,
                DateOfWorkPosted = date,
                DateToCompleteWork = submissionDate,
                HomeWork1 = Homework,
                Descriptions = desc,
                HomeWorkFileName = fileName,
                HomeWorkStatus = true,
                EmployeeRegisterId = FacultyId,          
            };
            db.HomeWork.Add(add);
            db.SaveChanges();
        }
        public List<HomeworkList> getHomeworkList(int yearId, int classId, int secId, int subId, DateTime fromDate, DateTime toDate, int FacultyId)
        {
            var list = (from a in db.HomeWork
                        where a.AcademicYearId == yearId &&
                                 a.ClassId == classId &&
                                 a.SectionId == secId &&
                                 a.SubjectId == subId &&
                                 a.EmployeeRegisterId == FacultyId &&
                                 a.DateToCompleteWork >= fromDate &&
                                 a.DateToCompleteWork <= toDate
                        select new HomeworkList
                        {
                            HomeWorkId = a.HomeWorkId,
                            DateOfWorkPosted = a.DateOfWorkPosted,
                            DateToCompleteWork = a.DateToCompleteWork,
                            HomeWork1 = a.HomeWork1,
                            Descriptions = a.Descriptions,
                            HomeWorkStatus = a.HomeWorkStatus,
                            HomeWorkFileName = a.HomeWorkFileName
                        }).ToList();

            return list;
        }
        public void updateHomeworkFile(int eid, string fileName1)
        {
            var getRow = (from a in db.HomeWork where a.HomeWorkId == eid select a).FirstOrDefault();
            getRow.HomeWorkFileName = fileName1;
            db.SaveChanges();
        }

        public void updateHomework(DateTime Date, string Homework, string desc, Boolean status, int eid)
        {
            var getRow = (from a in db.HomeWork where a.HomeWorkId == eid select a).FirstOrDefault();
            getRow.HomeWork1 = Homework;
            getRow.DateToCompleteWork = Date;
            getRow.Descriptions = desc;
            getRow.HomeWorkStatus = status;
            db.SaveChanges();
        }

        public void delHomework(int id)
        {
            var getRow = (from a in db.HomeWork where a.HomeWorkId == id select a).FirstOrDefault();
            db.HomeWork.Remove(getRow);
            db.SaveChanges();
        }

        public List<StudentHomework> getStudentHomework1(int yearId, int classId, int secId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in db.HomeWork
                        from b in db.Subjects
                        where a.SubjectId == b.SubjectId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.DateOfWorkPosted >= fdate &&
                        a.DateOfWorkPosted <= toDate
                        select new
                        {
                            HomeWorkId = a.HomeWorkId,
                            Subject = b.SubjectName,
                            DateOfWorkPosted = a.DateOfWorkPosted,
                            HomeWork1 = a.HomeWork1,
                            Descriptions = a.Descriptions,
                            HomeWorkFileName = a.HomeWorkFileName,
                            DateToCompleteWork = a.DateToCompleteWork
                        }).ToList().Select(a => new StudentHomework
                        {
                            HomeWork_Id = QSCrypt.Encrypt(a.HomeWorkId.ToString()),
                            Subject = a.Subject,
                            PostedDate = a.DateOfWorkPosted.Value.ToString("dd MMM, yyyy"),
                            Homework = a.HomeWork1,
                            Description = a.Descriptions,
                            HomeWorkFileName = getHomeworkWithFile(a.HomeWorkId),
                            DateOfSubmission = a.DateToCompleteWork.Value.ToString("dd MMM, yyyy"),
                            HomeWorkWithFileName = getParentHomeworkWithFile(a.HomeWorkId),
                        }).ToList();
            return list;
        }

        

        public List<StudentHomework> getStudentHomework1(int yearId, int classId, int secId, int subId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in db.HomeWork
                        from b in db.Subjects
                        where a.SubjectId == b.SubjectId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.SubjectId == subId &&
                        a.DateOfWorkPosted >= fdate &&
                        a.DateOfWorkPosted <= toDate
                        select new
                        {
                            HomeWorkId = a.HomeWorkId,
                            Subject = b.SubjectName,
                            DateOfWorkPosted = a.DateOfWorkPosted,
                            HomeWork1 = a.HomeWork1,
                            Descriptions = a.Descriptions,
                            HomeWorkFileName = a.HomeWorkFileName,
                            DateToCompleteWork = a.DateToCompleteWork
                        }).ToList().Select(a => new StudentHomework
                        {
                            HomeWork_Id = QSCrypt.Encrypt(a.HomeWorkId.ToString()),
                            Subject = a.Subject,
                            PostedDate = a.DateOfWorkPosted.Value.ToString("dd MMM, yyyy"),
                            Homework = a.HomeWork1,
                            Description = a.Descriptions,
                            HomeWorkFileName = a.HomeWorkFileName,
                            DateOfSubmission = a.DateToCompleteWork.Value.ToString("dd MMM, yyyy"),
                            HomeWorkWithFileName = getParentHomeworkWithFile(a.HomeWorkId),
                        }).ToList();
            return list;
        }



        public List<StudentHomework> getStudentHomework2(int yearId, int classId, int secId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in db.HomeWork
                        from b in db.Subjects
                        where a.SubjectId == b.SubjectId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.DateOfWorkPosted >= fdate &&
                        a.DateOfWorkPosted <= toDate
                        select new
                        {
                            HomeWorkId = a.HomeWorkId,
                            Subject = b.SubjectName,
                            DateOfWorkPosted = a.DateOfWorkPosted,
                            HomeWork1 = a.HomeWork1,
                            Descriptions = a.Descriptions,
                            HomeWorkFileName = a.HomeWorkFileName,
                            DateToCompleteWork = a.DateToCompleteWork
                        }).ToList().Select(a => new StudentHomework
                        {
                            HomeWork_Id = QSCrypt.Encrypt(a.HomeWorkId.ToString()),
                            Subject = a.Subject,
                            PostedDate = a.DateOfWorkPosted.Value.ToString("dd MMM, yyyy"),
                            Homework = a.HomeWork1,
                            Description = a.Descriptions,
                            HomeWorkFileName = a.HomeWorkFileName,
                            DateOfSubmission = a.DateToCompleteWork.Value.ToString("dd MMM, yyyy"),
                            HomeWorkWithFileName = getStudentHomeworkWithFile(a.HomeWorkId),
                        }).ToList();
            return list;
        }


        public List<StudentHomework> getStudentHomework2(int yearId, int classId, int secId, int subId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in db.HomeWork
                        from b in db.Subjects
                        where a.SubjectId == b.SubjectId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.SubjectId == subId &&
                        a.DateOfWorkPosted >= fdate &&
                        a.DateOfWorkPosted <= toDate
                        select new
                        {
                            HomeWorkId = a.HomeWorkId,
                            Subject = b.SubjectName,
                            DateOfWorkPosted = a.DateOfWorkPosted,
                            HomeWork1 = a.HomeWork1,
                            Descriptions = a.Descriptions,
                            HomeWorkFileName = a.HomeWorkFileName,
                            DateToCompleteWork = a.DateToCompleteWork
                        }).ToList().Select(a => new StudentHomework
                        {
                            HomeWork_Id = QSCrypt.Encrypt(a.HomeWorkId.ToString()),
                            Subject = a.Subject,
                            PostedDate = a.DateOfWorkPosted.Value.ToString("dd MMM, yyyy"),
                            Homework = a.HomeWork1,
                            Description = a.Descriptions,
                            HomeWorkFileName = a.HomeWorkFileName,
                            DateOfSubmission = a.DateToCompleteWork.Value.ToString("dd MMM, yyyy"),
                            HomeWorkWithFileName = getStudentHomeworkWithFile(a.HomeWorkId),
                        }).ToList();
            return list;
        }

        
        public List<HomeworkList> getHomeworkList(int? yearId, int? classId, int? secId, int? subId, DateTime? fromDate, DateTime? toDate, int? FacultyId)
        {
            var list = (from a in db.HomeWork
                        where a.AcademicYearId == yearId &&
                                 a.ClassId == classId &&
                                 a.SectionId == secId &&
                                 a.SubjectId == subId &&
                                 a.EmployeeRegisterId == FacultyId &&
                                 a.DateOfWorkPosted >= fromDate &&
                                 a.DateOfWorkPosted <= toDate
                        select new
                        {
                            HomeWorkId = a.HomeWorkId,
                            DateOfWorkPosted = a.DateOfWorkPosted,
                            DateToCompleteWork = a.DateToCompleteWork,
                            HomeWork1 = a.HomeWork1,
                            Descriptions = a.Descriptions,
                            HomeWorkStatus = a.HomeWorkStatus,
                            HomeWorkFileName = a.HomeWorkFileName,
                            MI_HW_List_txt_fromDate = a.DateOfWorkPosted,
                            MI_HW_List_txt_toDate = a.DateOfWorkPosted
                        }).ToList().Select(a => new HomeworkList
                        {
                            HomeWork_Id = QSCrypt.Encrypt(a.HomeWorkId.ToString()),
                            PostedtDate = a.DateOfWorkPosted.Value.ToString("dd MMM, yyyy"),
                            LastDate = a.DateToCompleteWork.Value.ToString("dd MMM, yyyy"),
                            HomeWork1 =     a.HomeWork1,  //+ "   <i class='fa fa-paperclip'></i>",
                            HomeWorkWithFileName = getHomeworkWithFile(a.HomeWorkId),
                            Descriptions = a.Descriptions,
                            HomeWorkStatus = a.HomeWorkStatus,
                            HomeWorkFileName = a.HomeWorkFileName,
                            MI_HW_List_txt_fromDate = a.DateOfWorkPosted,
                            MI_HW_List_txt_toDate = a.DateOfWorkPosted
                        }).ToList();

            return list;
        }

        public string getHomeworkWithFile(int HomeworkId)
        {
            string hw;
            var File = (from a in db.HomeWork where a.HomeWorkId == HomeworkId select a).FirstOrDefault().HomeWorkFileName;
            if (File != null)
            {
                var HW = (from a in db.HomeWork where a.HomeWorkId == HomeworkId select a).FirstOrDefault().HomeWork1;
                string HomeWork_Id = QSCrypt.Encrypt(HomeworkId.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    hw = "<a href=/ModulesInfo/OpenHomeworkFile/?id=" + HomeWork_Id + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + HW;
                }
                else
                {
                    hw = "<a href=/ModulesInfo/OpenHomeworkFile/?id=" + HomeWork_Id + "&name=student><i class='fa fa-file-word-o'></i></a>" + HW;
                }
            }
            else
            {
                var HW = (from a in db.HomeWork where a.HomeWorkId == HomeworkId select a).FirstOrDefault().HomeWork1;
                hw = HW;
            }
            return hw;
        }


        public string getParentHomeworkWithFile(int HomeworkId)
        {
            string hw;
            var File = (from a in db.HomeWork where a.HomeWorkId == HomeworkId select a).FirstOrDefault().HomeWorkFileName;
            if (File != null)
            {
                var HW = (from a in db.HomeWork where a.HomeWorkId == HomeworkId select a).FirstOrDefault().HomeWork1;
                string HomeWork_Id = QSCrypt.Encrypt(HomeworkId.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    hw = "<a href=/ParentModuleAssigned/OpenHomeworkFile/?id=" + HomeWork_Id + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + HW;
                }
                else
                {
                    hw = "<a href=/ParentModuleAssigned/OpenHomeworkFile/?id=" + HomeWork_Id + "&name=student><i class='fa fa-file-word-o'></i></a>" + HW;
                }
            }
            else
            {
                var HW = (from a in db.HomeWork where a.HomeWorkId == HomeworkId select a).FirstOrDefault().HomeWork1;
                hw = HW;
            }
            return hw;
        }

        public string getStudentHomeworkWithFile(int HomeworkId)
        {
            string hw;
            var File = (from a in db.HomeWork where a.HomeWorkId == HomeworkId select a).FirstOrDefault().HomeWorkFileName;
            if (File != null)
            {
                var HW = (from a in db.HomeWork where a.HomeWorkId == HomeworkId select a).FirstOrDefault().HomeWork1;
                string HomeWork_Id = QSCrypt.Encrypt(HomeworkId.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    hw = "<a href=/StudentModuleAssigned/OpenHomeworkFile/?id=" + HomeWork_Id + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + HW;
                }
                else
                {
                    hw = "<a href=/StudentModuleAssigned/OpenHomeworkFile/?id=" + HomeWork_Id + "&name=student><i class='fa fa-file-word-o'></i></a>" + HW;
                }
            }
            else
            {
                var HW = (from a in db.HomeWork where a.HomeWorkId == HomeworkId select a).FirstOrDefault().HomeWork1;
                hw = HW;
            }
            return hw;
        }



        public HomeWork getHomework(int HomeworkId)
        {
            var getRow = (from a in db.HomeWork where a.HomeWorkId == HomeworkId select a).FirstOrDefault();
            return getRow;
        }

        public HomeworkReplyList getHomeworkDetailById(int HomeworkId)
        {
            var getRow = (from a in db.HomeWork
                          from b in db.Subjects
                          where a.SubjectId == b.SubjectId &&
                           a.HomeWorkId == HomeworkId
                          select new HomeworkReplyList
                          {
                              Subject = b.SubjectName,
                              Title = a.HomeWork1,
                              Description = a.Descriptions,
                              SubmissionDate = SqlFunctions.DateName("day", a.DateToCompleteWork).Trim() + " " + SqlFunctions.DateName("month", a.DateToCompleteWork).Remove(3) + ", " + SqlFunctions.DateName("year", a.DateToCompleteWork)
                          }).FirstOrDefault();
            return getRow;
        }

        public void updateHomework(DateTime Date, string Homework, string desc, Boolean status, string fileName, int eid)
        {
            var getRow = (from a in db.HomeWork where a.HomeWorkId == eid select a).FirstOrDefault();
            getRow.HomeWork1 = Homework;
            getRow.DateToCompleteWork = Date;
            getRow.Descriptions = desc;
            getRow.HomeWorkStatus = status;
            getRow.HomeWorkFileName = fileName;
            db.SaveChanges();
        }
        public HomeWork GetClassDetails(int HomeworkId)
        {
            var row = (from a in db.HomeWork where a.HomeWorkId == HomeworkId select a).FirstOrDefault();
            return row;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}