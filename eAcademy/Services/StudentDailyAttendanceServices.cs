﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Globalization;
using System.Linq;

namespace eAcademy.Services
{
    public class StudentDailyAttendanceServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public StudentDailyAttendance AttendanceExist(int yearId, int classId, int secId, DateTime date)
        {
            var getRow = (from a in db.StudentDailyAttendances where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.CurrentDate == date select a).FirstOrDefault();
            return getRow;
        }

        public void updateStudentDailyAttendance(int RegId, int year, int classId, int secId, DateTime date, string status, string comment, DateTime updatedDate, int EmpRegId)
        {
            var getRow = (from a in db.StudentDailyAttendances where a.StudentRegisterId == RegId && a.AcademicYearId == year && a.ClassId == classId && a.SectionId == secId && a.CurrentDate == date select a).FirstOrDefault();
            getRow.AttendanceStatus = status;
            getRow.LeaveReason = comment;
            getRow.AttendanceUpdatedDate = updatedDate;
            getRow.AttendanceUpdatedBy = EmpRegId;
            db.SaveChanges();
        }

        public int getWorkingDaysCountNew(int yearId, int classId, int secId, DateTime SM_txt_FromDate, DateTime SM_txt_ToDate)
        {
            var list = (from a in db.StudentDailyAttendances where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.CurrentDate >= SM_txt_FromDate && a.CurrentDate <= SM_txt_ToDate select new { date = a.CurrentDate }).Distinct().ToList();
            return list.Count;
        }

        public List<StudentAttendanceExistList> getAttendanceExistList(int yearId, int classId, int secId, DateTime date)
        {
            var getRow = (from a in db.StudentDailyAttendances
                          from b in db.Students
                          from c in db.tblAttendanceStatus
                          where a.StudentRegisterId == b.StudentRegisterId &&
                          a.AttendanceStatus == c.AttendanceStatusValue &&
                          a.AcademicYearId == yearId &&
                          a.ClassId == classId &&
                          a.SectionId == secId &&
                          a.CurrentDate == date
                          select new
                          {
                              stuRegId = a.StudentRegisterId,
                              StudentId = b.StudentId,
                              FName = b.FirstName,
                              LName = b.LastName,
                              status = a.AttendanceStatus,
                              comment = a.LeaveReason,
                              AttendanceStatusName = c.AttendanceStatusName,
                              AttendanceId = a.Id
                          }).ToList().Select(c => new StudentAttendanceExistList
                          {
                              StudentRegId = QSCrypt.Encrypt(c.stuRegId.ToString()),
                              StudentId = c.StudentId,
                              StudentName = c.FName + " " + c.LName,
                              status = c.status,
                              comment = c.comment,
                              AttendanceStatusName = c.AttendanceStatusName
                          }).ToList();
            return getRow;
        }
        public void addStudentDailyAttendance(int StudentRegisterId, int AcademicYearId, int ClassId, int SectionId, DateTime CurrentDate, string AttendanceStatus, string LeaveReason, int EmpRegId)
        {
            StudentDailyAttendance add = new StudentDailyAttendance()
            {
                StudentRegisterId = StudentRegisterId,
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                CurrentDate = CurrentDate,
                LeaveReason = LeaveReason,
                AttendanceStatus = AttendanceStatus,
                EmployeeRegisterId = EmpRegId
            };
            db.StudentDailyAttendances.Add(add);
            db.SaveChanges();
        }

        public void addStudentDailyAttendanceOnLeaveAccept(int? StudentRegisterId, int? AcademicYearId, int? ClassId, int? SectionId, DateTime CurrentDate, string LeaveReason, string AttendanceStatus, DateTime MarkedDate)
        {
            StudentDailyAttendance add = new StudentDailyAttendance()
            {
                StudentRegisterId = StudentRegisterId,
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                CurrentDate = CurrentDate,
                LeaveReason = LeaveReason,
                AttendanceStatus = AttendanceStatus
            };
            db.StudentDailyAttendances.Add(add);
            db.SaveChanges();
        }

        public List<StudentAttendanceReport> getStudentDailyAttendance(int yearId, int classId, int secId, int StudentRegId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in db.StudentDailyAttendances
                        where a.AcademicYearId == yearId &&
                        a.CurrentDate >= fdate &&
                        a.CurrentDate <= toDate &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&


                        a.StudentRegisterId == StudentRegId
                        select new StudentAttendanceReport
                        {
                            Date = a.CurrentDate,
                            Status = a.AttendanceStatus,
                            Reason = a.LeaveReason
                        }).ToList();
            return list;
        }

        public IEnumerable<Object> getPresentStudent(int yearId, int classId, int secId, int StudentRegId, DateTime fdate, DateTime ToDate, string PresentStatus)
        {
            var getPresentlist = (from a in db.StudentDailyAttendances where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.StudentRegisterId == StudentRegId && a.CurrentDate >= fdate && a.CurrentDate <= ToDate && a.AttendanceStatus == PresentStatus select a).ToList();
            return getPresentlist;
        }

        public List<StudentDailyAttendance> getPresentStudents(int? yearId, int? classId, int? secId, DateTime? date1)
        {
            var getPresentStudents = (from a in db.StudentDailyAttendances where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.CurrentDate == date1 && a.AttendanceStatus == "p" select a).ToList();
            return getPresentStudents;
        }
        
        public List<FacultyAttendanceReport> StudentAttendanceList(int? studentId, DateTime? sDate, DateTime? eDate)
        {
            var list = (from a in db.StudentDailyAttendances
                        where a.StudentRegisterId == studentId &&
                              a.CurrentDate >= sDate &&
                              a.CurrentDate <= eDate
                        select new
                        {
                            Date = a.CurrentDate,
                            Status = a.AttendanceStatus,
                            Reason = a.LeaveReason,
                            fdate_search = a.CurrentDate,
                            toDate_search = a.CurrentDate,
                            stuId_search = a.StudentRegisterId
                        }).ToList().Select(c => new FacultyAttendanceReport
                        {
                            Date1 = c.Date.Value.ToString("MMM dd,yyyy"),
                            Status = c.Status,
                            Reason = c.Reason,
                            Att_SectionStudent = c.stuId_search,
                            txt_FromDate = c.fdate_search,
                            txt_ToDate = c.toDate_search
                        }).ToList();
            return list;
        }

        public List<GetClassTeacher> getAttendanceMonth(int yearId, int classId, int secId)
        {
            var getAttendanceMonth = (from a in db.StudentDailyAttendances
                                      where a.AcademicYearId == yearId &&
                                      a.ClassId == classId &&
                                      a.SectionId == secId
                                      select new GetClassTeacher
                                      {
                                          month = a.CurrentDate.Value.Month,
                                          monthname = a.CurrentDate
                                      }).OrderBy(a => a.month).Distinct().ToList();
            return getAttendanceMonth;
        }

        public List<StudentDailyAttendance> getPresentDays(int yearId, int classId, int secId, int month)
        {
            var getPresentDays = (from a in db.StudentDailyAttendances
                                  where a.AcademicYearId == yearId &&
                                  a.ClassId == classId &&
                                  a.SectionId == secId &&
                                  a.CurrentDate.Value.Month == month && 
                                  a.AttendanceStatus == "P"
                                  select a).ToList();

            return getPresentDays;
        }
        public IEnumerable getCurrentMonthWorkingDays(int yearId, int classId, int secId, int month)
        {
            var getPresentDays = (from a in db.StudentDailyAttendances
                                  where a.AcademicYearId == yearId &&
                                  a.ClassId == classId &&
                                  a.SectionId == secId &&
                                  a.CurrentDate.Value.Month == month

                                  select new { date = a.CurrentDate }).OrderBy(a => a.date).Distinct().ToList();

            return getPresentDays;
        }
        public List<StudentDailyAttendance> getStudentPresentDays(int yearId, int classId, int secId, int month, int StudentRegId)
        {
            var getPresentDays = (from a in db.StudentDailyAttendances
                                  where a.AcademicYearId == yearId &&
                                  a.ClassId == classId &&
                                  a.SectionId == secId &&
                                  a.CurrentDate.Value.Month == month && a.StudentRegisterId == StudentRegId &&
                                  a.AttendanceStatus == "P"
                                  select a).ToList();

            return getPresentDays;
        }

        public List<GetClassTeacher> getAttendanceDate(int? yearId, int? classId, int? secId, int currentMonth)
        {
            var getDate = (from a in db.StudentDailyAttendances
                           where a.AcademicYearId == yearId &&
                           a.ClassId == classId &&
                           a.SectionId == secId &&
                           a.CurrentDate.Value.Month == currentMonth
                           select new GetClassTeacher
                           {
                               date = a.CurrentDate
                           }).OrderBy(a => a.date).Distinct().ToList();

            return getDate;

        }
        public List<GetClassTeacher> getWorkingDays(int? yearId, int? classId, int? secId, int month)
        {
            var getWorkingDays = (from a in db.StudentDailyAttendances
                                  from b in db.Students
                                  where a.StudentRegisterId == b.StudentRegisterId &&
                                      a.AcademicYearId == yearId &&
                                      a.ClassId == classId &&
                                      a.SectionId == secId &&
                                      a.CurrentDate.Value.Month == month &&
                                      b.StudentStatus == true
                                  select new GetClassTeacher
                                  {
                                      date = a.CurrentDate
                                  }).Distinct().ToList();

            return getWorkingDays;
        }

        public List<StudentRemark> getStudentRemark(int stuRegId)
        {
            var list = ((from a in db.StudentDailyAttendances
                         from b in db.TechEmployees
                         where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                         a.StudentRegisterId == stuRegId &&
                         a.LeaveReason != ""
                         select new StudentRemark
                         {
                             date = SqlFunctions.DateName("day", a.CurrentDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.CurrentDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.CurrentDate),
                             CommentFrom = "Class Attendance",
                             Name = b.EmployeeName,
                             Remark = a.LeaveReason
                         })
                        .Union(from a in db.MarkTotals
                               from b in db.TechEmployees
                               where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                               a.StudentRegisterId == stuRegId &&
                               a.Comment != "null"
                               select new StudentRemark
                               {
                                   date = SqlFunctions.DateName("day", a.MarkedDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.MarkedDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.MarkedDate),
                                   CommentFrom = "Progrss card",
                                   Name = b.EmployeeName,
                                   Remark = a.Comment
                               })
                              .Union(from a in db.StudentGrades
                                     from b in db.TechEmployees
                                     where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                                     a.StudentRegisterId == stuRegId &&
                                     a.Comment != ""
                                     select new StudentRemark
                                     {
                                         date = SqlFunctions.DateName("day", a.CommentDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.CommentDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.CommentDate),
                                         CommentFrom = "Subject Mark",
                                         Name = b.EmployeeName,
                                         Remark = a.Comment
                                     })
                                      .Union(from a in db.StudentMarks
                                             from b in db.TechEmployees
                                             where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                                             a.StudentRegisterId == stuRegId &&
                                             a.Remark != null &&
                                             a.Remark != ""
                                             select new StudentRemark
                                             {
                                                 date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                                 CommentFrom = "Subject Mark",
                                                 Name = b.EmployeeName,
                                                 Remark = a.Remark
                                             })

                                      .Union(from a in db.ELearningReplyFromStudents
                                             from b in db.TechEmployees
                                             where a.RemarkGivenBy == b.EmployeeRegisterId &&
                                             a.StudentRegisterId == stuRegId &&
                                             a.Remark != null &&
                                              a.Remark != ""
                                             select new StudentRemark
                                             {
                                                 date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                                 CommentFrom = "E-Learning",
                                                 Name = b.EmployeeName,
                                                 Remark = a.Remark
                                             })
                                                    .Union(from a in db.HomeworkReplyFromStudents
                                                           from b in db.TechEmployees
                                                           where a.RemarkGivenBy == b.EmployeeRegisterId &&
                                                           a.StudentRegisterId == stuRegId &&
                                                           a.Remark != null &&
                                                            a.Remark != ""
                                                           select new StudentRemark
                                                           {
                                                               date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                                               CommentFrom = "Homework",
                                                               Name = b.EmployeeName,
                                                               Remark = a.Remark
                                                           })
                                                            .Union(from a in db.AssignmentReplyFromStudents
                                                                   from b in db.TechEmployees
                                                                   where a.RemarkGivenBy == b.EmployeeRegisterId &&
                                                                   a.StudentRegisterId == stuRegId &&
                                                                   a.Remark != null &&
                                                                   a.Remark != ""
                                                                   select new StudentRemark
                                                                   {
                                                                       date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                                                       CommentFrom = "Assignment",
                                                                       Name = b.EmployeeName,
                                                                       Remark = a.Remark
                                                                   })
                                     ).ToList();
            return list;
        }
        public List<StudentRemark> getStudentRemark(int stuRegId, DateTime? CI_Remark_fdate, DateTime? CI_Remark_toDate)
        {
            var list = ((from a in db.StudentDailyAttendances
                         from b in db.TechEmployees
                         where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                         a.StudentRegisterId == stuRegId &&
                         a.CurrentDate >= CI_Remark_fdate &&
                         a.CurrentDate <= CI_Remark_toDate &&
                         a.LeaveReason != ""
                         select new StudentRemark
                         {
                             date = SqlFunctions.DateName("day", a.CurrentDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.CurrentDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.CurrentDate),
                             CommentFrom = "Class Attendance",
                             Name = b.EmployeeName,
                             Remark = a.LeaveReason
                         })
                        .Union(from a in db.MarkTotals
                               from b in db.TechEmployees
                               where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                               a.StudentRegisterId == stuRegId &&
                               a.MarkedDate >= CI_Remark_fdate &&
                               a.MarkedDate <= CI_Remark_toDate &&
                               a.Comment != "null"
                               select new StudentRemark
                               {
                                   date = SqlFunctions.DateName("day", a.MarkedDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.MarkedDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.MarkedDate),
                                   CommentFrom = "Progress Card",
                                   Name = b.EmployeeName,
                                   Remark = a.Comment,

                               })
                                .Union(from a in db.StudentGrades
                                       from b in db.TechEmployees
                                       where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                                       a.StudentRegisterId == stuRegId &&
                                       a.CommentDate >= CI_Remark_fdate &&
                                       a.CommentDate <= CI_Remark_toDate &&
                                       a.Comment != ""
                                       select new StudentRemark
                                       {
                                           date = SqlFunctions.DateName("day", a.CommentDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.CommentDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.CommentDate),
                                           CommentFrom = "Subject Mark",
                                           Name = b.EmployeeName,
                                           Remark = a.Comment
                                       })
                .Union(from a in db.StudentMarks
                       from b in db.TechEmployees
                       where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                       a.StudentRegisterId == stuRegId &&
                          a.RemarkDate >= CI_Remark_fdate &&
                         a.RemarkDate <= CI_Remark_toDate &&
                          a.Remark != null
                       select new StudentRemark
                       {
                           date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                           CommentFrom = "Subject Mark",
                           Name = b.EmployeeName,
                           Remark = a.Remark
                       })

                                      .Union(from a in db.ELearningReplyFromStudents
                                             from b in db.TechEmployees
                                             where a.RemarkGivenBy == b.EmployeeRegisterId &&
                                             a.StudentRegisterId == stuRegId &&
                                                a.RemarkDate >= CI_Remark_fdate &&
                                                a.RemarkDate <= CI_Remark_toDate &&
                                                  a.Remark != null
                                             select new StudentRemark
                                             {
                                                 date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                                 CommentFrom = "E-Learning",
                                                 Name = b.EmployeeName,
                                                 Remark = a.Remark
                                             })
                                                    .Union(from a in db.HomeworkReplyFromStudents
                                                           from b in db.TechEmployees
                                                           where a.RemarkGivenBy == b.EmployeeRegisterId &&
                                                           a.StudentRegisterId == stuRegId &&
                                                              a.RemarkDate >= CI_Remark_fdate &&
                                                              a.RemarkDate <= CI_Remark_toDate &&
                                                              a.Remark != null
                                                           select new StudentRemark
                                                           {
                                                               date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                                               CommentFrom = "Homework",
                                                               Name = b.EmployeeName,
                                                               Remark = a.Remark
                                                           })
                                                            .Union(from a in db.AssignmentReplyFromStudents
                                                                   from b in db.TechEmployees
                                                                   where a.RemarkGivenBy == b.EmployeeRegisterId &&
                                                                   a.StudentRegisterId == stuRegId &&
                                                                   a.RemarkDate >= CI_Remark_fdate &&
                                                                   a.RemarkDate <= CI_Remark_toDate &&
                                                                   a.Remark != null
                                                                   select new StudentRemark
                                                                   {
                                                                       date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                                                       CommentFrom = "Assignment",
                                                                       Name = b.EmployeeName,
                                                                       Remark = a.Remark
                                                                   })
                                ).ToList();
            return list;
        }

        public List<StudentRemark> getStudentRemark(int Remark_allAcademicYears, int classId, int secId, int studentRegId, string Status, DateTime? Remark_fdate, DateTime? Remark_toDate, string mark)
        {
            if (Status == "ClassRemark")
            {
                var list = (from a in db.StudentDailyAttendances
                            from b in db.TechEmployees
                            where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                            a.AcademicYearId == Remark_allAcademicYears &&
                            a.ClassId == classId &&
                            a.SectionId == secId &&
                            a.StudentRegisterId == studentRegId &&
                            a.CurrentDate >= Remark_fdate &&
                            a.CurrentDate <= Remark_toDate
                            select new StudentRemark
                            {
                                date = SqlFunctions.DateName("day", a.CurrentDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.CurrentDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.CurrentDate),
                                CommentFrom = "Class Attendance",
                                Remark = a.LeaveReason,
                                Name = b.EmployeeName+" "+b.LastName
                            }).ToList();
                return list;
            }
            else if (Status == "ProgressCardRemark")
            {

                if (mark == "Mark")
                {
                    var list = (from a in db.MarkTotals
                                 from b in db.TechEmployees
                                where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                                a.AcademicYearId == Remark_allAcademicYears &&
                                a.ClassId == classId &&
                                a.SectionId == secId &&
                                a.StudentRegisterId == studentRegId &&
                                a.MarkedDate >= Remark_fdate &&
                                a.MarkedDate <= Remark_toDate
                                select new StudentRemark
                                {
                                    date = SqlFunctions.DateName("day", a.MarkedDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.MarkedDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.MarkedDate),
                                    CommentFrom = "ProgressCard",
                                    Remark = a.Comment,
                                    Name = b.EmployeeName + " " + b.LastName
                                }).ToList();
                    return list;
                }
                else
                {
                    var list = (from a in db.GradeComments
                                from b in db.TechEmployees
                                where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                                a.AcademicYearId == Remark_allAcademicYears &&
                                a.ClassId == classId &&
                                a.SectionId == secId &&
                                a.StudentRegisterId == studentRegId &&
                                a.CommentMarkedDate >= Remark_fdate &&
                                a.CommentMarkedDate <= Remark_toDate
                                select new StudentRemark
                                {
                                    date = SqlFunctions.DateName("day", a.CommentMarkedDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.CommentMarkedDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.CommentMarkedDate),
                                    CommentFrom = "ProgressCard",
                                    Remark = a.Comment,
                                    Name = b.EmployeeName + " " + b.LastName
                                }).ToList();

                    return list;
                }
            }
            if (Status == "ExamSubjectRemark")
            {
                if (mark == "Mark")
                {
                    var list = (from a in db.StudentMarks
                                from b in db.TechEmployees
                                where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                               a.AcademicYearId == Remark_allAcademicYears &&
                    a.ClassId == classId &&
                    a.SectionId == secId &&
                    a.StudentRegisterId == studentRegId &&
                    a.RemarkDate >= Remark_fdate &&
                    a.RemarkDate <= Remark_toDate
                                select new StudentRemark
                                {
                                    date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                    CommentFrom = "Subject Mark",                                  
                                    Remark = a.Remark,
                                    Name = b.EmployeeName + " " + b.LastName
                                }).ToList();
                    return list;
                }
                else
                {
                    var list = (from a in db.StudentGrades
                                from b in db.TechEmployees
                                where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                                a.AcademicYearId == Remark_allAcademicYears &&
                                a.ClassId == classId &&
                                a.SectionId == secId &&
                                a.StudentRegisterId == studentRegId &&
                                a.CommentDate >= Remark_fdate &&
                                a.CommentDate <= Remark_toDate
                                select new StudentRemark
                                {
                                    date = SqlFunctions.DateName("day", a.CommentDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.CommentDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.CommentDate),
                                    CommentFrom = "Subject Mark",
                                    Remark = a.Comment,
                                    Name = b.EmployeeName + " " + b.LastName
                                }).ToList();

                    return list;
                }
            }
            else if (Status == "HomeworkRemark")
            {
                var list = (from a in db.HomeworkReplyFromStudents
                            from b in db.HomeWork
                            from c in db.TechEmployees
                            where c.EmployeeRegisterId == a.RemarkGivenBy &&
                            a.HomeWorkId == b.HomeWorkId &&
                            b.AcademicYearId == Remark_allAcademicYears &&
                            b.ClassId == classId &&
                            b.SectionId == secId &&
                            a.StudentRegisterId == studentRegId &&
                            a.RemarkDate >= Remark_fdate &&
                            a.RemarkDate <= Remark_toDate
                            select new StudentRemark
                            {
                                date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                CommentFrom = "Homework",
                                Remark = a.Remark,
                                Name = c.EmployeeName + " " + c.LastName
                            }).ToList();

                return list;
            }
            else if (Status == "AssignmentRemark")
            {
                var list = (from a in db.AssignmentReplyFromStudents
                            from b in db.Assignments
                            from c in db.TechEmployees
                            where c.EmployeeRegisterId == a.RemarkGivenBy &&
                            a.AssignmentId == b.AssignmentId &&
                            b.AcademicYearId == Remark_allAcademicYears &&
                            b.ClassId == classId &&
                            b.SectionId == secId &&
                            a.StudentRegisterId == studentRegId &&
                            a.RemarkDate >= Remark_fdate &&
                            a.RemarkDate <= Remark_toDate
                            select new StudentRemark
                            {
                                date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                CommentFrom = "Assignment",
                                Remark = a.Remark,
                                Name = c.EmployeeName + " " + c.LastName
                            }).ToList();

                return list;
            }
            else if (Status == "ELearningRemark")
            {
                var list = (from a in db.ELearningReplyFromStudents
                            from b in db.ELearnings
                            from c in db.TechEmployees
                            where c.EmployeeRegisterId == a.RemarkGivenBy &&
                            a.ELearningId == b.ELearningId &&
                            b.AcademicYearId == Remark_allAcademicYears &&
                            b.ClassId == classId &&
                            b.SectionId == secId &&
                            a.StudentRegisterId == studentRegId &&
                            a.RemarkDate >= Remark_fdate &&
                            a.RemarkDate <= Remark_toDate
                            select new StudentRemark
                            {
                                date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                CommentFrom = "E-Learning",
                                Remark = a.Remark,
                                Name = c.EmployeeName + " " + c.LastName
                            }).ToList();

                return list;
            }
            else
            {
                var list = ((from a in db.StudentDailyAttendances
                             from b in db.TechEmployees
                             where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                             a.AcademicYearId == Remark_allAcademicYears &&
                             a.ClassId == classId &&
                             a.SectionId == secId &&
                             a.StudentRegisterId == studentRegId &&
                             a.CurrentDate >= Remark_fdate &&
                             a.CurrentDate <= Remark_toDate
                             select new StudentRemark
                             {
                                 date = SqlFunctions.DateName("day", a.CurrentDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.CurrentDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.CurrentDate),
                                 CommentFrom = "Class Attendance",
                                 Remark = a.LeaveReason,
                                 Name = b.EmployeeName + " " + b.LastName
                             })
                      .Union(from a in db.MarkTotals
                             from b in db.TechEmployees
                             where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                             a.AcademicYearId == Remark_allAcademicYears &&
                             a.ClassId == classId &&
                             a.SectionId == secId &&
                             a.StudentRegisterId == studentRegId &&
                             a.MarkedDate >= Remark_fdate &&
                             a.MarkedDate <= Remark_toDate
                             select new StudentRemark
                             {
                                 date = SqlFunctions.DateName("day", a.MarkedDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.MarkedDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.MarkedDate),
                                 CommentFrom = "ProgressCard",
                                 Remark = a.Comment,
                                 Name = b.EmployeeName + " " + b.LastName
                             })
                              .Union(from a in db.GradeComments
                                     from b in db.TechEmployees
                                     where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                                     a.AcademicYearId == Remark_allAcademicYears &&
                                     a.ClassId == classId &&
                                     a.SectionId == secId &&
                                     a.StudentRegisterId == studentRegId &&
                                     a.CommentMarkedDate >= Remark_fdate &&
                                     a.CommentMarkedDate <= Remark_toDate
                                     select new StudentRemark
                                     {
                                         date = SqlFunctions.DateName("day", a.CommentMarkedDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.CommentMarkedDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.CommentMarkedDate),
                                         CommentFrom = "ProgressCard",                                        
                                         Remark = a.Comment,
                                         Name = b.EmployeeName + " " + b.LastName
                                     })
                                    .Union(from a in db.StudentMarks
                                           from b in db.TechEmployees
                                           where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                                           a.AcademicYearId == Remark_allAcademicYears &&
                                           a.ClassId == classId &&
                                           a.SectionId == secId &&
                                           a.StudentRegisterId == studentRegId &&
                                           a.RemarkDate >= Remark_fdate &&
                                           a.RemarkDate <= Remark_toDate
                                           select new StudentRemark
                                           {
                                               date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                               CommentFrom = "Subject Mark",
                                               Remark = a.Remark,
                                               Name = b.EmployeeName + " " + b.LastName
                                           })
                                    .Union(from a in db.StudentGrades
                                           from b in db.TechEmployees
                                           where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                                           a.AcademicYearId == Remark_allAcademicYears &&
                                           a.ClassId == classId &&
                                           a.SectionId == secId &&
                                           a.StudentRegisterId == studentRegId &&
                                           a.CommentDate >= Remark_fdate &&
                                           a.CommentDate <= Remark_toDate
                                           select new StudentRemark
                                           {
                                               date = SqlFunctions.DateName("day", a.CommentDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.CommentDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.CommentDate),
                                               CommentFrom = "Subject Mark",
                                               Remark = a.Comment,
                                               Name = b.EmployeeName + " " + b.LastName
                                           })
                                     .Union(from a in db.HomeworkReplyFromStudents
                                            from b in db.HomeWork
                                            from c in db.TechEmployees
                                            where c.EmployeeRegisterId == a.RemarkGivenBy &&
                                            a.HomeWorkId == b.HomeWorkId &&
                                            b.AcademicYearId == Remark_allAcademicYears &&
                                            b.ClassId == classId &&
                                            b.SectionId == secId &&
                                            a.StudentRegisterId == studentRegId &&
                                            a.RemarkDate >= Remark_fdate &&
                                            a.RemarkDate <= Remark_toDate
                                            select new StudentRemark
                                            {
                                                date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                                CommentFrom = "Homework",
                                                Remark = a.Remark,
                                                Name = c.EmployeeName + " " + c.LastName
                                            })
                                      .Union(from a in db.AssignmentReplyFromStudents
                                             from b in db.Assignments
                                             from c in db.TechEmployees
                                             where c.EmployeeRegisterId == a.RemarkGivenBy &&
                                             a.AssignmentId == b.AssignmentId &&
                                             b.AcademicYearId == Remark_allAcademicYears &&
                                             b.ClassId == classId &&
                                             b.SectionId == secId &&
                                             a.StudentRegisterId == studentRegId &&
                                             a.RemarkDate >= Remark_fdate &&
                                             a.RemarkDate <= Remark_toDate
                                             select new StudentRemark
                                             {
                                                 date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                                 CommentFrom = "Assignment",
                                                 Remark = a.Remark,
                                                 Name = c.EmployeeName + " " + c.LastName
                                             })
                                        .Union(from a in db.ELearningReplyFromStudents
                                               from b in db.ELearnings
                                               from c in db.TechEmployees
                                               where c.EmployeeRegisterId == a.RemarkGivenBy &&
                                               a.ELearningId == b.ELearningId &&
                                               b.AcademicYearId == Remark_allAcademicYears &&
                                               b.ClassId == classId &&
                                               b.SectionId == secId &&
                                               a.StudentRegisterId == studentRegId &&
                                               a.RemarkDate >= Remark_fdate &&
                                               a.RemarkDate <= Remark_toDate
                                               select new StudentRemark
                                               {
                                                   date = SqlFunctions.DateName("day", a.RemarkDate).Trim() + "-" + SqlFunctions.StringConvert((double)a.RemarkDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.RemarkDate),
                                                   CommentFrom = "E-Learning",
                                                   Remark = a.Remark,
                                                   Name = c.EmployeeName + " " + c.LastName
                                               })).ToList();

                return list;
            }

        }
        //jegadeesh
        public List<Ranklist> GetStudentDailyAttendance(int yid, int cid, int sid, DateTime? da)
        {
            var ans = (from a in db.StudentDailyAttendances
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid && c.AcademicYearId == yid && c.ClassId == cid && c.SectionId == sid && d.AcademicYearId == yid && d.ClassId == cid && d.SectionId == sid && a.ClassId == cid && a.SectionId == sid && a.CurrentDate == da && b.StudentRegisterId == a.StudentRegisterId && a.StudentRegisterId == c.StudentRegisterId && c.Status == true && d.StudentRegisterId == b.StudentRegisterId  && d.HaveRollNumber == true
                       select new Ranklist
                       {
                           rollnumber = c.RollNumber,
                           stuname = b.FirstName + " " + b.LastName,
                           status = a.AttendanceStatus
                       }).OrderBy(x => x.rollnumber).ToList();
            return ans;
        }
        public List<StudentDailyAttendance> GetStudentDailyPresentStatus(int yid, int cid, int sid, DateTime da)
        {
            var ans = (from a in db.StudentDailyAttendances

                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.CurrentDate == da && a.AttendanceStatus == "P"
                       select a).ToList();
            return ans;
        }
        public List<StudentDailyAttendance> GetStudentDailyAbsentStatus(int yid, int cid, int sid, DateTime da)
        {
            var ans = (from a in db.StudentDailyAttendances

                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.CurrentDate == da && a.AttendanceStatus == "A"
                       select a).ToList();
            return ans;
        }
        public List<Ranklist> GetStudentFromToDateAttendance(int yid, int cid, int sid, DateTime? from1, DateTime? to)
        {
            var ans = (from a in db.StudentDailyAttendances
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid && c.AcademicYearId == yid && c.ClassId == cid && c.SectionId == sid && d.AcademicYearId == yid && d.ClassId == cid && d.SectionId == sid && a.ClassId == cid && a.SectionId == sid && a.CurrentDate >= from1 && a.CurrentDate <= to && b.StudentRegisterId == a.StudentRegisterId && a.StudentRegisterId == c.StudentRegisterId && c.Status == true && d.StudentRegisterId == b.StudentRegisterId && d.HaveRollNumber == true
                       select new Ranklist
                       {
                           rollnumber = c.RollNumber,
                           stuname = b.FirstName + " " + b.LastName,
                           date = a.CurrentDate.Value,
                           status = a.AttendanceStatus
                       }).OrderBy(q => q.date).ToList();
            return ans;
        }
        public List<StudentDailyAttendance> GetStudentFromToDatePresentStatus(int yid, int cid, int sid, DateTime from1, DateTime to, string r)
        {
            var ans = (from a in db.StudentDailyAttendances
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && c.RollNumber == r && b.StudentRegisterId == c.StudentRegisterId && a.StudentRegisterId == b.StudentRegisterId && a.AttendanceStatus == "P" && a.CurrentDate >= from1 && a.CurrentDate <= to && d.StudentRegisterId == b.StudentRegisterId && d.Status == true && d.HaveRollNumber == true
                       select a).ToList();
            return ans;

        }
        public List<Ranklist> GetStudentMonthAttendance(int yid, int cid, int sid, int? id)
        {
            var ans = (from a in db.StudentDailyAttendances
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid && c.AcademicYearId == yid && c.ClassId == cid && c.SectionId == sid && d.AcademicYearId == yid && d.ClassId == cid && d.SectionId == sid && a.ClassId == cid && a.SectionId == sid && a.CurrentDate.Value.Month == id && b.StudentRegisterId == a.StudentRegisterId && a.StudentRegisterId == c.StudentRegisterId && c.Status == true && d.StudentRegisterId == b.StudentRegisterId && d.HaveRollNumber == true
                       select new Ranklist
                       {
                           rollnumber = c.RollNumber,
                           stuname = b.FirstName + " " + b.LastName,
                           date = a.CurrentDate.Value,
                           status = a.AttendanceStatus,
                           d = SqlFunctions.DateName("day", a.CurrentDate.Value).Trim()
                       }).OrderBy(q => q.date).ToList();
            return ans;
        }
        public List<StudentDailyAttendance> GetStudentMonthPresentStatus(int yid, int cid, int sid, int id, string r)
        {
            var ans = (from a in db.StudentDailyAttendances
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && c.RollNumber == r && b.StudentRegisterId == c.StudentRegisterId && a.StudentRegisterId == b.StudentRegisterId && a.AttendanceStatus == "P" && a.CurrentDate.Value.Month == id && d.StudentRegisterId == b.StudentRegisterId && d.Status == true && d.HaveRollNumber == true

                       select a).ToList();
            return ans;
        }

        // individual student Attendance Details

        public List<Ranklist> GetParticularStudentDailyAttendance(int yid, int cid, int sid, DateTime? da, int stu_id)
        {
            var ans = (from a in db.StudentDailyAttendances
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.CurrentDate == da && a.StudentRegisterId == stu_id && b.StudentRegisterId == a.StudentRegisterId && a.StudentRegisterId == c.StudentRegisterId && c.Status == true && d.StudentRegisterId == b.StudentRegisterId && d.Status == true && d.HaveRollNumber == true
                       select new Ranklist
                       {
                           rollnumber = c.RollNumber,
                           stuname = b.FirstName + " " + b.LastName,
                           status = a.AttendanceStatus
                       }).ToList();
            return ans;

        }
        public List<StudentDailyAttendance> GetParticularStudentDailyPresentStatus(int yid, int cid, int sid, DateTime da, int stu_id)
        {
            var ans = (from a in db.StudentDailyAttendances

                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.CurrentDate == da && a.AttendanceStatus == "P" && a.StudentRegisterId == stu_id
                       select a).ToList();
            return ans;
        }
        public List<StudentDailyAttendance> GetParticularStudentDailyAbsentStatus(int yid, int cid, int sid, DateTime da, int stu_id)
        {
            var ans = (from a in db.StudentDailyAttendances
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.CurrentDate == da && a.AttendanceStatus == "A" && a.StudentRegisterId == stu_id
                       select a).ToList();
            return ans;
        }
        public List<Ranklist> GetParticularStudentFromToDateAttendance(int yid, int cid, int sid, DateTime? from1, DateTime? to, int stu_id)
        {
            var ans = (from a in db.StudentDailyAttendances
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.CurrentDate >= from1 && a.CurrentDate <= to && a.StudentRegisterId == stu_id && b.StudentRegisterId == a.StudentRegisterId && a.StudentRegisterId == c.StudentRegisterId && c.Status == true && d.StudentRegisterId == b.StudentRegisterId && d.Status == true && d.HaveRollNumber == true
                       select new Ranklist
                       {
                           rollnumber = c.RollNumber,
                           stuname = b.FirstName + " " + b.LastName,
                           date = a.CurrentDate.Value,
                           status = a.AttendanceStatus
                       }).OrderBy(q => q.date).ToList();
            return ans;
        }
        public List<StudentDailyAttendance> GetParticularStudentFromToDatePresentStatus(int yid, int cid, int sid, DateTime? from1, DateTime? to, string r, int stu_id)
        {
            var ans = (from a in db.StudentDailyAttendances
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && c.RollNumber == r && b.StudentRegisterId == c.StudentRegisterId && a.StudentRegisterId == stu_id && a.StudentRegisterId == b.StudentRegisterId && a.AttendanceStatus == "P" && a.CurrentDate >= from1 && a.CurrentDate <= to && c.Status == true && d.StudentRegisterId == b.StudentRegisterId && d.Status == true && d.HaveRollNumber == true
                       select a).ToList();
            return ans;

        }
        public List<Ranklist> GetParticularStudentMonthAttendance(int yid, int cid, int sid, int? id, int stu_id)
        {
            var ans = (from a in db.StudentDailyAttendances
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.CurrentDate.Value.Month == id && a.StudentRegisterId == stu_id && b.StudentRegisterId == a.StudentRegisterId && a.StudentRegisterId == c.StudentRegisterId && c.Status == true && d.StudentRegisterId == b.StudentRegisterId && d.Status == true && d.HaveRollNumber == true
                       select new Ranklist
                       {
                           rollnumber = c.RollNumber,
                           stuname = b.FirstName + " " + b.LastName,
                           date = a.CurrentDate.Value,
                           status = a.AttendanceStatus,
                           d = SqlFunctions.DateName("day", a.CurrentDate.Value).Trim()
                       }).OrderBy(q => q.date).ToList();
            return ans;
        }
        public List<StudentDailyAttendance> GetParticularStudentMonthPresentStatus(int yid, int cid, int sid, int? id, string r, int stu_id)
        {
            var ans = (from a in db.StudentDailyAttendances
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && c.RollNumber == r && b.StudentRegisterId == c.StudentRegisterId && a.StudentRegisterId == stu_id && a.StudentRegisterId == b.StudentRegisterId && a.AttendanceStatus == "P" && a.CurrentDate.Value.Month == id && c.Status == true && d.StudentRegisterId == b.StudentRegisterId && d.Status == true && d.HaveRollNumber == true
                       select a).ToList();
            return ans;

        }




        public object getAttendanceMonth(int yearId, int classId, int secId, int studentRegId)
        {
            var getAttendanceMonth = (from a in db.StudentDailyAttendances
                                      where a.AcademicYearId == yearId &&
                                      a.ClassId == classId &&
                                      a.SectionId == secId
                                      select new
                                      {
                                          month = a.CurrentDate.Value.Month,
                                          year = a.CurrentDate.Value.Year,
                                      }).OrderBy(m => m.month).Distinct().ToList().OrderBy(x=>x.year).ToList(); 

            ArrayList month1 = new ArrayList();
            ArrayList monthName = new ArrayList();
            ArrayList presentDaysCount = new ArrayList();
            ArrayList NumberOfWorkingDays = new ArrayList();

            var totalMonthCount = getAttendanceMonth.Count;

            for (int i = 0; i < getAttendanceMonth.Count; i++)
            {
                var month = getAttendanceMonth[i].month;
                var year =  getAttendanceMonth[i].year;
                //var month_Name = getAttendanceMonth[i].monthname.Value.ToString("MMM");
                //string month_Name="";

                //if(month == 1)
                //{
                //    month_Name = "Jan";
                //}
                string month_Name = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month).Substring(0, 3) + "/" + year;
                StudentDailyAttendanceServices a = new StudentDailyAttendanceServices();
                var getPresentDays = a.getPresentDays(yearId, classId, secId, month, studentRegId);
                var getWorkingDays = a.getWorkingDaysbyMonth(yearId, classId, secId, month);
                int TotalWorkingDays = getWorkingDays;
                presentDaysCount.Add(getPresentDays.Count);
                month1.Add(month);
                monthName.Add(month_Name);
                NumberOfWorkingDays.Add(TotalWorkingDays);
            }

            return new { monthName, presentDaysCount, NumberOfWorkingDays, totalMonthCount };
        }

        public List<StudentDailyAttendance> getPresentDays(int yearId, int classId, int secId, int month, int studentRegId)
        {
            var getPresentDays = (from a in db.StudentDailyAttendances
                                  where a.AcademicYearId == yearId &&
                                  a.ClassId == classId &&
                                  a.SectionId == secId &&
                                  a.StudentRegisterId == studentRegId &&
                                  a.CurrentDate.Value.Month == month &&
                                  a.AttendanceStatus == "P"
                                  select a).ToList();
            return getPresentDays;
        }

        public int getWorkingDaysbyMonth(int? yearId, int? classId, int? secId, int month)
        {
            var getWorkingDays = (from a in db.StudentDailyAttendances
                                  from b in db.Students
                                  where a.StudentRegisterId == b.StudentRegisterId &&
                                      a.AcademicYearId == yearId &&
                                      a.ClassId == classId &&
                                      a.SectionId == secId &&
                                      a.CurrentDate.Value.Month == month &&
                                      b.StudentStatus == true
                                  select new
                                  {
                                      date = a.CurrentDate
                                  }).Distinct().ToList().Count;
            return getWorkingDays;
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}