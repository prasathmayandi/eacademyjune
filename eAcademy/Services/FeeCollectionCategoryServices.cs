﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class FeeCollectionCategoryServices:IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public FeeCollectionCategory checkDataExist(int feeCollectionId, int year, int classId)
        {
            var getRow = (from a in db.FeeCollectionCategories where a.FeeCollectionId == feeCollectionId && a.AcademicYearId == year && a.ClassId == classId select a).FirstOrDefault();
            return getRow;
        }
        public void updateSectionWithRegId(int feeCollectionId, int year, int classId, int studentRegisterId, int sectionId)
        {
            var getList = (from a in db.FeeCollectionCategories where a.FeeCollectionId == feeCollectionId && a.AcademicYearId == year && a.ClassId == classId select a).ToList();
            getList.ForEach(a =>
                   {
                       a.StudentRegId = studentRegisterId;
                       a.SectionId = sectionId;
                   });
            db.SaveChanges();        
        }

        public List<getclassandsection> GetAnnualFeePaidList(int year, int classs, int sec, int paymenttype, int feeCategory)
        {
            var ans=(from a in db.FeeCollectionCategories
                         from b in db.FeeCategories
                         from c in db.Students
                         from d in db.StudentRollNumbers
                         from e in db.AssignClassToStudents
                         where a.AcademicYearId==year && a.ClassId==classs && a.SectionId==sec && a.PaymentTypeId==0 && a.FeeCategoryId ==feeCategory && b.FeeCategoryId==a.FeeCategoryId &&c.StudentRegisterId==a.StudentRegId && d.StudentRegisterId==c.StudentRegisterId && d.Status == true && e.StudentRegisterId == c.StudentRegisterId && e.Status ==true && e.HaveRollNumber ==true
                         select new getclassandsection{
                             StudentId=c.StudentId,
                         StudentName=c.FirstName+" "+c.LastName,
                        rollnumber=d.RollNumber,
                         StudentRegId=c.StudentRegisterId
                         
                         }).Distinct().ToList();
            return ans;
        }
        public List<getclassandsection> GetTermFeePaidList(int year, int classs, int sec, int paymenttype, int feeCategory, int TermId)
        {

            var ans = (from a in db.FeeCollectionCategories
                       from b in db.FeeCategories
                       from c in db.Students
                       from d in db.StudentRollNumbers
                       from e in db.AssignClassToStudents
                       where a.AcademicYearId == year && a.ClassId == classs && a.SectionId == sec && a.PaymentTypeId == TermId && a.FeeCategoryId == feeCategory && b.FeeCategoryId == a.FeeCategoryId && c.StudentRegisterId == a.StudentRegId && d.StudentRegisterId == c.StudentRegisterId && d.Status == true && e.StudentRegisterId == c.StudentRegisterId && e.Status == true && e.HaveRollNumber == true
                       select new getclassandsection
                       {
                           StudentId=c.StudentId,
                           StudentName = c.FirstName + " " + c.LastName,
                           rollnumber = d.RollNumber,
                           StudentRegId = c.StudentRegisterId

                       }).Distinct().ToList();
            return ans;
        }

        public void UpdateStudentDetails(int applicationId, int studentregisterid, int academicyearid, int classid)
        {
            var getrow1 = db.FeeCollectionCategories.Where(q => q.ApplicationId == applicationId).ToList();
            if (getrow1.Count != 0)
            {
                foreach (var v in getrow1)
                {
                 if(v.AcademicYearId == academicyearid)
                 {
                     v.StudentRegId = studentregisterid;
                     db.SaveChanges();
                 }
                }
            }
        }

        public List<M_StudentRegisteration> GetParticularClassSectionPeesDueList(int aid, int cid, int sid, int pid, int fid ,int tid)
        {
            var ans = (from a in db.AssignClassToStudents 
                       from b in db.Students
                       from d in db.StudentRollNumbers
                       where a.AcademicYearId == aid && a.ClassId == cid && a.SectionId == sid && a.Status == true && a.HaveRollNumber == true && a.StudentRegisterId == b.StudentRegisterId && d.StudentRegisterId == a.StudentRegisterId && d.AcademicYearId ==a.AcademicYearId && d.ClassId == a.ClassId && d.SectionId == a.SectionId && d.Status == true
                       && !(from c in db.FeeCollectionCategories
                                                   where c.ClassId == a.ClassId &&
                                                  c.SectionId == a.SectionId && c.StudentRegId == a.StudentRegisterId &&
                                                   c.ClassId == cid && c.PaymentTypeId == 0 && c.FeeCategoryId == fid 
                                                   select c.StudentRegId).Contains(a.StudentRegisterId) && 
                                                   !(from e in db.FeeCollectionCategories
                                 where e.ClassId == a.ClassId &&
                                e.SectionId == a.SectionId && e.StudentRegId == a.StudentRegisterId &&
                                 e.ClassId == cid && e.PaymentTypeId == pid && e.FeeCategoryId == fid
                                 select e.StudentRegId)
                                 .Contains(a.StudentRegisterId)
                       select new M_StudentRegisteration { StudentRollNumber = d.RollNumber, StuFirstName = b.FirstName + " " + b.LastName, StudentRegisterId =a.StudentRegisterId }).OrderBy(q=>q.StudentRollNumber).ToList();
            return ans;
        }
        public List<M_StudentRegisteration> GetParticularClassSectionPeesPaidList(int aid, int cid, int sid, int pid, int fid, int tid)
        {
     
            var ans1 =(from a in db.AssignClassToStudents 
                       from b in db.Students
                       from d in db.StudentRollNumbers
                       from c in db.FeeCollectionCategories
                       where  a.AcademicYearId == aid && a.ClassId == cid && a.SectionId == sid && a.Status == true && a.HaveRollNumber == true && a.StudentRegisterId == b.StudentRegisterId && d.StudentRegisterId == a.StudentRegisterId && d.AcademicYearId == a.AcademicYearId && d.ClassId == a.ClassId && d.SectionId == a.SectionId && d.Status == true   &&
                       c.ClassId == a.ClassId &&
                           c.SectionId == a.SectionId && c.StudentRegId == a.StudentRegisterId &&
                            c.ClassId == cid && c.PaymentTypeId == pid && c.FeeCategoryId == fid
                       select new M_StudentRegisteration { StudentRollNumber = d.RollNumber, StuFirstName = b.FirstName + " " + b.LastName, StudentRegisterId = a.StudentRegisterId }).OrderBy(q => q.StudentRollNumber).ToList();
            return ans1;
        }
     
        public RefundFeesConfig GetFeeCollectionDetails(int collectid)
        {
            var ans = (from a in db.FeeCollectionCategories
                       from b in db.Fees
                       from c in db.FeeCategories
                       where a.CollectionId == collectid && b.AcademicYearId == a.AcademicYearId && b.ClassId == a.ClassId && b.FeeCategoryId == a.FeeCategoryId && c.FeeCategoryId == a.FeeCategoryId
                       select new RefundFeesConfig
                       {
                           FeeCategoryname = c.FeeCategoryName,
                           CollectedAmount = a.TotalAmount,
                           RefundAmount = b.ReFundAmount,
                           FeeId =b.FeeId
                       }
                         ).FirstOrDefault();
            return ans;
        }
        public Decimal? Existmanagementrefund(int collectid,Decimal? refundamt,int feeid)
        {
            Decimal? amt = 0;
            var ans1 = db.FeesRefundToStudents.Where(q => q.CollectionId == collectid && q.RefundAmount == refundamt && q.FeeId == feeid).FirstOrDefault();
            if (ans1 == null)
            {
                amt = refundamt;
            }
            else
            {
                var ans2 = db.FeesRefundToStudents.Where(q => q.CollectionId == collectid && q.FeeId == feeid).FirstOrDefault();
                if (ans2 != null)
                {
                    amt = refundamt - Convert.ToDecimal(ans2.RefundAmount);
                }
              
            }
           
            return amt;
        }
        public Decimal? AlreadyRefundAmount(int collectid)
        {
            Decimal? amt = 0;
                
                var ans = (from a in db.FeesRefundToStudents

                           where a.CollectionId == collectid
                           select new RefundFeesConfig
                           {
                               CollectedAmount = a.RefundAmount,

                           }
                             ).ToList();
                if (ans.Count > 0)
                {
                    foreach (var b in ans)
                    {
                        amt = amt + b.CollectedAmount;

                    }
                }

            return amt;
        }
        public int FeesRefundupdate(int collectionid,Decimal Existcollectamt,Decimal refundamt,Decimal netamount,string reason,string receivedby,int empregid,int feeid)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            int FeeRefundId = 0;
           
            var Getrow = db.FeeCollectionCategories.Where(q => q.CollectionId == collectionid).FirstOrDefault();
            if(Getrow != null)
            {
                Decimal? oldrefundamt =Convert.ToDecimal( Getrow.ReFundAmount);
                Getrow.ReFundAmount = oldrefundamt+refundamt;
                Getrow.TotalAmount = netamount;
                Getrow.EmployeeRegisterId = empregid;
                var Getfeecollection = db.FeeCollections.Where(q => q.FeeCollectionId == Getrow.FeeCollectionId).FirstOrDefault();
                if(Getfeecollection != null)
                {
                    Decimal? tot=Convert.ToDecimal( Getfeecollection.AmountCollected);
                    Getfeecollection.AmountCollected = tot - refundamt;
                    Getfeecollection.EmployeeRegisterId = empregid;
                    db.SaveChanges();
                }

                if (feeid == 0)
                {
                    FeesRefundToStudent rre = new FeesRefundToStudent()
                    {
                        CollectionId = collectionid,
                        StudentRegisterId = Getrow.StudentRegId,
                        LastAmount = Existcollectamt,
                        RefundAmount = refundamt,
                        NetAmount = netamount,
                        Reason = reason,
                        IssuedBy = receivedby,
                        EmployeeRegisterId = empregid,
                        DateOfRefund = date,
                        Status = true
                    };
                    db.FeesRefundToStudents.Add(rre);
                    db.SaveChanges();
                    FeeRefundId = rre.StudentFeeRefundId;
                }
                else
                {
                    FeesRefundToStudent re = new FeesRefundToStudent()
                    {
                        CollectionId = collectionid,
                        StudentRegisterId = Getrow.StudentRegId,
                        LastAmount = Existcollectamt,
                        RefundAmount = refundamt,
                        NetAmount = netamount,
                        Reason = reason,
                        IssuedBy = receivedby,
                        EmployeeRegisterId = empregid,
                        DateOfRefund = date,
                        FeeId = feeid,
                        Status = true
                    };
                    db.FeesRefundToStudents.Add(re);
                    db.SaveChanges();
                    FeeRefundId = re.StudentFeeRefundId;
                }
            }
            db.SaveChanges();
            return FeeRefundId;
       }

        public List<FeeStructure> GetAdmissionpaidFees(int AdmissionId)
        {
            var result = (from a in db.FeeCollectionCategories
                          from b in db.FeeCategories
                          where a.ApplicationId == AdmissionId && b.FeeCategoryId == a.FeeCategoryId
                          select new FeeStructure
                          {
                              CollectedFeeCatId = a.CollectionId,
                              FeeCategory = b.FeeCategoryName,
                              Amount = a.Amount,
                              ServicesTax = a.ServicesTax,
                              Total = a.TotalAmount,
                              FeecollectionId = a.FeeCollectionId
                          }
                        ).ToList();
            return result;
        }
        
        public void AdmissionFessRefundtostudent(int collectid,int empregid,string receivedby,string reason)
        {
            DateTime date = DateTimeByZone.getCurrentDate();
            var check = db.FeeCollectionCategories.Where(q => q.CollectionId == collectid).FirstOrDefault();
            if(check != null)
            {
                check.ReFundAmount = check.TotalAmount;
                check.TotalAmount = 0;
                check.EmployeeRegisterId = empregid;
                check.ReceivedBy = receivedby;
                check.Reason = reason;
                check.RefundDate = date;
                db.SaveChanges();
            }

        }
        public bool CheckAlreadyrefundAdmissionFee(int AdmissionId)
        {
            var check = db.Students.Where(q => q.AdmissionId == AdmissionId && q.StudentStatus == false && q.Statusflag == "FeesRefund").FirstOrDefault();
            if(check!= null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool CheckAlreadyrefundPreAdmissionFee(int AdmissionId)
        {
            var check = db.PreAdmissionStudentRegisters.Where(q => q.StudentAdmissionId == AdmissionId && q.Statusflag == "Refunded_beforeassignclass").FirstOrDefault();
            if (check != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
      
        public List<FeeStructure> GetFeeCollectionCategory(int CollectId)
        {
            var ans = (
                    (from a in db.FeeCollections
                    from b in db.FeeCollectionCategories
                    from c in db.FeeCategories
                     where a.FeeCollectionId == CollectId && b.FeeCollectionId ==a.FeeCollectionId && c.FeeCategoryId == b.FeeCategoryId && b.PaymentTypeId == 0
                     select new FeeStructure
                     {
                        FeeCategory=c.FeeCategoryName,
                       Amount=b.Amount,
                       ServicesTax=b.ServicesTax,
                       Total=b.TotalAmount
                     })
                .Union
                    (

                   from a in db.FeeCollections
                   from b in db.FeeCollectionCategories
                   from c in db.FeeCategories
                     from d in db.Terms
                   where a.FeeCollectionId == CollectId && b.FeeCollectionId == a.FeeCollectionId && c.FeeCategoryId == b.FeeCategoryId && b.PaymentTypeId == d.TermId
                     select new FeeStructure
                     {
                         FeeCategory = c.FeeCategoryName +" "+d.TermName,
                         Amount = b.Amount,
                         ServicesTax = b.ServicesTax,
                         Total = b.TotalAmount
                     })
                ).Distinct().OrderBy(q=>q.FeeCategory).ToList();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}