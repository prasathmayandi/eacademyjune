﻿using eAcademy.Areas.Communication.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class AssignClassToStudentServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public AssignClassToStudent getAssigendClass(int StudentRegId)
        {
            var getRow = (from a in db.AssignClassToStudents where a.StudentRegisterId == StudentRegId && a.Status == true select a).FirstOrDefault();
            return getRow;
        }

        public AssignClassToStudent getAssigendClass(int StudentRegId, int yearId)
        {
            var getRow = (from a in db.AssignClassToStudents where a.StudentRegisterId == StudentRegId && a.AcademicYearId == yearId && a.Status == true select a).FirstOrDefault();
            return getRow;
        }
        public AssignClassToStudent checkConductStatusExist(int StudentRegId)
        {
            var getRow = (from a in db.AssignClassToStudents where a.StudentRegisterId == StudentRegId && a.Status == true select a).FirstOrDefault();
            return getRow;
        }
        public AssignClassToStudent StudentCurrentlyStudyingSelectedClass(int year,int classId,int secId,int StudentRegId)
        {
            var getRow = (from a in db.AssignClassToStudents where a.AcademicYearId == year && a.ClassId == classId && a.SectionId == secId && a.StudentRegisterId == StudentRegId && a.Status == true select a).FirstOrDefault();
            return getRow;
        }
        public void UpdateConductStatus(int StudentRegId, string conductStatus, string fileName)
        {
            var getRow = (from a in db.AssignClassToStudents where a.StudentRegisterId == StudentRegId && a.Status == true select a).FirstOrDefault();
            getRow.ConductStatus = conductStatus;
            getRow.ConductCertificate = fileName;
            db.SaveChanges();
        }
        public AssignClassToStudent checkAcademicYearResult(int? AcademicYears, int? ClassId, int secId)
        {
            var getRow = (from a in db.AssignClassToStudents where a.AcademicYearId == AcademicYears && a.ClassId == ClassId && a.SectionId == secId && a.Result != null select a).FirstOrDefault();
            return getRow;
        }
        public AssignClassToStudent checkAcademicYearResult(int? AcademicYears, int? ClassId)
        {
            var getRow = (from a in db.AssignClassToStudents where a.AcademicYearId == AcademicYears && a.ClassId == ClassId  && a.Result != null select a).FirstOrDefault();
            return getRow;
        }
        public IEnumerable<Object> getStudentListToMarkAttendance(int passYearId, int passClassId, int passSecId)
        {
            var studentList = (from a in db.Students
                               from b in db.AssignClassToStudents
                               where b.StudentRegisterId == a.StudentRegisterId &&
                                     b.AcademicYearId == passYearId &&
                                     b.ClassId == passClassId &&
                                     b.SectionId == passSecId &&
                                     a.StudentStatus == true &&
                                     b.Status == true
                               select new
                               {
                                   StudentRegId = a.StudentRegisterId,
                                   FName = a.FirstName,
                                   LName = a.LastName
                               }).ToList().Select(c => new
                               {
                                   StudentRegId = c.StudentRegId,
                                   StudentName = c.FName + " " + c.LName,
                                   Student_RegId = QSCrypt.Encrypt(c.StudentRegId.ToString())
                               }).ToList();

            return studentList;
        }

        public IEnumerable<Object> getStudents(int passSectionId)
        {
            var StudentList = (from a in db.AssignClassToStudents
                               from b in db.Students
                               where a.SectionId == passSectionId &&
                                     a.StudentRegisterId == b.StudentRegisterId &&
                                     b.StudentStatus == true
                               select new
                               {
                                   StudentRegId = a.StudentRegisterId,
                                   StudentName = b.FirstName +" "+b.LastName
                               }).ToList();
            return StudentList;
        }

        public IEnumerable<Object> getStudents(int passYearId, int passClassId, int passSecId)
        {
            var studentList = (from a in db.Students
                               from b in db.AssignClassToStudents
                               //from c in db.StudentRollNumbers
                               where b.StudentRegisterId == a.StudentRegisterId && 
                                     b.AcademicYearId == passYearId &&
                                     b.ClassId == passClassId &&
                                     b.SectionId == passSecId && a.StudentStatus==true
                                    // b.Status ==true &&
                                    // b.HaveRollNumber ==true
                               select new
                               {
                                   id = a.StudentRegisterId,
                                   name = a.FirstName+" "+a.LastName
                               }).ToList();

            return studentList;
        }

        public IEnumerable<Object> getfeepaidStudents(int passYearId, int passClassId, int passSecId)
        {
            var StudentList = (from a in db.Students
                               from b in db.TransportFeesCollections
                               where a.StudentRegisterId == b.StudentRegisterId
                               && b.SectionId==passSecId && b.ClassId==passClassId&& b.AcademicYearId==passYearId
                               select new
                               {
                                   id = b.StudentRegisterId,
                                   name = a.FirstName + " " + a.LastName
                               }).ToList();

            return StudentList;
        }

        public IEnumerable<Object> getStudents1(int passYearId, int classId, int secId)
        {
            var StudentList = (from a in db.AssignClassToStudents
                               from b in db.Students
                               where a.SectionId == secId &&
                                     a.ClassId == classId &&
                                     a.AcademicYearId == passYearId &&
                                     a.StudentRegisterId == b.StudentRegisterId 
                                     //b.StudentStatus == true &&
                                     //a.Status == true
                               select new
                               {
                                   StudentRegId = a.StudentRegisterId,
                                   StudentName = b.FirstName+" "+b.LastName 
                               }).ToList();

            return StudentList;
        }
        public IEnumerable<Object> getSecStudents(int passSecId)
        {
            var StudentList = (from a in db.Students
                               from b in db.AssignClassToStudents
                               where a.StudentRegisterId == b.StudentRegisterId &&
                               b.SectionId == passSecId &&
                               a.StudentStatus == true
                               select new
                               {
                                   StudentName = a.FirstName + " " + a.LastName,
                                   StudentId = a.StudentRegisterId
                               }).ToList().AsEnumerable();
            return StudentList;
        }

        public List<GetClassTeacher> getClassTeacherStudents(int? yearId, int? classId, int? secId)
        {
            var getStudents = (from a in db.AssignClassToStudents
                               where a.AcademicYearId == yearId &&
                               a.ClassId == classId &&
                               a.SectionId == secId &&
                               a.Status == true
                               select new GetClassTeacher
                               {
                                   stuRegId = a.StudentRegisterId
                               }).ToList();
            return getStudents;
        }

        public List<GradeProgressCard1> getClassTeacherStudentListForGrade(int? yearId, int? classId, int? secId)
        {
            var getStudents = (from a in db.StudentGrades
                               from b in db.Students
                               from c in db.StudentRollNumbers
                               where a.StudentRegisterId == b.StudentRegisterId &&
                               a.StudentRegisterId == c.StudentRegisterId &&
                               a.AcademicYearId == c.AcademicYearId &&
                               a.ClassId == c.ClassId &&
                               a.SectionId == c.SectionId &&
                               a.AcademicYearId == yearId &&
                               a.ClassId == classId &&
                               a.SectionId == secId 
                               select new 
                               {
                                   stuRegId = a.StudentRegisterId,
                                   stuRollNum = c.RollNumber,
                                   stuName = b.FirstName+" "+b.LastName
                               }).ToList().Distinct().Select(a => new GradeProgressCard1
                               {
                                   stuRegId = a.stuRegId,
                                   stuRollNum = a.stuRollNum,
                                   stuName = a.stuName,
                                   stu_RegId = QSCrypt.Encrypt(a.stuRegId.ToString())
                               }).ToList();
            return getStudents;
        }

        public List<ClassInchargeStudentsList> getClassInchargeStudents(int? yearId, int classId, int secId)
        {
            var list = (from a in db.AssignClassToStudents
                        from b in db.Students
                        from c in db.StudentRollNumbers
                        where   a.StudentRegisterId == b.StudentRegisterId &&
                                 a.StudentRegisterId == c.StudentRegisterId &&
                                 //b.StudentStatus == true &&
                                 //c.Status == true &&
                                 a.AcademicYearId == yearId &&
                                 a.ClassId == classId &&
                                 a.SectionId == secId &&
                                 c.AcademicYearId == yearId &&
                                 c.ClassId == classId &&
                                 c.SectionId == secId
                        select new
                        {
                            RollNumber = c.RollNumber,
                            FirstName = b.FirstName,
                            LastName = b.LastName,
                            Gender = b.Gender,
                            EmergencyContactPerson = b.EmergencyContactPerson,
                            EmergencyContactNumber = b.EmergencyContactNumber,
                            Hostel = a.HostelPreference,
                            Address1 = b.LocalAddress1,
                            Address2 = b.LocalAddress2,
                            City = b.LocalCity,
                            State = b.LocalState,
                            Country = b.LocalCountry,
                            StudentRegId = b.StudentRegisterId
                        }).ToList().Select(c => new ClassInchargeStudentsList
                        {
                            RollNumber = c.RollNumber,
                            StudentName = c.FirstName + " " + c.LastName,
                            Gender = c.Gender,
                            EmergencyContactPerson = c.EmergencyContactPerson,
                            EmergencyContactNumber = c.EmergencyContactNumber,
                            Address = c.Address1+c.Address2 + c.City + c.State + c.Country,
                            Student_RegId = QSCrypt.Encrypt(c.StudentRegId.ToString())
                        }).ToList();

            return list;
        }

        public IEnumerable<Object> getStudentListToGenerate_CC(int passYearId, int passClassId, int passSecId)
        {
            var studentList = (from a in db.Students
                               from b in db.AssignClassToStudents
                               from c in db.StudentRollNumbers
                               where b.StudentRegisterId == a.StudentRegisterId &&
                                     b.StudentRegisterId == c.StudentRegisterId &&
                                     b.AcademicYearId == passYearId &&
                                     b.ClassId == passClassId &&
                                     b.SectionId == passSecId &&
                                     c.AcademicYearId == passYearId &&
                                     c.ClassId == passClassId &&
                                     c.SectionId == passSecId 
                                    // a.StudentStatus == true
                               select new
                               {
                                   admissionId = a.AdmissionId,
                                   StudentRegId = a.StudentRegisterId,
                                   studentId = a.StudentId,
                                   RollNumber = c.RollNumber,
                                   FirstName = a.FirstName,
                                   LastName = a.LastName,
                                   Gender = a.Gender,
                                   conduct = b.ConductStatus,
                                   transfer = b.TransferStatus,
                                   status = b.SeparationStatus,
                                   CC = b.ConductCertificate,
                                   TC = b.TransferCertificate

                               }).ToList().Select(c => new
                               {
                                   AdmissionId = c.admissionId,
                                   StudentRegId = c.StudentRegId,
                                   StudentId = c.studentId,
                                   RollNumber = c.RollNumber,
                                   FirstName = c.FirstName,
                                   LastName = c.LastName,
                                   Gender = c.Gender,
                                   Conduct = c.conduct,
                                   Transfer = c.transfer,
                                   Status = c.status,
                                   CC = c.CC,
                                   TC = c.TC,
                                   Student_RegId = QSCrypt.Encrypt(c.StudentRegId.ToString())
                               });

            return studentList;
        }

        public IEnumerable<Object> getStudentsList(int passYearId, int passClassId, int passSecId)
        {
            var studentList = (from a in db.Students
                               from b in db.AssignClassToStudents
                               from c in db.StudentRollNumbers
                               where b.AcademicYearId == passYearId &&
                                     b.StudentRegisterId == a.StudentRegisterId &&
                                     b.StudentRegisterId == c.StudentRegisterId &&
                                     b.ClassId == passClassId &&
                                     b.SectionId == passSecId &&
                                     b.AcademicYearId==c.AcademicYearId &&
                                     b.ClassId==c.ClassId &&
                                     b.SectionId==c.SectionId
                               select new
                               {
                                   id = a.StudentRegisterId,
                                   RolNumber = c.RollNumber,
                                   name = a.FirstName+" "+a.LastName
                               }).ToList().AsQueryable();
            return studentList;
        }

        public List<FacultyClassStudentsList> GetFacultyClassStudents(int yearId, int classId, int secId)
        {
            var StudentsList = (from a in db.AssignClassToStudents
                                from b in db.Students
                                where a.StudentRegisterId == b.StudentRegisterId &&
                                b.StudentStatus == true &&
                                a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId
                                select new FacultyClassStudentsList
                                {
                                    StudentId = b.StudentId,
                                    StudentName = b.FirstName+ " "+b.LastName
                                }).ToList();
            return StudentsList;
        }

        public List<AssignClassToStudent> getNumberOfStudents(int? yearId, int? classId, int? secId)
        {
            var getNumberOfStudents = (from a in db.AssignClassToStudents
                                       from b in db.Students
                                       where a.StudentRegisterId == b.StudentRegisterId &&
                                       b.StudentStatus == true &&
                                       a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId
                                       select a).ToList();
            return getNumberOfStudents;
        }

        public IEnumerable<Object> getStudentsList(int passSecId)
        {
            var StudentList = (from a in db.Students
                               from b in db.AssignClassToStudents
                               where a.StudentRegisterId == b.StudentRegisterId &&
                               b.SectionId == passSecId &&
                               a.StudentStatus == true
                               select new
                               {
                                   StudentName = a.FirstName+" "+a.LastName,
                                   StudentId = a.StudentRegisterId
                               }).ToList().AsEnumerable();
            return StudentList;
        }

        public List<MI_Students> getFacultyStudents(int MI_allAcademicYears, int MI_allClass, int secId)
        {
            var StudentList = (from a in db.AssignClassToStudents
                               from b in db.StudentRollNumbers
                               from c in db.Students
                               from d in db.AdmissionTransactions
                               from e in db.FatherRegisters
                               from f in db.MotherRegisters
                               from g in db.GuardianRegisters
                               where a.AcademicYearId == MI_allAcademicYears &&
                               a.ClassId == MI_allClass &&
                               a.SectionId == secId &&
                               a.StudentRegisterId == b.StudentRegisterId &&
                               a.StudentRegisterId == c.StudentRegisterId &&
                               a.StudentRegisterId == d.StudentRegisterId &&
                               d.FatherRegisterId == e.FatherRegisterId &&
                               d.MotherRegisterId == f.MotherRegisterId &&
                               d.GuardianRegisterId == g.GuardianRegisterId
                               select new MI_Students
                               {
                                   RollNumber = b.RollNumber,
                                   StudentName = c.FirstName + " " + c.LastName,
                                   Gender = c.Gender,
                                   EmergencyContactPerson = c.EmergencyContactPerson,
                                   EmergencyContactNumber = c.EmergencyContactNumber,
                                   FatherName = e.FatherFirstName+" "+e.FatherLastName,
                                   FatherContactNumber = e.FatherMobileNo,
                                   MotherName = f.MotherFirstname+" "+f.MotherLastName,
                                   MotherContactNumber = f.MotherMobileNo,
                                   GuardianName = g.GuardianFirstname+" "+g.GuardianLastName,
                                   GuardianContactNumber = g.GuardianMobileNo,
                                   HostelPreference = a.HostelPreference
                               }).ToList();

            return StudentList;
        }

        public void addAssignClass(int StudentRegisterId,int AcademicYearId,int ClassId,int SectionId,string TransportPreference,string HostelPreference)
        {
            AssignClassToStudent s = new AssignClassToStudent()
            {
                StudentRegisterId = StudentRegisterId,
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                TransportPreference = TransportPreference,
                HostelPreference = HostelPreference
            };
            db.AssignClassToStudents.Add(s);
            db.SaveChanges();
        }
        public bool checkAssignClassStudent(int acid, int cid, int sec_id, int std_id)
        {
            var ans = db.AssignClassToStudents.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) && q.StudentRegisterId.Value.Equals(std_id)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void addAssignClassStudent(int acid, int cid, int sec_id, int std_id)
        {
            AssignClassToStudent std = new AssignClassToStudent()
            {
                AcademicYearId = acid,
                ClassId = cid,
                SectionId = sec_id,
                StudentRegisterId = std_id,
                Status=true,
                HaveRollNumber=false
                
            };

            db.AssignClassToStudents.Add(std);
            db.SaveChanges();
        }

        internal void promoteAssignClassStudent(int std_id, int old_acid, int old_cid, int old_sec_id)
        {
            var update = db.AssignClassToStudents.Where(q => q.AcademicYearId.Value.Equals(old_acid) && q.ClassId.Value.Equals(old_cid) && q.SectionId.Value.Equals(old_sec_id) && q.StudentRegisterId.Value.Equals(std_id)).FirstOrDefault();
            if (update != null)
            {
                //update.Status = false;
                //update.HaveRollNumber = false;
                update.Result = "Pass";
                update.StudentEducationStatus = "Completed";
                update.ClassAssignedForNextAcademicYear = "No";
                update.ReAppear = "No";
                db.SaveChanges();
            }
        }

        internal void demoteAssignClassStudent(int std_id, int old_acid, int old_cid, int old_sec_id)
        {
            var update = db.AssignClassToStudents.Where(q => q.AcademicYearId.Value.Equals(old_acid) && q.ClassId.Value.Equals(old_cid) && q.SectionId.Value.Equals(old_sec_id) && q.StudentRegisterId.Value.Equals(std_id)).FirstOrDefault();
            if (update != null)
            {
               // update.Status = false;
                update.Result = "Fail";
               // update.HaveRollNumber = false;
                update.StudentEducationStatus = "Completed";
                update.ClassAssignedForNextAcademicYear = "No";
                update.ReAppear = "No";
                db.SaveChanges();
            }
        }

        internal void RemoveAssignClassStudent(int std_id, int old_acid, int old_cid, int old_sec_id)
        {
            var update = db.AssignClassToStudents.Where(q => q.AcademicYearId.Value.Equals(old_acid) && q.ClassId.Value.Equals(old_cid) && q.SectionId.Value.Equals(old_sec_id) && q.StudentRegisterId.Value.Equals(std_id)).FirstOrDefault();
            if (update != null)
            {
               // update.Status = false;
              //  update.HaveRollNumber = false;
                update.StudentEducationStatus = "InComplete";
                update.ClassAssignedForNextAcademicYear = "No";
                update.ReAppear = "No";
                update.Result = null;
                db.SaveChanges();
            }
        }

        internal void ReAppearAssignClassStudent(int std_id, int old_acid, int old_cid, int old_sec_id)
        {
            var update = db.AssignClassToStudents.Where(q => q.AcademicYearId.Value.Equals(old_acid) && q.ClassId.Value.Equals(old_cid) && q.SectionId.Value.Equals(old_sec_id) && q.StudentRegisterId.Value.Equals(std_id)).FirstOrDefault();
            if (update != null)
            {
                update.ReAppear = "Yes";
                db.SaveChanges();
            }
        }

        public AssignClassToStudent getFinalResult(int acid,int cid,int sec_id,int std_id)
        {
            var row = (from a in db.AssignClassToStudents where a.AcademicYearId == acid && a.ClassId == cid && a.SectionId == sec_id && a.StudentRegisterId == std_id select a).FirstOrDefault();
            return row;
        }

        public IEnumerable<Object> getStudentsToGiveMark(int passYearId, int passClassId, int passSecId, int SubId,int ExamId)
        {
            var studentList = (from a in db.Students
                               from b in db.AssignClassToStudents
                               from c in db.StudentRollNumbers
                               from d in db.ExamAttendances
                               where b.StudentRegisterId == a.StudentRegisterId &&
                                     b.StudentRegisterId == c.StudentRegisterId &&
                                     b.AcademicYearId == passYearId &&
                                     b.ClassId == passClassId &&
                                     b.SectionId == passSecId &&
                                     a.StudentStatus == true &&
                                     b.Status == true &&
                                     b.AcademicYearId == c.AcademicYearId &&
                                     b.ClassId == c.ClassId &&
                                     b.SectionId == c.SectionId &&
                                     b.StudentRegisterId == d.StudentRegisterId &&
                                     d.StudentRegisterId == c.StudentRegisterId &&
                                     d.ClassId == passClassId &&
                                     d.SectionId == passSecId &&
                                     d.AcademicYearId == passYearId &&
                                     d.SubjectId == SubId &&
                                     d.ExamId == ExamId

                               //c.Status == true &&
                               //c.AcademicYearId == passYearId &&
                               //c.ClassId == passClassId &&
                               //c.SectionId == passSecId

                               select new
                               {
                                   id = a.StudentRegisterId,
                                   name = a.FirstName + " " + a.LastName,
                                   rollnumber = c.RollNumber,
                                   Attendance = d.AttendanceStatus

                               }).Distinct().ToList().Select(c => new
                               {
                                   id = QSCrypt.Encrypt(c.id.ToString()),
                                   name = c.name,
                                   rollnumber = c.rollnumber,
                                   attendance = c.Attendance
                               }).Distinct().ToList();
            return studentList;           
        }

        public int getStudentscount(int passYearId, int passClassId, int passSecId)
        {
            var studentList = (from a in db.Students
                               from b in db.AssignClassToStudents
                               from c in db.StudentRollNumbers
                               where b.StudentRegisterId == a.StudentRegisterId &&
                                     b.StudentRegisterId == c.StudentRegisterId &&
                                     b.AcademicYearId == passYearId &&
                                     b.ClassId == passClassId &&
                                     b.SectionId == passSecId &&
                                     a.StudentStatus == true &&
                                     b.Status == true &&
                                     b.AcademicYearId == c.AcademicYearId &&
                                     b.ClassId == c.ClassId &&
                                     b.SectionId == c.SectionId
                               select new
                               {                                  
                                   rollnumber = c.RollNumber
                               }).ToList().Count();

            return studentList;
        }






        public List<MI_Students> getFacultyStudents(int? MI_allAcademicYears, int? MI_allClass, int? secId)
        {
            var StudentList = (from a in db.AssignClassToStudents
                               from b in db.StudentRollNumbers
                               from c in db.Students
                               from d in db.AdmissionTransactions
                               where a.AcademicYearId == MI_allAcademicYears &&
                               a.ClassId == MI_allClass &&
                               a.SectionId == secId &&
                               a.StudentRegisterId == b.StudentRegisterId &&
                               a.StudentRegisterId == c.StudentRegisterId &&
                               a.StudentRegisterId == d.StudentRegisterId &&
                               //c.StudentStatus == true &&
                               //a.Status == true &&
                               //b.Status == true &&
                               b.AcademicYearId == MI_allAcademicYears &&
                               b.ClassId == MI_allClass &&
                               b.SectionId == secId &&
                               b.AcademicYearId==a.AcademicYearId &&
                               b.ClassId==b.ClassId &&
                               b.SectionId==b.SectionId
                               select new MI_Students
                               {
                                   RollNumber = b.RollNumber,
                                   StudentName = c.FirstName + " " + c.LastName,
                                   Gender = c.Gender,
                                   EmergencyContactPerson = c.EmergencyContactPerson,
                                   EmergencyContactNumber = c.EmergencyContactNumber
                               }).ToList();

            return StudentList;
        }

        public void AddAssignClassToStudent(int StudentRegisterId, int AcademicYearId, int ClassId, int SectionId, int AdminId, string StudentStatus)
        {
            AssignClassToStudent s = new AssignClassToStudent()
            {
                StudentRegisterId = StudentRegisterId,
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                StudentEducationStatus = "Studying",
                ClassAssignedForNextAcademicYear = "No",
                ReAppear = "No" ,
                ClassAssignedBy = AdminId,
                DateOfClassAssigning = DateTimeByZone.getCurrentDateTime(),
                Status = true,
                HaveRollNumber = false
            };
            db.AssignClassToStudents.Add(s);
            db.SaveChanges();

            int PreviousAcYearId;
            var PreviousAcYearIdVal = (from a in db.AcademicYears where a.AcademicYearId < AcademicYearId select a).ToList().Count;
            if (PreviousAcYearIdVal!=0)
            {
                PreviousAcYearId = (from a in db.AcademicYears where a.AcademicYearId < AcademicYearId select a).ToList().Max().AcademicYearId;
            }
            else
            {
                PreviousAcYearId = 0;
            }
            int PreviousClassId;
            if (ClassId == 1)
            {
                PreviousClassId = 0;
            }
            else
            {
                if (StudentStatus == "Pass Student")
                {
                    PreviousClassId = (from a in db.Classes where a.ClassId < ClassId select a.ClassId).ToList().Max();
                }
                else
                {
                    PreviousClassId = ClassId;
                }
            }

            var row = (from a in db.AssignClassToStudents where a.AcademicYearId == PreviousAcYearId && a.ClassId == PreviousClassId && a.StudentRegisterId == StudentRegisterId select a).FirstOrDefault();  
            if (row != null)
            {
                row.ClassAssignedForNextAcademicYear = "Yes";
                row.Status = false;
                db.SaveChanges();
            }
        }

        public IEnumerable<Object> getStudentListToChangeSection(int passYearId, int passClassId, int passSecId)
        {
            var studentList = (from a in db.Students
                               from b in db.AssignClassToStudents
                               where b.StudentRegisterId == a.StudentRegisterId &&
                                     b.AcademicYearId == passYearId &&
                                     b.ClassId == passClassId &&
                                     b.SectionId == passSecId &&
                                     a.StudentStatus == true &&
                                     b.Status == true
                               select new
                               {
                                   admissionId = a.AdmissionId,
                                   StudentRegId = a.StudentRegisterId,
                                   studentId = a.StudentId,
                                   FirstName = a.FirstName,
                                   LastName = a.LastName,
                                   Gender = a.Gender,
                                   FirstLanguage = a.FirstLanguage,
                                   //MediumOfStudy = c.BoardName,
                                   wayOfEnrollment = a.ApplicationSource
                               }).ToList().Select(c => new
                               {
                                   AdmissionId = c.admissionId,
                                   StudentRegId = c.StudentRegId,
                                   StudentId = c.studentId,
                                   FirstName = c.FirstName,
                                   LastName = c.LastName,
                                   Gender = c.Gender,
                                   FirstLanguage = c.FirstLanguage,
                                   MediumOfStudy = Boardname(c.StudentRegId),
                                   WayOfEnrollment = c.wayOfEnrollment,
                                   Student_RegId = QSCrypt.Encrypt(c.StudentRegId.ToString())
                               });

            return studentList;
        }

        public string Boardname(int studentid)
        {
            var s = (from a in db.Students
                     from b in db.BoardofSchools
                     where a.StudentRegisterId == studentid && a.BoardOfPreviousSchool == b.BoardId
                     select b.BoardName).FirstOrDefault();
            return s;
        }

        public void EditChangeSectionToStudent(int studentRegisterId, int year, int classId, int secId, int AdminId)
        {
            var getRow = (from a in db.AssignClassToStudents where a.StudentRegisterId == studentRegisterId && a.AcademicYearId == year && a.ClassId == classId select a).First();
            getRow.SectionId = secId;
            getRow.ClassAssignedBy = AdminId;
            getRow.HaveRollNumber = false;
            db.SaveChanges();
        }

        public List<CC_Pdf> generateCC(int StudentRegId)
        {
            var detail = ((from a in db.Students
                           from b in db.AdmissionTransactions
                           from c in db.PrimaryUserRegisters
                           where a.StudentRegisterId == b.StudentRegisterId &&
                           a.StudentRegisterId == b.StudentRegisterId &&
                           b.PrimaryUserRegisterId == c.PrimaryUserRegisterId &&
                           a.StudentRegisterId == StudentRegId
                           select new CC_Pdf
                           {
                               StudentRegId = a.StudentRegisterId,
                               AdmissionId = a.AdmissionId,
                               FirstName = a.FirstName,
                               LastName = a.LastName,
                               Gender = a.Gender,
                               PrimaryUserFirstName = c.PrimaryUserName
                           })
     
                                 ).ToList();
            return detail;
        }
        public List<CC_Pdf> getJoinedClass(int year,int classId,int secId,int studentRegisterId)
        {
             var getRow = (from a in db.AssignClassToStudents
                          where a.AcademicYearId == year &&
                          a.ClassId == classId &&
                          a.SectionId == secId &&
                          a.StudentRegisterId == studentRegisterId 
                          select new CC_Pdf
                          {
                              StudentRegId = a.StudentRegisterId,
                              //FromYear = b.AcademicYear1,
                              //FromClass = c.ClassType,
                              ConductStatus = a.ConductStatus
                          }).ToList();
            return getRow;
        }

        public List<CC_Pdf> getCurrentClass(int StudentRegId)
        {
            var getRow = (from a in db.AssignClassToStudents
                          from b in db.AcademicYears
                          from c in db.Classes
                          where a.AcademicYearId == b.AcademicYearId &&
                          a.ClassId == c.ClassId &&
                          a.Status == true &&
                          a.StudentRegisterId == StudentRegId
                          orderby a.AcademicYearId descending
                          select new CC_Pdf
                          {
                              StudentRegId = a.StudentRegisterId,
                              toYear = b.AcademicYear1,
                              toClass = c.ClassType
                          }).ToList();
            return getRow;
        }

        public void UpdateTransferStatus(int StudentRegId, string TransferStatus, string fileName, int year, int classId, int secId)
        {
            var getRow = (from a in db.AssignClassToStudents where a.StudentRegisterId == StudentRegId && a.Status == true && a.AcademicYearId == year && a.ClassId == classId && a.SectionId == secId select a).FirstOrDefault();
            getRow.TransferStatus = TransferStatus;
            getRow.TransferCertificate = fileName;
            db.SaveChanges();
        }

        public List<TC_Pdf> generateTC(int StudentRegId)
        {
            var detail = (from a in db.Students
                          from b in db.PrimaryUserRegisters
                          from c in db.AssignClassToStudents
                          from d in db.tblCommunities
                          from e in db.AdmissionTransactions
                          from f in db.AcademicYears
                          from g in db.Classes
                          where   a.StudentRegisterId == c.StudentRegisterId &&
                          a.StudentRegisterId == e.StudentRegisterId &&
                          e.PrimaryUserRegisterId == b.PrimaryUserRegisterId &&
                          a.StudentCommunity == d.CommunityId &&
                          a.StudentRegisterId == StudentRegId &&
                          a.AcademicyearId == f.AcademicYearId &&
                          a.AdmissionClass == g.ClassId
                          select new TC_Pdf
                          {
                              StudentRegId = a.StudentRegisterId,
                              AdmissionId = a.AdmissionId,
                              FirstName = a.FirstName,
                              LastName = a.LastName,
                              PrimaryUserFirstName = b.PrimaryUserName,
                              Nationality = a.Nationality,
                              Religion = a.Religion,
                              caste = d.CommunityName,
                              DOB = a.DOB,
                              DateOfAdmission = c.DateOfClassAssigning,
                              FromYear = f.AcademicYear1,
                              FromClass = g.ClassType
                            //  MediumOfStudy = a.MediumOfStudy,
                            //  ConductStatus = c.ConductStatus
                          }).Distinct().ToList();
            return detail;
        }

        public IEnumerable<Object> getStudentListToStudentCommunication(int passYearId, int passClassId, int passSecId)
        {
            var studentList = (from a in db.Students
                               from b in db.AssignClassToStudents
                               where b.StudentRegisterId == a.StudentRegisterId &&
                                     b.AcademicYearId == passYearId &&
                                     b.ClassId == passClassId &&
                                     b.SectionId == passSecId &&
                                     a.StudentStatus == true &&
                                     b.Status == true
                               select new
                               {
                                   StudentRegId = a.StudentRegisterId,
                                   StudentId = a.StudentId,
                                   FName = a.FirstName,
                                   LName = a.LastName

                               }).ToList().Select(c => new
                               {
                                   StudentRegId = c.StudentRegId,
                                   StudentId = c.StudentId,
                                   StudentName = c.FName + " " + c.LName,
                                   Student_RegId = QSCrypt.Encrypt(c.StudentRegId.ToString())
                               }).ToList();

            return studentList;
        }

        public List<EmailClassStudentList>getClassStudentList(int yearId, int classId)
        {
            var List = (from a in db.AssignClassToStudents
                        from b in db.AdmissionTransactions
                        from c in db.PrimaryUserRegisters
                        where a.StudentRegisterId == b.StudentRegisterId &&
                        a.StudentRegisterId == b.StudentRegisterId &&
                        b.PrimaryUserRegisterId == c.PrimaryUserRegisterId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.Status == true
                        select new EmailClassStudentList
                        {
                            Email = c.PrimaryUserEmail,
                            MobileNum = c.PrimaryUserContactNo
                        }).ToList();
            return List;
        }

        public EmailClassStudentList getStudentPrmaryUserEmail(int yearId, int classId, int secId, int studentRegId)
        {
            var List = (from a in db.AssignClassToStudents
                        from b in db.AdmissionTransactions
                        from c in db.PrimaryUserRegisters
                        where a.StudentRegisterId == b.StudentRegisterId &&
                        a.StudentRegisterId == b.StudentRegisterId &&
                        b.PrimaryUserRegisterId == c.PrimaryUserRegisterId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.StudentRegisterId == studentRegId &&
                        a.Status == true
                        select new EmailClassStudentList
                        {
                            Email = c.PrimaryUserEmail,
                            MobileNum = c.PrimaryUserContactNo
                        }).FirstOrDefault();
            return List;
        }

        public List<AssignClassToStudent> GetClassStudent(int yearid, int classid, int sectionid)
        {
            var ans = (from a in db.AssignClassToStudents
                       where a.AcademicYearId == yearid && a.ClassId == classid && a.SectionId == sectionid && a.Status == true && a.HaveRollNumber == true
                       select a).ToList();
          return ans;

        }
        public List<AssignClassToStudent> GetClassStudentBeforeAssigningRollNumber(int yearid, int classid, int sectionid)
        {
            var ans = (from a in db.AssignClassToStudents
                       where a.AcademicYearId == yearid && a.ClassId == classid && a.SectionId == sectionid && a.Status == true
                       select a).ToList();
            return ans;

        }
        public string getResult(int year, int classId, int secId, int studentRegisterId)
        {
            var row = (from a in db.AssignClassToStudents where a.AcademicYearId == year && a.ClassId == classId && a.SectionId == secId && a.StudentRegisterId == studentRegisterId && a.Status == true select a).FirstOrDefault();
            string result="";
            if(row.Result == "Pass")
            {
                return result = "Pass";
            }
            else if (row.Result == "Fail")
            {
                return result = "Fail";
            }
            else
            {
                return result;
            }
        }

        public string getPreviousYearResult(int classId, int studentRegisterId)
        {
            var row = (from a in db.AssignClassToStudents where a.ClassId == classId && a.StudentRegisterId == studentRegisterId  select a).FirstOrDefault();
            string result = "";
            if(row != null)
            {
                if (row.Result == "Pass")
                {
                    return result = "Pass";
                }
                else 
                {
                    return result = "Fail";
                }
            }
            else
            {
                return result="studentNotExist";
            }
        }

        public List<getclassandsection> SearchStudentNameList(int yid, int cid, int sid)
        {
            var ans = (from a in db.AssignClassToStudents
                       from b in db.Classes
                       from c in db.Sections
                       from d in db.Subjects
                       from e in db.Students
                       from f in db.StudentRollNumbers
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && b.Status.Value.Equals(true) && b.ClassId == cid && c.ClassId == cid && c.SectionId == sid && a.StudentRegisterId == e.StudentRegisterId && 
                       e.StudentRegisterId == f.StudentRegisterId  && f.Status == true && a.HaveRollNumber == true && e.StudentStatus==true && a.AcademicYearId ==f.AcademicYearId && a.ClassId==f.ClassId
                       && a.SectionId==f.SectionId 
                       select new getclassandsection
                       {
                           StudentId=e.StudentId,
                           rollnumber = f.RollNumber,
                           StudentName = e.FirstName + " " +e.LastName,
                           Rollnumber=f.RollNumber
                       }
                           ).Distinct().OrderBy(x => x.StudentName).ToList();
            return ans;
        }
        public IEnumerable GetClassStudentName(int yearid, int classid, int sectionid)
        {
            var ans =
                 (from a in db.Students
                  from b in db.AssignClassToStudents
                  from c in db.StudentRollNumbers
                  where b.ClassId == classid && b.SectionId == sectionid && b.AcademicYearId == yearid && a.StudentRegisterId == b.StudentRegisterId && c.AcademicYearId == b.AcademicYearId && c.ClassId == b.ClassId && c.SectionId == b.SectionId && c.StudentRegisterId == a.StudentRegisterId
                  select new { StudentRegisterId = a.StudentRegisterId, StudentName = a.FirstName + " " + a.LastName + " (" + c.RollNumber + ")" }).OrderBy(q => q.StudentRegisterId).ToList();
            return ans;

        }
        public IEnumerable<Object> GetJsonStudentRegid(int yearid, int classid, int sectionid)
        {
            var ans =(
                from t1 in db.AssignClassToStudents
                from t2 in db.Students
                from t3 in db.StudentRollNumbers
                where  t1.HaveRollNumber == true  && t1.StudentRegisterId == t2.StudentRegisterId && t1.SectionId == sectionid && t1.ClassId == classid && t1.AcademicYearId == yearid && t3.AcademicYearId == t1.AcademicYearId && t3.ClassId == t1.ClassId && t3.SectionId == t1.SectionId && t3.StudentRegisterId == t1.StudentRegisterId //&& t1.Status =true && t1.HaveRollNumber == true
                select new { sname = t2.FirstName +" "+t2.LastName + " (" + t3.RollNumber + ")", reg_id = t2.StudentRegisterId }).ToList();
            return ans;

        }

        public AssignClassToStudent getCurrentActiveClass(int studentRegId)
        {
            var row = (from a in db.AssignClassToStudents where a.StudentRegisterId == studentRegId && a.Status == true select a).FirstOrDefault();
            return row;
        }
        public IEnumerable<Object> getSession(int passYearId)
        {
            var List = (from a in db.SportsSessions
                        where
                        a.AcademicYearId == passYearId
                        select new
                        {
                            SessionlId = a.SessionId,
                            SessionName = a.SessionName
                        }).ToList();

            return List;
        }
        public IEnumerable<Object> getLevel(int passGameId)
        {
            var List = (from a in db.SportsGameLevels
                        from b in db.SportsGameLevelConfigs
                        where a.LevelId == b.GameLevelId &&
                        a.GameId == passGameId
                        select new
                        {
                            GameLevelId = b.Id,
                            GameLevelName = b.GameLevelName
                        }).ToList();

            return List;
        }

        public bool CheckClassAssignedtoAdmissionStudent(int AdmissionId)
        {
            var ans = (from a in db.Students
                       from b in db.AssignClassToStudents
                       where a.AdmissionId == AdmissionId && b.StudentRegisterId == a.StudentRegisterId && b.Status == true
                       select a).FirstOrDefault();
            if(ans != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}