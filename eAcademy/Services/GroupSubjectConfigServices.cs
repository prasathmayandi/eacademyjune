﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class GroupSubjectConfigServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public int getGroupId(int subId)
        {
            var row = (from a in db.GroupSubjectConfigs where a.SubjectId == subId select a).FirstOrDefault();
            int GroupId;
            if(row != null)
            {
                GroupId = row.GroupSubjectId.Value;
            }
            else
            {
                GroupId = 0;
            }
            return GroupId;
        }
        public void  AddSubjectConfig(int GroupSubjectId,int?  SubId, bool SubStatus, int AdminId)
        {
            GroupSubjectConfig add = new GroupSubjectConfig()
            {
                GroupSubjectId=GroupSubjectId,
                SubjectId=SubId,
                status = SubStatus,
                CreatedBy = AdminId,
                CreatedDate = DateTimeByZone.getCurrentDateTime()
            };

            db.GroupSubjectConfigs.Add(add);
            db.SaveChanges();
        }
        public List<GroupList> getubjectByGroupID(int gsId)
        {
            var Sublist = (from t1 in db.GroupSubjectConfigs
                           from t2 in db.Subjects
                       where t1.GroupSubjectId == gsId && t1.SubjectId==t2.SubjectId
                       select new GroupList
                       {
                           SubId = t1.SubjectId,
                           SubjectConfigId=t1.Id,
                           SubjectName=t2.SubjectName,
                           Status = t1.status
                       }).ToList();

            return Sublist;
        }
        public void UpdateSubjectConfig(int groupConfig_id, int Sub_id, bool S_status, int AdminId)
        {
            var update = db.GroupSubjectConfigs.Where(q => q.Id.Equals(groupConfig_id)).First();
            if (update != null)
            {
                update.SubjectId = Sub_id;
                update.status = S_status;
                update.UpdatedBy = AdminId;
                update.UpdatedDate = DateTimeByZone.getCurrentDateTime();
                db.SaveChanges();
            }
        }
        public bool checkSubjectAssignStatus(int? SubId)
        {
            var ans = (from t1 in db.GroupSubjectConfigs

                       where t1.SubjectId == SubId && t1.status== true 
                       select new GroupList
                       {
                           SubId = t1.SubjectId,
                           SubjectConfigId = t1.Id,                          
                           Status = t1.status
                       }).ToList();
            int count = ans.Count;
            if (count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}