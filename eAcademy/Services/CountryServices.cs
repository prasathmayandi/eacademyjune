﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class CountryServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public IEnumerable<Object> ShowCountries()
        {
            var CountryList = (from a in db.tblCountries
                               select new
                               {
                                   CountryName = a.CountryName,
                                   CountryCode = a.ISO3166_2LetterCode
                               }).ToList().AsEnumerable();
            return CountryList;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}