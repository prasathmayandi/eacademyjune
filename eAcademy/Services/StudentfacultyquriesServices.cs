﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class StudentfacultyquriesServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public void addQuery(int yearId, int classId, int secId, int subjectId, int FacultyId, string query, string QueryFileName, string QueryFrom, int? StudentRegId, int parentPrimaryUserId)
        {
            DateTime date = DateTimeByZone.getCurrentDate();
            StudentFacultyQuery add = new StudentFacultyQuery()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                SubjectId = subjectId,
                EmployeeRegisterId = FacultyId,
                StudentRegisterId = StudentRegId,
                DateOfQuery = date,
                Query = query,
                QueryFile = QueryFileName,
                QueryFrom = QueryFrom,
                PrimaryUserRegId = parentPrimaryUserId
            };
            db.StudentFacultyQueries.Add(add);
            db.SaveChanges();
        }

        public IEnumerable<MI_Queries> getFaculyQueryList(int? yearId, int classId, int secId, int? subId, DateTime? fdate, DateTime? toDate, int facultyId)
        {
            var list = (from a in db.StudentFacultyQueries
                        from b in db.Students
                        from d in db.Subjects
                        from e in db.StudentRollNumbers
                        from f in db.Classes
                        from g in db.Sections
                        where a.SubjectId == d.SubjectId &&
                        a.StudentRegisterId == b.StudentRegisterId &&
                        a.StudentRegisterId == e.StudentRegisterId &&
                        a.ClassId == f.ClassId &&
                        a.SectionId == g.SectionId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.SubjectId == subId &&
                        a.DateOfQuery >= fdate &&
                        a.DateOfQuery <= toDate &&
                        a.EmployeeRegisterId == facultyId &&
                        e.AcademicYearId == yearId &&
                        e.ClassId == classId &&
                        e.SectionId == secId 
                        select new 
                        {
                            Query_Id = a.QueryId,
                            Date = a.DateOfQuery,
                            StudentId = b.StudentId,
                            FirstName = b.FirstName+" "+b.LastName,
                            RollNumber = e.RollNumber,
                            classs = f.ClassType+" "+g.SectionName,
                            Query = a.Query,
                            Reply = a.Reply,
                            SentBy = a.QueryFrom,
                            subject = d.SubjectName
                        }).ToList().Select(a => new MI_Queries
                        {
                            Query_Id = QSCrypt.Encrypt(a.Query_Id.ToString()),
                            Date = a.Date.Value.ToString("dd MMM, yyyy"),
                            StudentId = a.StudentId,
                            StudentName = a.FirstName,
                            RollNumber = a.RollNumber,
                            classs = a.classs,
                            subject = a.subject,
                            Query = a.Query,
                            Reply = a.Reply,
                            SentBy = a.SentBy,
                            staffqueryfilename = getqueryattachfilename(a.Query_Id),
                            staffqueryreplyfilename=getreplyqueryattachfilename(a.Query_Id)
                        }).ToList();
            return list;
        }

        public IEnumerable<MI_Queries> getFaculyQueryListForClass(int? yearId, DateTime? fdate, DateTime? toDate, int facultyId)
        {
            var list = (from a in db.StudentFacultyQueries
                        from b in db.Students
                        from d in db.Subjects
                        from e in db.StudentRollNumbers
                        from f in db.Classes
                        from g in db.Sections
                        where a.SubjectId == d.SubjectId &&
                        a.StudentRegisterId == b.StudentRegisterId &&
                        a.StudentRegisterId == e.StudentRegisterId &&
                        a.ClassId == f.ClassId &&
                        a.SectionId == g.SectionId &&
                        a.AcademicYearId == yearId &&
                        a.DateOfQuery >= fdate &&
                        a.DateOfQuery <= toDate &&
                        a.EmployeeRegisterId == facultyId &&
                        e.AcademicYearId == yearId 
                        select new 
                        {
                            Query_Id = a.QueryId,
                            Date = a.DateOfQuery,
                            StudentId = b.StudentId,
                            FirstName = b.FirstName+" "+b.LastName,
                            RollNumber = e.RollNumber,
                            classs = f.ClassType+" "+g.SectionName,
                            Query = a.Query,
                            Reply = a.Reply,
                            SentBy = a.QueryFrom,
                            subject = d.SubjectName
                        }).ToList().Select(a => new MI_Queries
                        {
                            Query_Id = QSCrypt.Encrypt(a.Query_Id.ToString()),
                            Date = a.Date.Value.ToString("dd MMM, yyyy"),
                            StudentId = a.StudentId,
                            StudentName = a.FirstName,
                            RollNumber = a.RollNumber,
                            classs = a.classs,
                            subject = a.subject,
                            Query = a.Query,
                            Reply = a.Reply,
                            SentBy = a.SentBy,
                            staffqueryfilename = getqueryattachfilename(a.Query_Id),
                            staffqueryreplyfilename = getreplyqueryattachfilename(a.Query_Id)
                        }).ToList();
            return list;
        }

        public IEnumerable<MI_Queries> getParentQueryList(int? yearId, int classId, int secId, int? studentRegId, int? subId, DateTime? fdate, DateTime? toDate)
        {
            var list = (from a in db.StudentFacultyQueries
                        from b in db.Students
                        from c in db.Subjects
                        from d in db.TechEmployees
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.StudentRegisterId == studentRegId &&
                        a.SubjectId == subId &&
                        a.DateOfQuery >= fdate &&
                        a.DateOfQuery <= toDate &&
                        a.StudentRegisterId == b.StudentRegisterId &&
                        a.SubjectId == c.SubjectId &&
                        a.EmployeeRegisterId == d.EmployeeRegisterId
                        select new 
                        {
                            Query_Id = a.QueryId,
                            Date = a.DateOfQuery,
                            Subject = c.SubjectName,
                            Faculty = d.EmployeeName,
                            Query = a.Query,
                            QueryFile = a.QueryFile,
                            Reply = a.Reply,
                            ReplyFile = a.ReplyFile,
                            QueryFrom = a.QueryFrom
                        }).ToList().Select(a => new MI_Queries
                        {
                            Query_Id = QSCrypt.Encrypt(a.Query_Id.ToString()),
                            Date = a.Date.Value.ToString("MMM dd yyyy"),
                            Subject = a.Subject,
                            Faculty = a.Faculty,
                            QueryFrom = a.QueryFrom,
                            Query = a.Query,
                            QueryFile = a.QueryFile,
                            Reply = a.Reply,
                            ReplyFile = a.ReplyFile,
                            parentqueryattachementfilename=getparentqueryattachmentfilename(a.Query_Id),
                            parentreplyqueryattachementfilename = getparentreplyqueryattachmentfilename(a.Query_Id),
                        }).ToList();
            return list;
        }

        public IEnumerable<MI_Queries> getParentQueryListAll(int? yearId, int classId, int secId, int? studentRegId, DateTime? fdate, DateTime? toDate)
        {
            var list = (from a in db.StudentFacultyQueries
                        from b in db.Students
                        from c in db.Subjects
                        from d in db.TechEmployees
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.StudentRegisterId == studentRegId &&
                        a.DateOfQuery >= fdate &&
                        a.DateOfQuery <= toDate &&
                        a.StudentRegisterId == b.StudentRegisterId &&
                        a.SubjectId == c.SubjectId &&
                        a.EmployeeRegisterId == d.EmployeeRegisterId
                        select new
                        {
                            Query_Id = a.QueryId,
                            Date = a.DateOfQuery,
                            Subject = c.SubjectName,
                            Faculty = d.EmployeeName,
                            Query = a.Query,
                            QueryFile = a.QueryFile,
                            Reply = a.Reply,
                            ReplyFile = a.ReplyFile,
                            QueryFrom = a.QueryFrom
                        }).ToList().Select(a => new MI_Queries
                        {
                            Query_Id = QSCrypt.Encrypt(a.Query_Id.ToString()),
                            Date = a.Date.Value.ToString("MMM dd yyyy"),
                            Subject = a.Subject,
                            Faculty = a.Faculty,
                            QueryFrom = a.QueryFrom,
                            Query = a.Query,
                            QueryFile = a.QueryFile,
                            Reply = a.Reply,
                            ReplyFile = a.ReplyFile,
                            parentqueryattachementfilename = getparentqueryattachmentfilename(a.Query_Id),
                            parentreplyqueryattachementfilename = getparentreplyqueryattachmentfilename(a.Query_Id),
                        }).ToList();
            return list;
        }


        public IEnumerable<MI_Queries> getStudentQueryList(int? yearId, int classId, int secId, int? studentRegId, int? subId, DateTime? fdate, DateTime? toDate)
        {
            var list = (from a in db.StudentFacultyQueries
                        from b in db.Students
                        from c in db.Subjects
                        from d in db.TechEmployees
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.StudentRegisterId == studentRegId &&
                        a.SubjectId == subId &&
                        a.DateOfQuery >= fdate &&
                        a.DateOfQuery <= toDate &&
                        a.StudentRegisterId == b.StudentRegisterId &&
                        a.SubjectId == c.SubjectId &&
                        a.EmployeeRegisterId == d.EmployeeRegisterId
                        select new
                        {
                            Query_Id = a.QueryId,
                            Date = a.DateOfQuery,
                            Subject = c.SubjectName,
                            Faculty = d.EmployeeName,
                            Query = a.Query,
                            QueryFile = a.QueryFile,
                            Reply = a.Reply,
                            ReplyFile = a.ReplyFile,
                            QueryFrom = a.QueryFrom
                        }).ToList().Select(a => new MI_Queries
                        {
                            Query_Id = QSCrypt.Encrypt(a.Query_Id.ToString()),
                            Date = a.Date.Value.ToString("MMM dd yyyy"),
                            Subject = a.Subject,
                            Faculty = a.Faculty,
                            QueryFrom = a.QueryFrom,
                            Query = a.Query,
                            QueryFile = a.QueryFile,
                            Reply = a.Reply,
                            ReplyFile = a.ReplyFile,
                            Studentqueryattachementfilename = getstudentqueryattachmentfilename(a.Query_Id),
                            Studentreplyqueryattachementfilename = getstudentreplyqueryattachmentfilename(a.Query_Id),
                        }).ToList();
            return list;
        }

        public IEnumerable<MI_Queries> getStudentQueryListAll(int? yearId, int classId, int secId, int? studentRegId, DateTime? fdate, DateTime? toDate)
        {
            var list = (from a in db.StudentFacultyQueries
                        from b in db.Students
                        from c in db.Subjects
                        from d in db.TechEmployees
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.StudentRegisterId == studentRegId &&
                        a.DateOfQuery >= fdate &&
                        a.DateOfQuery <= toDate &&
                        a.StudentRegisterId == b.StudentRegisterId &&
                        a.SubjectId == c.SubjectId &&
                        a.EmployeeRegisterId == d.EmployeeRegisterId
                        select new
                        {
                            Query_Id = a.QueryId,
                            Date = a.DateOfQuery,
                            Subject = c.SubjectName,
                            Faculty = d.EmployeeName,
                            Query = a.Query,
                            QueryFile = a.QueryFile,
                            Reply = a.Reply,
                            ReplyFile = a.ReplyFile,
                            QueryFrom = a.QueryFrom
                        }).ToList().Select(a => new MI_Queries
                        {
                            Query_Id = QSCrypt.Encrypt(a.Query_Id.ToString()),
                            Date = a.Date.Value.ToString("MMM dd yyyy"),
                            Subject = a.Subject,
                            Faculty = a.Faculty,
                            QueryFrom = a.QueryFrom,
                            Query = a.Query,
                            QueryFile = a.QueryFile,
                            Reply = a.Reply,
                            ReplyFile = a.ReplyFile,
                            Studentqueryattachementfilename = getstudentqueryattachmentfilename(a.Query_Id),
                            Studentreplyqueryattachementfilename = getstudentreplyqueryattachmentfilename(a.Query_Id),
                        }).ToList();
            return list;
        }




        public string getqueryattachfilename(int Query_Id)
        {
            string qf;
            var File = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().QueryFile;
            if (File != null)
            {
                var QF = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().Query;
                string queryid = QSCrypt.Encrypt(Query_Id.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    qf = "<a href=/ModulesInfo/OpenQueryFile/?id=" + queryid + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + QF;
                }
                else
                {
                    qf = "<a href=/ModulesInfo/OpenQueryFile/?id=" + queryid + "&name=student><i class='fa fa-file-word-o'></i></a>" + QF;
                }
            }
            else
            {
                var QF = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().Query;
                qf = QF;
            }
            return qf;
        }

        public string getreplyqueryattachfilename(int Query_Id)
        {
            string rq;
            var File = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().ReplyFile;
            if (File != null)
            {
                var RQ = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().Reply;
                string queryid = QSCrypt.Encrypt(Query_Id.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    rq = "<a href=/ModulesInfo/OpenReplyFile/?id=" + queryid + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + RQ;
                }
                else
                {
                    rq = "<a href=/ModulesInfo/OpenReplyFile/?id=" + queryid + "&name=student><i class='fa fa-file-word-o'></i></a>" + RQ;
                }
            }
            else
            {
                var RQ = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().Reply;
                rq = RQ;
            }
            return rq;
        }


        public string getparentqueryattachmentfilename(int Query_Id)
        {
            string qf;
            var File = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().QueryFile;
            if (File != null)
            {
                var QF = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().Query;
                string queryid = QSCrypt.Encrypt(Query_Id.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    qf = "<a href=/ParentModuleAssigned/OpenQueryFile/?id=" + queryid + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + QF;
                }
                else
                {
                    qf = "<a href=/ParentModuleAssigned/OpenQueryFile/?id=" + queryid + "&name=student><i class='fa fa-file-word-o'></i></a>" + QF;
                }
            }
            else
            {
                var QF = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().Query;
                qf = QF;
            }
            return qf;
        }

        public string getparentreplyqueryattachmentfilename(int Query_Id)
        {
            string rq;
            var File = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().ReplyFile;
            if (File != null)
            {
                var RQ = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().Reply;
                string queryid = QSCrypt.Encrypt(Query_Id.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    rq = "<a href=/ParentModuleAssigned/OpenQueryFile/?id=" + queryid + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + RQ;
                }
                else
                {
                    rq = "<a href=/ParentModuleAssigned/OpenQueryFile/?id=" + queryid + "&name=student><i class='fa fa-file-word-o'></i></a>" + RQ;
                }
            }
            else
            {
                var RQ = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().Reply;
                rq = RQ;
            }
            return rq;
        }



        public string getstudentqueryattachmentfilename(int Query_Id)
        {
            string qf;
            var File = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().QueryFile;
            if (File != null)
            {
                var QF = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().Query;
                string queryid = QSCrypt.Encrypt(Query_Id.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    qf = "<a href=/StudentModuleAssigned/OpenQueryFile/?id=" + queryid + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + QF;
                }
                else
                {
                    qf = "<a href=/StudentModuleAssigned/OpenQueryFile/?id=" + queryid + "&name=student><i class='fa fa-file-word-o'></i></a>" + QF;
                }
            }
            else
            {
                var QF = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().Query;
                qf = QF;
            }
            return qf;
        }

        public string getstudentreplyqueryattachmentfilename(int Query_Id)
        {
            string rq;
            var File = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().ReplyFile;
            if (File != null)
            {
                var RQ = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().Reply;
                string queryid = QSCrypt.Encrypt(Query_Id.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    rq = "<a href=/StudentModuleAssigned/OpenReplyFile/" + queryid + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + RQ;
                }
                else
                {
                    rq = "<a href=/StudentModuleAssigned/OpenReplyFile/" + queryid + "&name=student><i class='fa fa-file-word-o'></i></a>" + RQ;
                }
            }
            else
            {
                var RQ = (from a in db.StudentFacultyQueries where a.QueryId == Query_Id select a).FirstOrDefault().Reply;
                rq = RQ;
            }
            return rq;
        }



        public StudentFacultyQuery getQueryFile(int id)
        {
            var row = (from a in db.StudentFacultyQueries where a.QueryId == id select a).FirstOrDefault();
            return row;
        }

        public StudentFacultyQuery checkReplySent(int id)
        {
            var row = (from a in db.StudentFacultyQueries where a.QueryId == id select a).FirstOrDefault();
            return row;
        }

        public MI_Queries getStudentDetails(int id)
        {
            var row = (from a in db.StudentFacultyQueries
                       from b in db.Students
                       from c in db.Classes 
                       from d in db.Sections 
                       from e in db.Subjects
                       where a.StudentRegisterId == b.StudentRegisterId &&
                       a.ClassId == c.ClassId &&
                       a.SectionId == d.SectionId &&
                       a.SubjectId == e.SubjectId &&
                       a.QueryId == id
                       select new MI_Queries
                       {
                           academicYearId=a.AcademicYearId,
                           StudentId = b.StudentId,
                           FName = b.FirstName,
                           LName = b.LastName,
                           classs = c.ClassType,
                           Sec = d.SectionName,
                           subject = e.SubjectName,
                           Query = a.Query
                       }).FirstOrDefault();
            return row;
        }

        public void addReply(int qId,string reply,string ReplyFileName,int FacultyId)
        {
            var row = (from a in db.StudentFacultyQueries where a.QueryId == qId select a).FirstOrDefault();
            row.Reply = reply;
            row.ReplyFile = ReplyFileName;
            row.ReplyEmployeeRegId = FacultyId;
            db.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}