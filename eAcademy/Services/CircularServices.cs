﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class CircularServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public Circulars getCircular(int CircularId)
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Circulars
                       from t3 in dc.TechEmployees
                       where t1.AcademicYearId == t2.AcademicYearId && t2.IssuedBy == t3.EmployeeRegisterId && t2.CricularId == CircularId
                       select new Circulars
                       {
                           acYear = t1.AcademicYearId,
                           AcademicYear = t1.AcademicYear1,
                           CircularId = t2.CricularId,
                           tacYSD = t2.IssuedOn,
                           Heading = t2.Heading,
                           Reason = t2.Reason,
                           Status = t2.Status,
                           EmpRegId = t3.EmployeeRegisterId,
                           Employee = t3.EmployeeName,
                           Date =SqlFunctions.DateName("year", t2.IssuedOn)+"/"+SqlFunctions.StringConvert((double)t2.IssuedOn.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("day", t2.IssuedOn).Trim()
                          
                       }).FirstOrDefault();
            return ans;
        }

        public List<Circulars> getCircular()
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Circulars
                       from t3 in dc.TechEmployees
                       where t1.AcademicYearId == t2.AcademicYearId && t2.IssuedBy == t3.EmployeeRegisterId
                       select new Circulars {acYear=t1.AcademicYearId ,AcademicYear = t1.AcademicYear1, CircularId = t2.CricularId, tacYSD = t2.IssuedOn, Heading = t2.Heading, Reason = t2.Reason, Status = t2.Status, EmpRegId = t3.EmployeeRegisterId, Employee = t3.EmployeeName,
                                              Date = SqlFunctions.DateName("day", t2.IssuedOn).Trim() + "-" + SqlFunctions.StringConvert((double)t2.IssuedOn.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", t2.IssuedOn)  }).ToList().
                       Select(q => new Circulars {Circular_ID=QSCrypt.Encrypt(q.CircularId.ToString()),acYear=q.acYear, AcademicYear = q.AcademicYear, CircularId = q.CircularId, Date = q.Date, Heading = q.Heading, Reason = q.Reason, Status = q.Status, EmpRegId = q.EmpRegId, Employee = q.Employee }).ToList();
            return ans;

        }

        public void addCircular(int acid, DateTime date, string heading, string reason, int empRegId)
        {
            Circular c = new Circular()
            {
                AcademicYearId = acid,
                IssuedOn = date,
                Heading = heading,
                Reason = reason,
                IssuedBy = empRegId,
                Status = true
            };
            dc.Circulars.Add(c);
            dc.SaveChanges();

        }
        public void updateCircular(int CircularId, DateTime date, string heading, string reason, int empRegId, string Status)
        {
            var update = dc.Circulars.Where(q => q.CricularId.Equals(CircularId)).First();
            update.IssuedOn = date;
            update.Heading = heading;
            update.Reason = reason;
            update.IssuedBy = empRegId;
            if (Status == "Active")
            {
                update.Status = true;
            }

            else
            {
                update.Status = false;
            }

            dc.SaveChanges();
        }

        public void deleteCircular(int CircularId)
        {
            var delete = dc.Circulars.Where(q => q.CricularId.Equals(CircularId)).First();
            dc.Circulars.Remove(delete);
            dc.SaveChanges();
        }

        public List<Circulars> GetDailyCircular(int yearid, DateTime dat)
        {
            var ans = (from a in dc.Circulars
                       from b in dc.TechEmployees
                       from c in dc.AcademicYears
                       where a.AcademicYearId == yearid && a.IssuedOn == dat && b.EmployeeRegisterId == a.IssuedBy && a.AcademicYearId == c.AcademicYearId
                       select new Circulars
                       {
                           AcademicYear = c.AcademicYear1,
                           ddates = SqlFunctions.DateName("day", a.IssuedOn).Trim() + "-" + SqlFunctions.StringConvert((double)a.IssuedOn.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.IssuedOn),
                           tacYSD = a.IssuedOn,
                           CircularId = a.CricularId,
                           Heading = a.Heading,
                           Reason = a.Reason,
                           Employee = b.EmployeeName +" "+b.LastName,
                           employeeid = b.EmployeeId
                       }).ToList();

            return ans;
        }
        public List<Circulars> GetFromToCircular(int yearid, DateTime From, DateTime To)
        {
            var ans = (from a in dc.Circulars
                       from b in dc.TechEmployees
                       from c in dc.AcademicYears
                       where a.AcademicYearId == yearid && a.IssuedOn >= From && a.IssuedOn <= To && b.EmployeeRegisterId == a.IssuedBy && a.AcademicYearId == c.AcademicYearId
                       select new Circulars
                       {
                           AcademicYear = c.AcademicYear1,
                           ddates = SqlFunctions.DateName("day", a.IssuedOn).Trim() + "-" + SqlFunctions.StringConvert((double)a.IssuedOn.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.IssuedOn),
                           tacYSD = a.IssuedOn,
                           CircularId = a.CricularId,
                           Heading = a.Heading,
                           Reason = a.Reason,
                           Employee = b.EmployeeName + " " + b.LastName,
                           employeeid = b.EmployeeId
                       }).OrderBy(q => q.CircularId).ToList();

            return ans;
        }
        public List<Circulars> GetMonthCircular(int yearid, int monthid)
        {
            var ans = (from a in dc.Circulars
                       from b in dc.TechEmployees
                       from c in dc.AcademicYears
                       where a.AcademicYearId == yearid && a.IssuedOn.Value.Month == monthid && b.EmployeeRegisterId == a.IssuedBy && a.AcademicYearId == c.AcademicYearId
                       select new Circulars
                       {
                           AcademicYear = c.AcademicYear1,
                           ddates = SqlFunctions.DateName("day", a.IssuedOn).Trim() + "-" + SqlFunctions.StringConvert((double)a.IssuedOn.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.IssuedOn),
                           tacYSD = a.IssuedOn,
                           CircularId = a.CricularId,
                           Heading = a.Heading,
                           Reason = a.Reason,
                           Employee = b.EmployeeName + " " + b.LastName,
                           employeeid = b.EmployeeId
                       }).OrderBy(q => q.CircularId).ToList();

            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}