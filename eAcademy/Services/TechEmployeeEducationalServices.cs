﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class TechEmployeeEducationalServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public List<M_TechEmployeeEducational> GetEmployeeEducationalDetails(int EmployeeId)
        {
            var res = (from a in dc.TechEmployeeEducations
                       where a.EmployeeRegisterId == EmployeeId
                       select new M_TechEmployeeEducational
                       {
                           EducationalDetailsId=a.TechEmployeeEducationId,
                           Cours = a.Cours,
                           Institute = a.Institute,
                           University = a.University,
                           YearOfCompletion = a.YearOfCompletion,
                           Mark = a.Mark
                       }).ToList();
            return res;
        }
        public List<M_TechEmployeeEducational> GetOtherEmployeeEducationalDetails(int EmployeeId)
        {
            var res = (from a in dc.TechEmployeeEducations
                       where a.OtherEmployeeRegisterId == EmployeeId
                       select new M_TechEmployeeEducational
                       {
                           EducationalDetailsId = a.TechEmployeeEducationId,
                           Cours = a.Cours,
                           Institute = a.Institute,
                           University = a.University,
                           YearOfCompletion = a.YearOfCompletion,
                           Mark = a.Mark
                       }).ToList();
            return res;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

       
    }
}