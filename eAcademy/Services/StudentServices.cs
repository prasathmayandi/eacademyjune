﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class StudentServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        public Int32 totalstudentstrength()
        {
            var ans = db.Students.Where(q => q.StudentStatus == true).ToList().Count;
            return ans;
        }
        public Student CheckGuardianStudent_Credential(int studentRegId, string studentId, DateTime DOB)
        {
            var row = (from a in db.Students where a.StudentRegisterId == studentRegId && a.StudentId == studentId && a.DOB == DOB select a).FirstOrDefault();
            return row;
        }

        public Student getStudentRow(int? studentRegId)
        {
            var row = (from a in db.Students where a.StudentRegisterId == studentRegId select a).FirstOrDefault();
            return row;
        }

        public IEnumerable<Object> getStudentListToAssignClass(int yearId, int classId)
        {
            var PreviousAcYearIdList = (from a in db.AcademicYears where a.AcademicYearId < yearId select a).ToList().Count;

            int PreviousAcYearId = 0;
            if (PreviousAcYearIdList != 0)
            {
                var PreviousAcYearIdListId = (from a in db.AcademicYears where a.AcademicYearId < yearId select a.AcademicYearId).ToList().Max();
                PreviousAcYearId = PreviousAcYearIdListId;
            }
            int PreviousClassId;
            if (classId == 1)
            {
                PreviousClassId = 0;
            }
            else
            {
                PreviousClassId = (from a in db.Classes where a.ClassId < classId select a.ClassId).Max();
            }
            var List = (((from a in db.Students
                          from b in db.BoardofSchools
                          where a.AcademicyearId == yearId &&
                          a.AdmissionClass == classId &&
                          a.StudentStatus == true &&
                          a.Statusflag == "Accepted" 
                          select new
                          {
                              StudentRegId = a.StudentRegisterId,
                              FName = a.FirstName,
                              LName = a.LastName,
                              Gender = a.Gender,
                              FirstLanguage = a.FirstLanguage,
                              MediumOfStudy = a.MediumOfStudy,
                              WayOfEnrollment = a.ApplicationSource
                          }).ToList().Select(a => new
                          {
                              StudentRegId = QSCrypt.Encrypt(a.StudentRegId.ToString()),
                              StuRegId = a.StudentRegId,
                              FName = a.FName,
                              LName = a.LName,
                              Gender = a.Gender,
                              FirstLanguage = a.FirstLanguage,
                              MediumOfStudy = a.MediumOfStudy,
                              WayOfEnrollment = a.WayOfEnrollment,
                              StudentStatus = "New Entry",
                              BoardOfPreviousSchool = Boardname(a.StudentRegId),
                          })).ToList()

                        .Union((from a in db.AssignClassToStudents
                                from b in db.Students
                                from c in db.BoardofSchools
                                where a.StudentRegisterId == b.StudentRegisterId &&
                                a.AcademicYearId == PreviousAcYearId &&
                                a.ClassId == PreviousClassId &&
                                a.Result == "Pass" &&
                                a.ReAppear != "Yes" &&
                                a.ClassAssignedForNextAcademicYear != "Yes" &&
                                a.StudentEducationStatus == "Completed" &&
                                b.StudentStatus == true 
                                select new
                                {
                                    StudentRegId = b.StudentRegisterId,
                                    FName = b.FirstName,
                                    LName = b.LastName,
                                    Gender = b.Gender,
                                    FirstLanguage = b.FirstLanguage,
                                    MediumOfStudy = b.MediumOfStudy,
                                    WayOfEnrollment = b.ApplicationSource,
                                }).ToList().Select(a => new
                                {
                                    StudentRegId = QSCrypt.Encrypt(a.StudentRegId.ToString()),
                                    StuRegId = a.StudentRegId,
                                    FName = a.FName,
                                    LName = a.LName,
                                    Gender = a.Gender,
                                    FirstLanguage = a.FirstLanguage,
                                    MediumOfStudy = a.MediumOfStudy,
                                    WayOfEnrollment = a.WayOfEnrollment,
                                    StudentStatus = "Pass Student",
                                    BoardOfPreviousSchool = Boardname(a.StudentRegId)
                                })).ToList()

                    .Union((from a in db.AssignClassToStudents
                            from b in db.Students
                            from c in db.BoardofSchools
                            where a.StudentRegisterId == b.StudentRegisterId &&
                                  a.AcademicYearId == PreviousAcYearId &&
                            a.ClassId == classId &&
                            a.Result == "Fail" &&
                            a.ClassAssignedForNextAcademicYear != "Yes" &&
                            a.ReAppear != "Yes" &&
                            a.StudentEducationStatus == "Completed" &&
                            b.StudentStatus == true 
                            select new
                            {
                                StudentRegId = b.StudentRegisterId,
                                FName = b.FirstName,
                                LName = b.LastName,
                                Gender = b.Gender,
                                FirstLanguage = b.FirstLanguage,
                                MediumOfStudy = b.MediumOfStudy,
                                WayOfEnrollment = b.ApplicationSource,
                            }).ToList().Select(a => new
                            {
                                StudentRegId = QSCrypt.Encrypt(a.StudentRegId.ToString()),
                                StuRegId = a.StudentRegId,
                                FName = a.FName,
                                LName = a.LName,
                                Gender = a.Gender,
                                FirstLanguage = a.FirstLanguage,
                                MediumOfStudy = a.MediumOfStudy,
                                WayOfEnrollment = a.WayOfEnrollment,
                                StudentStatus = "Fail Student",
                                BoardOfPreviousSchool = Boardname(a.StudentRegId)
                            })).ToList());

            return List;
        }


        public string Boardname(int studentid)
        {
            var s = (from a in db.Students
                     from b in db.BoardofSchools
                     where a.StudentRegisterId == studentid && a.BoardOfPreviousSchool == b.BoardId
                     select b.BoardName).FirstOrDefault();
            return s;
        }

        public void updatestudentenrollmenttype(int studentid)
        {
            var s = (from a in db.Students where a.StudentRegisterId == studentid select a).First();
            s.EnrollmentType = "Excel";
            db.SaveChanges();
        }


        public int CopyFromStudentRegToStudent(PreAdmissionStudentRegister row)
        {

            Student add = new Student()
            {
                AdmissionId = row.StudentAdmissionId,

                FirstName = row.StuFirstName,
                MiddleName = row.StuMiddlename,
                LastName = row.StuLastname,
                Gender = row.Gender,
                DOB = row.DOB,
                PlaceOfBirth = row.PlaceOfBirth,
                StudentCommunity = row.Community,
                Religion = row.Religion,
                Nationality = row.Nationality,
                FirstLanguage = row.FirstLanguage, // no excel data upload
                AdmissionClass = row.AdmissionClass,
                StudentBloodGroup = row.BloodGroup,
                Height = row.Height,
                Weights = row.Weights,
                IdentificationMark = row.IdentificationMark,
                LocalAddress1 = row.LocAddress1,
                LocalAddress2 = row.LocAddress2,
                LocalCity = row.LocCity,
                LocalState = row.LocState,
                LocalCountry = row.LocCountry,
                LocalPostalCode = row.LocPostelcode,
                Email = row.Email,
                Contact = row.Contact,
                PermanentAddress1 = row.PerAddress1,
                PermanentAddress2 = row.PerAddress2,
                PermanentCity = row.PerCity,
                PermanentState = row.PerState,
                PermanentCountry = row.PerCountry,
                PermanentPostalCode = row.PerPostelcode,
                Photo = row.StudentPhoto,
                PreviousSchoolName = row.PreSchholName,
                BoardOfPreviousSchool = row.PreSchoolMedium,
                PreviousSchoolClass = row.PreSchoolClass,
                PreviousSchoolMark = row.PreSchoolMark, //no excel data upload
                PreviousSchoolFromDate = row.PreSchoolFromdate, //no excel data upload
                PreviousSchoolToDate = row.PreSchoolToDate, //no excel data upload
                IncomeCertificate = row.IncomeCertificate,
                BirthCertificate = row.BirthCertificate,
                CommunityCertificate = row.CommunityCertificate,
                TransferCertificate = row.TransferCertificate,
                NoOfSibling = row.NoOfSibling,
                AcademicyearId = row.AcademicyearId,
                Distance = row.Distance, //no excel data upload
                Statusflag = row.Statusflag,
                ApplicationSource = row.ApplicationSource,
                DOR = row.DateOfApply,
                HaveGuardian = row.GuardianRequried,
                HaveSiblings = row.SiblingStatus,//no excel data upload
                EmergencyContactPerson = row.EmergencyContactPersonName,
                EmergencyContactNumber = row.EmergencyContactNumber,
                ContactPersonRelationship = row.ContactPersonRelationship,
                StudentStatus = true,
                EnrolledBy = row.EnrolledBy,
                HostelRequired = row.HostelRequired,
                TransportRequired = row.TransportRequired
            };
            db.Students.Add(add);
            db.SaveChanges();
            int StudentRegid = add.StudentRegisterId;
            return StudentRegid;
        }

        public void UpdateStudentIdWithPassword(int StudentRegisterId, string StudentId, string salt, string password)
        {
            var getRow = (from a in db.Students where a.StudentRegisterId == StudentRegisterId select a).First();
            getRow.StudentId = StudentId;
            getRow.Salt = salt;
            getRow.Password = password;
            db.SaveChanges();
        }

        public Student getStudentRegIdByAdmissionId(int admissionId)
        {
            var getRegisterId = (from a in db.Students where a.AdmissionId == admissionId select a).FirstOrDefault();
            return getRegisterId;
        }

        public StudentProfile getStudentDetails(int StudentRegId)
        {
            var ans = (from a in db.Students
                       from b in db.tblBloodGroups
                       from c in db.tblCommunities
                       from d in db.tblCountries
                       where a.StudentRegisterId == StudentRegId &&
                       a.StudentBloodGroup == b.BloodId &&
                       a.StudentCommunity == c.CommunityId
                       // && a.LocalCountry == d.CountryId
                       select new StudentProfile
                       {
                           Photo = a.Photo,
                           StudentName = a.FirstName + " " + a.LastName,
                           Gender = a.Gender,
                           StudentId = a.StudentId,
                           Date_birth = SqlFunctions.DateName("day", a.DOB.Value).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOB.Value),
                           BloodGroup = b.BloodGroupName,
                           Email = a.Email,
                           Religion = a.Religion,
                           Community = c.CommunityName,
                           Nationality = a.Nationality,
                           Contact = a.Contact,
                           EmergencyContactPerson = a.EmergencyContactPerson,
                           EmergencyContactNumber = a.EmergencyContactNumber.Value,
                           Address = a.LocalAddress1,//+" "+a.LocalAddress2,
                           City = a.LocalCity,
                           State = a.LocalState,
                           Country = a.LocalCountry,
                           Date_join = SqlFunctions.DateName("day", a.DOJ.Value).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOJ.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOJ.Value),
                           Date_Register = SqlFunctions.DateName("day", a.DOR.Value).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOR.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOR.Value)
                       }).FirstOrDefault();
            return ans;
        }

        public void updateStudentProfile(int StudentRegId, string BloodGroup, string Email, string Religion, string Community, string Nationality, long? Contact, string EmergencyContactPerson, long? EmergencyContactNumber, string Address, string City, string State, string Student_allCountries)
        {
            var getRow = (from a in db.Students where a.StudentRegisterId == StudentRegId select a).FirstOrDefault();
        }

        public Student getMale(int? sId)
        {
            var Male = (from a in db.Students where a.StudentRegisterId == sId && a.Gender == "Male" select a).FirstOrDefault();
            return Male;
        }
        public Student getFemale(int? sId)
        {
            var Female = (from a in db.Students where a.StudentRegisterId == sId && a.Gender == "Female" select a).FirstOrDefault();
            return Female;
        }

        // class Incharge

        public CI_StudentDetails getStudentParentDetails(int id)
        {
            var ans = (from a in db.AdmissionTransactions
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       from e in db.tblBloodGroups
                       from f in db.tblCommunities
                       from g in db.tblCountries
                       from h in db.FatherRegisters
                       from i in db.MotherRegisters
                       from j in db.GuardianRegisters
                       from k in db.PrimaryUserRegisters
                       where a.StudentRegisterId == id &&
                       a.StudentRegisterId == b.StudentRegisterId &&
                       a.StudentRegisterId == c.StudentRegisterId &&
                       a.StudentRegisterId == d.StudentRegisterId &&
                       a.FatherRegisterId == h.FatherRegisterId &&
                       a.MotherRegisterId == i.MotherRegisterId &&
                       a.GuardianRegisterId == j.GuardianRegisterId &&
                       a.PrimaryUserRegisterId == k.PrimaryUserRegisterId
                       //&& k.CompanyCountry == g.CountryId

                       select new CI_StudentDetails
                       {
                           StudentId = b.StudentId,
                           StudentPhoto = b.Photo,
                           StudentRollNum = c.RollNumber,
                           StudentName = b.FirstName + " " + b.LastName,
                           StudentGender = b.Gender,
                           StudentDOB = b.DOB,
                           StudentBlood = e.BloodGroupName,
                           StudentEmail = b.Email,
                           StudentContact = b.Contact,
                           StudentEmergencyContactPerson = b.EmergencyContactPerson,
                           StudentEmergencyContact = b.EmergencyContactNumber,
                           StudentCommunity = f.CommunityName,
                           StudentReligion = b.Religion,
                           StudentNationality = b.Nationality,
                           StudentAddress = b.LocalAddress1 + " " + b.LocalAddress2,
                           StudentCity = b.LocalCity,
                           StudentState = b.LocalState,
                           StudentCountry = b.LocalCountry,
                           StudentHostel = d.HostelPreference,
                           StudentTransport = d.TransportPreference,
                           StudentDOR = b.DOR,
                           FatherName = h.FatherFirstName + " " + h.FatherLastName,
                           FatherDOB = h.FatherDOB,
                           FatherEmail = h.FatherEmail,
                           FatherContact = h.FatherMobileNo,
                           FatherQualification = h.FatherQualification,
                           FatherOccupation = h.FatherOccupation,
                           FatherWorkNumber = k.CompanyContact,
                           MotherName = i.MotherFirstname + "" + i.MotherLastName,
                           MotherDOB = i.MotherDOB,
                           MotherEmail = i.MotherEmail,
                           MotherContact = i.MotherMobileNo,
                           MotherQualification = i.MotherQualification,
                           MotherOccupation = i.MotherOccupation,
                           GuardianName = j.GuardianFirstname + " " + j.GuardianLastName,
                           GuardianDOB = j.GuardianDOB,
                           GuardianGender = j.GuardianGender,
                           GuardianRelation = j.GuRelationshipToChild,
                           GuardianEmail = j.GuardianEmail,
                           GuardianContact = j.GuardianMobileNo,
                           GuardianQualification = j.GuardianQualification,
                           GuardianOccupation = j.GuardianOccupation,
                           Income = k.TotalIncome,
                           Phone = k.PrimaryUserContactNo,
                           Address = k.CompanyAddress1 + " " + k.CompanyAddress2,
                           City = k.CompanyCity,
                           State = k.CompanyState,
                           Country = k.CompanyCountry,
                       }).FirstOrDefault();
            return ans;
        }

        public Student CheckStudentEmailExist(string StuEmail)
        {
            var CheckStudentEmailExist = (from be in db.Students where be.Email == StuEmail select be).FirstOrDefault();
            return CheckStudentEmailExist;
        }

        public Student getStudentRegId(string StuPhoto)
        {
            var getStudentRegId = (from ac in db.Students where ac.Photo == StuPhoto select ac).First();
            return getStudentRegId;
        }

        public void separateStudent(int sid)
        {
            var getStudent = (from a in db.Students where a.StudentRegisterId == sid select a).First();
            getStudent.StudentStatus = false;
            db.SaveChanges();
        }

        public IEnumerable<object> getStudentByID(int sec_id, int cid, int acid)
        {
            var ans = from t1 in db.AssignClassToStudents
                      from t2 in db.Students
                      where t1.StudentRegisterId == t2.StudentRegisterId && t1.SectionId == sec_id && t1.ClassId == cid && t1.AcademicYearId == acid && t1.Status == true && t1.HaveRollNumber == true
                      select new { sname = t2.FirstName + " " + t2.LastName, sid = t2.StudentRegisterId };
            return ans;
        }

        public IEnumerable<object> getStudentByIDs(int sec_id, int cid, int acid)
        {
            var ans = from t1 in db.AssignClassToStudents
                      from t2 in db.Students
                      where t1.StudentRegisterId == t2.StudentRegisterId && t1.SectionId == sec_id && t1.ClassId == cid && t1.AcademicYearId == acid && t1.Status == true
                      select new { sname = t2.FirstName + " " + t2.LastName, sid = t2.StudentRegisterId };
            return ans;
        }
        public IEnumerable<object> getStudentByID(int sec_id, int cid)
        {
            var ans = (from t1 in db.Students
                       from t2 in db.AssignClassToStudents
                       from t3 in db.Classes
                       from t4 in db.Sections
                       where t1.StudentRegisterId == t2.StudentRegisterId &&
                       t2.ClassId == t3.ClassId &&
                       t2.SectionId == t4.SectionId &&
                       t2.ClassId == cid &&
                       t2.SectionId == sec_id
                       select new { Sname = t1.FirstName + " " + t1.LastName, Sid = t1.StudentRegisterId }).ToList();
            return ans;

        }
        public IEnumerable<object> getStudent(int acid, int cid, int sec_id)
        {
            var ans = (from t1 in db.Students
                       from t2 in db.AssignClassToStudents
                       from t3 in db.AcademicYears
                       where t1.StudentRegisterId == t2.StudentRegisterId && t2.ClassId == cid && t2.SectionId == sec_id && t3.AcademicYearId == t2.AcademicYearId && t2.AcademicYearId == acid && t1.StudentStatus == true && t2.Status == true  //  && t2.HaveRollNumber == true 
                       select new { std = t1.FirstName + " " + t1.LastName, std_id = t1.StudentRegisterId }).ToList();
            return ans;
        }
        public IEnumerable<object> getStudentWhoHaveStudied(int acid, int cid, int sec_id)
        {
            var ans = (from t1 in db.Students
                       from t2 in db.AssignClassToStudents
                       from t3 in db.AcademicYears
                       where t1.StudentRegisterId == t2.StudentRegisterId && t2.ClassId == cid && t2.SectionId == sec_id && t3.AcademicYearId == t2.AcademicYearId && t2.AcademicYearId == acid   //  && t2.HaveRollNumber == true && t2.Status == true && t1.StudentStatus == true 
                       select new { std = t1.FirstName + " " + t1.LastName, std_id = t1.StudentRegisterId }).ToList();
            return ans;
        }
        public List<ClassStudent> getStudentOrderByStudentName(int acid, int cid, int sec_id)
        {
            var ans = (from t1 in db.Students
                       from t2 in db.AssignClassToStudents
                       where t1.StudentRegisterId == t2.StudentRegisterId && t2.ClassId == cid && t2.SectionId == sec_id && t2.AcademicYearId == acid && (t2.HaveRollNumber == null || t2.HaveRollNumber == false)
                       select new ClassStudent { StudentName = t1.FirstName + " " + t1.LastName, StudentRegId = t1.StudentRegisterId }).OrderBy(q => q.StudentName).ToList();
            return ans;
        }


        public List<ClassStudent> getNonRollNumberStudents(int acid, int cid, int sec_id)
        {
            var ans = (from t1 in db.Students
                       from t2 in db.AssignClassToStudents
                       where t1.StudentRegisterId == t2.StudentRegisterId && t2.ClassId == cid && t2.SectionId == sec_id && t2.AcademicYearId == acid && (t2.HaveRollNumber == null || t2.HaveRollNumber == false)
                       select new ClassStudent { StudentName = t1.FirstName + " " + t1.LastName, StudentRegId = t1.StudentRegisterId }).OrderBy(q => q.StudentName).ToList();
            return ans;
        }

        public List<ClassStudent> StudentDetails(int acid, int cid, int sec_id)
        {
            var ans = ((from t1 in db.Students
                        from t2 in db.AssignClassToStudents
                        from t3 in db.AdmissionTransactions
                        from t4 in db.StudentRollNumbers
                        from t5 in db.PrimaryUserRegisters

                        where t1.StudentRegisterId == t2.StudentRegisterId && t2.ClassId == cid && t2.SectionId == sec_id && t1.StudentRegisterId == t3.StudentRegisterId && t2.AcademicYearId == acid && t1.StudentRegisterId == t4.StudentRegisterId && t4.AcademicYearId == t2.AcademicYearId && t4.ClassId == t2.ClassId && t4.SectionId == t2.SectionId && t2.HaveRollNumber == true && t5.PrimaryUserRegisterId == t3.PrimaryUserRegisterId && t1.StudentStatus == true
                        select new ClassStudent { AcademicYearId = t2.AcademicYearId.Value, StudentName = t1.FirstName + "-" + t1.LastName, Hostel = t2.HostelPreference, Emg_Person = t1.EmergencyContactPerson, Emg_Contact = t1.EmergencyContactNumber.Value, ClassId = t2.ClassId.Value, SectionId = t2.SectionId.Value, StudentRegId = t1.StudentRegisterId, RollNumber = t4.RollNumber, StudentId = t1.StudentId, PrimaryUserEmail = t5.PrimaryUserEmail, PrimaryUserName = t5.PrimaryUserName }).ToList()).
                            Select(q => new ClassStudent
                            {
                                AcademicYearId = q.AcademicYearId,
                                StudentName = q.StudentName,
                                PrimaryUserName = q.PrimaryUserName,
                                PrimaryUserEmail = q.PrimaryUserEmail,
                                Hostel = q.Hostel,
                                EmergencyContactPerson = q.Emg_Person,
                                ContactpersonContact = q.Emg_Contact,
                                ClassId = q.ClassId.Value,
                                SectionId = q.SectionId.Value,
                                StudentRegId = q.StudentRegId,
                                RollNumber = q.RollNumber,
                                StudentId = q.StudentId,
                                StdRegid = QSCrypt.Encrypt(q.StudentRegId.ToString())
                            }).ToList();
            return ans;
        }

        public List<ClassStudent> StudentDetails()
        {
            var ans = ((from t1 in db.Students
                        from t2 in db.AssignClassToStudents
                        from t3 in db.AdmissionTransactions
                        from t4 in db.StudentRollNumbers
                        from t5 in db.FatherRegisters
                        from t6 in db.MotherRegisters
                        from t7 in db.GuardianRegisters
                        where t1.StudentRegisterId == t2.StudentRegisterId && t1.StudentRegisterId == t3.StudentRegisterId && t1.StudentRegisterId == t4.StudentRegisterId && t4.AcademicYearId == t2.AcademicYearId && t4.ClassId == t2.ClassId && t4.SectionId == t2.SectionId && t3.FatherRegisterId == t5.FatherRegisterId && t3.MotherRegisterId == t6.MotherRegisterId && t3.GuardianRegisterId == t7.GuardianRegisterId
                        select new ClassStudent { AcademicYearId = t2.AcademicYearId.Value, StudentName = t1.FirstName + "-" + t1.LastName, FatherName = t5.FatherFirstName + " " + t5.FatherLastName, FatherContact = t5.FatherMobileNo.Value, MotherName = t6.MotherFirstname + " " + t6.MotherLastName, ParentName = t5.FatherFirstName + "-" + t6.MotherFirstname, MotherContact = t6.MotherMobileNo.Value, GuardianName = t7.GuardianFirstname, GuardiaContact = t7.GuardianMobileNo.Value, Hostel = t2.HostelPreference, Emg_Person = t1.EmergencyContactPerson, Emg_Contact = t1.EmergencyContactNumber.Value, ClassId = t2.ClassId.Value, SectionId = t2.SectionId.Value, StudentRegId = t1.StudentRegisterId, RollNumber = t4.RollNumber, StudentId = t1.StudentId }).ToList()).
                            Select(q => new ClassStudent
                            {
                                AcademicYearId = q.AcademicYearId,
                                StudentName = q.StudentName,
                                FatherName = q.FatherName,
                                FatherContact = q.FatherContact,
                                MotherName = q.MotherName,
                                ParentName = q.ParentName,
                                MotherContact = q.MotherContact,
                                GuardianName = q.GuardianName,
                                GuardiaContact = q.GuardiaContact,
                                Hostel = q.Hostel,
                                Emg_Person = q.Emg_Person,
                                Emg_Contact = q.Emg_Contact,
                                ClassId = q.ClassId.Value,
                                SectionId = q.SectionId.Value,
                                StudentRegId = q.StudentRegId,
                                RollNumber = q.RollNumber,
                                StudentId = q.StudentId,
                                StdRegid = QSCrypt.Encrypt(q.StudentRegId.ToString())
                            }).ToList();
            return ans;
        }

        public Student getStudentRow(int regId)
        {
            var ans = (from a in db.Students where a.StudentRegisterId == regId && a.StudentStatus == true select a).FirstOrDefault();
            return ans;
        }

        public void updateStatusFlag(int regId)
        {
            var row = (from a in db.Students where a.StudentRegisterId == regId select a).FirstOrDefault();
            row.Statusflag = "Class Assigned";
            db.SaveChanges();
        }

        public StudentProfile getStudentListByRegId(int std_id)
        {
            var ans = (from a in db.Students
                       from b in db.tblBloodGroups
                       from c in db.tblCommunities
                       from d in db.tblCountries
                       where a.StudentRegisterId == std_id &&
                       a.StudentBloodGroup == b.BloodId &&
                       a.StudentCommunity == c.CommunityId
                       select new StudentProfile
                       {
                           Photo = a.Photo,
                           StudentName = a.FirstName + " " + a.LastName,
                           Gender = a.Gender,
                           StudentId = a.StudentId,
                           Date_birth = SqlFunctions.DateName("day", a.DOB.Value).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOB.Value),
                           BloodGroup = b.BloodGroupName,
                           Email = a.Email,
                           Religion = a.Religion,
                           Community = c.CommunityName,
                           Nationality = a.Nationality,
                           Contact = a.Contact,
                           EmergencyContactPerson = a.EmergencyContactPerson,
                           EmergencyContactNumber = a.EmergencyContactNumber.Value,
                           Address = a.LocalAddress1,// + " " + a.LocalAddress2,
                           City = a.LocalCity,
                           State = a.LocalState,
                           Country = a.LocalCountry,
                           Date_join = SqlFunctions.DateName("day", a.DOJ.Value).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOJ.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOJ.Value),
                           Date_Register = SqlFunctions.DateName("day", a.DOR.Value).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOR.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOR.Value)
                       }).FirstOrDefault();
            return ans;
        }

        public ClassStudent getStudentByRegId(int std_id)
        {
            var std = db.Students.Where(q => q.StudentRegisterId.Equals(std_id)).Select(s => new ClassStudent { StudentName = s.FirstName + " " + s.LastName }).FirstOrDefault();
            return std;
        }

        public IEnumerable<object> getTransportStudents(int acid, int cid, int sec_id)
        {
            var ans = from t1 in db.AssignClassToStudents
                      from t2 in db.Students
                      where t1.StudentRegisterId == t2.StudentRegisterId && t1.SectionId == sec_id && t1.ClassId == cid && t1.AcademicYearId == acid && t1.Status == true && t1.HaveRollNumber == true
                      select new { sname = t2.FirstName + " " + t2.LastName, sid = t2.StudentRegisterId };
            return ans;
        }

        internal ClassStudent getStudentByStudentId(string RegNo)
        {
            var ans = (from t1 in db.Students
                       from t2 in db.AssignClassToStudents
                       from t3 in db.AcademicYears
                       from t4 in db.Classes
                       from t5 in db.Sections
                       where t1.StudentId == RegNo && t1.StudentRegisterId == t2.StudentRegisterId && t2.AcademicYearId == t3.AcademicYearId && t2.ClassId == t4.ClassId && t2.SectionId == t5.SectionId && t2.Status == true
                       select new ClassStudent { AcademicYearId = t2.AcademicYearId.Value, ClassId = t2.ClassId, SectionId = t2.SectionId, StudentRegId = t1.StudentRegisterId }).SingleOrDefault();
            return ans;
        }

        public ClassStudent getstudentassignhostelornot(string RegNo)
        {
            var ans = (from t1 in db.Students
                       from t2 in db.AssignClassToStudents
                       from t3 in db.RoomAllotments
                       where t1.StudentId == RegNo && t1.StudentRegisterId == t2.StudentRegisterId && t3.StudentRegisterId == t2.StudentRegisterId && t2.Status == true
                       select new ClassStudent { StudentRegId = t1.StudentRegisterId }).SingleOrDefault();
            return ans;
        }

        internal ClassStudent getStudentId(int acid, int cid, int sec_id, int std_id)
        {
            var ans = (from t1 in db.Students
                       from t2 in db.AssignClassToStudents

                       where t2.AcademicYearId == acid && t2.ClassId == cid && t2.SectionId == sec_id && t1.StudentRegisterId == std_id && t1.StudentRegisterId == t2.StudentRegisterId && t2.Status == true
                       select new ClassStudent { StudentId = t1.StudentId }).FirstOrDefault();
            return ans;
        }



        internal List<StudentsResults> getStudentwithResult(int acid, int cid, int sec_id)
        {
            var ans = (from t1 in db.AssignClassToStudents
                       from t2 in db.Students
                       from t3 in db.StudentRollNumbers
                       where t1.StudentRegisterId == t2.StudentRegisterId &&
                       t1.StudentRegisterId == t3.StudentRegisterId &&
                       t1.AcademicYearId == acid && t1.ClassId == cid && t1.SectionId == sec_id &&
                       t3.AcademicYearId == acid && t3.ClassId == cid && t3.SectionId == sec_id &&
                       t2.StudentStatus == true && t3.Status == true //&& t1.ClassAssignedForNextAcademicYear != "Yes"
                       select new StudentsResults { StudentRegId = t2.StudentRegisterId, StudentName = t2.FirstName + " " + t2.LastName, StudentRollNumber = t3.RollNumber, StudentId = t2.StudentId, StudentEducationStatus = t1.StudentEducationStatus, SchoolingCompleted = t1.ReAppear, FinalResult = t1.Result, ClassAssignedForNextAcYear = t1.ClassAssignedForNextAcademicYear }).OrderBy(q => q.StudentRegId).ToList();

            return ans;
        }

        public void updateSeparateStudentStatus(int studentRegId, int adminId, int yearId, int classId, int secId)
        {
            var getRow = (from a in db.Students where a.StudentRegisterId == studentRegId && a.StudentStatus == true select a).FirstOrDefault();
            getRow.StudentStatus = false;
            getRow.Statusflag = "Separated";
            db.SaveChanges();

            var getRow1 = (from a in db.AssignClassToStudents where a.StudentRegisterId == studentRegId && a.Status == true && a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId select a).FirstOrDefault();
            getRow1.SeparationStatus = "True";
            getRow1.SeparatedBy = adminId;
            db.SaveChanges();
        }

        public StudentPreviousYearDetail getJoinedClassDetail(int studentRegId)
        {
            var row = (from a in db.Students
                       from b in db.AcademicYears
                       from c in db.Classes
                       where a.AcademicyearId == b.AcademicYearId &&
                       a.AdmissionClass == c.ClassId &&
                       a.StudentRegisterId == studentRegId
                       select new StudentPreviousYearDetail
                       {
                           PreviousAcYear = b.AcademicYear1,
                           PreviousAcClass = c.ClassType
                       }).FirstOrDefault();
            return row;
        }

        //jegadeesh
        public IEnumerable GetParticularStudentName(int stu_id)
        {
            var ans = db.Students.Where(q => q.StudentRegisterId.Equals(stu_id)).FirstOrDefault().FirstName;
            return ans;
        }
        public List<Student> SearchStudentProfile(int id)
        {
            var q1 = db.Students.Where(q => q.StudentRegisterId.Equals(id)).ToList();
            return q1;
        }
        //Student Annual Due List
        public List<Ranklist> GetAnnualStudentNameRollNumber(int yid, int cid, int sid, List<AssignClassToStudent> a1)
        {
            var ans = (from a in a1
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.StudentRegisterId == b.StudentRegisterId && c.StudentRegisterId == b.StudentRegisterId && c.StudentRegisterId == a.StudentRegisterId && a.Status == true
                       select new Ranklist
                       {
                           StudentId = b.StudentId,
                           rollnumber = c.RollNumber,
                           stuname = b.FirstName + " " + b.LastName
                       }).OrderBy(q => q.rollnumber).ToList();

            return ans;
        }

        public List<AssignClassToStudent> GetAnnualDuestudentName(int? s, List<AssignClassToStudent> a1)
        {
            var ans = a1.Where(x => x.StudentRegisterId != s).ToList();
            return ans;
        }

        //Student Term Due List
        public List<Ranklist> GetTermStudentNameRollNumber(int yid, int cid, int sid, List<AssignClassToStudent> a1)
        {
            var ans = (from a in a1
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.StudentRegisterId == b.StudentRegisterId && c.StudentRegisterId == b.StudentRegisterId && c.StudentRegisterId == a.StudentRegisterId
                       select new Ranklist
                       {
                           rollnumber = c.RollNumber,
                           stuname = b.FirstName + " " + b.LastName
                       }).OrderBy(q => q.rollnumber).ToList();
            return ans;
        }
        public List<AssignClassToStudent> GetTermDuestudentName(int s, List<AssignClassToStudent> a1)
        {
            var ans = a1.Where(x => x.StudentRegisterId != s).ToList();
            return ans;
        }

        //jegadeesh

        public List<StudentDetails> getStudentProfile(int id)
        {
            var res = (from a in db.Students
                       from b in db.tblBloodGroups
                       from c in db.tblCommunities
                       where a.StudentRegisterId == id &&
                       a.StudentBloodGroup == b.BloodId && c.CommunityId == a.StudentCommunity
                       select new StudentDetails
                       {
                           StudentRegId = a.StudentRegisterId,
                           studentId = a.StudentId,
                           StudentName = a.FirstName + " " + a.LastName,
                           Gender = a.Gender,
                           Stu_DateOfBirth = SqlFunctions.DateName("day", a.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOB),
                           Bloodgroup = b.BloodGroupName,
                           Student_Email = a.Email,
                           Religion = a.Religion,
                           Community = c.CommunityName,
                           Nationality = a.Nationality,
                           Address = a.LocalAddress1 + "," + a.LocalAddress2,
                           City = a.LocalCity,
                           State = a.LocalState,
                           Contact = a.Contact,
                           Emg_Contact = a.EmergencyContactNumber,
                           Emg_Person = a.EmergencyContactPerson,
                           DateOfJoin = SqlFunctions.DateName("day", a.DOJ).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOJ.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOJ),
                           DateOfRegister = SqlFunctions.DateName("day", a.DOR).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOR.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOR),
                           DateOfLeave = SqlFunctions.DateName("day", a.DOL).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOL.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOL),
                           student_photo = a.Photo
                       }).ToList();
            return res;
        }

        //Get Student Register Id
        public Student GetStudentRegisterIdbyStudentid(string sid)
        {
            var ans = db.Students.Where(q => q.StudentId == sid).FirstOrDefault();
            return ans;
        }
        public Student GetStudentRegisterIdbyapplicationNumber(int Aid)
        {
            var ans = db.Students.Where(q => q.AdmissionId == Aid).FirstOrDefault();
            return ans;
        }

        //get Student&parents Details

        public List<M_StudentRegisteration> GetStudentalldetails(int sid, string uname)
        {
            List<M_StudentRegisteration> SS = new List<M_StudentRegisteration>();
            var PrimaryUserId = db.PrimaryUserRegisters.Where(q => q.PrimaryUserEmail == uname).ToList();
            if (PrimaryUserId.Count != 0)
            {
                int pid = PrimaryUserId.FirstOrDefault().PrimaryUserRegisterId;
                var Trans = db.AdmissionTransactions.Where(q => q.PrimaryUserRegisterId.Value.Equals(pid) && q.StudentRegisterId == sid).FirstOrDefault(); //OrderByDescending(q => q.AdmissionTransactionId)
                if (Trans.StudentRegisterId != null)
                {
                    if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Father")
                    {
                        if (Trans.MotherRegisterId != null && Trans.GuardianRegisterId != null)
                        {
                            var result = NewStudentAllUserdetails(pid, sid);
                            SS = result;
                        }
                        else if (Trans.MotherRegisterId != null && Trans.GuardianRegisterId == null)
                        {
                            var result = NewStudentFatherMotherDetails(pid, sid);
                            SS = result;
                        }
                        else if (Trans.MotherRegisterId == null && Trans.GuardianRegisterId != null)
                        {
                            var result = NewStudentFatherGuardianDetails(pid, sid);
                            SS = result;
                        }
                        else
                        {
                            var result = NewStudentFatherDetails(pid, sid);
                            SS = result;
                        }
                    }
                    else if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Mother")
                    {
                        if (Trans.FatherRegisterId != null && Trans.GuardianRegisterId != null)
                        {
                            var result = NewStudentAllUserdetails(pid, sid);
                            SS = result;
                        }
                        else if (Trans.FatherRegisterId != null && Trans.GuardianRegisterId == null)
                        {
                            var result = NewStudentFatherMotherDetails(pid, sid);
                            SS = result;
                        }
                        else if (Trans.FatherRegisterId == null && Trans.GuardianRegisterId != null)
                        {
                            var result = NewStudentMotherGuardianDetails(pid, sid);
                            SS = result;
                        }
                        else
                        {
                            var result = NewStudentMotherDetails(pid, sid);
                            SS = result;
                        }
                    }
                    else if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Guardian")
                    {
                        if (Trans.FatherRegisterId != null && Trans.MotherRegisterId != null)
                        {
                            var result = NewStudentAllUserdetails(pid, sid);
                            SS = result;
                        }
                        else if (Trans.FatherRegisterId != null && Trans.MotherRegisterId == null)
                        {
                            var result = NewStudentFatherGuardianDetails(pid, sid);
                            SS = result;
                        }
                        else if (Trans.FatherRegisterId == null && Trans.MotherRegisterId != null)
                        {
                            var result = NewStudentMotherGuardianDetails(pid, sid);
                            SS = result;
                        }
                        else
                        {
                            var result = NewStudentGuardianDetails(pid, sid);
                            SS = result;
                        }
                    }
                }
            }
            return SS;
        }

        public List<M_StudentRegisteration> NewStudentAllUserdetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in db.PrimaryUserRegisters
                       from trans in db.AdmissionTransactions
                       from stu in db.Students
                       from fa in db.FatherRegisters
                       from mo in db.MotherRegisters
                       from gu in db.GuardianRegisters
                       from f in db.BoardofSchools
                       where trans.PrimaryUserRegisterId == PrimaryuserId && trans.StudentRegisterId == StudentId && stu.StudentRegisterId == trans.StudentRegisterId && Priuser.PrimaryUserRegisterId == trans.PrimaryUserRegisterId
                                               && fa.FatherRegisterId == trans.FatherRegisterId && mo.MotherRegisterId == trans.MotherRegisterId && gu.GuardianRegisterId == trans.GuardianRegisterId                        //&& f.BoardId==stu.BoardOfPreviousSchool
                       select new M_StudentRegisteration
                       {
                           StudentId = stu.StudentId,
                           Stuadmissionid = stu.StudentRegisterId,
                           StuFirstName = stu.FirstName,
                           StuMiddlename = stu.MiddleName,
                           StuLastname = stu.LastName,
                           Gender = stu.Gender,
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.StudentCommunity,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.StudentBloodGroup,
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),
                           txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),
                           txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),
                           AdmissionClass = stu.AdmissionClass,
                           FatherFirstName = fa.FatherFirstName,
                           FatherOccupation = fa.FatherOccupation,
                           FatherMobileNo = fa.FatherMobileNo,
                           FatherEmail = fa.FatherEmail,
                           MotherFirstname = mo.MotherFirstname,
                           MotherOccupation = mo.MotherOccupation,
                           MotherMobileNo = mo.MotherMobileNo,
                           MotherEmail = mo.MotherEmail,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           GuardianFirstname = gu.GuardianFirstname,
                           GuardianLastName = gu.GuardianLastName,
                           GuRelationshipToChild = gu.GuRelationshipToChild,
                           GuardianDOB = gu.GuardianDOB,
                           GuardianGender = gu.GuardianGender,
                           GuardianQualification = gu.GuardianQualification,
                           GuardianMobileNo = gu.GuardianMobileNo,
                           GuardianEmail = gu.GuardianEmail,
                           GuardianOccupation = gu.GuardianOccupation,
                           GuardianIncome = gu.GuardianIncome,
                           LocAddress1 = stu.LocalAddress1,
                           LocAddress2 = stu.LocalAddress2,
                           LocCity = stu.LocalCity,
                           LocState = stu.LocalState,
                           LocCountry = stu.LocalCountry,
                           LocPostelcode = stu.LocalPostalCode,
                           StudentPhoto = stu.Photo,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreviousSchoolName,
                           //PreMedium = f.BoardName,
                           Pre_BoardofSchool = stu.BoardOfPreviousSchool,
                           PreMarks = stu.PreviousSchoolMark,
                           PreClass = stu.PreviousSchoolClass,
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreviousSchoolFromDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreviousSchoolFromDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreviousSchoolFromDate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreviousSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreviousSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreviousSchoolToDate),
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPerson,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PermanentAddress1,
                           PerAddress2 = stu.PermanentAddress2,
                           PerCity = stu.PermanentCity,
                           PerState = stu.PermanentState,
                           PerCountry = stu.PermanentCountry,
                           PerPostelcode = stu.PermanentPostalCode,
                           FatherLastName = fa.FatherLastName,
                           MotherLastName = mo.MotherLastName,
                           FatherDOB = fa.FatherDOB,
                           MotherDOB = mo.MotherDOB,
                           TotalIncome = Priuser.TotalIncome,
                           FatherQualification = fa.FatherQualification,
                           MotherQualification = mo.MotherQualification,
                           GuardianRequried = stu.HaveGuardian,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           WorkSameSchool = Priuser.WorkSameSchool
                       }).Distinct().ToList();
            return res;
        }

        public List<M_StudentRegisteration> NewStudentFatherMotherDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in db.PrimaryUserRegisters
                       from trans in db.AdmissionTransactions
                       from stu in db.Students
                       from fa in db.FatherRegisters
                       from mo in db.MotherRegisters
                       from f in db.BoardofSchools
                       where trans.PrimaryUserRegisterId == PrimaryuserId && trans.StudentRegisterId == StudentId && stu.StudentRegisterId == trans.StudentRegisterId
                        && fa.FatherRegisterId == trans.FatherRegisterId && mo.MotherRegisterId == trans.MotherRegisterId && Priuser.PrimaryUserRegisterId == trans.PrimaryUserRegisterId                       //&& f.BoardId == stu.BoardOfPreviousSchool
                       select new M_StudentRegisteration
                       {
                           StudentId = stu.StudentId,
                           Stuadmissionid = stu.StudentRegisterId,
                           StuFirstName = stu.FirstName,
                           StuMiddlename = stu.MiddleName,
                           StuLastname = stu.LastName,
                           Gender = stu.Gender,
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.StudentCommunity,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.StudentBloodGroup,
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),
                           txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),
                           AdmissionClass = stu.AdmissionClass,
                           FatherFirstName = fa.FatherFirstName,
                           FatherOccupation = fa.FatherOccupation,
                           FatherMobileNo = fa.FatherMobileNo,
                           FatherEmail = fa.FatherEmail,
                           MotherFirstname = mo.MotherFirstname,
                           MotherOccupation = mo.MotherOccupation,
                           MotherMobileNo = mo.MotherMobileNo,
                           MotherEmail = mo.MotherEmail,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           LocAddress1 = stu.LocalAddress1,
                           LocAddress2 = stu.LocalAddress2,
                           LocCity = stu.LocalCity,
                           LocState = stu.LocalState,
                           LocCountry = stu.LocalCountry,
                           LocPostelcode = stu.LocalPostalCode,
                           StudentPhoto = stu.Photo,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreviousSchoolName,
                           Pre_BoardofSchool = stu.BoardOfPreviousSchool,
                           //PreMedium = f.BoardName,
                           PreMarks = stu.PreviousSchoolMark,
                           PreClass = stu.PreviousSchoolClass,
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreviousSchoolFromDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreviousSchoolFromDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreviousSchoolFromDate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreviousSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreviousSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreviousSchoolToDate),
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPerson,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PermanentAddress1,
                           PerAddress2 = stu.PermanentAddress2,
                           PerCity = stu.PermanentCity,
                           PerState = stu.PermanentState,
                           PerCountry = stu.PermanentCountry,
                           PerPostelcode = stu.PermanentPostalCode,
                           FatherLastName = fa.FatherLastName,
                           MotherLastName = mo.MotherLastName,
                           FatherDOB = fa.FatherDOB,
                           MotherDOB = mo.MotherDOB,
                           TotalIncome = Priuser.TotalIncome,
                           FatherQualification = fa.FatherQualification,
                           MotherQualification = mo.MotherQualification,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           GuardianRequried = stu.HaveGuardian,
                           WorkSameSchool = Priuser.WorkSameSchool
                       }).Distinct().ToList();
            return res;
        }

        public List<M_StudentRegisteration> NewStudentFatherGuardianDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in db.PrimaryUserRegisters
                       from trans in db.AdmissionTransactions
                       from stu in db.Students
                       from fa in db.FatherRegisters
                       from gu in db.GuardianRegisters
                       from f in db.BoardofSchools
                       where trans.PrimaryUserRegisterId == PrimaryuserId && trans.StudentRegisterId == StudentId && stu.StudentRegisterId == trans.StudentRegisterId
                        && fa.FatherRegisterId == trans.FatherRegisterId && gu.GuardianRegisterId == trans.GuardianRegisterId && Priuser.PrimaryUserRegisterId == trans.PrimaryUserRegisterId                 //&& f.BoardId == stu.BoardOfPreviousSchool
                       select new M_StudentRegisteration
                       {
                           StudentId = stu.StudentId,
                           Stuadmissionid = stu.StudentRegisterId,
                           StuFirstName = stu.FirstName,
                           StuMiddlename = stu.MiddleName,
                           StuLastname = stu.LastName,
                           Gender = stu.Gender,
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),
                           txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.StudentCommunity,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.StudentBloodGroup,
                           AdmissionClass = stu.AdmissionClass,
                           FatherFirstName = fa.FatherFirstName,
                           FatherOccupation = fa.FatherOccupation,
                           FatherMobileNo = fa.FatherMobileNo,
                           FatherEmail = fa.FatherEmail,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           GuardianFirstname = gu.GuardianFirstname,
                           GuardianLastName = gu.GuardianLastName,
                           GuRelationshipToChild = gu.GuRelationshipToChild,
                           GuardianDOB = gu.GuardianDOB,
                           GuardianGender = gu.GuardianGender,
                           GuardianQualification = gu.GuardianQualification,
                           GuardianMobileNo = gu.GuardianMobileNo,
                           GuardianEmail = gu.GuardianEmail,
                           GuardianOccupation = gu.GuardianOccupation,
                           GuardianIncome = gu.GuardianIncome,
                           LocAddress1 = stu.LocalAddress1,
                           LocAddress2 = stu.LocalAddress2,
                           LocCity = stu.LocalCity,
                           LocState = stu.LocalState,
                           LocCountry = stu.LocalCountry,
                           LocPostelcode = stu.LocalPostalCode,
                           StudentPhoto = stu.Photo,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreviousSchoolName,
                           //PreMedium = f.BoardName,
                           Pre_BoardofSchool = stu.BoardOfPreviousSchool,
                           PreMarks = stu.PreviousSchoolMark,
                           PreClass = stu.PreviousSchoolClass,
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreviousSchoolFromDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreviousSchoolFromDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreviousSchoolFromDate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreviousSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreviousSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreviousSchoolToDate),
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPerson,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PermanentAddress1,
                           PerAddress2 = stu.PermanentAddress2,
                           PerCity = stu.PermanentCity,
                           PerState = stu.PermanentState,
                           PerCountry = stu.PermanentCountry,
                           PerPostelcode = stu.PermanentPostalCode,
                           FatherLastName = fa.FatherLastName,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           FatherDOB = fa.FatherDOB,
                           TotalIncome = Priuser.TotalIncome,
                           FatherQualification = fa.FatherQualification,
                           GuardianRequried = stu.HaveGuardian,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           WorkSameSchool = Priuser.WorkSameSchool
                       }).Distinct().ToList();
            return res;
        }

        public List<M_StudentRegisteration> NewStudentMotherGuardianDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in db.PrimaryUserRegisters
                       from trans in db.AdmissionTransactions
                       from stu in db.Students
                       from mo in db.MotherRegisters
                       from gu in db.GuardianRegisters
                       from f in db.BoardofSchools
                       where trans.PrimaryUserRegisterId == PrimaryuserId && trans.StudentRegisterId == StudentId && stu.StudentRegisterId == trans.StudentRegisterId
                       && mo.MotherRegisterId == trans.MotherRegisterId && gu.GuardianRegisterId == trans.GuardianRegisterId && Priuser.PrimaryUserRegisterId == trans.PrimaryUserRegisterId                //&& f.BoardId == stu.BoardOfPreviousSchool
                       select new M_StudentRegisteration
                       {
                           StudentId = stu.StudentId,
                           Stuadmissionid = stu.StudentRegisterId,
                           StuFirstName = stu.FirstName,
                           StuMiddlename = stu.MiddleName,
                           StuLastname = stu.LastName,
                           Gender = stu.Gender,
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),
                           txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.StudentCommunity,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.StudentBloodGroup,
                           AdmissionClass = stu.AdmissionClass,
                           MotherFirstname = mo.MotherFirstname,
                           MotherOccupation = mo.MotherOccupation,
                           MotherMobileNo = mo.MotherMobileNo,
                           MotherEmail = mo.MotherEmail,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           GuardianFirstname = gu.GuardianFirstname,
                           GuardianLastName = gu.GuardianLastName,
                           GuRelationshipToChild = gu.GuRelationshipToChild,
                           GuardianDOB = gu.GuardianDOB,
                           GuardianGender = gu.GuardianGender,
                           GuardianQualification = gu.GuardianQualification,
                           GuardianMobileNo = gu.GuardianMobileNo,
                           GuardianEmail = gu.GuardianEmail,
                           GuardianOccupation = gu.GuardianOccupation,
                           GuardianIncome = gu.GuardianIncome,
                           LocAddress1 = stu.LocalAddress1,
                           LocAddress2 = stu.LocalAddress2,
                           LocCity = stu.LocalCity,
                           LocState = stu.LocalState,
                           LocCountry = stu.LocalCountry,
                           LocPostelcode = stu.LocalPostalCode,
                           StudentPhoto = stu.Photo,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreviousSchoolName,
                           //PreMedium = f.BoardName,
                           Pre_BoardofSchool = stu.BoardOfPreviousSchool,
                           PreMarks = stu.PreviousSchoolMark,
                           PreClass = stu.PreviousSchoolClass,
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreviousSchoolFromDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreviousSchoolFromDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreviousSchoolFromDate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreviousSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreviousSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreviousSchoolToDate),
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPerson,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PermanentAddress1,
                           PerAddress2 = stu.PermanentAddress2,
                           PerCity = stu.PermanentCity,
                           PerState = stu.PermanentState,
                           PerCountry = stu.PermanentCountry,
                           PerPostelcode = stu.PermanentPostalCode,
                           MotherLastName = mo.MotherLastName,
                           MotherDOB = mo.MotherDOB,
                           TotalIncome = Priuser.TotalIncome,
                           GuardianRequried = stu.HaveGuardian,
                           MotherQualification = mo.MotherQualification,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           WorkSameSchool = Priuser.WorkSameSchool
                       }).Distinct().ToList();
            return res;
        }

        public List<M_StudentRegisteration> NewStudentFatherDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in db.PrimaryUserRegisters
                       from trans in db.AdmissionTransactions
                       from stu in db.Students
                       from fa in db.FatherRegisters
                       from f in db.BoardofSchools
                       where trans.PrimaryUserRegisterId == PrimaryuserId && trans.StudentRegisterId == StudentId && stu.StudentRegisterId == trans.StudentRegisterId
                        && fa.FatherRegisterId == trans.FatherRegisterId && Priuser.PrimaryUserRegisterId == trans.PrimaryUserRegisterId                     //&& f.BoardId==stu.BoardOfPreviousSchool
                       select new M_StudentRegisteration
                       {
                           StudentId = stu.StudentId,
                           Stuadmissionid = stu.StudentRegisterId,
                           StuFirstName = stu.FirstName,
                           StuMiddlename = stu.MiddleName,
                           StuLastname = stu.LastName,
                           Gender = stu.Gender,
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.StudentCommunity,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.StudentBloodGroup,
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),
                           AdmissionClass = stu.AdmissionClass,
                           FatherFirstName = fa.FatherFirstName,
                           FatherOccupation = fa.FatherOccupation,
                           FatherMobileNo = fa.FatherMobileNo,
                           FatherEmail = fa.FatherEmail,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           LocAddress1 = stu.LocalAddress1,
                           LocAddress2 = stu.LocalAddress2,
                           LocCity = stu.LocalCity,
                           LocState = stu.LocalState,
                           LocCountry = stu.LocalCountry,
                           LocPostelcode = stu.LocalPostalCode,
                           StudentPhoto = stu.Photo,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreviousSchoolName,
                           //PreMedium = f.BoardName,
                           Pre_BoardofSchool = stu.BoardOfPreviousSchool,
                           PreMarks = stu.PreviousSchoolMark,
                           PreClass = stu.PreviousSchoolClass,
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreviousSchoolFromDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreviousSchoolFromDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreviousSchoolFromDate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreviousSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreviousSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreviousSchoolToDate),
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPerson,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PermanentAddress1,
                           PerAddress2 = stu.PermanentAddress2,
                           PerCity = stu.PermanentCity,
                           PerState = stu.PermanentState,
                           PerCountry = stu.PermanentCountry,
                           PerPostelcode = stu.PermanentPostalCode,
                           FatherLastName = fa.FatherLastName,
                           FatherDOB = fa.FatherDOB,
                           GuardianRequried = stu.HaveGuardian,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           TotalIncome = Priuser.TotalIncome,
                           FatherQualification = fa.FatherQualification,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           WorkSameSchool = Priuser.WorkSameSchool
                       }).Distinct().ToList();
            return res;
        }
        public List<M_StudentRegisteration> NewStudentMotherDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in db.PrimaryUserRegisters
                       from trans in db.AdmissionTransactions
                       from stu in db.Students
                       from mo in db.MotherRegisters
                       from f in db.BoardofSchools
                       where trans.PrimaryUserRegisterId == PrimaryuserId && trans.StudentRegisterId == StudentId && stu.StudentRegisterId == trans.StudentRegisterId
                        && mo.MotherRegisterId == trans.MotherRegisterId && Priuser.PrimaryUserRegisterId == trans.PrimaryUserRegisterId                          //&& f.BoardId==stu.BoardOfPreviousSchool
                       select new M_StudentRegisteration
                       {
                           StudentId = stu.StudentId,
                           Stuadmissionid = stu.StudentRegisterId,
                           StuFirstName = stu.FirstName,
                           StuMiddlename = stu.MiddleName,
                           StuLastname = stu.LastName,
                           Gender = stu.Gender,
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.StudentCommunity,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.StudentBloodGroup,
                           AdmissionClass = stu.AdmissionClass,
                           MotherFirstname = mo.MotherFirstname,
                           MotherOccupation = mo.MotherOccupation,
                           MotherMobileNo = mo.MotherMobileNo,
                           MotherEmail = mo.MotherEmail,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           LocAddress1 = stu.LocalAddress1,
                           LocAddress2 = stu.LocalAddress2,
                           LocCity = stu.LocalCity,
                           LocState = stu.LocalState,
                           LocCountry = stu.LocalCountry,
                           LocPostelcode = stu.LocalPostalCode,
                           StudentPhoto = stu.Photo,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreviousSchoolName,
                           //PreMedium = f.BoardName,
                           Pre_BoardofSchool = stu.BoardOfPreviousSchool,
                           PreMarks = stu.PreviousSchoolMark,
                           PreClass = stu.PreviousSchoolClass,
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreviousSchoolFromDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreviousSchoolFromDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreviousSchoolFromDate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreviousSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreviousSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreviousSchoolToDate),
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPerson,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PermanentAddress1,
                           PerAddress2 = stu.PermanentAddress2,
                           PerCity = stu.PermanentCity,
                           PerState = stu.PermanentState,
                           PerCountry = stu.PermanentCountry,
                           PerPostelcode = stu.PermanentPostalCode,
                           MotherLastName = mo.MotherLastName,
                           MotherDOB = mo.MotherDOB,
                           TotalIncome = Priuser.TotalIncome,
                           MotherQualification = mo.MotherQualification,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           GuardianRequried = stu.HaveGuardian,
                           WorkSameSchool = Priuser.WorkSameSchool
                       }).Distinct().ToList();
            return res;
        }
        public List<M_StudentRegisteration> NewStudentGuardianDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in db.PrimaryUserRegisters
                       from trans in db.AdmissionTransactions
                       from stu in db.Students
                       from gu in db.GuardianRegisters
                       from f in db.BoardofSchools
                       where trans.PrimaryUserRegisterId == PrimaryuserId && trans.StudentRegisterId == StudentId && stu.StudentRegisterId == trans.StudentRegisterId
                        && gu.GuardianRegisterId == trans.GuardianRegisterId && Priuser.PrimaryUserRegisterId == trans.PrimaryUserRegisterId                            //&& f.BoardId==stu.BoardOfPreviousSchool
                       select new M_StudentRegisteration
                       {
                           StudentId = stu.StudentId,
                           Stuadmissionid = stu.StudentRegisterId,
                           StuFirstName = stu.FirstName,
                           StuMiddlename = stu.MiddleName,
                           StuLastname = stu.LastName,
                           Gender = stu.Gender,
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.StudentCommunity,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.StudentBloodGroup,
                           AdmissionClass = stu.AdmissionClass,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           GuardianFirstname = gu.GuardianFirstname,
                           GuardianLastName = gu.GuardianLastName,
                           GuRelationshipToChild = gu.GuRelationshipToChild,
                           GuardianDOB = gu.GuardianDOB,
                           GuardianGender = gu.GuardianGender,
                           GuardianQualification = gu.GuardianQualification,
                           GuardianMobileNo = gu.GuardianMobileNo,
                           GuardianEmail = gu.GuardianEmail,
                           GuardianOccupation = gu.GuardianOccupation,
                           GuardianIncome = gu.GuardianIncome,
                           LocAddress1 = stu.LocalAddress1,
                           LocAddress2 = stu.LocalAddress2,
                           LocCity = stu.LocalCity,
                           LocState = stu.LocalState,
                           LocCountry = stu.LocalCountry,
                           LocPostelcode = stu.LocalPostalCode,
                           StudentPhoto = stu.Photo,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreviousSchoolName,
                           //PreMedium =f.BoardName,
                           Pre_BoardofSchool = stu.BoardOfPreviousSchool,
                           PreMarks = stu.PreviousSchoolMark,
                           PreClass = stu.PreviousSchoolClass,
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreviousSchoolFromDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreviousSchoolFromDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreviousSchoolFromDate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreviousSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreviousSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreviousSchoolToDate),
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPerson,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PermanentAddress1,
                           PerAddress2 = stu.PermanentAddress2,
                           PerCity = stu.PermanentCity,
                           PerState = stu.PermanentState,
                           PerCountry = stu.PermanentCountry,
                           PerPostelcode = stu.PermanentPostalCode,
                           TotalIncome = Priuser.TotalIncome,
                           GuardianRequried = stu.HaveGuardian,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           WorkSameSchool = Priuser.WorkSameSchool
                       }).Distinct().ToList();
            return res;
        }

        //update Student Details
        public void UpdateStudentinformation(int sid, string sfname, string smname, string slname, string sgender, DateTime? dob, string pob, int? community, string religion, string nationality, int? blood,
     string addr1, string addr2, string city, string state, string country, long? postel, int? distance, string peraddr1, string peraddr2, string percity, string perstate, string percountry, long? perpostel,
            string gurdianreg, string height, string weight, string identificationmark, string email, long? contact, string emergencycontactperson, long? emergencycontactnumber, string emergencypersonRelationship, bool? hostelrequired, bool? transportrequired)
        {
            var ans = db.Students.Where(q => q.StudentRegisterId == sid).FirstOrDefault();
            if (ans != null)
            {
                ans.FirstName = sfname;
                ans.MiddleName = smname;
                ans.LastName = slname;
                ans.Gender = sgender;
                ans.DOB = dob;
                ans.PlaceOfBirth = pob;
                ans.StudentCommunity = community;
                ans.Religion = religion;
                ans.Nationality = nationality;
                ans.StudentBloodGroup = blood;
                ans.LocalAddress1 = addr1;
                ans.LocalAddress2 = addr2;
                ans.LocalCity = city;
                ans.LocalCountry = country;
                ans.LocalState = state;
                ans.LocalPostalCode = postel;
                ans.Distance = distance;
                ans.PermanentAddress1 = peraddr1;
                ans.PermanentAddress2 = peraddr2;
                ans.PermanentCity = percity;
                ans.PermanentState = perstate;
                ans.PermanentCountry = percountry;
                ans.PermanentPostalCode = perpostel;
                ans.HaveGuardian = gurdianreg;
                ans.Height = height;
                ans.Weights = weight;
                ans.IdentificationMark = identificationmark;
                ans.Email = email;
                ans.Contact = contact;
                ans.EmergencyContactPerson = emergencycontactperson;
                ans.EmergencyContactNumber = emergencycontactnumber;
                ans.ContactPersonRelationship = emergencypersonRelationship;
                ans.HostelRequired = hostelrequired;
                ans.TransportRequired = transportrequired;
                db.SaveChanges();
            }
        }

        //file upload when edit student file details

        //uploadTransfercertificate
        public void Edit_updateTransfercertificate(string stuid, string transfer)
        {
            var getrow = (from a in db.Students where a.StudentId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.TransferCertificate = transfer;

                db.SaveChanges();
            }
        }

        //uploadCommunitycertificate
        public void Edit_updateCommunitycertificate(string stuid, string community)
        {
            var getrow = (from a in db.Students where a.StudentId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.CommunityCertificate = community;

                db.SaveChanges();
            }
        }

        //uploadBirthcertificate
        public void Edit_updateBirthcertificate(string stuid, string birth)
        {
            var getrow = (from a in db.Students where a.StudentId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.BirthCertificate = birth;

                db.SaveChanges();
            }
        }

        //uploadStudentPhoto
        public void Edit_updateStudentPhoto(string stuid, string photo)
        {
            var getrow = (from a in db.Students where a.StudentId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.Photo = photo;

                db.SaveChanges();
            }
        }
        //upload IncomeCertificate
        public void Edit_updateIncomecertificate(string stuid, string Income)
        {
            var getrow = (from a in db.Students where a.StudentId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.IncomeCertificate = Income;

                db.SaveChanges();
            }
        }

        public void Edit_UpdatePreviousSchoolInformation(int studentregid, string preschoolname, int? preschoolmedium, string preschoolclass, string preschoolmark, DateTime? fdate, DateTime? tdate)
        {
            var get = db.Students.Where(q => q.StudentRegisterId == studentregid).FirstOrDefault();
            if (get != null)
            {
                get.PreviousSchoolName = preschoolname;
                get.BoardOfPreviousSchool = preschoolmedium;
                get.PreviousSchoolClass = preschoolclass;
                get.PreviousSchoolMark = preschoolmark;
                get.PreviousSchoolFromDate = fdate;
                get.PreviousSchoolToDate = tdate;
                db.SaveChanges();
            }
        }
        public void updatestudentstatusforRefundAdmissionfees(int collectid)
        {
            var ans = (from a in db.FeeCollectionCategories
                       from b in db.Students
                       where a.CollectionId == collectid && b.AdmissionId == a.ApplicationId
                       select b).FirstOrDefault();
            if (ans != null)
            {
                ans.StudentStatus = false;
                ans.Statusflag = "FeesRefund";
                db.SaveChanges();

            }
            var ans1 = (from a in db.FeeCollectionCategories
                        from b in db.PreAdmissionStudentRegisters
                        where a.CollectionId == collectid && b.StudentAdmissionId == a.ApplicationId
                        select b).FirstOrDefault();
            if (ans1 != null)
            {

                ans1.Statusflag = "FeesRefund";
                db.SaveChanges();

            }
        }


        public M_StudentRegisteration GetAdmissionStudentDetails(int admissionid)
        {
            var ans = (from a in db.Students
                       from b in db.AcademicYears
                       from c in db.Classes
                       where a.AdmissionId == admissionid && b.AcademicYearId == a.AcademicyearId && c.ClassId == a.AdmissionClass
                       select new M_StudentRegisteration
                       {
                           txt_academicyear = b.AcademicYear1,
                           txt_ApplyClass = c.ClassType,
                           StuFirstName = a.FirstName + " " + a.LastName
                       }).FirstOrDefault();
            return ans;
        }


        public M_StudentRegisteration GetPreAdmissionfeeStudentDetails(int admissionid)
        {
            var ans = (from a in db.PreAdmissionStudentRegisters
                       from b in db.AcademicYears
                       from c in db.Classes
                       where a.StudentAdmissionId == admissionid && b.AcademicYearId == a.AcademicyearId && c.ClassId == a.AdmissionClass
                       select new M_StudentRegisteration
                       {
                           txt_academicyear = b.AcademicYear1,
                           txt_ApplyClass = c.ClassType,
                           StuFirstName = a.StuFirstName + " " + a.StuLastname,
                           Stuadmissionid = a.StudentAdmissionId
                       }).FirstOrDefault();
            return ans;
        }

        public M_StudentRegisteration GetLibrayStudent(string Rollnumber)
        {
            var ans = (from a in db.Students
                       from b in db.AssignClassToStudents
                       from c in db.StudentRollNumbers
                       from d in db.AcademicYears
                       from e in db.Classes
                       from f in db.Sections
                       where c.RollNumber == Rollnumber && a.StudentRegisterId == c.StudentRegisterId && b.StudentRegisterId == c.StudentRegisterId && b.Status == true && b.HaveRollNumber == true && c.Status == true && d.AcademicYearId == c.AcademicYearId
                        && e.ClassId == c.ClassId && f.SectionId == c.SectionId
                       select new M_StudentRegisteration
                       {
                           StuFirstName = a.FirstName + " " + a.LastName,
                           StudentRollNumber = c.RollNumber,
                           txt_academicyear = d.AcademicYear1,
                           txt_Class = e.ClassType,
                           txt_Section = f.SectionName,
                           StudentId = a.StudentId,
                           StudentRegisterId = a.StudentRegisterId,
                           StudentPhoto = a.Photo
                       }).FirstOrDefault();
            return ans;
        }
        public M_StudentRegisteration GetHostelStudent(string StudentId)
        {
            var ans = (from a in db.Students
                       from b in db.AssignClassToStudents
                       from c in db.StudentRollNumbers
                       from d in db.AcademicYears
                       from e in db.Classes
                       from f in db.Sections
                       from g in db.HostelFeesCollections
                       where a.StudentId == StudentId && a.StudentRegisterId == c.StudentRegisterId && b.StudentRegisterId == c.StudentRegisterId && b.Status == true && b.HaveRollNumber == true && c.Status == true && d.AcademicYearId == c.AcademicYearId
                        && e.ClassId == c.ClassId && f.SectionId == c.SectionId && g.StudentRegisterId == a.StudentRegisterId && g.Status == "Active"
                       select new M_StudentRegisteration
                       {
                           StuFirstName = a.FirstName + " " + a.LastName,
                           StudentRollNumber = c.RollNumber,
                           txt_academicyear = d.AcademicYear1,
                           txt_Class = e.ClassType,
                           txt_Section = f.SectionName,
                           StudentId = a.StudentId,
                           StudentRegisterId = a.StudentRegisterId,
                           StudentPhoto = a.Photo,
                           Gender = a.Gender,
                       }).FirstOrDefault();
            return ans;
        }
        public M_StudentRegisteration GetHostelStudentDetails(int StudentRegId)
        {
            var ans = (from a in db.Students
                       from b in db.AssignClassToStudents
                       from c in db.StudentRollNumbers
                       from d in db.AcademicYears
                       from e in db.Classes
                       from f in db.Sections

                       where a.StudentRegisterId == StudentRegId && a.StudentRegisterId == c.StudentRegisterId && b.StudentRegisterId == c.StudentRegisterId && b.Status == true && b.HaveRollNumber == true && c.Status == true && d.AcademicYearId == c.AcademicYearId
                        && e.ClassId == c.ClassId && f.SectionId == c.SectionId
                       select new M_StudentRegisteration
                       {
                           StuFirstName = a.FirstName + " " + a.LastName,
                           StudentRollNumber = c.RollNumber,
                           txt_academicyear = d.AcademicYear1,
                           txt_Class = e.ClassType,
                           txt_Section = f.SectionName,
                           StudentId = a.StudentId,
                           StudentRegisterId = a.StudentRegisterId,
                           StudentPhoto = a.Photo,
                           Gender = a.Gender,
                       }).FirstOrDefault();
            return ans;
        }

        public Student get_stdid(string stu_id)
        {
            var stid = (from a in db.Students where a.StudentId == stu_id select a).FirstOrDefault();
            return stid;
        }
        public M_StudentRegisteration GetStudent1(int StudentRegisterId)
        {
            var list = (from a in db.Students
                        from b in db.AssignClassToStudents
                        from c in db.Classes
                        where a.StudentRegisterId == StudentRegisterId && b.StudentRegisterId == a.StudentRegisterId && b.Status == true && c.ClassId == b.ClassId
                        select new M_StudentRegisteration
                        {
                            StuFirstName = a.FirstName + " " + a.LastName,
                            txt_Class = c.ClassType
                        }).FirstOrDefault();
            return list;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}