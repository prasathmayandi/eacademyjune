﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class ELearningReplyFromStudentServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public List<ELearnReplyStudentList> getStudentList(int ElearnId, int yearId, int classId, int secId)
        {
            var List = (from a in db.ELearningReplyFromStudents
                        from b in db.StudentRollNumbers
                        from c in db.Students
                        where a.StudentRegisterId == b.StudentRegisterId &&
                        a.StudentRegisterId == c.StudentRegisterId &&
                        a.Id == ElearnId
                        orderby b.RollNumber
                        select new
                        {
                            StudentRegId = a.StudentRegisterId,
                            StudentRollNumber = b.RollNumber,
                            FName = c.FirstName,
                            LName = c.LastName,
                            Answer = a.Description,
                            HomeworkAnswerId = a.Id,
                            Remark = a.Remark
                        }).ToList().Select(a => new ELearnReplyStudentList
                            {
                                StudentReg_Id = QSCrypt.Encrypt(a.StudentRegId.ToString()),
                                StudentRollNumber = a.StudentRollNumber,
                                Name = a.FName + " " + a.LName,
                                Answer = a.Answer,
                                HomeworkAnswer_Id = QSCrypt.Encrypt(a.HomeworkAnswerId.ToString()),
                                Remark = a.Remark
                            }).ToList();
            return List;
        }

        public ELearningReplyFromStudent getELearnAnswerFile(int id)
        {
            var row = (from a in db.ELearningReplyFromStudents where a.Id == id select a).FirstOrDefault();
            return row;
        }
        DateTime date = DateTimeByZone.getCurrentDate();
        public void UpdateRemark(int ELearnAnswerId, string comment, int FacultyId)
        {
            var row = (from a in db.ELearningReplyFromStudents where a.Id == ELearnAnswerId select a).FirstOrDefault();
            row.Remark = comment;
            row.RemarkGivenBy = FacultyId;
            row.RemarkDate = date;
            db.SaveChanges();
        }

        public List<ELearnReplyList> getELearnReplyCount(int ELearnId, int StudentRegId)
        {
            var list = (from a in db.ELearningReplyFromStudents
                        where a.ELearningId == ELearnId &&
                        a.StudentRegisterId == StudentRegId
                        select new ELearnReplyList
                        { Description = a.Description
                        }).ToList();
            return list;
        }

        public void addAnswerForELearn(int EId, string reply, string ReplyFileName, int StudentRegId)
        {
            ELearningReplyFromStudent add = new ELearningReplyFromStudent()
            {
                ELearningId = EId,
                AnswerDate = date,
                Description = reply,
                AnswerFileName = ReplyFileName,
                StudentRegisterId = StudentRegId,
            };
            db.ELearningReplyFromStudents.Add(add);
            db.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}