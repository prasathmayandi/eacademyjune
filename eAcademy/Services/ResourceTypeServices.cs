﻿using eAcademy.DataModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class ResourceTypeServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public IEnumerable GetResourceType()
        {
            var list=(from a in db.ResourceTypes where a.Deleted == false select a).ToList();
            return list;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}