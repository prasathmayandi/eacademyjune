﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class tblTransferStatusServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public IEnumerable<Object> getList()
        {
            var list = (from a in db.tblTransferStatus
                        where a.Status == true
                        select new
                        {
                            StatusName = a.TransferReason,
                            StatusValue = a.TransferReason
                        }).ToList();
            return list;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}