﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class tblCountryServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public List<tblCountry> CountryList()
        {
            var ans = dc.tblCountries.Select(q => q).ToList();
            return ans;
        }

        public IEnumerable<Object> Currency()
        {
            var ans = dc.currencies.Select(q => new { id = q.id, symbol =q.country+" ("+q.symbol+")" }).ToList();
            return ans;
        }

        public List<DayOrder> dayOrderList()
        {
            var ans = dc.DayOrders.Select(q => q).ToList();
            return ans;
        }

        public tblCountry getCountry(string id)
        {
            int cid = Convert.ToInt32(id);
            var row = (from a in dc.tblCountries where a.CountryId == cid select a).FirstOrDefault();
            return row;
        }

        public int getCountryId(string country)
        {
            var record = dc.tblCountries.Where(q => q.CountryName == country).FirstOrDefault();
            if(record!=null)
            {
                return record.CountryId;
            }
            else
            {
                return 0;
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}