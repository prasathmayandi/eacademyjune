﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Collections;

namespace eAcademy.Services
{
    public class TermServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
       
        public IEnumerable<Object> selectedYearTerms(int passYearId)
        {
            var List = dc.Terms.Where(d => d.AcademicYearId == passYearId).Select(e => new { selTermId = e.TermId, selTermName = e.TermName }).ToList();
            return List;
        }

        public IEnumerable<Object> ShowTerms(int yearId)
        {
            var TermList = (from a in dc.Terms
                            select new
                            {
                                TermName = a.TermName,
                                TermId = a.TermId
                            }).ToList().AsEnumerable();
            return TermList;
        }

        public Term getTerm(int yearId, int termId)
        {
            var TermList = (from a in dc.Terms where a.AcademicYearId == yearId && a.TermId == termId select a).FirstOrDefault();
            return TermList;
        }

        public List<TermRecord> TermList()
        {
            var ans = (from t1 in dc.Terms
                       select new TermRecord { TermID = t1.TermId, TermName = t1.TermName, StartDate = t1.StartDate, EndDate = t1.EndDate }).ToList();
            return ans;
        }

        public List<TermRecord> TermList(int acid)
        {
            var ans = (from t1 in dc.Terms
                       where t1.AcademicYearId == acid
                       select new TermRecord { TermID = t1.TermId, TermName = t1.TermName, StartDate = t1.StartDate, EndDate = t1.EndDate }).ToList();
            return ans;
        }

        public string TermList(int acid, int tid)
        {
            var term = dc.Terms.Where(q => q.TermId.Equals(tid) && q.AcademicYearId.Value.Equals(acid)).Select(s => new { t = s.TermName }).FirstOrDefault().ToString();
            return term;
        }

        public List<TermRecord> TermOnYear(int acid)
        {
            var ans = (from t1 in dc.Terms
                       where t1.AcademicYearId == acid
                       select new TermRecord { TermID = t1.TermId, TermName = t1.TermName }).ToList();
            return ans;
        }

        public TermRecord Term(int tid)
        {
            var ans =(from t1 in dc.Terms
                      from t2 in dc.AcademicYears
                    where t1.TermId==tid && t1.AcademicYearId==t2.AcademicYearId
                select new TermRecord
                {
                    AcademicYear=t2.AcademicYear1,
                    AcademicYearId = t1.AcademicYearId.Value,
                    TermID = t1.TermId,
                    TermName = t1.TermName,
                    SDate = SqlFunctions.DateName("day", t1.StartDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.StartDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.StartDate),
                    EDate = SqlFunctions.DateName("day", t1.EndDate).Trim() + "/" + SqlFunctions.StringConvert((double)t1.EndDate.Month).TrimStart() + "/" + SqlFunctions.DateName("year", t1.EndDate)                      
                }).SingleOrDefault();
            return ans;
        }

        public bool deleteAcademicYear(int acid)
        {
            var ans = (from c in dc.Terms
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {             
                return true;
            }
            else
            {
                return false;
            }
        }      

        public Term checktermname(int acid, string termname)
        {
            var ans1 = (from t1 in dc.Terms
                       where t1.AcademicYearId == acid && t1.TermName==termname
                       select t1).FirstOrDefault();
            return ans1;
        }



       public bool checkTerm(int acid, DateTime sdate, DateTime edate, string termname)
        {
            var ans = (from t1 in dc.Terms
                       where t1.AcademicYearId == acid && sdate.Year == t1.StartDate.Year && sdate.Month == t1.StartDate.Month && sdate.Day == t1.StartDate.Day
                       &&  edate.Year==t1.EndDate.Year && edate.Month==t1.EndDate.Month && edate.Day==t1.EndDate.Day                            
                       select t1).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

       //public bool checkTermdate(int acid, DateTime sdate, DateTime edate, string termname)
       //{
       //    var ans = (from t1 in dc.Terms
       //               where t1.AcademicYearId == acid && sdate.Year == t1.StartDate.Year && sdate.Month == t1.StartDate.Month && sdate.Day == t1.StartDate.Day
       //               && edate.Year == t1.EndDate.Year && edate.Month == t1.EndDate.Month && edate.Day == t1.EndDate.Day 
       //               select t1).ToList();
       //    int count = ans.Count;
       //    if (count > 0)
       //    {
       //        return true;
       //    }
       //    else
       //    {
       //        return false;
       //    }
       //}

       public bool checkTermExitname(int acid, string termname, int tid)
       {
           var ans = (from t1 in dc.Terms
                      where t1.AcademicYearId == acid && tid!=t1.TermId && t1.TermName==termname
                      select t1).ToList();
           int count = ans.Count;
           if (count > 0)
           {
               return true;
           }
           else
           {
               return false;
           }
       }
        
        public List<Term> getTerm()
        {
            var ans = dc.Terms.Select(query => query).ToList();
            return ans;
        }

        public void addTerm(string termtype, DateTime sdate, DateTime edate, int acid)
        {
            Term term = new Term()
            {
                TermName = termtype,
                StartDate = sdate,
                EndDate = edate,
                AcademicYearId = acid
            };
            dc.Terms.Add(term);
            dc.SaveChanges();
           
        }
        public void updateTerm(int tid, string term, DateTime sdate, DateTime edate)
        {

            var update = dc.Terms.Where(query => query.TermId.Equals(tid)).First();
            update.TermName = term;
            update.StartDate = sdate;
            update.EndDate = edate;
            dc.SaveChanges();
          
        }

        public bool deleteTerm(int tid)
        {
            var ans = (from a in dc.FeeCollectionCategories
                       where a.PaymentTypeId == tid
                       select a).ToList();
            if (ans.Count == 0)
            {
                var delete = dc.Terms.Where(query => query.TermId.Equals(tid)).First();
                dc.Terms.Remove(delete);
                dc.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<TermRecord> getTermList()
        {

            var ans = (from t1 in dc.Terms
                       from t2 in dc.AcademicYears
                       where t1.AcademicYearId == t2.AcademicYearId
                       select new  { AcademicYearId = t2.AcademicYearId, AcademicYear = t2.AcademicYear1, TermID = t1.TermId, TermName = t1.TermName,
                                               SDate =t1.StartDate,
                                               EDate = t1.EndDate,
                                               acYear1 = t1.AcademicYearId
                       }).ToList().Select(q => new TermRecord
                       {
                           AcademicYearId = q.AcademicYearId,
                           AcademicYear = q.AcademicYear,
                           Term_ID = QSCrypt.Encrypt(q.TermID.ToString()),
                           TermName = q.TermName,
                            TermStartDate= q.SDate.ToString("MMM dd, yyyy"),
                           TermEndDate= q.EDate.ToString("MMM dd, yyyy"),
                           acYear1 = q.AcademicYearId,
                            
                       }).ToList();
            return ans;
        }


        internal IEnumerable<Object> getTerm(int acid)
        {
            var ans = (from t1 in dc.Terms
                       where t1.AcademicYearId == acid
                       select new { term = t1.TermName }).ToList();
            return ans;
        }

        //jegadeesh

        public IEnumerable GetTermList(int yearid)
        {
            var ans = (from b in dc.Terms
                       where b.AcademicYearId == yearid
                       select b).ToList();
            return ans;
        }
        public IEnumerable GetParticularTerm(int termid)
        {
            var ans = dc.Terms.Where(q => q.TermId.Equals(termid)).FirstOrDefault().TermName;
            return ans;
        }
        public IEnumerable GetjsonTerm(int year)
        {
            var ans = dc.Terms.Where(q => q.AcademicYearId.Value.Equals(year)).Select(s => new { termid = s.TermId, termname = s.TermName }).ToList();
            return ans;
        }

        public Term GetAcademicterm(int yid)
        {
            var ans = (from a in dc.AcademicYears
                       from b in dc.Terms
                       where a.AcademicYearId == yid && b.AcademicYearId == a.AcademicYearId && a.StartDate <= b.StartDate
                       select b).OrderBy(q => q.StartDate).FirstOrDefault();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}