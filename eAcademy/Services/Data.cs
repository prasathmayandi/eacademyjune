﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.ViewModel;
using eAcademy.HelperClass;


namespace eAcademy.Services
{
    public class Data : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        DateTime date = DateTimeByZone.getCurrentDate();

        public bool checkAcademicYear(string acYear, DateTime sdate, DateTime edate)
        {
            var ans = dc.AcademicYears.Where(q => q.AcademicYear1.Contains(acYear) || ( q.StartDate.Equals(sdate) && q.EndDate.Equals(edate))).ToList();
            int count = ans.Count;
            if(count>0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<AcademicYear> getAcademicYear()
        {
            var ans = dc.AcademicYears.Select(query => query).ToList();
            return ans;
        }

        public void addAcademicYear(string acyear,DateTime sdate, DateTime edate)
        {
            AcademicYear add = new AcademicYear()
            {
                AcademicYear1 = acyear,
                StartDate = sdate,
                EndDate = edate
            };
            dc.AcademicYears.Add(add);
            dc.SaveChanges();
        }

        public void updateAcademicYear(int acid,string acyear, DateTime sdate, DateTime edate)
        {
            var update = dc.AcademicYears.Where(query => query.AcademicYearId.Equals(acid)).First();
            update.AcademicYear1 = acyear;
            update.StartDate = sdate;
            update.EndDate = edate;
            dc.SaveChanges();
        }

        public void deleteAcademicYear(int acid)
        {
            var delete = dc.AcademicYears.Where(query => query.AcademicYearId.Equals(acid)).First();
            dc.AcademicYears.Remove(delete);
            dc.SaveChanges();
        }


        public bool checkTerm(int acid, DateTime sdate, DateTime edate, string termname)
        {
            var ans = (from t1 in dc.Terms
                       where t1.AcademicYearId.Value == acid && (t1.StartDate >= sdate || t1.StartDate < edate) && (t1.EndDate > sdate && t1.EndDate >= edate)
                       select t1).ToList();

            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Term> getTerm()
        {
            var ans = dc.Terms.Select(query => query).ToList();
            return ans;
        }

        public void addTerm(string termtype,DateTime sdate, DateTime edate,int acid)
        {
            Term term = new Term()
            {
                TermName=termtype,
                StartDate=sdate,
                EndDate=edate,
                AcademicYearId=acid
            };
            dc.Terms.Add(term);
            dc.SaveChanges();
        }
        public void updateTerm(int tid, string term, DateTime sdate, DateTime edate)
        {
            var update = dc.Terms.Where(query => query.TermId.Equals(tid)).First();
            update.TermName = term;
            update.StartDate = sdate;
            update.EndDate = edate;
            dc.SaveChanges();
        }

        public void deleteTerm(int tid)
        {
            var delete = dc.Terms.Where(query => query.TermId.Equals(tid)).First();
            dc.Terms.Remove(delete);
            dc.SaveChanges();
        }

        public bool checkClass(string classname)
        {
            var ans = dc.Classes.Where(q => q.ClassType.Equals(classname)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<Class> getClass()
        {
            var ans = dc.Classes.Select(query => query).ToList();
            return ans;
        }


        public void addClass(string className)
        {
            Class cls = new Class()
            {
              ClassType=className
            };

            dc.Classes.Add(cls);
            dc.SaveChanges();
        }

        public void updateClass(int cid, string classType)
        {
            var update = dc.Classes.Where(q => q.ClassId.Equals(cid)).First();
            update.ClassType = classType;
            dc.SaveChanges();
        }
        public void addSection(string sectionName,int cid)
        {
            Section sec = new Section()
            {
                ClassId=cid,
                SectionName=sectionName
            };
            dc.Sections.Add(sec);
            dc.SaveChanges();
        }


        public List<Section> getSection(int cid)
        {
            var ans = dc.Sections.Where(query => query.ClassId.Value.Equals(cid)).ToList();
            return ans;
        }

        public bool checkSubject(string subjectName)
        {
            var ans = dc.Subjects.Where(q => q.SubjectName.Equals(subjectName)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<Subject> getSubject()
        {
            var ans = dc.Subjects.Select(query => query).ToList();
            return ans;
        }

        public void addSubject(string subName)
        {
            Subject sub = new Subject()
            {
                SubjectName=subName
            };
            dc.Subjects.Add(sub);
            dc.SaveChanges();
        }

        public void updateSubject(int sub_id, string subjectName)
        {
            var update = dc.Subjects.Where(q => q.SubjectId.Equals(sub_id)).First();
            update.SubjectName = subjectName;
            dc.SaveChanges();
        }
        public List<Facility> getFacility()
        {
            var ans = dc.Facilities.Select(query => query).ToList();
            return ans;
        }

        public void addFacility(string facility,string fac_desc)
        {
            Facility fac = new Facility()
            {
                FacilityName = facility,
                FacilityDescription=fac_desc
            };

            dc.Facilities.Add(fac);
            dc.SaveChanges();
        }


        public List<ExtraActivity> getActivity()
        {
            var ans = dc.ExtraActivities.Select(query => query).ToList();
            return ans;
        }

        public void addActivity(string activity, string activity_desc)
        {
            ExtraActivity eac = new ExtraActivity()
            {
                Activity = activity,
                ActivityDescription = activity_desc
            };

            dc.ExtraActivities.Add(eac);
            dc.SaveChanges();
        }


        public bool checkCalendar(string eventName, DateTime date, string eventDesc)
        {
            var ans = dc.Events.Where(q => q.EventName.Equals(eventName) && q.EventDate.Equals(date) && q.EventDecribtion.Equals(eventDesc)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<Event> getEvent()
        {
            var ans = dc.Events.Select(query => query).ToList();
            return ans;
        }

        public void addEvent(int acYid,DateTime event_date,string eventtype, string event_desc)
        {
            Event eve = new Event()
            {
               AcademicYearId=acYid,
               EventDate=event_date,
               EventName=eventtype,
               EventDecribtion=event_desc
            };

            dc.Events.Add(eve);
            dc.SaveChanges();
        }

        public void updateEvent(int eventid, string eventName, DateTime date, string eventDesc)
        {
            var update = dc.Events.Where(q => q.EventId.Equals(eventid)).First();
            update.EventName =eventName;
            update.EventDate = date;
            update.EventDecribtion = eventDesc;
            dc.SaveChanges();
        }

        public void deleteEvent(int eventId)
        {
            var delete = dc.Events.Where(q => q.EventId.Equals(eventId)).First();
            dc.Events.Remove(delete);
        }

        public bool checkAnnouncement(string Announcement_type, DateTime Announcement_date, string Announcement_desc)
        {
            var ans = dc.Announcements.Where(q => q.AnnouncementName.Equals(Announcement_type) && q.AnnouncementDate.Equals(Announcement_date) && q.AnnouncementDescription.Equals(Announcement_desc)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Announcement> getAnnouncement()
        {
            var ans = dc.Announcements.Select(query => query).ToList();
            return ans;
        }

        public void addAnnouncement(int acYid, DateTime announcement_date, string announcement_type, string announcement_desc)
        {
            Announcement announcement = new Announcement()
            {
               AcademicYearId=acYid,
               AnnouncementDate=announcement_date,
               AnnouncementName=announcement_type,
               AnnouncementDescription=announcement_desc
            };

            dc.Announcements.Add(announcement);
            dc.SaveChanges();
        }

        public bool checkFacility(string fac, string fac_desc)
        {
            var ans = dc.Facilities.Where(q => q.FacilityName.Equals(fac) && q.FacilityDescription.Equals(fac_desc)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void updateFacility(int fid, string fac, string fac_desc)
        {
            var update = dc.Facilities.Where(q => q.FacilityId.Equals(fid)).First();
            update.FacilityName = fac;
            update.FacilityDescription = fac_desc;
            dc.SaveChanges();
        }

        public void deleteFacility(int FacilityId)
        {
            var delete = dc.Facilities.Where(q => q.FacilityId.Equals(FacilityId)).First();
            dc.Facilities.Remove(delete);
            dc.SaveChanges();
        }

        public bool checkAcitivity(string activity, string activity_desc)
        {
            var ans = dc.ExtraActivities.Where(q => q.Activity.Equals(activity) && q.ActivityDescription.Equals(activity_desc)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void updateAcitivity(int aid, string activity, string activity_desc)
        {
            var update = dc.ExtraActivities.Where(q => q.ActivityId.Equals(aid)).First();
            update.Activity = activity;
            update.ActivityDescription = activity_desc;
            dc.SaveChanges();
        }

        public void deleteAcitivity(int AcitivityId)
        {
            var delete = dc.ExtraActivities.Where(q => q.ActivityId.Equals(AcitivityId)).First();
            dc.ExtraActivities.Remove(delete);
            dc.SaveChanges();
        }

        public bool checkExam(string ename)
        {
            var ans = dc.Exams.Where(q => q.ExamName.Equals(ename) ).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<Exam> getExam()
        {
            var ans = dc.Exams.Select(q => q).ToList();
            return ans;
        }


        public void addExam(string examName)
        {
            Exam ex = new Exam()
            {
              ExamName=examName

            };
            dc.Exams.Add(ex);
            dc.SaveChanges();
        }
      
        public void updateExam(int examid, string ename)
        {
            var update = dc.Exams.Where(q => q.ExamId.Equals(examid)).First();
            update.ExamName = ename;
            dc.SaveChanges();
        }

        public void addSchoolSetting(string schoolName, string schoolLogo,string schoolAddress, string schoolContact, TimeSpan sTime, TimeSpan eTime, int peroid, int dayOrder, string markFormat, string stdIdFormat,string facIdFormat ,string empIdFormat )
        {
            SchoolSetting setting = new SchoolSetting()
            {
                SchoolName=schoolName,
                SchoolLogo=schoolLogo,
            };

            dc.SchoolSettings.Add(setting);
            dc.SaveChanges();
            if (markFormat == "Mark")
            {
                var update = dc.Features.Where(q => q.FeatureId.Equals(4) ).FirstOrDefault();
                update.FeatureStatus = true;
                dc.SaveChanges();
                var update1 = dc.Features.Where(q => q.FeatureId.Equals(5)).FirstOrDefault();
                update1.FeatureStatus = false;
                dc.SaveChanges();
                var update2 = dc.Features.Where(q=> q.FeatureId.Equals(27)).FirstOrDefault();
                update2.FeatureStatus = true;
                dc.SaveChanges();
                var update3 = dc.Features.Where(q => q.FeatureId.Equals(26)).FirstOrDefault();
                update3.FeatureStatus = false;
                dc.SaveChanges();
                var updateMark = dc.Menus.Where(q => q.MenuId.Equals(71)).FirstOrDefault();
                updateMark.MenuStatus = true;
                dc.SaveChanges();
                var updateGrade = dc.Menus.Where(q => q.MenuId.Equals(73)).FirstOrDefault();
                updateGrade.MenuStatus = false;
                dc.SaveChanges();
                var updateMark1 = dc.Menus.Where(q => q.MenuId.Equals(86)).FirstOrDefault();
                updateMark1.MenuStatus = true;
                dc.SaveChanges();
                var updateGrade1 = dc.Menus.Where(q => q.MenuId.Equals(89)).FirstOrDefault();
                updateGrade1.MenuStatus = false;
                dc.SaveChanges();
            }
            else
            {
                var update = dc.Features.Where(q => q.FeatureId.Equals(4)).FirstOrDefault();
                update.FeatureStatus = false;
                dc.SaveChanges();
                var update1 = dc.Features.Where(q => q.FeatureId.Equals(5)).FirstOrDefault();
                update1.FeatureStatus = true;
                dc.SaveChanges();
                var update2 = dc.Features.Where(q=>q.FeatureId.Equals(27)).FirstOrDefault();
                update2.FeatureStatus = false;
                dc.SaveChanges();
                var update3 = dc.Features.Where(q => q.FeatureId.Equals(26)).FirstOrDefault();
                update3.FeatureStatus = true;
                dc.SaveChanges();
                var updateMark = dc.Menus.Where(q => q.MenuId.Equals(71)).FirstOrDefault();
                updateMark.MenuStatus = false;
                dc.SaveChanges();
                var updateGrade = dc.Menus.Where(q => q.MenuId.Equals(73)).FirstOrDefault();
                updateGrade.MenuStatus = true;
                dc.SaveChanges();
                var updateMark1 = dc.Menus.Where(q => q.MenuId.Equals(86)).FirstOrDefault();
                updateMark1.MenuStatus = false;
                dc.SaveChanges();
                var updateGrade1 = dc.Menus.Where(q => q.MenuId.Equals(89)).FirstOrDefault();
                updateGrade1.MenuStatus = true;
                dc.SaveChanges();
            }
        }

        public void UpdateSchoolSetting(int id,string schoolName, string schoolLogo, string schoolAdress, string schollContact, TimeSpan sTime, TimeSpan eTime, int NOPeroid, int dayOrder, string markFormat, string stdIdFormat, string facIdFormat, string empIdFormat)
        {
            var update = dc.SchoolSettings.Where(q=>q.SettingsId.Equals(id)).FirstOrDefault();
            update.SchoolName = schoolName;
            update.SchoolLogo = schoolLogo;
            dc.SaveChanges();
            if (markFormat == "Mark")
            {
                var update3 = dc.Features.Where(q => q.FeatureId.Equals(4)).FirstOrDefault();
                update3.FeatureStatus = true;
                dc.SaveChanges();
                var update1 = dc.Features.Where(q => q.FeatureId.Equals(5)).FirstOrDefault();
                update1.FeatureStatus = false;
                dc.SaveChanges();
                var update4 = dc.Features.Where(q=>q.FeatureId.Equals(27)).FirstOrDefault();
                update4.FeatureStatus = true;
                dc.SaveChanges();
                var update5 = dc.Features.Where(q => q.FeatureId.Equals(26)).FirstOrDefault();
                update5.FeatureStatus = false;
                dc.SaveChanges();
                var updateMark = dc.Menus.Where(q => q.MenuId.Equals(71)).FirstOrDefault();
                updateMark.MenuStatus = true;
                dc.SaveChanges();
                var updateGrade = dc.Menus.Where(q => q.MenuId.Equals(73)).FirstOrDefault();
                updateGrade.MenuStatus = false;
                dc.SaveChanges();
                var updateMark1 = dc.Menus.Where(q => q.MenuId.Equals(86)).FirstOrDefault();
                updateMark1.MenuStatus = true;
                dc.SaveChanges();
                var updateGrade1 = dc.Menus.Where(q => q.MenuId.Equals(89)).FirstOrDefault();
                updateGrade1.MenuStatus = false;
                dc.SaveChanges();
            }
            else
            {
                var update3 = dc.Features.Where(q => q.FeatureId.Equals(4)).FirstOrDefault();
                update3.FeatureStatus = false;
                dc.SaveChanges();
                var update1 = dc.Features.Where(q => q.FeatureId.Equals(5)).FirstOrDefault();
                update1.FeatureStatus = true;
                dc.SaveChanges();
                var update4 = dc.Features.Where(q=>q.FeatureId.Equals(27)).FirstOrDefault();
                update4.FeatureStatus = false;
                dc.SaveChanges();
                var update5 = dc.Features.Where(q => q.FeatureId.Equals(26)).FirstOrDefault();
                update5.FeatureStatus = true;
                dc.SaveChanges();
                var updateMark = dc.Menus.Where(q => q.MenuId.Equals(71)).FirstOrDefault();
                updateMark.MenuStatus = false;
                dc.SaveChanges();
                var updateGrade = dc.Menus.Where(q => q.MenuId.Equals(73)).FirstOrDefault();
                updateGrade.MenuStatus = true;
                dc.SaveChanges();
                var updateMark1 = dc.Menus.Where(q => q.MenuId.Equals(86)).FirstOrDefault();
                updateMark1.MenuStatus = false;
                dc.SaveChanges();
                var updateGrade1 = dc.Menus.Where(q => q.MenuId.Equals(89)).FirstOrDefault();
                updateGrade1.MenuStatus = true;
                dc.SaveChanges();
            }
        }

        public void addTechEmployee(string eName, string gender, DateTime dob, string bgrp, string email, string community, string religion, string nationality, long contact, string ePerson, long eContact, string address, string city, string state, string expertise, string qua, string experience, DateTime dor, DateTime doj, string photo, string Country)
        {
            TechEmployee tech = new TechEmployee()
            {
                EmployeeName = eName,
                Gender = gender,
                DOB = dob,
                BloodGroup = bgrp,
                Community = community,
                Religion = religion,
                Nationality =nationality,
                Mobile = contact,
                Email = email,
                EmergencyContactPerson = ePerson,
                EmergencyContactNumber = eContact,
                AddressLine1=address,
                City = city,
                State = state,
                Experience=experience,
                DOR=dor,
                DOJ=doj,
                ImgFile=photo,
                Country=Country,
                EmployeeStatus=true
            };
            dc.TechEmployees.Add(tech);
            dc.SaveChanges();

            var emp = dc.TechEmployees.Where(q => q.ImgFile.Equals(photo)).Select(s => new { erid = s.EmployeeRegisterId,name=s.EmployeeName }).First();
            var empregid = emp.erid;
            var ename = emp.name;
            var ans = dc.SchoolSettings.Select(q => q).First();
            var updatetechemp = dc.TechEmployees.Where(q => q.ImgFile.Equals(photo)).First();
            updatetechemp.UserName = (ename + (empregid).ToString()).ToString();
            Random r = new Random();
            var psw = r.Next(99999);
            updatetechemp.Password = psw.ToString();
            dc.SaveChanges();
        }

        public void addOtherEmployee(string eName, string gender, DateTime dob, string bgrp, string email, string community, string religion, string nationality, long contact, string ePerson, long eContact, string address, string city, string state, string expertise, string qua, string experience, DateTime dor, DateTime doj, string photo, string Country)
        {
            OtherEmployee other = new OtherEmployee()
            {
                EmployeeName = eName,
                Gender = gender,
                DOB = dob,
                BloodGroup = bgrp,
                Community = community,
                Religion = religion,
                Nationality =nationality,
                Contact = contact,
                Email = email,
                EmergencyContactPerson = ePerson,
                EmergencyContactNumber = eContact,
                Address = address,
                City = city,
                State = state,
                Expertise = expertise,
                Qualification = qua,
                Experience = experience,
                DOR = dor,
                DOJ = doj,
                ImgFile = photo,
                Country=Country,
                EmployeeStatus=true
            };
            dc.OtherEmployees.Add(other);
            dc.SaveChanges();
            var emp = dc.OtherEmployees.Where(q => q.ImgFile.Equals(photo)).Select(s => new { erid = s.EmployeeRegisterId, name = s.EmployeeName }).First();
            var empregid = emp.erid;
            var ename = emp.name;
            var ans = dc.SchoolSettings.Select(q => q).First();
            var updateotheremp = dc.OtherEmployees.Where(q => q.ImgFile.Equals(photo)).First();
            updateotheremp.UserName = (ename + (empregid).ToString()).ToString();
            Random r = new Random();
            var psw = r.Next(99999);
            updateotheremp.Password = psw.ToString();
            dc.SaveChanges();
        }

        public bool checkRole(string rname)
        {
            var ans = dc.Roles.Where(q => q.RoleType.Equals(rname)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Role> getRole()
        {
            var ans = dc.Roles.Select(query => query).ToList();
            return ans;
        }

        public void addRole(string roleName)
        {
            Role add = new Role()
            {
               RoleType=roleName,
               Status=true
            };

            dc.Roles.Add(add);
            dc.SaveChanges();
        }

        internal bool checkRole(string rolename, string status)
        {
            int count;
            if (status == "Active")
            {
                
                var ans = dc.Roles.Where(q => q.RoleType.Equals(rolename) && q.Status.Value.Equals(true)).ToList();
                count = ans.Count;
            }

            else
            {
                var ans = dc.Roles.Where(q => q.RoleType.Equals(rolename) && q.Status.Value.Equals(false)).ToList();
                count = ans.Count;
            }
           
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void updateRole(int rid, string roleName,string status)
        {

            var update = dc.Roles.Where(query => query.RoleId.Equals(rid)).First();
            update.RoleType = roleName;

            if (status == "Active")
            {
                update.Status = true;
            }

            else
            {
                update.Status = false;
            }
            dc.SaveChanges();
        }

        public void deleteRole(int rid)
        {

            var delete = dc.Roles.Where(query => query.RoleId.Equals(rid)).First();
            dc.Roles.Remove(delete);
            dc.SaveChanges();
        }


        public bool checkMapRoleFeatures(int rid, int fid)
        {
            var ans = dc.MapRoleFeatures.Where(q => q.RoleId.Value.Equals(rid) && q.FeatureId.Value.Equals(fid) ).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<MapFeatureRole> getMapRole()
        {
           
            var ans = (from t1 in dc.Features
                       from t2 in dc.MapRoleFeatures
                       from t3 in dc.Roles
                       where t1.FeatureId == t2.FeatureId && t2.RoleId == t3.RoleId
                       select new MapFeatureRole { FeatureId = t1.FeatureId, FeatureType = t1.FaetureType, MapRoleFeatureId = t2.MapId, RoleId = t3.RoleId, RoleType = t3.RoleType, FeatureDetails = t1.FeatureDetails, Status = t2.status.Value }).OrderBy(q => q.RoleType).ToList();
            return ans;
        }

        public List<UserLog> getUserlog()
        {
            var ans = dc.UserLogs.Select(q => q).ToList();
            return ans;
        }

        public bool checkMark(int acid, int cid, int sec_id, int sid)
        {
            var ans = dc.Marks.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) && q.SubjectId.Value.Equals(sid)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<MapUserRole> getMapUser()
        {
            var ans = (from t1 in dc.TechEmployees
                       from t2 in dc.MapRoleUsers
                       from t3 in dc.Roles
                       where t1.EmployeeRegisterId == t2.EmployeeRegisterId && t2.RoleId == t3.RoleId
                       select new MapUserRole { UserId = t1.EmployeeRegisterId, UserName = t1.EmployeeName, MapRoleUserId = t2.MapId, RoleId = t3.RoleId, RoleType = t3.RoleType, Status = t2.status.Value }).OrderBy(q => q.RoleType).ToList();
            return ans;
        }

        public bool checkMark(int acid, int cid,int sec_id, int sid, int min, int max)
        {
            var ans = dc.Marks.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) && q.SubjectId.Value.Equals(sid) && q.MinMark.Value.Equals(min) && q.MaxMark.Value.Equals(max) ).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        public List<MarkDetails> getMark()
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.Subjects
                       from t4 in dc.Marks
                       from t5 in dc.Sections
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.SubjectId == t4.SubjectId && t5.SectionId==t4.SectionId
                       select new MarkDetails { AcYear = t1.AcademicYear1, Class = t2.ClassType, Subject = t3.SubjectName, MarkId = t4.MarkId, MaxMark = t4.MaxMark, MinMark = t4.MinMark,Section=t5.SectionName, SectionId=t5.SectionId }).ToList();
            return ans;
        }

        public void addMark(int acid,int cid, int sec_id, int sid,int min, int max)
        {
            Mark add = new Mark()
            {
                AcademicYearId = acid,
                ClassId=cid,
                SectionId=sec_id,
                SubjectId=sid,
                MinMark=min,
                MaxMark=max
            };

            dc.Marks.Add(add);
            dc.SaveChanges();
        }

        public void updateMark(int markId,int max,int min)
        {
            var update = dc.Marks.Where(q => q.MarkId.Equals(markId)).First();
            update.MaxMark = max;
            update.MinMark = min;
            dc.SaveChanges();
        }
        
        public bool checkFeeCategory(string ftype)
        {
            var ans = dc.FeeCategories.Where(q => q.FeeCategoryName.Equals(ftype)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<FeeCategory> getFeeCategory()
        {
            var ans = dc.FeeCategories.Select(query => query).ToList();
            return ans;
        }

        public void addFeeCategory(string feetype)
        {
            FeeCategory add = new FeeCategory()
            {
               FeeCategoryName=feetype
            };

            dc.FeeCategories.Add(add);
            dc.SaveChanges();
        }

        public void updateFeeCategory(int fid, string feetype)
        {
            var update = dc.FeeCategories.Where(query => query.FeeCategoryId.Equals(fid)).First();
            update.FeeCategoryName = feetype;
            dc.SaveChanges();
        }

        public void deleteFeeCategory(int fid)
        {
            var delete = dc.FeeCategories.Where(query => query.FeeCategoryId.Equals(fid)).First();
            dc.FeeCategories.Remove(delete);
            dc.SaveChanges();
        }

        public bool checkFeeConfig(int acid, int fid, int id)
        {
            var ans = dc.Fees.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(id) && q.FeeCategoryId.Value.Equals(fid) ).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<ConfigFee> getFeeConfig()
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.FeeCategories
                       from t4 in dc.Fees
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.FeeCategoryId == t4.FeeCategoryId
                       select new ConfigFee {AcYearId=t1.AcademicYearId,AcYear=t1.AcademicYear1,ClassId=t2.ClassId,Class=t2.ClassType,FeeCategoryId=t3.FeeCategoryId,FeeCategory=t3.FeeCategoryName,FeeId=t4.FeeId,Amount=t4.Amount,LastDate=t4.LastDate.Value }).ToList();
            return ans;
        }

        public void addFeeConfig(int acid,int fid,int cid,int amount,DateTime ldate)
        {
            Fee add = new Fee()
            {
                AcademicYearId=acid,
                FeeCategoryId = fid,
                ClassId=cid,
                PaymentTypeId=1,
                Amount=amount,
                LastDate=ldate
            };

            dc.Fees.Add(add);
            dc.SaveChanges();
        }

        public void updateFeeConfig(int feeid,int amount)
        {
            var update = dc.Fees.Where(query => query.FeeId.Equals(feeid)).First();
            update.Amount = amount;
            dc.SaveChanges();
        }

        public void deleteFeeConfig(int feeid)
        {

            var delete = dc.Fees.Where(query => query.FeeId.Equals(feeid)).First();
            dc.Fees.Remove(delete);
            dc.SaveChanges();
        }
        public bool checkTermFee(int acid, int cid, int fid,int termid)
        {
            var ans = dc.TermFees.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.FeeCategoryId.Value.Equals(fid) && q.TermId.Value.Equals(termid)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<TermFeeList> getTermFee(int feeid)
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.FeeCategories
                       from t4 in dc.TermFees
                       from t5 in dc.Fees
                       from t6 in dc.Terms
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.FeeCategoryId == t4.FeeCategoryId && t5.FeeId==t4.FeeId && t6.TermId==t4.TermId && t5.FeeCategoryId==t4.FeeCategoryId && t5.AcademicYearId==t4.AcademicYearId && t5.ClassId==t4.ClassId && t5.FeeId==feeid
                       select new TermFeeList { AcYearId = t1.AcademicYearId, AcYear = t1.AcademicYear1, TermId=t6.TermId, Term=t6.TermName, ClassId = t2.ClassId, Class = t2.ClassType, FeeCategoryId = t3.FeeCategoryId, FeeCategory = t3.FeeCategoryName, FeeId=t5.FeeId, TermFeeId = t4.TermFeeID, Amount = t4.Amount, LastDate = t4.LastDate.Value }).ToList();
            return ans;
        }
       
        public void addTermFee(int acid,int cid,int fid, int tid,int amt,DateTime ldate,int feeid)
        {
            TermFee fee = new TermFee()
            {
                AcademicYearId=acid,
                TermId=tid,
                ClassId=cid,
                FeeCategoryId=fid,
                Amount=amt,
                LastDate=ldate,
                PaymentTypeId=2,
                FeeId=feeid
            };
            dc.TermFees.Add(fee);
            dc.SaveChanges();
        }

        public void updateTermFee(int feeid, int amount)
        {

            var update = dc.TermFees.Where(query => query.TermFeeID.Equals(feeid)).First();
            update.Amount = amount;
            dc.SaveChanges();
        }

        public void deleteTermFee(int feeid)
        {
            var delete = dc.TermFees.Where(query => query.TermFeeID.Equals(feeid)).First();
            dc.TermFees.Remove(delete);
            dc.SaveChanges();
        }

        public void collectFeeAmount(DateTime cdate,int acid,int fid,int cid,int sec_id,int stu_id,  int feeAmount, int fine,int amtColl,string payMode,string bankName,string chequeNo, string DDno,string remark )
        {
            FeeCollection collection = new FeeCollection()
            {
                CollectionDate=cdate,
                AcademicYearId=acid,
                ClassId=cid,
                SectionId=sec_id,
                StudentRegisterId=stu_id,
                FeeAmount=feeAmount,
                Fine=fine,
                AmountCollected=amtColl,
                PaymentMode=payMode,
                BankName=bankName,
                ChequeNumber=chequeNo,
                DDNumber=DDno,
                Remark=remark

            };

            dc.FeeCollections.Add(collection);
            dc.SaveChanges();
        }


       public void addTransportDetails(int acid,int cid,int sec_id,int std_id,string routeNo,string busNo,string location ,string driver, long driverContact)
        {
            TransportDetail trans = new TransportDetail
            {
                AcademicYearId=acid,
                ClassId=cid,
                SectionId=sec_id,
                StudentRegisterId=std_id,
                routeNo=routeNo,
                BusNO=busNo,
                Location=location,
                BusDriver=driver,
                driverContact=driverContact
            };
            dc.TransportDetails.Add(trans);
            dc.SaveChanges();
        }

       public void updateTransportDetails(int transid,int acid, int cid, int sec_id, int std_id, string routeNo, string busNo, string location, string driver, long driverContact)
       {
           var update = dc.TransportDetails.Where(q => q.TransportId.Equals(transid)).First();
           update.AcademicYearId = acid;
           update.ClassId = cid;
           update.SectionId = sec_id;
           update.StudentRegisterId = std_id;
           update.routeNo = routeNo;
           update.BusNO = busNo;
           update.BusDriver = driver;
           update.driverContact = driverContact;
           dc.SaveChanges();
          
       }

       public void deleteTransportDetails(int transid)
       {
           var delete = dc.TransportDetails.Where(query => query.TransportId.Equals(transid)).First();
           dc.TransportDetails.Remove(delete);
           dc.SaveChanges();
       }



        public List<WardenList> getWarden()
       {
           var ans = (from t1 in dc.HostelWardens
                      from t2 in dc.TechEmployees
                      from t3 in dc.AcademicYears
                      where t1.EmployeeRegisterId == t2.EmployeeRegisterId && t1.AcademicYearId==t3.AcademicYearId
                      select new WardenList { acid = t1.AcademicYearId, empid = t1.EmployeeRegisterId, ename = t2.EmployeeName, empContact = t2.Mobile, empAddress = t2.AddressLine1, status = t1.Status, acYear=t3.AcademicYear1, wid=t1.WardenId }).ToList();
           return ans;
       }

        public void addWarden(int acid,int empid)
       {
           HostelWarden warden = new HostelWarden()
           {
               AcademicYearId = acid,
               EmployeeRegisterId = empid,
               Status = true
           };
           dc.HostelWardens.Add(warden);
           dc.SaveChanges();
       }

        public void updateWarden(int wid,string status)
        {
            
            var update = dc.HostelWardens.Where(q => q.WardenId.Equals(wid)).First();
            if (status == "Active")
            {
                update.Status = true;
            }
            else
            { update.Status = false; 
            }
            dc.SaveChanges();
        }


        public void deleteWarden(int wid)
        {
            var delete = dc.HostelWardens.Where(q => q.WardenId.Equals(wid)).First();

            dc.HostelWardens.Remove(delete);
            dc.SaveChanges();
        }

        public List<HostelStudentList> getHostelStudent()
        {
            var ans = (from t1 in dc.HostelStudents
                       from t2 in dc.Students
                       from t3 in dc.AcademicYears
                       from t4 in dc.AssignClassToStudents
                       from t5 in dc.Classes
                       from t6 in dc.Sections
                       where t1.StudentRegisterId == t2.StudentRegisterId && t4.HostelPreference == "Yes" && t1.AcademicYearId == t4.AcademicYearId && t5.ClassId == t6.ClassId && t1.StudentRegisterId == t4.StudentRegisterId && t6.SectionId==t4.SectionId && t1.AcademicYearId==t3.AcademicYearId
                       select new HostelStudentList { acid = t1.AcademicYearId, acYear = t3.AcademicYear1, cid = t1.ClassId, classType = t5.ClassType, sec_id = t1.SectionId, section = t6.SectionName, std_id = t1.StudentRegisterId, stdName = t2.FirstName + " " + t2.LastName, stdContact = t2.Contact.Value, stdAddress = t2.LocalAddress1+""+t2.LocalAddress2, emgyPerson = t2.EmergencyContactPerson, emgyContact = t2.EmergencyContactNumber, status = t1.status, hsid = t1.HostelStudentId, roomNumber = t1.RoomNumber }).ToList();
            return ans;
        }

        public void addHostelStudent(int acid, int cid, int sec_id,int sid,string rno)
        {
            HostelStudent std = new HostelStudent()
            {
                AcademicYearId=acid,
                ClassId=cid,
                SectionId=sec_id,
                StudentRegisterId=sid,
                RoomNumber=rno,
                status=true
                
            };

            dc.HostelStudents.Add(std);
            dc.SaveChanges();
        }

        public void updateHostelStudent(int wid, string status)
        {

            var update = dc.HostelWardens.Where(q => q.WardenId.Equals(wid)).First();
            if (status == "Active")
            {
                update.Status = true;
            }
            else
            {
                update.Status = false;
            }
            dc.SaveChanges();
        }


        public void deleteHostelStudent(int wid)
        {
            var delete = dc.HostelWardens.Where(q => q.WardenId.Equals(wid)).First();
            dc.HostelWardens.Remove(delete);
            dc.SaveChanges();
        }


        public void addFacilityIncharge(int acid, int fid, int empRegId, string Note, string empid)
        {
            FacilityIncharge fac = new FacilityIncharge()
            {
                AcademicYearId = acid,
                FacilityId = fid,
                EmployeeRegisterId = empRegId,
                Note = Note,
                EmployeeId=empid
            };
            dc.FacilityIncharges.Add(fac);
            dc.SaveChanges();
        }

       public List<FaciltyIncharge> getFacilityIncharge()
       {
          
           var list = ((from a in dc.FacilityIncharges
                       from b in dc.TechEmployees
                       from c in dc.AcademicYears
                       from d in dc.Facilities
                       where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                             a.AcademicYearId == c.AcademicYearId &&
                             a.FacilityId == d.FacilityId &&
                             a.EmployeeId==b.EmployeeId
                       select new FaciltyIncharge
                       {
                           Fac_Incharge = b.EmployeeName,
                           Fac_AcYear = c.AcademicYear1,
                           Fac = d.FacilityName,
                           EmpId=b.EmployeeId,
                           EmpContact=b.Mobile,
                           Note=a.Note,
                           FacilityInchargeId=a.FacilityInchargeId
                       })
                   .Union(from a in dc.FacilityIncharges
                       from b in dc.OtherEmployees
                       from c in dc.AcademicYears
                       from d in dc.Facilities
                       where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                             a.AcademicYearId == c.AcademicYearId &&
                             a.FacilityId == d.FacilityId &&
                             a.EmployeeId==b.EmployeeId
                         
                       select new FaciltyIncharge
                       {
                           Fac_Incharge = b.EmployeeName,
                           Fac_AcYear = c.AcademicYear1,
                           Fac = d.FacilityName,
                           EmpId = b.EmployeeId,
                           EmpContact = b.Contact,
                           Note = a.Note,
                           FacilityInchargeId = a.FacilityInchargeId
                       })).ToList();
           return list;

       }

       public bool checkMapRoleFeatures(int rid, int fid, string status)
       {
           int count;
           if (status == "Active")
           {
               var ans = dc.MapRoleFeatures.Where(q => q.RoleId.Value.Equals(rid) && q.FeatureId.Value.Equals(fid) && q.status.Value.Equals(true)).ToList();
               count = ans.Count;
           }

           else
           {
               var ans = dc.MapRoleFeatures.Where(q => q.RoleId.Value.Equals(rid) && q.FeatureId.Value.Equals(fid) && q.status.Value.Equals(false)).ToList();
               count = ans.Count;
           }

           if (count > 0)
           {
               return true;
           }
           else
           {
               return false;
           }
       }

        public List<MapRoleFeature> getMapRoleFeatures()
       {
           var ans = dc.MapRoleFeatures.Select(q=>q).OrderBy(d=>d.RoleId).ToList();
           return ans;
       }

        public void addMapRoleFeatures(int rid, int fid)
        {
            MapRoleFeature map = new MapRoleFeature()
            {
                FeatureId = fid,
                RoleId = rid,
                status = true
            };

            dc.MapRoleFeatures.Add(map);
            dc.SaveChanges();
        }
        public void updateMapRole(int mapId, string status)
        {
            var update = dc.MapRoleFeatures.Where(q => q.MapId.Equals(mapId)).First();
            if (status == "Active")
            {
                update.status = true;
            }
            else
            {
                update.status = false;
            }
            dc.SaveChanges();
        }

        public bool checkMapRoleUser(int rid, int uid)
        {
            var ans = dc.MapRoleUsers.Where(q => q.EmployeeRegisterId.Value.Equals(uid) && q.RoleId.Value.Equals(rid)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void addMapRoleUser(int rid, int uid)
        {
            MapRoleUser map = new MapRoleUser()
            {
                EmployeeRegisterId = uid,
                RoleId = rid,
                status = true
            };

            dc.MapRoleUsers.Add(map);
            dc.SaveChanges();
        }


        public bool checkMapRoleUser(int rid, int uid, string status)
        {
            int count;
            if (status == "Active")
            {

                var ans = dc.MapRoleUsers.Where(q => q.RoleId.Value.Equals(rid) && q.EmployeeRegisterId.Value.Equals(uid) && q.status.Value.Equals(true)).ToList();
                count = ans.Count;
            }

            else
            {
                var ans = dc.MapRoleUsers.Where(q => q.RoleId.Value.Equals(rid) && q.EmployeeRegisterId.Value.Equals(uid) && q.status.Value.Equals(false)).ToList();
                count = ans.Count;
            }

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void updateMapRoleUser(int mapId, string status)
        {
            var update = dc.MapRoleUsers.Where(q => q.MapId.Equals(mapId)).First();
            if (status == "Active")
            {
                update.status = true;
            }
            else
            {
                update.status = false;
            }
            dc.SaveChanges();
        }
        public bool checkGradeType(string gradeType)
        {
            var ans = dc.GradeTypes.Where(q => q.GradeTypeName.Equals(gradeType)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void addGradeType(string GradeTypeName)
        {
            GradeType gtype = new GradeType()
            {
                GradeTypeName=GradeTypeName
            };
            dc.GradeTypes.Add(gtype);
            dc.SaveChanges();
        }

        public void addgrade(int gTypeId,string gradeName,int minMark,int maxMark, string gradeDes)
        {
            Grade grade = new Grade()
            {
                GradeTypeId=gTypeId,
                GradeName=gradeName,
                MinRange=minMark,
                MaxRange=maxMark,
                GradeDescribtion=gradeDes
            };
            dc.Grades.Add(grade);
            dc.SaveChanges();
        }

        public void updateGrade(int gId, string gradeName, int minMark, int maxMark, string gradeDes)
        {
            var update = dc.Grades.Where(q => q.GradeId.Equals(gId)).First();
            update.GradeName = gradeName;
            update.MinRange = minMark;
            update.MaxRange = maxMark;
            update.GradeDescribtion = gradeDes;
            dc.SaveChanges();
        }

        public bool checkAssignGradeClass(int acid, int cid, int gid)
        {
            var ans = dc.AssignGradeTypeToClasses.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) ).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void addAssignGradeClass(int acid, int cid, int gtypeid)
        {
            AssignGradeTypeToClass add = new AssignGradeTypeToClass()
            {
                AcademicYearId=acid,
                ClassId=cid,
                GradeTypeId=gtypeid
            };

            dc.AssignGradeTypeToClasses.Add(add);
            dc.SaveChanges();
        }

        public List<GradeClass> getAssignGrade()
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.GradeTypes
                       from t4 in dc.AssignGradeTypeToClasses
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.GradeTypeId == t4.GradeTypeId
                       select new GradeClass { AcYear = t1.AcademicYear1, Class = t2.ClassType, GradeType = t3.GradeTypeName, GradeClassId = t4.GradeClassId }).ToList();
            return ans;
        }

        public void addActivityIncharge(int acid, int aid, int empRegId, string Note,string empid)
        {
            eAcademy.DataModel.ActivityIncharge act = new eAcademy.DataModel.ActivityIncharge()
            {
                AcademicYearId = acid,
                ActivityId = aid,
                EmployeeRegisterId = empRegId,
                Note = Note,
                EmployeeId=empid
            };
            dc.ActivityIncharges.Add(act);
            dc.SaveChanges();

        }

        public List<eAcademy.Models.ActivityIncharge> getAcitityIncharge()
        {
            var list = ((from a in dc.ActivityIncharges
                         from b in dc.TechEmployees
                         from c in dc.AcademicYears
                         from d in dc.ExtraActivities
                         where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                               a.AcademicYearId == c.AcademicYearId &&
                               a.ActivityId == d.ActivityId &&
                               a.EmployeeId==b.EmployeeId
                         select new eAcademy.Models.ActivityIncharge
                         {
                             Fac_Incharge = b.EmployeeName,
                             Fac_AcYear = c.AcademicYear1,
                             Activity=d.Activity,
                             EmpId = b.EmployeeId,
                             EmpContact = b.Mobile,
                             Note = a.Note,
                             ActivityInchargeId=a.ActivityInchargeId
                         })
                    .Union(from a in dc.ActivityIncharges
                           from b in dc.OtherEmployees
                           from c in dc.AcademicYears
                           from d in dc.ExtraActivities
                           where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                                 a.AcademicYearId == c.AcademicYearId &&
                                 a.ActivityId == d.ActivityId &&
                                 a.EmployeeId==b.EmployeeId
                           select new eAcademy.Models.ActivityIncharge
                           {
                               Fac_Incharge = b.EmployeeName,
                               Fac_AcYear = c.AcademicYear1,
                               Activity = d.Activity,
                               EmpId = b.EmployeeId,
                               EmpContact = b.Contact,
                               Note = a.Note,
                               ActivityInchargeId=a.ActivityInchargeId
                           })).ToList();
            return list;
        }

        public void addActivityStudent(int acid,int cid, int sec_id,int std_id, int aid,string note)
        {
            AssignActivityToStudent activity = new AssignActivityToStudent()
            {
                AcademicYearId=acid,
                ActivityId=aid,
                ClassId=cid,
                SectionId=sec_id,
                StudentRegisterId=std_id,
                Note=note
            };

            dc.AssignActivityToStudents.Add(activity);
            dc.SaveChanges();
        }

        public List<eAcademy.Models.ActivityIncharge> getActivityStudent()
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.Sections
                       from t4 in dc.Students
                       from t5 in dc.TechEmployees
                       from t6 in dc.ExtraActivities
                       from t7 in dc.ActivityIncharges
                       from t8 in dc.AssignActivityToStudents
                       where t1.AcademicYearId == t8.AcademicYearId && t2.ClassId == t8.ClassId && t3.SectionId == t8.SectionId && t4.StudentRegisterId == t8.StudentRegisterId && t5.EmployeeRegisterId == t7.EmployeeRegisterId
                       && t6.ActivityId == t8.ActivityId && t6.ActivityId == t7.ActivityId
                       select new eAcademy.Models.ActivityIncharge { Fac_AcYear = t1.AcademicYear1, Class = t2.ClassType, Section = t3.SectionName, Student = t4.FirstName + " " + t4.LastName, Fac_Incharge = t5.EmployeeName, Activity = t6.Activity, StudentActivityId = t8.StudentActivityId, Note = t8.Note }).ToList();

            return ans;
        }

        public List<NewsTrends> getNewsTrends()
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.NewsTrends
                       where t1.AcademicYearId == t2.AcademicYearId
                       select new NewsTrends { AcademicYear = t1.AcademicYear1, NewsTrendId = t2.NewsTrendsId, tacYSD = t2.NewsDate, Heading = t2.Heading, Desc = t2.Description, Status = t2.Status }).ToList();
            return ans;
        }

        public void addNewsTrends(int acid,DateTime? date,string heading,string desc)
        {
            NewsTrend news = new NewsTrend()
            {
                AcademicYearId=acid,
                NewsDate=date,
                Heading=heading,
                Description=desc,
                Status=true
            };
            dc.NewsTrends.Add(news);
            dc.SaveChanges();
        }

        public void updateNewsTrends(int newsId,DateTime date,string heading,string desc)
        {
            var update = dc.NewsTrends.Where(q => q.NewsTrendsId.Equals(newsId)).First();
            update.NewsDate = date;
            update.Heading = heading;
            update.Description = desc;
            dc.SaveChanges();
        }

        public void deleteNewsTrends(int newsId)
        {
            var delete = dc.NewsTrends.Where(q => q.NewsTrendsId.Equals(newsId)).First();
            dc.NewsTrends.Remove(delete);
            dc.SaveChanges();
        }
        
        public List<Announcements> getAnnouncements()
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Announcements
                       where t1.AcademicYearId == t2.AcademicYearId
                       select new Announcements { AcademicYear = t1.AcademicYear1, AnnoumcementId = t2.AnnouncementId, tacYSD = t2.AnnouncementDate, Heading = t2.AnnouncementName, Desc = t2.AnnouncementDescription, Status = t2.Status }).ToList();
            return ans;
        }

        public void addAnnouncements(int acid, DateTime date, string heading, string desc)
        {
            Announcement announ = new Announcement()
            {
                AcademicYearId = acid,
                AnnouncementDate = date,
                AnnouncementName = heading,
                AnnouncementDescription = desc,
                Status = true
            };
            dc.Announcements.Add(announ);
            dc.SaveChanges();

        }
        public void updateAnnouncement(int newsId, DateTime date, string heading, string desc)
        {
            var update = dc.Announcements.Where(q => q.AnnouncementId.Equals(newsId)).First();
            update.AnnouncementDate = date;
            update.AnnouncementName = heading;
            update.AnnouncementDescription = desc;
            dc.SaveChanges();
        }

        public void deleteAnnouncement(int AnnouncementId)
        {
            var delete = dc.Announcements.Where(q => q.AnnouncementId.Equals(AnnouncementId)).First();
            dc.Announcements.Remove(delete);
            dc.SaveChanges();
        }

        public List<Circulars> getCircular()
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Circulars
                       from t3 in dc.TechEmployees
                       where t1.AcademicYearId == t2.AcademicYearId && t2.IssuedBy==t3.EmployeeRegisterId
                       select new Circulars { AcademicYear = t1.AcademicYear1, CircularId = t2.CricularId, tacYSD = t2.IssuedOn, Heading = t2.Heading, Reason = t2.Reason, Status = t2.Status ,EmpRegId=t3.EmployeeRegisterId,Employee=t3.EmployeeName}).ToList();
            return ans;
        }

        public void addCircular(int acid, DateTime date, string heading, string reason,int empRegId)
        {
            Circular c = new Circular()
            {
                AcademicYearId = acid,
                IssuedOn = date,
                Heading = heading,
                Reason = reason,
                IssuedBy=empRegId,
                Status = true
            };
            dc.Circulars.Add(c);
            dc.SaveChanges();

        }
        public void updateCircular(int CircularId, DateTime date, string heading, string reason,int empRegId)
        {
            var update = dc.Circulars.Where(q => q.CricularId.Equals(CircularId)).First();
            update.IssuedOn = date;
            update.Heading = heading;
            update.Reason = reason;
            update.IssuedBy = empRegId;
            dc.SaveChanges();
        }

        public void deleteCircular(int CircularId)
        {
            var delete = dc.Circulars.Where(q => q.CricularId.Equals(CircularId)).First();
            dc.Circulars.Remove(delete);
            dc.SaveChanges();
        }

        // Achievement  -- studentAchievement

        public void addStudentAchievement(int yearId, int classId, int secId, int studentId, DateTime doa, string achievement, string desc)
        {
            StudentAchievement add = new StudentAchievement()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                StudentRegisterId = studentId,
                DateOfAchievement = doa,
                Achievement = achievement,
                Descriptions = desc
            };

            dc.StudentAchievements.Add(add);
            dc.SaveChanges();
        }

        public List<AchievementList> getStudentAchievement()
        {
            var list = (from a in dc.StudentAchievements
                        from b in dc.Students
                        from c in dc.Classes
                        from d in dc.Sections
                        where a.StudentRegisterId == b.StudentRegisterId &&
                              a.ClassId == c.ClassId &&
                              a.SectionId == d.SectionId
                        select new AchievementList
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = b.FirstName + " " + b.LastName,
                            Class = c.ClassType,
                            Section = d.SectionName,
                            Id = b.StudentRegisterId,
                            AchievementId = a.Id
                        }).ToList();
            return list;
        }

        public void updateStudentAchievement(DateTime doa, string achievement, string desc, int AchievementEId)
        {
            var getRow = (from a in dc.StudentAchievements where a.Id == AchievementEId select a).FirstOrDefault();
            getRow.DateOfAchievement = doa;
            getRow.Achievement = achievement;
            getRow.Descriptions = desc;
            dc.SaveChanges();
        }

        public void delStudentAchievement(int id)
        {
            var getRow = (from a in dc.StudentAchievements where a.Id == id select a).FirstOrDefault();
            dc.StudentAchievements.Remove(getRow);
            dc.SaveChanges();
        }

        public List<AchievementList> sortStudentAchievementByYear(int Yearid)
        {
            var list = (from a in dc.StudentAchievements
                        from b in dc.Students
                        from c in dc.Classes
                        from d in dc.Sections
                        where a.StudentRegisterId == b.StudentRegisterId &&
                              a.ClassId == c.ClassId &&
                              a.SectionId == d.SectionId &&
                              a.AcademicYearId == Yearid
                        select new AchievementList
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = b.FirstName + " " + b.LastName,
                            Class = c.ClassType,
                            Section = d.SectionName,
                            Id = b.StudentRegisterId,
                            AchievementId = a.Id
                        }).ToList();
            return list;
        }

        public List<AchievementList> getFacultyAchievement()
        {
            var list = (from a in dc.EmployeeAchievements
                        from b in dc.TechEmployees
                        where a.EmployeeRegisterId == b.EmployeeRegisterId
                        select new AchievementList
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = b.EmployeeName,
                            Id = b.EmployeeRegisterId,
                            AchievementId = a.Id
                        }).ToList();
            return list;
        }

        public void addFacultyAchievement(int yearId, int EmployeeId, DateTime doa, string achievement, string desc)
        {
            EmployeeAchievement add = new EmployeeAchievement()
            {
                AcademicYearId = yearId,
                EmployeeRegisterId = EmployeeId,
                DateOfAchievement = doa,
                Achievement = achievement,
                Descriptions = desc
            };
            dc.EmployeeAchievements.Add(add);
            dc.SaveChanges();
        }

        public void updateFacultyAchievement(DateTime doa, string achievement, string desc, int AchievementEId)
        {
            var getRow = (from a in dc.EmployeeAchievements where a.Id == AchievementEId select a).FirstOrDefault();
            getRow.DateOfAchievement = doa;
            getRow.Achievement = achievement;
            getRow.Descriptions = desc;
            dc.SaveChanges();
        }

        public void delFacultyAchievement(int id)
        {
            var getRow = (from a in dc.EmployeeAchievements where a.Id == id select a).FirstOrDefault();
            dc.EmployeeAchievements.Remove(getRow);
            dc.SaveChanges();
        }

        public List<AchievementList> sortFacultyAchievementByYear(int Yearid)
        {
            var list = (from a in dc.EmployeeAchievements
                        from b in dc.TechEmployees
                        where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                              a.AcademicYearId == Yearid
                        select new AchievementList
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = b.EmployeeName,
                            Id = b.EmployeeRegisterId,
                            AchievementId = a.Id
                        }).ToList();
            return list;
        }

        public List<AchievementList> getSchoolAchievement()
        {
            var list = (from a in dc.SchoolAchievements
                        select new AchievementList
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = a.PeopleInvolved,
                            AchievementId = a.Id
                        }).ToList();
            return list;
        }

        public void addSchoolAchievement(int yearId, DateTime doa, string achievement, string desc, string peopleInvolved)
        {
            SchoolAchievement add = new SchoolAchievement()
            {
                AcademicYearId = yearId,
                DateOfAchievement = doa,
                Achievement = achievement,
                Descriptions = desc,
                PeopleInvolved = peopleInvolved
            };
            dc.SchoolAchievements.Add(add);
            dc.SaveChanges();
        }

        public void updateSchoolAchievement(DateTime doa, string achievement, string desc, int AchievementEId, string people)
        {
            var getRow = (from a in dc.SchoolAchievements where a.Id == AchievementEId select a).FirstOrDefault();
            getRow.DateOfAchievement = doa;
            getRow.Achievement = achievement;
            getRow.Descriptions = desc;
            getRow.PeopleInvolved = people;
            dc.SaveChanges();
        }

        public void delSchoolAchievement(int id)
        {
            var getRow = (from a in dc.SchoolAchievements where a.Id == id select a).FirstOrDefault();
            dc.SchoolAchievements.Remove(getRow);
            dc.SaveChanges();
        }

        public List<AchievementList> sortSchoolAchievementByYear(int Yearid)
        {
            var list = (from a in dc.SchoolAchievements
                        where a.AcademicYearId == Yearid
                        select new AchievementList
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = a.PeopleInvolved,
                            AchievementId = a.Id
                        }).ToList();
            return list;

        }

        // Faculty Info

        public List<AssignClassTeacher> getFacultyClass(int FacultyId, int yearId)
        {
            var list = (from a in dc.AssignClassTeachers where a.EmployeeRegisterId == FacultyId && a.AcademicYearId == yearId select a).ToList();
            return list;
        }

        public List<FacultyClassStudentsList> GetFacultyClassStudents(int yearId, int classId, int secId)
        {
            var StudentsList = (from a in dc.AssignClassToStudents
                                from b in dc.Students
                                where a.StudentRegisterId == b.StudentRegisterId &&
                                b.StudentStatus == true &&
                                a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId
                                select new FacultyClassStudentsList
                                {
                                    StudentId = b.StudentId,
                                    StudentName = b.FirstName + " " + b.LastName
                                }).ToList();
            return StudentsList;
        }

        public List<ModulesAssignedList> getModulesAssigned(int yearId, int FacultyId)
        {
            var moduleList = (from a in dc.AssignFacultyToSubjects
                              from b in dc.Classes
                              from c in dc.Sections
                              from d in dc.Subjects
                              where a.ClassId == b.ClassId &&
                                    a.SectionId == c.SectionId &&
                                    a.SubjectId == d.SubjectId &&
                                    a.AcademicYearId == yearId &&
                                    a.EmployeeRegisterId == FacultyId
                              select new ModulesAssignedList
                              {
                                  ClassName = b.ClassType,
                                  SectionName = c.SectionName,
                                  SubjectName = d.SubjectName
                              }).ToList();
            return moduleList;
        }

        public List<StaffTimetableList> getTimetable(int yearId, int FacultyId, int dayId)
        {
            var list = (from a in dc.AssignFacultyToSubjects
                        from b in dc.AssignSubjectToPeriods
                        from c in dc.Classes
                        from d in dc.Sections
                        from e in dc.Subjects
                        where a.SubjectId == b.SubjectId &&
                              a.EmployeeRegisterId == FacultyId &&
                              b.Day == dayId &&
                              a.AcademicYearId == yearId &&
                              a.ClassId == c.ClassId &&
                              a.SectionId == d.SectionId &&
                              a.SubjectId == e.SubjectId
                        select new StaffTimetableList
                        {
                            Period = b.Period,
                            Class = c.ClassType,
                            Section = d.SectionName,
                            Subject = e.SubjectName
                        }).ToList();

            return list;
        }

        // Class Incharge

        public List<ClassInchargeStudentsList> getClassInchargeStudents(int yearId, int classId, int secId)
        {
            var list = (from a in dc.AssignClassToStudents
                        from b in dc.Students
                        from c in dc.StudentRollNumbers
                        from d in dc.tblCountries
                        where a.AcademicYearId == yearId &&
                                 a.ClassId == classId &&
                                 a.SectionId == secId &&
                                 a.StudentRegisterId == b.StudentRegisterId &&
                                 a.StudentRegisterId == c.StudentRegisterId &&
                                 b.StudentStatus == true &&
                                 c.AcademicYearId == yearId &&
                                 c.ClassId == classId &&
                                 c.SectionId == secId
                        select new ClassInchargeStudentsList
                        {
                            RollNumber = c.RollNumber,
                            StudentName = b.FirstName + " " + b.LastName,
                            Gender = b.Gender,
                            EmergencyContactPerson = b.EmergencyContactPerson,
                            EmergencyContactNumber = b.EmergencyContactNumber,
                            Hostel = a.HostelPreference,
                            Address = b.LocalAddress1+" "+b.LocalAddress2,
                            City = b.LocalCity,
                            State = b.LocalState,
                            Country = d.CountryName,
                            StudentRegId = b.StudentRegisterId
                        }).ToList();

            return list;
        }

        // Module Info

        public void addSubjectNotes(int yearId, int classId, int secId, int subId, DateTime notesDate, string topic, string desc, string fileName, int FacultyId)
        {
            SubjectNote add = new SubjectNote()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                SubjectId = subId,
                DateOfNotes = notesDate,
                Topic = topic,
                Descriptions = desc,
                NotesFileName = fileName,
                NotesStatus = true,
                EmployeeRegisterId = FacultyId
            };
            dc.SubjectNotes.Add(add);
            dc.SaveChanges();
        }

        public List<SubjectNotesList> getSubjectNotesList(int yearId, int classId, int secId, int subId, int FacultyId)
        {
            var list = (from a in dc.SubjectNotes
                        where a.AcademicYearId == yearId &&
                                 a.ClassId == classId &&
                                 a.SectionId == secId &&
                                 a.SubjectId == subId &&
                                 a.EmployeeRegisterId == FacultyId
                        select new SubjectNotesList
                        {
                            NotesId = a.NotesId,
                            DateOfNotes = a.DateOfNotes,
                            Topic = a.Topic,
                            Descriptions = a.Descriptions,
                            NotesStatus = a.NotesStatus,
                            NotesFileName = a.NotesFileName
                        }).ToList();

            return list;
        }

        public void updateSubjectNotes(DateTime notesDate, string topic, string desc, Boolean status, int eid)
        {
            var getRow = (from a in dc.SubjectNotes where a.NotesId == eid select a).FirstOrDefault();
            getRow.Topic = topic;
            getRow.DateOfNotes = notesDate;
            getRow.Descriptions = desc;
            getRow.NotesStatus = status;
            dc.SaveChanges();
        }

        public void updateSubjectNotesFile(int eid, string fileName1)
        {
            var getRow = (from a in dc.SubjectNotes where a.NotesId == eid select a).FirstOrDefault();
            getRow.NotesFileName = fileName1;
            dc.SaveChanges();
        }


        public void delSubjectNotes(int id)
        {
            var getRow = (from a in dc.SubjectNotes where a.NotesId == id select a).FirstOrDefault();
            dc.SubjectNotes.Remove(getRow);
            dc.SaveChanges();
        }

        public void addHomework(int yearId, int classId, int secId, int subId, DateTime submissionDate, string Homework, string desc, string fileName, int FacultyId)
        {
            HomeWork add = new HomeWork()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                SubjectId = subId,
                DateOfWorkPosted = date,
                DateToCompleteWork = submissionDate,
                HomeWork1 = Homework,
                Descriptions = desc,
                HomeWorkFileName = fileName,
                HomeWorkStatus = true,
                EmployeeRegisterId = FacultyId
            };
            dc.HomeWork.Add(add);
            dc.SaveChanges();
        }

        public List<HomeworkList> getHomeworkList(int yearId, int classId, int secId, int subId, DateTime fromDate, DateTime toDate, int FacultyId)
        {
            var list = (from a in dc.HomeWork
                        where a.AcademicYearId == yearId &&
                                 a.ClassId == classId &&
                                 a.SectionId == secId &&
                                 a.SubjectId == subId &&
                                 a.EmployeeRegisterId == FacultyId &&
                                 a.DateToCompleteWork >= fromDate &&
                                 a.DateToCompleteWork <= toDate
                        select new HomeworkList
                        {
                            HomeWorkId = a.HomeWorkId,
                            DateOfWorkPosted = a.DateOfWorkPosted,
                            DateToCompleteWork = a.DateToCompleteWork,
                            HomeWork1 = a.HomeWork1,
                            Descriptions = a.Descriptions,
                            HomeWorkStatus = a.HomeWorkStatus,
                            HomeWorkFileName = a.HomeWorkFileName
                        }).ToList();

            return list;
        }

        public void updateHomeworkFile(int eid, string fileName1)
        {
            var getRow = (from a in dc.HomeWork where a.HomeWorkId == eid select a).FirstOrDefault();
            getRow.HomeWorkFileName = fileName1;
            dc.SaveChanges();
        }

        public void updateHomework(DateTime Date, string Homework, string desc, Boolean status, int eid)
        {
            var getRow = (from a in dc.HomeWork where a.HomeWorkId == eid select a).FirstOrDefault();
            getRow.HomeWork1 = Homework;
            getRow.DateToCompleteWork = Date;
            getRow.Descriptions = desc;
            getRow.HomeWorkStatus = status;
            dc.SaveChanges();
        }


        public void delHomework(int id)
        {
            var getRow = (from a in dc.HomeWork where a.HomeWorkId == id select a).FirstOrDefault();
            dc.HomeWork.Remove(getRow);
            dc.SaveChanges();
        }

        public void addAssignment(int yearId, int classId, int secId, int subId, DateTime submissionDate, string Assignment, string desc, string fileName, int FacultyId)
        {
            Assignment add = new Assignment()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                SubjectId = subId,
                AssignmentPostedDate = date,
                DateOfSubmission = submissionDate,
                Assignment1 = Assignment,
                Descriptions = desc,
                AssignmentFileName = fileName,
                AssignmentStatus = true,
                EmployeeRegisterId = FacultyId
            };
            dc.Assignments.Add(add);
            dc.SaveChanges();
        }

        public List<HomeworkList> getAssignmentList(int yearId, int classId, int secId, int subId, DateTime fromDate, DateTime toDate, int FacultyId)
        {
            var list = (from a in dc.Assignments
                        where a.AcademicYearId == yearId &&
                                 a.ClassId == classId &&
                                 a.SectionId == secId &&
                                 a.SubjectId == subId &&
                                 a.EmployeeRegisterId == FacultyId &&
                                 a.DateOfSubmission >= fromDate &&
                                 a.DateOfSubmission <= toDate
                        select new HomeworkList
                        {
                            HomeWorkId = a.AssignmentId,
                            DateOfWorkPosted = a.AssignmentPostedDate,
                            DateToCompleteWork = a.DateOfSubmission,
                            HomeWork1 = a.Assignment1,
                            Descriptions = a.Descriptions,
                            HomeWorkStatus = a.AssignmentStatus,
                            HomeWorkFileName = a.AssignmentFileName
                        }).ToList();

            return list;
        }

        public void updateAssignmentFile(int eid, string fileName1)
        {
            var getRow = (from a in dc.Assignments where a.AssignmentId == eid select a).FirstOrDefault();
            getRow.AssignmentFileName = fileName1;
            dc.SaveChanges();
        }

        public void updateAssignment(DateTime Date, string Assignment, string desc, Boolean status, int eid)
        {
            var getRow = (from a in dc.Assignments where a.AssignmentId == eid select a).FirstOrDefault();
            getRow.Assignment1 = Assignment;
            getRow.DateOfSubmission = Date;
            getRow.Descriptions = desc;
            getRow.AssignmentStatus = status;
            dc.SaveChanges();
        }

        public void delAssignment(int id)
        {
            var getRow = (from a in dc.Assignments where a.AssignmentId == id select a).FirstOrDefault();
            dc.Assignments.Remove(getRow);
            dc.SaveChanges();
        }

        public List<RemarkList> getStudentRemarksList(int yearId, int classId, int secId, int stuId, DateTime fromDate, DateTime toDate, int FacultyId)
        {
            var list = ((from t1 in dc.ModuleInfoStudentRemarks
                         from t2 in dc.TechEmployees
                         where t1.AcademicYearId == yearId &&
                         t1.ClassId == classId &&
                         t1.SectionId == secId &&
                         t1.StudentRegisterId == stuId &&
                         t1.DateOfRemarks >= fromDate &&
                         t1.DateOfRemarks <= toDate &&
                         t1.EmployeeRegisterId == t2.EmployeeRegisterId
                         select new RemarkList { Remark = t1.Remark, DateOfRemarks = t1.DateOfRemarks, UserName = t2.EmployeeName })

                      .Union(from t2 in dc.ExamRemarks
                             where t2.AcademicYearId == yearId &&
                          t2.ClassId == classId &&
                          t2.SectionId == secId &&
                          t2.StudentRegisterId == stuId &&
                          t2.ExamDate >= fromDate &&
                          t2.ExamDate <= toDate
                             select new RemarkList { Remark = t2.ExamRemark1, DateOfRemarks = t2.ExamDate, UserName = t2.ExamRemark1 })).ToList();
            return list;
        }
        // student module

        public void addFacultyFeedback(int StudentRegId, int yearId, int classId, int secId, int subId, int FacultyId, string Feedback)
        {
            StudentFacultyFeedback add = new StudentFacultyFeedback()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                SubjectId = subId,
                EmployeeRegisterId = FacultyId,
                StudentRegisterId = StudentRegId,
                DateOfFeedback = date,
                Feedback = Feedback,
            };
            dc.StudentFacultyFeedbacks.Add(add);
            dc.SaveChanges();
        }

        public void addFacultyQueries(int StudentRegId, int yearId, int classId, int secId, int subId, int FacultyId, string Query)
        {
            StudentFacultyQuery add = new StudentFacultyQuery()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                SubjectId = subId,
                EmployeeRegisterId = FacultyId,
                StudentRegisterId = StudentRegId,
                DateOfQuery = date,
                Query = Query,
            };
            dc.StudentFacultyQueries.Add(add);
            dc.SaveChanges();
        }

        public List<StudentHomework> getStudentHomework(int yearId, int classId, int secId, int subId, DateTime date)
        {
            var list = (from a in dc.HomeWork
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.SubjectId == subId &&
                        a.DateOfWorkPosted == date
                        select new 
                        {
                            HomeWorkId = a.HomeWorkId,
                            PostedDate = a.DateOfWorkPosted,
                            Homework = a.HomeWork1,
                            Description = a.Descriptions,
                            HomeWorkFileName = a.HomeWorkFileName,
                            DateOfSubmission = a.DateToCompleteWork
                        }).ToList().Select(a=>new StudentHomework
                        {
                            HomeWorkId = a.HomeWorkId,
                            PostedDate = a.PostedDate.Value.ToString("dd MMM, yyyy"),
                            Homework = a.Homework,
                            Description = a.Description,
                            HomeWorkFileName = a.HomeWorkFileName,
                            DateOfSubmission = a.DateOfSubmission.Value.ToString("dd MMM, yyyy")
                        }).ToList();
            return list;
        }

        public List<StudentAssignment> getStudentAssignment(int yearId, int classId, int secId, int subId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in dc.Assignments
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.SubjectId == subId &&
                        a.AssignmentPostedDate >= fdate &&
                        a.AssignmentPostedDate <= toDate
                        select new 
                        {
                            AssignmentId = a.AssignmentId,
                            AssignmentPostedDate = a.AssignmentPostedDate,
                            Assignment1 = a.Assignment1,
                            Descriptions = a.Descriptions,
                            AssignmentFileName = a.AssignmentFileName,
                            DateOfSubmission = a.DateOfSubmission
                        }).ToList().Select(a=> new StudentAssignment{
                            AssignmentId = a.AssignmentId,
                            PostedDate = a.AssignmentPostedDate.Value.ToString("dd MMM, yyyy"),
                            Assignment = a.Assignment1,
                            Description = a.Descriptions,
                            AssignmentFileName = a.AssignmentFileName,
                            SubmissionDate = a.DateOfSubmission.Value.ToString("dd MMM, yyyy")
                        }).ToList();
            return list;
        }

        public List<SubjectNotes> getStudentNotes(int yearId, int classId, int secId, int subId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in dc.SubjectNotes
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.SubjectId == subId &&
                        a.DateOfNotes >= fdate &&
                        a.DateOfNotes <= toDate
                        select new SubjectNotes
                        {
                            NotesId = a.NotesId,
                            NotesPostedDate = a.DateOfNotes,
                            Topic = a.Topic,
                            Descriptions = a.Descriptions,
                            NotesFileName = a.NotesFileName,
                        }).ToList();
            return list;
        }

        // parent
        public List<ParentHomework> getParentHomework(int yearId, int classId, int secId, DateTime date)
        {
            var list = (from a in dc.HomeWork
                        from b in dc.AssignSubjectToSections
                        from c in dc.Subjects
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.DateOfWorkPosted == date &&
                        a.SubjectId == b.SubjectId &&
                        a.SubjectId == c.SubjectId
                        select new ParentHomework
                        {
                            HomeWorkId = a.HomeWorkId,
                            DateOfWorkPosted = a.DateOfWorkPosted,
                            Subject = c.SubjectName,
                            HomeWork1 = a.HomeWork1,
                            Descriptions = a.Descriptions,
                            HomeWorkFileName = a.HomeWorkFileName,
                            DateToCompleteWork = a.DateToCompleteWork
                        }).ToList();
            return list;
        }

        public List<ParentAssignment> getParentAssignment(int yearId, int classId, int secId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in dc.Assignments
                        from b in dc.AssignSubjectToSections
                        from c in dc.Subjects
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.AssignmentPostedDate >= fdate &&
                        a.AssignmentPostedDate <= toDate &&
                        a.SubjectId == c.SubjectId
                        select new ParentAssignment
                        {
                            AssignmentId = a.AssignmentId,
                            AssignmentPostedDate = a.AssignmentPostedDate,
                            Subject = c.SubjectName,
                            Assignment1 = a.Assignment1,
                            Descriptions = a.Descriptions,
                            AssignmentFileName = a.AssignmentFileName,
                            DateOfSubmission = a.DateOfSubmission
                        }).ToList();
            return list;
        }

        public void addParentFacultyFeedback(int ParentRegId, int StudentRegId, int yearId, int classId, int secId, int subId, int FacultyId, string Feedback)
        {
            ParentFacultyFeedback add = new ParentFacultyFeedback()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                SubjectId = subId,
                EmployeeRegisterId = FacultyId,
                StudentRegisterId = StudentRegId,
                ParentRegisterId = ParentRegId,
                DateOfFeedback = date,
                Feedback = Feedback,
            };
            dc.ParentFacultyFeedbacks.Add(add);
            dc.SaveChanges();
        }

        public void AddParentLeaveRequest(int ParentRegId, int StudentRegId, int yearId, int classId, int secId, DateTime fdate, DateTime toDate, int NoOfDays, string reason, int? EmployeeRegId)
        {
            ParentLeaveRequest add = new ParentLeaveRequest()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                ParentRegisterId = ParentRegId,
                StudentRegisterId = StudentRegId,
                FromDate = fdate,
                ToDate = toDate,
                NumberOfDays = NoOfDays,
                LeaveReason = reason,
                Status = "Pending",
                EmployeeRegisterId = EmployeeRegId
            };
            dc.ParentLeaveRequests.Add(add);
            dc.SaveChanges();
        }

        public void AddFacultyLeaveRequest(int FacultyId, DateTime fdate, DateTime toDate, int NoOfDays, string reason, int ApproverId)
        {
            FacultyLeaveRequest add = new FacultyLeaveRequest()
            {
                EmployeeRegisterId = FacultyId,
                FromDate = fdate,
                ToDate = toDate,
                NumberOfDays = NoOfDays,
                LeaveReason = reason,
                Status = "Pending",
                ApproverId = ApproverId
            };
            dc.FacultyLeaveRequests.Add(add);
            dc.SaveChanges();
        }

        public List<RollNumberList> getRollNumberFormat(int acid)
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.Sections
                       from t4 in dc.RollNumberFormats
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.SectionId == t4.SectionId && t4.AcademicYearId == acid
                       select new RollNumberList { AcademicYear = t1.AcademicYear1, Class = t2.ClassType, Section = t3.SectionName, RollNumberFormatId = t4.RollNumberFormatId, RollNumberFormat = t4.RollNumber, Status = t4.Status.Value }).ToList();
            return ans;
        }

        public bool checkRollNumberFormat(int acid, int cid, int sec_id)
        {
            var ans = dc.RollNumberFormats.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void addRollNumberFormat(int acid, int cid, int sec_id,string rollNumber)
        {
            RollNumberFormat roll = new RollNumberFormat()
            {
                AcademicYearId = acid,
                ClassId = cid,
                SectionId = sec_id,
                RollNumber = rollNumber,
                Status = true

            };
            dc.RollNumberFormats.Add(roll);
            dc.SaveChanges();
        }

        public void addStudentRollNumber(int acid, int cid, int sec_id, int p, string roll_number)
        {
            StudentRollNumber number = new StudentRollNumber()
            {
                AcademicYearId=acid,
                ClassId=cid,
                SectionId=sec_id,
                StudentRegisterId=p,
                RollNumber=roll_number,
                Status=true
            };
            dc.StudentRollNumbers.Add(number);
            dc.SaveChanges();

            var update = dc.AssignClassToStudents.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) && q.StudentRegisterId.Value.Equals(p)).First();
            update.HaveRollNumber = true;
            dc.SaveChanges();
        }

        public bool checkSection(string secname, int cid)
        {
            var ans = dc.Sections.Where(q => q.ClassId.Value.Equals(cid) && q.SectionName.Equals(secname)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkSection(int cid)
        {
            var ans = dc.Sections.Where(q => q.ClassId.Value.Equals(cid) ).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void updateSection(int cid, string sectionName)
        {
            var update = dc.Sections.Where(q => q.ClassId.Value.Equals(cid)).First();
            update.SectionName = sectionName;

            dc.SaveChanges();
            
        }

        public bool checkAssignClassStudent(int acid, int cid, int sec_id, int std_id)
        {
            var ans = dc.AssignClassToStudents.Where(q =>q.AcademicYearId.Value.Equals(acid)&& q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) && q.StudentRegisterId.Value.Equals(std_id)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void addAssignClassStudent(int acid, int cid, int sec_id, int std_id)
        {
            AssignClassToStudent std = new AssignClassToStudent()
            {
                AcademicYearId=acid,
                ClassId=cid,
                SectionId=sec_id,
                StudentRegisterId=std_id
            };

            dc.AssignClassToStudents.Add(std);
            dc.SaveChanges();
        }

        public void updateFeeConfig(int feeid, int fee, DateTime ldate)
        {
            var update = dc.Fees.Where(q => q.FeeId.Equals(feeid)).First();
            update.Amount = fee;
            update.LastDate = ldate;
            dc.SaveChanges();
        }

        public bool checkFeeConfig(int acid, int fid, int cid, int fee, DateTime ldate)
        {
            var ans = dc.Fees.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.FeeCategoryId.Value.Equals(fid) && q.Amount.Equals(fee) && q.LastDate.Value.Equals(ldate)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void addUserLog(string sessionid, string username, string p, string ClientIP, DateTime login,int empregid)
        {
            UserLog log = new UserLog()
            {
                SessionId=sessionid,
                UserId=username,
                UserType=p,
                IpAddress=ClientIP,
                LoginTime=login,
                LogoutTime = login,
                UserRegId = empregid
            };

            dc.UserLogs.Add(log);
            dc.SaveChanges();
        }

        public void UpdateUserLog(string sessionid, string username, string p, string ClientIP, DateTime login, DateTime logout)
        {
            var Update = dc.UserLogs.Where(q => q.SessionId.Equals(sessionid)).FirstOrDefault();
            if (Update != null)
            {
                Update.LogoutTime = logout;
                dc.SaveChanges();
            }
        }

        public List<Student> SearchStudentProfile(int id)
        {
            var q1 = dc.Students.Where(q => q.StudentRegisterId.Equals(id)).ToList();
            return q1;
        }
       
        public List<Eprofile> SearchTechEmployeeProfile(int id)
        {
            var q1 = (from a in dc.TechEmployees
                      where a.EmployeeRegisterId == id
                      select new Eprofile
                      {
                          EmployeeRegisterId = a.EmployeeRegisterId,
                          EmployeeId = a.EmployeeId,
                          EmployeeName = a.EmployeeName,
                          Gender = a.Gender,
                          BloodGroup = a.BloodGroup,
                          Community = a.Community,
                          Email = a.Email,
                          Religion = a.Religion,
                          Nationality = a.Nationality,
                          Contact = a.Mobile,
                          Address = a.AddressLine1,
                          State = a.State,
                          DOB = a.DOB.Value,
                          DOL = a.DOL.Value,
                          DOR = a.DOR.Value,
                         
                          EmergencyContactPerson = a.EmergencyContactPerson,
                          EmergencyContactNumber = a.EmergencyContactNumber,
                          EmployeeStatus = a.EmployeeStatus,
                          Experience = a.Experience,
                         
                          ImgFile = a.ImgFile,
                          City = a.City


                      }).ToList();
           
            return q1;

        }
        public List<Eprofile> SearchOtherEmployeeProfile(int id)
        {
            var q2 = (from a in dc.OtherEmployees
                      where a.EmployeeRegisterId == id
                      select new Eprofile
                      {
                          EmployeeRegisterId = a.EmployeeRegisterId,
                          EmployeeId = a.EmployeeId,
                          EmployeeName = a.EmployeeName,
                          Gender = a.Gender,
                          BloodGroup = a.BloodGroup,
                          Community = a.Community,
                          Email = a.Email,
                          Religion = a.Religion,
                          Nationality = a.Nationality,
                          Contact = a.Contact,
                          Address = a.Address,
                          State = a.State,
                          DOB = a.DOB.Value,
                          DOL = a.DOL.Value,
                          DOR = a.DOR.Value,
                          Qualification = a.Qualification,
                          EmergencyContactPerson = a.EmergencyContactPerson,
                          EmergencyContactNumber = a.EmergencyContactNumber,
                          EmployeeStatus = a.EmployeeStatus,
                          Experience = a.Experience,
                          Expertise = a.Expertise,
                          ImgFile = a.ImgFile,
                          City = a.City
                      }).ToList();
           
            return q2;
        }
        public List<getclassandsection> SearchClassList()
        {
            var ans = (from b in dc.Classes
                       from c in dc.Sections
                       where
                       b.ClassId == c.ClassId
                       select new getclassandsection
                       {
                           ClassName = b.ClassType,
                           SectionName = c.SectionName
                       }).Distinct().OrderBy(q=>q.ClassName).ToList();

            return ans;

        }
        public List<getclassandsection> SearchClassIncharge(int id)
        {
            var ans = (from a in dc.AssignClassTeachers
                       from b in dc.Classes
                       from c in dc.Sections
                       from d in dc.TechEmployees
                       where a.AcademicYearId == id &&
                       a.ClassId == b.ClassId &&
                       a.SectionId == c.SectionId &&
                       a.EmployeeRegisterId==d.EmployeeRegisterId
                       
                       select new getclassandsection
                       {
                           ClassName = b.ClassType,
                           SectionName = c.SectionName,
                           ClassTeacher=d.EmployeeName
                       }).Distinct().ToList();

            return ans;
        }
        public List<getclassandsection> SearchClassSubject1(int yid, int cid, int sid)
        {
            var ans = (from a in dc.AssignSubjectToSections
                       from b in dc.Classes
                       from c in dc.Sections
                       from d in dc.Subjects

                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && b.ClassId == cid && c.ClassId == cid && c.SectionId == sid && a.SubjectId == d.SubjectId
                       select new getclassandsection
                       {
                           ClassName = b.ClassType,
                           SectionName = c.SectionName,
                           subjectName = d.SubjectName

                       }
                          ).Distinct().ToList();
            return ans;

        }
        public List<getclassandsection> SearchClassSubject(int yid,int cid,int sid)
        {
            var ans = (from a in dc.AssignSubjectToSections
                       from b in dc.Classes
                       from c in dc.Sections
                       from d in dc.Subjects
                      
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && b.ClassId == cid && c.ClassId==cid  && c.SectionId == sid && a.SubjectId==d.SubjectId 
                       select new getclassandsection
                       {
                           ClassName = b.ClassType,
                           SectionName = c.SectionName,
                           subjectName=d.SubjectName

                       }
                          ).Distinct().ToList();

            return ans;

        }
        public List<getclassandsection> SearchSubjectTeacher(int yid, int cid, int sid)
        {
            var ans = (from a in dc.AssignFacultyToSubjects
                       from b in dc.Classes
                       from c in dc.Sections
                       from d in dc.Subjects
                       from e in dc.TechEmployees
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && b.ClassId == cid && c.ClassId == cid && c.SectionId == sid &&  a.SubjectId== d.SubjectId && a.EmployeeRegisterId==e.EmployeeRegisterId
                       select new getclassandsection
                       {
                           ClassName = b.ClassType,
                           SectionName = c.SectionName,
                           subjectName = d.SubjectName,
                           ClassTeacher=e.EmployeeName

                       }
                          ).Distinct().ToList();
            return ans;
        }
        public List<getclassandsection> SearchStudentNameList(int yid, int cid, int sid)
        {
            var ans = (from a in dc.AssignClassToStudents
                       from b in dc.Classes
                       from c in dc.Sections
                       from d in dc.Subjects
                       from e in dc.Students
                       from f in dc.StudentRollNumbers
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && b.ClassId == cid && c.ClassId == cid && c.SectionId == sid && a.StudentRegisterId == e.StudentRegisterId && e.StudentRegisterId == f.StudentRegisterId && a.StudentRegisterId == f.StudentRegisterId
                       select new getclassandsection
                       {
                           rollnumber = f.RollNumber,
                           StudentName = e.FirstName + " " + e.LastName
                       }
                           ).Distinct().OrderBy(x=>x.StudentName).ToList();
            return ans;
        }
       
        public List<getclassandsection> StudentProgress(int yid, int cid, int sid, int stu_id)
        {
            var ans = (from a in dc.StudentMarks

                       from d in dc.Exams
                       from e in dc.Students
                       from f in dc.Subjects
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.StudentRegisterId == stu_id && e.StudentRegisterId == stu_id && a.ExamId == d.ExamId &&
                       a.SubjectId == f.SubjectId
                       select new getclassandsection
                       {
                           SubjectMark = a.Mark,
                           ExamType = d.ExamName,
                           subjectName = f.SubjectName

                       }
                           ).ToList();
            return ans;
        }
        public List<getclassandsection> StudentGradeProgress(int yid, int cid, int sid, int stu_id)
        {
            var ans = (from a in dc.StudentGrades

                       from d in dc.Exams
                       from e in dc.Students
                       from f in dc.Subjects
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.StudentRegisterId == stu_id && e.StudentRegisterId == stu_id && a.ExamId == d.ExamId &&
                       a.SubjectId == f.SubjectId
                       select new getclassandsection
                       {
                           Grade = a.Grade,
                           ExamType = d.ExamName,
                           subjectName = f.SubjectName

                       }
                          ).ToList();

            return ans;

        }
        public List<totalmark> GetTotalMark(int yid, int cid, int sid, int stu_id)
        {

            
            var ans = (from a in dc.MarkTotals
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.StudentRegisterId == stu_id 
                       select new totalmark { Totalmark=a.TotalMark}).ToList();
            return ans;

        }
       
        public List<FeeInformation> Feedetails(int yid, int cid)
        {
            var ans = (from a in dc.Fees
                       from b in dc.FeeCategories
                       where a.AcademicYearId == yid && a.ClassId == cid && a.FeeCategoryId == b.FeeCategoryId
                       select new FeeInformation
                       {
                           FeeCategory = b.FeeCategoryName,
                           FeeAmount = a.Amount
                       }).ToList();
            return ans;

        }
        public List<FeeInformation> TermFeedetails(int yid, int cid, int pid, int tid)
        {
            var ans = (from a in dc.TermFees
                       from b in dc.FeeCategories
                       from c in dc.Terms
                       where a.AcademicYearId == yid && a.ClassId == cid && a.FeeCategoryId == b.FeeCategoryId && a.PaymentTypeId == pid && a.TermId == tid && a.TermId == c.TermId
                       select new FeeInformation
                       {
                           term = c.TermName,
                           FeeCategory = b.FeeCategoryName,
                           FeeAmount = a.Amount
                       }).OrderBy(x => x.term).ToList();
            return ans;
        }

        public bool checkFacilityIncharge(int acid, int fid, string empid, int empRegId)
        {
            var ans = dc.FacilityIncharges.Where(q => q.AcademicYearId.Value.Equals(acid) && q.FacilityId.Value.Equals(fid) && q.EmployeeId.Equals(empid) && q.EmployeeRegisterId.Value.Equals(empRegId)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void updateFacilityIncharge(int facId1, int acid, int fid, string empid, int empRegId, string note)
        {
            var update = dc.FacilityIncharges.Where(q => q.FacilityInchargeId.Equals(facId1)).First();
            update.AcademicYearId = acid;
            update.FacilityId = fid;
            update.EmployeeId = empid;
            update.EmployeeRegisterId = empRegId;
            update.Note = note;
            dc.SaveChanges();
        }

        public void deleteFacilityIncharge(int facId)
        {
            var delete = dc.FacilityIncharges.Where(q => q.FacilityInchargeId.Equals(facId)).First();
            dc.FacilityIncharges.Remove(delete);
            dc.SaveChanges();
        }

        public bool checkActivityIncharge(int acid, int aid, string empid, int empRegId)
        {
            var ans = dc.ActivityIncharges.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ActivityId.Value.Equals(aid) && q.EmployeeId.Equals(empid) && q.EmployeeRegisterId.Value.Equals(empRegId)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void updateActivityIncharge(int activityId, int acid, int aid, string empid, int empRegId, string note)
        {
            var update = dc.ActivityIncharges.Where(q => q.ActivityInchargeId.Equals(activityId)).First();
            update.AcademicYearId = acid;
            update.ActivityId = aid;
            update.EmployeeId = empid;
            update.EmployeeRegisterId = empRegId;
            update.Note = note;

            dc.SaveChanges();
        }

        public void deleteActivityIncharge(int activityId)
        {
            var delete = dc.ActivityIncharges.Where(q => q.ActivityInchargeId.Equals(activityId)).First();
            dc.ActivityIncharges.Remove(delete);
            dc.SaveChanges();
        }

        public bool checkActivityStudent(int acid, int cid, int sec_id, int std_id, int aid)
        {
            var ans = dc.AssignActivityToStudents.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ActivityId.Value.Equals(aid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) && q.StudentRegisterId.Value.Equals(std_id)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal void updateActivityStudent(int s_aid, int acid, int cid, int sec_id, int std_id, int aid, string note)
        {
            var update = dc.AssignActivityToStudents.Where(q => q.StudentActivityId.Equals(s_aid)).First();
            update.ActivityId = aid;
            update.Note = note;
            dc.SaveChanges();
        }

        internal void deleteActivityStudent(int s_aid)
        {
            var delete = dc.AssignActivityToStudents.Where(q => q.StudentActivityId.Equals(s_aid)).First();
            dc.AssignActivityToStudents.Remove(delete);
            dc.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
    
