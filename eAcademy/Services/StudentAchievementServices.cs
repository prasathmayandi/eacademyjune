﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class StudentAchievementServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        public List<AchievementList> getStudentAchievement1()
        {
            var list = (from a in db.StudentAchievements
                        from b in db.Students
                        from c in db.Classes
                        from d in db.Sections
                        where a.StudentRegisterId == b.StudentRegisterId &&
                           a.ClassId == c.ClassId &&
                           a.SectionId == d.SectionId
                        select new 
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Class = c.ClassType,
                            Section = d.SectionName,
                            FName = b.FirstName,
                            LName = b.LastName,
                            AcademicYear = a.AcademicYearId,
                        }).ToList().Select(a => new AchievementList
                        {
                            Date = a.DateOfAchievement.Value.ToString("dd MMM, yyyy"),
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Class = a.Class,
                            Section = a.Section,
                            Name = a.FName+" "+a.LName,
                            Ach_allAcademicYearsA1 = a.AcademicYear,
                            txt_doa_search = a.DateOfAchievement
                        }).ToList();
            return list;
        }
        public List<AchievementList> getStudentAchievement(int yearId)
        {
            var list = (from a in db.StudentAchievements
                        from b in db.Students
                        from c in db.Classes
                        from d in db.Sections
                        where a.AcademicYearId == yearId &&
                           a.StudentRegisterId == b.StudentRegisterId &&
                           a.ClassId == c.ClassId &&
                           a.SectionId == d.SectionId
                        select new AchievementList
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Class = c.ClassType,
                            Section = d.SectionName,
                            Name = b.FirstName+" "+b.LastName                        
                        }).ToList();
            return list;
        }

        public List<AchievementList> getStudentAchievement()
        {
            var list = (from a in db.StudentAchievements
                        from b in db.Students
                        from c in db.Classes
                        from d in db.Sections
                        where a.StudentRegisterId == b.StudentRegisterId &&
                              a.ClassId == c.ClassId &&
                              a.SectionId == d.SectionId
                        select new
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            FName = b.FirstName,
                            LName = b.LastName,
                            Class = c.ClassType,
                            Section = d.SectionName,
                            Id = b.StudentRegisterId,
                            AchievementId = a.Id,
                            AcademicYearId = a.AcademicYearId
                        }).ToList().Select(c => new AchievementList
                        {
                            Date = c.DateOfAchievement.Value.ToString("dd MMM, yyyy"),
                            Achievement = c.Achievement,
                            Descriptions = c.Descriptions,
                            Name = c.FName+" "+c.LName,
                            Class = c.Class,
                            Section = c.Section,
                            AchievementId = c.AchievementId,
                            AchievementId1 = QSCrypt.Encrypt(c.AchievementId.ToString()),
                            txt_doa_search = c.DateOfAchievement,
                            Ach_allAcademicYearsA1 = c.AcademicYearId
                        }).ToList();
            return list;
        }

        public StudentAchievement getAchievementById(int AchievementId)
        {
            var getRow = (from a in db.StudentAchievements where a.Id == AchievementId select a).FirstOrDefault();
            return getRow;
        }

        public List<AchievementList> sortStudentAchievementByYear(int Yearid)
        {
            var list = (from a in db.StudentAchievements
                        from b in db.Students
                        from c in db.Classes
                        from d in db.Sections
                        where a.StudentRegisterId == b.StudentRegisterId &&
                              a.ClassId == c.ClassId &&
                              a.SectionId == d.SectionId &&
                              a.AcademicYearId == Yearid
                        select new AchievementList
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = b.FirstName+" "+b.LastName,
                            Class = c.ClassType,
                            Section = d.SectionName,
                            Id = b.StudentRegisterId,
                            AchievementId = a.Id
                        }).ToList();
            return list;
        }

        public void addStudentAchievement(int yearId, int classId, int secId, int studentId, DateTime doa, string achievement, string desc)
        {
            var get = db.StudentAchievements.Where(q => q.DateOfAchievement == doa && q.Achievement == achievement && q.AcademicYearId == yearId && q.ClassId == classId && q.SectionId == secId && q.StudentRegisterId==studentId && q.Descriptions== desc).FirstOrDefault();
            if (get == null)
            {

                StudentAchievement add = new StudentAchievement()
                {
                    AcademicYearId = yearId,
                    ClassId = classId,
                    SectionId = secId,
                    StudentRegisterId = studentId,
                    DateOfAchievement = doa,
                    Achievement = achievement,
                    Descriptions = desc
                };
                db.StudentAchievements.Add(add);
                db.SaveChanges();
            }
        }

        public void updateStudentAchievement(DateTime doa, string achievement, string desc, int AchievementEId)
        {
            var getRow = (from a in db.StudentAchievements where a.Id == AchievementEId select a).FirstOrDefault();
            if (getRow != null)
            {
                getRow.DateOfAchievement = doa;
                getRow.Achievement = achievement;
                getRow.Descriptions = desc;
                db.SaveChanges();
            }
        }

        public void delStudentAchievement(int id)
        {
            var getRow = (from a in db.StudentAchievements where a.Id == id select a).FirstOrDefault();
            if (getRow != null)
            {
                db.StudentAchievements.Remove(getRow);
                db.SaveChanges();
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}