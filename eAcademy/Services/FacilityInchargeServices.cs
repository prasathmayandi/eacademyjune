﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class FacilityInchargeServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public FaciltyInchargeList FacilityIncharge(int facId,string empid)
        {
            var ans = ((from a in dc.FacilityIncharges
                         from b in dc.AcademicYears
                         from c in dc.TechEmployees
                         from d in dc.Facilities
                         where a.FacilityInchargeId == facId &&
                         a.EmployeeId==empid &&
                         a.AcademicYearId == b.AcademicYearId &&
                         a.EmployeeRegisterId == c.EmployeeRegisterId &&
                         c.EmployeeId ==a.EmployeeId &&
                         a.FacilityId == d.FacilityId
                        select new FaciltyInchargeList { Fac = d.FacilityName, FacId = d.FacilityId, Emp = c.EmployeeName, EmpRegId = c.EmployeeRegisterId, EmpId = c.EmployeeId, AcYear = b.AcademicYear1, AcYearId = a.AcademicYearId, Note = a.Note, status = a.Status, EmpType="Tech" })
                         .Union
                         (from a in dc.FacilityIncharges
                          from b in dc.AcademicYears
                          from c in dc.OtherEmployees
                          from d in dc.Facilities
                          where a.FacilityInchargeId == facId &&
                          a.EmployeeId == empid &&
                          a.AcademicYearId == b.AcademicYearId &&
                          c.EmployeeId == a.EmployeeId &&
                          a.EmployeeRegisterId == c.EmployeeRegisterId &&
                          a.FacilityId == d.FacilityId
                          select new FaciltyInchargeList { Fac = d.FacilityName, FacId = d.FacilityId, Emp = c.EmployeeName, EmpRegId = c.EmployeeRegisterId, EmpId = c.EmployeeId, AcYear = b.AcademicYear1, AcYearId = a.AcademicYearId, Note = a.Note, status = a.Status, EmpType="Other" })).First();

            return ans;
        }



        public void addFacilityIncharge(int acid, int fid, int empRegId, string Note, string empid)
        {
            FacilityIncharge fac = new FacilityIncharge()
            {
                AcademicYearId = acid,
                FacilityId = fid,
                EmployeeRegisterId = empRegId,
                Note = Note,
                EmployeeId = empid,
                Status=true
            };
            dc.FacilityIncharges.Add(fac);
            dc.SaveChanges();
        }

        public List<FaciltyIncharge> getFacilityIncharge()
        {
            var list = ((from a in dc.FacilityIncharges
                         from b in dc.TechEmployees
                         from c in dc.AcademicYears
                         from d in dc.Facilities
                         where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                               a.AcademicYearId == c.AcademicYearId &&
                               a.FacilityId == d.FacilityId &&
                               a.EmployeeId == b.EmployeeId                                
                         select new 
                         {
                             Fac_Incharge = b.EmployeeName,
                             Fac_AcYear = c.AcademicYear1,
                             Fac_AcYearId = c.AcademicYearId,
                             Fac = d.FacilityName,
                             EmpId = b.EmployeeId,
                             EmpContact = b.Mobile,
                             Note = a.Note,
                             FacilityInchargeId = a.FacilityInchargeId,
                             Status=a.Status
                         }).ToList().Select(q=>new FaciltyIncharge
                         {
                             Fac_Incharge = q.Fac_Incharge,
                             Fac_AcYear = q.Fac_AcYear,
                             Fac_AcYearId = q.Fac_AcYearId,
                             Fac = q.Fac,
                             EmpId = q.EmpId,
                             EmpContact = q.EmpContact,
                             Note = q.Note,
                             FacilityInchargeId = q.FacilityInchargeId,
                             FacilityId=QSCrypt.Encrypt(q.FacilityInchargeId.ToString()+"/"+q.EmpId),
                             Status=q.Status,
                             FacilityIncharge=q.Fac_Incharge,
                             Facility=q.Fac,
                             EmployeeId=q.EmpId,
                             AcademicYear=q.Fac_AcYear,
                             InchargeContact=q.EmpContact,
                             EmpType="Tech"
                         })                        
                    .Union(from a in dc.FacilityIncharges
                           from b in dc.OtherEmployees
                           from c in dc.AcademicYears
                           from d in dc.Facilities
                           where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                                 a.AcademicYearId == c.AcademicYearId &&
                                 a.FacilityId == d.FacilityId &&
                                 a.EmployeeId == b.EmployeeId                                
                           select new FaciltyIncharge
                           {
                               Fac_Incharge = b.EmployeeName,
                               Fac_AcYear = c.AcademicYear1,
                               Fac_AcYearId = c.AcademicYearId,
                               Fac = d.FacilityName,
                               EmpId = b.EmployeeId,
                               EmpContact = b.Contact,
                               Note = a.Note,
                               FacilityInchargeId = a.FacilityInchargeId,
                               Status=a.Status
                           }).ToList().Select(q => new FaciltyIncharge
                           {
                               Fac_Incharge = q.Fac_Incharge,
                               Fac_AcYear = q.Fac_AcYear,
                               Fac_AcYearId = q.Fac_AcYearId,
                               Fac = q.Fac,
                               EmpId = q.EmpId,
                               EmpContact = q.EmpContact,
                               Note = q.Note,
                               FacilityInchargeId = q.FacilityInchargeId,
                               FacilityId = QSCrypt.Encrypt(q.FacilityInchargeId.ToString()+"/"+q.EmpId),
                               Status=q.Status,
                               FacilityIncharge = q.Fac_Incharge,
                               Facility = q.Fac,
                               EmployeeId = q.EmpId,
                               AcademicYear = q.Fac_AcYear,
                               InchargeContact = q.EmpContact,
                               EmpType = "Other"
                           })).ToList();
        
            return list;
        }


        public bool checkFacilityIncharge(int acid, int fid, string empid, int empRegId)
        {
            var ans = dc.FacilityIncharges.Where(q => q.AcademicYearId.Value.Equals(acid) && q.FacilityId.Value.Equals(fid) && q.EmployeeId.Equals(empid) && q.EmployeeRegisterId.Value.Equals(empRegId)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void updateFacilityIncharge(int facId1, int acid, int fid, string empid, int empRegId, string note, string Status)
        {
            var update = dc.FacilityIncharges.Where(q => q.FacilityInchargeId.Equals(facId1)).First();
            update.AcademicYearId = acid;
            update.FacilityId = fid;
            update.EmployeeId = empid;
            update.EmployeeRegisterId = empRegId;
            update.Note = note;
            if (Status == "Active")
            {
                update.Status = true;
            }

            else
            {
                update.Status = false;
            }

            dc.SaveChanges();
        }

        public void deleteFacilityIncharge(int facId)
        {
            var delete = dc.FacilityIncharges.Where(q => q.FacilityInchargeId.Equals(facId)).First();
            dc.FacilityIncharges.Remove(delete);
            dc.SaveChanges();
        }


        //jegadeesh

        public List<extracurricular> GetFacilitiesIncharge(int yearid)
        {
            var ans = ((from a in dc.Facilities
                        from b in dc.FacilityIncharges
                        from c in dc.TechEmployees
                        from e in dc.AcademicYears
                        where b.AcademicYearId == yearid && a.FacilityId == b.FacilityId && b.EmployeeId == c.EmployeeId && b.AcademicYearId == e.AcademicYearId
                        select new extracurricular
                        {
                            FacilityName = a.FacilityName,
                            FacultyInchargeName = c.EmployeeName,
                            FacultyInchargeId = c.EmployeeId,
                            ContactNumber=c.Mobile,
                            usertype = "t"
                        })
                              .Union(from a in dc.Facilities
                                     from b in dc.FacilityIncharges
                                     from c in dc.OtherEmployees
                                     from e in dc.AcademicYears
                                     where b.AcademicYearId == yearid && a.FacilityId == b.FacilityId && b.EmployeeId == c.EmployeeId && b.AcademicYearId == e.AcademicYearId
                                     select new extracurricular
                                     {
                                         FacilityName = a.FacilityName,
                                         FacultyInchargeName = c.EmployeeName,
                                         FacultyInchargeId = c.EmployeeId,
                                         ContactNumber=c.Contact,
                                         usertype = "o"
                                     })).ToList();

            return ans;
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}