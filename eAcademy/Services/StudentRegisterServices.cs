﻿using eAcademy.DataModel;
using System;
using System.Linq;

namespace eAcademy.Services
{
    public class StudentRegisterServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public void AddStudentdetailsForm4(int regid, string sfname, string smname, string slname, string sgender, DateTime? dob, string pob, int? community, string religion, string nationality, int? blood, int? applyclass,
         string addr1, string addr2, string city, string state, string country, long? postel, int? NoOfsibling, string height, string weight, string identificationmark, string email, long? contact, string peraddr1, string peraddr2, string percity, string perstate, string percountry, long? perpostel, int? acyear, string distance, string SiblingStatus)
        {
            PreAdmissionStudentRegister add = new PreAdmissionStudentRegister()
            {
                OnlineRegisterId = regid,
                StuFirstName = sfname,
                StuMiddlename = smname,
                StuLastname = slname,
                Gender = sgender,
                DOB = dob,
                PlaceOfBirth = pob,
                Community = community,
                Religion = religion,
                Nationality = nationality,
                BloodGroup = blood,
                AdmissionClass = applyclass,
                LocAddress1 = addr1,
                LocAddress2 = addr2,
                LocCity = city,
                LocState = state,
                LocCountry = country,
                LocPostelcode = postel,
                Height = height,
                Weights = weight,
                IdentificationMark = identificationmark,
                Email = email,
                Contact = contact,
                PerAddress1 = peraddr1,
                PerAddress2 = peraddr2,
                PerCity = percity,
                PerState = perstate,
                PerCountry = percountry,
                PerPostelcode = perpostel,

                NoOfSibling = NoOfsibling,
                AcademicyearId = acyear,
                Distance =Convert.ToInt32(distance),
                SiblingStatus = SiblingStatus
            };
            dc.PreAdmissionStudentRegisters.Add(add);
            dc.SaveChanges();
        }

        public void UpdateStudentdetailsForm4(int regid, string sfname, string smname, string slname, string sgender, DateTime? dob, string pob, int? community, string religion, string nationality, int? blood, int? applyclass,
           string addr1, string addr2, string city, string state, string country, long? postel, int? NoOfsibling, string height, string weight, string identificationmark, string email, long? contact, string peraddr1, string peraddr2, string percity, string perstate, string percountry, long? perpostel, int? acyear, string distance, string SiblingStatus)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.OnlineRegisterId == regid select a).FirstOrDefault();
            if (getrow != null)
            {

                getrow.StuFirstName = sfname;
                getrow.StuMiddlename = smname;
                getrow.StuLastname = slname;
                getrow.Gender = sgender;
                getrow.DOB = dob;
                getrow.PlaceOfBirth = pob;
                getrow.Community = community;
                getrow.Religion = religion;
                getrow.Nationality = nationality;
                getrow.BloodGroup = blood;
                getrow.AdmissionClass = applyclass;
                getrow.LocAddress1 = addr1;
                getrow.LocAddress2 = addr2;
                getrow.LocCity = city;
                getrow.LocState = state;
                getrow.LocCountry = country;
                getrow.LocPostelcode = postel;
                getrow.Height = height;
                getrow.Weights = weight;
                getrow.IdentificationMark = identificationmark;
                getrow.Email = email;
                getrow.Contact = contact;
                getrow.PerAddress1 = peraddr1;
                getrow.PerAddress2 = peraddr2;
                getrow.PerCity = percity;
                getrow.PerState = perstate;
                getrow.PerCountry = percountry;
                getrow.PerPostelcode = perpostel;
                getrow.NoOfSibling = NoOfsibling;
                getrow.Distance =Convert.ToInt32(distance);
                getrow.AcademicyearId = acyear;
                getrow.SiblingStatus = SiblingStatus;
                dc.SaveChanges();
            }

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}