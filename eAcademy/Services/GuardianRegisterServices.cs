﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class GuardianRegisterServices:IDisposable
    {

        EacademyEntities dc = new EacademyEntities();
        public int GuardiandetailsCopyfromPreAdmissiontoguardian(PreAdmissionGuardianRegister old, int primaryuserid, int studentregid)
        {
            var checkguardianexist = (from a in dc.AdmissionTransactions
                                    from b in dc.GuardianRegisters
                                    where a.PrimaryUserRegisterId == primaryuserid && a.StudentRegisterId != studentregid && b.GuardianRegisterId == a.GuardianRegisterId && b.GuardianFirstname == old.GuardianFirstname &&
                                       b.GuardianLastName == old.GuardianLastName //&& b.GuardianDOB == old.GuardianDOB
                                    select b).FirstOrDefault();
            if (checkguardianexist != null)
            {
                return checkguardianexist.GuardianRegisterId;
            }
            else
            {
                GuardianRegister user = new GuardianRegister()
                {
                    GuardianFirstname = old.GuardianFirstname,
                    GuardianLastName = old.GuardianLastName,
                    GuardianDOB = old.GuardianDOB,
                    GuardianQualification = old.GuardianQualification,
                    GuardianOccupation = old.GuardianOccupation,
                    GuardianMobileNo = old.GuardianMobileNo,
                    GuardianEmail = old.GuardianEmail,
                    GuardianGender=old.GuardianGender,
                    GuRelationshipToChild=old.GuRelationshipToChild,
                    GuardianIncome=old.GuardianIncome
                };
                dc.GuardianRegisters.Add(user);
                dc.SaveChanges();
                int guardianregid = user.GuardianRegisterId;
                return guardianregid;
            }
        }

        public int EditAddGuardiandetails(string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email, long? income, string gurelation, string gender)
        {
            GuardianRegister guardian = new GuardianRegister()
            {
                GuardianFirstname = firstname,
                GuardianLastName = lastname,
                GuardianDOB = dob,
                GuardianQualification = qual,
                GuardianOccupation = occu,
                GuardianMobileNo = mobile,
                GuardianEmail = email,
                GuardianIncome = income,
                GuRelationshipToChild = gurelation,
                GuardianGender = gender
            };
            dc.GuardianRegisters.Add(guardian);
            dc.SaveChanges();
            return guardian.GuardianRegisterId;
        }

        public int EditGuardianDetails(int? guardianid, string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email, long? income, string gurelation, string gender) //, 
        {
            var get = dc.GuardianRegisters.Where(q => q.GuardianRegisterId == guardianid).FirstOrDefault();

            if (get != null)
            {
                get.GuardianFirstname = firstname;
                get.GuardianLastName = lastname;
                get.GuardianDOB = dob;
                get.GuardianEmail = email;
                get.GuardianQualification = qual;
                get.GuardianOccupation = occu;
                get.GuardianMobileNo = mobile;
                get.GuardianIncome = income;
                get.GuRelationshipToChild = gurelation;
                get.GuardianGender = gender;
                dc.SaveChanges();
            }
            return get.GuardianRegisterId;
        }
        public int UpdateGuardianDetails(int? guardianid, string qual, string occu, long? mobile, long? income, string gurelation, string gender) //, 
        {
            var get = dc.GuardianRegisters.Where(q => q.GuardianRegisterId == guardianid).FirstOrDefault();

            if (get != null)
            {
                get.GuardianQualification = qual;
                get.GuardianOccupation = occu;
                get.GuardianMobileNo = mobile;
                get.GuardianIncome = income;
                get.GuRelationshipToChild = gurelation;
                get.GuardianGender = gender;
                dc.SaveChanges();
            }
            return get.GuardianRegisterId;
        }

        public int CheckGuardianExit(int primaryuserid, int Newstudentid, string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email, long? guincome, string gurelation, string gender)
        {
            int Guardianid = 0;
            var GetMother = (from a in dc.AdmissionTransactions
                             from b in dc.GuardianRegisters
                             where a.PrimaryUserRegisterId == primaryuserid && b.GuardianRegisterId == a.GuardianRegisterId && b.GuardianFirstname == firstname && b.GuardianLastName == lastname && b.GuardianDOB == dob
                             select b).FirstOrDefault();
            if (GetMother != null)
            {
                Guardianid = GetMother.GuardianRegisterId;
                UpdateGuardianDetails(Guardianid, qual, occu, mobile, guincome, gurelation, gender);
            }
            else
            {
                Guardianid = EditAddGuardiandetails(firstname, lastname, dob, qual, occu, mobile, email, guincome, gurelation, gender);
            }
            return Guardianid;
        }

        public int CheckEditGuardiandetailsExit(int primaryuserid, int Newstudentid, int gid, string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email, long? guincome, string gurelation, string gender)
        {
            int Guardianid = 0;
            var GetMother = (from a in dc.AdmissionTransactions
                             from b in dc.GuardianRegisters
                             where a.PrimaryUserRegisterId == primaryuserid && b.GuardianRegisterId == a.GuardianRegisterId && b.GuardianFirstname == firstname && b.GuardianLastName == lastname && b.GuardianDOB == dob
                             select b).FirstOrDefault();
            if (GetMother != null)
            {
                Guardianid = GetMother.GuardianRegisterId;
                UpdateGuardianDetails(Guardianid, qual, occu, mobile, guincome, gurelation, gender);
                ChangeGuardianStatusFlag(gid, Newstudentid);
            }
            else
            {
                Guardianid = EditGuardianDetails(gid, firstname, lastname, dob, qual, occu, mobile, email,guincome,gurelation,gender);
            }
            return Guardianid;
        }

        public void ChangeGuardianStatusFlag(int Guardianid, int studentid)
        {
            var get = dc.GuardianRegisters.Where(q => q.GuardianRegisterId == Guardianid).FirstOrDefault();
            if (get != null)
            {
                get.GuardianStatusFlag = "False";
                get.StudentRegisterId = studentid;
                dc.SaveChanges();
            }
        }

        public bool CheckGuardianSameinRemaingstudent(int primaryuserid, int studentid, List<AdmissionTransaction> old, string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email, long? guincome, string gurelation, string gender)
        {
            int flag = 0;
            int count = Convert.ToInt16(old.Count);
            foreach (var v in old)
            {
                var ans = (from a in dc.AdmissionTransactions
                           from b in dc.GuardianRegisters
                           where a.PrimaryUserRegisterId == v.PrimaryUserRegisterId && a.StudentRegisterId == v.StudentRegisterId && b.GuardianRegisterId == a.GuardianRegisterId && b.GuardianFirstname == firstname && b.GuardianLastName == lastname && b.GuardianDOB == dob
                           select b).FirstOrDefault();
                if (ans != null)
                {
                    flag++;
                }
            }
            if (flag == count)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}