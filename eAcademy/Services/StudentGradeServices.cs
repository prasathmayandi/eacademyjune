﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class StudentGradeServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        DateTime date = DateTimeByZone.getCurrentDate();
        public List<ProgressCard> getGrade(int yearId, int classId, int secId, int examId, int StudentRegId)
        {
            var list = (from a in db.StudentGrades
                        from b in db.Subjects
                        where a.AcademicYearId == yearId &&
                              a.ClassId == classId &&
                              a.SectionId == secId &&
                              a.ExamId == examId &&
                              a.StudentRegisterId == StudentRegId &&
                              a.SubjectId == b.SubjectId
                        select new ProgressCard
                        {
                            Subject = b.SubjectName,
                            Grade = a.Grade
                        }).ToList();
            return list;
        }

        public StudentGrade CheckDataAlreadyExist(int yearId, int classId, int secId, int subId, int examId, int StudentRegisterId)
        {
            var CheckDataAlreadyExist = (from a in db.StudentGrades
                                         where a.AcademicYearId == yearId &&
                                         a.ClassId == classId &&
                                         a.SectionId == secId &&
                                         a.SubjectId == subId &&
                                         a.ExamId == examId &&
                                         a.StudentRegisterId == StudentRegisterId
                                         select a).FirstOrDefault();
            return CheckDataAlreadyExist;
        }

        public void addStudentGrade(int StudentRegisterId,int yearId,int classId,int secId,int examId,int subId,int mark,string Grade,int FacultyId, string comment)
        {
            StudentGrade add = new StudentGrade()
            {
                StudentRegisterId = StudentRegisterId,
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                ExamId = examId,
                SubjectId = subId,
                Mark = mark,
                Grade = Grade,
                EmployeeRegisterId = FacultyId,
                Comment = comment,
                CommentDate = date
            };
            db.StudentGrades.Add(add);
            db.SaveChanges();
        }

        public StudentGrade checkGradeExist(int yearId, int classId, int secId, int subId, int examId)
        {
            var getRow = (from a in db.StudentGrades where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.SubjectId == subId && a.ExamId == examId select a).FirstOrDefault();
            return getRow;
        }

        public IEnumerable<object> getExistGrade(int yearId, int classId, int secId, int subId, int examId)
        {
            var list = (from a in db.StudentGrades
                        from b in db.StudentRollNumbers
                        from c in db.Students
                        where a.StudentRegisterId == b.StudentRegisterId &&
                             a.StudentRegisterId == c.StudentRegisterId &&
                            a.AcademicYearId == yearId &&
                            a.ClassId == classId &&
                            a.SectionId == secId &&
                            a.SubjectId == subId &&
                            a.ExamId == examId
                        select new
                        {
                            gradeId = a.StudentGradeId,
                            rollnumber = b.RollNumber,
                            id = a.StudentRegisterId,
                            name = c.FirstName+" "+c.LastName,
                            mark = a.Mark,
                            comment = a.Comment
                        }).ToList().Select(c => new
                        {
                            gradeId = QSCrypt.Encrypt(c.gradeId.ToString()),
                            rollnumber = c.rollnumber,
                            id = QSCrypt.Encrypt(c.id.ToString()),
                            name = c.name,
                            mark = c.mark,
                            comment = c.comment
                        }).ToList();
            return list;
        }

        public void updateStudentGrade(int yearId, int classId, int secId, int examId, int subId, int StudentRegisterId, int Mark, string grade, int EmployeeRegisterId, string comment)
        {
            var getrow = (from a in db.StudentGrades where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.SubjectId == subId && a.ExamId == examId && a.StudentRegisterId == StudentRegisterId && a.EmployeeRegisterId == EmployeeRegisterId select a).FirstOrDefault();
            getrow.Mark = Mark;
            getrow.Grade = grade;
            getrow.Comment = comment;
            getrow.CommentUpdatedDate = date;
            db.SaveChanges();
        }

        public List<ProgressCard> getGradeList(int yearId, int classId, int secId, int examId)
        {
            var getGradeList = (from a in db.StudentGrades
                               from b in db.Subjects
                               where a.AcademicYearId == yearId &&
                               a.ClassId == classId &&
                               a.SectionId == secId &&
                               a.ExamId == examId &&
                               a.SubjectId == b.SubjectId
                               select new ProgressCard
                               {
                                   Subject_Id = a.SubjectId,
                                   Subject = b.SubjectName
                               }).Distinct().ToList();
            return getGradeList;
        }

        public List<ProgressCard> getStudentGradeList(int yearId, int classId, int secId, int examId)
        {
            var getStudentGrade = (from a in db.StudentGrades
                                   from b in db.Subjects
                                   from c in db.StudentRollNumbers
                                   from d in db.Students
                                   where a.AcademicYearId == yearId &&
                                   a.ClassId == classId &&
                                   a.SectionId == secId &&
                                   a.ExamId == examId &&
                                   a.SubjectId == b.SubjectId &&
                                   a.StudentRegisterId == c.StudentRegisterId &&
                                   a.StudentRegisterId == d.StudentRegisterId &&
                                   b.Status == true
                                   select new ProgressCard
                                   {
                                       StudentRegId = a.StudentRegisterId,
                                       RollNumber = c.RollNumber,
                                       StudentName = d.FirstName+" "+d.LastName,
                                       Subject_Id = a.SubjectId,
                                       Subject = b.SubjectName,
                                       Grade = a.Grade
                                   }).Distinct().ToList();
            return getStudentGrade;
        }

        //jegadeesh

        public List<getclassandsection> StudentGradeProgress(int yid, int cid, int sid, int stu_id)
        {
            var ans = (from a in db.StudentGrades

                       from d in db.Exams
                       from e in db.Students
                       from f in db.Subjects
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.StudentRegisterId == stu_id && e.StudentRegisterId == stu_id && a.ExamId == d.ExamId &&
                       a.SubjectId == f.SubjectId
                       select new getclassandsection
                       {
                           Grade = a.Grade,
                           ExamType = d.ExamName,
                           subjectName = f.SubjectName
                       }
                          ).ToList();
            return ans;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}