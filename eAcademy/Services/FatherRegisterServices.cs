﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class FatherRegisterServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
      
        public int FatherdetailsCopyfromPreAdmissiontoFather(PreAdmissionFatherRegister old,int primaryuserid,int studentregid)
        {
            var checkfatherexist = (from a in dc.AdmissionTransactions
                                         from b in dc.FatherRegisters
                                         where a.PrimaryUserRegisterId == primaryuserid && a.StudentRegisterId != studentregid && b.FatherRegisterId == a.FatherRegisterId && b.FatherFirstName == old.FatherFirstName &&
                                            b.FatherLastName == old.FatherLastName //&& b.FatherDOB == old.FatherDOB
                                         select b).FirstOrDefault();
            if (checkfatherexist != null)
            {
                return checkfatherexist.FatherRegisterId;
            }
            else
            {
                FatherRegister user = new FatherRegister()
                {
                  FatherFirstName=old.FatherFirstName,
                  FatherLastName=old.FatherLastName,
                  FatherDOB=old.FatherDOB,
                  FatherQualification=old.FatherQualification,
                  FatherOccupation=old.FatherOccupation,
                  FatherMobileNo=old.FatherMobileNo,
                  FatherEmail=old.FatherEmail
                  
                };
                dc.FatherRegisters.Add(user);
                dc.SaveChanges();
                int fatherregid = user.FatherRegisterId;
               
                return fatherregid;
            }
        }

        public int EditFatherDetails(int fatherid, string FatherFirstName,string FatherLastName,DateTime?  FatherDOB,string FatherQualification,string FatherOccupation,long? FatherMobileNo,string FatherEmail)
        {
            var getrow = dc.FatherRegisters.Where(q => q.FatherRegisterId == fatherid).FirstOrDefault();

            if(getrow !=null)
            {
                getrow.FatherFirstName = FatherFirstName;
                getrow.FatherLastName = FatherLastName;
                getrow.FatherDOB = FatherDOB;
                getrow.FatherQualification = FatherQualification;
                getrow.FatherOccupation = FatherOccupation;
                getrow.FatherMobileNo = FatherMobileNo;
                getrow.FatherEmail = FatherEmail;
                dc.SaveChanges();
              
            }
            return getrow.FatherRegisterId;
        }

        public int EditAddFatherdetails(string fafirstname, string falastname, DateTime? fadob, string faqual, string faoccu, long? famobile, string faemail)
        {

            FatherRegister father = new FatherRegister()
            {
                FatherFirstName = fafirstname,
                FatherLastName = falastname,
                FatherDOB = fadob,
                FatherQualification = faqual,
                FatherOccupation = faoccu,
                FatherMobileNo = famobile,
                FatherEmail = faemail
            };
            dc.FatherRegisters.Add(father);
            dc.SaveChanges();
            
            return father.FatherRegisterId;
        }

        public int CheckFatherExit(int primaryuserid, int Newstudentid, string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email)
        {

            int Fatherid = 0;

            var GetMother = (from a in dc.AdmissionTransactions
                             from b in dc.FatherRegisters
                             where a.PrimaryUserRegisterId == primaryuserid && b.FatherRegisterId == a.FatherRegisterId && b.FatherFirstName == firstname && b.FatherLastName == lastname && b.FatherDOB == dob
                             select b).FirstOrDefault();
            if (GetMother != null)
            {
                Fatherid = GetMother.FatherRegisterId;

                UpdatefatherDetails(Fatherid, qual, occu, mobile);
            }
            else
            {
                Fatherid = EditAddFatherdetails(firstname, lastname, dob, qual, occu, mobile, email);
            }
            return Fatherid;

        }

        public int UpdatefatherDetails(int? fatherid, string faqual, string faoccu, long? famobile) //, 
        {
            var get = dc.FatherRegisters.Where(q => q.FatherRegisterId == fatherid).FirstOrDefault();

            if (get != null)
            {
                get.FatherQualification = faqual;
                get.FatherOccupation = faoccu;
                get.FatherMobileNo = famobile;
                dc.SaveChanges();
                
            }
            return get.FatherRegisterId;

        }

        public int CheckEditFatherdetailsExit(int primaryuserid, int Newstudentid, int fid, string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email)
        {

            int Fatherid = 0;

            var GetMother = (from a in dc.AdmissionTransactions
                             from b in dc.FatherRegisters
                             where a.PrimaryUserRegisterId == primaryuserid && b.FatherRegisterId == a.FatherRegisterId && b.FatherFirstName == firstname && b.FatherLastName == lastname && b.FatherDOB == dob
                             select b).FirstOrDefault();
            if (GetMother != null)
            {
                Fatherid = GetMother.FatherRegisterId;

                UpdatefatherDetails(Fatherid, qual, occu, mobile);

                ChangeFatherStatusFlag(fid, Newstudentid);
            }
            else
            {
                Fatherid = EditFatherDetails(fid, firstname, lastname, dob, qual, occu, mobile, email);
            }
            return Fatherid;

        }

        public void ChangeFatherStatusFlag(int fatherid, int studentid)
        {
            var get = dc.FatherRegisters.Where(q => q.FatherRegisterId == fatherid).FirstOrDefault();
            if (get != null)
            {
                get.FatherStatusFlag = "False";
                get.StudentRegisterId = studentid;
                dc.SaveChanges();
              
            }
        }

        public bool CheckFatherSameinRemaingstudent(int primaryuserid,int studentid,List<AdmissionTransaction> old,string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email)
        {
            int flag = 0;
            int count = Convert.ToInt16(old.Count);
            foreach(var v in old)
            {
                var ans = (from a in dc.AdmissionTransactions
                           from b in dc.FatherRegisters
                           where a.PrimaryUserRegisterId == v.PrimaryUserRegisterId && a.StudentRegisterId == v.StudentRegisterId && b.FatherRegisterId == a.FatherRegisterId && b.FatherFirstName == firstname && b.FatherLastName == lastname && b.FatherDOB == dob
                           select b).FirstOrDefault();
                if(ans !=null)
                {
                    flag++;
                }
            }
            if(flag == count)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}