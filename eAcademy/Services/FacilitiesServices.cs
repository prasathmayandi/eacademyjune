﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class FacilitiesServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
       
        public FacilityList getFacilitesByFaculityId(int fid)
        {
            var ans = (from t1 in dc.Facilities
                       where t1.FacilityId == fid
                       select new FacilityList { Facility = t1.FacilityName, Desc = t1.FacilityDescription, Status = t1.Status }).FirstOrDefault();
            return ans;
        }

        public List<FacilityList> getFacility()
        {
            var ans = (from t1 in dc.Facilities
                       select new { fid = t1.FacilityId, fac = t1.FacilityName, desc = t1.FacilityDescription, status = t1.Status }).ToList()
                         .Select(q => new FacilityList { FacilityId = QSCrypt.Encrypt(q.fid.ToString()), Facility = q.fac, Desc = q.desc, Status = q.status, Description=q.desc }).ToList();
            return ans;
        }

        public IEnumerable<object> getFacilities()
        {
            var ans = dc.Facilities.Select(s => new { fid = s.FacilityId, ftype = s.FacilityName }).ToList();
            return ans;
        }

        public void addFacility(string facility, string fac_desc)
        {
            Facility fac = new Facility()
            {
                FacilityName = facility,
                FacilityDescription = fac_desc,
                Status=true
            };

            dc.Facilities.Add(fac);
            dc.SaveChanges();
            
        }

        public bool checkFacility(string fac, string fac_desc)
        {
            var ans = dc.Facilities.Where(q => q.FacilityName.Equals(fac)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkFacility(int fid, string fac, string fac_desc)
        {
            var ans = (from t1 in dc.Facilities
                       where t1.FacilityId != fid && t1.FacilityName == fac
                       select t1).ToList();

            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkFacility(int fid, string fac, string fac_desc, string status)
        {
            int count;
            if (status == "Active")
            {
                var ans = (from t1 in dc.Facilities
                           where t1.FacilityId != fid && t1.FacilityName == fac && t1.Status==true
                           select t1).ToList();
                count = ans.Count;
            }

            else
            {
                var ans = (from t1 in dc.Facilities
                           where t1.FacilityId != fid && t1.FacilityName == fac && t1.Status == false
                           select t1).ToList();

                count = ans.Count;
            }

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void updateFacility(int fid, string fac, string fac_desc, string status)
        {
            var update = dc.Facilities.Where(q => q.FacilityId.Equals(fid)).First();
            update.FacilityName = fac;
            update.FacilityDescription = fac_desc;
            if (status == "Active")
            {
                update.Status = true;
            }
            else
            {
                update.Status = false;
            }
            dc.SaveChanges();
           
        }

        public void deleteFacility(int FacilityId)
        {
            var delete = dc.Facilities.Where(q => q.FacilityId.Equals(FacilityId)).First();
            dc.Facilities.Remove(delete);
            dc.SaveChanges();
            
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}