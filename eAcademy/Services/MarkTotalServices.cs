﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class MarkTotalServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public MarkTotal getMarkTotal(int yearId, int classId, int secId, int examId, int StudentRegId)
        {
            var MarkTotal = (from a in db.MarkTotals
                             where a.AcademicYearId == yearId &&
                             a.ClassId == classId &&
                             a.SectionId == secId &&
                             a.ExamId == examId &&
                             a.StudentRegisterId == StudentRegId
                             select a).FirstOrDefault();
            return MarkTotal;
        }

        public void addMarkTotal(int AcademicYearId,int ClassId,int SectionId,int ExamId,int? StudentRegisterId,int? TotalMark,string result)
        {
            MarkTotal add = new MarkTotal()
            {
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                ExamId = ExamId,
                StudentRegisterId = StudentRegisterId,
                TotalMark = TotalMark,
                Result = result
            };
            db.MarkTotals.Add(add);
            db.SaveChanges();
        }

        public string checkCommentsExist(int yearId, int classId, int secId, int examId)
        {
            var MarkTotal = (from a in db.MarkTotals
                             where a.AcademicYearId == yearId &&
                             a.ClassId == classId &&
                             a.SectionId == secId &&
                             a.ExamId == examId
                             select a).FirstOrDefault().Comment;
            return MarkTotal;
        }

        public MarkTotal MarkTotalExist(int yearId, int classId, int secId, int examId)
        {
            var MarkTotal = (from a in db.MarkTotals
                             where a.AcademicYearId == yearId &&
                             a.ClassId == classId &&
                             a.SectionId == secId &&
                             a.ExamId == examId
                             select a).FirstOrDefault();
            return MarkTotal;
        }

        public MarkTotal ExistRankList(int yearId, int classId, int secId, int examId)
        {
            var StudentRank = (from a in db.MarkTotals
                               where a.AcademicYearId == yearId &&
                               a.ClassId == classId &&
                               a.SectionId == secId &&
                               a.ExamId == examId
                               select a).FirstOrDefault();

            return StudentRank;
        }

        public List<GetRankList> getRankList(int yearId, int classId, int secId, int examId)
        {
            var RankList = from o in db.MarkTotals
                           where o.Result == "Pass" &&
                           o.AcademicYearId == yearId &&
                           o.ClassId == classId &&
                           o.SectionId == secId &&
                           o.ExamId == examId
                           select o;

            var RankListOrder = from o in db.MarkTotals
                                where o.Result == "Pass" &&
                                o.AcademicYearId == yearId &&
                           o.ClassId == classId &&
                           o.SectionId == secId &&
                           o.ExamId == examId
                                orderby o.TotalMark descending
                                select new GetRankList
                                {
                                    MarkTotalId = (from n in db.MarkTotals
                                                   where n.TotalMark == o.TotalMark & n.MarkTotalId == o.MarkTotalId & n.Result == "Pass"
                                                   select n.MarkTotalId).FirstOrDefault(),
                                    TotalMark = o.TotalMark,
                                    Rank = RankList.Count(s2 => s2.TotalMark > o.TotalMark) + 1
                                };

            var RankListOrder1 = RankListOrder.ToList();
            return RankListOrder1;
        }

        public void UpdateFailList(int yearId, int classId, int secId, int examId)
        {
            var RankList = (from o in db.MarkTotals
                           where o.Result == "Fail" &&
                           o.AcademicYearId == yearId &&
                           o.ClassId == classId &&
                           o.SectionId == secId &&
                           o.ExamId == examId
                           select o).ToList();
            RankList.ForEach(a => a.StudentRank = 0);
            db.SaveChanges();
        }

        public void updateRank(int id, int rank)
        {
            var updateRank = (from a in db.MarkTotals where a.MarkTotalId == id select a).FirstOrDefault();
            if (updateRank != null)
            {
                updateRank.StudentRank = rank;
                db.SaveChanges();
            }          
        }

        public List<RankList> getStudentRankList(int yearId, int classId, int secId, int examId)
        {
            var StudentRanklist = (from a in db.MarkTotals
                                   from b in db.Students
                                   where a.AcademicYearId == yearId &&
                                   a.ClassId == classId &&
                                   a.SectionId == secId &&
                                   a.ExamId == examId &&
                                   a.StudentRegisterId == b.StudentRegisterId
                                   select new 
                                   {
                                       StudentRegisterId = a.StudentRegisterId,
                                       StudentName = b.FirstName + " " + b.LastName,
                                       TotalMark = a.TotalMark,
                                       Result = a.Result,
                                       StudentRank = a.StudentRank,
                                       Comment = a.Comment,
                                       MarkTotalId = a.MarkTotalId
                                   }).ToList().Select(a => new RankList
                        {
                            StudentRegisterId = a.StudentRegisterId,
                            StudentName = a.StudentName,
                            TotalMark = a.TotalMark,
                            Result = a.Result,
                            StudentRank = a.StudentRank,
                            Comment = a.Comment,
                            MarkTotalId = QSCrypt.Encrypt(a.MarkTotalId.ToString()),
                        }).ToList();
            return StudentRanklist;
        }

        public void updateMarkTotal(int id, string comment)
        {
            var update = (from a in db.MarkTotals where a.MarkTotalId == id select a).FirstOrDefault();
            if (update != null)
            {
                update.Comment = comment;
                db.SaveChanges();
            }
        }

        public RankList getTotalMark(int acid, int cid, int sec_id, int examId, int std_id)
        {
            var MarkTotal = (from a in db.MarkTotals
                             where a.AcademicYearId == acid &&
                             a.ClassId == cid &&
                             a.SectionId == sec_id &&
                             a.ExamId == examId &&
                             a.StudentRegisterId == std_id
                             select new RankList { Comment=a.Comment, Result=a.Result, TotalMark=a.TotalMark,StudentRank=a.StudentRank  }).FirstOrDefault();
            return MarkTotal;
        }


        public MarkTotal checkTotalCalculated(int yearId, int classId, int secId, int examId)
        {
            var getRow = (from a in db.MarkTotals where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.ExamId == examId select a).FirstOrDefault();
            return getRow;
        }

        public MarkTotal checkTotalCalculated(int yearId, int classId, int examId)
        {
            var getRow = (from a in db.MarkTotals where a.AcademicYearId == yearId && a.ClassId == classId && a.ExamId == examId select a).FirstOrDefault();
            return getRow;
        }

        public List<getclassandsection> ConductedExam(int yearId, int classId, int secId, int StudentRegId)
        {
            var list = (from a in db.MarkTotals 
                        from b in db.Exams 
                            where a.AcademicYearId == yearId &&
                            a.ClassId == classId &&
                            a.SectionId == secId &&
                            a.StudentRegisterId == StudentRegId &&
                            a.ExamId == b.ExamId
                        select new getclassandsection
                        {
                              ExamId = b.ExamId,
                              ExamName = b.ExamName
                          }).OrderBy(x=>x.ExamId).Distinct().ToList();
            return list;
        }

        //jegadeesh

        public List<Ranklist> GetStudentRankList(int yid, int cid, int sid, int eid)
        {
            var ans = (from a in db.MarkTotals
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.StudentRegisterId == b.StudentRegisterId &&
                       a.ExamId == eid && a.StudentRegisterId == c.StudentRegisterId && 
                       c.Status == true && d.StudentRegisterId == b.StudentRegisterId && 
                       d.HaveRollNumber == true &&
                       c.AcademicYearId ==d.AcademicYearId &&
                       d.AcademicYearId == yid &&
                       c.ClassId==d.ClassId &&
                       c.SectionId==d.SectionId
                       select new Ranklist
                       {
                           StudentId = b.StudentId,
                           rollnumber = c.RollNumber,
                           StudentName = b.FirstName + " " + b.LastName,
                           total = a.TotalMark,
                           rank = a.StudentRank,
                           Result = a.Result
                       }).OrderByDescending(q => q.total).ToList().Select(p => new Ranklist {
                       
                       StudentId=p.StudentId,
                       rollnumber=p.rollnumber,
                       StudentName=p.StudentName,
                       total=p.total,
                       Result=p.Result,
                       Rank=GetRankList(p.rank)
                       }).ToList();
            return ans;
        }
        public string GetRankList(int? rank)
        {
            if(rank!=null)
            {
                return rank.ToString();
            }
            else
            {
                return "-";
            }
        }
        public List<Ranklist> GetAllStudentMarkList(int yid, int cid, int sid, int eid)
        {
            var ans = (from a in db.MarkTotals
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.StudentRegisterId == b.StudentRegisterId && a.ExamId == eid 
                       && a.StudentRegisterId == c.StudentRegisterId &&
                       d.StudentRegisterId == b.StudentRegisterId && 
                       d.Status == true && d.HaveRollNumber == true &&
                       c.AcademicYearId ==d.AcademicYearId &&
                       c.ClassId==d.ClassId &&
                       c.SectionId==d.SectionId
                       select new Ranklist
                       {
                           sturegid = b.StudentRegisterId,
                           rollnumber = c.RollNumber,
                           stuname = b.FirstName + " " + b.LastName,
                           total = a.TotalMark,
                           rank = a.StudentRank
                       }).OrderBy(q => q.stuname).ToList();
            return ans;
        }
        public List<R_ExamResult> GetPassStudentList(int? yid, int? cid, int sid, int eid)
        {
            var ans = (from a in db.MarkTotals
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid &&
                           a.ClassId == cid &&
                           a.SectionId == sid &&
                           a.ExamId == eid &&
                           a.Result == "Pass" &&
                           a.StudentRegisterId == b.StudentRegisterId &&
                           a.StudentRegisterId == c.StudentRegisterId && c.Status == true && d.StudentRegisterId == b.StudentRegisterId && d.HaveRollNumber == true &&
                            d.AcademicYearId == yid &&
                            c.AcademicYearId == d.AcademicYearId &&
                       c.ClassId == d.ClassId &&
                       c.SectionId == d.SectionId
                       select new R_ExamResult
                       {
                           StudentId = b.StudentId,
                           RollNumber = c.RollNumber,
                           StudentName = b.FirstName + " " + b.LastName,
                           Total = a.TotalMark,
                           Result = a.Result,
                           Ranks = a.StudentRank,
                           Comment = a.Comment
                       }).Distinct().OrderBy(q => q.RollNumber).ToList().Select(p => new R_ExamResult {
                       StudentId=p.StudentId,
                       RollNumber=p.RollNumber,
                       StudentName=p.StudentName,
                       Total=p.Total,
                       Result=p.Result,
                       Rank=GetRankList(p.Ranks),
                       Comment=p.Comment

                       }).ToList();
            return ans;
        }
        public List<R_ExamResult> GetFailStudentList(int? yid, int? cid, int sid, int eid)
        {
            var ans = (from a in db.MarkTotals
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid &&
                           a.ClassId == cid &&
                           a.SectionId == sid &&
                           a.ExamId == eid &&
                           a.Result == "fail" &&
                           a.StudentRegisterId == b.StudentRegisterId &&
                           a.StudentRegisterId == c.StudentRegisterId && c.Status == true && d.StudentRegisterId == b.StudentRegisterId   && d.HaveRollNumber == true &&
                            d.AcademicYearId == yid &&
                            c.AcademicYearId == d.AcademicYearId &&
                       c.ClassId == d.ClassId &&
                       c.SectionId == d.SectionId
                       select new R_ExamResult
                       {
                           StudentId = b.StudentId,
                           RollNumber = c.RollNumber,
                           StudentName = b.FirstName + " " + b.LastName,
                           Total = a.TotalMark,
                           Result = a.Result,
                           Ranks = a.StudentRank,
                           Comment = a.Comment
                       }).Distinct().OrderBy(q => q.RollNumber).ToList().Select(p => new R_ExamResult
                       {
                           StudentId = p.StudentId,
                           RollNumber = p.RollNumber,
                           StudentName = p.StudentName,
                           Total = p.Total,
                           Result = p.Result,
                           Rank = GetRankList(p.Ranks),
                           Comment = p.Comment

                       }).ToList();
            return ans;
        }
        public List<R_ExamResult> GetBothStudentList(int? yid, int? cid, int sid, int eid)
        {

            var ans = (from a in db.MarkTotals
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == yid &&
                           a.ClassId == cid &&
                           a.SectionId == sid &&
                           a.ExamId == eid &&
                           a.StudentRegisterId == b.StudentRegisterId &&
                           a.StudentRegisterId == c.StudentRegisterId && c.Status == true && d.StudentRegisterId == b.StudentRegisterId &&  d.HaveRollNumber == true &&
                            c.AcademicYearId == d.AcademicYearId &&
                            d.AcademicYearId == yid &&
                       c.ClassId == d.ClassId &&
                       c.SectionId == d.SectionId
                       select new R_ExamResult
                       {
                           StudentId = b.StudentId,
                           RollNumber = c.RollNumber,
                           StudentName = b.FirstName + " " + b.LastName,
                           Total = a.TotalMark,
                           Result = a.Result,
                           Ranks = a.StudentRank,
                           Comment = a.Comment
                       }).Distinct().OrderBy(q => q.RollNumber).ToList().Select(p => new R_ExamResult
                       {
                           StudentId = p.StudentId,
                           RollNumber = p.RollNumber,
                           StudentName = p.StudentName,
                           Total = p.Total,
                           Result = p.Result,
                           Rank = GetRankList(p.Ranks),
                           Comment = p.Comment

                       }).ToList();
            return ans;
        }
        public List<totalmark> GetParticularStudentTotalMark(int yid, int cid, int sid, int stu_id)
        {
            var ans = (from a in db.MarkTotals
                       from b in db.Exams
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.StudentRegisterId == stu_id && b.ExamId==a.ExamId
                       select new totalmark { 
                           ExamType=b.ExamName,
                          Result=a.Result,
                           Totalmark = a.TotalMark,
                       Rank=a.StudentRank}).ToList();
            return ans;
        }

        public List<M_ClassMarkList>GetTotalMarkList(int year,int cid,int sec,int examid)
        {

            var res = (from a in db.MarkTotals
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.AssignClassToStudents
                       where a.AcademicYearId == year && a.ClassId == cid && a.SectionId == sec && a.ExamId == examid && b.StudentRegisterId == a.StudentRegisterId 
                       && c.StudentRegisterId == a.StudentRegisterId && c.Status == true && d.StudentRegisterId == b.StudentRegisterId && d.Status == true && d.HaveRollNumber == true &&
                        c.AcademicYearId ==d.AcademicYearId &&
                       c.ClassId==d.ClassId &&
                       c.SectionId==d.SectionId
                       select new M_ClassMarkList
                       {
                           StudentRegid = a.StudentRegisterId,
                           StudentName = b.FirstName + " " + b.LastName,
                           RollNumber1 = c.RollNumber,
                           StudentId = b.StudentId,
                           TotalMark = a.TotalMark,
                           Ranks = a.StudentRank
                       }).OrderBy(q => q.RollNumber1).ToList().Select(p => new M_ClassMarkList {
                       StudentRegid=p.StudentRegid,
                       StudentName=p.StudentName,
                       RollNumber1=p.RollNumber1,
                       StudentId=p.StudentId,
                       TotalMark=p.TotalMark,
                       Rank=GetRankList(p.Ranks)                       
                       }).ToList();
            return res;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}