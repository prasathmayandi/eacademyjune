﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class AssignSubjectToPeriodServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public List<StaffTimetableList> getPeriodList(int yearId, int classId, int secId, int passDayId)
        {
            var PeriodList = (from a in db.AssignSubjectToPeriods
                              from b in db.Subjects
                              where a.AcademicYearId == yearId &&
                              a.ClassId == classId &&
                              a.SectionId == secId &&
                              a.Day == passDayId &&
                              a.SubjectId == b.SubjectId
                              select new StaffTimetableList
                              {
                                  Period = a.Period,
                                  Subject = b.SubjectName
                              }).ToList();
            return PeriodList;
        }

        public List<StaffTimetableList> StudentTimetable(int yearId, int classId, int secId)
        {
            var PeriodList = (from a in db.AssignSubjectToPeriods
                              from b in db.Subjects
                              where a.AcademicYearId == yearId &&
                              a.ClassId == classId &&
                              a.SectionId == secId &&
                              a.SubjectId == b.SubjectId
                              select new StaffTimetableList
                              {
                                  Period = a.Period,
                                  Day = a.Day,
                                  Subject = b.SubjectName
                              }).ToList();
            return PeriodList;
        }

        public void updateSubjectToPeriod(int AcademicYearId, int ClassId, int SectionId, int Day, int Period, int SubjectId)
        {
            var getRow = (from a in db.AssignSubjectToPeriods where a.AcademicYearId == AcademicYearId && a.ClassId == ClassId && a.SectionId == SectionId && a.Day == Day && a.Period == Period select a).FirstOrDefault();
            if (getRow != null)
            {
                getRow.SubjectId = SubjectId;
                db.SaveChanges();
            }
            else
            {
                AssignSubjectToPeriod sp = new AssignSubjectToPeriod()
                {
                    AcademicYearId = AcademicYearId,
                    ClassId = ClassId,
                    SectionId = SectionId,
                    Day = Day,
                    Period = Period,
                    SubjectId = SubjectId
                };
                db.AssignSubjectToPeriods.Add(sp);
                db.SaveChanges();
            }

        }
        public AssignSubjectToPeriod checkDataAlreadyExist(int yearId, int classId, int secId, int dayId)
        {
            var checkDataAlreadyExist = (from a in db.AssignSubjectToPeriods where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.Day == dayId select a).FirstOrDefault();
            return checkDataAlreadyExist;
        }

        public IEnumerable<object> getExistPeriodList(int yearId, int classId, int secId)
        {
            var List = (from a in db.AssignSubjectToPeriods
                        from b in db.Subjects
                        where a.AcademicYearId == yearId &&
                           a.ClassId == classId &&
                           a.SectionId == secId &&
                           a.SubjectId == b.SubjectId
                        select new
                        {
                            day = a.Day,
                            period = a.Period,
                            subject = b.SubjectName
                        }).ToList();
            return List;
        }

        public void addSubjectToPeriod(int AcademicYearId,int ClassId,int SectionId,int Day,int Period,int SubjectId)
        {
            AssignSubjectToPeriod sp = new AssignSubjectToPeriod()
            {
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                Day = Day,
                Period = Period,
                SubjectId = SubjectId
            };
            db.AssignSubjectToPeriods.Add(sp);
            db.SaveChanges();
        }

        public AssignSubjectToPeriod getRecord(int id)
        {
            var getSubId = (from a in db.AssignSubjectToPeriods where a.AssignSubjectToPeriodId == id select a).FirstOrDefault();
            return getSubId;
        }

        public List<ClassTimetableList> getClassTimtableList(int yearId, int classId, int secId, int dayId)
        {
            var PeriodList = (from a in db.AssignSubjectToPeriods
                              from b in db.Subjects
                              where a.AcademicYearId == yearId &&
                                    a.ClassId == classId &&
                                    a.SectionId == secId &&
                                    a.Day == dayId &&
                                    a.SubjectId == b.SubjectId
                              select new ClassTimetableList
                              {
                                  SubjectId = a.SubjectId,
                                  Period1 = a.Period,
                                  SubjectName = b.SubjectName,
                                  id = a.AssignSubjectToPeriodId
                              }).ToList();
            return PeriodList;
        }
        public void updateSubject(int id, int subId)
        {
            var update = (from a in db.AssignSubjectToPeriods where a.AssignSubjectToPeriodId == id select a).First();
            update.SubjectId = subId;
            db.SaveChanges();
        }

        public AssignSubjectToPeriod checkDataAlreadyExist(int yearId, int classId, int secId)
        {
            var getRow = (from a in db.AssignSubjectToPeriods where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId select a).FirstOrDefault();
            return getRow;
        }

        public AssignSubjectToPeriod checkFacultyFreeInPeriod(int yearId, int classId, int secId, int subId, int passDayId, int passPeriodId)
        {
            var checkFacultyFreeInPeriod = (from a in db.AssignSubjectToPeriods where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.SubjectId == subId && a.Day == passDayId && a.Period == passPeriodId select a).FirstOrDefault();
            return checkFacultyFreeInPeriod;
        }

        public IEnumerable<FacultyPeriodSubject> getFacultyPeriodSubject(int yearId, int facultyId)
        {
            var FacultyPeriodSubjectList = (from a in db.AssignSubjectToPeriods
                                            from b in db.AssignFacultyToSubjects
                                            from c in db.Subjects
                                            from d in db.Classes
                                            from e in db.Sections
                                            from f in db.PeriodsSchedules
                                            where a.AcademicYearId == b.AcademicYearId &&
                                            a.ClassId == b.ClassId &&
                                            a.SectionId == b.SectionId &&
                                            a.SubjectId == b.SubjectId &&
                                            a.SubjectId == c.SubjectId &&
                                            a.AcademicYearId == yearId &&
                                            b.EmployeeRegisterId == facultyId &&
                                            a.ClassId == d.ClassId &&
                                            a.SectionId == e.SectionId &&
                                            d.TimeScheduleId == f.TimeScheduleId
                                            select new
                                            {
                                                day = a.Day,
                                                period = a.Period, 
                                                periodNum=a.Period,
                                                TimeScheduleId = d.TimeScheduleId,
                                                SubjectName = c.SubjectName,
                                                ClassType = d.ClassType,
                                                SectionName = e.SectionName

                                            }).ToList().Select(q => new FacultyPeriodSubject
                                            {
                                                day = q.day,
                                                period = q.period,
                                                TimeScheduleId = q.TimeScheduleId,
                                                subject = q.SubjectName + " (" + q.ClassType + " " + q.SectionName + ")",
                                                PeriodNum = q.periodNum
                                            }).ToList().Select(a=> new  
                                            //FacultyPeriodSubject
                                            {
                                                day = a.day,
                                                period = a.period,
                                                subject = a.subject,
                                                txt_PeriodTime = GetPeriodTime(a.TimeScheduleId, Convert.ToInt16(a.PeriodNum)) 

                                            }).Distinct().ToList().Select(a => new FacultyPeriodSubject
                                            {
                                                day = a.day,
                                                period = a.period,
                                                subject = a.subject,
                                                PeriodTime = a.txt_PeriodTime
                                            }).ToList(); 
            return FacultyPeriodSubjectList;
        }

        public string GetPeriodTime(int? TimeScheduleId, int PeriodNum)
        {
            var File = (from a in db.PeriodsSchedules
                        where a.TimeScheduleId == TimeScheduleId && a.PeriodNumber == PeriodNum
                        select new
                        {

                            StTime = a.StartTime.Value,
                            EdTime = a.EndTime.Value

                        }).ToList().Select(x => new TimePeriodSchedule
                        {
                            txt_PeriodTime = "(" + x.StTime.ToString(@"hh\:mm") + " - " + x.EdTime.ToString(@"hh\:mm") + ")"
                        }).ToList();

            string[] txt_PeriodTime = new string[File.Count];
            for (int i = 0; i < File.Count; i++)
            {
                txt_PeriodTime[i] = File[i].txt_PeriodTime;
            }
            string txt_PeriodTimeList = string.Join(",", txt_PeriodTime);
            return txt_PeriodTimeList;

        }
        
        public List<TimeTable> GetClassTimeTable(int yid, int cid, int sid)
        {
            var ans = (from a in db.AssignSubjectToPeriods
                       from b in db.Subjects
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.SubjectId == b.SubjectId
                       select new TimeTable
                       {
                           day = a.Day,
                           period = a.Period,
                           subject = b.SubjectName
                       }).ToList();
            return ans;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}