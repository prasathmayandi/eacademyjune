﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class ELearningServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        DateTime date = DateTimeByZone.getCurrentDate();

        public void addELearn(int yearId,int classId,int secId,DateTime submissionDate,string title,string desc,string FileName,int FacultyId)
        {
            ELearning add = new ELearning()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                DateOfSubmission = submissionDate,
                Title = title,
                Descriptions = desc,
                FileName = FileName,
                EmployeeRegisterId = FacultyId,
                DateOfPosted = date,
                Status = true
            };
            db.ELearnings.Add(add);
            db.SaveChanges();
        }

        public string getElearningAttachfilename(int ELearn_Id)
        {
            string el;
            var File = (from a in db.ELearnings where a.ELearningId == ELearn_Id select a).FirstOrDefault().FileName;
            if (File != null)
            {
                var EL = (from a in db.ELearnings where a.ELearningId == ELearn_Id select a).FirstOrDefault().Title;
                string elearnid = QSCrypt.Encrypt(ELearn_Id.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    el = "<a href=/ModulesInfo/OpenELearnFile/?id=" + elearnid + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + EL;
                }
                else
                {
                    el = "<a href=/ModulesInfo/OpenELearnFile/?id=" + elearnid + "&name=student><i class='fa fa-file-word-o'></i></a>" + EL;
                }
            }
            else
            {
                var EL = (from a in db.ELearnings where a.ELearningId == ELearn_Id select a).FirstOrDefault().Title;
                el = EL;
            }
            return el;
        }
        public List<ELearningList> getELearningList(int? ELearn_List_allAcademicYears,int classId,int secId,DateTime? ELearn_List_txt_fromDate,DateTime? ELearn_List_txt_toDate,int FacultyId)
        {
            var List = (from a in db.ELearnings
                        where a.AcademicYearId == ELearn_List_allAcademicYears &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.DateOfPosted >= ELearn_List_txt_fromDate &&
                        a.DateOfPosted <= ELearn_List_txt_toDate &&
                        a.EmployeeRegisterId == FacultyId
                        select new
                        {
                            ELearn_Id = a.ELearningId,
                            PostedtDate1 = a.DateOfPosted,
                            Title = a.Title,
                            Descriptions = a.Descriptions,
                            LastDate1 = a.DateOfSubmission,
                            status = a.Status,
                            File = a.FileName
                        }).ToList().Select(a => new ELearningList
                        {
                            ELearn_Id = QSCrypt.Encrypt(a.ELearn_Id.ToString()),
                            PostedtDate = a.PostedtDate1.Value.ToString("dd MMM, yyyy"),
                            Title = a.Title,
                            Description = a.Descriptions,
                            LastDate = a.LastDate1.Value.ToString("dd MMM, yyyy"),
                            Status = a.status,
                            File = a.File,
                            ElearningAttachfilename =getElearningAttachfilename(a.ELearn_Id),
                        }).ToList();
            return List;
        }


        public ELearning getELearnFile(int fid)
        {
            var row = (from a in db.ELearnings where a.ELearningId == fid select a).FirstOrDefault();
            return row;
        }

        public ELearnReplyList getELearnDetailById(int ELearnId)
        {
            var getRow = (from a in db.ELearnings
                          where a.ELearningId == ELearnId
                          select new ELearnReplyList
                          {
                              Title = a.Title,
                              Description = a.Descriptions,
                              SubmissionDate = SqlFunctions.DateName("day", a.DateOfSubmission).Trim() + " " + SqlFunctions.DateName("month", a.DateOfSubmission).Remove(3) + ", " + SqlFunctions.DateName("year", a.DateOfSubmission),
                          }).FirstOrDefault();
            return getRow;
        }

        public List<ELearningList> getStudentElearn(int yearId, int classId, int secId, DateTime ELearn_FromDate, DateTime ELearn_ToDate)
        {
            var List = (from a in db.ELearnings
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.DateOfPosted >= ELearn_FromDate &&
                        a.DateOfPosted <= ELearn_ToDate
                        select new
                        {
                            ELearn_Id = a.ELearningId,
                            PostedtDate1 = a.DateOfPosted,
                            Title = a.Title,
                            Descriptions = a.Descriptions,
                            LastDate1 = a.DateOfSubmission,
                            status = a.Status,
                            File = a.FileName
                        }).ToList().Select(a => new ELearningList
                        {
                            ELearn_Id = QSCrypt.Encrypt(a.ELearn_Id.ToString()),
                            PostedtDate = a.PostedtDate1.Value.ToString("dd MMM, yyyy"),
                            Title = a.Title,
                            Description = a.Descriptions,
                            LastDate = a.LastDate1.Value.ToString("dd MMM, yyyy"),
                            Status = a.status,
                            File = a.File,
                            ElearningAttachfilename = getStudentElearningAttachfilename(a.ELearn_Id),
                        }).ToList();
            return List;
        }


        public string getParentElearningAttachfilename(int ELearn_Id)
        {
            string el;
            var File = (from a in db.ELearnings where a.ELearningId == ELearn_Id select a).FirstOrDefault().FileName;
            if (File != null)
            {
                var EL = (from a in db.ELearnings where a.ELearningId == ELearn_Id select a).FirstOrDefault().Title;
                string elearnid = QSCrypt.Encrypt(ELearn_Id.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    el = "<a href=/ParentModuleAssigned/OpenELearnFile/?id=" + elearnid + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + EL;
                }
                else
                {
                    el = "<a href=/ParentModuleAssigned/OpenELearnFile/?id=" + elearnid + "&name=student><i class='fa fa-file-word-o'></i></a>" + EL;
                }
            }
            else
            {
                var EL = (from a in db.ELearnings where a.ELearningId == ELearn_Id select a).FirstOrDefault().Title;
                el = EL;
            }
            return el;
        }

        public string getStudentElearningAttachfilename(int ELearn_Id)
        {
            string el;
            var File = (from a in db.ELearnings where a.ELearningId == ELearn_Id select a).FirstOrDefault().FileName;
            if (File != null)
            {
                var EL = (from a in db.ELearnings where a.ELearningId == ELearn_Id select a).FirstOrDefault().Title;
                string elearnid = QSCrypt.Encrypt(ELearn_Id.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    el = "<a href=/StudentModuleAssigned/OpenELearnFile/?id=" + elearnid + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + EL;
                }
                else
                {
                    el = "<a href=/StudentModuleAssigned/OpenELearnFile/?id=" + elearnid + "&name=student><i class='fa fa-file-word-o'></i></a>" + EL;
                }
            }
            else
            {
                var EL = (from a in db.ELearnings where a.ELearningId == ELearn_Id select a).FirstOrDefault().Title;
                el = EL;
            }
            return el;
        }


        public ELearning getStudentELearnFile(int id)
        {
            var row = (from a in db.ELearnings where a.ELearningId == id select a).FirstOrDefault();
            return row;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}