﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class NewsTrendsServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public NewsTrends getNewsTrendByID(int newsId)
        {
            var news = (from t1 in dc.AcademicYears
                        from t2 in dc.NewsTrends
                        where t1.AcademicYearId == t2.AcademicYearId && t2.NewsTrendsId==newsId
                        select new NewsTrends
                        {
                            AcademicYear = t1.AcademicYear1,
                            acYear = t1.AcademicYearId,
                            NewsTrendId = t2.NewsTrendsId,
                            tacYSD = t2.NewsDate,
                            Heading = t2.Heading,
                            Desc = t2.Description,
                            Status = t2.Status,
                            Date =  SqlFunctions.DateName("year", t2.NewsDate)+"/"+ SqlFunctions.StringConvert((double)t2.NewsDate.Value.Month).TrimStart() + "/" +SqlFunctions.DateName("day", t2.NewsDate).Trim() 
                        }).FirstOrDefault();
            return news;
        }

        public List<NewsTrends> getNewsTrends()
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.NewsTrends
                       where t1.AcademicYearId == t2.AcademicYearId
                       select new NewsTrends {  AcademicYear = t1.AcademicYear1, acYear=t1.AcademicYearId, NewsTrendId = t2.NewsTrendsId, tacYSD = t2.NewsDate, Heading = t2.Heading, Desc = t2.Description, Status = t2.Status,
                                                Date = SqlFunctions.DateName("day",t2.NewsDate).Trim() + "-" + SqlFunctions.StringConvert((double)t2.NewsDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", t2.NewsDate) 
                       }).ToList().
                       Select(q => new NewsTrends {acYear=q.acYear,NewsId=QSCrypt.Encrypt(q.NewsTrendId.ToString()), AcademicYear = q.AcademicYear, NewsTrendId = q.NewsTrendId, tacYSD = q.tacYSD, Heading = q.Heading, Desc = q.Desc, Status = q.Status,                     
                       Date= q.Date}).ToList();
            return ans;
        }

        public void addNewsTrends(int acid, DateTime? date, string heading, string desc)
        {
            NewsTrend news = new NewsTrend()
            {
                AcademicYearId = acid,
                NewsDate = date,
                Heading = heading,
                Description = desc,
                Status = true
            };
            dc.NewsTrends.Add(news);
            dc.SaveChanges();
        }

        public void updateNewsTrends(int newsId, DateTime date, string heading, string desc, string Status)
        {
            var update = dc.NewsTrends.Where(q => q.NewsTrendsId.Equals(newsId)).First();
            update.NewsDate = date;
            update.Heading = heading;
            update.Description = desc;
            if (Status == "Active")
            {
                update.Status = true;
            }

            else
            {
                update.Status = false;
            }

            dc.SaveChanges();
        }

        public void deleteNewsTrends(int newsId)
        {
            var delete = dc.NewsTrends.Where(q => q.NewsTrendsId.Equals(newsId)).First();
            dc.NewsTrends.Remove(delete);
            dc.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
       
    }
}