﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class AssignActivityToStudentServices:IDisposable
    
    {
        EacademyEntities dc = new EacademyEntities();
        public void addActivityStudent(int acid, int cid, int sec_id, int std_id, int aid, string note)
        {
            AssignActivityToStudent activity = new AssignActivityToStudent()
            {
                AcademicYearId = acid,
                ActivityId = aid,
                ClassId = cid,
                SectionId = sec_id,
                StudentRegisterId = std_id,
                Note = note,
                Status=true
            };

            dc.AssignActivityToStudents.Add(activity);
            dc.SaveChanges();
        }

        public List<eAcademy.Models.ActivityIncharge> getActivityStudent()
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.Sections
                       from t4 in dc.Students                       
                       from t6 in dc.ExtraActivities                       
                       from t8 in dc.AssignActivityToStudents
                       where t1.AcademicYearId == t8.AcademicYearId && t2.ClassId == t8.ClassId && t3.SectionId == t8.SectionId && t4.StudentRegisterId == t8.StudentRegisterId 
                       && t6.ActivityId == t8.ActivityId 
                       select new eAcademy.Models.ActivityIncharge {ActivityId=t6.ActivityId, Fac_AcYear = t1.AcademicYear1, Fac_AcYearId=t1.AcademicYearId, Class = t2.ClassType, Section = t3.SectionName, Student = t4.FirstName+" "+t4.LastName, Activity = t6.Activity, StudentActivityId = t8.StudentActivityId, Note = t8.Note ,Status=t8.Status}).Distinct().ToList().
                       Select(q => new eAcademy.Models.ActivityIncharge { ActivityId = q.ActivityId, AcademicYear = q.Fac_AcYear, EmployeeId = q.EmpId, EmployeeContactNo = q.EmpContact, Fac_AcYear = q.Fac_AcYear, Fac_AcYearId = q.Fac_AcYearId, Class = q.Class, Section = q.Section, Student = q.Student,  Activity = q.Activity, StudentActivityId = q.StudentActivityId, Note = q.Note, Status = q.Status, Description = q.Note, ClassSection = q.Class + "-" + q.Section, StdActivityId = QSCrypt.Encrypt(q.StudentActivityId.ToString()) }).Distinct().ToList();
            return ans;
        }

        public bool checkActivityStudent(int acid, int cid, int sec_id, int std_id, int aid)
        {
            var ans = dc.AssignActivityToStudents.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ActivityId.Value.Equals(aid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) && q.StudentRegisterId.Value.Equals(std_id)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal void updateActivityStudent(int s_aid, int acid, int cid, int sec_id, int std_id, int aid, string note,string Status)
        {
            var update = dc.AssignActivityToStudents.Where(q => q.StudentActivityId.Equals(s_aid)).First();
            update.ActivityId = aid;
            update.Note = note;

            if (Status == "Active")
            {
                update.Status = true;
            }

            else
            {
                update.Status = false;
            }

            dc.SaveChanges();
        }

        internal void deleteActivityStudent(int s_aid)
        {
            var delete = dc.AssignActivityToStudents.Where(q => q.StudentActivityId.Equals(s_aid)).First();
            dc.AssignActivityToStudents.Remove(delete);
            dc.SaveChanges();
        }

        private bool disposed = false;
     
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}