﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class ResourceValueService
    {
        public Dictionary<string, string> GetResourceValues(Models.ResourceType type, string cultureName, string orientation, string countryCode)
        {
            var list = new Dictionary<string, string>();
            string typeName = type.ToString();

            using (var ctx = new EacademyEntities())
            {
                var query =
                    from resource in ctx.DefaultResources
                    join rt in ctx.ResourceTypes on resource.ResourceTypeId equals rt.Id into rt1
                    from resourceType in rt1.DefaultIfEmpty()
                    join rv in ctx.ResourceValues on resource.Id equals rv.ResourceId into rv1
                    from resourceValue in rv1.Where(rv => rv.CultureName == cultureName).DefaultIfEmpty()
                    where resourceType.Name == typeName && resourceValue.CultureName == cultureName && !resource.Deleted && resourceValue.TextOrientation == orientation 
                    select new
                    {
                        Name = resource.Name,
                        Value = resourceValue.Value,
                    };

                query.ToList().ForEach(x => { if (!list.ContainsKey(x.Name)) list.Add(x.Name, x.Value); });
            }

            return list;
        }

        public static string GetResourceValue(string resourceName, string cultureName, string orientation, string countryCode)
        {
            using (var ctx = new EacademyEntities())
            {
                var query =
                    from resource in ctx.DefaultResources
                    join rv in ctx.ResourceValues on resource.Id equals rv.ResourceId into rv1
                    from resourceValue in rv1.Where(rv => rv.CultureName == cultureName).DefaultIfEmpty()
                    where resource.Name == resourceName && resourceValue.CultureName == cultureName && !resource.Deleted && resourceValue.TextOrientation == orientation 
                    select new
                    {
                        Value = resourceValue.Value 
                    };

                var queryResult = query.FirstOrDefault();

                
                return queryResult != null ? queryResult.Value : string.Empty;
            }
        }

        EacademyEntities db = new EacademyEntities();


        public List<eAcademy.Models.ResourceList> GetResourceList()
        {
            var List = (from a in db.ResourceTypes
                        from b in db.DefaultResources
                        from c in db.ResourceValues
                        from d in db.ResourceLanguages
                        where a.Id == b.ResourceTypeId &&
                        b.Id == c.ResourceId && c.ResourcelanquageId == d.ResourcelanquageId
                        select new eAcademy.Models.ResourceList
                        {
                            Id = c.Id,
                            RType = a.Name,
                            Name = b.Name,
                            Culture = d.LanguageCultureName,
                            Value = c.Value
                        }).ToList();
            return List;
        }

        public ResourceAdd GetparticularRecord(long rowId)
        {
            var row = (from a in db.DefaultResources
                       from b in db.ResourceValues
                       from c in db.ResourceLanguages
                       where b.Id == rowId && a.Id == b.ResourceId && c.ResourcelanquageId == b.ResourcelanquageId
                       select new ResourceAdd
                       {
                           TypeId = a.ResourceTypeId.Value,
                           Resource_Name = a.Name,
                           Resource_Value = b.Value,
                           Id = a.Id,
                           ResourcelanquageId = c.ResourcelanquageId
                       }).FirstOrDefault();

            return row;
        }
        private static bool CultureEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["CultureEnabled"]);
        private static bool OrientationEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["OrientationEnabled"]);
        private static bool CountryEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["CountryEnabled"]);
        public void AddResourcevalue(ResourceAdd m, long ResourceId,string culturename)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            ResourceValue add = new ResourceValue()
            {
                Value = m.Resource_Value,
                DateCreated = date,
                DateUpdated = date,
                Deleted = false,
                ResourceId = ResourceId,
                CultureName = CultureEnabled ? culturename : ConfigurationManager.AppSettings["DefaultCultureValue"],
                TextOrientation = OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"],
                ResourcelanquageId = m.ResourcelanquageId
            };
            db.ResourceValues.Add(add);
            db.SaveChanges();
        }

        public ResourceValue CheckResourceValueExist(long resourceid, int languageid)
        {
            var checkexistresourcevalue = db.ResourceValues.Where(q => q.ResourceId == resourceid && q.ResourcelanquageId == languageid).FirstOrDefault();
            return checkexistresourcevalue;
        }

        public ResourceValue CheckalreadyExit(string Resource_Value ,long id,int languageid,long resourceid )
        {
            var CheckResourceExist = (from a in db.ResourceValues
                                      where a.Id != id &&  a.ResourcelanquageId == languageid && a.ResourceId == resourceid
                                      select a).FirstOrDefault();
            return CheckResourceExist;
        }

        public void UpdateResourcevalue(ResourceAdd m)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var row1 = (from a in db.ResourceValues where a.Id == m.Id select a).FirstOrDefault();
            if (row1 != null)
            {
                row1.Value = m.Resource_Value;
                row1.DateUpdated = date;
                row1.ResourcelanquageId = m.ResourcelanquageId;
                db.SaveChanges();
            }
        }

        public void Updateparticularresourcevalue(ResourceAdd m, long ResourceId, string getCultureName)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var GetCurrentResourceValue = db.ResourceValues.Where(q => q.Id == m.Id).FirstOrDefault();
            if (GetCurrentResourceValue != null)
            {
                GetCurrentResourceValue.Value = m.Resource_Value;
                GetCurrentResourceValue.DateUpdated = date;
                GetCurrentResourceValue.ResourceId = ResourceId;
                GetCurrentResourceValue.CultureName = CultureEnabled ? getCultureName : ConfigurationManager.AppSettings["DefaultCultureValue"];
                GetCurrentResourceValue.TextOrientation = OrientationEnabled ? ConfigurationManager.AppSettings["OrientationValue"] : ConfigurationManager.AppSettings["DefaultOrientationValue"];
                GetCurrentResourceValue.ResourcelanquageId = m.ResourcelanquageId;
                db.SaveChanges();
            }
        }
        
    }
}