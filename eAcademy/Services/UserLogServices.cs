﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class UserLogServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public List<UserLogList> getUserlog()
        {
            var ans = (dc.UserLogs.
                Select(q => new UserLogList
                {
                    UserLogId = q.UserLogId,
                    IpAddress = q.IpAddress,
                    Login = q.LoginTime,
                    logout = q.LogoutTime,
                    LoginTime = SqlFunctions.DateName("day", q.LoginTime).Trim() + "-" + SqlFunctions.StringConvert((double)q.LoginTime.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", q.LoginTime) + " " + SqlFunctions.DateName("hh", q.LoginTime) + ":" + SqlFunctions.DateName("mi", q.LoginTime) + ":" + SqlFunctions.DateName("s", q.LoginTime),
                    LogoutTime = SqlFunctions.DateName("day", q.LogoutTime).Trim() + "-" + SqlFunctions.StringConvert((double)q.LogoutTime.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", q.LogoutTime) + " " + SqlFunctions.DateName("hh", q.LogoutTime) + ":" + SqlFunctions.DateName("mi", q.LogoutTime) + ":" + SqlFunctions.DateName("s", q.LogoutTime),
                    SessionId = q.SessionId,
                    UserRegId = q.UserId,
                    UserType = q.UserType,
                    Link = "click",
                    LoginSource = "Web"
                }).OrderBy(q => q.Login).ToList().Select(a => new UserLogList
                {
                    userid = Convert.ToString(a.UserLogId) + "/" + a.UserType,
                    IpAddress = a.IpAddress,
                    Login = a.Login,
                    logout = a.logout,
                    LoginTime = a.LoginTime,
                    LogoutTime = a.LogoutTime,
                    SessionId = a.SessionId,
                    UserRegId = a.UserRegId,
                    UserType = a.UserType,
                    Link = a.Link,
                    LoginSource = a.LoginSource
                }).OrderBy(q => q.LoginTime).ToList()).Union(
                (from a in dc.TokenAuths
                 from b in dc.PrimaryUserRegisters
                 where b.PrimaryUserRegisterId == a.UserId
                 select new UserLogList
                 {
                     UserLogId = a.Id,
                     Login = a.Login,
                     logout = a.Logout,
                     LoginTime = SqlFunctions.DateName("day", a.Login).Trim() + "-" + SqlFunctions.StringConvert((double)a.Login.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.Login) + " " + SqlFunctions.DateName("hh", a.Login) + ":" + SqlFunctions.DateName("mi", a.Login) + ":" + SqlFunctions.DateName("ss", a.Login),
                     LogoutTime = SqlFunctions.DateName("day", a.Logout).Trim() + "-" + SqlFunctions.StringConvert((double)a.Logout.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.Logout) + " " + SqlFunctions.DateName("hh", a.Logout) + ":" + SqlFunctions.DateName("mi", a.Logout) + ":" + SqlFunctions.DateName("ss", a.Logout),
                     SessionId = a.Token,
                     UserRegId = b.PrimaryUserId,
                     UserType = "Parent",
                     Link = "click",
                     LoginSource = "App"
                 }).OrderBy(q => q.Login).ToList().Select(q => new UserLogList
                 {
                     userid = Convert.ToString(q.UserLogId) + "/" + q.UserType,
                     LoginTime = q.LoginTime,
                     LogoutTime = q.LogoutTime,
                     SessionId = q.SessionId,
                     UserRegId = q.UserRegId,
                     UserType = q.UserType,
                     Login = q.Login,
                     logout = q.logout,
                     Link = q.Link,
                     LoginSource = q.LoginSource
                 }).ToList()).OrderByDescending(q => q.Login).ToList();
            return ans;
        }
        public List<UserLogList> getUserlogDate(DateTime? fromdate, DateTime? todate)
        {
            //&& s.LogoutTime >=fromdate && s.LogoutTime <= todate
            DateTime newdate = todate.Value.AddDays(1);

            var ans = (dc.UserLogs.Where(s => s.LoginTime >= fromdate && s.LoginTime <= newdate).
              Select(q => new UserLogList
              {
                  UserLogId = q.UserLogId,
                  IpAddress = q.IpAddress,
                  Login = q.LoginTime,
                  logout = q.LogoutTime,
                  LoginTime = SqlFunctions.DateName("day", q.LoginTime).Trim() + "-" + SqlFunctions.StringConvert((double)q.LoginTime.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", q.LoginTime) + " " + SqlFunctions.DateName("hh", q.LoginTime) + ":" + SqlFunctions.DateName("mi", q.LoginTime) + ":" + SqlFunctions.DateName("s", q.LoginTime),
                  LogoutTime = SqlFunctions.DateName("day", q.LogoutTime).Trim() + "-" + SqlFunctions.StringConvert((double)q.LogoutTime.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", q.LogoutTime) + " " + SqlFunctions.DateName("hh", q.LogoutTime) + ":" + SqlFunctions.DateName("mi", q.LogoutTime) + ":" + SqlFunctions.DateName("s", q.LogoutTime),
                  SessionId = q.SessionId,
                  UserRegId = q.UserId,
                  UserType = q.UserType,
                  Link = "click",
                  LoginSource = "Web"
              }).OrderBy(q => q.Login).ToList().Select(a => new UserLogList
              {
                  userid = Convert.ToString(a.UserLogId) + "/" + a.UserType,
                  IpAddress = a.IpAddress,
                  Login = a.Login,
                  logout = a.logout,
                  LoginTime = a.LoginTime,
                  LogoutTime = a.LogoutTime,
                  SessionId = a.SessionId,
                  UserRegId = a.UserRegId,
                  UserType = a.UserType,
                  Link = a.Link,
                  LoginSource = a.LoginSource
              }).OrderBy(q => q.LoginTime).ToList()).Union(
              (from a in dc.TokenAuths
               from b in dc.PrimaryUserRegisters
               where b.PrimaryUserRegisterId == a.UserId && a.Login >= fromdate && a.Login <= newdate
               select new UserLogList
               {
                   UserLogId = a.Id,
                   Login = a.Login,
                   logout = a.Logout,
                   LoginTime = SqlFunctions.DateName("day", a.Login).Trim() + "-" + SqlFunctions.StringConvert((double)a.Login.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.Login) + " " + SqlFunctions.DateName("hh", a.Login) + ":" + SqlFunctions.DateName("mi", a.Login) + ":" + SqlFunctions.DateName("ss", a.Login),
                   LogoutTime = SqlFunctions.DateName("day", a.Logout).Trim() + "-" + SqlFunctions.StringConvert((double)a.Logout.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.Logout) + " " + SqlFunctions.DateName("hh", a.Logout) + ":" + SqlFunctions.DateName("mi", a.Logout) + ":" + SqlFunctions.DateName("ss", a.Logout),
                   SessionId = a.Token,
                   UserRegId = b.PrimaryUserId,
                   UserType = "Parent",
                   Link = "click",
                   LoginSource = "App"
               }).OrderBy(q => q.Login).ToList().Select(q => new UserLogList
               {
                   userid = Convert.ToString(q.UserLogId) + "/" + q.UserType,
                   LoginTime = q.LoginTime,
                   LogoutTime = q.LogoutTime,
                   SessionId = q.SessionId,
                   UserRegId = q.UserRegId,
                   UserType = q.UserType,
                   Login = q.Login,
                   logout = q.logout,
                   Link = q.Link,
                   LoginSource = q.LoginSource
               }).ToList()).OrderByDescending(q => q.Login).ToList();
            return ans;
        }
        public List<UserLogList> getUserlogActivity(int id,string type)
        {
           
            if(type =="Employee")
            {
                var ans1=(from a in dc.UserLogs
                          from b in dc.EmployeeUserActivities
                          where a.UserLogId == id && a.UserType == type && b.UserLogId == a.UserLogId
                         select new UserLogList{
                     UserRegId = a.UserId,
                       UserType=a.UserType,
                       Link=b.Features,
                       Activity=b.Activity,
                       Time=b.Time
                         }).OrderByDescending(q=>q.Time).ToList();
                return ans1;
            }
            else if(type == "Student")
            {
                var ans1 = (from a in dc.UserLogs
                            from b in dc.StudentUserActivities
                            where a.UserLogId == id && a.UserType == type && b.UserLogId == a.UserLogId
                            select new UserLogList
                            {
                                UserRegId = a.UserId,
                                UserType = a.UserType,
                                Link = b.Features,
                                Activity = b.Activity,
                                Time = b.Time
                            }).OrderByDescending(q => q.Time).ToList();
                return ans1;
            }
            else
            {
                var ans1 = (from a in dc.UserLogs
                            from b in dc.ParentUserActivities
                            where a.UserLogId == id && a.UserType == type && b.UserLogId == a.UserLogId
                            select new UserLogList
                            {
                                UserRegId = a.UserId,
                                UserType = a.UserType,
                                Link = b.Features,
                                Activity = b.Activity,
                                Time = b.Time
                            }).OrderByDescending(q => q.Time).ToList();
                return ans1;
            }
        }

        public void checkPreviousloginclose(int id)
        {
            var ans = (from a in dc.UserLogs
                       from b in dc.EmployeeUserActivities
                       where a.UserRegId == id && a.LogoutTime == null && b.UserLogId == a.UserLogId
                       select b).OrderByDescending(q => q.EmployeeActivityId).FirstOrDefault();
            if (ans != null)
            {
                var getrow = dc.UserLogs.Where(q => q.UserRegId == id && q.LogoutTime == null).FirstOrDefault();
                if (getrow != null)
                {
                    DateTime ss = ans.Time.Value.AddMinutes(20);
                    getrow.LogoutTime = ss;
                    dc.SaveChanges();
                }
            }
            else
            {
                var getrow = dc.UserLogs.Where(q => q.UserRegId == id && q.LogoutTime == null).FirstOrDefault();
                if (getrow != null)
                {
                    DateTime ss = getrow.LoginTime.Value.AddMinutes(20);
                    getrow.LogoutTime = ss;
                    dc.SaveChanges();
                }
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}