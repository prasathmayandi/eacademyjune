﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;

namespace eAcademy.Services
{
    public class PreAdmissionOnlineRegisterServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        //Jegadeesh
        public PreAdmissionOnLineRegister getRow(int onlineRegId)
        {
            var getRow = (from a in dc.PreAdmissionOnLineRegisters where a.OnlineRegisterId == onlineRegId select a).FirstOrDefault();
            return getRow;
        }
        public PreAdmissionOnLineRegister CheckEmailUserAvaliable(string uname, string pwd)
        {
            PreAdmissionOnLineRegister pre = new PreAdmissionOnLineRegister();
            string covertpwd = FormsAuthentication.HashPasswordForStoringInConfigFile(pwd, "SHA1");
            var a = dc.PreAdmissionOnLineRegisters.Where(q => q.Username.Equals(uname) && q.Passwords.Equals(covertpwd)).FirstOrDefault();
            if (a == null)
            {
                var checkexist = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserEmail.Equals(uname)).FirstOrDefault();
                if (checkexist != null)
                {
                    var salt = checkexist.Salt;
                    var PasswordWithSalt = salt + pwd;
                    string covertpwd1 = FormsAuthentication.HashPasswordForStoringInConfigFile(PasswordWithSalt, "SHA1");
                    var available = dc.PreAdmissionOnLineRegisters.Where(q => q.Username.Equals(uname) && q.Passwords.Equals(covertpwd1)).FirstOrDefault();
                    if (available != null)
                    {
                        return available;
                    }
                    else
                    {
                        var Primaryuser = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserEmail.Equals(uname) && q.PrimaryUserPwd.Equals(covertpwd)).FirstOrDefault();
                        if (Primaryuser != null)
                        {
                            int onlineid = Convert.ToInt16(Primaryuser.OnlineRegisterId);
                            pre.Username = Primaryuser.PrimaryUserContactNo.ToString();
                            pre.OnlineRegisterId = onlineid;
                            return pre;
                        }
                        else
                        {
                            return available;
                        }
                    }             
                }
                else
                {
                    return a;
                }
            }
            else
            {
                return a;
            }
        }
        public PreAdmissionOnLineRegister CheckMobileUserAvaliable(string uname, string pwd)
        {
            PreAdmissionOnLineRegister pre = new PreAdmissionOnLineRegister();
            string covertpwd = FormsAuthentication.HashPasswordForStoringInConfigFile(pwd, "SHA1");
            var a = dc.PreAdmissionOnLineRegisters.Where(q => q.Username.Equals(uname) && q.Passwords.Equals(covertpwd)).FirstOrDefault();
            if (a == null)
            {
                long num = Convert.ToInt64(uname);
                var checkexist = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserContactNo.Value.Equals(num)).FirstOrDefault();
                if (checkexist != null)
                {
                    var salt = checkexist.Salt;
                    var PasswordWithSalt = salt + pwd;
                    string covertpwd1 = FormsAuthentication.HashPasswordForStoringInConfigFile(PasswordWithSalt, "SHA1");
                    var available = dc.PreAdmissionOnLineRegisters.Where(q => q.Username.Equals(uname) && q.Passwords.Equals(covertpwd1)).FirstOrDefault();
                    if(available!=null)
                    {
                        return available;
                    }
                    else
                    {
                        var Primaryuser = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserContactNo.Value.Equals(num) && q.PrimaryUserPwd.Equals(covertpwd)).FirstOrDefault();
                        if (Primaryuser != null)
                        {
                            int onlineid=Convert.ToInt16(Primaryuser.OnlineRegisterId);
                            pre.Username = Primaryuser.PrimaryUserContactNo.ToString();
                            pre.OnlineRegisterId = onlineid;
                            return pre;
                        }
                        else
                        {
                            return available;
                        }
                    }                    
                }
                else
                {
                    return a;
                }
            }
            else
            {
                return a;
            }
        }
        public PreAdmissionPrimaryUserResetPassword CheckPrimaryUserId(int regid)
        {
            var ans = (from a in dc.PreAdmissionPrimaryUserResetPasswords where a.UserId == regid select a).FirstOrDefault();
            return ans;
        }

        public M_Preuser CheckOnlineUserName(string uname)
        {
            var res = (from a in dc.PreAdmissionOnLineRegisters                    
                       where a.Username == uname
                       select new M_Preuser
                       {
                           PrimaryUserAdmissionId=a.OnlineRegisterId,
                           PrimaryUserName = a.Username,
                           PrimaryUserEmail=a.Username
                       }).FirstOrDefault();
            return res;
        }
        public M_Preuser CheckMasterUserName(string uname)
        {
            var res = (from a in dc.PrimaryUserRegisters
                       where a.PrimaryUserEmail == uname
                       select new M_Preuser
                       {
                           PrimaryUserAdmissionId = a.PrimaryUserRegisterId,
                           PrimaryUserName = a.PrimaryUserName,
                           PrimaryUserEmail = a.PrimaryUserEmail
                       }).FirstOrDefault();
            return res;
        }
        public PreAdmissionPrimaryUserRegister CheckExitUserName(string uname)
        {
            var res = (from b in dc.PreAdmissionPrimaryUserRegisters
                       where  b.PrimaryUserEmail == uname
                       select b).FirstOrDefault();
            return res;
        }

        public bool Emailregister(string uname, string pwd, string rpwd)
        {
            var Onlinetable = dc.PreAdmissionOnLineRegisters.Where(q => q.Username.Equals(uname)).FirstOrDefault();
            var OnlinePrimaryUsertable = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserEmail.Equals(uname)).FirstOrDefault();
            var offlinePrimaryUsertable = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserEmail.Equals(uname)).FirstOrDefault();
            if (Onlinetable == null && OnlinePrimaryUsertable == null && offlinePrimaryUsertable == null)
            {
                PreAdmissionOnLineRegister add = new PreAdmissionOnLineRegister()
                {
                    Username = uname,
                    Passwords = pwd
                };
                dc.PreAdmissionOnLineRegisters.Add(add);
                dc.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool Mobileregister(string uname, string pwd, string rpwd)
        {
            long num = Convert.ToInt64(uname);
            var Onlinetable = dc.PreAdmissionOnLineRegisters.Where(q => q.Username.Equals(uname)).FirstOrDefault();
            var OnlinePrimaryUsertable = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserContactNo.Value.Equals(num)).FirstOrDefault();
            var offlinePrimaryUsertable = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserContactNo.Value.Equals(num)).FirstOrDefault();
            if (Onlinetable == null && OnlinePrimaryUsertable == null && offlinePrimaryUsertable == null)
            {
                PreAdmissionOnLineRegister add = new PreAdmissionOnLineRegister()
                {
                    Username = uname,
                    Passwords = pwd
                };
                dc.PreAdmissionOnLineRegisters.Add(add);
                dc.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        public PreAdmissionOnLineRegister findUser(int id)
        {
            var a = dc.PreAdmissionOnLineRegisters.Where(q => q.OnlineRegisterId.Equals(id)).FirstOrDefault();
            return a;
        }
        public int GetStudentId(int id)
        {
            var ans = (from a in dc.PreAdmissionPrimaryUserRegisters
                       from b in dc.PreAdmissionTransactions
                       from c in dc.PreAdmissionStudentRegisters
                       where a.OnlineRegisterId == id && b.PrimaryUserAdmissionId == a.PrimaryUserAdmissionId && c.StudentAdmissionId == b.StudentAdmissionId && c.Statusflag == "InComplete"
                       // && c.Statusflag == "InComplete"   //&& c.Statusflag != "InterView" && c.Statusflag != "Conditionaloffer" && c.Statusflag != "Accepted" && c.Statusflag != "FeesPaid" && c.Statusflag != "Reject" && c.Statusflag != "Enrolled" && c.Statusflag != "Pending" && c.Statusflag == "InComplete" // || c.Statusflag == "AddMore"  
                       select c).FirstOrDefault();

          
            if (ans != null)
            {
                return ans.StudentAdmissionId;
            }
            else
            {
                return 0;
            }
        }       


        public int Get_Addmoredetails_by_StudentId(int id, int studentid)
        {
            var ans = (from a in dc.PreAdmissionPrimaryUserRegisters
                       from b in dc.PreAdmissionTransactions
                       from c in dc.PreAdmissionStudentRegisters
                       where a.OnlineRegisterId == id && b.PrimaryUserAdmissionId == a.PrimaryUserAdmissionId 
                       && c.StudentAdmissionId == b.StudentAdmissionId 
                       && c.StudentAdmissionId==studentid &&  c.Statusflag == "AddMore"
                       select c).FirstOrDefault();
            if (ans != null)
            {
                return ans.StudentAdmissionId;
            }
            else
            {
                return 0;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}