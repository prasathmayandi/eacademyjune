﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class AssignSubstitudeFacultyServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public void addSubstitude(int yearId, int classId, int secId, int subId, int periodId, DateTime SubstitudeDate, int dayOrder, int FacultyId, int AssignedBy, int actualEmpRegId)
        {
            AssignSubstitudeFaculty add = new AssignSubstitudeFaculty()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                SubjectId = subId,
                Period = periodId,
                DateOfSubstitude = SubstitudeDate,
                DayOrder = dayOrder,
                ActualEmployeeRegisterId = actualEmpRegId,
                SubstitudeEmployeeRegisterId = FacultyId,
                AssignedBy = AssignedBy
            };
            db.AssignSubstitudeFaculties.Add(add);
            db.SaveChanges();
        }

        public AssignSubstitudeFaculty checkSubstitudeExist(DateTime DateOfsubstitude, int employeeRegId, int dayOrder)
        {
            var getRow = (from a in db.AssignSubstitudeFaculties where a.DateOfSubstitude == DateOfsubstitude && a.ActualEmployeeRegisterId == employeeRegId && a.DayOrder == dayOrder select a).FirstOrDefault();
            return getRow;
        }

        public IEnumerable<object> getExistFacultyPeriodList(DateTime DateOfsubstitude, int employeeRegId, int dayOrder)
        {
            var list = (from a in db.AssignSubstitudeFaculties
                        from b in db.Classes
                        from c in db.Sections
                        from d in db.Subjects
                        from e in db.TechEmployees
                        where a.ClassId == b.ClassId &&
                              a.SectionId == c.SectionId &&
                              a.SubjectId == d.SubjectId &&
                        
                            a.DateOfSubstitude == DateOfsubstitude &&
                            a.ActualEmployeeRegisterId == employeeRegId &&
                            a.DayOrder == dayOrder &&
                            a.SubstitudeEmployeeRegisterId == e.EmployeeRegisterId
                        select new
                        {
                            Period = a.Period,
                            Class = b.ClassType,
                            Section = c.SectionName,
                            Subject = d.SubjectName,
                            ClassId = a.ClassId,
                            SectionId = a.SectionId,
                            SubjectId = a.SubjectId,
                            substitudeFacultyId = a.SubstitudeEmployeeRegisterId,
                            substitudeFacultyName = e.EmployeeName
                        }).ToList().Select(a=>new {
                            Period = a.Period,
                            Class = a.Class,
                            Section = a.Section,
                            Subject = a.Subject,
                            ClassId1 = QSCrypt.Encrypt(a.ClassId.ToString()),
                            SectionId1 = QSCrypt.Encrypt(a.SectionId.ToString()),
                            SubjectId1 = QSCrypt.Encrypt(a.SubjectId.ToString()),
                            Period1 = QSCrypt.Encrypt(a.Period.ToString()),
                            substitudeFacultyId = a.substitudeFacultyId,
                            substitudeFacultyName = a.substitudeFacultyName
                        }).ToList();
            return list;
        }

        public void updateSubstitude(int yearId, int classId, int secId, int subId, int periodId, DateTime SubstitudeDate, int dayOrder, int FacultyId, int AssignedBy, int actualEmpRegId)
        {
            var getRecord = (from a in db.AssignSubstitudeFaculties where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.SubjectId == subId && a.Period == periodId && a.DateOfSubstitude == SubstitudeDate && a.DayOrder == dayOrder && a.ActualEmployeeRegisterId == actualEmpRegId select a).FirstOrDefault();
            getRecord.SubstitudeEmployeeRegisterId = FacultyId;
            getRecord.AssignedBy = AssignedBy;
            db.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}