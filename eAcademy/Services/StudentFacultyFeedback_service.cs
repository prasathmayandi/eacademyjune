﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class StudentFacultyFeedback_service : IDisposable
    {
        EacademyEntities ee = new EacademyEntities();

        public List<R_FacultyFeedback> getFaculyFeedbackList(int? FacultyId, DateTime? R_Feedback_FromDate, DateTime? R_Feedback_ToDate)
        {
            var FeedbackList = ((from a in ee.ParentFacultyFeedbacks
                                 from b in ee.PrimaryUserRegisters
                                 from c in ee.Subjects
                                 where a.EmployeeRegisterId == FacultyId &&
                                 a.DateOfFeedback >= R_Feedback_FromDate &&
                                 a.DateOfFeedback <= R_Feedback_ToDate &&
                                 a.ParentRegisterId == b.PrimaryUserRegisterId && a.SubjectId == c.SubjectId

                                 select new R_FacultyFeedback
                                 {
                                     DateOfFeedback = a.DateOfFeedback,
                                      Date = SqlFunctions.DateName("day", a.DateOfFeedback).Trim() + "/" + SqlFunctions.StringConvert((double)a.DateOfFeedback.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DateOfFeedback),
                                     Feedback = a.Feedback,
                                     UserName = b.PrimaryUserName,
                                     SubjectName = c.SubjectName,
                                     UserType = "Parent"
                                 }).ToList().Select(a => new R_FacultyFeedback
                                 {
                                     Date =a.Date,// a.DateOfFeedback.Value.ToString("MMM dd, yyyy"),
                                     Feedback = a.Feedback,
                                     UserName = a.UserName,
                                     SubjectName = a.SubjectName,
                                     UserType = a.UserType
                                 })
                                           .Union(from a in ee.StudentFacultyFeedbacks
                                                  from b in ee.Students
                                                  from c in ee.Subjects
                                                  where a.EmployeeRegisterId == FacultyId &&
                                                  a.DateOfFeedback >= R_Feedback_FromDate &&
                                                  a.DateOfFeedback <= R_Feedback_ToDate &&
                                                  a.StudentRegisterId == b.StudentRegisterId && a.SubjectId == c.SubjectId
                                                  select new R_FacultyFeedback
                                                  {
                                                      Date = SqlFunctions.DateName("day", a.DateOfFeedback).Trim() + "/" + SqlFunctions.StringConvert((double)a.DateOfFeedback.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DateOfFeedback),
                                                      Feedback = a.Feedback,
                                                      UserName = b.FirstName + " " + b.LastName,
                                                      SubjectName = c.SubjectName,
                                                      UserType = "Student",
                                                      DateOfFeedback=a.DateOfFeedback
                                                  })
                                                  ).ToList();
            return FeedbackList;
        }

        public void addFacultyFeedback(int StudentRegId, int yearId, int classId, int secId, int subId, int FacultyId, string Feedback)
        {
            DateTime date = DateTimeByZone.getCurrentDate();
            StudentFacultyFeedback add = new StudentFacultyFeedback()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                SubjectId = subId,
                EmployeeRegisterId = FacultyId,
                StudentRegisterId = StudentRegId,
                DateOfFeedback = date,
                Feedback = Feedback,
            };
            ee.StudentFacultyFeedbacks.Add(add);
            ee.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ee.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}