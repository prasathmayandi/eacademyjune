﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class SubjectnotesServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        
        public List<SubjectNotes> getStudentNotes(int yearId, int classId, int secId, int subId, DateTime fdate, DateTime toDate)
        {
            //var list = (from a in db.SubjectNotes
            //            where a.AcademicYearId == yearId &&
            //            a.ClassId == classId &&
            //            a.SectionId == secId &&
            //            a.SubjectId == subId &&
            //            a.DateOfNotes >= fdate &&
            //            a.DateOfNotes <= toDate
            //            select new SubjectNotes
            //            {
            //                NotesId = a.NotesId,
            //                NotesPostedDate = a.DateOfNotes,
            //                Topic = a.Topic,
            //                Descriptions = a.Descriptions,
            //                NotesFileName = a.NotesFileName,
            //                 studentattachedfilename=getSubjectNotesWithFile(a.NotesId)
            //            }).ToList();
            //return list;
            var list = (from a in db.SubjectNotes
                        from b in db.Subjects
                        where a.SubjectId == b.SubjectId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.DateOfNotes >= fdate &&
                        a.DateOfNotes <= toDate
                        select new
                        {
                            NotesId = a.NotesId,
                            NotesPostedDate = a.DateOfNotes,
                            Topic = a.Topic,
                            Descriptions = a.Descriptions,
                            NotesFileName = a.NotesFileName,
                            subject = b.SubjectName,
                        }).ToList().Select(a => new SubjectNotes
                        {
                            Notes_Id = QSCrypt.Encrypt(a.NotesId.ToString()),
                            NotesPosted_Date = a.NotesPostedDate.Value.ToString("MMM dd yyyy"),
                            Topic = a.Topic,
                            Descriptions = a.Descriptions,
                            NotesFileName = a.NotesFileName,
                            Subject = a.subject,
                            studentattachedfilename = getParentSubjectNotesWithFile(a.NotesId)

                        }).ToList();
            return list;
                    }

        public SubjectNote getStudentNotesFile(int fid)
        {
            var getSubjectNotesFile = (from a in db.SubjectNotes where a.NotesId == fid select a).FirstOrDefault();
            return getSubjectNotesFile;
        }

        public void addSubjectNotes(int yearId, int classId, int secId, int subId, string topic, string desc, string fileName, int FacultyId)
        {
            DateTime notesDate = DateTimeByZone.getCurrentDate();
            SubjectNote add = new SubjectNote()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                SubjectId = subId,
                DateOfNotes = notesDate,
                Topic = topic,
                Descriptions = desc,
                NotesFileName = fileName,
                NotesStatus = true,
                EmployeeRegisterId = FacultyId
            };
            db.SubjectNotes.Add(add);
            db.SaveChanges();
        }

        public List<SubjectNotesList> getSubjectNotesList(int yearId, int classId, int secId, int subId, int FacultyId)
        {
            var list = (from a in db.SubjectNotes
                        where a.AcademicYearId == yearId &&
                                 a.ClassId == classId &&
                                 a.SectionId == secId &&
                                 a.SubjectId == subId &&
                                 a.EmployeeRegisterId == FacultyId
                        select new SubjectNotesList
                        {
                            NotesId = a.NotesId,
                            DateOfNotes = a.DateOfNotes,
                            Topic = a.Topic,
                            Descriptions = a.Descriptions,
                            NotesStatus = a.NotesStatus,
                            NotesFileName = a.NotesFileName
                        }).ToList();

            return list;
        }

        public void updateSubjectNotes(DateTime notesDate, string topic, string desc, Boolean status, int eid)
        {
            var getRow = (from a in db.SubjectNotes where a.NotesId == eid select a).FirstOrDefault();
            getRow.Topic = topic;
            getRow.DateOfNotes = notesDate;
            getRow.Descriptions = desc;
            getRow.NotesStatus = status;
            db.SaveChanges();
        }

        public void updateSubjectNotesFile(int eid, string fileName1)
        {
            var getRow = (from a in db.SubjectNotes where a.NotesId == eid select a).FirstOrDefault();
            getRow.NotesFileName = fileName1;
            db.SaveChanges();
        }

        public void delSubjectNotes(int id)
        {
            var getRow = (from a in db.SubjectNotes where a.NotesId == id select a).FirstOrDefault();
            db.SubjectNotes.Remove(getRow);
            db.SaveChanges();
        }

        public List<SubjectNotes> getStudentNotes(int yearId, int classId, int secId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in db.SubjectNotes
                        from b in db.Subjects
                        where a.SubjectId == b.SubjectId &&
                        a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.DateOfNotes >= fdate &&
                        a.DateOfNotes <= toDate
                        select new
                        {
                            NotesId = a.NotesId,
                            NotesPostedDate = a.DateOfNotes,
                            Topic = a.Topic,
                            Descriptions = a.Descriptions,
                            NotesFileName = a.NotesFileName,
                            subject = b.SubjectName,
                        }).ToList().Select(a => new SubjectNotes
                        {
                            Notes_Id = QSCrypt.Encrypt(a.NotesId.ToString()),
                            NotesPosted_Date = a.NotesPostedDate.Value.ToString("MMM dd yyyy"),
                            Topic = a.Topic,
                            Descriptions = a.Descriptions,
                            NotesFileName = a.NotesFileName,
                            Subject = a.subject,
                            studentattachedfilename = getParentSubjectNotesWithFile(a.NotesId)
                                
                        }).ToList();
            return list;
        }
        public string getParentSubjectNotesWithFile(int NotesId)
        {
            string sn;
            var File = (from a in db.SubjectNotes where a.NotesId == NotesId select a).FirstOrDefault().NotesFileName;
            if (File != null)
            {
                var SN = (from a in db.SubjectNotes where a.NotesId == NotesId select a).FirstOrDefault().Topic;
                string subjectid = QSCrypt.Encrypt(NotesId.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    sn = "<a href=/Modules/OpenSubjectNotesFile/?id=" + subjectid + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + SN;
                }
                else
                {
                    sn = "<a href=/ModulesInfo/OpenSubjectNotesFile/?id=" + subjectid + "&name=student><i class='fa fa-file-word-o'></i></a>" + SN;
                }
            }
            else
            {
                var SN = (from a in db.SubjectNotes where a.NotesId == NotesId select a).FirstOrDefault().Topic;
                sn = SN;
            }
            return sn;
        }
        public string getSubjectNotesWithFile(int NotesId)
        {
            string sn;
            var File = (from a in db.SubjectNotes where a.NotesId == NotesId select a).FirstOrDefault().NotesFileName;
            if (File != null)
            {
                var SN = (from a in db.SubjectNotes where a.NotesId == NotesId select a).FirstOrDefault().Topic;
                string subjectid = QSCrypt.Encrypt(NotesId.ToString());
                string[] words = File.Split('.');
                if (words[1] == "pdf")
                {
                    sn = "<a href=/ModulesInfo/OpenSubjectNotesFile/?id=" + subjectid + "&name=student><i class='fa fa-file-pdf-o'></i></a>" + SN;
                }
                else
                {
                    sn = "<a href=/ModulesInfo/OpenSubjectNotesFile/?id=" + subjectid + "&name=student><i class='fa fa-file-word-o'></i></a>" + SN;
                }
            }
            else
            {
                var SN = (from a in db.SubjectNotes where a.NotesId == NotesId select a).FirstOrDefault().Topic;
                sn = SN;
            }
            return sn;
        }


        public List<SubjectNotesList> getSubjectNotesList(int? yearId, int? classId, int? secId, int? subId, DateTime? fdate, DateTime? toDate, int? FacultyId)
        {
            var list = (from a in db.SubjectNotes
                        where a.AcademicYearId == yearId &&
                                 a.ClassId == classId &&
                                 a.SectionId == secId &&
                                 a.SubjectId == subId &&
                                 a.EmployeeRegisterId == FacultyId &&
                                 a.DateOfNotes >= fdate &&
                                 a.DateOfNotes <= toDate
                        select new
                        {
                            NotesId = a.NotesId,
                            DateOfNotes = a.DateOfNotes,
                            Topic = a.Topic,
                            Descriptions = a.Descriptions,
                            NotesStatus = a.NotesStatus,
                            NotesFileName = a.NotesFileName,
                            MI_notes_List_txt_fromDate = a.DateOfNotes,
                            MI_notes_List_txt_toDate = a.DateOfNotes
                        }).ToList().Select(a => new SubjectNotesList
                        {
                            Notes_Id = QSCrypt.Encrypt(a.NotesId.ToString()),
                            DateOf_Notes = a.DateOfNotes.Value.ToString("dd MMM, yyyy"),
                            Topic = a.Topic,
                            Descriptions = a.Descriptions,
                            NotesStatus = a.NotesStatus,
                            NotesFileName = a.NotesFileName,
                            MI_notes_List_txt_fromDate = a.MI_notes_List_txt_fromDate,
                            MI_notes_List_txt_toDate = a.MI_notes_List_txt_toDate,
                            subjectnotesfilename = getSubjectNotesWithFile(a.NotesId),
                        }).ToList();

            return list;
        }

        public void updateSubjectNotes(DateTime notesDate, string topic, string desc, Boolean status, string fileName, int eid)
        {
            var getRow = (from a in db.SubjectNotes where a.NotesId == eid select a).FirstOrDefault();
            getRow.Topic = topic;
            getRow.DateOfNotes = notesDate;
            getRow.Descriptions = desc;
            getRow.NotesStatus = status;
            getRow.NotesFileName = fileName;
            db.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}