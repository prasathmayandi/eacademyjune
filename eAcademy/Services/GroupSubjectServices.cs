﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class GroupSubjectServices
    {
        EacademyEntities dc = new EacademyEntities();
        public int  addGroup(string groupName,int empId)
        {
            GroupSubject group = new GroupSubject()
            {
                GroupSubjectName= groupName,                
                status = true,
                CreatedBy=empId,
                CreatedDate=DateTimeByZone.getCurrentDateTime()
            };

            dc.GroupSubjects.Add(group);
            dc.SaveChanges();
            int GroupSubjectId = group.GroupSubjectId;
            return GroupSubjectId;
            
        }
        public List<GroupList> getGroup()
        {
            var ans = (from t1 in dc.GroupSubjects
                       
                       select new
                       {
                           GroupName=t1.GroupSubjectName,
                           GroupSubjectId=t1.GroupSubjectId,
                           status=t1.status

                       }).ToList().Select(q => new GroupList
                       {
                           GroupName=q.GroupName,
                           GroupSubjectId=q.GroupSubjectId,
                           SubjectName = GroupSubjectName(q.GroupSubjectId),
                           Status = q.status,
                           
                       }).ToList();

            return ans;
        }
        public string GroupSubjectName(int GroupSubjectId)
        {
            var File = (from a in dc.GroupSubjectConfigs
                        from b in dc.Subjects
                        from c in dc.GroupSubjects

                        where a.GroupSubjectId == GroupSubjectId && a.SubjectId==b.SubjectId  && a.status == true

                        select new
                        {
                            Subject = b.SubjectName

                        }).Distinct().ToList();


            string[] Subjects = new string[File.Count];
            for (int i = 0; i < File.Count; i++)
            {
                Subjects[i] = File[i].Subject;
            }
            string SubjectList = string.Join(",", Subjects);
            return SubjectList;
        }
        public GroupList getGroupByGroupID(int gsId)
        {
            var ans = (from t1 in dc.GroupSubjects
                       where t1.GroupSubjectId == gsId
                       select new GroupList
                       {
                           GroupSubjectId=t1.GroupSubjectId,
                           GroupName = t1.GroupSubjectName,                          
                           Status = t1.status
                       }).FirstOrDefault();

            return ans;
        }
        public void UpdateGroup(int group_id, string GroupName, bool G_status, int AdminId)
        {
            var update = dc.GroupSubjects.Where(q => q.GroupSubjectId.Equals(group_id)).First();
            update.GroupSubjectName = GroupName;
            update.status = G_status;
            update.UpdatedBy = AdminId;
            update.UpdatedDate = DateTimeByZone.getCurrentDateTime();            
            dc.SaveChanges();
        }
        public void UpdateGroupStatus(int group_id, int SubId)
        {
            
            var update = dc.GroupSubjectConfigs.Where(q => q.GroupSubjectId.Equals(group_id) && q.SubjectId.Equals(SubId)).First();
            update.status = false;            
            dc.SaveChanges();
        }
        public bool checkUpdateGroupName(string GroupName, int group_id)
        {
            //var ans = dc.Subjects.Where(q => q.SubjectName.Equals(subjectName) && q.Status.Value.Equals(true)).ToList();
            var ans = (from a in dc.GroupSubjects where GroupName == a.GroupSubjectName && a.GroupSubjectId!= group_id select a).FirstOrDefault();
            if (ans == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        
        
        public bool checkGroupName(string GroupName)
        {
            var ans = dc.GroupSubjects.Where(q => q.GroupSubjectName.Equals(GroupName)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}