﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class FeatureServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public List<Feature> getFeature()
        {
            var ans = dc.Features.Where(q => q.FeatureStatus.Equals(true)).OrderBy(q => q.FaetureType).ToList();
            return ans;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}