﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class TechEmployeeEmploymentServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        
        public List<M_TechEmployeeEmployment> GetEmployeeExperience(int eid)
        {
            var res = (from a in dc.TechEmployeeEmployments
                       where a.EmployeeRegisterId == eid
                       select new M_TechEmployeeEmployment
                       {
                           EmployeeExperienceId=a.TechEmployeeEmploymentId,
                           Institute = a.Institute,
                           Designation = a.Designation,
                           Expertise = a.Expertise,
                           StartDate = SqlFunctions.DateName("day", a.StartDate).Trim() + "/" + SqlFunctions.StringConvert((double)a.StartDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.StartDate),
                           EndDate = SqlFunctions.DateName("day", a.EndDate).Trim() + "/" + SqlFunctions.StringConvert((double)a.EndDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.EndDate),
                           Tootalyears=a.TotalYears,
                       }).ToList();
            return res;
        }
        public List<M_TechEmployeeEmployment> GetOtherEmployeeExperience(int eid)
        {
            var res = (from a in dc.TechEmployeeEmployments
                       where a.OtherEmployeeRegisterId == eid
                       select new M_TechEmployeeEmployment
                       {
                           EmployeeExperienceId = a.TechEmployeeEmploymentId,
                           Institute = a.Institute,
                           Designation = a.Designation,
                           Expertise = a.Expertise,
                           StartDate = SqlFunctions.DateName("day", a.StartDate).Trim() + "/" + SqlFunctions.StringConvert((double)a.StartDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.StartDate),
                           EndDate = SqlFunctions.DateName("day", a.EndDate).Trim() + "/" + SqlFunctions.StringConvert((double)a.EndDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.EndDate),
                           Tootalyears = a.TotalYears,
                       }).ToList();
            return res;

        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}