﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class EmployeeAchievementServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        
        public List<AchievementList> getEmployeeAchievement()
        {
            var list = (from a in db.EmployeeAchievements
                        from b in db.TechEmployees
                        where a.EmployeeRegisterId == b.EmployeeRegisterId
                        select new AchievementList
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = b.EmployeeName,
                            Ach_allAcademicYearsA1 = a.AcademicYearId
                        }).ToList().Select(a => new AchievementList
                        {
                            Date = a.DateOfAchievement.Value.ToString("dd MMM, yyyy"),
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = a.Name,
                            txt_doa_search = a.DateOfAchievement,
                            Ach_allAcademicYearsA1 = a.Ach_allAcademicYearsA1
                        }).ToList();
            return list;
        }

        public List<AchievementList> getEmployeeAchievement(int yearId)
        {
            var list = (from a in db.EmployeeAchievements
                        from b in db.TechEmployees
                        where a.AcademicYearId == yearId &&
                           a.EmployeeRegisterId == b.EmployeeRegisterId
                        select new AchievementList
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = b.EmployeeName
                        }).ToList();
            return list;
        }

        public EmployeeAchievement getAchievementById(int EFacultyAchievementId)
        {
            var getRow = (from a in db.EmployeeAchievements where a.Id == EFacultyAchievementId select a).FirstOrDefault();
            return getRow;
        }

        public List<AchievementList> getFacultyAchievement()
        {
            var list = (from a in db.EmployeeAchievements
                        from b in db.TechEmployees
                        where a.EmployeeRegisterId == b.EmployeeRegisterId
                        select new
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = b.EmployeeName,
                            Id = b.EmployeeId,
                            AchievementId = a.Id,
                            AcademicYearId = a.AcademicYearId
                        }).ToList().Select(c => new AchievementList
                        {
                            Date = c.DateOfAchievement.Value.ToString("dd MMM, yyyy"),
                            Achievement = c.Achievement,
                            Descriptions = c.Descriptions,
                            Name = c.Name,
                            EmployeeId = c.Id,
                            AchievementId = c.AchievementId,
                            AchievementId1 = QSCrypt.Encrypt(c.AchievementId.ToString()),
                            txt_doa_search = c.DateOfAchievement,
                            Ach_allAcademicYearsA1 = c.AcademicYearId
                        }).ToList();
            return list;
        }

        public List<AchievementList> sortFacultyAchievementByYear(int Yearid)
        {
            var list = (from a in db.EmployeeAchievements
                        from b in db.TechEmployees
                        where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                              a.AcademicYearId == Yearid
                        select new AchievementList
                        {
                            DateOfAchievement = a.DateOfAchievement,
                            Achievement = a.Achievement,
                            Descriptions = a.Descriptions,
                            Name = b.EmployeeName,
                            Id = b.EmployeeRegisterId,
                            AchievementId = a.Id
                        }).ToList();
            return list;
        }

        public void addFacultyAchievement(int yearId, int EmployeeId, DateTime doa, string achievement, string desc)
        {
            var get = db.EmployeeAchievements.Where(q => q.DateOfAchievement == doa && q.Achievement == achievement && q.AcademicYearId == yearId && q.EmployeeRegisterId == EmployeeId).FirstOrDefault();
            if (get == null)
            {
                EmployeeAchievement add = new EmployeeAchievement()
                {
                    AcademicYearId = yearId,
                    EmployeeRegisterId = EmployeeId,
                    DateOfAchievement = doa,
                    Achievement = achievement,
                    Descriptions = desc
                };
                db.EmployeeAchievements.Add(add);
                db.SaveChanges();
            }
        }

        public void updateFacultyAchievement(DateTime doa, string achievement, string desc, int AchievementEId)
        {
            var getRow = (from a in db.EmployeeAchievements where a.Id == AchievementEId select a).FirstOrDefault();
            if (getRow != null)
            {
                getRow.DateOfAchievement = doa;
                getRow.Achievement = achievement;
                getRow.Descriptions = desc;
                db.SaveChanges();
            }
        }

        public void delFacultyAchievement(int id)
        {
            var getRow = (from a in db.EmployeeAchievements where a.Id == id select a).FirstOrDefault();
            if (getRow != null)
            {

                db.EmployeeAchievements.Remove(getRow);
                db.SaveChanges();
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}