﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using eAcademy.Models;
using eAcademy.Helpers;
using System.Data.Objects.SqlClient;
using eAcademy.HelperClass;
using System.Text.RegularExpressions;

namespace eAcademy.Services
{
    public class PreAdmissionStudentRegisterServices :IDisposable
    {

        UserActivityHelper useractivity = new UserActivityHelper();
        EacademyEntities dc = new EacademyEntities();
        public PreAdmissionStudentRegister getStudentRegisterRow(int onlineRegId, int admissionId)
        {
            var getRow = (from ac in dc.PreAdmissionStudentRegisters where ac.OnlineRegisterId == onlineRegId && ac.StudentAdmissionId == admissionId select ac).First();
            return getRow;
        }
        public PreAdmissionStudentRegister GetStatusFlag(int OnlineRegid)
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters
                       from b in dc.PreAdmissionOnLineRegisters
                       where b.OnlineRegisterId == OnlineRegid && a.OnlineRegisterId == b.OnlineRegisterId
                       select a).FirstOrDefault();
            return ans;
        }
        public void UpdateStudentTransportDetails(int studentid,bool? transreq,int? designation,int? pickpoint,decimal? feeamount)
        {
            var check=dc.PreAdmissionStudentRegisters.Where(q=>q.StudentAdmissionId==studentid).FirstOrDefault();
            if(check!=null)
            {
                check.TransportRequired = transreq;
                check.TransportDesignation = designation;
                check.TransportPickpoint = pickpoint;
                check.TransportFeeAmount = feeamount;
                dc.SaveChanges();
            }
        }
        public void UpdateStudentHostelDetails(int studentid, bool? hostelreq, int? AccomdationFeeid, int? AccomdationSubFeeid, int? FoodFeeid, int? FoodSubFeeid)
        {
            var check = dc.PreAdmissionStudentRegisters.Where(q => q.StudentAdmissionId == studentid).FirstOrDefault();
            if (check != null)
            {
                check.HostelRequired = hostelreq;
                check.AccommodationFeeCategoryId = AccomdationFeeid;
                check.AccommodationSubFeeCategoryId = AccomdationSubFeeid;
                check.FoodFeeCategoryId = FoodFeeid;
                check.FoodSubFeeCategoryId = FoodSubFeeid;
                dc.SaveChanges();
            }
        }
        public bool GetStudentDetailsExist(int primaryuserId,string studentFirstName,string StudentLastName,DateTime? dob)
        {
            var list = (from a in dc.PreAdmissionPrimaryUserRegisters
                        from b in dc.PreAdmissionTransactions
                        from c in dc.PreAdmissionStudentRegisters
                        where a.PrimaryUserAdmissionId == primaryuserId && b.PrimaryUserAdmissionId == a.PrimaryUserAdmissionId && c.StudentAdmissionId == b.StudentAdmissionId && c.StuFirstName == studentFirstName && c.StuLastname == StudentLastName && c.DOB == dob
                        select c).FirstOrDefault();
            if(list!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool GetStudentDetailsExist(int primaryuserId,int studentAdmissionId, string studentFirstName, string StudentLastName, DateTime? dob)
        {
            var list = (from a in dc.PreAdmissionPrimaryUserRegisters
                        from b in dc.PreAdmissionTransactions
                        from c in dc.PreAdmissionStudentRegisters
                        where a.PrimaryUserAdmissionId == primaryuserId && b.PrimaryUserAdmissionId == a.PrimaryUserAdmissionId && b.StudentAdmissionId!=studentAdmissionId && c.StudentAdmissionId == b.StudentAdmissionId && c.StuFirstName == studentFirstName && c.StuLastname == StudentLastName && c.DOB == dob
                        select c).FirstOrDefault();
            if (list != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<M_PreviousApplicationStatus> GetPrevivousApplication(int OnlineRegid)
        {
            SchoolSettingServices ss = new SchoolSettingServices();
            var formtype = ss.getSchoolSettings();
            string formformat = "";
            if (formtype.FormFormat == "Single Form")
            {
                formformat = "AdmissionForm1";
            }
            else
            {
                formformat = "AdmissionForm4";
            }
            var ans = (from a in dc.PreAdmissionPrimaryUserRegisters
                       from b in dc.PreAdmissionTransactions
                       from c in dc.PreAdmissionStudentRegisters
                       from d in dc.AcademicYears
                       from e in dc.Classes
                       where a.OnlineRegisterId == OnlineRegid && b.PrimaryUserAdmissionId == a.PrimaryUserAdmissionId && c.StudentAdmissionId == b.StudentAdmissionId && d.AcademicYearId == c.AcademicyearId && e.ClassId == c.AdmissionClass
                       select new M_PreviousApplicationStatus
                       {
                           ApplicationRegisterNumber = c.StudentAdmissionId,
                           PreStudentName = c.StuFirstName + " " + c.StuLastname,
                           PreAcademicYear = d.AcademicYear1,
                           PreApplyClass = e.ClassType,
                           PreApplicationStatus = c.Statusflag,
                           ApplicationFormat = formformat,
                           Oid = a.OnlineRegisterId,//QSCrypt.Encrypt(a.OnlineRegisterId.ToString()),
                           StudentRegId = c.StudentAdmissionId,

                           PrimaryuserId=a.PrimaryUserAdmissionId,

                           Priadmission_id = a.PrimaryUserAdmissionId
                       }).Distinct().OrderBy(q=>q.ApplicationRegisterNumber).ToList().
                       Select(s => new M_PreviousApplicationStatus
                         {
                             ApplicationRegisterNumber = s.ApplicationRegisterNumber,
                             PreStudentName = s.PreStudentName,
                             ApplicationFormat = s.ApplicationFormat,
                             PreAcademicYear = s.PreAcademicYear,
                             PreApplyClass = s.PreApplyClass,
                             PreApplicationStatus = s.PreApplicationStatus,
                             OnlnlineId = QSCrypt.Encrypt(s.Oid.ToString()),
                             stuid = QSCrypt.Encrypt(s.StudentRegId.ToString()),
                             StudentRegId = s.StudentRegId,
                             PrimaryUserAdmissionId = QSCrypt.Encrypt(s.PrimaryuserId.ToString()),
                             priAdmissionid = QSCrypt.Encrypt(s.Priadmission_id.ToString())
                         }).ToList();
            
            
            
            return ans;
        }
        public List<M_PreviousApplicationStatus> GetPrevivousApplicationOffline(string email)
        {
            string MatchEmailPattern =
                       @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
            string uname = "";
            if (Regex.IsMatch(email, MatchEmailPattern))
            {
                var ans = (from a in dc.PreAdmissionPrimaryUserRegisters
                           from b in dc.PreAdmissionTransactions
                           from c in dc.PreAdmissionStudentRegisters
                           from d in dc.AcademicYears
                           from e in dc.Classes
                           where a.PrimaryUserEmail == email && b.PrimaryUserAdmissionId == a.PrimaryUserAdmissionId && c.StudentAdmissionId == b.StudentAdmissionId && d.AcademicYearId == c.AcademicyearId && e.ClassId == c.AdmissionClass
                           select new M_PreviousApplicationStatus
                           {
                               ApplicationRegisterNumber = c.StudentAdmissionId,
                               PreStudentName = c.StuFirstName + " " + c.StuLastname,
                               PreAcademicYear = d.AcademicYear1,
                               PreApplyClass = e.ClassType,
                               PreApplicationStatus = c.Statusflag,
                               OfflineApplicationNumber = c.OfflineApplicationID,
                               Oid = a.OnlineRegisterId,
                               StudentRegId = c.StudentAdmissionId
                           }).Distinct().ToList().
                           Select(s => new M_PreviousApplicationStatus
                           {
                               ApplicationRegisterNumber = s.ApplicationRegisterNumber,
                               PreStudentName = s.PreStudentName,
                               OfflineApplicationNumber = s.OfflineApplicationNumber,
                               PreAcademicYear = s.PreAcademicYear,
                               PreApplyClass = s.PreApplyClass,
                               PreApplicationStatus = s.PreApplicationStatus,
                               OnlnlineId = QSCrypt.Encrypt(s.Oid.ToString()),
                               stuid = QSCrypt.Encrypt(s.StudentRegId.ToString()),
                               StudentRegId = s.StudentRegId
                           }).ToList();
                return ans;
            }
            else
            {
                long contat = Convert.ToInt64(email);
                var ans = (from a in dc.PreAdmissionPrimaryUserRegisters
                           from b in dc.PreAdmissionTransactions
                           from c in dc.PreAdmissionStudentRegisters
                           from d in dc.AcademicYears
                           from e in dc.Classes
                           where a.PrimaryUserContactNo == contat && b.PrimaryUserAdmissionId == a.PrimaryUserAdmissionId && c.StudentAdmissionId == b.StudentAdmissionId && d.AcademicYearId == c.AcademicyearId && e.ClassId == c.AdmissionClass
                           select new M_PreviousApplicationStatus
                           {
                               ApplicationRegisterNumber = c.StudentAdmissionId,
                               PreStudentName = c.StuFirstName + " " + c.StuLastname,
                               PreAcademicYear = d.AcademicYear1,
                               PreApplyClass = e.ClassType,
                               PreApplicationStatus = c.Statusflag,
                               OfflineApplicationNumber = c.OfflineApplicationID,
                               Oid = a.OnlineRegisterId,
                               StudentRegId = c.StudentAdmissionId
                           }).Distinct().ToList().
                          Select(s => new M_PreviousApplicationStatus
                          {
                              ApplicationRegisterNumber = s.ApplicationRegisterNumber,
                              PreStudentName = s.PreStudentName,
                              OfflineApplicationNumber = s.OfflineApplicationNumber,
                              PreAcademicYear = s.PreAcademicYear,
                              PreApplyClass = s.PreApplyClass,
                              PreApplicationStatus = s.PreApplicationStatus,
                              OnlnlineId = QSCrypt.Encrypt(s.Oid.ToString()),
                              stuid = QSCrypt.Encrypt(s.StudentRegId.ToString()),
                              StudentRegId = s.StudentRegId
                          }).ToList();
                return ans;
            }
        }

        public List<M_StudentRegisteration> findParticularStudentDetails(int Oid, int Sid)
        {
            List<M_StudentRegisteration> SS = new List<M_StudentRegisteration>();
            
            var PrimaryUserId = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.OnlineRegisterId == Oid).ToList();
            if (PrimaryUserId.Count != 0)
            {
                if (PrimaryUserId.FirstOrDefault().PrimaryUserAdmissionId != null)
                {
                    int pid=PrimaryUserId.FirstOrDefault().PrimaryUserAdmissionId;
                    var Trans = dc.PreAdmissionTransactions.Where(q => q.PrimaryUserAdmissionId.Value.Equals(pid) && q.StudentAdmissionId == Sid).OrderByDescending(q => q.PreAdmissionTransactionId).FirstOrDefault();
                    if (Sid==0)
                    {
                         Trans = dc.PreAdmissionTransactions.Where(q => q.PrimaryUserAdmissionId.Value.Equals(pid)).OrderByDescending(q => q.PreAdmissionTransactionId).FirstOrDefault();
                    }
                    else
                    {
                         Trans = dc.PreAdmissionTransactions.Where(q => q.PrimaryUserAdmissionId.Value.Equals(pid) && q.StudentAdmissionId == Sid).OrderByDescending(q => q.PreAdmissionTransactionId).FirstOrDefault();
                    }
                   
                   if(Sid ==0 || Trans.StudentAdmissionId == null)
                    {
                        if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Father")
                        {

                            if (Trans.MotherAdmissionId == null && Trans.GuardianAdmissionId == null)
                            {
                                var result = OldStudentFatherdetails(pid);
                                SS = result;
                            }
                            else if (Trans.MotherAdmissionId != null && Trans.GuardianAdmissionId == null)
                            {
                                var result = OldStudentFatherMotherdetails(pid);
                          
                                SS = result;
                            }
                            else if (Trans.MotherAdmissionId == null && Trans.GuardianAdmissionId != null)
                            {
                                var result = OldStudentFatherGuardiandetails(pid);
                                SS = result;
                            }
                            else
                            {
                                var result = OldStudentAllUserdetails(pid);
                                SS = result;
                            }

                        }
                        else if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Mother")
                        {
                            if (Trans.FatherAdmissionId == null && Trans.GuardianAdmissionId == null)
                            {
                                var result = OldStudentMotherdetails(pid);
                                SS = result;
                            }
                            else if (Trans.FatherAdmissionId != null && Trans.GuardianAdmissionId == null)
                            {
                                var result = OldStudentFatherMotherdetails(pid);
                                SS = result;
                            }
                            else if (Trans.FatherAdmissionId == null && Trans.GuardianAdmissionId != null)
                            {
                                var result = OldStudentMotherGuardiandetails(pid);
                                SS = result;
                            }
                            else
                            {
                                var result = OldStudentAllUserdetails(pid);
                                SS = result;
                            }

                        }
                        else if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Guardian")
                        {
                            if (Trans.MotherAdmissionId == null && Trans.FatherAdmissionId == null)
                            {
                                var result = OldStudentGuardiandetails(pid);
                                SS = result;
                            }
                            else if (Trans.MotherAdmissionId != null && Trans.FatherAdmissionId == null)
                            {

                                var result = OldStudentMotherGuardiandetails(pid);
                                SS = result;
                            }
                            else if (Trans.MotherAdmissionId == null && Trans.FatherAdmissionId != null)
                            {

                                var result = OldStudentFatherGuardiandetails(pid);
                                SS = result;
                            }
                            else
                            {
                                var result = OldStudentAllUserdetails(pid);
                                SS = result;
                            }

                        }
                      
                    }
                    else if (Trans.StudentAdmissionId != null)
                    {
                        if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Father")
                        {
                            if (Trans.MotherAdmissionId != null && Trans.GuardianAdmissionId != null)
                            {
                                var result = NewStudentAllUserdetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else if(Trans.MotherAdmissionId != null && Trans.GuardianAdmissionId ==null)
                            {
                                var result = NewStudentFatherMotherDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else if (Trans.MotherAdmissionId == null && Trans.GuardianAdmissionId != null)
                            {
                                var result = NewStudentFatherGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else
                            {
                                var result = NewStudentFatherDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                        }
                        else if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Mother")
                        {
                            if (Trans.FatherAdmissionId != null && Trans.GuardianAdmissionId != null)
                            {
                                var result = NewStudentAllUserdetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else if (Trans.FatherAdmissionId != null && Trans.GuardianAdmissionId == null)
                            {
                                var result = NewStudentFatherMotherDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else if (Trans.FatherAdmissionId == null && Trans.GuardianAdmissionId != null)
                            {
                                var result = NewStudentMotherGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else
                            {
                                var result = NewStudentMotherDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                        }
                        else if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Guardian")
                        {
                            if (Trans.FatherAdmissionId != null && Trans.MotherAdmissionId != null)
                            {
                                var result = NewStudentAllUserdetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else if (Trans.FatherAdmissionId != null && Trans.MotherAdmissionId == null)
                            {
                                var result = NewStudentFatherGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else if (Trans.FatherAdmissionId == null && Trans.MotherAdmissionId != null)
                            {
                                var result = NewStudentMotherGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else
                            {
                                var result = NewStudentGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                        }
                    }
                    
                   
                }
                return SS;
            }
            else
            {
                var ans = (from a in dc.PreAdmissionStudentRegisters
                           where a.OnlineRegisterId == 0
                           select new M_StudentRegisteration
              {

              }).ToList();
                return ans;
            }

        }

        public List<M_StudentRegisteration> NewStudentAllUserdetails(int PrimaryuserId,int StudentId)
        {
            var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                                              from trans in dc.PreAdmissionTransactions
                                              from stu in dc.PreAdmissionStudentRegisters
                                              from fa in dc.PreAdmissionFatherRegisters
                                              from mo in dc.PreAdmissionMotherRegisters
                                              from gu in dc.PreAdmissionGuardianRegisters
                                              from f in dc.BoardofSchools
                       where trans.PrimaryUserAdmissionId == PrimaryuserId && trans.StudentAdmissionId == StudentId && stu.StudentAdmissionId == trans.StudentAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                                               && fa.FatherAdmissionId == trans.FatherAdmissionId && mo.MotherAdmissionId == trans.MotherAdmissionId && gu.GuardianAdmissionId == trans.GuardianAdmissionId                          //&& stu.PreSchoolMedium==f.BoardId
                                              select new M_StudentRegisteration
                                              {
                                                  Stuadmissionid = stu.StudentAdmissionId,
                                                  StuFirstName = stu.StuFirstName,
                                                  StuMiddlename = stu.StuMiddlename,
                                                  StuLastname = stu.StuLastname,
                                                  Gender = stu.Gender,
                                                  PlaceOfBirth = stu.PlaceOfBirth,
                                                  Community = stu.Community,
                                                  Religion = stu.Religion,
                                                  Nationality = stu.Nationality,
                                                  BloodGroup = stu.BloodGroup,
                                                  txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),
                                                  txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),
                                                  txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),
                                                  txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                                                  txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreSchoolFromdate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolFromdate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolFromdate),
                                                  txt_PreToDate = SqlFunctions.DateName("day", stu.PreSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolToDate),      
                                                  AdmissionClass = stu.AdmissionClass,
                                                  FatherFirstName = fa.FatherFirstName,
                                                  FatherOccupation = fa.FatherOccupation,
                                                  FatherMobileNo = fa.FatherMobileNo,
                                                  FatherEmail = fa.FatherEmail,
                                                  MotherFirstname = mo.MotherFirstname,
                                                  MotherOccupation = mo.MotherOccupation,
                                                  MotherMobileNo = mo.MotherMobileNo,
                                                  MotherEmail = mo.MotherEmail,
                                                  UserCompanyname = Priuser.UserCompanyname,
                                                  CompanyAddress1 = Priuser.CompanyAddress1,
                                                  CompanyAddress2 = Priuser.CompanyAddress2,
                                                  CompanyCity = Priuser.CompanyCity,
                                                  CompanyState = Priuser.CompanyState,
                                                  CompanyCountry = Priuser.CompanyCountry,
                                                  CompanyPostelcode = Priuser.CompanyPostelcode,
                                                  CompanyContact = Priuser.CompanyContact,
                                                  GuardianFirstname = gu.GuardianFirstname,
                                                  GuardianLastName = gu.GuardianLastName,
                                                  GuRelationshipToChild = gu.GuRelationshipToChild,
                                                  GuardianDOB = gu.GuardianDOB,
                                                  GuardianGender = gu.GuardianGender,
                                                  GuardianQualification = gu.GuardianQualification,
                                                  GuardianMobileNo = gu.GuardianMobileNo,
                                                  GuardianEmail = gu.GuardianEmail,
                                                  GuardianOccupation = gu.GuardianOccupation,
                                                  GuardianIncome = gu.GuardianIncome,
                                                  LocAddress1 = stu.LocAddress1,
                                                  LocAddress2 = stu.LocAddress2,
                                                  LocCity = stu.LocCity,
                                                  LocState = stu.LocState,
                                                  LocCountry = stu.LocCountry,
                                                  LocPostelcode = stu.LocPostelcode,
                                                  StudentPhoto = stu.StudentPhoto,
                                                  CommunityCertificate = stu.CommunityCertificate,
                                                  BirthCertificate = stu.BirthCertificate,
                                                  TransferCertificate = stu.TransferCertificate,
                                                  PreSchool = stu.PreSchholName,
                                                  Pre_BoardofSchool=stu.PreSchoolMedium,
                                                  //PreMedium = f.BoardName,
                                                  PreMarks = stu.PreSchoolMark,
                                                  PreClass = stu.PreSchoolClass,
                                                  PreFromDate1 =stu.PreSchoolFromdate,
                                                  PreToDate = stu.PreSchoolToDate,                                                                                         
                                                  NoOfSbling = stu.NoOfSibling,
                                                  PrimaryUser = Priuser.PrimaryUser,
                                                  EmergencyContactPersonName = stu.EmergencyContactPersonName,
                                                  EmergencyContactNumber = stu.EmergencyContactNumber,
                                                  ContactPersonRelationship = stu.ContactPersonRelationship,
                                                  Email = stu.Email,
                                                  Contact = stu.Contact,
                                                  Height = stu.Height,
                                                  Weights = stu.Weights,
                                                  IdentificationMark = stu.IdentificationMark,
                                                  PerAddress1 = stu.PerAddress1,
                                                  PerAddress2 = stu.PerAddress2,
                                                  PerCity = stu.PerCity,
                                                  PerState = stu.PerState,
                                                  PerCountry = stu.PerCountry,
                                                  PerPostelcode = stu.PerPostelcode,
                                                  FatherLastName = fa.FatherLastName,
                                                  MotherLastName = mo.MotherLastName,
                                                  FatherDOB = fa.FatherDOB,
                                                  MotherDOB = mo.MotherDOB,
                                                  TotalIncome = fa.FatherMotherTotalIncome,
                                                  FatherQualification = fa.FatherQualification,
                                                  MotherQualification = mo.MotherQualification,
                                                  GuardianRequried = stu.GuardianRequried,
                                                  IncomeCertificate = stu.IncomeCertificate,
                                                  AcademicyearId = stu.AcademicyearId,
                                                  Distance = stu.Distance,
                                                  primaryUserEmail = Priuser.PrimaryUserEmail,
                                                  WorkSameSchool = Priuser.WorkSameSchool,
                                                  EmployeeDesignationId = Priuser.EmployeeDesignationId,
                                                  EmployeeId = Priuser.EmployeeId,
                                                  EmailRequired=Priuser.EmailRequired,
                                                  SmsRequired=Priuser.SmsRequired,
                                                  TransportRequired=stu.TransportRequired,
                                                  TransportDestination=stu.TransportDesignation,
                                                  TransportPickpoint=stu.TransportPickpoint,
                                                  TransportFeeAmount=stu.TransportFeeAmount,
                                                  HostelRequired=stu.HostelRequired,
                                                  AccommodationFeeCategoryId=stu.AccommodationFeeCategoryId,
                                                  AccommodationSubFeeCategoryId=stu.AccommodationSubFeeCategoryId,
                                                  FoodFeeCategoryId=stu.FoodFeeCategoryId,
                                                  FoodSubFeeCategoryId=stu.FoodSubFeeCategoryId,
                                                  primaryUserContact=Priuser.PrimaryUserContactNo
                                              }).Distinct().ToList();
            return res;
        }


        public List<M_StudentRegisteration> NewStudentFatherMotherDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                       from trans in dc.PreAdmissionTransactions
                       from stu in dc.PreAdmissionStudentRegisters
                       from fa in dc.PreAdmissionFatherRegisters
                       from mo in dc.PreAdmissionMotherRegisters
                       from f in dc.BoardofSchools
                       where trans.PrimaryUserAdmissionId == PrimaryuserId && trans.StudentAdmissionId == StudentId && stu.StudentAdmissionId == trans.StudentAdmissionId
                        && fa.FatherAdmissionId == trans.FatherAdmissionId && mo.MotherAdmissionId == trans.MotherAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                       select new M_StudentRegisteration
                       {
                           Stuadmissionid = stu.StudentAdmissionId,
                           StuFirstName = stu.StuFirstName,
                           StuMiddlename = stu.StuMiddlename,
                           StuLastname = stu.StuLastname,
                           Gender = stu.Gender,                           
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.Community,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.BloodGroup,
                           txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),                                                    
                           txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),                           
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreSchoolFromdate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolFromdate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolFromdate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolToDate),      
                           AdmissionClass = stu.AdmissionClass,
                           FatherFirstName = fa.FatherFirstName,
                           FatherOccupation = fa.FatherOccupation,
                           FatherMobileNo = fa.FatherMobileNo,
                           FatherEmail = fa.FatherEmail,
                           MotherFirstname = mo.MotherFirstname,
                           MotherOccupation = mo.MotherOccupation,
                           MotherMobileNo = mo.MotherMobileNo,
                           MotherEmail = mo.MotherEmail,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           LocAddress1 = stu.LocAddress1,
                           LocAddress2 = stu.LocAddress2,
                           LocCity = stu.LocCity,
                           LocState = stu.LocState,
                           LocCountry = stu.LocCountry,
                           LocPostelcode = stu.LocPostelcode,
                           StudentPhoto = stu.StudentPhoto,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreSchholName,
                           Pre_BoardofSchool = stu.PreSchoolMedium,
                          // PreMedium=f.BoardName,
                           PreMarks = stu.PreSchoolMark,
                           PreClass = stu.PreSchoolClass,
                           PreFromDate1 = stu.PreSchoolFromdate,
                           PreToDate = stu.PreSchoolToDate,                       
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPersonName,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PerAddress1,
                           PerAddress2 = stu.PerAddress2,
                           PerCity = stu.PerCity,
                           PerState = stu.PerState,
                           PerCountry = stu.PerCountry,
                           PerPostelcode = stu.PerPostelcode,
                           FatherLastName = fa.FatherLastName,
                           MotherLastName = mo.MotherLastName,
                           FatherDOB = fa.FatherDOB,
                           MotherDOB = mo.MotherDOB,
                           TotalIncome = fa.FatherMotherTotalIncome,
                           FatherQualification = fa.FatherQualification,
                           MotherQualification = mo.MotherQualification,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           GuardianRequried=stu.GuardianRequried,
                           WorkSameSchool = Priuser.WorkSameSchool,
                           EmployeeDesignationId = Priuser.EmployeeDesignationId,
                           EmployeeId = Priuser.EmployeeId,
                           EmailRequired = Priuser.EmailRequired,
                           SmsRequired = Priuser.SmsRequired,
                           TransportRequired = stu.TransportRequired,
                           TransportDestination = stu.TransportDesignation,
                           TransportPickpoint = stu.TransportPickpoint,
                           TransportFeeAmount = stu.TransportFeeAmount,
                           HostelRequired = stu.HostelRequired,
                           AccommodationFeeCategoryId = stu.AccommodationFeeCategoryId,
                           AccommodationSubFeeCategoryId = stu.AccommodationSubFeeCategoryId,
                           FoodFeeCategoryId = stu.FoodFeeCategoryId,
                           FoodSubFeeCategoryId = stu.FoodSubFeeCategoryId,
                           primaryUserContact = Priuser.PrimaryUserContactNo
                       }).Distinct().ToList();
            return res;
        }

        public List<M_StudentRegisteration> NewStudentFatherGuardianDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                       from trans in dc.PreAdmissionTransactions
                       from stu in dc.PreAdmissionStudentRegisters
                       from fa in dc.PreAdmissionFatherRegisters
                       from f in dc.BoardofSchools
                       from gu in dc.PreAdmissionGuardianRegisters
                       where trans.PrimaryUserAdmissionId == PrimaryuserId && trans.StudentAdmissionId == StudentId && stu.StudentAdmissionId == trans.StudentAdmissionId
                        && fa.FatherAdmissionId == trans.FatherAdmissionId && gu.GuardianAdmissionId == trans.GuardianAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId               //&& f.BoardId == stu.PreSchoolMedium
                       select new M_StudentRegisteration
                       {
                           Stuadmissionid = stu.StudentAdmissionId,
                           StuFirstName = stu.StuFirstName,
                           StuMiddlename = stu.StuMiddlename,
                           StuLastname = stu.StuLastname,
                           Gender = stu.Gender,
                           txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),                           
                           txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreSchoolFromdate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolFromdate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolFromdate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolToDate),      
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.Community,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.BloodGroup,
                           AdmissionClass = stu.AdmissionClass,
                           FatherFirstName = fa.FatherFirstName,
                           FatherOccupation = fa.FatherOccupation,
                           FatherMobileNo = fa.FatherMobileNo,
                           FatherEmail = fa.FatherEmail,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           GuardianFirstname = gu.GuardianFirstname,
                           GuardianLastName = gu.GuardianLastName,
                           GuRelationshipToChild = gu.GuRelationshipToChild,
                           GuardianDOB = gu.GuardianDOB,
                           GuardianGender = gu.GuardianGender,
                           GuardianQualification = gu.GuardianQualification,
                           GuardianMobileNo = gu.GuardianMobileNo,
                           GuardianEmail = gu.GuardianEmail,
                           GuardianOccupation = gu.GuardianOccupation,
                           GuardianIncome = gu.GuardianIncome,
                           LocAddress1 = stu.LocAddress1,
                           LocAddress2 = stu.LocAddress2,
                           LocCity = stu.LocCity,
                           LocState = stu.LocState,
                           LocCountry = stu.LocCountry,
                           LocPostelcode = stu.LocPostelcode,
                           StudentPhoto = stu.StudentPhoto,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreSchholName,
                           //PreMedium = f.BoardName,
                           Pre_BoardofSchool = stu.PreSchoolMedium,
                           PreMarks = stu.PreSchoolMark,
                           PreClass = stu.PreSchoolClass,
                           PreFromDate1 = stu.PreSchoolFromdate,
                           PreToDate = stu.PreSchoolToDate,                     
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPersonName,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PerAddress1,
                           PerAddress2 = stu.PerAddress2,
                           PerCity = stu.PerCity,
                           PerState = stu.PerState,
                           PerCountry = stu.PerCountry,
                           PerPostelcode = stu.PerPostelcode,
                           FatherLastName = fa.FatherLastName,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           FatherDOB = fa.FatherDOB,                        
                           TotalIncome = fa.FatherMotherTotalIncome,
                           FatherQualification = fa.FatherQualification,
                           GuardianRequried = stu.GuardianRequried,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           WorkSameSchool = Priuser.WorkSameSchool,
                           EmployeeDesignationId = Priuser.EmployeeDesignationId,
                           EmployeeId = Priuser.EmployeeId,
                           EmailRequired = Priuser.EmailRequired,
                           SmsRequired = Priuser.SmsRequired,
                           TransportRequired = stu.TransportRequired,
                           TransportDestination = stu.TransportDesignation,
                           TransportPickpoint = stu.TransportPickpoint,
                           TransportFeeAmount = stu.TransportFeeAmount,
                           HostelRequired = stu.HostelRequired,
                           AccommodationFeeCategoryId = stu.AccommodationFeeCategoryId,
                           AccommodationSubFeeCategoryId = stu.AccommodationSubFeeCategoryId,
                           FoodFeeCategoryId = stu.FoodFeeCategoryId,
                           FoodSubFeeCategoryId = stu.FoodSubFeeCategoryId,
                           primaryUserContact = Priuser.PrimaryUserContactNo
                       }).Distinct().ToList();
            return res;
        }

        public List<M_StudentRegisteration> NewStudentMotherGuardianDetails(int PrimaryuserId, int StudentId)
          {
              var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                         from trans in dc.PreAdmissionTransactions
                         from stu in dc.PreAdmissionStudentRegisters                      
                         from mo in dc.PreAdmissionMotherRegisters
                         from gu in dc.PreAdmissionGuardianRegisters
                         from f in dc.BoardofSchools
                         where trans.PrimaryUserAdmissionId == PrimaryuserId && trans.StudentAdmissionId == StudentId && stu.StudentAdmissionId == trans.StudentAdmissionId
                         && mo.MotherAdmissionId == trans.MotherAdmissionId && gu.GuardianAdmissionId == trans.GuardianAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId              //&& f.BoardId == stu.PreSchoolMedium
                         select new M_StudentRegisteration
                         {
                             Stuadmissionid = stu.StudentAdmissionId,
                             StuFirstName = stu.StuFirstName,
                             StuMiddlename = stu.StuMiddlename,
                             StuLastname = stu.StuLastname,
                             Gender = stu.Gender,
                             txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),
                             txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),                             
                             txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                             txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreSchoolFromdate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolFromdate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolFromdate),
                             txt_PreToDate = SqlFunctions.DateName("day", stu.PreSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolToDate),      
                             PlaceOfBirth = stu.PlaceOfBirth,
                             Community = stu.Community,
                             Religion = stu.Religion,
                             Nationality = stu.Nationality,
                             BloodGroup = stu.BloodGroup,
                             AdmissionClass = stu.AdmissionClass,
                             MotherFirstname = mo.MotherFirstname,
                             MotherOccupation = mo.MotherOccupation,
                             MotherMobileNo = mo.MotherMobileNo,
                             MotherEmail = mo.MotherEmail,
                             UserCompanyname = Priuser.UserCompanyname,
                             CompanyAddress1 = Priuser.CompanyAddress1,
                             CompanyAddress2 = Priuser.CompanyAddress2,
                             CompanyCity = Priuser.CompanyCity,
                             CompanyState = Priuser.CompanyState,
                             CompanyCountry = Priuser.CompanyCountry,
                             CompanyPostelcode = Priuser.CompanyPostelcode,
                             CompanyContact = Priuser.CompanyContact,
                             GuardianFirstname = gu.GuardianFirstname,
                             GuardianLastName = gu.GuardianLastName,
                             GuRelationshipToChild = gu.GuRelationshipToChild,
                             GuardianDOB = gu.GuardianDOB,
                             GuardianGender = gu.GuardianGender,
                             GuardianQualification = gu.GuardianQualification,
                             GuardianMobileNo = gu.GuardianMobileNo,
                             GuardianEmail = gu.GuardianEmail,
                             GuardianOccupation = gu.GuardianOccupation,
                             GuardianIncome = gu.GuardianIncome,
                             LocAddress1 = stu.LocAddress1,
                             LocAddress2 = stu.LocAddress2,
                             LocCity = stu.LocCity,
                             LocState = stu.LocState,
                             LocCountry = stu.LocCountry,
                             LocPostelcode = stu.LocPostelcode,
                             StudentPhoto = stu.StudentPhoto,
                             CommunityCertificate = stu.CommunityCertificate,
                             BirthCertificate = stu.BirthCertificate,
                             TransferCertificate = stu.TransferCertificate,
                             PreSchool = stu.PreSchholName,
                            // PreMedium = f.BoardName,
                             Pre_BoardofSchool = stu.PreSchoolMedium,
                             PreMarks = stu.PreSchoolMark,
                             PreClass = stu.PreSchoolClass,
                             PreFromDate1 = stu.PreSchoolFromdate,
                             PreToDate = stu.PreSchoolToDate,                          
                             NoOfSbling = stu.NoOfSibling,
                             PrimaryUser = Priuser.PrimaryUser,
                             EmergencyContactPersonName = stu.EmergencyContactPersonName,
                             EmergencyContactNumber = stu.EmergencyContactNumber,
                             ContactPersonRelationship = stu.ContactPersonRelationship,
                             Email = stu.Email,
                             Contact = stu.Contact,
                             Height = stu.Height,
                             Weights = stu.Weights,
                             IdentificationMark = stu.IdentificationMark,
                             PerAddress1 = stu.PerAddress1,
                             PerAddress2 = stu.PerAddress2,
                             PerCity = stu.PerCity,
                             PerState = stu.PerState,
                             PerCountry = stu.PerCountry,
                             PerPostelcode = stu.PerPostelcode,
                             MotherLastName = mo.MotherLastName,
                             MotherDOB = mo.MotherDOB,
                             TotalIncome = mo.FatherMotherTotalIncome,
                             GuardianRequried = stu.GuardianRequried,
                             MotherQualification = mo.MotherQualification,
                             primaryUserEmail = Priuser.PrimaryUserEmail,
                             IncomeCertificate = stu.IncomeCertificate,
                             AcademicyearId = stu.AcademicyearId,
                             Distance = stu.Distance,
                             WorkSameSchool = Priuser.WorkSameSchool,
                             EmployeeDesignationId = Priuser.EmployeeDesignationId,
                             EmployeeId = Priuser.EmployeeId,
                             EmailRequired = Priuser.EmailRequired,
                             SmsRequired = Priuser.SmsRequired,
                             TransportRequired = stu.TransportRequired,
                             TransportDestination = stu.TransportDesignation,
                             TransportPickpoint = stu.TransportPickpoint,
                             TransportFeeAmount = stu.TransportFeeAmount,
                             HostelRequired = stu.HostelRequired,
                             AccommodationFeeCategoryId = stu.AccommodationFeeCategoryId,
                             AccommodationSubFeeCategoryId = stu.AccommodationSubFeeCategoryId,
                             FoodFeeCategoryId = stu.FoodFeeCategoryId,
                             FoodSubFeeCategoryId = stu.FoodSubFeeCategoryId,
                             primaryUserContact = Priuser.PrimaryUserContactNo
                         }).Distinct().ToList();
              return res;
          }

        public List<M_StudentRegisteration> NewStudentFatherDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                       from trans in dc.PreAdmissionTransactions
                       from stu in dc.PreAdmissionStudentRegisters
                       from fa in dc.PreAdmissionFatherRegisters
                       from f in dc.BoardofSchools
                       where trans.PrimaryUserAdmissionId == PrimaryuserId && trans.StudentAdmissionId == StudentId && stu.StudentAdmissionId == trans.StudentAdmissionId
                        && fa.FatherAdmissionId == trans.FatherAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                       select new M_StudentRegisteration
                       {
                           Stuadmissionid = stu.StudentAdmissionId,
                           StuFirstName = stu.StuFirstName,
                           StuMiddlename = stu.StuMiddlename,
                           StuLastname = stu.StuLastname,
                           Gender = stu.Gender,                          
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.Community,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.BloodGroup,
                           txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreSchoolFromdate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolFromdate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolFromdate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolToDate),      
                           AdmissionClass = stu.AdmissionClass,
                           FatherFirstName = fa.FatherFirstName,
                           FatherOccupation = fa.FatherOccupation,
                           FatherMobileNo = fa.FatherMobileNo,
                           FatherEmail = fa.FatherEmail,                        
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,                          
                           LocAddress1 = stu.LocAddress1,
                           LocAddress2 = stu.LocAddress2,
                           LocCity = stu.LocCity,
                           LocState = stu.LocState,
                           LocCountry = stu.LocCountry,
                           LocPostelcode = stu.LocPostelcode,
                           StudentPhoto = stu.StudentPhoto,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreSchholName,
                           //PreMedium = f.BoardName,
                           Pre_BoardofSchool = stu.PreSchoolMedium,
                           PreMarks = stu.PreSchoolMark,
                           PreClass = stu.PreSchoolClass,
                           PreFromDate1 = stu.PreSchoolFromdate,
                           PreToDate = stu.PreSchoolToDate,
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPersonName,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PerAddress1,
                           PerAddress2 = stu.PerAddress2,
                           PerCity = stu.PerCity,
                           PerState = stu.PerState,
                           PerCountry = stu.PerCountry,
                           PerPostelcode = stu.PerPostelcode,
                           FatherLastName = fa.FatherLastName,
                           FatherDOB = fa.FatherDOB,
                           GuardianRequried = stu.GuardianRequried,
                          primaryUserEmail=Priuser.PrimaryUserEmail,
                           TotalIncome = fa.FatherMotherTotalIncome,
                           FatherQualification = fa.FatherQualification,                         
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           WorkSameSchool = Priuser.WorkSameSchool,
                           EmployeeDesignationId = Priuser.EmployeeDesignationId,
                           EmployeeId = Priuser.EmployeeId,
                           EmailRequired = Priuser.EmailRequired,
                           SmsRequired = Priuser.SmsRequired,
                           TransportRequired = stu.TransportRequired,
                           TransportDestination = stu.TransportDesignation,
                           TransportPickpoint = stu.TransportPickpoint,
                           TransportFeeAmount = stu.TransportFeeAmount,
                           HostelRequired = stu.HostelRequired,
                           AccommodationFeeCategoryId = stu.AccommodationFeeCategoryId,
                           AccommodationSubFeeCategoryId = stu.AccommodationSubFeeCategoryId,
                           FoodFeeCategoryId = stu.FoodFeeCategoryId,
                           FoodSubFeeCategoryId = stu.FoodSubFeeCategoryId,
                           primaryUserContact = Priuser.PrimaryUserContactNo
                       }).Distinct().ToList();
            return res;
        }
        public List<M_StudentRegisteration> NewStudentMotherDetails(int PrimaryuserId, int StudentId)
         {
             var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                        from trans in dc.PreAdmissionTransactions
                        from stu in dc.PreAdmissionStudentRegisters                      
                        from mo in dc.PreAdmissionMotherRegisters
                        from f in dc.BoardofSchools
                        where trans.PrimaryUserAdmissionId == PrimaryuserId && trans.StudentAdmissionId == StudentId && stu.StudentAdmissionId == trans.StudentAdmissionId
                         && mo.MotherAdmissionId == trans.MotherAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                        select new M_StudentRegisteration
                        {
                            Stuadmissionid = stu.StudentAdmissionId,
                            StuFirstName = stu.StuFirstName,
                            StuMiddlename = stu.StuMiddlename,
                            StuLastname = stu.StuLastname,
                            Gender = stu.Gender,
                            txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" +SqlFunctions.DateName("year", mo.MotherDOB)  ,                          
                            txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),                           
                            txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreSchoolFromdate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolFromdate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolFromdate),
                            txt_PreToDate = SqlFunctions.DateName("day", stu.PreSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolToDate),                                    
                            PlaceOfBirth = stu.PlaceOfBirth,
                            Community = stu.Community,
                            Religion = stu.Religion,
                            Nationality = stu.Nationality,
                            BloodGroup = stu.BloodGroup,
                            AdmissionClass = stu.AdmissionClass,                          
                            MotherFirstname = mo.MotherFirstname,
                            MotherOccupation = mo.MotherOccupation,
                            MotherMobileNo = mo.MotherMobileNo,
                            MotherEmail = mo.MotherEmail,
                            UserCompanyname = Priuser.UserCompanyname,
                            CompanyAddress1 = Priuser.CompanyAddress1,
                            CompanyAddress2 = Priuser.CompanyAddress2,
                            CompanyCity = Priuser.CompanyCity,
                            CompanyState = Priuser.CompanyState,
                            CompanyCountry = Priuser.CompanyCountry,
                            CompanyPostelcode = Priuser.CompanyPostelcode,
                            CompanyContact = Priuser.CompanyContact,                          
                            LocAddress1 = stu.LocAddress1,
                            LocAddress2 = stu.LocAddress2,
                            LocCity = stu.LocCity,
                            LocState = stu.LocState,
                            LocCountry = stu.LocCountry,
                            LocPostelcode = stu.LocPostelcode,
                            StudentPhoto = stu.StudentPhoto,
                            CommunityCertificate = stu.CommunityCertificate,
                            BirthCertificate = stu.BirthCertificate,
                            TransferCertificate = stu.TransferCertificate,
                            PreSchool = stu.PreSchholName,
                            //PreMedium = f.BoardName,
                            Pre_BoardofSchool = stu.PreSchoolMedium,
                            PreMarks = stu.PreSchoolMark,
                            PreClass = stu.PreSchoolClass,
                            PreFromDate1 = stu.PreSchoolFromdate,
                            PreToDate = stu.PreSchoolToDate,
                            NoOfSbling = stu.NoOfSibling,
                            PrimaryUser = Priuser.PrimaryUser,
                            EmergencyContactPersonName = stu.EmergencyContactPersonName,
                            EmergencyContactNumber = stu.EmergencyContactNumber,
                            ContactPersonRelationship = stu.ContactPersonRelationship,
                            Email = stu.Email,
                            Contact = stu.Contact,
                            Height = stu.Height,
                            Weights = stu.Weights,
                            IdentificationMark = stu.IdentificationMark,
                            PerAddress1 = stu.PerAddress1,
                            PerAddress2 = stu.PerAddress2,
                            PerCity = stu.PerCity,
                            PerState = stu.PerState,
                            PerCountry = stu.PerCountry,
                            PerPostelcode = stu.PerPostelcode,                         
                            MotherLastName = mo.MotherLastName,                         
                            MotherDOB = mo.MotherDOB,
                            TotalIncome = mo.FatherMotherTotalIncome,                         
                            MotherQualification = mo.MotherQualification,                         
                            IncomeCertificate = stu.IncomeCertificate,
                            AcademicyearId = stu.AcademicyearId,
                            Distance = stu.Distance,
                            primaryUserEmail = Priuser.PrimaryUserEmail,
                            GuardianRequried = stu.GuardianRequried,
                            WorkSameSchool = Priuser.WorkSameSchool,
                            EmployeeDesignationId = Priuser.EmployeeDesignationId,
                            EmployeeId = Priuser.EmployeeId,
                            EmailRequired = Priuser.EmailRequired,
                            SmsRequired = Priuser.SmsRequired,
                            TransportRequired = stu.TransportRequired,
                            TransportDestination = stu.TransportDesignation,
                            TransportPickpoint = stu.TransportPickpoint,
                            TransportFeeAmount = stu.TransportFeeAmount,
                            HostelRequired = stu.HostelRequired,
                            AccommodationFeeCategoryId = stu.AccommodationFeeCategoryId,
                            AccommodationSubFeeCategoryId = stu.AccommodationSubFeeCategoryId,
                            FoodFeeCategoryId = stu.FoodFeeCategoryId,
                            FoodSubFeeCategoryId = stu.FoodSubFeeCategoryId,
                            primaryUserContact = Priuser.PrimaryUserContactNo
                        }).Distinct().ToList();
             return res;
         }
        public List<M_StudentRegisteration> NewStudentGuardianDetails(int PrimaryuserId, int StudentId)
         {
             var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                        from trans in dc.PreAdmissionTransactions
                        from stu in dc.PreAdmissionStudentRegisters                      
                        from gu in dc.PreAdmissionGuardianRegisters
                        from f in dc.BoardofSchools
                        where trans.PrimaryUserAdmissionId == PrimaryuserId && trans.StudentAdmissionId == StudentId && stu.StudentAdmissionId == trans.StudentAdmissionId
                         && gu.GuardianAdmissionId == trans.GuardianAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                        select new M_StudentRegisteration
                        {
                            Stuadmissionid = stu.StudentAdmissionId,
                            StuFirstName = stu.StuFirstName,
                            StuMiddlename = stu.StuMiddlename,
                            StuLastname = stu.StuLastname,
                            Gender = stu.Gender,
                            PlaceOfBirth = stu.PlaceOfBirth,
                            Community = stu.Community,
                            Religion = stu.Religion,
                            Nationality = stu.Nationality,
                            BloodGroup = stu.BloodGroup,
                            AdmissionClass = stu.AdmissionClass,                            
                            txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                            txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),
                            txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreSchoolFromdate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolFromdate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolFromdate),
                            txt_PreToDate = SqlFunctions.DateName("day", stu.PreSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolToDate),                                    
                            UserCompanyname = Priuser.UserCompanyname,
                            CompanyAddress1 = Priuser.CompanyAddress1,
                            CompanyAddress2 = Priuser.CompanyAddress2,
                            CompanyCity = Priuser.CompanyCity,
                            CompanyState = Priuser.CompanyState,
                            CompanyCountry = Priuser.CompanyCountry,
                            CompanyPostelcode = Priuser.CompanyPostelcode,
                            CompanyContact = Priuser.CompanyContact,
                            GuardianFirstname = gu.GuardianFirstname,
                            GuardianLastName = gu.GuardianLastName,
                            GuRelationshipToChild = gu.GuRelationshipToChild,
                            GuardianDOB = gu.GuardianDOB,
                            GuardianGender = gu.GuardianGender,
                            GuardianQualification = gu.GuardianQualification,
                            GuardianMobileNo = gu.GuardianMobileNo,
                            GuardianEmail = gu.GuardianEmail,
                            GuardianOccupation = gu.GuardianOccupation,
                            GuardianIncome = gu.GuardianIncome,
                            LocAddress1 = stu.LocAddress1,
                            LocAddress2 = stu.LocAddress2,
                            LocCity = stu.LocCity,
                            LocState = stu.LocState,
                            LocCountry = stu.LocCountry,
                            LocPostelcode = stu.LocPostelcode,
                            StudentPhoto = stu.StudentPhoto,
                            CommunityCertificate = stu.CommunityCertificate,
                            BirthCertificate = stu.BirthCertificate,
                            TransferCertificate = stu.TransferCertificate,
                            PreSchool = stu.PreSchholName,
                           // PreMedium = f.BoardName,
                            Pre_BoardofSchool = stu.PreSchoolMedium,
                            PreMarks = stu.PreSchoolMark,
                            PreClass = stu.PreSchoolClass,
                            PreFromDate1 = stu.PreSchoolFromdate,
                            PreToDate = stu.PreSchoolToDate,
                            NoOfSbling = stu.NoOfSibling,
                            PrimaryUser = Priuser.PrimaryUser,
                            EmergencyContactPersonName = stu.EmergencyContactPersonName,
                            EmergencyContactNumber = stu.EmergencyContactNumber,
                            ContactPersonRelationship = stu.ContactPersonRelationship,
                            Email = stu.Email,
                            Contact = stu.Contact,
                            Height = stu.Height,
                            Weights = stu.Weights,
                            IdentificationMark = stu.IdentificationMark,
                            PerAddress1 = stu.PerAddress1,
                            PerAddress2 = stu.PerAddress2,
                            PerCity = stu.PerCity,
                            PerState = stu.PerState,
                            PerCountry = stu.PerCountry,
                            PerPostelcode = stu.PerPostelcode,                        
                           // TotalIncome = Priuser.TotalIncome,
                            GuardianRequried = stu.GuardianRequried,
                            IncomeCertificate = stu.IncomeCertificate,
                            AcademicyearId = stu.AcademicyearId,
                            Distance = stu.Distance,
                            primaryUserEmail = Priuser.PrimaryUserEmail,
                            WorkSameSchool = Priuser.WorkSameSchool,
                            EmployeeDesignationId = Priuser.EmployeeDesignationId,
                            EmployeeId = Priuser.EmployeeId,
                            EmailRequired = Priuser.EmailRequired,
                            SmsRequired = Priuser.SmsRequired,
                            TransportRequired = stu.TransportRequired,
                            TransportDestination = stu.TransportDesignation,
                            TransportPickpoint = stu.TransportPickpoint,
                            TransportFeeAmount = stu.TransportFeeAmount,
                            HostelRequired = stu.HostelRequired,
                            AccommodationFeeCategoryId = stu.AccommodationFeeCategoryId,
                            AccommodationSubFeeCategoryId = stu.AccommodationSubFeeCategoryId,
                            FoodFeeCategoryId = stu.FoodFeeCategoryId,
                            FoodSubFeeCategoryId = stu.FoodSubFeeCategoryId,
                            primaryUserContact = Priuser.PrimaryUserContactNo
                        }).Distinct().ToList();
             return res;
         }


         public List<M_StudentRegisteration> OldStudentAllUserdetails(int PrimaryuserId)
        {
            var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                          from trans in dc.PreAdmissionTransactions
                          from fa in dc.PreAdmissionFatherRegisters
                          from mo in dc.PreAdmissionMotherRegisters
                          from stu in dc.PreAdmissionStudentRegisters
                          from gu in dc.PreAdmissionGuardianRegisters
                          from f in dc.BoardofSchools
                          where trans.PrimaryUserAdmissionId == PrimaryuserId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId && Priuser.OnlineRegisterId == stu.OnlineRegisterId
                           && fa.FatherAdmissionId == trans.FatherAdmissionId && mo.MotherAdmissionId == trans.MotherAdmissionId && gu.GuardianAdmissionId == trans.GuardianAdmissionId
                          select new M_StudentRegisteration
                          {
                              FatherFirstName = fa.FatherFirstName,
                              FatherOccupation = fa.FatherOccupation,
                              FatherMobileNo = fa.FatherMobileNo,
                              FatherEmail = fa.FatherEmail,
                              MotherFirstname = mo.MotherFirstname,
                              MotherOccupation = mo.MotherOccupation,
                              MotherMobileNo = mo.MotherMobileNo,
                              MotherEmail = mo.MotherEmail,
                              LocAddress1 = stu.LocAddress1,
                              LocAddress2 = stu.LocAddress2,
                              LocCity = stu.LocCity,
                              LocCountry = stu.LocCountry,
                              LocState = stu.LocState,
                              LocPostelcode = stu.LocPostelcode,
                              Distance = stu.Distance,
                              UserCompanyname = Priuser.UserCompanyname,
                              CompanyAddress1 = Priuser.CompanyAddress1,
                              CompanyAddress2 = Priuser.CompanyAddress2,
                              CompanyCity = Priuser.CompanyCity,
                              CompanyState = Priuser.CompanyState,
                              CompanyCountry = Priuser.CompanyCountry,
                              CompanyPostelcode = Priuser.CompanyPostelcode,
                              CompanyContact = Priuser.CompanyContact,
                              GuardianFirstname = gu.GuardianFirstname,
                              GuRelationshipToChild = gu.GuRelationshipToChild,
                              GuardianGender = gu.GuardianGender,
                              GuardianDOB = gu.GuardianDOB,
                              GuardianQualification = gu.GuardianQualification,
                              GuardianMobileNo = gu.GuardianMobileNo,
                              GuardianEmail = gu.GuardianEmail,
                              GuardianOccupation = gu.GuardianOccupation,
                              GuardianIncome = gu.GuardianIncome,
                              GuardianLastName = gu.GuardianLastName,
                              PrimaryUser = Priuser.PrimaryUser,
                              txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),                               
                              txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),
                              txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),
                              FatherLastName = fa.FatherLastName,
                              MotherLastName = mo.MotherLastName,
                              FatherDOB = fa.FatherDOB,
                              MotherDOB = mo.MotherDOB,
                              TotalIncome = fa.FatherMotherTotalIncome,
                              FatherQualification = fa.FatherQualification,
                              MotherQualification = mo.MotherQualification,
                              primaryUserEmail = Priuser.PrimaryUserEmail,
                              WorkSameSchool = Priuser.WorkSameSchool,
                              EmployeeDesignationId = Priuser.EmployeeDesignationId,
                              EmployeeId = Priuser.EmployeeId,
                              EmailRequired = Priuser.EmailRequired,
                              SmsRequired = Priuser.SmsRequired,
                              primaryUserContact = Priuser.PrimaryUserContactNo
                          }).Distinct().ToList();
            return res;
        }

         public List<M_StudentRegisteration> OldStudentFatherMotherdetails(int PrimaryuserId)
         {
             var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                        from trans in dc.PreAdmissionTransactions
                        from fa in dc.PreAdmissionFatherRegisters
                        from mo in dc.PreAdmissionMotherRegisters
                        from stu in dc.PreAdmissionStudentRegisters
                        from f in dc.BoardofSchools
                        where trans.PrimaryUserAdmissionId == PrimaryuserId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                            && fa.FatherAdmissionId == trans.FatherAdmissionId && mo.MotherAdmissionId == trans.MotherAdmissionId && Priuser.OnlineRegisterId == stu.OnlineRegisterId
                        select new M_StudentRegisteration
                        {
                            FatherFirstName = fa.FatherFirstName,
                            FatherOccupation = fa.FatherOccupation,
                            FatherMobileNo = fa.FatherMobileNo,
                            FatherEmail = fa.FatherEmail,
                            LocAddress1 = stu.LocAddress1,
                            LocAddress2 = stu.LocAddress2,
                            LocCity = stu.LocCity,
                            LocCountry = stu.LocCountry,
                            LocState = stu.LocState,
                            LocPostelcode = stu.LocPostelcode,
                            Distance = stu.Distance,
                            MotherFirstname = mo.MotherFirstname,
                            MotherOccupation = mo.MotherOccupation,
                            MotherMobileNo = mo.MotherMobileNo,
                            MotherEmail = mo.MotherEmail,
                            UserCompanyname = Priuser.UserCompanyname,
                            CompanyAddress1 = Priuser.CompanyAddress1,
                            CompanyAddress2 = Priuser.CompanyAddress2,
                            CompanyCity = Priuser.CompanyCity,
                            CompanyState = Priuser.CompanyState,
                            CompanyCountry = Priuser.CompanyCountry,
                            CompanyPostelcode = Priuser.CompanyPostelcode,
                            CompanyContact = Priuser.CompanyContact,                           
                            PrimaryUser = Priuser.PrimaryUser,
                            FatherLastName = fa.FatherLastName,
                            MotherLastName = mo.MotherLastName,
                            FatherDOB = fa.FatherDOB,
                            MotherDOB = mo.MotherDOB,
                            TotalIncome = fa.FatherMotherTotalIncome,
                            FatherQualification = fa.FatherQualification,
                            MotherQualification = mo.MotherQualification,
                            txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),
                            txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),                            
                            primaryUserEmail = Priuser.PrimaryUserEmail,
                            WorkSameSchool = Priuser.WorkSameSchool,
                            EmployeeDesignationId = Priuser.EmployeeDesignationId,
                            EmployeeId = Priuser.EmployeeId,
                            EmailRequired = Priuser.EmailRequired,
                            SmsRequired = Priuser.SmsRequired,
                            primaryUserContact = Priuser.PrimaryUserContactNo
                        }).Distinct().ToList();
             return res;
         }

        public List<M_StudentRegisteration> OldStudentFatherGuardiandetails(int PrimaryuserId)
         {
             var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                           from trans in dc.PreAdmissionTransactions
                           from stu in dc.PreAdmissionStudentRegisters
                           from fa in dc.PreAdmissionFatherRegisters
                           from gu in dc.PreAdmissionGuardianRegisters
                           from f in dc.BoardofSchools
                           where trans.PrimaryUserAdmissionId == PrimaryuserId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                            && fa.FatherAdmissionId == trans.FatherAdmissionId && gu.GuardianAdmissionId == trans.GuardianAdmissionId && Priuser.OnlineRegisterId == stu.OnlineRegisterId
                           select new M_StudentRegisteration
                           {
                               FatherFirstName = fa.FatherFirstName,
                               FatherOccupation = fa.FatherOccupation,
                               FatherMobileNo = fa.FatherMobileNo,
                               FatherEmail = fa.FatherEmail,
                               LocAddress1 = stu.LocAddress1,
                               LocAddress2 = stu.LocAddress2,
                               LocCity = stu.LocCity,
                               LocCountry = stu.LocCountry,
                               LocState = stu.LocState,
                               LocPostelcode = stu.LocPostelcode,
                               Distance = stu.Distance,
                               UserCompanyname = Priuser.UserCompanyname,
                               CompanyAddress1 = Priuser.CompanyAddress1,
                               CompanyAddress2 = Priuser.CompanyAddress2,
                               CompanyCity = Priuser.CompanyCity,
                               CompanyState = Priuser.CompanyState,
                               CompanyCountry = Priuser.CompanyCountry,
                               CompanyPostelcode = Priuser.CompanyPostelcode,
                               CompanyContact = Priuser.CompanyContact,
                               GuardianFirstname = gu.GuardianFirstname,
                               GuRelationshipToChild = gu.GuRelationshipToChild,
                               GuardianGender = gu.GuardianGender,
                               GuardianDOB = gu.GuardianDOB,
                               GuardianQualification = gu.GuardianQualification,
                               GuardianMobileNo = gu.GuardianMobileNo,
                               GuardianEmail = gu.GuardianEmail,
                               GuardianOccupation = gu.GuardianOccupation,
                               GuardianIncome = gu.GuardianIncome,
                               GuardianLastName = gu.GuardianLastName,
                               PrimaryUser = Priuser.PrimaryUser,
                               FatherLastName = fa.FatherLastName,
                               primaryUserEmail = Priuser.PrimaryUserEmail,
                               FatherDOB = fa.FatherDOB,
                               txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),
                               txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),                               
                               TotalIncome = fa.FatherMotherTotalIncome,
                               FatherQualification = fa.FatherQualification,
                               WorkSameSchool = Priuser.WorkSameSchool,
                               EmployeeDesignationId = Priuser.EmployeeDesignationId,
                               EmployeeId = Priuser.EmployeeId,
                               EmailRequired = Priuser.EmailRequired,
                               SmsRequired = Priuser.SmsRequired,
                               primaryUserContact = Priuser.PrimaryUserContactNo
                           }).Distinct().ToList();
             return res;
         }

        public List<M_StudentRegisteration> OldStudentMotherGuardiandetails(int PrimaryuserId)
        {
            var result = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                                              from trans in dc.PreAdmissionTransactions
                                              from mo in dc.PreAdmissionMotherRegisters
                                              from gu in dc.PreAdmissionGuardianRegisters
                                              from stu in dc.PreAdmissionStudentRegisters                
                                              from f in dc.BoardofSchools
                                              where trans.PrimaryUserAdmissionId == PrimaryuserId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                                            && mo.MotherAdmissionId == trans.MotherAdmissionId && gu.GuardianAdmissionId == trans.GuardianAdmissionId && Priuser.OnlineRegisterId == stu.OnlineRegisterId
                                              select new M_StudentRegisteration
                                              {
                                                  MotherFirstname = mo.MotherFirstname,
                                                  MotherOccupation = mo.MotherOccupation,
                                                  MotherMobileNo = mo.MotherMobileNo,
                                                  MotherEmail = mo.MotherEmail,
                                                  LocAddress1 = stu.LocAddress1,
                                                  LocAddress2 = stu.LocAddress2,
                                                  LocCity = stu.LocCity,
                                                  LocCountry = stu.LocCountry,
                                                  LocState = stu.LocState,
                                                  LocPostelcode = stu.LocPostelcode,
                                                  Distance = stu.Distance,
                                                  UserCompanyname = Priuser.UserCompanyname,
                                                  CompanyAddress1 = Priuser.CompanyAddress1,
                                                  CompanyAddress2 = Priuser.CompanyAddress2,
                                                  CompanyCity = Priuser.CompanyCity,
                                                  CompanyState = Priuser.CompanyState,
                                                  CompanyCountry = Priuser.CompanyCountry,
                                                  CompanyPostelcode = Priuser.CompanyPostelcode,
                                                  CompanyContact = Priuser.CompanyContact,
                                                  GuardianFirstname = gu.GuardianFirstname,
                                                  GuRelationshipToChild = gu.GuRelationshipToChild,
                                                  GuardianGender = gu.GuardianGender,
                                                  GuardianDOB = gu.GuardianDOB,
                                                  GuardianQualification = gu.GuardianQualification,
                                                  GuardianMobileNo = gu.GuardianMobileNo,
                                                  GuardianEmail = gu.GuardianEmail,
                                                  GuardianOccupation = gu.GuardianOccupation,
                                                  GuardianIncome = gu.GuardianIncome,
                                                  GuardianLastName = gu.GuardianLastName,
                                                  PrimaryUser = Priuser.PrimaryUser,
                                                  MotherLastName = mo.MotherLastName,
                                                  MotherDOB = mo.MotherDOB,
                                                  TotalIncome = mo.FatherMotherTotalIncome,
                                                  MotherQualification = mo.MotherQualification,
                                                  primaryUserEmail = Priuser.PrimaryUserEmail,
                                                  txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),
                                                  txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB) ,
                                                  WorkSameSchool = Priuser.WorkSameSchool,
                                                  EmployeeDesignationId = Priuser.EmployeeDesignationId,
                                                  EmployeeId = Priuser.EmployeeId,
                                                  EmailRequired=Priuser.EmailRequired,
                                                  SmsRequired=Priuser.SmsRequired,
                                                  primaryUserContact = Priuser.PrimaryUserContactNo
                                              }).Distinct().ToList();
            return result;
        }

        public List<M_StudentRegisteration> OldStudentFatherdetails(int PrimaryuserId)
        {
            var result = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                          from trans in dc.PreAdmissionTransactions
                          from stu in dc.PreAdmissionStudentRegisters
                          from fa in dc.PreAdmissionFatherRegisters
                          from f in dc.BoardofSchools
                          where trans.PrimaryUserAdmissionId == PrimaryuserId
                           && fa.FatherAdmissionId == trans.FatherAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId && Priuser.OnlineRegisterId == stu.OnlineRegisterId
                          select new M_StudentRegisteration
                          {
                              FatherFirstName = fa.FatherFirstName,
                              FatherOccupation = fa.FatherOccupation,
                              FatherMobileNo = fa.FatherMobileNo,
                              FatherEmail = fa.FatherEmail,
                              LocAddress1 = stu.LocAddress1,
                              LocAddress2 = stu.LocAddress2,
                              LocCity = stu.LocCity,
                              LocCountry = stu.LocCountry,
                              LocState = stu.LocState,
                              LocPostelcode = stu.LocPostelcode,
                              Distance = stu.Distance,
                              UserCompanyname = Priuser.UserCompanyname,
                              CompanyAddress1 = Priuser.CompanyAddress1,
                              CompanyAddress2 = Priuser.CompanyAddress2,
                              CompanyCity = Priuser.CompanyCity,
                              CompanyState = Priuser.CompanyState,
                              CompanyCountry = Priuser.CompanyCountry,
                              CompanyPostelcode = Priuser.CompanyPostelcode,
                              CompanyContact = Priuser.CompanyContact,
                              PrimaryUser = Priuser.PrimaryUser,
                              FatherLastName = fa.FatherLastName,
                              FatherDOB = fa.FatherDOB,
                              TotalIncome = fa.FatherMotherTotalIncome,
                              FatherQualification = fa.FatherQualification,
                              WorkSameSchool = Priuser.WorkSameSchool,
                              EmployeeDesignationId=Priuser.EmployeeDesignationId,
                              EmployeeId=Priuser.EmployeeId,
                              primaryUserEmail = Priuser.PrimaryUserEmail,
                              EmailRequired=Priuser.EmailRequired,
                                                  SmsRequired=Priuser.SmsRequired,
                                                  primaryUserContact=Priuser.PrimaryUserContactNo,
                              txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),                              
                          }).Distinct().ToList();
            return result;
        }

        public List<M_StudentRegisteration> OldStudentMotherdetails(int PrimaryUserid)
        {
           var result= (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                                              from trans in dc.PreAdmissionTransactions
                                              from mo in dc.PreAdmissionMotherRegisters
                                              from stu in dc.PreAdmissionStudentRegisters
                                              from f in dc.BoardofSchools
                                               where trans.PrimaryUserAdmissionId == PrimaryUserid && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId 
                                              && mo.MotherAdmissionId == trans.MotherAdmissionId && Priuser.OnlineRegisterId == stu.OnlineRegisterId
                                              select new M_StudentRegisteration
                                              {
                                                  MotherFirstname = mo.MotherFirstname,
                                                  MotherOccupation = mo.MotherOccupation,
                                                  MotherMobileNo = mo.MotherMobileNo,
                                                  MotherEmail = mo.MotherEmail,


                                                  LocAddress1 = stu.LocAddress1,
                                                  LocAddress2 = stu.LocAddress2,
                                                  LocCity = stu.LocCity,
                                                  LocCountry = stu.LocCountry,
                                                  LocState = stu.LocState,
                                                  LocPostelcode = stu.LocPostelcode,
                                                  Distance = stu.Distance,


                                                  UserCompanyname = Priuser.UserCompanyname,
                                                  CompanyAddress1 = Priuser.CompanyAddress1,
                                                  CompanyAddress2 = Priuser.CompanyAddress2,
                                                  CompanyCity = Priuser.CompanyCity,
                                                  CompanyState = Priuser.CompanyState,
                                                  CompanyCountry = Priuser.CompanyCountry,
                                                  CompanyPostelcode = Priuser.CompanyPostelcode,
                                                  CompanyContact = Priuser.CompanyContact,
                                                  PrimaryUser = Priuser.PrimaryUser,
                                                  MotherLastName = mo.MotherLastName,
                                                  MotherDOB = mo.MotherDOB,
                                                  txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),                                                  
                                                  TotalIncome = mo.FatherMotherTotalIncome,
                                                  primaryUserEmail = Priuser.PrimaryUserEmail,
                                                  MotherQualification = mo.MotherQualification,
                                                  WorkSameSchool = Priuser.WorkSameSchool,
                                                  EmployeeDesignationId = Priuser.EmployeeDesignationId,
                                                  EmployeeId = Priuser.EmployeeId,
                                                  EmailRequired = Priuser.EmailRequired,
                                                  SmsRequired = Priuser.SmsRequired,
                                                  primaryUserContact = Priuser.PrimaryUserContactNo
                                              }).Distinct().ToList();
            return result;
        }

        public List<M_StudentRegisteration> OldStudentGuardiandetails(int PrimaryuserId)
        {
            var result = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                          from trans in dc.PreAdmissionTransactions
                          from gu in dc.PreAdmissionGuardianRegisters
                          from f in dc.BoardofSchools
                          from stu in dc.PreAdmissionStudentRegisters
                          where trans.PrimaryUserAdmissionId == PrimaryuserId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                          && gu.GuardianAdmissionId == trans.GuardianAdmissionId && Priuser.OnlineRegisterId == stu.OnlineRegisterId
                          select new M_StudentRegisteration
                          {
                              UserCompanyname = Priuser.UserCompanyname,
                              CompanyAddress1 = Priuser.CompanyAddress1,
                              CompanyAddress2 = Priuser.CompanyAddress2,
                              CompanyCity = Priuser.CompanyCity,
                              CompanyState = Priuser.CompanyState,
                              CompanyCountry = Priuser.CompanyCountry,
                              CompanyPostelcode = Priuser.CompanyPostelcode,
                              CompanyContact = Priuser.CompanyContact,
                              LocAddress1 = stu.LocAddress1,
                              LocAddress2 = stu.LocAddress2,
                              LocCity = stu.LocCity,
                              LocCountry = stu.LocCountry,
                              LocState = stu.LocState,
                              LocPostelcode = stu.LocPostelcode,
                              Distance = stu.Distance,
                              GuardianFirstname = gu.GuardianFirstname,
                              GuRelationshipToChild = gu.GuRelationshipToChild,
                              GuardianGender = gu.GuardianGender,
                              GuardianDOB = gu.GuardianDOB,
                              GuardianQualification = gu.GuardianQualification,
                              GuardianMobileNo = gu.GuardianMobileNo,
                              GuardianEmail = gu.GuardianEmail,
                              GuardianOccupation = gu.GuardianOccupation,
                              GuardianIncome = gu.GuardianIncome,
                              GuardianLastName = gu.GuardianLastName,
                              PrimaryUser = Priuser.PrimaryUser,
                              TotalIncome = gu.GuardianIncome,
                              primaryUserEmail = Priuser.PrimaryUserEmail,
                              txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),                              
                              WorkSameSchool = Priuser.WorkSameSchool,
                              EmployeeDesignationId = Priuser.EmployeeDesignationId,
                              EmployeeId = Priuser.EmployeeId,
                              EmailRequired = Priuser.EmailRequired,
                              SmsRequired = Priuser.SmsRequired,
                              primaryUserContact = Priuser.PrimaryUserContactNo
                          }).Distinct().ToList();
            return result;

        }


        public List<M_StudentRegisteration> StudentAllUserdetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                       from trans in dc.PreAdmissionTransactions
                       from stu in dc.PreAdmissionStudentRegisters
                       from fa in dc.PreAdmissionFatherRegisters
                       from mo in dc.PreAdmissionMotherRegisters
                       from gu in dc.PreAdmissionGuardianRegisters
                       from a in dc.AcademicYears
                       from b in dc.Classes
                       from c in dc.tblCommunities
                       from d in dc.tblCountries
                       from e in dc.tblBloodGroups
                       from f in dc.BoardofSchools
                       where trans.PrimaryUserAdmissionId == PrimaryuserId && trans.StudentAdmissionId == StudentId && stu.StudentAdmissionId == trans.StudentAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                       && fa.FatherAdmissionId == trans.FatherAdmissionId && mo.MotherAdmissionId == trans.MotherAdmissionId && gu.GuardianAdmissionId == trans.GuardianAdmissionId
                       && a.AcademicYearId == stu.AcademicyearId && b.ClassId == stu.AdmissionClass && c.CommunityId == stu.Community  && e.BloodId == stu.BloodGroup                                            // && f.BoardId==stu.PreSchoolMedium
                       select new M_StudentRegisteration
                       {
                           Stuadmissionid = stu.StudentAdmissionId,
                           StuFirstName = stu.StuFirstName,
                           StuMiddlename = stu.StuMiddlename,
                           StuLastname = stu.StuLastname,
                           Gender = stu.Gender,
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.Community,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.BloodGroup,
                           txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),
                           txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),
                           txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreSchoolFromdate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolFromdate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolFromdate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolToDate),
                           AdmissionClass = stu.AdmissionClass,
                           FatherFirstName = fa.FatherFirstName,
                           FatherOccupation = fa.FatherOccupation,
                           FatherMobileNo = fa.FatherMobileNo,
                           FatherEmail = fa.FatherEmail,
                           MotherFirstname = mo.MotherFirstname,
                           MotherOccupation = mo.MotherOccupation,
                           MotherMobileNo = mo.MotherMobileNo,
                           MotherEmail = mo.MotherEmail,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           GuardianFirstname = gu.GuardianFirstname,
                           GuardianLastName = gu.GuardianLastName,
                           GuRelationshipToChild = gu.GuRelationshipToChild,
                           GuardianDOB = gu.GuardianDOB,
                           GuardianGender = gu.GuardianGender,
                           GuardianQualification = gu.GuardianQualification,
                           GuardianMobileNo = gu.GuardianMobileNo,
                           GuardianEmail = gu.GuardianEmail,
                           GuardianOccupation = gu.GuardianOccupation,
                           GuardianIncome = gu.GuardianIncome,
                           LocAddress1 = stu.LocAddress1,
                           LocAddress2 = stu.LocAddress2,
                           LocCity = stu.LocCity,
                           LocState = stu.LocState,
                           LocCountry = stu.LocCountry,
                           LocPostelcode = stu.LocPostelcode,
                           StudentPhoto = stu.StudentPhoto,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreSchholName,
                           //PreMedium = f.BoardName,
                           Pre_BoardofSchool = stu.PreSchoolMedium,
                           PreMarks = stu.PreSchoolMark,
                           PreClass = stu.PreSchoolClass,
                           PreFromDate1 = stu.PreSchoolFromdate,
                           PreToDate = stu.PreSchoolToDate,
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPersonName,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PerAddress1,
                           PerAddress2 = stu.PerAddress2,
                           PerCity = stu.PerCity,
                           PerState = stu.PerState,
                           PerCountry = stu.PerCountry,
                           PerPostelcode = stu.PerPostelcode,
                           FatherLastName = fa.FatherLastName,
                           MotherLastName = mo.MotherLastName,
                           FatherDOB = fa.FatherDOB,
                           MotherDOB = mo.MotherDOB,
                           TotalIncome = fa.FatherMotherTotalIncome,
                           FatherQualification = fa.FatherQualification,
                           MotherQualification = mo.MotherQualification,
                           GuardianRequried = stu.GuardianRequried,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           txt_academicyear = a.AcademicYear1,
                           txt_ApplyClass = b.ClassType,
                           txt_community = c.CommunityName,
                           txt_ComCountry = Priuser.CompanyCountry,
                           txt_LocCountry = stu.LocCountry,
                           txt_PerCountry = stu.PerCountry,
                           txt_bloodgroup = e.BloodGroupName,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           WorkSameSchool = Priuser.WorkSameSchool,
                           EmployeeDesignationId = Priuser.EmployeeDesignationId,
                           EmployeeId = Priuser.EmployeeId,
                           EmailRequired = Priuser.EmailRequired,
                           SmsRequired = Priuser.SmsRequired,
                           TransportRequired = stu.TransportRequired,
                           TransportDestination = stu.TransportDesignation,
                           TransportPickpoint = stu.TransportPickpoint,
                           TransportFeeAmount = stu.TransportFeeAmount,
                           HostelRequired = stu.HostelRequired,
                           AccommodationFeeCategoryId = stu.AccommodationFeeCategoryId,
                           AccommodationSubFeeCategoryId = stu.AccommodationSubFeeCategoryId,
                           FoodFeeCategoryId = stu.FoodFeeCategoryId,
                           FoodSubFeeCategoryId = stu.FoodSubFeeCategoryId,
                           primaryUserContact = Priuser.PrimaryUserContactNo
                       }).Distinct().ToList();
            return res;
        }


        public List<M_StudentRegisteration> StudentFatherMotherDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                       from trans in dc.PreAdmissionTransactions
                       from stu in dc.PreAdmissionStudentRegisters
                       from fa in dc.PreAdmissionFatherRegisters
                       from mo in dc.PreAdmissionMotherRegisters
                       from a in dc.AcademicYears
                       from b in dc.Classes
                       from c in dc.tblCommunities
                       from d in dc.tblCountries
                       from e in dc.tblBloodGroups
                       from f in dc.BoardofSchools
                       where trans.PrimaryUserAdmissionId == PrimaryuserId && trans.StudentAdmissionId == StudentId && stu.StudentAdmissionId == trans.StudentAdmissionId
                        && fa.FatherAdmissionId == trans.FatherAdmissionId && mo.MotherAdmissionId == trans.MotherAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                          && a.AcademicYearId == stu.AcademicyearId && b.ClassId == stu.AdmissionClass && c.CommunityId == stu.Community  && e.BloodId == stu.BloodGroup                                     //&& f.BoardId == stu.PreSchoolMedium
                       select new M_StudentRegisteration
                       {
                           Stuadmissionid = stu.StudentAdmissionId,
                           StuFirstName = stu.StuFirstName,
                           StuMiddlename = stu.StuMiddlename,
                           StuLastname = stu.StuLastname,
                           Gender = stu.Gender,
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.Community,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.BloodGroup,
                           txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),
                           txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),                           
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreSchoolFromdate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolFromdate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolFromdate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolToDate),      
                           AdmissionClass = stu.AdmissionClass,
                           FatherFirstName = fa.FatherFirstName,
                           FatherOccupation = fa.FatherOccupation,
                           FatherMobileNo = fa.FatherMobileNo,
                           FatherEmail = fa.FatherEmail,
                           MotherFirstname = mo.MotherFirstname,
                           MotherOccupation = mo.MotherOccupation,
                           MotherMobileNo = mo.MotherMobileNo,
                           MotherEmail = mo.MotherEmail,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           LocAddress1 = stu.LocAddress1,
                           LocAddress2 = stu.LocAddress2,
                           LocCity = stu.LocCity,
                           LocState = stu.LocState,
                           LocCountry = stu.LocCountry,
                           LocPostelcode = stu.LocPostelcode,
                           StudentPhoto = stu.StudentPhoto,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreSchholName,
                           //PreMedium = f.BoardName,
                           Pre_BoardofSchool = stu.PreSchoolMedium,
                           PreMarks = stu.PreSchoolMark,
                           PreClass = stu.PreSchoolClass,
                           PreFromDate1 = stu.PreSchoolFromdate,
                           PreToDate = stu.PreSchoolToDate,
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPersonName,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PerAddress1,
                           PerAddress2 = stu.PerAddress2,
                           PerCity = stu.PerCity,
                           PerState = stu.PerState,
                           PerCountry = stu.PerCountry,
                           PerPostelcode = stu.PerPostelcode,
                           FatherLastName = fa.FatherLastName,
                           MotherLastName = mo.MotherLastName,
                           FatherDOB = fa.FatherDOB,
                           MotherDOB = mo.MotherDOB,
                           TotalIncome = fa.FatherMotherTotalIncome,
                           FatherQualification = fa.FatherQualification,
                           MotherQualification = mo.MotherQualification,
                           IncomeCertificate = stu.IncomeCertificate,
                           GuardianRequried = stu.GuardianRequried,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           txt_academicyear = a.AcademicYear1,
                           txt_ApplyClass = b.ClassType,
                           txt_community = c.CommunityName,
                           txt_ComCountry = Priuser.CompanyCountry,
                           txt_LocCountry = stu.LocCountry,
                           txt_PerCountry = stu.PerCountry,
                           txt_bloodgroup = e.BloodGroupName,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           WorkSameSchool = Priuser.WorkSameSchool,
                           EmployeeDesignationId = Priuser.EmployeeDesignationId,
                           EmployeeId = Priuser.EmployeeId,
                           EmailRequired = Priuser.EmailRequired,
                           SmsRequired = Priuser.SmsRequired,
                           TransportRequired = stu.TransportRequired,
                           TransportDestination = stu.TransportDesignation,
                           TransportPickpoint = stu.TransportPickpoint,
                           TransportFeeAmount = stu.TransportFeeAmount,
                           HostelRequired = stu.HostelRequired,
                           AccommodationFeeCategoryId = stu.AccommodationFeeCategoryId,
                           AccommodationSubFeeCategoryId = stu.AccommodationSubFeeCategoryId,
                           FoodFeeCategoryId = stu.FoodFeeCategoryId,
                           FoodSubFeeCategoryId = stu.FoodSubFeeCategoryId,
                           primaryUserContact = Priuser.PrimaryUserContactNo
                       }).Distinct().ToList();
            return res;
        }

        public List<M_StudentRegisteration> StudentFatherGuardianDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                       from trans in dc.PreAdmissionTransactions
                       from stu in dc.PreAdmissionStudentRegisters
                       from fa in dc.PreAdmissionFatherRegisters
                       from a in dc.AcademicYears
                       from b in dc.Classes
                       from c in dc.tblCommunities
                       from d in dc.tblCountries
                       from e in dc.tblBloodGroups
                       from gu in dc.PreAdmissionGuardianRegisters
                       from f in dc.BoardofSchools
                       where trans.PrimaryUserAdmissionId == PrimaryuserId && trans.StudentAdmissionId == StudentId && stu.StudentAdmissionId == trans.StudentAdmissionId
                        && fa.FatherAdmissionId == trans.FatherAdmissionId && gu.GuardianAdmissionId == trans.GuardianAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                          && a.AcademicYearId == stu.AcademicyearId && b.ClassId == stu.AdmissionClass && c.CommunityId == stu.Community && e.BloodId == stu.BloodGroup                                         //&& f.BoardId == stu.PreSchoolMedium
                       select new M_StudentRegisteration
                       {
                           Stuadmissionid = stu.StudentAdmissionId,
                           StuFirstName = stu.StuFirstName,
                           StuMiddlename = stu.StuMiddlename,
                           StuLastname = stu.StuLastname,
                           Gender = stu.Gender,
                           txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),                           
                           txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreSchoolFromdate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolFromdate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolFromdate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolToDate),                                    
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.Community,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.BloodGroup,
                           AdmissionClass = stu.AdmissionClass,
                           FatherFirstName = fa.FatherFirstName,
                           FatherOccupation = fa.FatherOccupation,
                           FatherMobileNo = fa.FatherMobileNo,
                           FatherEmail = fa.FatherEmail,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           GuardianFirstname = gu.GuardianFirstname,
                           GuardianLastName = gu.GuardianLastName,
                           GuRelationshipToChild = gu.GuRelationshipToChild,
                           GuardianDOB = gu.GuardianDOB,
                           GuardianGender = gu.GuardianGender,
                           GuardianQualification = gu.GuardianQualification,
                           GuardianMobileNo = gu.GuardianMobileNo,
                           GuardianEmail = gu.GuardianEmail,
                           GuardianOccupation = gu.GuardianOccupation,
                           GuardianIncome = gu.GuardianIncome,
                           LocAddress1 = stu.LocAddress1,
                           LocAddress2 = stu.LocAddress2,
                           LocCity = stu.LocCity,
                           LocState = stu.LocState,
                           LocCountry = stu.LocCountry,
                           LocPostelcode = stu.LocPostelcode,
                           StudentPhoto = stu.StudentPhoto,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreSchholName,
                           //PreMedium = f.BoardName,
                           Pre_BoardofSchool = stu.PreSchoolMedium,
                           PreMarks = stu.PreSchoolMark,
                           PreClass = stu.PreSchoolClass,
                           PreFromDate1 = stu.PreSchoolFromdate,
                           PreToDate = stu.PreSchoolToDate,
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPersonName,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PerAddress1,
                           PerAddress2 = stu.PerAddress2,
                           PerCity = stu.PerCity,
                           PerState = stu.PerState,
                           PerCountry = stu.PerCountry,
                           PerPostelcode = stu.PerPostelcode,
                           FatherLastName = fa.FatherLastName,
                           FatherDOB = fa.FatherDOB,
                           TotalIncome = fa.FatherMotherTotalIncome,
                           FatherQualification = fa.FatherQualification,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           GuardianRequried = stu.GuardianRequried,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           txt_academicyear = a.AcademicYear1,
                           txt_ApplyClass = b.ClassType,
                           txt_community = c.CommunityName,
                           txt_ComCountry = Priuser.CompanyCountry,
                           txt_LocCountry = stu.LocCountry,
                           txt_PerCountry = stu.PerCountry,
                           txt_bloodgroup = e.BloodGroupName,
                           WorkSameSchool = Priuser.WorkSameSchool,
                           EmployeeDesignationId = Priuser.EmployeeDesignationId,
                           EmployeeId = Priuser.EmployeeId,
                           EmailRequired = Priuser.EmailRequired,
                           SmsRequired = Priuser.SmsRequired,
                           TransportRequired = stu.TransportRequired,
                           TransportDestination = stu.TransportDesignation,
                           TransportPickpoint = stu.TransportPickpoint,
                           TransportFeeAmount = stu.TransportFeeAmount,
                           HostelRequired = stu.HostelRequired,
                           AccommodationFeeCategoryId = stu.AccommodationFeeCategoryId,
                           AccommodationSubFeeCategoryId = stu.AccommodationSubFeeCategoryId,
                           FoodFeeCategoryId = stu.FoodFeeCategoryId,
                           FoodSubFeeCategoryId = stu.FoodSubFeeCategoryId,
                           primaryUserContact = Priuser.PrimaryUserContactNo
                       }).Distinct().ToList();
            return res;
        }

        public List<M_StudentRegisteration> StudentMotherGuardianDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                       from trans in dc.PreAdmissionTransactions
                       from stu in dc.PreAdmissionStudentRegisters
                       from mo in dc.PreAdmissionMotherRegisters
                       from gu in dc.PreAdmissionGuardianRegisters
                       from a in dc.AcademicYears
                       from b in dc.Classes
                       from c in dc.tblCommunities
                       from d in dc.tblCountries
                       from e in dc.tblBloodGroups
                       from f in dc.BoardofSchools
                       where trans.PrimaryUserAdmissionId == PrimaryuserId && trans.StudentAdmissionId == StudentId && stu.StudentAdmissionId == trans.StudentAdmissionId
                       && mo.MotherAdmissionId == trans.MotherAdmissionId && gu.GuardianAdmissionId == trans.GuardianAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                         && a.AcademicYearId == stu.AcademicyearId && b.ClassId == stu.AdmissionClass && c.CommunityId == stu.Community && e.BloodId == stu.BloodGroup                                              //&& f.BoardId == stu.PreSchoolMedium
                       select new M_StudentRegisteration
                       {
                           Stuadmissionid = stu.StudentAdmissionId,
                           StuFirstName = stu.StuFirstName,
                           StuMiddlename = stu.StuMiddlename,
                           StuLastname = stu.StuLastname,
                           Gender = stu.Gender,
                           txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),
                           txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreSchoolFromdate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolFromdate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolFromdate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolToDate),                                    
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.Community,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.BloodGroup,
                           AdmissionClass = stu.AdmissionClass,
                           MotherFirstname = mo.MotherFirstname,
                           MotherOccupation = mo.MotherOccupation,
                           MotherMobileNo = mo.MotherMobileNo,
                           MotherEmail = mo.MotherEmail,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           GuardianFirstname = gu.GuardianFirstname,
                           GuardianLastName = gu.GuardianLastName,
                           GuRelationshipToChild = gu.GuRelationshipToChild,
                           GuardianDOB = gu.GuardianDOB,
                           GuardianGender = gu.GuardianGender,
                           GuardianQualification = gu.GuardianQualification,
                           GuardianMobileNo = gu.GuardianMobileNo,
                           GuardianEmail = gu.GuardianEmail,
                           GuardianOccupation = gu.GuardianOccupation,
                           GuardianIncome = gu.GuardianIncome,
                           LocAddress1 = stu.LocAddress1,
                           LocAddress2 = stu.LocAddress2,
                           LocCity = stu.LocCity,
                           LocState = stu.LocState,
                           LocCountry = stu.LocCountry,
                           LocPostelcode = stu.LocPostelcode,
                           StudentPhoto = stu.StudentPhoto,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreSchholName,
                           //PreMedium = f.BoardName,
                           Pre_BoardofSchool = stu.PreSchoolMedium,
                           PreMarks = stu.PreSchoolMark,
                           PreClass = stu.PreSchoolClass,
                           PreFromDate1 = stu.PreSchoolFromdate,
                           PreToDate = stu.PreSchoolToDate,
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPersonName,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PerAddress1,
                           PerAddress2 = stu.PerAddress2,
                           PerCity = stu.PerCity,
                           PerState = stu.PerState,
                           PerCountry = stu.PerCountry,
                           PerPostelcode = stu.PerPostelcode,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           MotherLastName = mo.MotherLastName,
                           MotherDOB = mo.MotherDOB,
                           TotalIncome = mo.FatherMotherTotalIncome,
                           MotherQualification = mo.MotherQualification,
                           GuardianRequried = stu.GuardianRequried,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           txt_academicyear = a.AcademicYear1,
                           txt_ApplyClass = b.ClassType,
                           txt_community = c.CommunityName,
                           txt_ComCountry = Priuser.CompanyCountry,
                           txt_LocCountry = stu.LocCountry,
                           txt_PerCountry = stu.PerCountry,
                           txt_bloodgroup = e.BloodGroupName,
                           WorkSameSchool = Priuser.WorkSameSchool,
                           EmployeeDesignationId = Priuser.EmployeeDesignationId,
                           EmployeeId = Priuser.EmployeeId,
                           EmailRequired = Priuser.EmailRequired,
                           SmsRequired = Priuser.SmsRequired,
                           TransportRequired = stu.TransportRequired,
                           TransportDestination = stu.TransportDesignation,
                           TransportPickpoint = stu.TransportPickpoint,
                           TransportFeeAmount = stu.TransportFeeAmount,
                           HostelRequired = stu.HostelRequired,
                           AccommodationFeeCategoryId = stu.AccommodationFeeCategoryId,
                           AccommodationSubFeeCategoryId = stu.AccommodationSubFeeCategoryId,
                           FoodFeeCategoryId = stu.FoodFeeCategoryId,
                           FoodSubFeeCategoryId = stu.FoodSubFeeCategoryId,
                           primaryUserContact = Priuser.PrimaryUserContactNo
                       }).Distinct().ToList();
            return res;
        }

        public List<M_StudentRegisteration> StudentFatherDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                       from trans in dc.PreAdmissionTransactions
                       from stu in dc.PreAdmissionStudentRegisters
                       from fa in dc.PreAdmissionFatherRegisters
                       from a in dc.AcademicYears
                       from b in dc.Classes
                       from c in dc.tblCommunities
                       from d in dc.tblCountries
                       from e in dc.tblBloodGroups
                       from f in dc.BoardofSchools
                       where trans.PrimaryUserAdmissionId == PrimaryuserId && trans.StudentAdmissionId == StudentId && stu.StudentAdmissionId == trans.StudentAdmissionId
                        && fa.FatherAdmissionId == trans.FatherAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                          && a.AcademicYearId == stu.AcademicyearId && b.ClassId == stu.AdmissionClass && c.CommunityId == stu.Community && e.BloodId == stu.BloodGroup                                           //&& f.BoardId == stu.PreSchoolMedium
                       select new M_StudentRegisteration
                       {
                           Stuadmissionid = stu.StudentAdmissionId,
                           StuFirstName = stu.StuFirstName,
                           StuMiddlename = stu.StuMiddlename,
                           StuLastname = stu.StuLastname,
                           Gender = stu.Gender,
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.Community,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.BloodGroup,
                           txt_Fatherdob = SqlFunctions.DateName("day", fa.FatherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)fa.FatherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", fa.FatherDOB),                                                                                 
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreSchoolFromdate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolFromdate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolFromdate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolToDate),                                    
                           AdmissionClass = stu.AdmissionClass,
                           FatherFirstName = fa.FatherFirstName,
                           FatherOccupation = fa.FatherOccupation,
                           FatherMobileNo = fa.FatherMobileNo,
                           FatherEmail = fa.FatherEmail,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           LocAddress1 = stu.LocAddress1,
                           LocAddress2 = stu.LocAddress2,
                           LocCity = stu.LocCity,
                           LocState = stu.LocState,
                           LocCountry = stu.LocCountry,
                           LocPostelcode = stu.LocPostelcode,
                           StudentPhoto = stu.StudentPhoto,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreSchholName,
                           //PreMedium = f.BoardName,
                           Pre_BoardofSchool = stu.PreSchoolMedium,
                           PreMarks = stu.PreSchoolMark,
                           PreClass = stu.PreSchoolClass,
                           PreFromDate1 = stu.PreSchoolFromdate,
                           PreToDate = stu.PreSchoolToDate,
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPersonName,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PerAddress1,
                           PerAddress2 = stu.PerAddress2,
                           PerCity = stu.PerCity,
                           PerState = stu.PerState,
                           PerCountry = stu.PerCountry,
                           PerPostelcode = stu.PerPostelcode,
                           FatherLastName = fa.FatherLastName,
                           GuardianRequried = stu.GuardianRequried,
                           FatherDOB = fa.FatherDOB,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           TotalIncome = fa.FatherMotherTotalIncome,
                           FatherQualification = fa.FatherQualification,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           txt_academicyear = a.AcademicYear1,
                           txt_ApplyClass = b.ClassType,
                           txt_community = c.CommunityName,
                           txt_ComCountry = Priuser.CompanyCountry,
                           txt_LocCountry = stu.LocCountry,
                           txt_PerCountry = stu.PerCountry,
                           txt_bloodgroup = e.BloodGroupName,
                           WorkSameSchool = Priuser.WorkSameSchool,
                           EmployeeDesignationId = Priuser.EmployeeDesignationId,
                           EmployeeId = Priuser.EmployeeId,
                           EmailRequired = Priuser.EmailRequired,
                           SmsRequired = Priuser.SmsRequired,
                           TransportRequired = stu.TransportRequired,
                           TransportDestination = stu.TransportDesignation,
                           TransportPickpoint = stu.TransportPickpoint,
                           TransportFeeAmount = stu.TransportFeeAmount,
                           HostelRequired = stu.HostelRequired,
                           AccommodationFeeCategoryId = stu.AccommodationFeeCategoryId,
                           AccommodationSubFeeCategoryId = stu.AccommodationSubFeeCategoryId,
                           FoodFeeCategoryId = stu.FoodFeeCategoryId,
                           FoodSubFeeCategoryId = stu.FoodSubFeeCategoryId,
                           primaryUserContact = Priuser.PrimaryUserContactNo
                       }).Distinct().ToList();
            return res;
        }
        public List<M_StudentRegisteration> StudentMotherDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                       from trans in dc.PreAdmissionTransactions
                       from stu in dc.PreAdmissionStudentRegisters
                       from mo in dc.PreAdmissionMotherRegisters
                       from a in dc.AcademicYears
                       from b in dc.Classes
                       from c in dc.tblCommunities
                       from d in dc.tblCountries
                       from e in dc.tblBloodGroups
                       from f in dc.BoardofSchools
                       where trans.PrimaryUserAdmissionId == PrimaryuserId && trans.StudentAdmissionId == StudentId && stu.StudentAdmissionId == trans.StudentAdmissionId
                        && mo.MotherAdmissionId == trans.MotherAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                          && a.AcademicYearId == stu.AcademicyearId && b.ClassId == stu.AdmissionClass && c.CommunityId == stu.Community && e.BloodId == stu.BloodGroup                                   //&& f.BoardId == stu.PreSchoolMedium
                       select new M_StudentRegisteration
                       {
                           Stuadmissionid = stu.StudentAdmissionId,
                           StuFirstName = stu.StuFirstName,
                           StuMiddlename = stu.StuMiddlename,
                           StuLastname = stu.StuLastname,
                           Gender = stu.Gender,
                           txt_Motherdob = SqlFunctions.DateName("day", mo.MotherDOB).Trim() + "/" + SqlFunctions.StringConvert((double)mo.MotherDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", mo.MotherDOB),                                                                                 
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),                           
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreSchoolFromdate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolFromdate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolFromdate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolToDate),                                    
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.Community,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.BloodGroup,
                           AdmissionClass = stu.AdmissionClass,
                           MotherFirstname = mo.MotherFirstname,
                           MotherOccupation = mo.MotherOccupation,
                           MotherMobileNo = mo.MotherMobileNo,
                           MotherEmail = mo.MotherEmail,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           LocAddress1 = stu.LocAddress1,
                           LocAddress2 = stu.LocAddress2,
                           LocCity = stu.LocCity,
                           LocState = stu.LocState,
                           LocCountry = stu.LocCountry,
                           LocPostelcode = stu.LocPostelcode,
                           StudentPhoto = stu.StudentPhoto,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreSchholName,
                           //PreMedium = f.BoardName,
                           Pre_BoardofSchool = stu.PreSchoolMedium,
                           PreMarks = stu.PreSchoolMark,
                           PreClass = stu.PreSchoolClass,
                           PreFromDate1 = stu.PreSchoolFromdate,
                           PreToDate = stu.PreSchoolToDate,
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPersonName,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PerAddress1,
                           PerAddress2 = stu.PerAddress2,
                           PerCity = stu.PerCity,
                           PerState = stu.PerState,
                           PerCountry = stu.PerCountry,
                           PerPostelcode = stu.PerPostelcode,
                           MotherLastName = mo.MotherLastName,
                           MotherDOB = mo.MotherDOB,
                           TotalIncome = mo.FatherMotherTotalIncome,
                           MotherQualification = mo.MotherQualification,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           GuardianRequried = stu.GuardianRequried,
                           txt_academicyear = a.AcademicYear1,
                           txt_ApplyClass = b.ClassType,
                           txt_community = c.CommunityName,
                           txt_ComCountry = Priuser.CompanyCountry,
                           txt_LocCountry = stu.LocCountry,
                           txt_PerCountry = stu.PerCountry,
                           txt_bloodgroup = e.BloodGroupName,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           EmployeeDesignationId = Priuser.EmployeeDesignationId,
                           EmployeeId = Priuser.EmployeeId,
                           WorkSameSchool = Priuser.WorkSameSchool,
                           EmailRequired = Priuser.EmailRequired,
                           SmsRequired = Priuser.SmsRequired,
                           TransportRequired = stu.TransportRequired,
                           TransportDestination = stu.TransportDesignation,
                           TransportPickpoint = stu.TransportPickpoint,
                           TransportFeeAmount = stu.TransportFeeAmount,
                           HostelRequired = stu.HostelRequired,
                           AccommodationFeeCategoryId = stu.AccommodationFeeCategoryId,
                           AccommodationSubFeeCategoryId = stu.AccommodationSubFeeCategoryId,
                           FoodFeeCategoryId = stu.FoodFeeCategoryId,
                           FoodSubFeeCategoryId = stu.FoodSubFeeCategoryId,
                           primaryUserContact = Priuser.PrimaryUserContactNo
                       }).Distinct().ToList();
            return res;
        }
        public List<M_StudentRegisteration> StudentGuardianDetails(int PrimaryuserId, int StudentId)
        {
            var res = (from Priuser in dc.PreAdmissionPrimaryUserRegisters
                       from trans in dc.PreAdmissionTransactions
                       from stu in dc.PreAdmissionStudentRegisters
                       from gu in dc.PreAdmissionGuardianRegisters
                       from a in dc.AcademicYears
                       from b in dc.Classes
                       from c in dc.tblCommunities
                       from d in dc.tblCountries
                       from e in dc.tblBloodGroups
                       from f in dc.BoardofSchools
                       where trans.PrimaryUserAdmissionId == PrimaryuserId && trans.StudentAdmissionId == StudentId && stu.StudentAdmissionId == trans.StudentAdmissionId
                        && gu.GuardianAdmissionId == trans.GuardianAdmissionId && Priuser.PrimaryUserAdmissionId == trans.PrimaryUserAdmissionId
                           && a.AcademicYearId == stu.AcademicyearId && b.ClassId == stu.AdmissionClass && c.CommunityId == stu.Community  && e.BloodId == stu.BloodGroup                           //&& f.BoardId == stu.PreSchoolMedium
                       select new M_StudentRegisteration
                       {
                           Stuadmissionid = stu.StudentAdmissionId,
                           StuFirstName = stu.StuFirstName,
                           StuMiddlename = stu.StuMiddlename,
                           StuLastname = stu.StuLastname,
                           Gender = stu.Gender,
                           txt_dob = SqlFunctions.DateName("day", stu.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)stu.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.DOB),
                           txt_Guardiandob = SqlFunctions.DateName("day", gu.GuardianDOB).Trim() + "/" + SqlFunctions.StringConvert((double)gu.GuardianDOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", gu.GuardianDOB),
                           txt_PreFromDate1 = SqlFunctions.DateName("day", stu.PreSchoolFromdate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolFromdate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolFromdate),
                           txt_PreToDate = SqlFunctions.DateName("day", stu.PreSchoolToDate).Trim() + "/" + SqlFunctions.StringConvert((double)stu.PreSchoolToDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", stu.PreSchoolToDate),                                    
                           PlaceOfBirth = stu.PlaceOfBirth,
                           Community = stu.Community,
                           Religion = stu.Religion,
                           Nationality = stu.Nationality,
                           BloodGroup = stu.BloodGroup,
                           AdmissionClass = stu.AdmissionClass,
                           UserCompanyname = Priuser.UserCompanyname,
                           CompanyAddress1 = Priuser.CompanyAddress1,
                           CompanyAddress2 = Priuser.CompanyAddress2,
                           CompanyCity = Priuser.CompanyCity,
                           CompanyState = Priuser.CompanyState,
                           CompanyCountry = Priuser.CompanyCountry,
                           CompanyPostelcode = Priuser.CompanyPostelcode,
                           CompanyContact = Priuser.CompanyContact,
                           GuardianFirstname = gu.GuardianFirstname,
                           GuardianLastName = gu.GuardianLastName,
                           GuRelationshipToChild = gu.GuRelationshipToChild,
                           GuardianDOB = gu.GuardianDOB,
                           GuardianGender = gu.GuardianGender,
                           GuardianQualification = gu.GuardianQualification,
                           GuardianMobileNo = gu.GuardianMobileNo,
                           GuardianEmail = gu.GuardianEmail,
                           GuardianOccupation = gu.GuardianOccupation,
                           GuardianIncome = gu.GuardianIncome,
                           LocAddress1 = stu.LocAddress1,
                           LocAddress2 = stu.LocAddress2,
                           LocCity = stu.LocCity,
                           LocState = stu.LocState,
                           LocCountry = stu.LocCountry,
                           LocPostelcode = stu.LocPostelcode,
                           StudentPhoto = stu.StudentPhoto,
                           CommunityCertificate = stu.CommunityCertificate,
                           BirthCertificate = stu.BirthCertificate,
                           TransferCertificate = stu.TransferCertificate,
                           PreSchool = stu.PreSchholName,
                           //PreMedium = f.BoardName,
                           Pre_BoardofSchool = stu.PreSchoolMedium,
                           PreMarks = stu.PreSchoolMark,
                           PreClass = stu.PreSchoolClass,
                           PreFromDate1 = stu.PreSchoolFromdate,
                           PreToDate = stu.PreSchoolToDate,
                           NoOfSbling = stu.NoOfSibling,
                           PrimaryUser = Priuser.PrimaryUser,
                           EmergencyContactPersonName = stu.EmergencyContactPersonName,
                           EmergencyContactNumber = stu.EmergencyContactNumber,
                           ContactPersonRelationship = stu.ContactPersonRelationship,
                           Email = stu.Email,
                           Contact = stu.Contact,
                           Height = stu.Height,
                           Weights = stu.Weights,
                           IdentificationMark = stu.IdentificationMark,
                           PerAddress1 = stu.PerAddress1,
                           PerAddress2 = stu.PerAddress2,
                           PerCity = stu.PerCity,
                           PerState = stu.PerState,
                           PerCountry = stu.PerCountry,
                           PerPostelcode = stu.PerPostelcode,
                           //TotalIncome = Priuser.TotalIncome,
                           GuardianRequried = stu.GuardianRequried,
                           IncomeCertificate = stu.IncomeCertificate,
                           AcademicyearId = stu.AcademicyearId,
                           Distance = stu.Distance,
                           txt_academicyear = a.AcademicYear1,
                           txt_ApplyClass = b.ClassType,
                           txt_community = c.CommunityName,
                           txt_ComCountry = Priuser.CompanyCountry,
                           txt_LocCountry = stu.LocCountry,
                           txt_PerCountry = stu.PerCountry,
                           txt_bloodgroup = e.BloodGroupName,
                           primaryUserEmail = Priuser.PrimaryUserEmail,
                           EmployeeDesignationId = Priuser.EmployeeDesignationId,
                           EmployeeId = Priuser.EmployeeId,
                           WorkSameSchool = Priuser.WorkSameSchool,
                           EmailRequired = Priuser.EmailRequired,
                           SmsRequired = Priuser.SmsRequired,
                           TransportRequired = stu.TransportRequired,
                           TransportDestination = stu.TransportDesignation,
                           TransportPickpoint = stu.TransportPickpoint,
                           TransportFeeAmount = stu.TransportFeeAmount,
                           HostelRequired = stu.HostelRequired,
                           AccommodationFeeCategoryId = stu.AccommodationFeeCategoryId,
                           AccommodationSubFeeCategoryId = stu.AccommodationSubFeeCategoryId,
                           FoodFeeCategoryId = stu.FoodFeeCategoryId,
                           FoodSubFeeCategoryId = stu.FoodSubFeeCategoryId,
                           primaryUserContact = Priuser.PrimaryUserContactNo
                       }).Distinct().ToList();
            return res;
        }


        public int AddStudentdetailsForm1(int regid, string sfname, string smname, string slname, string sgender, DateTime? dob, string pob, int? community, string religion, string nationality, int? blood, int? applyclass,
     string addr1, string addr2, string city, string state, string country, long? postel, int? NoOfsibling, int? acyear, int? distance, string siblingstatus, string gurdianreg, string emergencycontactperson, long? emergencycontactnumber, string emergencypersonRelationship, string studentemail, long? studentmobile)
        {
            PreAdmissionStudentRegister add = new PreAdmissionStudentRegister()
            {
                OnlineRegisterId = regid,
                StuFirstName = sfname,
                StuMiddlename = smname,
                StuLastname = slname,
                Gender = sgender,
                DOB = dob,
                PlaceOfBirth = pob,
                Community = community,
                Religion = religion,
                Nationality = nationality,
                BloodGroup = blood,
                AdmissionClass = applyclass,
                LocAddress1 = addr1,
                LocAddress2 = addr2,
                LocCity = city,
                LocState = state,
                LocCountry = country,
                LocPostelcode = postel,
                NoOfSibling = NoOfsibling,
                AcademicyearId = acyear,
                Distance = distance,
                SiblingStatus = siblingstatus,
                GuardianRequried =gurdianreg,
                EmergencyContactPersonName=emergencycontactperson,
                EmergencyContactNumber=emergencycontactnumber,
                ContactPersonRelationship=emergencypersonRelationship,
                Statusflag ="InComplete",
                Email = studentemail,
                Contact = studentmobile
            };
            dc.PreAdmissionStudentRegisters.Add(add);
            dc.SaveChanges();

            return add.StudentAdmissionId;
        }

        public int UpdateStudentdetailsForm1(int regid, string sfname, string smname, string slname, string sgender, DateTime? dob, string pob, int? community, string religion, string nationality, int? blood, int? applyclass,
           string addr1, string addr2, string city, string state, string country, long? postel, int? NoOfsibling, int? acyear, int? distance, string siblingstatus, int? stuid, string gurdianreg, string emergencycontactperson, long? emergencycontactnumber, string emergencypersonRelationship, string studentemail, long? studentmobile)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {

                getrow.StuFirstName = sfname;
                getrow.StuMiddlename = smname;
                getrow.StuLastname = slname;
                getrow.Gender = sgender;
                getrow.DOB = dob;
                getrow.PlaceOfBirth = pob;
                getrow.Community = community;
                getrow.Religion = religion;
                getrow.Nationality = nationality;
                getrow.BloodGroup = blood;
                getrow.AdmissionClass = applyclass;
                getrow.LocAddress1 = addr1;
                getrow.LocAddress2 = addr2;
                getrow.LocCity = city;
                getrow.LocState = state;
                getrow.LocCountry = country;
                getrow.LocPostelcode = postel;
                getrow.NoOfSibling = NoOfsibling;
                getrow.AcademicyearId = acyear;
                getrow.Distance = distance;
                getrow.GuardianRequried = gurdianreg;
                getrow.SiblingStatus = siblingstatus;
                getrow.EmergencyContactPersonName = emergencycontactperson;
                getrow.EmergencyContactNumber = emergencycontactnumber;
                getrow.ContactPersonRelationship = emergencypersonRelationship;
                getrow.Email=studentemail;
                getrow.Contact = studentmobile;
                dc.SaveChanges();
            }
            return getrow.StudentAdmissionId;
        }
        public PreAdmissionStudentRegister findStudent(int id)
        {
            var a = dc.PreAdmissionStudentRegisters.Where(q => q.StudentAdmissionId ==id && q.Statusflag == "InComplete").FirstOrDefault();
            return a;
        }

        //Previous School InFormation
        public void UpdatePreviousSchool(int stuid, string schoolname, string claas, int? medium, string mark, DateTime? from, DateTime? to)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where  a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.PreSchholName = schoolname;
                getrow.PreSchoolMedium = medium;
                getrow.PreSchoolMark = mark;
                getrow.PreSchoolFromdate = from;
                getrow.PreSchoolToDate = to;
                getrow.PreSchoolClass = claas;
                dc.SaveChanges();
            }

        }

        public M_ParentSibling GetStudentApplicationdetails(int sid)
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters
                       from b in dc.AcademicYears
                       from c in dc.Classes
                       where a.StudentAdmissionId == sid && b.AcademicYearId == a.AcademicyearId && c.ClassId == a.AdmissionClass
                       select new M_ParentSibling
                       {
                           Academicyear = b.AcademicYear1,
                           classs = c.ClassType,
                           Studentid=a.OfflineApplicationID
                       }).FirstOrDefault();
            return ans;
        }


        public PreAdmissionStudentRegister getStudentid(int id)
        {
            var a = dc.PreAdmissionStudentRegisters.Where(q => q.OnlineRegisterId.Value.Equals(id)).OrderByDescending(q => q.StudentAdmissionId).FirstOrDefault();
            return a;

        }
        //uploadTransfercertificate
        public void updateTransfercertificate(int regid, int stuid, string transfer)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.OnlineRegisterId == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.TransferCertificate = transfer;
                dc.SaveChanges();
            }
        }

        //uploadCommunitycertificate
        public void updateCommunitycertificate(int regid, int stuid, string community)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.OnlineRegisterId == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.CommunityCertificate = community;
                dc.SaveChanges();
            }
        }

        //uploadBirthcertificate
        public void updateBirthcertificate(int regid, int stuid, string birth)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.OnlineRegisterId == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.BirthCertificate = birth;
                dc.SaveChanges();
            }
        }

        //uploadStudentPhoto
        public void updateStudentPhoto(int regid, int stuid, string photo)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.OnlineRegisterId == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.StudentPhoto = photo;
                dc.SaveChanges();
            }
        }
        //upload IncomeCertificate
        public void updateIncomecertificate(int regid, int stuid, string Income)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.OnlineRegisterId == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.IncomeCertificate = Income;
                dc.SaveChanges();
            }
        }

        //GetStatusFlagViewPageDetails

        public List<M_GetFlagDetails> GetStatusFlagViewPageDetails(int regid)
        {
            var ans = (from a in dc.PreAdmissionPrimaryUserRegisters
                       from b in dc.SchoolSettings
                       from c in dc.AcademicYears
                       from d in dc.Classes
                       from e in dc.PreAdmissionStudentRegisters
                       from f in dc.PreAdmissionTransactions
                       where e.StudentAdmissionId == regid && e.AcademicyearId == c.AcademicYearId && e.AdmissionClass == d.ClassId && f.StudentAdmissionId.Value.Equals(e.StudentAdmissionId) && f.PrimaryUserAdmissionId.Value.Equals(a.PrimaryUserAdmissionId)
                       select new M_GetFlagDetails
                       {
                           PrimaryUserName = a.PrimaryUserName,
                           SchoolLogo = b.SchoolLogo,
                           SchoolName = b.SchoolName,
                           StudentName = e.StuFirstName + " " + e.StuLastname,
                           ApplicationNo = e.StudentAdmissionId,
                           Txt_Academicyear = c.AcademicYear1,
                           Txt_class = d.ClassType,
                           OnlineregisterId=a.OnlineRegisterId
                       }).ToList();
            return ans;

        }
        public List<AdminSelectionList> GetStatusflagApplicationList(int? acyear, string status)
        {
            var ans = (
                      from a in dc.PreAdmissionStudentRegisters
                      from b in dc.PreAdmissionTransactions
                      from c in dc.PreAdmissionPrimaryUserRegisters
                      from d in dc.Classes
                      where a.AcademicyearId == acyear && b.StudentAdmissionId == a.StudentAdmissionId && c.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId && a.AdmissionClass == d.ClassId && a.Statusflag == status
                      select new 
                      {
                          StudentAdmissionId = a.StudentAdmissionId,
                          PrimaryUserName = c.PrimaryUserName,
                          StuFirstName = a.StuFirstName,
                          txt_AdmissionClass = d.ClassType,
                          Distance = a.Distance,
                          SiblingStatus = a.SiblingStatus,
                          WorkSameSchool = c.WorkSameSchool,
                          statusFlag = a.Statusflag,
                          ApplicationSource = a.ApplicationSource
                      }).ToList().Select(a => new AdminSelectionList
                      {
                          StudentAdmissionId = a.StudentAdmissionId,
                          ApplicationId = QSCrypt.Encrypt(a.StudentAdmissionId.ToString()),
                          PrimaryUserName = a.PrimaryUserName,
                          StuFirstName = a.StuFirstName,
                          txt_AdmissionClass = a.txt_AdmissionClass,
                          Distance = a.Distance + "Km",
                          SiblingStatus = a.SiblingStatus,
                          WorkSameSchool = a.WorkSameSchool,
                          statusFlag = a.statusFlag,
                          StudentName = a.StuFirstName,
                          ApplyClass = a.txt_AdmissionClass,
                          ApplicationSource = a.ApplicationSource
                      }).ToList();
            return ans;
        }

        public List<AdminSelectionList> GetYearAndClass(int OnlineRegid)
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters
                       from b in dc.AcademicYears
                       from c in dc.Classes
                       where a.StudentAdmissionId == OnlineRegid && b.AcademicYearId == a.AcademicyearId && c.ClassId == a.AdmissionClass
                       select new AdminSelectionList
                       {
                           AcademicyearId = a.AcademicyearId,
                           ClassId = a.AdmissionClass,
                           StuFirstName=a.StuFirstName+" "+a.StuLastname,
                           txt_AcademicYear = b.AcademicYear1,
                           txt_AdmissionClass = c.ClassType,
                           StudentAdmissionId = a.StudentAdmissionId,
                           Photo=a.StudentPhoto,
                           OnlineRegid=a.OnlineRegisterId
                       }).ToList();
            return ans;
        }
        public List<M_AdmissionConfirmationList> GetFeesPaidList(int? year, int? classid)
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters

                       from b in dc.AcademicYears
                       from c in dc.Classes
                       from d in dc.PreAdmissionPrimaryUserRegisters
                       from e in dc.PreAdmissionTransactions
                       where a.AcademicyearId == year && a.AdmissionClass == classid && b.AcademicYearId == a.AcademicyearId && c.ClassId == a.AdmissionClass && e.StudentAdmissionId == a.StudentAdmissionId && d.PrimaryUserAdmissionId == e.PrimaryUserAdmissionId && a.Statusflag == "FeesPaid"
                       select new M_AdmissionConfirmationList
                       {
                           OnlineRegid = a.OnlineRegisterId,
                           StudentAdmissionId = a.StudentAdmissionId,
                           PrimaryUserName = d.PrimaryUserName,
                           StuFirstName = a.StuFirstName,
                           txt_AdmissionClass = c.ClassType,
                           Applicationstatus = a.Statusflag,
                           ApplicationSource = a.ApplicationSource
                       }).ToList().Select(a => new M_AdmissionConfirmationList
                       {
                           OnlineRegid = a.OnlineRegid,
                           ApplicationId = QSCrypt.Encrypt(a.StudentAdmissionId.ToString()),
                           PrimaryUserName = a.PrimaryUserName,
                           StuFirstName = a.StuFirstName,
                           txt_AdmissionClass = a.txt_AdmissionClass,
                           StudentAdmissionId = a.StudentAdmissionId,
                           Applicationstatus = a.Applicationstatus,
                           StudentName = a.StuFirstName,                           
                           ApplyClass = a.txt_AdmissionClass,
                           ApplicationSource = a.ApplicationSource,
                           Source=a.ApplicationSource,
                           AdmissionId=a.StudentAdmissionId
                       }).ToList();
            return ans;
        }
        public List<M_StudentRegisteration> GetStudentAlldetails(int primaryuserid,int studentid)
        {
            List<M_StudentRegisteration> SS = new List<M_StudentRegisteration>();
            var Trans = dc.PreAdmissionTransactions.Where(q => q.PrimaryUserAdmissionId.Value.Equals(primaryuserid) && q.StudentAdmissionId.Value.Equals(studentid)).FirstOrDefault();
            var primaryuser = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserAdmissionId == primaryuserid).FirstOrDefault().PrimaryUser;
            if(Trans != null)
            {
                if(primaryuser == "Father")
                {
                    if(Trans.MotherAdmissionId !=null && Trans.GuardianAdmissionId !=null)
                    {
                        SS = StudentAllUserdetails(primaryuserid, studentid);
                    }
                    else if(Trans.MotherAdmissionId != null && Trans.GuardianAdmissionId == null)
                    {
                        SS = StudentFatherMotherDetails(primaryuserid, studentid);
                    }
                    else if(Trans.MotherAdmissionId == null && Trans.GuardianAdmissionId !=null)
                    {
                        SS = StudentFatherGuardianDetails(primaryuserid, studentid);
                    }
                    else
                    {
                        SS = StudentFatherDetails(primaryuserid, studentid);
                    }
                }
                else if(primaryuser == "Mother")
                {
                    if(Trans.FatherAdmissionId != null && Trans.GuardianAdmissionId != null)
                    {
                        SS = StudentAllUserdetails(primaryuserid, studentid);
                    }
                    else if (Trans.FatherAdmissionId != null && Trans.GuardianAdmissionId == null)
                    {
                        SS = StudentFatherMotherDetails(primaryuserid, studentid);
                    }
                    else if(Trans.FatherAdmissionId == null && Trans.GuardianAdmissionId != null)
                    {
                        SS = StudentMotherGuardianDetails(primaryuserid, studentid);
                    }
                    else
                    {
                        SS = StudentMotherDetails(primaryuserid, studentid);
                    }
                }
                else if(primaryuser == "Guardian")
                {
                    if(Trans.FatherAdmissionId !=null && Trans.GuardianAdmissionId != null)
                    {
                        SS = StudentAllUserdetails(primaryuserid, studentid);
                    }
                    else if(Trans.FatherAdmissionId != null && Trans.MotherAdmissionId == null)
                    {
                        SS = StudentFatherGuardianDetails(primaryuserid, studentid);
                    }
                    else if(Trans.FatherAdmissionId == null && Trans.MotherAdmissionId !=null)
                    {
                        SS = StudentMotherGuardianDetails(primaryuserid, studentid);

                    }
                    else
                    {
                        SS = StudentGuardianDetails(primaryuserid, studentid);
                    }
                }
                return SS;
               
            }
            else
            {
                var ans = (from a in dc.PreAdmissionStudentRegisters
                           where a.OnlineRegisterId == 0
                           select new M_StudentRegisteration
                           {

                           }).ToList();
                return ans;
            }
        }

        //update status flag for online application
        DateTime date = DateTimeByZone.getCurrentDate();
        public bool UpdateStudentRegisterStatus(int regid, string statusflag)
        {
            
            var ans = dc.PreAdmissionStudentRegisters.Where(q => q.StudentAdmissionId == regid).FirstOrDefault();
            if (ans != null)
            {
                ans.Statusflag = statusflag;
                ans.ApplicationSource = "OnlineForm";
                ans.DateOfApply = Convert.ToDateTime(date);
                dc.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }

        }
        public bool UpdateStudentRegisterStatusOffline(int regid, string statusflag,int Empid)
        {
            var ans = dc.PreAdmissionStudentRegisters.Where(q => q.StudentAdmissionId == regid).FirstOrDefault();
            if (ans != null)
            {
                ans.Statusflag = statusflag;
                ans.ApplicationSource = "OfflineForm";
                ans.DateOfApply = Convert.ToDateTime(date);
                ans.EnrolledBy = Empid;
                dc.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }

        }

        public PreAdmissionStudentRegister getOnlineregid(int sid)
        {
            var ans = dc.PreAdmissionStudentRegisters.Where(q => q.StudentAdmissionId == sid).FirstOrDefault();
            return ans;
        }


        //Steps Form

        public int AddStudentdetailsForm4(int regid, string sfname, string smname, string slname, string sgender, DateTime? dob, string pob, int? community, string religion, string nationality, int? applyclass,
   int? acyear,  string gurdianreg)
        {
            PreAdmissionStudentRegister add = new PreAdmissionStudentRegister()
            {
                OnlineRegisterId = regid,
                StuFirstName = sfname,
                StuMiddlename = smname,
                StuLastname = slname,
                Gender = sgender,
                DOB = dob,
                PlaceOfBirth = pob,
                Community = community,
                Religion = religion,
                Nationality = nationality,                
                AdmissionClass = applyclass,                                
                AcademicyearId = acyear,                
                GuardianRequried = gurdianreg,
                Statusflag = "InComplete"
            };
            dc.PreAdmissionStudentRegisters.Add(add);
            dc.SaveChanges();
            return add.StudentAdmissionId;
        }
        public int UpdateStudentdetailsForm4(int regid,int stuid, string sfname, string smname, string slname, string sgender, DateTime? dob, string pob, int? community, string religion, string nationality, int? applyclass,
int? acyear, string gurdianreg)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {

                getrow.StuFirstName = sfname;
                getrow.StuMiddlename = smname;
                getrow.StuLastname = slname;
                getrow.Gender = sgender;
                getrow.DOB = dob;
                getrow.PlaceOfBirth = pob;
                getrow.Community = community;
                getrow.Religion = religion;
                getrow.Nationality = nationality;             
                getrow.AdmissionClass = applyclass;                
                getrow.AcademicyearId = acyear;                
                getrow.GuardianRequried = gurdianreg;
                dc.SaveChanges();
            }
            return getrow.StudentAdmissionId;
        }


        public int UpdatestudentAddressothersDetailsForm4(string addr1, string addr2, string city, string state, string country, long? postel, int? NoOfsibling, string height, string weight, string identificationmark, string email, long? contact, 
            string peraddr1, string peraddr2, string percity, string perstate, string percountry, long? perpostel,int? distance, string siblingstatus, int? stuid, int? blood, string emergencycontactperson, long? emergencycontactnumber, string emergencypersonRelationship)
        {
             var getrow = (from a in dc.PreAdmissionStudentRegisters where a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if(getrow != null)
            {
                getrow.LocAddress1 = addr1;
                getrow.LocAddress2 = addr2;
                getrow.LocCity = city;
                getrow.LocState = state;
                getrow.LocCountry = country;
                getrow.LocPostelcode = postel;
                getrow.NoOfSibling=NoOfsibling;
                getrow.Height=height;
                getrow.Weights=weight;
                getrow.IdentificationMark=identificationmark;
                getrow.Email=email;
                getrow.Contact=contact;
                getrow.PerAddress1=peraddr1;
                getrow.PerAddress2=peraddr2;
                getrow.PerCity=percity;
                getrow.PerState=perstate;
                getrow.PerCountry=percountry;
                getrow.PerPostelcode=perpostel;
                getrow.Distance=distance;
                getrow.SiblingStatus=siblingstatus;
                getrow.BloodGroup=blood;
                 getrow.EmergencyContactPersonName = emergencycontactperson;
                getrow.EmergencyContactNumber = emergencycontactnumber;
                getrow.ContactPersonRelationship = emergencypersonRelationship;
                dc.SaveChanges();
            }
            return getrow.StudentAdmissionId;

        }


        //Offline Application code

        public PreAdmissionStudentRegister GetOfflineStudentdetails (string offlineid)
        {
            var ans = dc.PreAdmissionStudentRegisters.Where(q => q.OfflineApplicationID == offlineid).FirstOrDefault();
            return ans;
        }

        public List<M_StudentRegisteration> GetPreviousApplicationDetails(string email)
        {
            var Puser = new PreAdmissionPrimaryUserRegister();
            List<M_StudentRegisteration> SS = new List<M_StudentRegisteration>();
            string MatchEmailPattern =
                       @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";            
            if (Regex.IsMatch(email, MatchEmailPattern))
            {
                Puser = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserEmail == email).FirstOrDefault();
            }
            else
            {
                long contact = Convert.ToInt64(email);
                Puser = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserContactNo == contact).FirstOrDefault();
            }
            if(Puser!=null)
            {
                int pid = Puser.PrimaryUserAdmissionId;

                var Trans = dc.PreAdmissionTransactions.Where(q => q.PrimaryUserAdmissionId.Value.Equals(pid)).OrderByDescending(q => q.PreAdmissionTransactionId).FirstOrDefault();
                if (Trans.StudentAdmissionId == null)
                {
                    if (Puser.PrimaryUser == "Father")
                    {
                        if (Trans.MotherAdmissionId == null && Trans.GuardianAdmissionId == null)
                        {
                            var result = OldStudentFatherdetails(pid);
                            SS = result;
                        }
                        else if (Trans.MotherAdmissionId != null && Trans.GuardianAdmissionId == null)
                        {
                            var result = OldStudentFatherMotherdetails(pid);

                            SS = result;
                        }
                        else if (Trans.MotherAdmissionId == null && Trans.GuardianAdmissionId != null)
                        {
                            var result = OldStudentFatherGuardiandetails(pid);
                            SS = result;
                        }
                        else
                        {
                            var result = OldStudentAllUserdetails(pid);
                            SS = result;
                        }
                    }
                    else if (Puser.PrimaryUser == "Mother")
                    {
                        if (Trans.FatherAdmissionId == null && Trans.GuardianAdmissionId == null)
                        {
                            var result = OldStudentMotherdetails(pid);
                            SS = result;
                        }
                        else if (Trans.FatherAdmissionId != null && Trans.GuardianAdmissionId == null)
                        {
                            var result = OldStudentFatherMotherdetails(pid);
                            SS = result;
                        }
                        else if (Trans.FatherAdmissionId == null && Trans.GuardianAdmissionId != null)
                        {
                            var result = OldStudentMotherGuardiandetails(pid);
                            SS = result;
                        }
                        else
                        {
                            var result = OldStudentAllUserdetails(pid);
                            SS = result;
                        }

                    }
                    else if (Puser.PrimaryUser == "Guardian")
                    {
                        if (Trans.MotherAdmissionId == null && Trans.FatherAdmissionId == null)
                        {
                            var result = OldStudentGuardiandetails(pid);
                            SS = result;
                        }
                        else if (Trans.MotherAdmissionId != null && Trans.FatherAdmissionId == null)
                        {

                            var result = OldStudentMotherGuardiandetails(pid);
                            SS = result;
                        }
                        else if (Trans.MotherAdmissionId == null && Trans.FatherAdmissionId != null)
                        {

                            var result = OldStudentFatherGuardiandetails(pid);
                            SS = result;
                        }
                        else
                        {
                            var result = OldStudentAllUserdetails(pid);
                            SS = result;
                        }

                    }
                }
                else
                {
                    if (Puser.PrimaryUser == "Father")
                    {
                        if (Trans.MotherAdmissionId != null && Trans.GuardianAdmissionId != null)
                        {
                            var result = NewStudentAllUserdetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                            SS = result;
                        }
                        else if (Trans.MotherAdmissionId != null && Trans.GuardianAdmissionId == null)
                        {
                            var result = NewStudentFatherMotherDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                            SS = result;
                        }
                        else if (Trans.MotherAdmissionId == null && Trans.GuardianAdmissionId != null)
                        {
                            var result = NewStudentFatherGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                            SS = result;
                        }
                        else
                        {
                            var result = NewStudentFatherDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                            SS = result;
                        }
                    }
                    else if (Puser.PrimaryUser == "Mother")
                    {
                        if (Trans.FatherAdmissionId != null && Trans.GuardianAdmissionId != null)
                        {
                            var result = NewStudentAllUserdetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                            SS = result;
                        }
                        else if (Trans.FatherAdmissionId != null && Trans.GuardianAdmissionId == null)
                        {
                            var result = NewStudentFatherMotherDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                            SS = result;
                        }
                        else if (Trans.FatherAdmissionId == null && Trans.GuardianAdmissionId != null)
                        {
                            var result = NewStudentMotherGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                            SS = result;
                        }
                        else
                        {
                            var result = NewStudentMotherDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                            SS = result;
                        }
                    }
                    else if (Puser.PrimaryUser == "Guardian")
                    {
                        if (Trans.FatherAdmissionId != null && Trans.MotherAdmissionId != null)
                        {
                            var result = NewStudentAllUserdetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                            SS = result;
                        }
                        else if (Trans.FatherAdmissionId != null && Trans.MotherAdmissionId == null)
                        {
                            var result = NewStudentFatherGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                            SS = result;
                        }
                        else if (Trans.FatherAdmissionId == null && Trans.MotherAdmissionId != null)
                        {
                            var result = NewStudentMotherGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                            SS = result;
                        }
                        else
                        {
                            var result = NewStudentGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                            SS = result;
                        }
                    }
                }
               
            }
            return SS;
        }
        public List<M_StudentRegisteration> findParticularStudentDetailsOffline(string OfflineId, int Sid)
        {
            List<M_StudentRegisteration> SS = new List<M_StudentRegisteration>();

            var PrimaryUserId = (from a in dc.PreAdmissionStudentRegisters
                                 from b in dc.PreAdmissionTransactions
                                 from c in dc.PreAdmissionPrimaryUserRegisters
                                 where a.OfflineApplicationID == OfflineId && b.StudentAdmissionId == a.StudentAdmissionId && c.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId
                                 select c).ToList(); //dc.PreAdmissionPrimaryUserRegisters.Where(q => q.OfflineApplicationID == OfflineId).ToList();
            if (PrimaryUserId.Count != 0)
            {
                if (PrimaryUserId.FirstOrDefault().PrimaryUserAdmissionId != null)
                {
                    int pid = PrimaryUserId.FirstOrDefault().PrimaryUserAdmissionId;

                    var Trans = dc.PreAdmissionTransactions.Where(q => q.PrimaryUserAdmissionId.Value.Equals(pid)).OrderByDescending(q => q.PreAdmissionTransactionId).FirstOrDefault();

                    if (Sid == 0 || Trans.StudentAdmissionId == null)
                    {
                        if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Father")
                        {

                            if (Trans.MotherAdmissionId == null && Trans.GuardianAdmissionId == null)
                            {
                                var result = OldStudentFatherdetails(pid);
                                SS = result;
                            }
                            else if (Trans.MotherAdmissionId != null && Trans.GuardianAdmissionId == null)
                            {
                                var result = OldStudentFatherMotherdetails(pid);

                                SS = result;
                            }
                            else if (Trans.MotherAdmissionId == null && Trans.GuardianAdmissionId != null)
                            {
                                var result = OldStudentFatherGuardiandetails(pid);
                                SS = result;
                            }
                            else
                            {
                                var result = OldStudentAllUserdetails(pid);
                                SS = result;
                            }

                        }
                        else if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Mother")
                        {
                            if (Trans.FatherAdmissionId == null && Trans.GuardianAdmissionId == null)
                            {
                                var result = OldStudentMotherdetails(pid);
                                SS = result;
                            }
                            else if (Trans.FatherAdmissionId != null && Trans.GuardianAdmissionId == null)
                            {
                                var result = OldStudentFatherMotherdetails(pid);
                                SS = result;
                            }
                            else if (Trans.FatherAdmissionId == null && Trans.GuardianAdmissionId != null)
                            {
                                var result = OldStudentMotherGuardiandetails(pid);
                                SS = result;
                            }
                            else
                            {
                                var result = OldStudentAllUserdetails(pid);
                                SS = result;
                            }

                        }
                        else if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Guardian")
                        {
                            if (Trans.MotherAdmissionId == null && Trans.FatherAdmissionId == null)
                            {
                                var result = OldStudentGuardiandetails(pid);
                                SS = result;
                            }
                            else if (Trans.MotherAdmissionId != null && Trans.FatherAdmissionId == null)
                            {

                                var result = OldStudentMotherGuardiandetails(pid);
                                SS = result;
                            }
                            else if (Trans.MotherAdmissionId == null && Trans.FatherAdmissionId != null)
                            {

                                var result = OldStudentFatherGuardiandetails(pid);
                                SS = result;
                            }
                            else
                            {
                                var result = OldStudentAllUserdetails(pid);
                                SS = result;
                            }

                        }

                    }
                    else if (Trans.StudentAdmissionId != null)
                    {
                        if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Father")
                        {
                            if (Trans.MotherAdmissionId != null && Trans.GuardianAdmissionId != null)
                            {
                                var result = NewStudentAllUserdetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else if (Trans.MotherAdmissionId != null && Trans.GuardianAdmissionId == null)
                            {
                                var result = NewStudentFatherMotherDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else if (Trans.MotherAdmissionId == null && Trans.GuardianAdmissionId != null)
                            {
                                var result = NewStudentFatherGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else
                            {
                                var result = NewStudentFatherDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                        }
                        else if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Mother")
                        {
                            if (Trans.FatherAdmissionId != null && Trans.GuardianAdmissionId != null)
                            {
                                var result = NewStudentAllUserdetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else if (Trans.FatherAdmissionId != null && Trans.GuardianAdmissionId == null)
                            {
                                var result = NewStudentFatherMotherDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else if (Trans.FatherAdmissionId == null && Trans.GuardianAdmissionId != null)
                            {
                                var result = NewStudentMotherGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else
                            {
                                var result = NewStudentMotherDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                        }
                        else if (PrimaryUserId.FirstOrDefault().PrimaryUser == "Guardian")
                        {
                            if (Trans.FatherAdmissionId != null && Trans.MotherAdmissionId != null)
                            {
                                var result = NewStudentAllUserdetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else if (Trans.FatherAdmissionId != null && Trans.MotherAdmissionId == null)
                            {
                                var result = NewStudentFatherGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else if (Trans.FatherAdmissionId == null && Trans.MotherAdmissionId != null)
                            {
                                var result = NewStudentMotherGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                            else
                            {
                                var result = NewStudentGuardianDetails(pid, Convert.ToInt16(Trans.StudentAdmissionId));
                                SS = result;
                            }
                        }
                    }
                }
                return SS;
            }
            else
            {
                var ans = (from a in dc.PreAdmissionStudentRegisters
                           where a.OnlineRegisterId == 0
                           select new M_StudentRegisteration
                           {

                           }).ToList();
                return ans;
            }

        }

        public int AddStudentdetailsFormOffline1(string Offlineid, string sfname, string smname, string slname, string sgender, DateTime? dob, string pob, int? community, string religion, string nationality, int? blood, int? applyclass,
    string addr1, string addr2, string city, string state, string country, long? postel, int? NoOfsibling, int? acyear, int? distance, string siblingstatus, string gurdianreg, string emergencycontactperson, long? emergencycontactnumber, string emergencypersonRelationship, string studentemail, long? studentmobile)
        {
            PreAdmissionStudentRegister add = new PreAdmissionStudentRegister()
            {
                OfflineApplicationID = Offlineid,
                StuFirstName = sfname,
                StuMiddlename = smname,
                StuLastname = slname,
                Gender = sgender,
                DOB = dob,
                PlaceOfBirth = pob,
                Community = community,
                Religion = religion,
                Nationality = nationality,
                BloodGroup = blood,
                AdmissionClass = applyclass,
                LocAddress1 = addr1,
                LocAddress2 = addr2,
                LocCity = city,
                LocState = state,
                LocCountry = country,
                LocPostelcode = postel,
                NoOfSibling = NoOfsibling,
                AcademicyearId = acyear,
                Distance = distance,
                SiblingStatus = siblingstatus,
                GuardianRequried = gurdianreg,
                EmergencyContactPersonName = emergencycontactperson,
                EmergencyContactNumber = emergencycontactnumber,
                ContactPersonRelationship = emergencypersonRelationship,
                Statusflag = "InComplete",
                Email=studentemail,
                Contact = studentmobile
            };
            dc.PreAdmissionStudentRegisters.Add(add);
            dc.SaveChanges();

            return add.StudentAdmissionId;
        }

        public int UpdateStudentdetailsFormOffline1(string Offlineid, string sfname, string smname, string slname, string sgender, DateTime? dob, string pob, int? community, string religion, string nationality, int? blood, int? applyclass,
           string addr1, string addr2, string city, string state, string country, long? postel, int? NoOfsibling, int? acyear, int? distance, string siblingstatus, string gurdianreg, string emergencycontactperson, long? emergencycontactnumber, string emergencypersonRelationship, string studentemail, long? studentmobile)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.OfflineApplicationID == Offlineid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.StuFirstName = sfname;
                getrow.StuMiddlename = smname;
                getrow.StuLastname = slname;
                getrow.Gender = sgender;
                getrow.DOB = dob;
                getrow.PlaceOfBirth = pob;
                getrow.Community = community;
                getrow.Religion = religion;
                getrow.Nationality = nationality;
                getrow.BloodGroup = blood;
                getrow.AdmissionClass = applyclass;
                getrow.LocAddress1 = addr1;
                getrow.LocAddress2 = addr2;
                getrow.LocCity = city;
                getrow.LocState = state;
                getrow.LocCountry = country;
                getrow.LocPostelcode = postel;
                getrow.NoOfSibling = NoOfsibling;
                getrow.AcademicyearId = acyear;
                getrow.Distance = distance;
                getrow.GuardianRequried = gurdianreg;
                getrow.SiblingStatus = siblingstatus;
                getrow.EmergencyContactPersonName = emergencycontactperson;
                getrow.EmergencyContactNumber = emergencycontactnumber;
                getrow.ContactPersonRelationship = emergencypersonRelationship;
                getrow.Email=studentemail;
                getrow.Contact = studentmobile;
                dc.SaveChanges();
            }
            return getrow.StudentAdmissionId;
        }


        //form3
        public int AddStudentdetailsFormOffline3(string Offlineid, string sfname, string smname, string slname, string sgender, DateTime? dob, string pob, int? community, string religion, string nationality, int? applyclass,
 int? NoOfsibling, int? acyear, string siblingstatus, string gurdianreg)
        {
            PreAdmissionStudentRegister add = new PreAdmissionStudentRegister()
            {
                OfflineApplicationID = Offlineid,
                StuFirstName = sfname,
                StuMiddlename = smname,
                StuLastname = slname,
                Gender = sgender,
                DOB = dob,
                PlaceOfBirth = pob,
                Community = community,
                Religion = religion,
                Nationality = nationality,
                AdmissionClass = applyclass,
                NoOfSibling = NoOfsibling,
                AcademicyearId = acyear,              
                SiblingStatus = siblingstatus,
                GuardianRequried = gurdianreg,
                Statusflag = "InComplete"
            };
            dc.PreAdmissionStudentRegisters.Add(add);
            dc.SaveChanges();
            return add.StudentAdmissionId;
        }

        public int UpdateStudentdetailsFormOffline3(string Offlineid, string sfname, string smname, string slname, string sgender, DateTime? dob, string pob, int? community, string religion, string nationality, int? applyclass,
       int? NoOfsibling, int? acyear, string siblingstatus, string gurdianreg)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.OfflineApplicationID == Offlineid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.StuFirstName = sfname;
                getrow.StuMiddlename = smname;
                getrow.StuLastname = slname;
                getrow.Gender = sgender;
                getrow.DOB = dob;
                getrow.PlaceOfBirth = pob;
                getrow.Community = community;
                getrow.Religion = religion;
                getrow.Nationality = nationality;
                getrow.AdmissionClass = applyclass;
                getrow.NoOfSibling = NoOfsibling;
                getrow.AcademicyearId = acyear;              
                getrow.GuardianRequried = gurdianreg;
                getrow.SiblingStatus = siblingstatus;
                dc.SaveChanges();
            }
            return getrow.StudentAdmissionId;
        }
        private bool disposed = false;
        //Admission Process
        public List<PreAdmissionStudentRegister> GetParticularStatusFlagCount(int year, int classid, string statusflag)
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters
                       from b in dc.PreAdmissionTransactions
                       
                       where a.AcademicyearId == year && a.Statusflag == statusflag && a.AdmissionClass == classid && b.StudentAdmissionId ==a.StudentAdmissionId
                       select a).ToList();
            return ans;
        }

        public bool UpdateStudentRegisterStatusSelectionProcess(int regid, string statusflag, DateTime? date)
        {
            var ans = dc.PreAdmissionStudentRegisters.Where(q => q.StudentAdmissionId.Equals(regid)).FirstOrDefault();
            if (ans != null)
            {
               ans.Statusflag = statusflag;
                ans.SendEmailStatus = "No";
                ans.DateOfApply = Convert.ToDateTime(date);
                 ans.FeePayLastDate=date;
                dc.SaveChanges();
                useractivity.AddEmployeeActivityDetails("Update");
                return true;
            }
            else
            {
                return false;
            }
        }

        public AdminSelectionList GetPrimaryUserMaildetails(int id)
        {
            var ans = (
                    from a in dc.PreAdmissionStudentRegisters
                    from b in dc.PreAdmissionPrimaryUserRegisters
                    from c in dc.Classes
                    from d in dc.AcademicYears
                    from e in dc.PreAdmissionTransactions
                    where a.StudentAdmissionId == id && e.StudentAdmissionId == a.StudentAdmissionId && e.PrimaryUserAdmissionId == b.PrimaryUserAdmissionId && c.ClassId == a.AdmissionClass && d.AcademicYearId == a.AcademicyearId
                    select new AdminSelectionList
                    {
                        OnlineRegid = a.OnlineRegisterId,
                        PrimaryUserName = b.PrimaryUserName,
                        StuFirstName = a.StuFirstName,
                        txt_AdmissionClass = c.ClassType,
                        txt_AcademicYear = d.AcademicYear1,
                        statusFlag = a.Statusflag,
                        AcademicyearId = a.AcademicyearId,
                        ClassId = a.AdmissionClass,
                        PrimaryUserEmail = b.PrimaryUserEmail
                    }).ToList().Select(a => new AdminSelectionList
                    {
                        OnlineRegid = a.OnlineRegid,
                        ApplicationId = QSCrypt.Encrypt(a.OnlineRegid.ToString()),
                        PrimaryUserName = a.PrimaryUserName,
                        StuFirstName = a.StuFirstName,
                        txt_AdmissionClass = a.txt_AdmissionClass,
                        txt_AcademicYear = a.txt_AcademicYear,
                        statusFlag = a.statusFlag,
                        AcademicyearId = a.AcademicyearId,
                        ClassId = a.ClassId,
                        PrimaryUserEmail = a.PrimaryUserEmail
                    }).FirstOrDefault();
            return ans;
        }

        public bool UpdateSendEmailStatus(int regid)
        {
            var ans = dc.PreAdmissionStudentRegisters.Where(q => q.StudentAdmissionId == regid).FirstOrDefault();
            if (ans != null)
            {
                ans.SendEmailStatus = "Yes";
                dc.SaveChanges();
                useractivity.AddEmployeeActivityDetails("Update");
                return true;
            }
            else
            {
                return false;
            }

        }

        public PreAdmissionStudentRegister GetFeesApplicationStatus(int StudentAdmissionId, int Acyear, int AdmissionClass)//GetFeesApplicationStatus
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters
                       where a.StudentAdmissionId == StudentAdmissionId && a.AcademicyearId == Acyear && a.AdmissionClass == AdmissionClass 
                       select a).FirstOrDefault();
            return ans;
        }
        public bool UpdateStudentRegisterStatusFeesCollection(int regid, string statusflag)
        {
            var ans = dc.PreAdmissionStudentRegisters.Where(q => q.StudentAdmissionId == regid).FirstOrDefault();
            if (ans != null)
            {
                ans.Statusflag = statusflag;
                dc.SaveChanges();

                PreAdmissionRegisterStatusFlag s = new PreAdmissionRegisterStatusFlag()
                {
                    StatusFlag = statusflag,
                    Descriptions = "Fees Collected",
                    StudentAdmissionId = regid
                };
                dc.PreAdmissionRegisterStatusFlags.Add(s);
                dc.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public void updatepreadmissionfeerefundstatus(int studentapplicationid){
            var s = (from a in dc.PreAdmissionStudentRegisters where a.StudentAdmissionId == studentapplicationid select a).FirstOrDefault();
            s.Statusflag = "Refunded_beforeassignclass";
            dc.SaveChanges();
        }
        public List<PreAdmissionStudentRegister> GetStatusFlagCount(int year, string statusflag)
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters
                       from b in dc.PreAdmissionTransactions
                       where a.AcademicyearId == year && a.Statusflag == statusflag && b.StudentAdmissionId ==a.StudentAdmissionId
                       select a).ToList();
            return ans;

        }
        public List<PreAdmissionStudentRegister> GetStatusFlagCountforClass(int year,int cid, string statusflag)
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters
                       where a.AcademicyearId == year && a.AdmissionClass == cid && a.Statusflag == statusflag
                       select a).ToList();
            return ans;

        }

        //uploadTransfercertificate
        public void updateTransfercertificateOffline(string regid, int stuid, string transfer)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.OfflineApplicationID == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.TransferCertificate = transfer;

                dc.SaveChanges();
            }
        }

        //uploadCommunitycertificate
        public void updateCommunitycertificateOffline(string regid, int stuid, string community)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.OfflineApplicationID == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.CommunityCertificate = community;
                dc.SaveChanges();
            }
        }

        //uploadBirthcertificate
        public void updateBirthcertificateOffline(string regid, int stuid, string birth)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.OfflineApplicationID == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.BirthCertificate = birth;
                dc.SaveChanges();
            }
        }

        //uploadStudentPhoto
        public void updateStudentPhotoOffline(string regid, int stuid, string photo)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.OfflineApplicationID == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.StudentPhoto = photo;
                dc.SaveChanges();
            }
        }
        //upload IncomeCertificate
        public void updateIncomecertificateOffline(string regid, int stuid, string Income)
        {
            var getrow = (from a in dc.PreAdmissionStudentRegisters where a.OfflineApplicationID == regid && a.StudentAdmissionId == stuid select a).FirstOrDefault();
            if (getrow != null)
            {
                getrow.IncomeCertificate = Income;
                dc.SaveChanges();
            }
        }

        public PreAdmissionStudentRegister getStudentidOffline(string id)
        {
            var a = dc.PreAdmissionStudentRegisters.Where(q => q.OfflineApplicationID.Equals(id)).FirstOrDefault();
            return a;

        }

        public PreAdmissionStudentRegister StudentDetails(int studentadmissionid)
        {
            var ans = dc.PreAdmissionStudentRegisters.Where(q => q.StudentAdmissionId == studentadmissionid).FirstOrDefault();
            return ans;
        }

        public int getconditionalofferstudents(int year, int classid)
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters where a.AcademicyearId == year && a.AdmissionClass == classid && a.Statusflag == "Conditionaloffer" select a).ToList().Count();
            return ans;
        }
        public int getcAcceptedstudents(int year, int classid)
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters where a.AcademicyearId == year && a.AdmissionClass == classid && a.Statusflag == "Accepted" select a).ToList().Count();
            return ans;
        }
        public int getFeePaidstudents(int year, int classid)
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters where a.AcademicyearId == year && a.AdmissionClass == classid && a.Statusflag == "FeesPaid" select a).ToList().Count();
            return ans;
        }
      
      
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}