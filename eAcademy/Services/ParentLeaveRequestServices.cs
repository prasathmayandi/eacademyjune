﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class ParentLeaveRequestServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public void EditLeaveReq(int Req_id, int StudentRegId, DateTime fdate, DateTime toDate, int NoOfDays, string reason)
        {
            var update = (from a in db.ParentLeaveRequests where a.RequestId == Req_id && a.StudentRegisterId == StudentRegId select a).First();
            update.FromDate = fdate;
            update.ToDate = toDate;
            update.NumberOfDays = NoOfDays;
            update.LeaveReason = reason;
            db.SaveChanges();
        }

        public ParentLeaveRequest IsLeaveReqExist(DateTime fdate, DateTime toDate)
        {
            var row = (from a in db.ParentLeaveRequests where a.FromDate == fdate && a.ToDate == toDate select a).FirstOrDefault();
            return row;
        }

        public void AddParentLeaveRequest(int ParentRegId, int StudentRegId, int yearId, int classId, int secId, DateTime fdate, DateTime toDate, int NoOfDays, string reason, int? EmployeeRegId)
        {
            ParentLeaveRequest add = new ParentLeaveRequest()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                ParentRegisterId = ParentRegId,
                StudentRegisterId = StudentRegId,
                FromDate = fdate,
                ToDate = toDate,
                NumberOfDays = NoOfDays,
                LeaveReason = reason,
                Status = "Pending",
                EmployeeRegisterId = EmployeeRegId,
                NotificationStatus = "UnRead"
            };
            db.ParentLeaveRequests.Add(add);
            db.SaveChanges();
        }

        public List<LeaveRequestStatus> geLeaveRequest(DateTime fdate, DateTime toDate)
        {
            var list = (from a in db.ParentLeaveRequests
                        from b in db.TechEmployees
                        where a.FromDate >= fdate &&
                          a.FromDate <= toDate &&
                        a.EmployeeRegisterId == b.EmployeeRegisterId

                        select new LeaveRequestStatus
                        {
                            FromDate = a.FromDate,
                            ToDate = a.ToDate,
                            NumberOfDays = a.NumberOfDays,
                            LeaveReason = a.LeaveReason,
                            Status = a.Status,
                            ClassIncharge = b.EmployeeName
                        }).ToList();
            return list;
        }

        // class Incharge
        public List<LeaveRequestStatus> getParentLeaveRequest(int ClassInchargeId,DateTime fdate, DateTime toDate)
        {
            var list = (from a in db.ParentLeaveRequests
                        from b in db.Students
                        where a.StudentRegisterId == b.StudentRegisterId &&
                            a.FromDate >= fdate &&
                            a.ToDate <= toDate &&
                           a.EmployeeRegisterId == ClassInchargeId &&                           
                           b.StudentStatus == true
                        select new LeaveRequestStatus
                        {
                            RequestId = a.RequestId,
                            ClassIncharge = b.FirstName+" "+b.LastName,
                            From_Date = SqlFunctions.DateName("day", a.FromDate).Trim() + " " + SqlFunctions.DateName("month", a.FromDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.FromDate).Trim(),
                            To_Date = SqlFunctions.DateName("day", a.ToDate).Trim() + " " + SqlFunctions.DateName("month", a.ToDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.ToDate).Trim(),
                            NumberOfDays = a.NumberOfDays,
                            LeaveReason = a.LeaveReason,
                            Status = a.Status
                        }).ToList();     
            return list;
        }

        public List<LeaveRequestStatus> getParentLeaveRequest(int ClassInchargeId, DateTime fdate, DateTime toDate, string Status)
        {
            var list = (from a in db.ParentLeaveRequests
                        from b in db.Students
                        where a.StudentRegisterId == b.StudentRegisterId &&
                            a.FromDate >= fdate &&
                            a.ToDate <= toDate &&
                            a.Status == Status &&
                            a.EmployeeRegisterId == ClassInchargeId &&
                            b.StudentStatus == true
                        select new LeaveRequestStatus
                        {
                            RequestId = a.RequestId,
                            ClassIncharge = b.FirstName + " " + b.LastName,
                            From_Date = SqlFunctions.DateName("day", a.FromDate).Trim() + " " + SqlFunctions.DateName("month", a.FromDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.FromDate).Trim(),
                            To_Date = SqlFunctions.DateName("day", a.ToDate).Trim() + " " + SqlFunctions.DateName("month", a.ToDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.ToDate).Trim(),
                            NumberOfDays = a.NumberOfDays,
                            LeaveReason = a.LeaveReason,
                            Status = a.Status
                        }).ToList();
            return list;
        }

        public void updateParentLeaveRequest(int Req_id, string status)
        {
            var getRow = (from a in db.ParentLeaveRequests where a.RequestId == Req_id select a).FirstOrDefault();
            getRow.Status = status;
            db.SaveChanges();
        }
        public ParentLeaveRequest getLeaveDates(int Req_id, string status)
        {
            var getRow = (from a in db.ParentLeaveRequests where a.RequestId == Req_id && a.Status == status select a).FirstOrDefault();
            return getRow;
        }
        public ParentLeaveRequest getLeaveReqDetail(int StudentRegId)
        {
            var LeaveRequestStatus1 = (from a in db.ParentLeaveRequests where a.StudentRegisterId == StudentRegId select a).OrderByDescending(x => x.FromDate).FirstOrDefault();
            return LeaveRequestStatus1;
        }

        public List<LeaveRequestStatus> geLeaveRequest(int StudentRegId, DateTime fdate, DateTime toDate)
        {
            var list = (from a in db.ParentLeaveRequests
                        from b in db.TechEmployees
                        where a.FromDate >= fdate &&
                          a.FromDate <= toDate &&
                        a.EmployeeRegisterId == b.EmployeeRegisterId &&
                        a.StudentRegisterId == StudentRegId
                        select new
                        {
                            RowId = a.RequestId,
                            FromDate = a.FromDate,
                            ToDate = a.ToDate,
                            NumberOfDays = a.NumberOfDays,
                            LeaveReason = a.LeaveReason,
                            Status = a.Status,
                            ClassIncharge = b.EmployeeName
                        }).ToList().Select(a => new LeaveRequestStatus
                        {
                            Request_Id = QSCrypt.Encrypt(a.RowId.ToString()),
                            From_Date = a.FromDate.Value.ToString("MMM dd,yyyy"),
                            To_Date = a.ToDate.Value.ToString("MMM dd,yyyy"),
                            NumberOfDays = a.NumberOfDays,
                            LeaveReason = a.LeaveReason,
                            Status = a.Status,
                            ClassIncharge = a.ClassIncharge
                        }).ToList();
            return list;
        }
        //public ParentLeaveRequest IsLeaveReqExist(DateTime fdate, DateTime todate, int eid)
        //{
        //    var row = (from a in db.ParentLeaveRequests where a.FromDate == fdate && a.ToDate == todate && a.StudentRegisterId == eid select a).FirstOrDefault();
        //    return row;
        //}
        public string IsLeaveReqExist(DateTime fdate, DateTime todate, int studentRegId)
        {
            // var row = (from a in ee.FacultyLeaveRequests where a.FromDate == fdate && a.ToDate == todate select a).FirstOrDefault();

            var LeaveReqList = (from a in db.ParentLeaveRequests
                                where a.FromDate >= fdate.Date &&
                                a.FromDate <= todate.Date &&
                                a.StudentRegisterId == studentRegId
                                select new
                                {
                                    id = a.RequestId,
                                    fdate = a.FromDate,
                                    tdate = a.ToDate
                                }).ToList();

            for (int i = 0; i < LeaveReqList.Count; i++)
            {
                DateTime? fd = LeaveReqList[i].fdate;
                DateTime? td = LeaveReqList[i].tdate;

                if (fdate >= fd && fdate <= td)
                {
                    return "dateExist";
                }
                if (todate >= fd && todate <= td)
                {
                    return "dateExist";
                }
            }

            var LeaveReqList1 = (from a in db.ParentLeaveRequests
                                 where a.ToDate >= fdate.Date &&
                                 a.ToDate <= todate.Date &&
                                 a.StudentRegisterId == studentRegId
                                 select new
                                 {
                                     id = a.RequestId,
                                     fdate = a.FromDate,
                                     tdate = a.ToDate
                                 }).ToList();

            for (int j = 0; j < LeaveReqList1.Count; j++)
            {
                DateTime? fd1 = LeaveReqList1[j].fdate;
                DateTime? td1 = LeaveReqList1[j].tdate;

                if (fdate >= fd1 && fdate <= td1)
                {
                    return "dateExist";
                }
                if (todate >= fd1 && todate <= td1)
                {
                    return "dateExist";
                }
            }

            var LeaveReqList2 = (from a in db.ParentLeaveRequests
                                 where a.ToDate >= todate.Date &&
                                 a.StudentRegisterId == studentRegId
                                 select new
                                 {
                                     id = a.RequestId,
                                     fdate = a.FromDate,
                                     tdate = a.ToDate
                                 }).ToList();
            for (int j = 0; j < LeaveReqList2.Count; j++)
            {
                DateTime? fd2 = LeaveReqList2[j].fdate;
                DateTime? td2 = LeaveReqList2[j].tdate;
                if (fdate >= fd2 && fdate <= td2)
                {
                    return "dateExist";
                }
                if (todate >= fd2 && todate <= td2)
                {
                    return "dateExist";
                }
            }

            return "dateFree";
        }

        public string IsLeaveReqExist(DateTime fdate, DateTime todate, int studentRegId, int RequsetId)
        {
            // var row = (from a in ee.FacultyLeaveRequests where a.FromDate == fdate && a.ToDate == todate select a).FirstOrDefault();

            var LeaveReqList = (from a in db.ParentLeaveRequests
                                where a.FromDate >= fdate.Date &&
                                a.FromDate <= todate.Date &&
                                a.StudentRegisterId == studentRegId &&
                                a.RequestId != RequsetId
                                select new
                                {
                                    id = a.RequestId,
                                    fdate = a.FromDate,
                                    tdate = a.ToDate
                                }).ToList();

            for (int i = 0; i < LeaveReqList.Count; i++)
            {
                DateTime? fd = LeaveReqList[i].fdate;
                DateTime? td = LeaveReqList[i].tdate;

                if (fdate >= fd && fdate <= td)
                {
                    return "dateExist";
                }
                if (todate >= fd && todate <= td)
                {
                    return "dateExist";
                }
            }

            var LeaveReqList1 = (from a in db.ParentLeaveRequests
                                 where a.ToDate >= fdate.Date &&
                                 a.ToDate <= todate.Date &&
                                 a.RequestId != RequsetId
                                 select new
                                 {
                                     id = a.RequestId,
                                     fdate = a.FromDate,
                                     tdate = a.ToDate
                                 }).ToList();

            for (int j = 0; j < LeaveReqList1.Count; j++)
            {
                DateTime? fd1 = LeaveReqList1[j].fdate;
                DateTime? td1 = LeaveReqList1[j].tdate;

                if (fdate >= fd1 && fdate <= td1)
                {
                    return "dateExist";
                }
                if (todate >= fd1 && todate <= td1)
                {
                    return "dateExist";
                }
            }

            var LeaveReqList2 = (from a in db.ParentLeaveRequests
                                 where a.ToDate >= todate.Date &&
                                 a.RequestId != RequsetId
                                 select new
                                 {
                                     id = a.RequestId,
                                     fdate = a.FromDate,
                                     tdate = a.ToDate
                                 }).ToList();
            for (int j = 0; j < LeaveReqList2.Count; j++)
            {
                DateTime? fd2 = LeaveReqList2[j].fdate;
                DateTime? td2 = LeaveReqList2[j].tdate;
                if (fdate >= fd2 && fdate <= td2)
                {
                    return "dateExist";
                }
                if (todate >= fd2 && todate <= td2)
                {
                    return "dateExist";
                }
            }

            return "dateFree";
        }

        public ParentLeaveRequest ParentLeaveReq(int id)
        {
            var getRow = (from a in db.ParentLeaveRequests where a.RequestId == id select a).FirstOrDefault();
            return getRow;
        }

        public void delLeaveReq(int Did)
        {
            var getRow = (from a in db.ParentLeaveRequests where a.RequestId == Did select a).FirstOrDefault();
            db.ParentLeaveRequests.Remove(getRow);
            db.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}