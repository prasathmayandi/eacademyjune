﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class StudentMarkServices:IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        DateTime date = DateTimeByZone.getCurrentDate();


        public List<ProgressCard> getMark(int yearId, int classId, int secId, int examId, int StudentRegId)
        {
            var list = (from a in db.StudentMarks
                        from b in db.Subjects
                        from c in db.Marks
                        where a.AcademicYearId == c.AcademicYearId &&
                              a.ClassId == c.ClassId &&
                              a.SectionId == c.SectionId &&
                              a.SubjectId == c.SubjectId &&
                              a.SubjectId == b.SubjectId &&
                              a.AcademicYearId == yearId &&
                              a.ClassId == classId &&
                              a.SectionId == secId &&
                              a.ExamId == examId &&
                              a.StudentRegisterId == StudentRegId 
                        select new ProgressCard
                        {

                            Subject = b.SubjectName,
                            Subject_Id=b.SubjectId,
                            Mark = a.Mark,
                            MinMark = c.MinMark,
                            MaxMark = c.MaxMark
                        }).OrderBy(a =>a.Subject_Id).ToList();
            return list;
        }

        public List<GetStudentId> getStudentId(int yearId, int classId, int secId, int examId)
        {
            var getAllStudentId = (from a in db.StudentMarks
                                   where a.AcademicYearId == yearId &&
                                   a.ClassId == classId &&
                                   a.SectionId == secId &&
                                   a.ExamId == examId
                                   select new GetStudentId
                                   {
                                       stuRegId = a.StudentRegisterId
                                   }).Distinct().ToList();
            return getAllStudentId;
        }

        public List<GetStudentId> getStudentId1(int yearId, int classId, int secId, int examId)
        {
            var getAllStudentId = (from a in db.StudentMarks
                                   from b in db.Students
                                   where a.AcademicYearId == yearId &&
                                   a.ClassId == classId &&
                                   a.SectionId == secId &&
                                   a.ExamId == examId &&
                                   a.StudentRegisterId == b.StudentRegisterId
                                   select new GetStudentId
                                   {
                                       name = b.FirstName+" "+b.LastName,
                                       stuRegId = a.StudentRegisterId
                                   }).Distinct().ToList();
            return getAllStudentId;
        }

        public List<ProgressCard> getMarkList(int yearId, int classId, int secId, int examId, int? sId)
        {
            var getMarkList = (from a in db.StudentMarks

                               where a.AcademicYearId == yearId &&
                               a.ClassId == classId &&
                               a.SectionId == secId &&
                               a.ExamId == examId &&
                               a.StudentRegisterId == sId
                               select new ProgressCard
                               {
                                   Mark = a.Mark,
                                   Subject_Id=a.SubjectId
                               }).OrderBy(a => a.Subject_Id).ToList();
            return getMarkList;
        }

        public List<ProgressCard> getMarkList(int yearId, int classId, int secId, int examId)
        {
            var getMarkList = (from a in db.StudentMarks
                               from b in db.Subjects
                               where a.AcademicYearId == yearId &&
                               a.ClassId == classId &&
                               a.SectionId == secId &&
                               a.ExamId == examId &&
                               a.SubjectId == b.SubjectId
                               select new ProgressCard
                               {
                                   Subject_Id = a.SubjectId,
                                   Subject = b.SubjectName
                               }).Distinct().ToList();
            return getMarkList;
        }

        public List<ProgressCard> getMarkList(int yearId, int classId, int secId, int examId, int ClassInchargeId)
        {
            var MarkList = (from a in db.StudentMarks
                            from b in db.Subjects
                            from c in db.Students
                            where a.AcademicYearId == yearId &&
                            a.ClassId == classId &&
                            a.SectionId == secId &&
                            a.ExamId == examId &&
                            a.EmployeeRegisterId == ClassInchargeId &&
                            a.SubjectId == b.SubjectId &&
                            a.StudentRegisterId == c.StudentRegisterId
                            select new ProgressCard
                            {
                                sid = a.StudentRegisterId,
                                name = c.FirstName + " " + c.LastName,
                                mark = a.Mark
                            }).ToList();
            return MarkList;
        }

        public List<ProgressCard1> getProgressCardList(int yearId, int classId, int secId, int examId, int ClassInchargeId)
        {
            var ProgressCardList = (from a in db.StudentMarks
                                    from b in db.Subjects
                                    from c in db.Marks
                                    where a.AcademicYearId == yearId &&
                                          a.ClassId == classId &&
                                          a.SectionId == secId &&
                                          a.ExamId == examId &&
                                          a.EmployeeRegisterId == ClassInchargeId &&

                                          a.SubjectId == b.SubjectId &&
                                          a.AcademicYearId == c.AcademicYearId &&
                                          a.ClassId == c.ClassId &&
                                          a.SubjectId == c.SubjectId &&
                                          a.SubjectId == c.SubjectId
                                    select new ProgressCard1
                                    {

                                        Subject = b.SubjectName,
                                        Mark = a.Mark
                                    }).ToList();
            return ProgressCardList;
        }

        public List<GetSubjectList> getSubjectList(int yearId, int classId, int secId, int examId, int ClassInchargeId)
        {
            var SubjectList = (from a in db.StudentMarks
                               from b in db.Subjects
                               where a.AcademicYearId == yearId &&
                               a.ClassId == classId &&
                               a.SectionId == secId &&
                               a.ExamId == examId &&
                               a.EmployeeRegisterId == ClassInchargeId &&
                               a.SubjectId == b.SubjectId
                               select new GetSubjectList
                               {
                                   subject = b.SubjectName,
                               }).Distinct().ToList();
            return SubjectList;
        }

        public void addStudentMark(int yearId,int classId,int secId,int examId,int subId,int StudentRegisterId,int Mark,int EmployeeRegisterId, string comment)
        {
            StudentMark sm = new StudentMark()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                ExamId = examId,
                SubjectId = subId,
                StudentRegisterId = StudentRegisterId,
                Mark = Mark,
                EmployeeRegisterId = EmployeeRegisterId,
                Remark = comment,
                RemarkDate = date
            };
            db.StudentMarks.Add(sm);
            db.SaveChanges();
        }

        public StudentMark checkMarkExist(int yearId, int classId, int secId, int subId, int examId)
        {
            var getRow = (from a in db.StudentMarks where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.SubjectId == subId && a.ExamId == examId select a).FirstOrDefault();
            return getRow;
        }

        public IEnumerable<object> getExistMark(int yearId, int classId, int secId, int subId, int examId)
        {
            var list = (from a in db.StudentMarks
                        from b in db.StudentRollNumbers
                        from c in db.Students
                        from d in db.ExamAttendances
                        where a.StudentRegisterId == b.StudentRegisterId &&
                             a.StudentRegisterId == c.StudentRegisterId &&
                            a.AcademicYearId == yearId &&
                            a.ClassId == classId &&
                            a.SectionId == secId &&
                            a.SubjectId == subId &&
                            a.ExamId == examId &&
                            c.StudentStatus == true &&
                            b.Status == true &&
                            b.AcademicYearId == yearId &&
                            b.ClassId == classId &&
                            b.SectionId == secId &&
                            d.AcademicYearId == yearId &&
                            d.ClassId == classId &&
                            d.SectionId == secId &&
                            d.SubjectId == subId &&
                            a.StudentRegisterId == d.StudentRegisterId &&
                            d.ExamId == examId


                        select new
                        {
                            rollnumber = b.RollNumber,
                            id = a.StudentRegisterId,
                            name = c.FirstName + " " + c.LastName,
                            mark = a.Mark,
                            comment = a.Remark,
                            Attendance = d.AttendanceStatus
                        }).ToList().Select(c => new
                        {
                            rollnumber = c.rollnumber,
                            id = QSCrypt.Encrypt(c.id.ToString()),
                            name = c.name,
                            mark = c.mark,
                            attendance = c.Attendance,
                            comment = blank(c.comment)
                        }).Distinct().ToList();
            return list;
        }

        public string blank(string a)
        {
            if(a == null)
            {
                a = "";
            }
            return a;
        }

        public void updateStudentMark(int yearId, int classId, int secId, int examId, int subId, int StudentRegisterId, int Mark, int EmployeeRegisterId,string comment)
        {
            var getrow = (from a in db.StudentMarks where a.AcademicYearId == yearId && a.ClassId == classId && a.SectionId == secId && a.SubjectId == subId && a.ExamId == examId && a.StudentRegisterId == StudentRegisterId && a.EmployeeRegisterId == EmployeeRegisterId select a).FirstOrDefault();
            getrow.Mark = Mark;
            getrow.Remark = comment;
            getrow.CommentUpdatedDate = date;
            getrow.RemarkUpdatedBy = EmployeeRegisterId;
            db.SaveChanges();
        }

        //jegadeesh

        public List<getclassandsection> GetStudentmarksList(int yid, int cid, int sid, int eid)
        {
            var ans = (from a in db.StudentMarks

                       from d in db.Exams
                       from e in db.Students
                       from f in db.Subjects
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.StudentRegisterId == e.StudentRegisterId && d.ExamId == eid && a.ExamId == d.ExamId &&
                       a.SubjectId == f.SubjectId
                       select new getclassandsection
                       {
                           SubjectMark = a.Mark,
                           ExamType = d.ExamName,
                           subjectName = f.SubjectName,
                           StudentRegId = e.StudentRegisterId,
                           StudentId=e.StudentId
                       }
                           ).OrderBy(q => q.StudentId).ToList();
            return ans;
        }
        public List<getclassandsection> StudentProgress(int yid, int cid, int sid, int stu_id)
        {
            var ans = (from a in db.StudentMarks

                       from d in db.Exams
                       from e in db.Students
                       from f in db.Subjects
                       where a.AcademicYearId == yid && a.ClassId == cid && a.SectionId == sid && a.StudentRegisterId == stu_id && e.StudentRegisterId == stu_id && a.ExamId == d.ExamId &&
                       a.SubjectId == f.SubjectId
                       select new getclassandsection
                       {
                           SubjectMark = a.Mark,
                           ExamType = d.ExamName,
                           subjectName = f.SubjectName,
                           subjectId = a.SubjectId,
                           ExamId = a.ExamId
                       }).OrderBy(x=>x.subjectId).ToList();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}