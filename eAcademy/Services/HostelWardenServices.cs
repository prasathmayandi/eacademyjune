﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System.Collections.Generic;
using System.Linq;
using System;

namespace eAcademy.Services
{
    public class HostelWardenServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public WardenList getWarden(int wid)
        {
            var ans = (from t1 in dc.HostelWardens
                       from t2 in dc.TechEmployees
                       from t3 in dc.AcademicYears
                       where t1.EmployeeRegisterId == t2.EmployeeRegisterId && t1.AcademicYearId == t3.AcademicYearId && t1.WardenId == wid
                       select new WardenList { acid = t1.AcademicYearId, empid = t1.EmployeeRegisterId, ename = t2.EmployeeName, empContact = t2.Mobile, empAddress = t2.AddressLine1, status = t1.Status, acYear = t3.AcademicYear1, wid = t1.WardenId }).First();

            return ans;
        }

        public List<WardenList> getWarden()
        {
            var ans = (from t1 in dc.HostelWardens
                       from t2 in dc.TechEmployees
                       from t3 in dc.AcademicYears
                       where t1.EmployeeRegisterId == t2.EmployeeRegisterId && t1.AcademicYearId == t3.AcademicYearId
                       select new { acid = t1.AcademicYearId, empid = t1.EmployeeRegisterId, ename = t2.EmployeeName, empContact = t2.Mobile, empAddress = t2.AddressLine1, status = t1.Status, acYear = t3.AcademicYear1, wid = t1.WardenId }).ToList().
                       Select(q => new WardenList { acid = q.acid, empid = q.empid, ename = q.ename, empContact = q.empContact, empAddress = q.empAddress, status = q.status, acYear = q.acYear, wid = q.wid, WardenID=QSCrypt.Encrypt(q.wid.ToString())}).ToList();
            return ans;
        }

        public void addWarden(int acid, int empid)
        {
            HostelWarden warden = new HostelWarden()
            {
                AcademicYearId = acid,
                EmployeeRegisterId = empid,
                Status = true
            };

            dc.HostelWardens.Add(warden);
            dc.SaveChanges();
        }

        public void updateWarden(int wid, string status)
        {

            var update = dc.HostelWardens.Where(q => q.WardenId.Equals(wid)).First();
            if (status == "Active")
            {
                update.Status = true;
            }

            else
            {
                update.Status = false;
            }
            dc.SaveChanges();
        }

        public void deleteWarden(int wid)
        {
            var delete = dc.HostelWardens.Where(q => q.WardenId.Equals(wid)).First();

            dc.HostelWardens.Remove(delete);
            dc.SaveChanges();
        }

        internal bool checkWarden(int acid, int empid)
        {
            var ans = dc.HostelWardens.Where(q => q.AcademicYearId.Value.Equals(acid) && q.EmployeeRegisterId.Value.Equals(empid)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
       
    }
}