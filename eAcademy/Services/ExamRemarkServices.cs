﻿using eAcademy.DataModel;
using System;
using System.Linq;

namespace eAcademy.Services
{
    public class ExamRemarkServices:IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public ExamRemark checkRecordExist(int AcademicYearId, int ClassId, int SectionId, int ExamId, int SubjectId, int StudentRegisterId)
        {
            var CheckRecordExist = (from a in db.ExamRemarks where a.AcademicYearId == AcademicYearId && a.ClassId == ClassId && a.SectionId == SectionId && a.ExamId == ExamId && a.SubjectId == SubjectId && a.StudentRegisterId == StudentRegisterId select a).FirstOrDefault();
            return CheckRecordExist;
        }

        public void addExamRemark(int AcademicYearId,int ClassId,int SectionId,int ExamId,int SubjectId,int StudentRegisterId,string ExamRemark1)
        {
            ExamRemark r = new ExamRemark()
            {
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                ExamId = ExamId,
                SubjectId = SubjectId,
                StudentRegisterId = StudentRegisterId,
                ExamRemark1 = ExamRemark1
            };
            db.ExamRemarks.Add(r);
            db.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}