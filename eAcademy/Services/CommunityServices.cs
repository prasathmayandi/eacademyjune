﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class CommunityServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public List<tblCommunity> GetCommunity()
        {
            var community = dc.tblCommunities.Select(q => q).ToList();
            return community;
        }

        public int getCommunityId(string community)
        {
            var record = dc.tblCommunities.Where(q => q.CommunityName == community).FirstOrDefault();
            if(record !=null)
            {
                return record.CommunityId;
            }
            else
            {
                return 0;
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}