﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class FeesRefundToStudentServices:IDisposable
    {
        EacademyEntities db = new EacademyEntities();
        
        internal List<FeesRefundList> getRefundFeePaidStudentList(int acid, int cid, int secid,int fid)
        {
            if (fid == 0)
            {
                var ans1 = (from t1 in db.FeesRefundToStudents
                            from t2 in db.FeesRefunds
                            from t3 in db.AcademicYears
                            from t4 in db.Classes
                            from t5 in db.FeeCategories
                            from t6 in db.FeeCollectionCategories
                            from t7 in db.AssignClassToStudents
                            from t8 in db.Students
                            where t6.AcademicYearId == acid && t6.ClassId == cid && t6.SectionId == secid && t1.CollectionId == t6.CollectionId && t8.StudentRegisterId == t1.StudentRegisterId && t7.StudentRegisterId == t1.StudentRegisterId && t7.Status == true && t7.HaveRollNumber == true
                            && t2.FeeId == t1.FeeId && t3.AcademicYearId == t6.AcademicYearId && t4.ClassId == t6.ClassId && t5.FeeCategoryId == t6.FeeCategoryId
                            select new FeesRefundList
                            {
                                AcademicYear = t3.AcademicYear1,
                                Class = t4.ClassType,
                                StudentId = t8.StudentId,
                                StudentName = t8.FirstName + " " + t8.LastName,
                                FeeCategory = t5.FeeCategoryName,
                                Refund = t1.RefundAmount,
                                NetAmount = t1.NetAmount,
                                Reason = t1.Reason,
                                Date = SqlFunctions.DateName("day", t1.DateOfRefund).Trim() + "-" + SqlFunctions.StringConvert((double)t1.DateOfRefund.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", t1.DateOfRefund),      // a.DOB,
                                dd=t1.DateOfRefund.Value
                            }).ToList().Select(a => new FeesRefundList { 
                            AcademicYear=a.AcademicYear,
                            Class=a.Class,
                            StudentId=a.StudentId,
                            StudentName=a.StudentName,
                            FeeCategory=a.FeeCategory,
                            Refund=a.Refund,
                            NetAmount=a.NetAmount,
                            Reason=a.Reason,
                            Date = a.dd.ToString("MMM dd, yyyy")
                            
                            }).ToList();

                return ans1;
            }
            else
            {
                var ans1 = (from t1 in db.FeesRefundToStudents
                            from t2 in db.FeesRefunds
                            from t3 in db.AcademicYears
                            from t4 in db.Classes
                            from t5 in db.FeeCategories
                            from t6 in db.FeeCollectionCategories
                            from t7 in db.AssignClassToStudents
                            from t8 in db.Students
                            where t6.AcademicYearId == acid && t6.ClassId == cid && t6.SectionId == secid && t1.CollectionId == t6.CollectionId && t8.StudentRegisterId == t1.StudentRegisterId && t7.StudentRegisterId == t1.StudentRegisterId && t7.Status == true && t7.HaveRollNumber == true
                            && t2.FeeId == t1.FeeId && t3.AcademicYearId == t6.AcademicYearId && t4.ClassId == t6.ClassId && t6.FeeCategoryId == fid && t5.FeeCategoryId == t6.FeeCategoryId
                            select new FeesRefundList
                            {
                                AcademicYear = t3.AcademicYear1,
                                Class = t4.ClassType,
                                StudentId = t8.StudentId,
                                StudentName = t8.FirstName + " " + t8.LastName,
                                FeeCategory = t5.FeeCategoryName,
                                Refund = t1.RefundAmount,
                                NetAmount = t1.NetAmount,
                                Reason = t1.Reason,
                                Date = SqlFunctions.DateName("day", t1.DateOfRefund).Trim() + "-" + SqlFunctions.StringConvert((double)t1.DateOfRefund.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", t1.DateOfRefund),      // a.DOB,
                                dd = t1.DateOfRefund.Value
                            }).ToList().Select(a => new FeesRefundList
                            {
                                AcademicYear = a.AcademicYear,
                                Class = a.Class,
                                StudentId = a.StudentId,
                                StudentName = a.StudentName,
                                FeeCategory = a.FeeCategory,
                                Refund = a.Refund,
                                NetAmount = a.NetAmount,
                                Reason = a.Reason,
                                Date = a.dd.ToString("MMM dd, yyyy")

                            }).ToList();

                return ans1;
            }
        }
        internal List<FeesRefundList> getStudentRefundFeePaidStudentList(int acid, int cid, int secid)
        {

            var ans1 = (from t1 in db.FeesRefundToStudents
                      
                        from t3 in db.AcademicYears
                        from t4 in db.Classes
                        from t5 in db.FeeCategories
                        from t6 in db.FeeCollectionCategories
                        from t7 in db.AssignClassToStudents
                        from t8 in db.Students
                        where t6.AcademicYearId == acid && t6.ClassId == cid && t6.SectionId == secid &&t1.CollectionId == t6.CollectionId && t8.StudentRegisterId == t1.StudentRegisterId && t7.StudentRegisterId == t1.StudentRegisterId && t7.Status == true && t7.HaveRollNumber == true
                        &&  t1.FeeId == null && t3.AcademicYearId == t6.AcademicYearId && t4.ClassId == t6.ClassId && t5.FeeCategoryId == t6.FeeCategoryId
                        select new FeesRefundList
                        {
                            AcademicYear = t3.AcademicYear1,
                            Class = t4.ClassType,
                            StudentId = t8.StudentId,
                            StudentName = t8.FirstName + " " + t8.LastName,
                            FeeCategory = t5.FeeCategoryName,
                            Refund = t1.RefundAmount,
                            NetAmount = t1.NetAmount,
                            Reason=t1.Reason,
                            Date = SqlFunctions.DateName("day", t1.DateOfRefund).Trim() + "-" + SqlFunctions.StringConvert((double)t1.DateOfRefund.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", t1.DateOfRefund),      // a.DOB,
                            dd = t1.DateOfRefund.Value
                        }).ToList().Select(a => new FeesRefundList
                        {
                            AcademicYear = a.AcademicYear,
                            Class = a.Class,
                            StudentId = a.StudentId,
                            StudentName = a.StudentName,
                            FeeCategory = a.FeeCategory,
                            Refund = a.Refund,
                            NetAmount = a.NetAmount,
                            Reason = a.Reason,
                            Date = a.dd.ToString("MMM dd, yyyy")

                        }).ToList();
          
            return ans1;
        }
        internal List<FeesRefundList> getAdmissionFeerefund(int acid, int cid)
        {

            var ans1 = (

                        from t3 in db.AcademicYears
                        from t4 in db.Classes
                        from t5 in db.FeeCategories
                        from t6 in db.FeeCollectionCategories                      
                        from t8 in db.Students
                        where t8.AcademicyearId == acid && t8.AdmissionClass == cid && t8.StudentStatus == false && t8.Statusflag == "FeesRefund" && t3.AcademicYearId == t8.AcademicyearId && t4.ClassId == t8.AdmissionClass && t6.ApplicationId == t8.AdmissionId && t5.FeeCategoryId == t6.FeeCategoryId
                        select new FeesRefundList
                        {
                            AcademicYear = t3.AcademicYear1,
                            Class = t4.ClassType,
                            StudentId = t8.StudentId,
                            StudentName = t8.FirstName + " " + t8.LastName,
                            FeeCategory = t5.FeeCategoryName,
                            Refund = t6.ReFundAmount,
                            NetAmount =t6.TotalAmount,
                            Reason = t6.Reason,
                            Date = SqlFunctions.DateName("day", t6.RefundDate).Trim() + "-" + SqlFunctions.StringConvert((double)t6.RefundDate.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", t6.RefundDate),      // a.DOB,
                            dd = t6.RefundDate.Value
                        }).ToList().Select(a => new FeesRefundList
                        {
                            AcademicYear = a.AcademicYear,
                            Class = a.Class,
                            StudentId = a.StudentId,
                            StudentName = a.StudentName,
                            FeeCategory = a.FeeCategory,
                            Refund = a.Refund,
                            NetAmount = a.NetAmount,
                            Reason = a.Reason,
                            Date = a.dd.ToString("MMM dd, yyyy")

                        }).ToList();
            return ans1;
        }
        internal List<FeesRefundList> getRefundFeeDueStudentList(int acid, int cid, int secid, int fid)
        {
            if (fid == 0)
            {
                var ans = (from a in db.AssignClassToStudents
                           from b in db.Students
                           from d in db.StudentRollNumbers
                           from c in db.FeeCollectionCategories
                           from t3 in db.AcademicYears
                           from t4 in db.Classes
                           from t5 in db.FeeCategories
                           from f in db.Fees
                           where a.AcademicYearId == acid && a.ClassId == cid && a.SectionId == secid && a.Status == true && a.HaveRollNumber == true && a.StudentRegisterId == b.StudentRegisterId && d.StudentRegisterId == a.StudentRegisterId && d.AcademicYearId == a.AcademicYearId && d.ClassId == a.ClassId && d.SectionId == a.SectionId && d.Status == true && c.ClassId == a.ClassId &&
                              c.SectionId == a.SectionId && c.StudentRegId == a.StudentRegisterId &&
                               c.ClassId == cid && t3.AcademicYearId == a.AcademicYearId && t4.ClassId == a.ClassId && t5.FeeCategoryId == c.FeeCategoryId && f.ReFundAmount != null && f.FeeCategoryId == c.FeeCategoryId && f.AcademicYearId == a.AcademicYearId && f.ClassId == a.ClassId
                           && !(from e in db.FeesRefundToStudents

                                where e.StudentRegisterId == c.StudentRegId && e.FeeId == f.FeeId
                                select e.StudentRegisterId).Contains(a.StudentRegisterId)
                           select new FeesRefundList { AcademicYear = t3.AcademicYear1, Class = t4.ClassType, StudentId = b.StudentId, StudentName = b.FirstName + " " + b.LastName, FeeCategory = t5.FeeCategoryName,Refund =f.ReFundAmount }).Distinct().ToList();
                return ans;
            }
            else
            {
                var ans = (from a in db.AssignClassToStudents
                           from b in db.Students
                           from d in db.StudentRollNumbers
                           from c in db.FeeCollectionCategories
                           from t3 in db.AcademicYears
                           from t4 in db.Classes
                           from t5 in db.FeeCategories
                           from f in db.Fees
                           where a.AcademicYearId == acid && a.ClassId == cid && a.SectionId == secid && a.Status == true && a.HaveRollNumber == true && a.StudentRegisterId == b.StudentRegisterId && d.StudentRegisterId == a.StudentRegisterId && d.AcademicYearId == a.AcademicYearId && d.ClassId == a.ClassId && d.SectionId == a.SectionId && d.Status == true && c.ClassId == a.ClassId &&
                              c.SectionId == a.SectionId && c.StudentRegId == a.StudentRegisterId && c.FeeCategoryId == fid &&
                               c.ClassId == cid && t3.AcademicYearId == a.AcademicYearId && t4.ClassId == a.ClassId && t5.FeeCategoryId == c.FeeCategoryId && f.ReFundAmount != null && f.FeeCategoryId == c.FeeCategoryId && f.AcademicYearId == a.AcademicYearId && f.ClassId == a.ClassId
                           && !(from e in db.FeesRefundToStudents

                                where e.StudentRegisterId == c.StudentRegId && e.FeeId == f.FeeId
                                select e.StudentRegisterId).Contains(a.StudentRegisterId)
                           select new FeesRefundList { AcademicYear = t3.AcademicYear1, Class = t4.ClassType, StudentId = b.StudentId, StudentName = b.FirstName + " " + b.LastName, FeeCategory = t5.FeeCategoryName, Refund = f.ReFundAmount }).Distinct().ToList();
                return ans;
            }
        }
        public void addpreaddmissionstudentdetails(int FeeCollectionidstudent,decimal TotalAmount,decimal AmountRefunded,string reason,string ReceivedBy, int adminid, int studentadmissionid)
        {
            var date = DateTimeByZone.getCurrentDateTime();
                FeesRefundToStudent rre = new FeesRefundToStudent()
                {
                    CollectionId = FeeCollectionidstudent,
                    LastAmount = TotalAmount,
                    RefundAmount = AmountRefunded,
                    NetAmount = TotalAmount - AmountRefunded,
                    Reason = reason,
                    IssuedBy = ReceivedBy,
                    EmployeeRegisterId = adminid,
                    DateOfRefund = date,
                    Status = true,
                    StudentAdmissionId = studentadmissionid
                };
                db.FeesRefundToStudents.Add(rre);
                db.SaveChanges();
                //FeeRefundId = rre.StudentFeeRefundId;
            
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}