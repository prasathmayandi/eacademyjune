﻿using eAcademy.DataModel;
using System;
using System.Linq;

namespace eAcademy.Services
{
    public class PrimaryUserResetPasswordServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public PreAdmissionPrimaryUserResetPassword CheckPrimaryId(Guid id)
        {
            var ans = (from a in dc.PreAdmissionPrimaryUserResetPasswords
                       where a.Id == id
                       select a).FirstOrDefault();
            return ans;
        }
        public PreAdmissionPrimaryUserResetPassword CheckOtpPrimaryId(Guid id,long? otp)
        {
            var ans = (from a in dc.PreAdmissionPrimaryUserResetPasswords
                       where a.Id == id && a.OTP==otp
                       select a).FirstOrDefault();
            return ans;
        }
        public void DeleteRequestPrimary(Guid uid)
        {
            var res = (from a in dc.PreAdmissionPrimaryUserResetPasswords
                       where a.Id == uid
                       select a).FirstOrDefault();
            if (res != null)
            {
                dc.PreAdmissionPrimaryUserResetPasswords.Remove(res);
                dc.SaveChanges();
            }

        }
        public void AddPrimaryUserResetPwd(Guid uid, int regid, DateTime date,string uname)
        {
            PreAdmissionPrimaryUserResetPassword RP = new PreAdmissionPrimaryUserResetPassword();
            RP.Id = uid;
            RP.UserId = regid;
            RP.Username = uname;
            RP.ResetRequestDateTime = date;
            dc.PreAdmissionPrimaryUserResetPasswords.Add(RP);

            dc.SaveChanges();
        }
        public void AddPrimaryUserMobileResetPwd(Guid uid, int regid, DateTime date,long? otp,string uname)
        {
            PreAdmissionPrimaryUserResetPassword RP = new PreAdmissionPrimaryUserResetPassword();
            RP.Id = uid;
            RP.UserId = regid;
            RP.ResetRequestDateTime = date;
            RP.OTP = otp;
            RP.Username = uname;
            dc.PreAdmissionPrimaryUserResetPasswords.Add(RP);

            dc.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}