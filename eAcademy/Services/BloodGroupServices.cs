﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class BloodGroupServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();


        public List<tblBloodGroup> GetBloodGroup()
        {
            var blood = dc.tblBloodGroups.Select(q => q).ToList();
            return blood;
        }

        public int GetBloodGroupId(string blood)
        {
            var record = dc.tblBloodGroups.Where(q => q.BloodGroupName == blood).FirstOrDefault();
            if(record != null)
            {
                return record.BloodId;
            }
            else
            {
                return 0;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}