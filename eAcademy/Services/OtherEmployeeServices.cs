﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;
using System.Collections;
using System.Web.Security;
using eAcademy.HelperClass;
using eAcademy.Areas.Communication.Models;

namespace eAcademy.Services
{
    public class OtherEmployeeServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public IEnumerable<Object> ShowEmployees()
        {
            var EmployeeList = (from a in dc.OtherEmployees
                                where a.EmployeeStatus == true
                                select new
                                {
                                    EmployeeName = a.EmployeeName,
                                    EmployeeId = a.EmployeeRegisterId
                                }).ToList().AsEnumerable();
            return EmployeeList;
        }
        internal Eprofile getEmployeeListById(int empId)
        {
            var ans = (from a in dc.OtherEmployees
                       from b in dc.EmployeeTypes
                       from c in dc.EmployeeDesignations
                       where a.EmployeeRegisterId == empId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId
                       select new Eprofile
                       {
                           Fname = a.EmployeeName,
                           Mname = a.Middlename,
                           Lname = a.Lastname,
                           EmployeeRegisterId = a.EmployeeRegisterId,
                           EmployeeId = a.EmployeeId,
                           EmployeeName = a.EmployeeName + " " + a.Lastname,
                           DateOfBirth = SqlFunctions.DateName("day", a.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOB),                           
                           Gender = a.Gender,
                           BloodGroup = a.BloodGroup,
                           Community = a.Community,
                           Religion = a.Religion,
                           Nationality = a.Nationality,
                           Marital = a.MaritalStatus,
                           EmployeeTypeId = a.EmployeeTypeId,
                           EmployeeDesignationId = a.EmployeeDesignationId,
                           emptype = b.EmployeeTypes,
                           empdesign = c.Designation,
                           Email = a.Email,
                           LandlineNo = a.Phone,
                           Contact = a.Mobile,
                           Addressline1 = a.Address,
                           Addressline2 = a.Address2,
                           State = a.State,
                           City = a.City,
                           Country = a.Country,
                           Pincode = a.ZipCode,
                           PermanentAddressline1 = a.PermanentAddress1,
                           PermanentAddressline2 = a.PermanentAddress2,
                           PermanentCity = a.PermanentCity,
                           PermanentCountry = a.PermanentCountry,
                           PermanentState = a.PermanentState,
                           PermanentPincode = a.PermanentZipCode,
                           DateOfLeaving = SqlFunctions.DateName("day", a.DOL).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOL.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOL),
                           DateOfRegister = SqlFunctions.DateName("day", a.DOR).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOR.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOR) ,
                           Qualification = a.Qualification,
                           EmergencyContactPerson = a.EmergencyContactPerson,
                           EmergencyContactNumber = a.EmergencyContactNumber,
                           EmergencyContactPersonRelationShip = a.EmergencyContactpersonRelationship,
                           EmployeeStatus = a.EmployeeStatus,
                           Experience = a.Experience,
                           Expertise = a.Expertise,
                           ImgFile = a.ImgFile,
                           ResumeFile = a.ResumeFile,
                           RationCardFile = a.RationCard,
                           VoterIdFile = a.VoterId,
                           DegreeFile = a.DegreeCertificateFile,
                           Salary = a.Salary,
                           DateOfJoin = SqlFunctions.DateName("day", a.DOJ).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOJ.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOJ) ,
                           PAN = a.PAN,
                           BankAcName = a.AccountName,
                           BankAcNo = a.AccountNo,
                           BankName = a.BankName,
                           LicenseNo=a.LicenseNo,
                           txt_Expiredate = SqlFunctions.DateName("day", a.LicenseExpireDate).Trim() + "/" + SqlFunctions.StringConvert((double)a.LicenseExpireDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.LicenseExpireDate),
                           
                       }).First();

            return ans;
        }

        public List<Employee> getEmployee()
        {           
            var ans = (from t1 in dc.OtherEmployees
                       from t2 in dc.EmployeeTypes
                       from t3 in dc.EmployeeDesignations
                       where t2.EmployeeTypeId == t1.EmployeeTypeId && t3.EmployeeDesignationId == t1.EmployeeDesignationId 
                       select new { EmployeeId = t1.EmployeeRegisterId, EmpId = t1.EmployeeId, Employee = t1.EmployeeName, DOB = t1.DOJ, DOJ = t1.DOJ, DOR = t1.DOR, DOResigned = t1.DOL, Address = t1.Address, Contact = t1.Mobile,  EmgPerson = t1.EmergencyContactPerson, EmgContact = t1.EmergencyContactNumber,EmployeeTypes =t2.EmployeeTypes,EmployeeDesignations=t3.Designation,Status=t1.EmployeeStatus }).ToList()
                       .Select(q => new Employee
                       {
                           EmployeeId = q.EmployeeId,
                           EmpRegId = QSCrypt.Encrypt(q.EmployeeId.ToString()),
                           EmpId = q.EmpId,
                           EmployeeName = q.Employee,
                           Address = q.Address,
                           
                           Contact = q.Contact,
                           Edit = " <i class='fa fa-pencil-square-o'></i>",
                           PersonContact = q.EmgPerson + " / " + q.EmgContact,
                           EmployeeTypes = q.EmployeeTypes,
                           EmployeeDesianations = q.EmployeeDesignations,
                            Status=q.Status
                       }).ToList();
            return ans;
        }

        public OtherEmployee getEmployeeByImg(string imgFile)
        {
            var ans = dc.OtherEmployees.Where(q => q.ImgFile.Equals(imgFile)).SingleOrDefault();
            return ans;
        }

        public string addOtherEmployee(string Fname, string Mname, string Lname, DateTime DOB, string Gender, string Bloodgrp, string Religion, string Nationality, string Community, string Marital, int employeetype, int empdesignation, string licenseno, DateTime? Expirdate, string Contact, long Mobile, string Email, string Address1, string Address2, string City, string State, string Country, string PIN, string PermanentAddress1, string PermanentAddress2, string PermanentCity, string PermanentState, string PermanentCountry, string PermanentPIN, string EmgPerson, long EmgContact, string Relationship)
        {
            OtherEmployee other = new OtherEmployee()
            {
                EmployeeName = Fname,
                Middlename=Mname,
                Lastname=Lname,
                Gender = Gender,
                DOB = DOB,
                BloodGroup = Bloodgrp,
                Community = Community,
                Religion = Religion,
                Nationality = Nationality,
                MaritalStatus=Marital,
                Phone = Contact,
                Email =Email,
                Mobile=Mobile,
                EmergencyContactPerson = EmgPerson,
                EmergencyContactNumber = EmgContact,
                EmergencyContactpersonRelationship=Relationship,
                Address = Address1,
                Address2=Address2,
                City = City,
                State = State,
                Country = Country,
                ZipCode=PIN,
                PermanentAddress1=PermanentAddress1,
                PermanentAddress2=PermanentAddress2,
                PermanentCity=PermanentCity,
                PermanentState=PermanentState,
                PermanentCountry=PermanentCountry,
                PermanentZipCode=PermanentPIN,
                LicenseExpireDate=Expirdate,
               LicenseNo=licenseno,
               EmployeeTypeId=employeetype,
               EmployeeDesignationId=empdesignation,
                EmployeeStatus = true
            };
            dc.OtherEmployees.Add(other);
            dc.SaveChanges();
            var empregid = other.EmployeeRegisterId;//emp.erid;
            var ename = other.EmployeeName;//emp.name;
            var ans = dc.SchoolSettings.Select(q => q).First();
            var schoolcode = ans.SchoolCode;
            var empidformat = ans.Non_StaffIDFormat;
            var updateotheremp = dc.OtherEmployees.Where(q => q.EmployeeRegisterId == empregid).First();
            updateotheremp.EmployeeId = (schoolcode+empidformat + (empregid).ToString()).ToString();
            updateotheremp.UserName = (ename + (empregid).ToString()).ToString();
            var path = HttpContext.Current.Server.MapPath("~/img/OthersPhotos/" + updateotheremp.EmployeeId);
            System.IO.Directory.CreateDirectory(path);
            dc.SaveChanges();
            return updateotheremp.EmployeeId;
        }

        public OtherEmployee GetOtherEmployeeDetails(string epmid)
        {
            var ans = dc.OtherEmployees.Where(q => q.EmployeeId == epmid).FirstOrDefault();
            return ans;
        }

        internal string addOtherEmployee2(string Empid, string txt_Experience, HttpPostedFileBase Resume, HttpPostedFileBase Certificate, int count1, string Course, string Insitute, string University, string Year, string Mark, int count3, string EmpInst, string Designation, string Expertise, string EmpSdate, string EmpEdate, string empTotal, string Email, HttpPostedFileBase empphoto, HttpPostedFileBase RationCard, HttpPostedFileBase VoterId, DateTime txt_doj, long? txt_Salary, string txt_AcNo, string txt_AcName, string txt_BName, string txt_Pan_Number, long psw)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            FileHelper fh = new FileHelper();
            var emp = dc.OtherEmployees.Where(q => q.EmployeeId.Equals(Empid)).Select(s => new { erid = s.EmployeeRegisterId, name = s.EmployeeName }).First();
            var empregid = emp.erid;
            var update = dc.OtherEmployees.Where(q => q.EmployeeRegisterId.Equals(empregid)).SingleOrDefault();
            string name;
            string foldername = "OthersPhotos";
            long FileSzie = 0;
            string ResumeCertificate = ""; string DegreeCertificate = ""; string RationCardCertificate = ""; string VoterIdCertificate = ""; string EmployeePhoto = ""; string courseCertificate = "";
            if (Resume != null)
            {
                name = "Resume";
                FileSzie = 512000;
                var ResumeCertificate1 = fh.CheckEmployeeFile(Resume, Empid, name, FileSzie, foldername);
                if (ResumeCertificate1.FileSizeId == 98)
                {
                    string ErrorMessage = "Resume certificate file size exceeds,whithin 512kb";
                    return ErrorMessage;
                }
                else if (ResumeCertificate1.FileTypeId == 99)
                {
                    string ErrorMessage = "Resume certificate file format not supported";
                    return ErrorMessage;
                }
                else if (ResumeCertificate1.FileSizeId != 98 && ResumeCertificate1.FileTypeId != 99 && ResumeCertificate1.filename != "")
                {
                    ResumeCertificate = ResumeCertificate1.filename;
                    update.ResumeFile = ResumeCertificate;
                }
            }
            if (Certificate != null)
            {
                name = "DegreeCertificate";
                FileSzie = 512000;
                var DegreeCertificate2 = fh.CheckEmployeeFile(Certificate, Empid, name, FileSzie, foldername);
                if (DegreeCertificate2.FileSizeId == 98)
                {
                    string ErrorMessage = "Degree certificate file size exceeds,whithin 512kb";
                    return ErrorMessage;
                }
                else if (DegreeCertificate2.FileTypeId == 99)
                {
                    string ErrorMessage = "Degree certificate file format not supported";
                    return ErrorMessage;
                }
                else if (DegreeCertificate2.FileSizeId != 98 && DegreeCertificate2.FileTypeId != 99 && DegreeCertificate2.filename != "")
                {
                    DegreeCertificate = DegreeCertificate2.filename;
                    update.DegreeCertificateFile = DegreeCertificate;
                }
            }
            if (RationCard != null)
            {
                name = "RationCard";
                FileSzie = 512000;
                var RationCardCertificate1 = fh.CheckEmployeeFile(RationCard, Empid, name, FileSzie, foldername);
                if (RationCardCertificate1.FileSizeId == 98)
                {
                    string ErrorMessage = "RationCard certificate file size exceeds,whithin 512kb";
                    return ErrorMessage;
                }
                else if (RationCardCertificate1.FileTypeId == 99)
                {
                    string ErrorMessage = "RationCard certificate file format not supported";
                    return ErrorMessage;
                }
                else if (RationCardCertificate1.FileSizeId != 98 && RationCardCertificate1.FileTypeId != 99 && RationCardCertificate1.filename != "")
                {
                    RationCardCertificate = RationCardCertificate1.filename;
                    update.RationCard = RationCardCertificate;
                }
            }
            if (VoterId != null)
            {
                name = "VoterId";
                FileSzie = 512000;
                var VoterIdCertificate1 = fh.CheckEmployeeFile(VoterId, Empid, name, FileSzie, foldername);
                if (VoterIdCertificate1.FileSizeId == 98)
                {
                    string ErrorMessage = "VoterId certificate file size exceeds,whithin 512kb";
                    return ErrorMessage;
                }
                else if (VoterIdCertificate1.FileTypeId == 99)
                {
                    string ErrorMessage = "VoterId certificate file format not supported";
                    return ErrorMessage;
                }
                else if (VoterIdCertificate1.FileSizeId != 98 && VoterIdCertificate1.FileTypeId != 99 && VoterIdCertificate1.filename != "")
                {
                    VoterIdCertificate = VoterIdCertificate1.filename;
                    update.VoterId = VoterIdCertificate;
                }
            }
            if (empphoto != null)
            {
                name = "Photo";
                FileSzie = 512000;
                var EmployeePhoto1 = fh.CheckEmployeeImageFile(empphoto, Empid, name, FileSzie, foldername);
                if (EmployeePhoto1.FileSizeId == 98)
                {
                    string ErrorMessage = "Employee photo file size exceeds,whithin 512kb";
                    return ErrorMessage;
                }
                else if (EmployeePhoto1.FileTypeId == 99)
                {
                    string ErrorMessage = "Employee photo file format not supported";
                    return ErrorMessage;
                }
                else if (EmployeePhoto1.FileSizeId != 98 && EmployeePhoto1.FileTypeId != 99 && EmployeePhoto1.filename != "")
                {
                    EmployeePhoto = EmployeePhoto1.filename;
                    update.ImgFile = EmployeePhoto;
                }
            }


            if (txt_Experience != "")
            {
                update.Experience = txt_Experience;
            }   
            update.DOJ = txt_doj;
            update.Salary = txt_Salary;
            update.AccountNo = txt_AcNo;
            update.AccountName = txt_AcName;
            update.BankName = txt_BName;
            update.PAN = txt_Pan_Number;
            update.DOR = date;
            update.Password = FormsAuthentication.HashPasswordForStoringInConfigFile(psw.ToString(), "SHA1"); 
            dc.SaveChanges();
            string[] Single_Course = Course.Split(',');
            string[] Single_Insitute = Insitute.Split(',');
            string[] Single_University = University.Split(',');
            string[] Single_Year = Year.Split(',');
            string[] Single_Mark = Mark.Split(',');
            for (int i = 0; i < count1; i++)
            {
                if (Single_Course[i] != "" && Single_Insitute[i] != "" && Single_Year[i] != "" && Single_Mark[i] != "")
                {
                    TechEmployeeEducation edu = new TechEmployeeEducation()
                    {
                        OtherEmployeeRegisterId = empregid,
                        Cours = Single_Course[i],
                        Institute = Single_Insitute[i],
                        University = Single_University[i],
                        YearOfCompletion = Single_Year[i],
                        Mark = float.Parse(Single_Mark[i])
                    };

                    dc.TechEmployeeEducations.Add(edu);
                    dc.SaveChanges();
                }
            }
            string[] Single_EmpInst = EmpInst.Split(',');
            string[] Single_Designation = Designation.Split(',');
            string[] Single_Expertise = Expertise.Split(',');
            string[] Single_EmpSdate = EmpSdate.Split(',');
            string[] Single_EmpEdate = EmpEdate.Split(',');
            string[] Single_empTotal = empTotal.Split(',');

            for (int i = 0; i < count3; i++)
            {
                if (Single_EmpInst[i] != "" && Single_Designation[i] != "" && Single_Expertise[i] != "" && Single_EmpSdate[i] != "" && Single_EmpEdate[i] != "" && Single_empTotal[i] != "")
                {
                    TechEmployeeEmployment emps = new TechEmployeeEmployment()
                    {
                        OtherEmployeeRegisterId = empregid,
                        Institute = Single_EmpInst[i],
                        Designation = Single_Designation[i],
                        Expertise = Single_Expertise[i],
                        StartDate = DateTime.Parse(Single_EmpSdate[i]),
                        EndDate = DateTime.Parse(Single_EmpEdate[i]),
                        TotalYears = Single_empTotal[i]
                    };
                    dc.TechEmployeeEmployments.Add(emps);
                    dc.SaveChanges();
                }
            }
            string Message = "True";
            return Message;
        }
        internal string addExcelOtherEmployee2(string Empid, string txt_Experience, string Resume, string Certificate, int count1, string Course, string Insitute, string University, string Year, string Mark, int count3, string EmpInst, string Designation, string Expertise, string EmpSdate, string EmpEdate, string empTotal, string Email, string empphoto, string RationCard, string VoterId, DateTime txt_doj, long? txt_Salary, string txt_AcNo, string txt_AcName, string txt_BName, string txt_Pan_Number, long psw)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            FileHelper fh = new FileHelper();
            var emp = dc.OtherEmployees.Where(q => q.EmployeeId.Equals(Empid)).Select(s => new { erid = s.EmployeeRegisterId, name = s.EmployeeName }).First();
            var empregid = emp.erid;
            var update = dc.OtherEmployees.Where(q => q.EmployeeRegisterId.Equals(empregid)).SingleOrDefault();
            string name;
            string foldername = "OthersPhotos";
            long FileSzie = 0;
            string ResumeCertificate = ""; string DegreeCertificate = ""; string RationCardCertificate = ""; string VoterIdCertificate = ""; string EmployeePhoto = ""; string courseCertificate = "";

            update.ResumeFile = Resume;


            update.DegreeCertificateFile = Certificate;


            update.RationCard = RationCard;


            update.VoterId = VoterId;


            update.ImgFile = empphoto;

            if (txt_Experience != "")
            {
                update.Experience = txt_Experience;
            }  
            
            update.DOJ = txt_doj;
            update.Salary = txt_Salary;
            update.AccountNo = txt_AcNo;
            update.AccountName = txt_AcName;
            update.BankName = txt_BName;
            update.PAN = txt_Pan_Number;
            update.DOR = date;
            update.Password = FormsAuthentication.HashPasswordForStoringInConfigFile(psw.ToString(), "SHA1");
            dc.SaveChanges();
            string[] Single_Course = Course.Split(',');
            string[] Single_Insitute = Insitute.Split(',');
            string[] Single_University = University.Split(',');
            string[] Single_Year = Year.Split(',');
            string[] Single_Mark = Mark.Split(',');
            for (int i = 0; i < count1; i++)
            {
                if (Single_Course[i] != "" && Single_Insitute[i] != "" && Single_Year[i] != "" && Single_Mark[i] != "")
                {
                    TechEmployeeEducation edu = new TechEmployeeEducation()
                    {
                        OtherEmployeeRegisterId = empregid,
                        Cours = Single_Course[i],
                        Institute = Single_Insitute[i],
                        University = Single_University[i],
                        YearOfCompletion = Single_Year[i],
                        Mark = float.Parse(Single_Mark[i])
                    };

                    dc.TechEmployeeEducations.Add(edu);
                    dc.SaveChanges();
                }
            }
            string[] Single_EmpInst = EmpInst.Split(',');
            string[] Single_Designation = Designation.Split(',');
            string[] Single_Expertise = Expertise.Split(',');
            string[] Single_EmpSdate = EmpSdate.Split(',');
            string[] Single_EmpEdate = EmpEdate.Split(',');
            string[] Single_empTotal = empTotal.Split(',');

            for (int i = 0; i < count3; i++)
            {
                if (Single_EmpInst[i] != "" && Single_Designation[i] != "" && Single_Expertise[i] != "" && Single_EmpSdate[i] != "" && Single_EmpEdate[i] != "" && Single_empTotal[i] != "")
                {
                    TechEmployeeEmployment emps = new TechEmployeeEmployment()
                    {
                        OtherEmployeeRegisterId = empregid,
                        Institute = Single_EmpInst[i],
                        Designation = Single_Designation[i],
                        Expertise = Single_Expertise[i],
                        StartDate = DateTime.Parse(Single_EmpSdate[i]),
                        EndDate = DateTime.Parse(Single_EmpEdate[i]),
                        TotalYears = Single_empTotal[i]
                    };
                    dc.TechEmployeeEmployments.Add(emps);
                    dc.SaveChanges();
                }
            }
            string Message = "True";
            return Message;
        }

        public IEnumerable<Object> EmployeeListToMarkAttendance(DateTime passDate)
        {
            var list = (from a in dc.OtherEmployees
                        where a.EmployeeStatus == true &&
                        a.DOJ <= passDate
                        orderby a.EmployeeId
                        select new
                        {
                            employeeId = a.EmployeeId,
                            employeeName = a.EmployeeName,
                            empRegId = a.EmployeeRegisterId
                        }).OrderBy(x=>x.empRegId).ToList().Select(c => new FacultyListForAttendance
                        {
                            employeeId = c.employeeId,
                            employeeName = c.employeeName,
                            empRegId = QSCrypt.Encrypt(c.empRegId.ToString())
                        }).ToList();
            return list;
        }

        public void updateOtherEmployee1(int empRegId, string Fname, string Mname, string Lname, DateTime DOB, string Gender, string Bloodgrp, string Religion, string Nationality, string Community, string Marital, int employeetype, int empdesignation,  string Contact, long Mobile, string Email, string Address1, string Address2, string City, string State, string Country, string PIN, string PermanentAddress1, string PermanentAddress2, string PermanentCity, string PermanentState, string PermanentCountry, string PermanentPIN, string EmgPerson, long EmgContact, string Relationship,string licenseNo,DateTime? Expiredate)
        {
            var update =dc.OtherEmployees.Where(q=>q.EmployeeRegisterId == empRegId).FirstOrDefault();
            update.EmployeeName = Fname;
            update.Middlename = Mname;
            update.Lastname= Lname;
            update.DOB = DOB;
            update.Gender = Gender;
            update.MaritalStatus = Marital;
            update.BloodGroup = Bloodgrp;
            update.Community = Community;
            update.Religion = Religion;
            update.Nationality = Nationality;
            update.EmployeeTypeId = employeetype;
            update.EmployeeDesignationId = empdesignation;
            update.Phone =Contact;
            update.Mobile = Mobile;
            update.Email = Email;
            update.Address = Address1;
            update.Address2 = Address2;
            update.City = City;
            update.State = State;
            update.Country = Country;
            update.ZipCode = PIN;
            update.PermanentAddress1 = PermanentAddress1;
            update.PermanentAddress2 = PermanentAddress2;
            update.PermanentCity = PermanentCity;
            update.PermanentState = PermanentState;
            update.PermanentCountry = PermanentCountry;
            update.PermanentZipCode = PermanentPIN;
            update.EmergencyContactPerson = EmgPerson;
            update.EmergencyContactNumber = EmgContact;
            update.EmergencyContactpersonRelationship = Relationship;
            update.LicenseNo = licenseNo;
            update.LicenseExpireDate = Expiredate;
            dc.SaveChanges();
        }
        internal string updateotherEmployee2(int empRegId, string txt_Experience, HttpPostedFileBase Resume, HttpPostedFileBase Certificate, int count1, string Course, string Insitute, string University, string Year, string Mark, int count3, string EmpInst, string Designation, string Expertise, string EmpSdate, string EmpEdate, string empTotal, string Email, HttpPostedFileBase empphoto, HttpPostedFileBase RationCard, HttpPostedFileBase VoterId, DateTime txt_doj, long? txt_Salary, string txt_AcNo, string txt_AcName, string txt_BName, string txt_Pan_Number, string EducationalDetailsId, string EmployeeExperienceId, long? psw, DateTime? txt_dor)
        {
            FileHelper fh = new FileHelper();
            var emp = dc.OtherEmployees.Where(q => q.EmployeeRegisterId == empRegId).Select(s => new { erid = s.EmployeeRegisterId, name = s.EmployeeName, empid = s.EmployeeId }).First();
            string Empid = emp.empid;
            var update = dc.OtherEmployees.Where(q => q.EmployeeRegisterId.Equals(empRegId)).SingleOrDefault();
            string name;
            string foldername = "OthersPhotos";
            long FileSzie = 0;
            string ResumeCertificate = ""; string DegreeCertificate = ""; string RationCardCertificate = ""; string VoterIdCertificate = ""; string EmployeePhoto = ""; string courseCertificate = "";
            if (Resume != null)
            {
                name = "Resume";
                FileSzie = 512000;
                var ResumeCertificate1 = fh.CheckEmployeeFile(Resume, Empid, name, FileSzie, foldername);
                if (ResumeCertificate1.FileSizeId == 98)
                {
                    string ErrorMessage = "Resume certificate file size exceeds,whithin 512kb";
                    return ErrorMessage;
                }
                else if (ResumeCertificate1.FileTypeId == 99)
                {
                    string ErrorMessage = "Resume certificate file format not supported";
                    return ErrorMessage;
                }
                else if (ResumeCertificate1.FileSizeId != 98 && ResumeCertificate1.FileTypeId != 99 && ResumeCertificate1.filename != "")
                {
                    ResumeCertificate = ResumeCertificate1.filename;
                    update.ResumeFile = ResumeCertificate;
                }
            }
            if (Certificate != null)
            {
                name = "DegreeCertificate";
                FileSzie = 512000;
                var DegreeCertificate2 = fh.CheckEmployeeFile(Certificate, Empid, name, FileSzie, foldername);
                if (DegreeCertificate2.FileSizeId == 98)
                {
                    string ErrorMessage = "Degree certificate file size exceeds,whithin 512kb";
                    return ErrorMessage;
                }
                else if (DegreeCertificate2.FileTypeId == 99)
                {
                    string ErrorMessage = "Degree certificate file format not supported";
                    return ErrorMessage;
                }
                else if (DegreeCertificate2.FileSizeId != 98 && DegreeCertificate2.FileTypeId != 99 && DegreeCertificate2.filename != "")
                {
                    DegreeCertificate = DegreeCertificate2.filename;
                    update.DegreeCertificateFile = DegreeCertificate;
                }
            }
            if (RationCard != null)
            {
                name = "RationCard";
                FileSzie = 512000;
                var RationCardCertificate1 = fh.CheckEmployeeFile(RationCard, Empid, name, FileSzie, foldername);
                if (RationCardCertificate1.FileSizeId == 98)
                {
                    string ErrorMessage = "RationCard certificate file size exceeds,whithin 512kb";
                    return ErrorMessage;
                }
                else if (RationCardCertificate1.FileTypeId == 99)
                {
                    string ErrorMessage = "RationCard certificate file format not supported";
                    return ErrorMessage;
                }
                else if (RationCardCertificate1.FileSizeId != 98 && RationCardCertificate1.FileTypeId != 99 && RationCardCertificate1.filename != "")
                {
                    RationCardCertificate = RationCardCertificate1.filename;
                    update.RationCard = RationCardCertificate;
                }
            }
            if (VoterId != null)
            {
                name = "VoterId";
                FileSzie = 512000;
                var VoterIdCertificate1 = fh.CheckEmployeeFile(VoterId, Empid, name, FileSzie, foldername);
                if (VoterIdCertificate1.FileSizeId == 98)
                {
                    string ErrorMessage = "VoterId certificate file size exceeds,whithin 512kb";
                    return ErrorMessage;
                }
                else if (VoterIdCertificate1.FileTypeId == 99)
                {
                    string ErrorMessage = "VoterId certificate file format not supported";
                    return ErrorMessage;
                }
                else if (VoterIdCertificate1.FileSizeId != 98 && VoterIdCertificate1.FileTypeId != 99 && VoterIdCertificate1.filename != "")
                {
                    VoterIdCertificate = VoterIdCertificate1.filename;
                    update.VoterId = VoterIdCertificate;
                }
            }
            if (empphoto != null)
            {
                name = "Photo";
                FileSzie = 512000;
                var EmployeePhoto1 = fh.CheckEmployeeImageFile(empphoto, Empid, name, FileSzie, foldername);
                if (EmployeePhoto1.FileSizeId == 98)
                {
                    string ErrorMessage = "Employee photo file size exceeds,whithin 512kb";
                    return ErrorMessage;
                }
                else if (EmployeePhoto1.FileTypeId == 99)
                {
                    string ErrorMessage = "Employee photo file format not supported";
                    return ErrorMessage;
                }
                else if (EmployeePhoto1.FileSizeId != 98 && EmployeePhoto1.FileTypeId != 99 && EmployeePhoto1.filename != "")
                {
                    EmployeePhoto = EmployeePhoto1.filename;
                    update.ImgFile = EmployeePhoto;
                }
            }
            if (txt_Experience != "")
            {
                update.Experience = txt_Experience;
            }  
            
            update.DOJ = txt_doj;
            update.Salary = txt_Salary;
            update.AccountNo = txt_AcNo;
            update.AccountName = txt_AcName;
            update.BankName = txt_BName;
            update.PAN = txt_Pan_Number;
            DateTime date = DateTimeByZone.getCurrentDate();
            if (update.DOR == null)
            {
                update.DOR = date.Date;
            }
               if(txt_dor !=null)
               {
                   update.EmployeeStatus = false;
                   update.DOL = txt_dor;
               }
            dc.SaveChanges();
            string[] Single_Course = Course.Split(',');
            string[] Single_Insitute = Insitute.Split(',');
            string[] Single_University = University.Split(',');
            string[] Single_Year = Year.Split(',');
            string[] Single_Mark = Mark.Split(',');
            string[] Single_Educationalid = EducationalDetailsId.Split(',');
            ArrayList ids = new ArrayList();
            for (int i = 0; i < count1; i++)
            {
                if ( Convert.ToInt32(Single_Educationalid[i]) !=0)
                {
                    int Edu_id = Convert.ToInt16(Single_Educationalid[i]);
                    var updateEmployeeEducation = dc.TechEmployeeEducations.Where(q => q.OtherEmployeeRegisterId.Value.Equals(empRegId) && q.TechEmployeeEducationId.Equals(Edu_id)).First();
                    updateEmployeeEducation.Cours = Single_Course[i];
                    updateEmployeeEducation.Institute = Single_Insitute[i];
                    updateEmployeeEducation.University = Single_University[i];
                    updateEmployeeEducation.YearOfCompletion = Single_Year[i];
                    updateEmployeeEducation.Mark = float.Parse(Single_Mark[i]);
                    updateEmployeeEducation.OtherEmployeeRegisterId = empRegId;
                    dc.SaveChanges();
                    ids.Add(Edu_id);
                }
                else
                {
                    TechEmployeeEducation edu = new TechEmployeeEducation()
                    {
                        //EmployeeRegisterId = empRegId,
                        Cours = Single_Course[i],
                        Institute = Single_Insitute[i],
                        University = Single_University[i],
                        YearOfCompletion = Single_Year[i],
                        Mark = float.Parse(Single_Mark[i]),
                        OtherEmployeeRegisterId = empRegId
                    };

                    dc.TechEmployeeEducations.Add(edu);
                    dc.SaveChanges();
                }
            }
            string[] Single_EmpInst = EmpInst.Split(',');
            string[] Single_Designation = Designation.Split(',');
            string[] Single_Expertise = Expertise.Split(',');
            string[] Single_EmpSdate = EmpSdate.Split(',');
            string[] Single_EmpEdate = EmpEdate.Split(',');
            string[] Single_empTotal = empTotal.Split(',');
            string[] Single_EmployeeExperienceId = EmployeeExperienceId.Split(',');
            for (int i = 0; i < count3; i++)
            {
                if (Convert.ToInt16(Single_EmployeeExperienceId[i]) != 0)
                {
                    int exp_id = Convert.ToInt16(Single_EmployeeExperienceId[i]);
                    var updateEmployeeEducation = dc.TechEmployeeEmployments.Where(q => q.OtherEmployeeRegisterId.Value.Equals(empRegId) && q.TechEmployeeEmploymentId.Equals(exp_id)).First();
                    updateEmployeeEducation.Institute = Single_EmpInst[i];
                    updateEmployeeEducation.Designation = Single_Designation[i];
                    updateEmployeeEducation.Expertise = Single_Expertise[i];
                    updateEmployeeEducation.StartDate = DateTime.Parse(Single_EmpSdate[i]);
                    updateEmployeeEducation.EndDate = DateTime.Parse(Single_EmpEdate[i]);
                    updateEmployeeEducation.TotalYears = Single_empTotal[i];
                    dc.SaveChanges();
                }
                else
                {
                    TechEmployeeEmployment emps = new TechEmployeeEmployment()
                    {
                        //EmployeeRegisterId = empRegId,
                        Institute = Single_EmpInst[i],
                        Designation = Single_Designation[i],
                        Expertise = Single_Expertise[i],
                        StartDate = DateTime.Parse(Single_EmpSdate[i]),
                        EndDate = DateTime.Parse(Single_EmpEdate[i]),
                        TotalYears =Single_empTotal[i],
                        OtherEmployeeRegisterId = empRegId
                    };

                    dc.TechEmployeeEmployments.Add(emps);
                    dc.SaveChanges();
                }
            }
            var update1 = dc.OtherEmployees.Where(q => q.EmployeeRegisterId.Equals(empRegId)).SingleOrDefault();
            if (update1.Password == null || update1.Password == "")
            {
                update1.Password = FormsAuthentication.HashPasswordForStoringInConfigFile(psw.ToString(), "SHA1");
                dc.SaveChanges();
            }

            string Message = "Success";
            return Message;
        }
        internal bool checkEmail(int empRegId, string Email)
        {
            var ans = (from t1 in dc.TechEmployees
                       from t2 in dc.OtherEmployees
                       where t2.EmployeeRegisterId != empRegId && t1.Email == Email && t2.Email == Email
                       select t1).ToList();

            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<EmploeeListByType>getEmploeeListByEmployeeTypeId(int typeId)
        {
            var list = (from a in dc.OtherEmployees
                        where a.EmployeeTypeId == typeId
                        select new
                        {
                            EmpRegId = a.EmployeeRegisterId,
                            EmpId = a.EmployeeId,
                            EmpName = a.EmployeeName,
                        }).ToList().Select(a => new EmploeeListByType
                            {
                                EmployeeId = a.EmpId,
                                EmployeeRegId = QSCrypt.Encrypt(a.EmpRegId.ToString()),
                                Name = a.EmpName
                            }).ToList();
            return list;
        }

        //jegadeesh

        public IEnumerable GetOtherEmployee()
        {
            var ans = (from a in dc.OtherEmployees
                       select new { EmployeeRegisterId = a.EmployeeRegisterId, EmployeeName = a.EmployeeName + " (" + a.EmployeeId + ")" }).ToList();// dc.TechEmployees.Select(q => q);
            return ans;
        }
        public IEnumerable GetParticularOtherEmployeeName(int id)
        {
            var ans = dc.OtherEmployees.Where(q => q.EmployeeRegisterId.Equals(id)).FirstOrDefault().EmployeeName;
            return ans;
        }

        public List<Eprofile> SearchOtherEmployeeProfile(int id)
        {
            var q2 = (from a in dc.OtherEmployees
                      where a.EmployeeRegisterId == id
                      select new Eprofile
                      {
                          EmployeeRegisterId = a.EmployeeRegisterId,
                          EmployeeId = a.EmployeeId,
                          EmployeeName = a.EmployeeName,
                          Gender = a.Gender,
                          BloodGroup = a.BloodGroup,
                          Community = a.Community,
                          Email = a.Email,
                          Religion = a.Religion,
                          Nationality = a.Nationality,
                          Contact = a.Contact,
                          Address = a.Address,
                          State = a.State,
                          //DOB = a.DOB.Value,
                          //DOL = a.DOL.Value,
                          //DOR = a.DOR.Value,
                          //DOJ = a.DOJ.Value,
                          DateOfBirth = SqlFunctions.DateName("day", a.DOB).Trim() + "-" + SqlFunctions.StringConvert((double)a.DOB.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.DOB),
                          DateOfJoin = SqlFunctions.DateName("day", a.DOJ).Trim() + "-" + SqlFunctions.StringConvert((double)a.DOJ.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.DOJ),
                          DateOfRegister = SqlFunctions.DateName("day", a.DOR).Trim() + "-" + SqlFunctions.StringConvert((double)a.DOR.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.DOR),
                          DateOfLeaving = SqlFunctions.DateName("day", a.DOL).Trim() + "-" + SqlFunctions.StringConvert((double)a.DOL.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", a.DOL),
                          Qualification = a.Qualification,
                          EmergencyContactPerson = a.EmergencyContactPerson,
                          EmergencyContactNumber = a.EmergencyContactNumber,
                          EmployeeStatus = a.EmployeeStatus,
                          Experience = a.Experience,
                          Expertise = a.Expertise,
                          ImgFile = a.ImgFile,
                          City = a.City
                      }).ToList();
                      //.Select(q => new Eprofile
                      //{
                      //    EmployeeRegisterId = q.EmployeeRegisterId,
                      //    EmployeeId = q.EmployeeId,
                      //    EmployeeName = q.EmployeeName,
                      //    Gender = q.Gender,
                      //    BloodGroup = q.BloodGroup,
                      //    Community = q.Community,
                      //    Email = q.Email,
                      //    Religion = q.Religion,
                      //    Nationality = q.Nationality,
                      //    LandlineNo = q.LandlineNo,
                      //    Contact = q.Contact,
                      //    Addressline1 = q.Addressline1,
                      //    State = q.State,
                      //    DateOfBirth = q.DOB.ToString("MMM dd, yyyy"),
                      //    DateOfLeaving = q.DOL.ToString("MMM dd, yyyy"),
                      //    DateOfRegister = q.DOR.ToString("MMM dd, yyyy"),
                      //    DateOfJoin = q.DOJ.ToString("MMM dd, yyyy"),
                      //    Qualification = q.Qualification,
                      //    EmergencyContactPerson = q.EmergencyContactPerson,
                      //    EmergencyContactNumber = q.EmergencyContactNumber,
                      //    EmployeeStatus = q.EmployeeStatus,
                      //    Experience = q.Experience,
                      //    Expertise = q.Expertise,
                      //    ImgFile = q.ImgFile,
                      //    City = q.City
                      //}).ToList(); 
            return q2;
        }
        public IEnumerable GetjsonOtherEmployee()
        {
            var ans = dc.OtherEmployees.Select(q => q).Select(s => new { empid = s.EmployeeRegisterId, empname = s.EmployeeName }).ToList();
            return ans;
        }
        public IEnumerable GetjsonOtherEmployeeId(int eid)
        {
            var ans = (from a in dc.EmployeeDesignations
                       from b in dc.OtherEmployees
                       where a.EmployeeDesignationId == eid && b.EmployeeDesignationId == a.EmployeeDesignationId
                       select new
                       {
                           empRid = b.EmployeeRegisterId,
                           empid = b.EmployeeId,
                           EmpName = b.EmployeeName + "(" + b.EmployeeId + ")"
                       }).ToList();
            return ans;
        }

        public Eprofile GethostelEmployee(string EmployeeId)
        {
            var ans = (from a in dc.OtherEmployees
                       from b in dc.EmployeeTypes
                       from c in dc.EmployeeDesignations
                      
                       where a.EmployeeId == EmployeeId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId && a.EmployeeStatus == true 
                       select new Eprofile
                       {
                           EmployeeRegisterId = a.EmployeeRegisterId,
                           EmployeeName = a.EmployeeName + " " + a.Lastname,
                           EmployeeId = a.EmployeeId,
                           EmployeeType = b.EmployeeTypes,
                           EmployeeDesignation = c.Designation,
                           ImgFile = a.ImgFile,
                           Gender = a.Gender,
                           Etype="Other",
                           Salary = a.Salary
                       }).FirstOrDefault();
            return ans;
        }
        public Eprofile GethostelEditEmployee(int EmployeeRegId)
        {
            var ans = (from a in dc.OtherEmployees
                       from b in dc.EmployeeTypes
                       from c in dc.EmployeeDesignations
                       
                       where a.EmployeeRegisterId == EmployeeRegId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId &&  a.EmployeeStatus == true
                       select new Eprofile
                       {
                           EmployeeRegisterId = a.EmployeeRegisterId,
                           EmployeeName = a.EmployeeName + " " + a.Lastname,
                           EmployeeId = a.EmployeeId,
                           EmployeeType = b.EmployeeTypes,
                           EmployeeDesignation = c.Designation,
                           ImgFile = a.ImgFile,
                           Gender = a.Gender,
                           Etype = "Other"
                       }).FirstOrDefault();
            return ans;
        }
        public Eprofile GetEmployeeForSalaryConfig(string EmployeeId)
        {
            var ans = (from a in dc.OtherEmployees
                       from b in dc.EmployeeTypes
                       from c in dc.EmployeeDesignations
                       where a.EmployeeId == EmployeeId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId && a.EmployeeStatus == true && !(from d in dc.EmployeeSalaryConfigurations

                                                                                                                                                                                       where d.OtherEmployeeregisterId == a.EmployeeRegisterId
                                                                                                                                                                                       select d.OtherEmployeeregisterId).Contains(a.EmployeeRegisterId)
                       select new Eprofile
                       {
                           EmployeeRegisterId = a.EmployeeRegisterId,
                           EmployeeName = a.EmployeeName + " " + a.Lastname,
                           EmployeeId = a.EmployeeId,
                           EmployeeType = b.EmployeeTypes,
                           EmployeeDesignation = c.Designation,
                           ImgFile = a.ImgFile,
                           Gender = a.Gender,
                           Etype = "Other",
                           Salary = a.Salary
                       }).FirstOrDefault();
            return ans;
        }

        public IEnumerable<Object> DriverList()
        {
            var FuelList = (from a in dc.OtherEmployees
                            from b in dc.EmployeeDesignations
                            where a.EmployeeTypeId == b.EmployeeTypeId &&
                            a.EmployeeDesignationId == b.EmployeeDesignationId &&
                            b.EmployeeTypeId == 5 &&
                            b.EmployeeDesignationId == 12 &&
                            a.EmployeeStatus == true
                            select new
                            {
                                DriverId = a.EmployeeRegisterId,
                                DriverName = a.EmployeeName
                            }).ToList().AsEnumerable();
            return FuelList;
        }

        public IEnumerable<Object> CleanerList()
        {
            var FuelList = (from a in dc.OtherEmployees
                            from b in dc.EmployeeDesignations
                            where a.EmployeeTypeId == b.EmployeeTypeId &&
                            a.EmployeeDesignationId == b.EmployeeDesignationId &&
                            b.EmployeeTypeId == 5 &&
                            b.EmployeeDesignationId == 13 &&
                            a.EmployeeStatus == true
                            select new
                            {
                                HelperId = a.EmployeeRegisterId,
                                HelperName = a.EmployeeName
                            }).ToList().AsEnumerable();
            return FuelList;
        }

        public bool Checkalreadysalaryconfigtoemployee(string EmployeeId)
        {
            var ans = (from a in dc.OtherEmployees

                       from d in dc.EmployeeSalaryConfigurations
                       where a.EmployeeId == EmployeeId && d.OtherEmployeeregisterId == a.EmployeeRegisterId && a.EmployeeStatus == true && d.Status == true
                       select d).FirstOrDefault();
            if (ans != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool Checkalreadyhostelroomtoemployee(string EmployeeId)
        {
            var ans = (from a in dc.OtherEmployees

                       from d in dc.RoomAllotments
                       where a.EmployeeId == EmployeeId && d.OtherEmpoyeeRegisterId == a.EmployeeRegisterId && a.EmployeeStatus == true && d.status == true
                       select d).FirstOrDefault();

            if (ans != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetEditEmployeeId(string fname,string lname,DateTime dob)
        {
            var record = dc.OtherEmployees.Where(q => q.EmployeeName == fname && q.Lastname == lname && q.DOB == dob).FirstOrDefault();
            if(record!=null)
            {
                return record.EmployeeRegisterId;
            }
            else
            {
                return 0;
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}