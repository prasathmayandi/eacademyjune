﻿using eAcademy.DataModel;
using System;
using System.Linq;

namespace eAcademy.Services
{
    public class PreAdmissionRegisterStatusFlagServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public Object GetDescription(int id, string statusflag)
        {
            var ans = dc.PreAdmissionRegisterStatusFlags.Where(q => q.StudentAdmissionId.Value.Equals(id) && q.StatusFlag.Equals(statusflag)).OrderByDescending(s => s.StatusId).FirstOrDefault().Descriptions;
            return ans;
        }

        public void AddRegisterStatusFlag(int regid, string statusflag, string description)
        {
            PreAdmissionRegisterStatusFlag add = new PreAdmissionRegisterStatusFlag()
            {
                StudentAdmissionId = regid,
                StatusFlag = statusflag,
                Descriptions = description,
                             };
            dc.PreAdmissionRegisterStatusFlags.Add(add);
            dc.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}