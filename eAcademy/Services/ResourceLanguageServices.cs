﻿using eAcademy.Models;
using eAcademy.ViewModel;
using eAcademy.DataModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eAcademy.HelperClass;



namespace eAcademy.Services
{
    public class ResourceLanguageServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public bool AddResourceLanguage(AddResourceLanguage rl)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var checkexist = db.ResourceLanguages.Where(q => q.LanquageDisplayName == rl.LanguageDisplayname || q.LanguageCultureName == rl.LanguageCultureName || q.LanguageCultureCode == rl.LanguageCultureCode || q.LanguageCountryCode == rl.LanguageCountryCode).FirstOrDefault();
            if(checkexist == null)
            {
                ResourceLanguage add = new ResourceLanguage {
                  LanquageDisplayName=rl.LanguageDisplayname,
                  LanguageCultureName=rl.LanguageCultureName,
                  LanguageCultureCode=rl.LanguageCultureCode,
                  LanguageCountryCode=rl.LanguageCountryCode,
                  Status=rl.status,
                  DateOfCreated=date,
                  DateOfUpdated=date
                };
                db.ResourceLanguages.Add(add);
                db.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }

        public List<ResourceLanguageList> GetAllresourcelanguage()
        {
            var list = db.ResourceLanguages.Select(q =>  new ResourceLanguageList {
            ResourceLanguageId=q.ResourcelanquageId,
            LanguageDisplayname=q.LanquageDisplayName,
            LanguageCultureName=q.LanguageCultureName,
            LanguageCultureCode=q.LanguageCultureCode,
            LanguageCountryCode=q.LanguageCountryCode,
            status=q.Status
            }).ToList();
            return list;
        }

        public AddResourceLanguage Editlanguage(int id)
        {
            var getrecord = db.ResourceLanguages.Where(q => q.ResourcelanquageId == id).Select(b => new AddResourceLanguage
            {
                ResourceLanguageId=b.ResourcelanquageId,
                LanguageDisplayname=b.LanquageDisplayName,
                LanguageCultureName=b.LanguageCultureName,
                LanguageCultureCode=b.LanguageCultureCode,
                LanguageCountryCode=b.LanguageCountryCode,
                status=b.Status
            }).FirstOrDefault();
            return getrecord;
        }

        public string UpdateResourceLanquage(AddResourceLanguage rl)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var checkexist = db.ResourceLanguages.Where(q => q.ResourcelanquageId != rl.ResourceLanguageId &&  q.LanguageCultureName == rl.LanguageCultureName ).ToList();
            if(checkexist.Count ==0)
            {
                var checkexist2 = db.ResourceLanguages.Where(q => q.ResourcelanquageId != rl.ResourceLanguageId && q.LanguageCultureCode == rl.LanguageCultureCode ).ToList();
                if (checkexist2.Count == 0)
                {
                    var checkexist3 = db.ResourceLanguages.Where(q => q.ResourcelanquageId != rl.ResourceLanguageId && q.LanguageCountryCode == rl.LanguageCountryCode).ToList();
                    if (checkexist3.Count == 0)
                    {
                        var getrecord = db.ResourceLanguages.Where(q => q.ResourcelanquageId == rl.ResourceLanguageId).FirstOrDefault();
                        if (getrecord != null)
                        {
                            getrecord.LanquageDisplayName = rl.LanguageDisplayname;
                            getrecord.LanguageCultureName = rl.LanguageCultureName;
                            getrecord.LanguageCultureCode = rl.LanguageCultureCode;
                            getrecord.LanguageCountryCode = rl.LanguageCountryCode;
                            getrecord.Status = rl.status;
                            getrecord.DateOfUpdated = date;
                            db.SaveChanges();
                        }
                        return "success";
                    }
                    else
                    {
                        return "Country code already exist";
                    }
                }
                else
                {
                    return "Culture code already exist";
                }
            }
            else
            {
                return "Culture name already exist";
            }

        }
        public IEnumerable GetActiveLanquage()
        {
            var list = db.ResourceLanguages.Where(q => q.Status == true).Select(b => new
            {
                ResourcelanquageId = b.ResourcelanquageId,
                CultureName =b.LanquageDisplayName + "("+ b.LanguageCultureName +")"
            }).ToList();
            return list;
        }

        public string GetCulturename(int languageid)
        {
            var record = db.ResourceLanguages.Where(q => q.ResourcelanquageId == languageid).FirstOrDefault();
            return record.LanguageCultureName;
        }

        public void ActivateLanguages(int id)
        {
            var record = db.ResourceLanguages.Where(q => q.ResourcelanquageId == id).FirstOrDefault();
            if(record !=null)
            {
                record.Status = true;
                db.SaveChanges();
            }
        }
        public void InActivateLanguages(int id)
        {
            var record = db.ResourceLanguages.Where(q => q.ResourcelanquageId == id).FirstOrDefault();
            if (record != null)
            {
                record.Status = false;
                db.SaveChanges();
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}