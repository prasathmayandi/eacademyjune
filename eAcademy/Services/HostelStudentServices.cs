﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class HostelStudentServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        
        public List<HostelStudentList> getHostelStudent()
        {
            var ans = (from t1 in dc.HostelStudents
                       from t2 in dc.Students
                       from t3 in dc.AcademicYears
                       from t4 in dc.AssignClassToStudents
                       from t5 in dc.Classes
                       from t6 in dc.Sections
                       where t1.StudentRegisterId == t2.StudentRegisterId && t4.HostelPreference == "Yes" && t1.AcademicYearId == t4.AcademicYearId && t5.ClassId == t6.ClassId && t1.StudentRegisterId == t4.StudentRegisterId && t6.SectionId == t4.SectionId && t1.AcademicYearId == t3.AcademicYearId
                       select new  { acid = t1.AcademicYearId, acYear = t3.AcademicYear1, cid = t1.ClassId, classType = t5.ClassType, sec_id = t1.SectionId, section = t6.SectionName, std_id = t1.StudentRegisterId, stdName = t2.FirstName+" "+t2.LastName, stdContact = t2.Contact.Value, stdAddress = t2.LocalAddress1+" "+t2.LocalAddress2, emgyPerson = t2.EmergencyContactPerson, emgyContact = t2.EmergencyContactNumber, status = t1.status, hsid = t1.HostelStudentId, roomNumber = t1.RoomNumber }).ToList().
                       Select(q => new HostelStudentList { acid = q.acid, acYear = q.acYear, cid = q.cid, classType = q.classType, sec_id = q.sec_id, section = q.section, std_id = q.std_id, stdName = q.stdName, stdContact = q.stdContact, stdAddress = q.stdAddress, emgyPerson = q.emgyPerson, emgyContact = q.emgyContact, status = q.status, hsid = q.hsid,HStdID=QSCrypt.Encrypt(q.hsid.ToString()), roomNumber = q.roomNumber, NameContact=q.emgyPerson+"/"+q.emgyContact, ClassSection=q.classType+"-"+q.section }).ToList();
            return ans;
        }

        public void addHostelStudent(int acid, int cid, int sec_id, int sid, string rno)
        {
            HostelStudent std = new HostelStudent()
            {
                AcademicYearId = acid,
                ClassId = cid,
                SectionId = sec_id,
                StudentRegisterId = sid,
                RoomNumber = rno,
                status = true

            };
            dc.HostelStudents.Add(std);
            dc.SaveChanges();
        }

        public void updateHostelStudent(int wid, string status)
        {
            var update = dc.HostelWardens.Where(q => q.WardenId.Equals(wid)).First();
            if (status == "Active")
            {
                update.Status = true;
            }
            else
            {
                update.Status = false;
            }
            dc.SaveChanges();
        }


        public void deleteHostelStudent(int wid)
        {
            var delete = dc.HostelWardens.Where(q => q.WardenId.Equals(wid)).First();
            dc.HostelWardens.Remove(delete);
            dc.SaveChanges();
        }
        

        internal bool checkHostelStudent(int acid, int cid, int sec_id, int sid)
        {
            var ans = dc.HostelStudents.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) && q.StudentRegisterId.Value.Equals(sid)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        internal HostelStudentList getHostelStudent(int hId)
        {
            var ans = (from t1 in dc.HostelStudents
                       from t2 in dc.Students
                       from t3 in dc.AcademicYears
                       from t4 in dc.AssignClassToStudents
                       from t5 in dc.Classes
                       from t6 in dc.Sections
                       where t1.StudentRegisterId == t2.StudentRegisterId && t4.HostelPreference == "Yes" && t1.AcademicYearId == t4.AcademicYearId && t5.ClassId == t6.ClassId && t1.StudentRegisterId == t4.StudentRegisterId && t6.SectionId == t4.SectionId && t1.AcademicYearId == t3.AcademicYearId && t1.HostelStudentId == hId
                       select new HostelStudentList { acid = t1.AcademicYearId, acYear = t3.AcademicYear1, cid = t1.ClassId, classType = t5.ClassType, sec_id = t1.SectionId, section = t6.SectionName, std_id = t1.StudentRegisterId, stdName = t2.FirstName + " " + t2.LastName, stdContact = t2.Contact.Value, stdAddress = t2.LocalAddress1+""+t2.LocalAddress2, emgyPerson = t2.EmergencyContactPerson, emgyContact = t2.EmergencyContactNumber, status = t1.status, hsid = t1.HostelStudentId, roomNumber = t1.RoomNumber }).FirstOrDefault();
                     
            return ans;
        }

        internal bool checkHostelStudent(int sid, string rno, string status)
        {
           
            if (status == "Active")
            {
               var ans = dc.HostelStudents.Where(q => q.RoomNumber.Equals(rno) && q.status.Value.Equals(true) && q.StudentRegisterId.Value.Equals(sid)).ToList();
                int count = ans.Count;
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            else
            {
                var ans = dc.HostelStudents.Where(q => q.RoomNumber.Equals(rno) && q.status.Value.Equals(false) && q.StudentRegisterId.Value.Equals(sid)).ToList();
                int count = ans.Count;
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
           
           
        }

        internal void updateHostelStudent(int hsid, string rno, string status)
        {
            var update = dc.HostelStudents.Where(q => q.HostelStudentId.Equals(hsid)).First();
            if (status == "Active")
            {
                update.status = true;
            }
            else
            {
                update.status = false;
            }
            update.RoomNumber = rno;
            dc.SaveChanges();
        }

        //jegadeesh

        public List<HostalList> GetHostelStudent(int yearid)
        {
            var ans = (from a in dc.RoomAllotments
                       from b in dc.Students
                       from c in dc.Classes
                       from d in dc.Sections
                       from e in dc.AcademicYears
                       from f in dc.AssignClassToStudents
                       from g in dc.StudentRollNumbers
                       from h in dc.HostelBlocks
                       from i in dc.HostelRooms
                       where f.AcademicYearId == yearid && f.AcademicYearId == e.AcademicYearId && a.status == true && g.AcademicYearId == f.AcademicYearId && g.ClassId == f.ClassId && g.SectionId == f.SectionId && e.AcademicYearId ==f.AcademicYearId && c.ClassId ==f.ClassId && d.SectionId ==f.SectionId  && a.StudentRegisterId == b.StudentRegisterId && f.StudentRegisterId == a.StudentRegisterId && 
                       b.StudentRegisterId == a.StudentRegisterId && g.StudentRegisterId==b.StudentRegisterId && f.Status==true && g.Status == true && h.BlockId ==a.BlockId && i.RoomId == a.RoomId
                       select new HostalList
                       {
                           AcademicYear = e.AcademicYear1,
                           StudentId=b.StudentId,
                           RollNumber=g.RollNumber,
                           StudentName = b.FirstName + " " + b.LastName,
                           Class = c.ClassType,
                           Section = d.SectionName,
                           StudentContact = b.Contact,
                           BlockName=h.BlockName,
                           RoomNumber = i.RoomName,
                           EmergencyContactPerson = b.EmergencyContactPerson,
                           EmergencyContactPersonNo = b.EmergencyContactNumber,
                           Address = b.LocalAddress1+" "+b.LocalAddress2 + "," + b.LocalCity + "," + b.LocalState
                       }).OrderBy(q=>q.RollNumber).ToList();
            return ans;

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}