﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class AdmissionTransactionServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public AdmissionTransaction getRow(int studentRegId)
        {
            var row = (from a in db.AdmissionTransactions where a.StudentRegisterId == studentRegId select a).FirstOrDefault();
            return row;
        }
        public List<GuardianStudentList> GetStudentRegId(int primaryUserRegId)
        {
            var List = (from a in db.AdmissionTransactions
                                    from b in db.Students
                                    where a.StudentRegisterId == b.StudentRegisterId &&
                                    a.PrimaryUserRegisterId == primaryUserRegId &&
                                    b.StudentStatus == true &&
                                    b.Statusflag == "Class Assigned"
                                    select new 
                                    {
                                       Stu_RegId = a.StudentRegisterId,
                                       StudentId = b.StudentId,
                                       StudentPhoto = b.Photo
                                    }).ToList().Select(a => new  GuardianStudentList
                                    {
                                        StudentPhoto = a.StudentPhoto,
                                        StudentId = a.StudentId,
                                        StudentRegId =  QSCrypt.Encrypt(a.Stu_RegId.ToString())
                                    }).ToList();
            return List;
        }

        public IEnumerable<object> getStudentList(int primaryUserRegId)
        {
            var List = (from a in db.AdmissionTransactions
                        from b in db.Students
                        from c in db.AssignClassToStudents
                        where a.StudentRegisterId == b.StudentRegisterId &&
                        a.PrimaryUserRegisterId == primaryUserRegId &&
                        b.StudentStatus == true &&
                        b.Statusflag == "Class Assigned" &&
                        b.StudentRegisterId == c.StudentRegisterId &&
                        c.Status == true
                        select new
                        {
                            StudentRegisterId = a.StudentRegisterId,
                            StudentName = b.FirstName+" "+b.LastName
                        }).ToList();
            return List;
        }

        public List<ParentStudentList> getDefaultStudent(int primaryUserRegId)
        {
            var List = (from a in db.AdmissionTransactions
                        from b in db.Students
                        where a.StudentRegisterId == b.StudentRegisterId &&
                        a.PrimaryUserRegisterId == primaryUserRegId &&
                        b.StudentStatus == true &&
                        b.Statusflag == "Class Assigned"
                        select new ParentStudentList
                        {
                            StudentRegisterId = a.StudentRegisterId,
                            StudentName = b.FirstName + " " + b.LastName
                        }).ToList();
            return List;
        }

        public List<AdmissionTransaction> GetRemainingStudent(int PrimaryUserId, int StudentId)
        {
            var ans = db.AdmissionTransactions.Where(q => q.PrimaryUserRegisterId== PrimaryUserId && q.StudentRegisterId != StudentId).Distinct().ToList();
            return ans;

        }
        public AdmissionTransaction GetPrimaryUser(int PrimaryUserAdmissionId, int StudentId)
        {
            var ans = db.AdmissionTransactions.Where(q => q.PrimaryUserRegisterId == PrimaryUserAdmissionId && q.StudentRegisterId == StudentId).FirstOrDefault();
            return ans;
        }

        public void UpdateFathertId(int PrimaryUserAdmissionId, int StudentId, int fatherid)
        {
            var get = db.AdmissionTransactions.Where(q => q.PrimaryUserRegisterId == PrimaryUserAdmissionId && q.StudentRegisterId == StudentId).FirstOrDefault();
            if (get != null)
            {
                get.FatherRegisterId = fatherid;
                db.SaveChanges();
            }
        }
        public void UpdateMothertId(int PrimaryUserAdmissionId, int StudentId, int motherid)
        {
            var get = db.AdmissionTransactions.Where(q => q.PrimaryUserRegisterId == PrimaryUserAdmissionId && q.StudentRegisterId == StudentId).FirstOrDefault();
            if (get != null)
            {
                get.MotherRegisterId = motherid;
                db.SaveChanges();
            }
        }
        public void UpdateGuardiantId(int PrimaryUserAdmissionId, int StudentId, int guardianid)
        {
            var get = db.AdmissionTransactions.Where(q => q.PrimaryUserRegisterId == PrimaryUserAdmissionId && q.StudentRegisterId == StudentId).FirstOrDefault();
            if (get != null)
            {
                get.GuardianRegisterId = guardianid;
                db.SaveChanges();
            }
        }

        //one student primary user email is change ,but remain student primary user email can't change
        public void EditUpdateFatherPrimaryUserId(int PrimaryUserAdmissionId, int StudentId, int fatherid)
        {
            var get = db.AdmissionTransactions.Where(q =>  q.StudentRegisterId == StudentId).FirstOrDefault();
            if (get != null)
            {
                get.PrimaryUserRegisterId = PrimaryUserAdmissionId;
                get.FatherRegisterId = fatherid;
                db.SaveChanges();
            }
        }
        //one student primary user email is change ,but remain student primary user email can't change
        public void EditUpdateMotherPrimaryUserId(int PrimaryUserAdmissionId, int StudentId, int Motherid)
        {
            var get = db.AdmissionTransactions.Where(q => q.StudentRegisterId == StudentId).FirstOrDefault();
            if (get != null)
            {
                get.PrimaryUserRegisterId = PrimaryUserAdmissionId;
                get.MotherRegisterId = Motherid;
                db.SaveChanges();
            }
        }
        //one student primary user email is change ,but remain student primary user email can't change
        public void EditUpdateGuardianPrimaryUserId(int PrimaryUserAdmissionId, int StudentId, int Guardianid)
        {
            var get = db.AdmissionTransactions.Where(q => q.StudentRegisterId == StudentId).FirstOrDefault();
            if (get != null)
            {
                get.PrimaryUserRegisterId = PrimaryUserAdmissionId;
                get.GuardianRegisterId = Guardianid;
                db.SaveChanges();
            }
        }

        //in edit add student to exist user,so update exist user primaryuser id
        public void EditUpdatePrimaryUserIdfatherId(int PrimaryUserAdmissionId, int StudentId,int fatherid)
        {
            var get = db.AdmissionTransactions.Where(q => q.StudentRegisterId == StudentId).FirstOrDefault();
            if (get != null)
            {
                get.PrimaryUserRegisterId = PrimaryUserAdmissionId;
                get.FatherRegisterId = fatherid;
                db.SaveChanges();
            }
        }
        public void EditUpdatePrimaryUserIdMotherId(int PrimaryUserAdmissionId, int StudentId, int motherid)
        {
            var get = db.AdmissionTransactions.Where(q => q.StudentRegisterId == StudentId).FirstOrDefault();
            if (get != null)
            {
                get.PrimaryUserRegisterId = PrimaryUserAdmissionId;
                get.MotherRegisterId = motherid;
                db.SaveChanges();
            }
        }

        public void EditUpdatePrimaryUserIdGuardianId(int PrimaryUserAdmissionId, int StudentId, int guardianid)
        {
            var get = db.AdmissionTransactions.Where(q => q.StudentRegisterId == StudentId).FirstOrDefault();
            if (get != null)
            {
                get.PrimaryUserRegisterId = PrimaryUserAdmissionId;
                get.GuardianRegisterId = guardianid;
                db.SaveChanges();
            }
        }

        //Enrolement email is not exist in master table ,so inserted
        public void AddStudentPrimaryuser(int primaryuserid,int studentregisterid)
        {
            AdmissionTransaction trans = new AdmissionTransaction()
            {
                PrimaryUserRegisterId = primaryuserid,
                StudentRegisterId = studentregisterid
            };
            db.AdmissionTransactions.Add(trans);
            db.SaveChanges();
        }

        public AdmissionTransaction getPrimaryUserId(int studentRegId)
        {
            var row = (from a in db.AdmissionTransactions where a.StudentRegisterId == studentRegId select a).FirstOrDefault();
            return row;
        }

        public AdmissionTransaction checkStudentParentProfileExist(int primaryUserRegId, int StudentRegId)
        {
            var getRow = (from a in db.AdmissionTransactions where a.PrimaryUserRegisterId == primaryUserRegId && a.StudentRegisterId == StudentRegId select a).FirstOrDefault();
            return getRow;
        }

        public StudentParentProfile getStudentParentDetails(int primaryUserRegId, int StudentRegId)
        {
            var list = (from a in db.AdmissionTransactions
                        from b in db.Students
                        from c in db.PrimaryUserRegisters
                        from d in db.tblCountries
                        where a.StudentRegisterId == b.StudentRegisterId &&
                            a.PrimaryUserRegisterId == c.PrimaryUserRegisterId &&
                            a.PrimaryUserRegisterId == primaryUserRegId &&
                            a.StudentRegisterId == StudentRegId
                        //   &&  c.CompanyCountry == d.CountryId
                        select new StudentParentProfile
                        {
                            StudentId = b.StudentId,
                            StudentName = b.FirstName+" "+b.LastName,
                            StudentDob = SqlFunctions.DateName("day", b.DOB).Trim() + "-" + SqlFunctions.StringConvert((double)b.DOB.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", b.DOB),
                            StudentGender = b.Gender,
                            StudentEmail = b.Email,
                            StudentMobile = b.Contact,
                            StudentPlaceOfBirth = b.PlaceOfBirth,
                            StudentHeight = b.Height,
                            StudentWeight = b.Weights,
                            StudentAddress = b.LocalAddress1 + "," + b.LocalAddress2,
                            StudentCity = b.LocalCity,
                            StudentState = b.LocalState,
                            StudentPostalCode = b.LocalPostalCode,
                            StudentEmergencyContactPersonName = b.EmergencyContactPerson,
                            StudentEmergencyContactPersonPhone = b.EmergencyContactNumber,
                            PrimaryUser = c.PrimaryUser,
                            PrimaryUserCompanyAddress = c.CompanyAddress1 + " " + c.CompanyAddress2,
                            PrimaryUserCompanyCity = c.CompanyCity,
                            PrimaryUserCompanyState = c.CompanyState,
                            PrimaryUserCompanyContactNumber = c.CompanyContact,
                            PrimaryUserYearlyIncome = c.TotalIncome,
                            PrimaryUserCompanyCountry = c.CompanyCountry
                        }).FirstOrDefault();

            return list;
        }

        public StudentParentProfile getFatherDetail(int primaryUserRegId, int StudentRegId, int? FatherRegId)
        {
            var list = (from a in db.AdmissionTransactions
                        from b in db.Students
                        from c in db.PrimaryUserRegisters
                        from d in db.FatherRegisters
                        where a.StudentRegisterId == b.StudentRegisterId &&
                            a.PrimaryUserRegisterId == c.PrimaryUserRegisterId &&
                            a.FatherRegisterId == d.FatherRegisterId &&
                            a.PrimaryUserRegisterId == primaryUserRegId &&
                            a.StudentRegisterId == StudentRegId &&
                            a.FatherRegisterId == FatherRegId
                        select new StudentParentProfile
                        {
                            FatherName = d.FatherFirstName + " " + d.FatherLastName,
                            FatherDob = SqlFunctions.DateName("day", d.FatherDOB).Trim() + "-" + SqlFunctions.StringConvert((double)d.FatherDOB.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", d.FatherDOB),
                            FatherEmail = d.FatherEmail,
                            FatherMobile = d.FatherMobileNo,
                            FatherOccupation = d.FatherOccupation,
                            FatherQualification = d.FatherQualification
                        }).FirstOrDefault();

            return list;
        }

        public StudentParentProfile getMotherDetail(int primaryUserRegId, int StudentRegId, int? MotherRegId)
        {
            var list = (from a in db.AdmissionTransactions
                        from b in db.Students
                        from c in db.PrimaryUserRegisters
                        from e in db.MotherRegisters
                        where a.StudentRegisterId == b.StudentRegisterId &&
                            a.PrimaryUserRegisterId == c.PrimaryUserRegisterId &&
                            a.MotherRegisterId == e.MotherRegisterId &&
                            a.PrimaryUserRegisterId == primaryUserRegId &&
                            a.StudentRegisterId == StudentRegId
                        select new StudentParentProfile
                        {
                            MotherName = e.MotherFirstname + " " + e.MotherLastName,
                            MotherDob = SqlFunctions.DateName("day", e.MotherDOB).Trim() + "-" + SqlFunctions.StringConvert((double)e.MotherDOB.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", e.MotherDOB),
                            MotherEmail = e.MotherEmail,
                            MotherMobile = e.MotherMobileNo,
                            MotherOccupation = e.MotherOccupation,
                            MotherQualification = e.MotherQualification
                        }).FirstOrDefault();

            return list;
        }

        public StudentParentProfile getGuardianDetail(int primaryUserRegId, int StudentRegId, int? GuardianRegId)
        {
            var list = (from a in db.AdmissionTransactions
                        from b in db.Students
                        from c in db.PrimaryUserRegisters
                        from f in db.GuardianRegisters
                        where a.StudentRegisterId == b.StudentRegisterId &&
                            a.PrimaryUserRegisterId == c.PrimaryUserRegisterId &&
                            a.GuardianRegisterId == f.GuardianRegisterId &&
                            a.PrimaryUserRegisterId == primaryUserRegId &&
                            a.StudentRegisterId == StudentRegId
                        select new StudentParentProfile
                        {
                            GuardianName = f.GuardianFirstname + " " + f.GuardianLastName,
                            GuardianGender = f.GuardianGender,
                            GuardianRelationship = f.GuRelationshipToChild,
                            GuardianDob = SqlFunctions.DateName("day", f.GuardianDOB).Trim() + "-" + SqlFunctions.StringConvert((double)f.GuardianDOB.Value.Month).TrimStart() + "-" + SqlFunctions.DateName("year", f.GuardianDOB),
                            GuardianEmail = f.GuardianEmail,
                            GuardianMobile = f.GuardianMobileNo,
                            GuardianQualification = f.GuardianQualification,
                            GuardianOccupation = f.GuardianOccupation,
                            GuardianIncome = f.GuardianIncome
                        }).FirstOrDefault();

            return list;
        }

	 public List<StudentDetails> CheckStudentDetailsExit(int primaryuserId, string studentFirstname, string StuLastname, DateTime? StudentDob)
        {
            var record = (from a in db.AdmissionTransactions
                          from b in db.Students
                          where a.PrimaryUserRegisterId == primaryuserId && b.StudentRegisterId == a.StudentRegisterId && b.FirstName == studentFirstname && b.LastName == StuLastname && b.DOB == StudentDob
                          select new StudentDetails
                          {
                              StudentRegId = b.StudentRegisterId
                          }).ToList();
            return record;
        }
        public tblBloodGroup getBloodGroup(int? id)
        {
            var group = (from a in db.tblBloodGroups where a.BloodId == id  select a).FirstOrDefault();
            return group;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}