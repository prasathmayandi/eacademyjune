﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class ExamTimeScheduleServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public ExamTimeSchedule isNameExist(string scheduleName)
        {
            var getRow = (from a in db.ExamTimeSchedules where a.ScheduleName == scheduleName select a).FirstOrDefault();
            return getRow;
        }
        
        public ExamTimeSchedule isTimeExist(TimeSpan stime, TimeSpan etime)
        {
            var getRow = (from a in db.ExamTimeSchedules where a.StartTime == stime && a.EndTime == etime select a).FirstOrDefault();
            return getRow;
        }

        public void addExamTimeSchedule(string ScheduleName, TimeSpan stime, TimeSpan etime)
        {
            ExamTimeSchedule add = new ExamTimeSchedule()
            {
                ScheduleName = ScheduleName,
                StartTime = stime,
                EndTime = etime,
                Status = "true"       
            };
            db.ExamTimeSchedules.Add(add);
            db.SaveChanges();
        }

        public ExamTimeSchedule isScheduleExist ()
        {
            var row = (from a in db.ExamTimeSchedules select a).FirstOrDefault();
            return row;
        }

        public List<examTimeSchedule> ScheduleList()
        {
            var list = (from a in db.ExamTimeSchedules select a).ToList().Select(a => new examTimeSchedule
            {
                ExamScheduleId = QSCrypt.Encrypt(a.Id.ToString()),
                ScheduleName = a.ScheduleName,
                StartTime = a.StartTime.Value.ToString(@"hh\:mm"),
                EndTime = a.EndTime.Value.ToString(@"hh\:mm"),
                Status = a.Status
            }).ToList();
            return list;
        }

        public List<examTimeSchedule>getSchduleListBetweenTime(TimeSpan stime, TimeSpan etime)
        {
            var list = (from a in db.ExamTimeSchedules
                        where a.StartTime >= stime &&
                        a.EndTime <= etime &&
                        a.Status == "true"
                        select new examTimeSchedule
                        {
                            ScheduleId = a.Id
                        }).ToList();
            return list;
        }

        public ExamTimeSchedule getRow(int id)
        {
            var row = (from a in db.ExamTimeSchedules where a.Id == id select a).FirstOrDefault();
            return row;
        }

        public void EditExamTimeSchedule(int id, string status)
        {
            var getRow = (from a in db.ExamTimeSchedules where a.Id == id select a).FirstOrDefault();
            getRow.Status = status;
            db.SaveChanges();
        }

        public void delRow(int id)
        {
            var row = (from a in db.ExamTimeSchedules where a.Id == id select a).FirstOrDefault();
            db.ExamTimeSchedules.Remove(row);
            db.SaveChanges();
        }
        
        public IEnumerable<Object> ShowSchedule()
        {
            var AcademicYearList = (from a in db.ExamTimeSchedules
                                    where a.Status == "true"
                                    select new
                                    {
                                        ScheduleName = a.ScheduleName,
                                        ScheduleValue = a.Id
                                    }).ToList().AsEnumerable();
            return AcademicYearList;
        }

        public ExamTimeSchedule getScheduleTiming(int? ScheduleId)
        {
            var row = (from a in db.ExamTimeSchedules where a.Id == ScheduleId select a).FirstOrDefault();
            return row;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}