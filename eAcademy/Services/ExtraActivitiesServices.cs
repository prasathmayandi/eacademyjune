﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using eAcademy.Models;
using eAcademy.Helpers;
namespace eAcademy.Services
{
    public class ExtraActivitiesServices:IDisposable
    {

        EacademyEntities dc = new EacademyEntities();
        public ActivityList ExtraActivitiesByActivityID(int aid)
        {
            var ans = (from t1 in dc.ExtraActivities
                       where t1.ActivityId == aid
                       select new ActivityList { Activity = t1.Activity, Desc = t1.ActivityDescription, Status = t1.Status }).FirstOrDefault();
                        
            return ans;
        }

        public IEnumerable<object> getExtraActivity()
        {
            var ans = dc.ExtraActivities.Select(s => new { aid = s.ActivityId, atype = s.Activity }).ToList();
            return ans;
        }

        public List<ActivityList> getExtraActivityList()
        {
            var ans = (from t1 in dc.ExtraActivities
                       where t1.Status == true

                       select new ActivityList { ActivityId = t1.ActivityId, Activity = t1.Activity, Status = t1.Status }).ToList();
            return ans;
        }

        public ActivityList getActivityByActivityID(int s_aid)
        {
            var ans = (from a in dc.AssignActivityToStudents
                        from b in dc.AcademicYears
                        from c in dc.Classes
                        from d in dc.Sections
                        from e in dc.ExtraActivities
                        from f in dc.Students
                        where a.StudentActivityId == s_aid &&
                        a.AcademicYearId == b.AcademicYearId &&
                        a.ClassId == c.ClassId &&
                        a.SectionId == d.SectionId &&
                        a.ActivityId == e.ActivityId &&
                        a.StudentRegisterId == f.StudentRegisterId
                        select new  ActivityList { Activity = e.Activity, ActivityId = e.ActivityId, Student = f.FirstName+" "+f.LastName, StdRegId = f.StudentRegisterId, AcYear = b.AcademicYear1, AcYearId = a.AcademicYearId, Class = c.ClassType, ClassId = c.ClassId, Section = d.SectionName, SectionId = d.SectionId, Note = a.Note, Status=a.Status }).SingleOrDefault();

            return ans;
        
        }

        public List<ActivityList> getActivity()
        {
            var ans = (from t1 in dc.ExtraActivities
                       select new { aid = t1.ActivityId, activity = t1.Activity, desc = t1.ActivityDescription, status = t1.Status }).ToList()

                         .Select(q => new ActivityList { Aid = QSCrypt.Encrypt(q.aid.ToString()), ActivityId = q.aid, Activity = q.activity, Desc = q.desc, Status = q.status, Description=q.desc }).ToList();
            return ans;
        }

        public void addActivity(string activity, string activity_desc)
        {
            ExtraActivity eac = new ExtraActivity()
            {
                Activity = activity,
                ActivityDescription = activity_desc,
                Status=true
            };

            dc.ExtraActivities.Add(eac);
            dc.SaveChanges();
        }

        public bool checkAcitivity(string activity, string activity_desc)
        {
            //var ans = dc.ExtraActivities.Where(q => q.Activity.Equals(activity) && q.ActivityDescription.Equals(activity_desc)).ToList();
            var ans = dc.ExtraActivities.Where(q => q.Activity.Equals(activity)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool checkAcitivity(string activity, string activity_desc, string status)
        {
            int count;
            if (status == "Active")
            {

                var ans = dc.ExtraActivities.Where(q => q.Activity.Equals(activity) && q.ActivityDescription.Equals(activity_desc) && q.Status.Value.Equals(true)).ToList();
                count = ans.Count;
            }

            else
            {
                var ans = dc.ExtraActivities.Where(q => q.Activity.Equals(activity) && q.ActivityDescription.Equals(activity_desc) && q.Status.Value.Equals(false)).ToList();
                count = ans.Count;
            }

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool checkAcitivity(int aid, string activity, string activity_desc, string status)
        {
            int count;
            if (status == "Active")
            {

                var ans = (from t1 in dc.ExtraActivities
                           where t1.ActivityId != aid && t1.Activity == activity && t1.Status == true
                           select t1).ToList();

                count = ans.Count;
            }

            else
            {
                var ans = (from t1 in dc.ExtraActivities
                           where t1.ActivityId != aid && t1.Activity == activity && t1.Status == false
                           select t1).ToList();
                count = ans.Count;
            }

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void updateAcitivity(int aid, string activity, string activity_desc , string status)
        {
            var update = dc.ExtraActivities.Where(q => q.ActivityId.Equals(aid)).First();
            update.Activity = activity;
            update.ActivityDescription = activity_desc;
            if (status == "Active")
            {
                update.Status = true;
            }
            else
            {
                update.Status = false;
            }
            dc.SaveChanges();
           
        }

        public void deleteAcitivity(int AcitivityId)
        {
            var delete = dc.ExtraActivities.Where(q => q.ActivityId.Equals(AcitivityId)).First();
            dc.ExtraActivities.Remove(delete);
            dc.SaveChanges();
        }
        //jegadeesh

        public List<extracurricular> GetExtraCurricularIncharge(int yearid)
        {
            var ans = ((from a in dc.ExtraActivities
                        from b in dc.ActivityIncharges
                        from c in dc.TechEmployees
                        from e in dc.AcademicYears
                        where b.AcademicYearId == yearid && a.ActivityId == b.ActivityId && b.EmployeeId == c.EmployeeId && b.AcademicYearId == e.AcademicYearId
                        select new extracurricular
                        {
                            ActivityName = a.Activity,
                            ActivityInchargeName = c.EmployeeName,
                            InchargeId = c.EmployeeId,
                            ContactNumber=c.Mobile,
                            usertype = "t"
                        })
                              .Union(from a in dc.ExtraActivities
                                     from b in dc.ActivityIncharges
                                     from c in dc.OtherEmployees
                                     from e in dc.AcademicYears
                                     where b.AcademicYearId == yearid && a.ActivityId == b.ActivityId && b.EmployeeId == c.EmployeeId && b.AcademicYearId == e.AcademicYearId
                                     select new extracurricular
                                     {
                                        
                                         ActivityName = a.Activity,
                                         ActivityInchargeName = c.EmployeeName,
                                         InchargeId = c.EmployeeId,
                                         ContactNumber=c.Contact,
                                         usertype = "o"
                                     })).ToList();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

       
    }
}