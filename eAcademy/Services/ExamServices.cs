﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using eAcademy.ViewModel;

namespace eAcademy.Services
{
    public class ExamServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public IEnumerable<Object> ShowExams()
        {
            var ExamList = (from a in dc.Exams
                            where a.Status==true
                            select new
                            {
                                ExamName = a.ExamName,
                                ExamId = a.ExamId
                            }).ToList().AsEnumerable();
            return ExamList;
        }

        public ExamList getExamByExamID(int examid)
        {
            var ans = (from t1 in dc.Exams
                       where t1.ExamId == examid
                       select new ExamList { Exam = t1.ExamName,status=t1.Status }).First();                         
            return ans;
        }

        public IEnumerable<object> getExamList()
        {
            var ans = dc.Exams.Select(s => new { exam = s.ExamName, examId = s.ExamId }).ToList();
            return ans;
        }

        public bool checkExam(int examid, string ename)
        {
            var ans = dc.Exams.Where(q =>q.ExamId !=examid && q.ExamName.Equals(ename)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool checkExam1(string ename)
        {
            var ans = dc.Exams.Where(q => q.ExamName.Equals(ename)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<ExamList> getExam()
        {
            var ans = (from t1 in dc.Exams
                       select new { examid = t1.ExamId, exam = t1.ExamName, status = t1.Status }).ToList().Select(q => new ExamList { eid = QSCrypt.Encrypt(q.examid.ToString()), Exam = q.exam, ExamId = q.examid, status=q.status }).ToList(); 
            return ans;
        }


        public void addExam(string examName)
        {
            Exam ex = new Exam()
            {
                ExamName = examName,
                Status=true
            };
            dc.Exams.Add(ex);
            dc.SaveChanges();
        }

        public void updateExam(int examid, string ename, string status)
        {
            var update = dc.Exams.Where(q => q.ExamId.Equals(examid)).First();
            update.ExamName = ename;
            if (status == "Active")
            {
                update.Status = true;
            }
            else
            {
                update.Status = false;
            }
            dc.SaveChanges();
        }

        internal List<ExamList> getExamsAsTableColumns()
        {
            var ans = dc.Exams.Select(q => new ExamList{ ExamId=q.ExamId, Exam=q.ExamName}).ToList();
            return ans;

        }

        internal List<StudentsResults> getOverAllResultbySection(int AcyearStdList, int ClassStdList, int SectionStdList1, int[] Examids)
        {
            List<StudentsResults> StudentsResultsList = new List<StudentsResults>();


            for (int i = 0; i < Examids.Length; i++)
            {
                int eid = Examids[i];

                var ans = (from t1 in dc.MarkTotals
                           from t2 in dc.Exams
                           from t3 in dc.Students
                           from t4 in dc.AssignClassToStudents

                           where
                           t1.ExamId == t2.ExamId &&
                           t1.StudentRegisterId == t3.StudentRegisterId &&
                           t1.AcademicYearId == t4.AcademicYearId && t1.ClassId == t4.ClassId && t1.SectionId == t4.SectionId && t1.StudentRegisterId == t4.StudentRegisterId &&
                           t3.StudentRegisterId == t4.StudentRegisterId &&
                           t1.AcademicYearId == AcyearStdList && t1.ClassId == ClassStdList && t1.SectionId == SectionStdList1 && t1.ExamId==eid &&
                           t3.StudentStatus == true

                           select new StudentsResults { ExamId = t2.ExamId, StudentRegId = t3.StudentRegisterId, Result = t1.Result }).ToList();

                StudentsResultsList.AddRange(ans);
            }

            return StudentsResultsList;
        }
        //jegadeesh

        public IEnumerable GetExamName()
        {
            var ans = dc.Exams.Select(q => q);
            return ans;

        }
        public IEnumerable GetParticularExamName(int examid)
        {
            var ans = dc.Exams.Where(q => q.ExamId.Equals(examid)).FirstOrDefault().ExamName;
            return ans;

        }
        public IEnumerable<Exam> GetExamNameList()
        {
            var ans = dc.Exams.Select(q => q);
            return ans;

        }


        //Shobana
         internal List<SubjectList> getSubjectsAsTableColumns()
        {
            var ans = dc.Subjects.Select(q => new SubjectList { SubjectId = q.SubjectId, Subject = q.SubjectName }).ToList();
            return ans;
        }
         internal List<StudentsResults> getStudentwithResult(int acid, int cid, int sec_id)
         {
             var ans = (from t1 in dc.AssignClassToStudents
                        from t2 in dc.Students
                        from t3 in dc.StudentRollNumbers
                        where t1.StudentRegisterId == t2.StudentRegisterId &&
                        t1.StudentRegisterId == t3.StudentRegisterId &&
                        t1.AcademicYearId == acid && t1.ClassId == cid && t1.SectionId == sec_id &&
                        t3.AcademicYearId == acid && t3.ClassId == cid && t3.SectionId == sec_id &&
                        t2.StudentStatus == true && t3.Status == true //&& t1.ClassAssignedForNextAcademicYear != "Yes"
                        select new StudentsResults { StudentRegId = t2.StudentRegisterId,
                            StudentName = t2.FirstName + " " + t2.LastName, 
                            StudentRollNumber = t3.RollNumber,
                            StudentId = t2.StudentId ,
                            FinalResult=t1.Result
                        }).OrderBy(q => q.StudentRegId).ToList();

             return ans;
         }



         internal List<StudentsResults> getGradeResultbySection(int AcyearStdList, int ClassStdList, int SectionStdList1, int Exam, int[] Subject)
         {
             List<StudentsResults> StudentsGradeList = new List<StudentsResults>();


             for (int i = 0; i < Subject.Length; i++)
             {
                 int eid = Subject[i];

                 var ans = (from t1 in dc.StudentGrades
                            from t2 in dc.Subjects
                            from t3 in dc.Students
                            from t4 in dc.AssignClassToStudents
                            from t5 in dc.Exams 
                            from t6 in dc.StudentRollNumbers
                            where
                            t1.AcademicYearId == AcyearStdList && t1.ClassId == ClassStdList && t1.SectionId == SectionStdList1 && t1.SubjectId == eid && t1.ExamId == Exam &&
                            t1.SubjectId == t2.SubjectId &&
                            t1.StudentRegisterId == t3.StudentRegisterId &&
                            t3.StudentRegisterId ==t4.StudentRegisterId &&
                            t1.ExamId == t5.ExamId &&
                            t1.StudentRegisterId==t6.StudentRegisterId
                            select new StudentsResults
                            {
                                StudentRegId=t3.StudentRegisterId,
                                Subject=t2.SubjectId,
                                Result=t1.Grade
                            }).ToList();


                            //t1.SubjectId == t2.SubjectId &&
                            //t1.StudentRegisterId == t3.StudentRegisterId &&
                            //t1.AcademicYearId == t4.AcademicYearId && t1.ClassId == t4.ClassId && t1.SectionId == t4.SectionId && t1.StudentRegisterId == t4.StudentRegisterId &&
                            //t3.StudentRegisterId == t4.StudentRegisterId &&
                            //t1.AcademicYearId == AcyearStdList && t1.ClassId == ClassStdList && t1.SectionId == SectionStdList1 && t1.SubjectId == eid && t1.ExamId == Exam
                            //select new StudentsResults
                            //{
                            //    StudentRegId = t3.StudentRegisterId,
                            //    Subject = t2.SubjectId,
                            //    Result = t1.Grade
                            //}).ToList();

                 StudentsGradeList.AddRange(ans);
             }
                 return StudentsGradeList;
         }



        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}