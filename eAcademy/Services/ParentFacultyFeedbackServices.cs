﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class ParentFacultyFeedbackServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public void addParentFacultyFeedback(int ParentRegId, int StudentRegId, int yearId, int classId, int secId, int subId, int FacultyId, string Feedback)
        {
            DateTime date = DateTimeByZone.getCurrentDate();
            ParentFacultyFeedback add = new ParentFacultyFeedback()
            {
                AcademicYearId = yearId,
                ClassId = classId,
                SectionId = secId,
                SubjectId = subId,
                EmployeeRegisterId = FacultyId,
                StudentRegisterId = StudentRegId,
                ParentRegisterId = ParentRegId,
                DateOfFeedback = date,
                Feedback = Feedback,
            };
            db.ParentFacultyFeedbacks.Add(add);
            db.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}