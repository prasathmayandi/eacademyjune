﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class SchoolSettingsServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public List<SchoolSetting> getSchoolSettings()
        {
            var schoolsetting = dc.SchoolSettings.Select(q => q).ToList();
            return schoolsetting;
        }

        public SchoolSetting SchoolSettings()
        {
            var schoolsetting = dc.SchoolSettings.Select(q => q).FirstOrDefault();
            return schoolsetting;
        }

        internal void addSchoolSetting(string schoolName, string schoolLogo, string Adress1, string Adress2, string City, string State, string Zip, string Country, string Phone1, string Phone2, long Mobile1, long Mobile2, string Email, string Fax, string Website, string stdIdFormat, string staffIdFormat, string non_staffIdFormat, int Currency, int dayOrder, string markFormat, Decimal? tax, Decimal? VAT, string formFormat, string schoolCode, string parentIdFormat,bool transport,bool hostel,bool Email1,bool Sms,bool App)
        {

            SchoolSetting setting = new SchoolSetting()
            {
                SchoolName = schoolName,
                SchoolLogo = schoolLogo,
                AddressLine1 = Adress1,
                AddressLine2 = Adress2,
                City = City,
                State = State,
                ZIP = Zip,
                Country = Country,
                Phone1 = Phone1,
                Phone2 = Phone2,
                Mobile1 = Mobile1,
                Mobile2 = Mobile2,
                Fax = Fax,
                Email = Email,
                Website = Website,
                CurrencyType = Currency,
                DayOrder = dayOrder,
                MarkFormat = markFormat,
                StudentIdFormat = stdIdFormat,
                StaffIDFormat = staffIdFormat,
                Non_StaffIDFormat = non_staffIdFormat,
                ServicesTax=tax,
                VAT=VAT,
                FormFormat=formFormat,
                SchoolCode=schoolCode,
                ParentIDFormat=parentIdFormat,
                TransportRequired=transport,
                HostelRequired=hostel,
                EmailRequired=Email1,
                SmsRequired=Sms,
                AppRequired=App
            
            };

            dc.SchoolSettings.Add(setting);
            dc.SaveChanges();
            if (markFormat == "Mark")
            {
                //var update = dc.Features.Where(q => q.FeatureId.Equals(8)).FirstOrDefault();
                //update.FeatureStatus = true;
                //dc.SaveChanges();
                var update1 = dc.Features.Where(q => q.FeatureId.Equals(9)).FirstOrDefault();
                update1.FeatureStatus = false;
                dc.SaveChanges();
                var update2 = dc.Features.Where(q => q.FeatureId.Equals(31)).FirstOrDefault();
                update2.FeatureStatus = true;
                dc.SaveChanges();
                var update3 = dc.Features.Where(q => q.FeatureId.Equals(32)).FirstOrDefault();
                update3.FeatureStatus = false;
                dc.SaveChanges();
                var updateMark = dc.Menus.Where(q => q.MenuId.Equals(98)).FirstOrDefault();
                updateMark.MenuStatus = true;
                dc.SaveChanges();
                var updateGrade = dc.Menus.Where(q => q.MenuId.Equals(100)).FirstOrDefault();
                updateGrade.MenuStatus = false;
                dc.SaveChanges();
                var updateMark1 = dc.Menus.Where(q => q.MenuId.Equals(130)).FirstOrDefault();
                updateMark1.MenuStatus = true;
                dc.SaveChanges();
                var updateGrade1 = dc.Menus.Where(q => q.MenuId.Equals(133)).FirstOrDefault();
                updateGrade1.MenuStatus = false;
                dc.SaveChanges();

               // modified

                var AcademicGradeResult = dc.Menus.Where(q => q.MenuId.Equals(176)).FirstOrDefault();
                AcademicGradeResult.MenuStatus = false;
                dc.SaveChanges();
                var AcademicMarkResult = dc.Menus.Where(q => q.MenuId.Equals(66)).FirstOrDefault();
                AcademicMarkResult.MenuStatus = true;
                dc.SaveChanges();
                var GradeResult = dc.Menus.Where(q => q.MenuId.Equals(177)).FirstOrDefault();
                GradeResult.MenuStatus = false;
                dc.SaveChanges();
                var MarkResult = dc.Menus.Where(q => q.MenuId.Equals(158)).FirstOrDefault();
                MarkResult.MenuStatus = true;
                dc.SaveChanges();
                var Marklist = dc.Menus.Where(q => q.MenuId.Equals(148)).FirstOrDefault();
                Marklist.MenuStatus = true;
                dc.SaveChanges();
                var RankList = dc.Menus.Where(q => q.MenuId.Equals(145)).FirstOrDefault();
                RankList.MenuStatus = true;
                dc.SaveChanges();
            }
            else
            {
                //var update = dc.Features.Where(q => q.FeatureId.Equals(8)).FirstOrDefault();
                //update.FeatureStatus = false;
                //dc.SaveChanges();
                var update1 = dc.Features.Where(q => q.FeatureId.Equals(9)).FirstOrDefault();
                update1.FeatureStatus = true;
                dc.SaveChanges();
                var update2 = dc.Features.Where(q => q.FeatureId.Equals(31)).FirstOrDefault();
                update2.FeatureStatus = false;
                dc.SaveChanges();
                var update3 = dc.Features.Where(q => q.FeatureId.Equals(32)).FirstOrDefault();
                update3.FeatureStatus = true;
                dc.SaveChanges();
                var updateMark = dc.Menus.Where(q => q.MenuId.Equals(98)).FirstOrDefault();
                updateMark.MenuStatus = false;
                dc.SaveChanges();
                var updateGrade = dc.Menus.Where(q => q.MenuId.Equals(100)).FirstOrDefault();
                updateGrade.MenuStatus = true;
                dc.SaveChanges();
                var updateMark1 = dc.Menus.Where(q => q.MenuId.Equals(130)).FirstOrDefault();
                updateMark1.MenuStatus = false;
                dc.SaveChanges();
                var updateGrade1 = dc.Menus.Where(q => q.MenuId.Equals(133)).FirstOrDefault();
                updateGrade1.MenuStatus = true;
                dc.SaveChanges();

                // modified

                var AcademicGradeResult = dc.Menus.Where(q => q.MenuId.Equals(176)).FirstOrDefault();
                AcademicGradeResult.MenuStatus = true;
                dc.SaveChanges();
                var AcademicMarkResult = dc.Menus.Where(q => q.MenuId.Equals(66)).FirstOrDefault();
                AcademicMarkResult.MenuStatus = false;
                dc.SaveChanges();
                var GradeResult = dc.Menus.Where(q => q.MenuId.Equals(177)).FirstOrDefault();
                GradeResult.MenuStatus = true;
                dc.SaveChanges();
                var MarkResult = dc.Menus.Where(q => q.MenuId.Equals(158)).FirstOrDefault();
                MarkResult.MenuStatus = false;
                dc.SaveChanges();
                var Marklist = dc.Menus.Where(q => q.MenuId.Equals(148)).FirstOrDefault();
                Marklist.MenuStatus = false;
                dc.SaveChanges();
                var RankList = dc.Menus.Where(q => q.MenuId.Equals(145)).FirstOrDefault();
                RankList.MenuStatus = false;
                dc.SaveChanges();
            }



            if (formFormat == "Single Form")
            {
                var updateFeature = dc.Features.Where(q => q.FeatureId.Equals(11)).FirstOrDefault();
                updateFeature.FeatureLink = "/StudentEnrollment/SingleFormEnrollment";
                dc.SaveChanges();
                var updateMenu = dc.Menus.Where(q => q.MenuId.Equals(34)).FirstOrDefault();
                updateMenu.MenuStatus = true;
                dc.SaveChanges();
                var updateMenu1 = dc.Menus.Where(q => q.MenuId.Equals(35)).FirstOrDefault();
                updateMenu1.MenuStatus = false;
                dc.SaveChanges();
            }
            else
            {
                var updateFeature = dc.Features.Where(q => q.FeatureId.Equals(11)).FirstOrDefault();
                updateFeature.FeatureLink = "/StudentEnrollment/StepFormEnrollment";
                dc.SaveChanges();

                var updateMenu = dc.Menus.Where(q => q.MenuId.Equals(35)).FirstOrDefault();
                updateMenu.MenuStatus = false;
                dc.SaveChanges();

                var updateMenu1 = dc.Menus.Where(q => q.MenuId.Equals(34)).FirstOrDefault();
                updateMenu1.MenuStatus = true;
                dc.SaveChanges();
            }
        }

        internal void UpdateSchoolSetting(int id, string schoolName, string schoolLogo, string Adress1, string Adress2, string City, string State, string Zip, string Country, string Phone1, string Phone2, long Mobile1, long Mobile2, string Email, string Fax, string Website, string stdIdFormat, string staffIdFormat, string non_staffIdFormat, int Currency, int dayOrder, string markFormat, Decimal? tax, Decimal? VAT, string formFormat, string schoolCode, string parentIdFormat, bool transport, bool hostel,bool Email1,bool Sms,bool App)
        {
            var update = dc.SchoolSettings.Where(q => q.SettingsId.Equals(id)).FirstOrDefault();
            update.SchoolName = schoolName;
            update.SchoolLogo = schoolLogo;
            update.AddressLine1 = Adress1;
            update.AddressLine2 = Adress2;
            update.City = City;
            update.State = State;
            update.ZIP = Zip;
            update.Country = Country;
            update.Phone1 = Phone1;
            update.Phone2 = Phone2;
            update.Mobile1 = Mobile1;
            update.Mobile2 = Mobile2;
            update.Fax = Fax;
            update.Email = Email;
            update.Website = Website;
            update.CurrencyType = Currency;
            update.DayOrder = dayOrder;
            update.MarkFormat = markFormat;
            update.StudentIdFormat = stdIdFormat;
            update.StaffIDFormat = staffIdFormat;
            update.Non_StaffIDFormat = non_staffIdFormat;
            update.ServicesTax =tax;
            update.VAT = VAT;
            update.FormFormat = formFormat;
            update.SchoolCode = schoolCode;
            update.ParentIDFormat = parentIdFormat;
            update.TransportRequired = transport;
            update.HostelRequired = hostel;
            update.EmailRequired = Email1;
            update.SmsRequired = Sms;
            update.AppRequired = App;
            dc.SaveChanges();

            if (markFormat == "Mark")
            {
                var update0 = dc.Features.Where(q => q.FeatureId.Equals(8)).FirstOrDefault();
                update0.FeatureStatus = true;
                dc.SaveChanges();
                var update1 = dc.Features.Where(q => q.FeatureId.Equals(9)).FirstOrDefault();
                update1.FeatureStatus = false;
                dc.SaveChanges();
                var update2 = dc.Features.Where(q => q.FeatureId.Equals(31)).FirstOrDefault();
                update2.FeatureStatus = true;
                dc.SaveChanges();
                var update3 = dc.Features.Where(q => q.FeatureId.Equals(32)).FirstOrDefault();
                update3.FeatureStatus = false;
                dc.SaveChanges();
                var updateMark = dc.Menus.Where(q => q.MenuId.Equals(98)).FirstOrDefault();
                updateMark.MenuStatus = true;
                dc.SaveChanges();
                var updateGrade = dc.Menus.Where(q => q.MenuId.Equals(100)).FirstOrDefault();
                updateGrade.MenuStatus = false;
                dc.SaveChanges();
                var updateMark1 = dc.Menus.Where(q => q.MenuId.Equals(130)).FirstOrDefault();
                updateMark1.MenuStatus = true;
                dc.SaveChanges();
                var updateGrade1 = dc.Menus.Where(q => q.MenuId.Equals(133)).FirstOrDefault();
                updateGrade1.MenuStatus = false;
                dc.SaveChanges();

                // modified

                var AcademicGradeResult = dc.Menus.Where(q => q.MenuId.Equals(176)).FirstOrDefault();
                AcademicGradeResult.MenuStatus = false;
                dc.SaveChanges();
                var AcademicMarkResult = dc.Menus.Where(q => q.MenuId.Equals(66)).FirstOrDefault();
                AcademicMarkResult.MenuStatus = true;
                dc.SaveChanges();
                var GradeResult = dc.Menus.Where(q => q.MenuId.Equals(177)).FirstOrDefault();
                GradeResult.MenuStatus = false;
                dc.SaveChanges();
                var MarkResult = dc.Menus.Where(q => q.MenuId.Equals(158)).FirstOrDefault();
                MarkResult.MenuStatus = true;
                dc.SaveChanges();
                var Marklist = dc.Menus.Where(q => q.MenuId.Equals(148)).FirstOrDefault();
                Marklist.MenuStatus = true;
                dc.SaveChanges();
                var RankList = dc.Menus.Where(q => q.MenuId.Equals(145)).FirstOrDefault();
                RankList.MenuStatus = true;
                dc.SaveChanges();
            }
            else
            {
                var update0 = dc.Features.Where(q => q.FeatureId.Equals(8)).FirstOrDefault();
                update0.FeatureStatus = true;
                dc.SaveChanges();
                var update1 = dc.Features.Where(q => q.FeatureId.Equals(9)).FirstOrDefault();
                update1.FeatureStatus = true;
                dc.SaveChanges();
                var update2 = dc.Features.Where(q => q.FeatureId.Equals(31)).FirstOrDefault();
                update2.FeatureStatus = false;
                dc.SaveChanges();
                var update3 = dc.Features.Where(q => q.FeatureId.Equals(32)).FirstOrDefault();
                update3.FeatureStatus = true;
                dc.SaveChanges();
                var updateMark = dc.Menus.Where(q => q.MenuId.Equals(98)).FirstOrDefault();
                updateMark.MenuStatus = false;
                dc.SaveChanges();
                var updateGrade = dc.Menus.Where(q => q.MenuId.Equals(100)).FirstOrDefault();
                updateGrade.MenuStatus = true;
                dc.SaveChanges();
                var updateMark1 = dc.Menus.Where(q => q.MenuId.Equals(130)).FirstOrDefault();
                updateMark1.MenuStatus = false;
                dc.SaveChanges();
                var updateGrade1 = dc.Menus.Where(q => q.MenuId.Equals(133)).FirstOrDefault();
                updateGrade1.MenuStatus = true;
                dc.SaveChanges();


                // modified

                var AcademicGradeResult = dc.Menus.Where(q => q.MenuId.Equals(176)).FirstOrDefault();
                AcademicGradeResult.MenuStatus = true;
                dc.SaveChanges();
                var AcademicMarkResult = dc.Menus.Where(q => q.MenuId.Equals(66)).FirstOrDefault();
                AcademicMarkResult.MenuStatus = false;
                dc.SaveChanges();
                var GradeResult = dc.Menus.Where(q => q.MenuId.Equals(177)).FirstOrDefault();
                GradeResult.MenuStatus = true;
                dc.SaveChanges();
                var MarkResult = dc.Menus.Where(q => q.MenuId.Equals(158)).FirstOrDefault();
                MarkResult.MenuStatus = false;
                dc.SaveChanges();
                var Marklist = dc.Menus.Where(q => q.MenuId.Equals(148)).FirstOrDefault();
                Marklist.MenuStatus = false;
                dc.SaveChanges();
                var RankList = dc.Menus.Where(q => q.MenuId.Equals(145)).FirstOrDefault();
                RankList.MenuStatus = false;
                dc.SaveChanges();
            }



            if (formFormat == "Single Form")
            {
                var updateFeature = dc.Features.Where(q => q.FeatureId.Equals(11)).FirstOrDefault();
                updateFeature.FeatureLink = "/StudentEnrollment/SingleFormEnrollment";
                dc.SaveChanges();
                var updateMenu = dc.Menus.Where(q => q.MenuId.Equals(34)).FirstOrDefault();
                updateMenu.MenuStatus = true;
                dc.SaveChanges();
                var updateMenu1 = dc.Menus.Where(q => q.MenuId.Equals(35)).FirstOrDefault();
                updateMenu1.MenuStatus = false;
                dc.SaveChanges();
            }
            else
            {
                var updateFeature = dc.Features.Where(q => q.FeatureId.Equals(11)).FirstOrDefault();
                updateFeature.FeatureLink = "/StudentEnrollment/StepFormEnrollment";
                dc.SaveChanges();

                var updateMenu = dc.Menus.Where(q => q.MenuId.Equals(34)).FirstOrDefault();
                updateMenu.MenuStatus = false;
                dc.SaveChanges();

                var updateMenu1 = dc.Menus.Where(q => q.MenuId.Equals(35)).FirstOrDefault();
                updateMenu1.MenuStatus = true;
                dc.SaveChanges();
            }
        }

        //jegadeesh

        public SchoolSetting GetSchoolDayOrder()
        {
            var ans = dc.SchoolSettings.Select(q => q).FirstOrDefault();
            return ans;
        }
        public SchoolSetting GetSchoolPeriod()
        {
            var ans = dc.SchoolSettings.Select(q => q).FirstOrDefault();
           
            return ans;
        }
        public SchoolSetting GetJsonSchoolnameLogo()
        {
            var ans = dc.SchoolSettings.Select(q => q).FirstOrDefault();
            return ans;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}