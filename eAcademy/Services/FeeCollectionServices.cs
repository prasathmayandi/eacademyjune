﻿using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using eAcademy.DataModel;
using System.Collections;

namespace eAcademy.Services
{
    public class FeeCollectionServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public FeeCollection checkDataExist(int StudentRegId, int year, int classId)
        {
            var getRow = (from a in db.FeeCollections where a.StudentRegisterId == StudentRegId && a.AcademicYearId == year && a.ClassId == classId select a).FirstOrDefault();
            return getRow;
        }

        public void updateSectionWithRegId(int year, int classId, int studentRegisterId, int sectionId)
        {
            var getRow = (from a in db.FeeCollections where a.StudentRegisterId == studentRegisterId && a.AcademicYearId == year && a.ClassId == classId select a).FirstOrDefault();
            getRow.SectionId = sectionId;
            db.SaveChanges();
        }

        public List<eAcademy.ViewModel.FeeDetails> getAnnualPaidFees(int yearId, int classId, int secId, int StudentRegId)
        {
            var list = (from a in db.FeeCollections
                        from b in db.FeeCategories
                        from c in db.Fees
                        from d in db.FeeCollectionCategories
                        where a.AcademicYearId == yearId &&
                         a.ClassId == classId &&
                         a.SectionId == secId &&
                        a.StudentRegisterId == StudentRegId &&
                        a.FeeCollectionId == d.FeeCollectionId &&
                        d.FeeCategoryId == b.FeeCategoryId
                        select new eAcademy.ViewModel.FeeDetails
                        {
                            FeeName = b.FeeCategoryName,
                            FeeAmount = d.TotalAmount,
                            PaidDate = a.CollectionDate
                        }).Distinct().ToList();
            return list;
        }

        public List<eAcademy.ViewModel.FeeDetails> getAnnualPaidFees1(int yearId, int StudentRegId)
        {
            var list = (from a in db.FeeCollections
                        from b in db.Classes
                        from c in db.Sections
                        from d in db.FeeCategories
                        from e in db.Fees
                        from f in db.FeeCollectionCategories
                        where a.AcademicYearId == yearId &&
                        a.ClassId == b.ClassId &&
                        a.SectionId == c.SectionId &&
                        a.StudentRegisterId == StudentRegId &&
                        a.FeeCollectionId == f.FeeCollectionId &&
                        f.FeeCategoryId == d.FeeCategoryId
                        select new
                        {
                            FeeName = d.FeeCategoryName,
                            FeeAmount = f.TotalAmount,
                            PaidDate = a.CollectionDate
                        }).Distinct().ToList().Select(a => new eAcademy.ViewModel.FeeDetails
                        {
                            FeeName = a.FeeName,
                            FeeAmount = Math.Round(a.FeeAmount.Value, 2, MidpointRounding.AwayFromZero),
                            PaidDate1 = a.PaidDate.ToString("dd MMM, yyyy")
                        }).ToList();
            return list;
        }

        public List<eAcademy.ViewModel.FeeDetails> getParentAnnualPaidFees(int yearId, int classId)
        {
            var getTermFeeStructure = (from a in db.TermFees
                                       from b in db.Terms
                                       from c in db.FeeCategories
                                       where a.AcademicYearId == yearId &&
                                          a.ClassId == classId &&
                                          a.TermId == b.TermId &&
                                          a.FeeCategoryId == c.FeeCategoryId
                                       select new eAcademy.ViewModel.FeeDetails
                                       {
                                           TermId = a.TermId,
                                           TermName = b.TermName,
                                           TermFeeName = c.FeeCategoryName,
                                           TermFeeAmount = a.Amount,
                                           TermFeeLastDate = a.LastDate
                                       }).OrderBy(a => a.TermId).ToList();
            return getTermFeeStructure;
        }

        public List<FeeCollection> getFeeCollection()
        {
            var result = db.FeeCollections.Select(q => q).ToList();
            return result;
        }

        public List<FeeDetails> getFeeCollection(int acid, int cid, int sec_id, int fid, int stu_id)
        {
            var ans = db.FeeCollections.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) && q.StudentRegisterId.Value.Equals(stu_id)).Select(s => new FeeDetails { Fee = s.FeeAmount, Fine = s.Fine, AmountCollected = s.AmountCollected, Remark = s.Remark }).ToList();
            return ans;
        }

        public int getFeeCollectionOrderByFeeCollectionID()
        {
            var ans = db.FeeCollections.OrderByDescending(q => q.FeeCollectionId).Select(s => s.FeeCollectionId).First();
            return ans;
        }

        public FeeCollection checkDataExist_ChangeSection(int studentRegisterId, int year, int classId)
        {
            var getRow = (from a in db.FeeCollections where a.StudentRegisterId == studentRegisterId && a.AcademicYearId == year && a.ClassId == classId select a).FirstOrDefault();
            return getRow;
        }

        public int collectFeeAmount(DateTime FCDate, int acid, int cid, int sec_id, int stu_id, decimal feeAmount, decimal fine, decimal amountCollected, decimal discount, decimal VAT, string mode, string bankName, string chequeNumber, string DDNumber, string remark, int empregid)
        {
            FeeCollection collection = new FeeCollection()
            {
                CollectionDate = FCDate,
                AcademicYearId = acid,
                ClassId = cid,
                SectionId = sec_id,
                StudentRegisterId = stu_id,
                FeeAmount = feeAmount,
                Fine = fine,
                AmountCollected = amountCollected,
                PaymentMode = mode,
                BankName = bankName,
                ChequeNumber = chequeNumber,
                DDNumber = DDNumber,
                Remark = remark,
                Discount = discount,
                VAT = VAT,
                EmployeeRegisterId = empregid
            };

            db.FeeCollections.Add(collection);
            db.SaveChanges();

            return collection.FeeCollectionId;
        }

        internal void collectFeeAmount(DateTime FCDate, int acid, int cid, int sec_id, int stu_id, decimal feeAmount, decimal fine, decimal amountCollected, decimal discount, decimal VAT, string mode, string bankName, string chequeNumber, string DDNumber, string remark, int FeeCatId, int PaymentTypeId, int empregid, int collectionid)
        {
            if (PaymentTypeId == 0)
            {
                var list = (from t1 in db.FeeCategories
                            from t2 in db.Fees
                            where t1.FeeCategoryId == t2.FeeCategoryId && t2.AcademicYearId == acid && t2.ClassId == cid && t2.FeeCategoryId == FeeCatId
                            select new { amount = t2.Amount, tax = t2.Tax, total = t2.Total }).SingleOrDefault();


                //var ans = db.FeeCollections.
                //    Where(q => q.CollectionDate.Equals(FCDate) && q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) &&
                //        q.AmountCollected.Value.Equals(amountCollected) && q.FeeAmount.Value.Equals(feeAmount) && q.PaymentMode.Equals(mode)).
                //    Select(s => new { feecollectionId = s.FeeCollectionId }).SingleOrDefault();

                FeeCollectionCategory add = new FeeCollectionCategory()
                {
                    FeeCollectionId = collectionid,
                    FeeCategoryId = FeeCatId,
                    PaymentTypeId = PaymentTypeId,
                    Amount = list.amount.Value,
                    ServicesTax = list.tax.Value,
                    TotalAmount = list.total.Value,
                    AcademicYearId = acid,
                    ClassId = cid,
                    SectionId = sec_id,
                    StudentRegId = stu_id,
                    EmployeeRegisterId = empregid
                };

                db.FeeCollectionCategories.Add(add);
                db.SaveChanges();
            }

            else
            {
                var list = (from t1 in db.FeeCategories
                            from t2 in db.Fees
                            from t3 in db.TermFees
                            where t1.FeeCategoryId == t2.FeeCategoryId && t1.FeeCategoryId == t3.FeeCategoryId && t3.AcademicYearId == acid && t3.ClassId == cid && t3.FeeCategoryId == FeeCatId && t2.FeeId == t3.FeeId && t3.TermId == PaymentTypeId
                            select new { amount = t3.Amount, tax = t3.Tax, total = t3.Total }).SingleOrDefault();
                //var ans = db.FeeCollections.
                //    Where(q => q.CollectionDate.Equals(FCDate) && q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.SectionId.Value.Equals(sec_id) &&
                //        q.AmountCollected.Value.Equals(amountCollected) && q.FeeAmount.Value.Equals(feeAmount) && q.PaymentMode.Equals(mode)).
                //    Select(s => new { feecollectionId = s.FeeCollectionId }).SingleOrDefault();

                FeeCollectionCategory add = new FeeCollectionCategory()
                {
                    FeeCollectionId = collectionid,
                    FeeCategoryId = FeeCatId,
                    PaymentTypeId = PaymentTypeId,
                    Amount = list.amount.Value,
                    ServicesTax = list.tax.Value,
                    TotalAmount = list.total.Value,
                    AcademicYearId = acid,
                    ClassId = cid,
                    SectionId = sec_id,
                    StudentRegId = stu_id,
                    EmployeeRegisterId = empregid
                };

                db.FeeCollectionCategories.Add(add);
                db.SaveChanges();
            }

        }

        public void updateSectionWithRegId_ChangeSection(int year, int classId, int studentRegisterId, int secId)
        {
            var getRow = (from a in db.FeeCollections where a.StudentRegisterId == studentRegisterId && a.AcademicYearId == year && a.ClassId == classId select a).FirstOrDefault();
            getRow.SectionId = secId;
            db.SaveChanges();
        }
        //jegadeesh

        public List<Ranklist> GetAnnualPaidCollection(int yearid, int classid, int sectionid, int feesid)
        {
            var ans = (from a in db.FeeCollections
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       from d in db.Fees
                       where a.AcademicYearId == yearid && a.ClassId == classid && a.SectionId == sectionid && a.StudentRegisterId == b.StudentRegisterId && c.StudentRegisterId == b.StudentRegisterId && c.StudentRegisterId == a.StudentRegisterId && d.Amount <= a.AmountCollected
                       select new Ranklist
                       {
                           rollnumber = c.RollNumber,
                           stuname = b.FirstName + " " + b.LastName
                       }).Distinct().ToList();

            return ans;
        }
        public List<FeeCollection> GetAnnualDueCollection(int yearid, int classid, int sectionid, int feesid)
        {
            var ans = (from a in db.FeeCollections
                       where a.AcademicYearId == yearid && a.ClassId == classid && a.SectionId == sectionid //&& a.PaymentTypeId == 0 && a.FeeCategoryId == feesid
                       select a).ToList();

            return ans;
        }

        public List<Ranklist> GetTermPaidCollection(int yearid, int classid, int sectionid, int termid, int feesid)
        {
            var ans = (from a in db.FeeCollections
                       from b in db.Students
                       from c in db.StudentRollNumbers
                       where a.AcademicYearId == yearid && a.ClassId == classid && a.SectionId == sectionid && a.StudentRegisterId == b.StudentRegisterId && c.StudentRegisterId == b.StudentRegisterId && c.StudentRegisterId == a.StudentRegisterId
                       select new Ranklist
                       {
                           rollnumber = c.RollNumber,
                           stuname = b.FirstName + " " + b.LastName
                       }).ToList();

            return ans;
        }
        public List<FeeCollection> GetTermDueCollection(int yearid, int classid, int sectionid, int feesid, int termid)
        {
            var ans = (from a in db.FeeCollections
                       where a.AcademicYearId == yearid && a.ClassId == classid && a.SectionId == sectionid
                       select a).ToList();
            return ans;
        }
        public IEnumerable GetJsonAnnualPay(int yearid, int classid, int sectionid, int stu_id)
        {
            var ans = (from t1 in db.FeeCollections
                       from t2 in db.FeeCategories
                       where t1.StudentRegisterId == stu_id && t1.SectionId == sectionid && t1.ClassId == classid && t1.AcademicYearId == yearid
                       select new { fcategory = t2.FeeCategoryName, famount = t1.FeeAmount, pay = t1.AmountCollected, stat = t1.Remark }).ToList();
            return ans;
        }
        public IEnumerable GetJsonTermPay(int yearid, int classid, int sectionid, int stu_id, int tid)
        {
            var ans = (from t1 in db.FeeCollections
                       from t2 in db.FeeCategories
                       from t3 in db.TermFees
                       from t4 in db.Terms
                       where t1.StudentRegisterId == stu_id && t1.SectionId == sectionid && t1.ClassId == classid && t1.AcademicYearId == yearid && t3.FeeCategoryId == t2.FeeCategoryId && t3.AcademicYearId == yearid && t4.TermId == t3.TermId && t3.ClassId == classid
                       select new { stu = t1.StudentRegisterId, term = t4.TermName, fcategory = t2.FeeCategoryName, famount = t1.FeeAmount, pay = t1.AmountCollected, stat = t1.Remark }).Distinct().ToList();
            return ans;
        }
        public List<FeeCollection> GetjsonAnnual(int year, int clas, int sec, int stu_id)
        {
            var ans = (from t1 in db.FeeCollections
                       where t1.AcademicYearId == year && t1.ClassId == clas && t1.SectionId == sec && t1.StudentRegisterId == stu_id
                       select t1).ToList();
            return ans;
        }
        public List<Fee> GetjsonRemovefeecategory(List<Fee> a2, int s)
        {
            var ans = a2.Where(x => x.FeeCategoryId != s).ToList();
            return ans;
        }
        public List<FeeCollection> GetjsonTerm(int year, int clas, int sec, int stu_id, int tid)
        {
            var ans = (from t1 in db.FeeCollections
                       where t1.AcademicYearId == year && t1.ClassId == clas && t1.SectionId == sec && t1.StudentRegisterId == stu_id
                       select t1).ToList();
            return ans;
        }

        public void UpdateStudentDetails(int applicationId, int studentregisterid, int academicyearid, int classid)
        {
            var getrow = db.FeeCollections.Where(q => q.ApplicationId == applicationId).FirstOrDefault();
            if (getrow != null)
            {
                getrow.StudentRegisterId = studentregisterid;
                getrow.AcademicYearId = academicyearid;
                getrow.ClassId = classid;
                db.SaveChanges();
            }
        }

        public void UpdateFeeRefundCollection(int collectid, int empregid)
        {
            var getCollection = (from a in db.FeeCollections
                                 from b in db.FeeCollectionCategories
                                 where b.CollectionId == collectid && a.FeeCollectionId == b.FeeCollectionId
                                 select a).FirstOrDefault();
            if (getCollection != null)
            {
                getCollection.AmountCollected = 0;
                getCollection.EmployeeRegisterId = empregid;
                db.SaveChanges();
            }
        }
        internal IEnumerable<object> getFeePaidList(int acid, int cid, int secid, int fid)
        {
            var ans = (from t1 in db.Students
                       from t2 in db.AssignClassToStudents
                       from t3 in db.FeeCollectionCategories
                       from t4 in db.StudentRollNumbers
                       where t3.AcademicYearId == t2.AcademicYearId && t3.ClassId == t2.ClassId && t3.SectionId == t2.SectionId 
                       && t1.StudentRegisterId == t4.StudentRegisterId && t1.StudentRegisterId == t2.StudentRegisterId && t1.StudentRegisterId == t3.StudentRegId
                       && t4.AcademicYearId == t2.AcademicYearId && t4.ClassId == t2.ClassId && t4.SectionId == t2.SectionId
                             && t3.AcademicYearId == acid && t3.ClassId == cid && t3.SectionId == secid && t3.FeeCategoryId == fid && t2.Status == true && t2.HaveRollNumber == true
                       select new { StudentName = t1.FirstName + " " + t1.LastName, RollNumber = t4.RollNumber, StudentID = t1.StudentId, PaymentTypeId = t3.PaymentTypeId, Amount = t3.TotalAmount }).ToList();
            return ans;
        }

        internal IEnumerable<object> getFeePaidStudentList(int acid, int cid, int secid, int fid)
        {
            var ans = (from t1 in db.Students
                       from t2 in db.AssignClassToStudents
                       from t3 in db.FeeCollectionCategories
                       from t4 in db.StudentRollNumbers
                       where t3.AcademicYearId == t2.AcademicYearId && t3.ClassId == t2.ClassId && t3.SectionId == t2.SectionId
                       && t1.StudentRegisterId == t4.StudentRegisterId && t1.StudentRegisterId == t2.StudentRegisterId && t1.StudentRegisterId == t3.StudentRegId
                       && t4.AcademicYearId == t2.AcademicYearId && t4.ClassId == t2.ClassId && t4.SectionId == t2.SectionId
                             && t3.AcademicYearId == acid && t3.ClassId == cid && t3.SectionId == secid && t3.FeeCategoryId == fid && t2.Status == true && t2.HaveRollNumber == true
                       select new { StudentName = t1.FirstName + " " + t1.LastName, RollNumber = t4.RollNumber, StudentID = t1.StudentId }).Distinct().ToList();
            return ans;
        }

        public M_StudentRegisteration GetFeeCollectDetails(int feecollectid)
        {
            var ans = (from a in db.FeeCollections
                       from b in db.AcademicYears
                       from c in db.Classes
                       from d in db.Sections
                       from e in db.Students
                       from f in db.StudentRollNumbers
                       where a.FeeCollectionId == feecollectid && b.AcademicYearId == a.AcademicYearId && c.ClassId == a.ClassId && d.SectionId == a.SectionId && e.StudentRegisterId == a.StudentRegisterId && e.StudentStatus == true  && f.StudentRegisterId == a.StudentRegisterId && f.Status == true
                       select new M_StudentRegisteration
                       {
                           txt_academicyear = b.AcademicYear1,
                           txt_Class = c.ClassType,
                           txt_Section = d.SectionName,
                           StuFirstName = e.FirstName + " " + e.LastName,
                           StudentRollNumber = f.RollNumber,
                           StudentId = e.StudentId,
                           ReceiptNo = a.FeeCollectionId,
                       }).Distinct().FirstOrDefault();
            return ans;
        }
        public M_StudentRegisteration GetPreAdmissionFeeCollectDetails(int feecollectid)
        {
            var ans = (from a in db.FeeCollections
                       from b in db.AcademicYears
                       from c in db.Classes
                       from e in db.PreAdmissionStudentRegisters
                       where a.FeeCollectionId == feecollectid && e.StudentAdmissionId == a.ApplicationId && b.AcademicYearId == a.AcademicYearId && c.ClassId == a.ClassId
                       select new M_StudentRegisteration
                       {
                           txt_academicyear = b.AcademicYear1,
                           txt_Class = c.ClassType,
                           StuFirstName = e.StuFirstName + " " + e.StuLastname,
                           StudentRegisterId = e.StudentAdmissionId,
                           ReceiptNo = a.FeeCollectionId,

                       }).Distinct().FirstOrDefault();
            return ans;
        }
        public FeeCollection GetCollectionDetails(int collectid)
        {
            var ans = db.FeeCollections.Where(q => q.FeeCollectionId == collectid).FirstOrDefault();
            return ans;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}