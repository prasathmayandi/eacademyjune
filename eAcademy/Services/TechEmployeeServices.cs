﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;
using System.Collections;
using System.IO;
using System.Web.Security;
using eAcademy.HelperClass;
using eAcademy.Areas.Communication.Models;

namespace eAcademy.Services
{
    public class TechEmployeeServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public List<Employee> LoginCheck(string username, string password)
        {
            var ans = dc.TechEmployees.Where(q => q.EmployeeId.Equals(username) && q.Password.Equals(password) && q.EmployeeStatus == true).Select(s => new Employee { EmployeeId = s.EmployeeRegisterId,EmpRegId=s.EmployeeId, EmployeeName = s.EmployeeName, File = s.ImgFile, Email = s.Email }).ToList();
            return ans;
        }

        public List<Employee> getEmployee()
        {
            var ans = (from t1 in dc.TechEmployees
                       from t2 in dc.EmployeeTypes
                       from t3 in dc.EmployeeDesignations

                       where t2.EmployeeTypeId == t1.EmployeeTypeId && t3.EmployeeDesignationId == t1.EmployeeDesignationId 
                       select new { EmployeeId = t1.EmployeeRegisterId, EmpId = t1.EmployeeId, Employee = t1.EmployeeName, DOB = t1.DOJ, DOJ = t1.DOJ, DOR = t1.DOR, DOResigned = t1.DOL, Address = t1.AddressLine1, Contact = t1.Mobile, EmgPerson = t1.EmergencyContactPerson, EmgContact = t1.EmergencyContactNumber, EmployeeTypes = t2.EmployeeTypes, EmployeeDesignations = t3.Designation,Status=t1.EmployeeStatus }).ToList()
                       .Select(q => new Employee
                         {
                             EmployeeId = q.EmployeeId,
                             EmpRegId = QSCrypt.Encrypt(q.EmployeeId.ToString()),
                             EmpId = q.EmpId,
                             EmployeeName = q.Employee,
                             Address = q.Address,

                             Contact = q.Contact,
                             Edit = " <i class='fa fa-pencil-square-o'></i>",
                             PersonContact = q.EmgPerson + " / " + q.EmgContact,
                             EmployeeTypes = q.EmployeeTypes,
                             EmployeeDesianations = q.EmployeeDesignations,
                              Status=q.Status
                         }).ToList();
            return ans;
        }

        public List<TechEmployee> getTechEmployee()
        {
            var ans = dc.TechEmployees.Where(q => q.EmployeeStatus.Value.Equals(true)).ToList();
            return ans;
        }

        public List<TechEmployee> getEmp()
        {
            var ans = dc.TechEmployees.Select(q => q).ToList();
            return ans;
        }
        public IEnumerable<object> getEmployees()
        {

            var ans = (
                    (from t1 in dc.TechEmployees
                     select new { empRegid = t1.EmployeeRegisterId, ename = t1.EmployeeName, empid = t1.EmployeeId })
                     .Union
                     (from t2 in dc.OtherEmployees
                      select new { empRegid = t2.EmployeeRegisterId, ename = t2.EmployeeName, empid = t2.EmployeeId })).ToList();
            return ans;
        }

        public IEnumerable<object> EmployeeList()
        {
            var ans = ((from t1 in dc.TechEmployees
                        select new { empRegid = t1.EmployeeRegisterId, ename = t1.EmployeeName, empid = t1.EmployeeId })
                            .Union
                            (from t2 in dc.OtherEmployees
                             select new { empRegid = t2.EmployeeRegisterId, ename = t2.EmployeeName, empid = t2.EmployeeId })).ToList();
            return ans;
        }

        public TechEmployee EmployeeListByRegId(int empRegId)
        {
            var emp = dc.TechEmployees.Where(q => q.EmployeeRegisterId.Equals(empRegId)).First();
            return emp;
        }
        public Employee getEmployeeID(string EmpRegid)
        {

            var ans = ((from t1 in dc.TechEmployees
                        where t1.EmployeeId == EmpRegid
                        select new Employee { EmployeeId = t1.EmployeeRegisterId })
                        .Union(from t2 in dc.OtherEmployees
                               where t2.EmployeeId == EmpRegid
                               select new Employee { EmployeeId = t2.EmployeeRegisterId })).First();
            return ans;
        }

        public TechEmployee getEmployeeByImg(string imgFile)
        {
            var ans = dc.TechEmployees.Where(q => q.ImgFile.Equals(imgFile)).SingleOrDefault();
            return ans;
        }

        public IEnumerable<object> getEmployeeList()
        {
            var ans = dc.TechEmployees.Select(s => new { eid = s.EmployeeRegisterId, ename = s.EmployeeName });
            return ans;
        }

        internal string addTechEmployee1(string Fname, string Mname, string Lname, DateTime DOB, string Gender, string Bloodgrp, string Religion, string Nationality, string Community, string Marital, int employeetype, int empdesignation, string txt_ClassList1, string txt_SubjectList1, string Contact, long Mobile, string Email, string Address1, string Address2, string City, string State, string Country, string PIN, string PermanentAddress1, string PermanentAddress2, string PermanentCity, string PermanentState, string PermanentCountry, string PermanentPIN, string EmgPerson, long EmgContact, string Relationship)
        {
            TechEmployee techs = new TechEmployee()
            {
                EmployeeName = Fname,
                MiddleName = Mname,
                LastName = Lname,
                Gender = Gender,
                DOB = DOB,
                BloodGroup = Bloodgrp,
                Community = Community,
                Religion = Religion,
                Nationality = Nationality,
                MaritalStatus = Marital,
                Mobile = Mobile,
                Phone = Contact,
                Email = Email,
                AddressLine1 = Address1,
                AddressLine2 = Address2,
                City = City,
                State = State,
                Country = Country,
                ZipCode = PIN,
                PermanentAddress1 = PermanentAddress1,
                PermanentAddress2 = PermanentAddress2,
                PermanentCity = PermanentCity,
                PermanentState = PermanentState,
                PermanentCountry = PermanentCountry,
                PermanentZipCode = PermanentPIN,
                EmergencyContactPerson = EmgPerson,
                EmergencyContactNumber = EmgContact,
                EmergencyContactpersonRelationship = Relationship,
                EmployeeTypeId = employeetype,
                EmployeeDesignationId = empdesignation,
                EmployeeStatus = true

            };
            dc.TechEmployees.Add(techs);
            dc.SaveChanges();
            int empregid = techs.EmployeeRegisterId;
            string[] Class = txt_ClassList1.Split(',');
            string[] Subject = txt_SubjectList1.Split(',');
           
            for (int i = 0; i < Class.Length; i++)
            {
                if (Class[i] != "")
                {
                    TechEmployeeClassSubject classSub = new TechEmployeeClassSubject()
                    {
                        ClassId = int.Parse(Class[i]),
                        EmployeeRegisterId = empregid,
                        Status = true
                    };
                    dc.TechEmployeeClassSubjects.Add(classSub);
                    dc.SaveChanges();
                }
            }

            for (int i = 0; i < Subject.Length; i++)
            {
                if (Subject[i] != "")
                {
                    TechEmployeeClassSubject classSub = new TechEmployeeClassSubject()
                    {
                        EmployeeRegisterId = empregid,
                        SubjectId = int.Parse(Subject[i]),
                        Status = true
                    };
                    dc.TechEmployeeClassSubjects.Add(classSub);
                    dc.SaveChanges();
                }
            }
            var ename = techs.EmployeeName;
            var ans = dc.SchoolSettings.Select(q => q).First();
            var schoolcode = ans.SchoolCode;
            var empid = ans.StaffIDFormat;
            string eid = "";
            var updatetechemp = dc.TechEmployees.Where(q => q.EmployeeRegisterId == empregid).First();
            if (updatetechemp != null)
            {
                updatetechemp.EmployeeId = (schoolcode+empid + (empregid).ToString()).ToString();
                updatetechemp.UserName = (ename + (empregid).ToString()).ToString();
                dc.SaveChanges();
                var path = HttpContext.Current.Server.MapPath("~/img/EmployeePhotos/" + updatetechemp.EmployeeId);
                System.IO.Directory.CreateDirectory(path);
                eid = updatetechemp.EmployeeId;
            }

            return eid;
        }
        public TechEmployee GetTechEmployeeDetails(string epmid)
        {
            var ans = dc.TechEmployees.Where(q => q.EmployeeId == epmid).FirstOrDefault();
            return ans;
        }

        internal string addTechEmployee2(string Empid, string txt_Experience, HttpPostedFileBase Resume, HttpPostedFileBase Certificate, int count1, string Course, string Insitute, string University, string Year, string Mark, int count2, string CourseName, string CertificateName, HttpPostedFileBase[] CertificationFile, int count3, string EmpInst, string Designation, string Expertise, string EmpSdate, string EmpEdate, string empTotal, string Email, HttpPostedFileBase empphoto, HttpPostedFileBase RationCard, HttpPostedFileBase VoterId, DateTime txt_doj, long? txt_Salary, string txt_AcNo, string txt_AcName, string txt_BName, string txt_Pan_Number, long psw)
        {
            FileHelper fh = new FileHelper();
            var emp = dc.TechEmployees.Where(q => q.EmployeeId.Equals(Empid)).Select(s => new { erid = s.EmployeeRegisterId, name = s.EmployeeName }).First();
            var empregid = emp.erid;

            var update = dc.TechEmployees.Where(q => q.EmployeeRegisterId.Equals(empregid)).SingleOrDefault();
            var date = DateTimeByZone.getCurrentDateTime();
            if (update != null)
            {
                string name;
                string foldername = "EmployeePhotos";
                long FileSzie = 0;
                string ResumeCertificate = ""; string DegreeCertificate = ""; string RationCardCertificate = ""; string VoterIdCertificate = ""; string EmployeePhoto = ""; string courseCertificate = "";
                if (Resume != null)
                {
                    name = "Resume";
                    FileSzie = 512000;
                    var ResumeCertificate1 = fh.CheckEmployeeFile(Resume, Empid, name, FileSzie, foldername);
                    if (ResumeCertificate1.FileSizeId == 98)
                    {
                        string ErrorMessage = "Resume certificate file size exceeds,whithin 512kb";
                        return ErrorMessage;
                    }
                    else if (ResumeCertificate1.FileTypeId == 99)
                    {
                        string ErrorMessage = "Resume certificate file format not supported";
                        return ErrorMessage;
                    }
                    else if (ResumeCertificate1.FileSizeId != 98 && ResumeCertificate1.FileTypeId != 99 && ResumeCertificate1.filename != "")
                    {
                        ResumeCertificate = ResumeCertificate1.filename;
                        update.ResumeFile = ResumeCertificate;
                    }
                }
                if (Certificate != null)
                {
                    name = "DegreeCertificate";
                    FileSzie = 512000;
                    var DegreeCertificate2 = fh.CheckEmployeeFile(Certificate, Empid, name, FileSzie, foldername);
                    if (DegreeCertificate2.FileSizeId == 98)
                    {
                        string ErrorMessage = "Degree certificate file size exceeds,whithin 512kb";
                        return ErrorMessage;
                    }
                    else if (DegreeCertificate2.FileTypeId == 99)
                    {
                        string ErrorMessage = "Degree certificate file format not supported";
                        return ErrorMessage;
                    }
                    else if (DegreeCertificate2.FileSizeId != 98 && DegreeCertificate2.FileTypeId != 99 && DegreeCertificate2.filename != "")
                    {
                        DegreeCertificate = DegreeCertificate2.filename;
                        update.DegreeCertificateFile = DegreeCertificate;
                    }
                }
                if (RationCard != null)
                {
                    name = "RationCard";
                    FileSzie = 512000;
                    var RationCardCertificate1 = fh.CheckEmployeeFile(RationCard, Empid, name, FileSzie, foldername);
                    if (RationCardCertificate1.FileSizeId == 98)
                    {
                        string ErrorMessage = "RationCard certificate file size exceeds,whithin 512kb";
                        return ErrorMessage;
                    }
                    else if (RationCardCertificate1.FileTypeId == 99)
                    {
                        string ErrorMessage = "RationCard certificate file format not supported";
                        return ErrorMessage;
                    }
                    else if (RationCardCertificate1.FileSizeId != 98 && RationCardCertificate1.FileTypeId != 99 && RationCardCertificate1.filename != "")
                    {
                        RationCardCertificate = RationCardCertificate1.filename;
                        update.RationCard = RationCardCertificate;
                    }
                }
                if (VoterId != null)
                {
                    name = "VoterId";
                    FileSzie = 512000;
                    var VoterIdCertificate1 = fh.CheckEmployeeFile(VoterId, Empid, name, FileSzie, foldername);
                    if (VoterIdCertificate1.FileSizeId == 98)
                    {
                        string ErrorMessage = "VoterId certificate file size exceeds,whithin 512kb";
                        return ErrorMessage;
                    }
                    else if (VoterIdCertificate1.FileTypeId == 99)
                    {
                        string ErrorMessage = "VoterId certificate file format not supported";
                        return ErrorMessage;
                    }
                    else if (VoterIdCertificate1.FileSizeId != 98 && VoterIdCertificate1.FileTypeId != 99 && VoterIdCertificate1.filename != "")
                    {
                        VoterIdCertificate = VoterIdCertificate1.filename;
                        update.VoterId = VoterIdCertificate;
                    }
                }
                if (empphoto != null)
                {
                    name = "Photo";
                    FileSzie = 512000;
                    var EmployeePhoto1 = fh.CheckEmployeeImageFile(empphoto, Empid, name, FileSzie, foldername);
                    if (EmployeePhoto1.FileSizeId == 98)
                    {
                        string ErrorMessage = "Employee photo file size exceeds,whithin 512kb";
                        return ErrorMessage;
                    }
                    else if (EmployeePhoto1.FileTypeId == 99)
                    {
                        string ErrorMessage = "Employee photo file format not supported";
                        return ErrorMessage;
                    }
                    else if (EmployeePhoto1.FileSizeId != 98 && EmployeePhoto1.FileTypeId != 99 && EmployeePhoto1.filename != "")
                    {
                        EmployeePhoto = EmployeePhoto1.filename;
                        update.ImgFile = EmployeePhoto;
                    }
                }
                if (txt_Experience != "")
                {
                    update.Experience = txt_Experience;
                }                
                update.DOJ = txt_doj;
                update.Salary = txt_Salary;
                update.AccountNo = txt_AcNo;
                update.AccountName = txt_AcName;
                update.BankName = txt_BName;
                update.PAN = txt_Pan_Number;
                update.DOR = date;
                update.Password = FormsAuthentication.HashPasswordForStoringInConfigFile(psw.ToString(), "SHA1");
                dc.SaveChanges();
                string[] Single_Course = Course.Split(',');
                string[] Single_Insitute = Insitute.Split(',');
                string[] Single_University = University.Split(',');
                string[] Single_Year = Year.Split(',');
                string[] Single_Mark = Mark.Split(',');
                for (int i = 0; i < count1; i++)
                {
                    if (Single_Course[i] != "" && Single_Insitute[i] != "" && Single_Year[i] != "" && Single_Mark[i] != "")
                    {
                        TechEmployeeEducation edu = new TechEmployeeEducation()
                        {
                            EmployeeRegisterId = empregid,
                            Cours = Single_Course[i],
                            Institute = Single_Insitute[i],
                            University = Single_University[i],
                            YearOfCompletion = Single_Year[i],
                            Mark = float.Parse(Single_Mark[i])
                        };

                        dc.TechEmployeeEducations.Add(edu);
                        dc.SaveChanges();
                    }
                }
                string[] Single_EmpInst = EmpInst.Split(',');
                string[] Single_Designation = Designation.Split(',');
                string[] Single_Expertise = Expertise.Split(',');
                string[] Single_EmpSdate = EmpSdate.Split(',');
                string[] Single_EmpEdate = EmpEdate.Split(',');
                string[] Single_empTotal = empTotal.Split(',');

                for (int i = 0; i < count3; i++)
                {
                    if (Single_EmpInst[i] != "" && Single_Designation[i] != "" && Single_Expertise[i] != "" && Single_EmpSdate[i] != "" && Single_EmpEdate[i] != "" && Single_empTotal[i] != "")
                    {
                        TechEmployeeEmployment emps = new TechEmployeeEmployment()
                        {
                            EmployeeRegisterId = empregid,
                            Institute = Single_EmpInst[i],
                            Designation = Single_Designation[i],
                            Expertise = Single_Expertise[i],
                            StartDate = DateTime.Parse(Single_EmpSdate[i]),
                            EndDate = DateTime.Parse(Single_EmpEdate[i]),
                            TotalYears = Single_empTotal[i]
                        };

                        dc.TechEmployeeEmployments.Add(emps);
                        dc.SaveChanges();
                    }
                }
                string[] Single_CourseName = CourseName.Split(',');
                string[] Single_CertificateName = CertificateName.Split(',');

                for (int i = 0; i < count2; i++)
                {
                    if (Single_CourseName[i] != "" && Single_CertificateName[i] != "" && CertificationFile[i].FileName != "")
                    {
                        string Certify = Path.GetFileName(CertificationFile[i].FileName) + Path.GetExtension(CertificationFile[i].FileName);
                        HttpPostedFileBase file = CertificationFile[i];
                        name = "CourseFile(" + (i + 1) + ")";
                        FileSzie = 512000;
                        var courseCertificate1 = fh.CheckEmployeeFile(file, Empid, name, FileSzie, foldername);
                        if (courseCertificate1.FileSizeId == 98)
                        {
                            string ErrorMessage = "CourseFile certificate file size exceeds,whithin 512kb";
                            return ErrorMessage;
                        }
                        else if (courseCertificate1.FileTypeId == 99)
                        {
                            string ErrorMessage = "CourseFile certificate file format not supported";
                            return ErrorMessage;
                        }
                        else if (courseCertificate1.FileSizeId != 98 && courseCertificate1.FileTypeId != 99 && courseCertificate1.filename != "")
                        {
                            courseCertificate = courseCertificate1.filename;
                            TechEmployeeProfessional pro = new TechEmployeeProfessional()
                            {
                                EmployeeRegisterId = empregid,
                                Cours = Single_CourseName[i],
                                CertificateName = Single_CertificateName[i],
                                CertificateFile = courseCertificate
                            };

                            dc.TechEmployeeProfessionals.Add(pro);
                            dc.SaveChanges();
                        }
                    }
                }
            }
            string Message = "True";
            return Message;
        }
        internal string addExcelTechEmployee2(string Empid, string txt_Experience, string Resume, string Certificate, int count1, string Course, string Insitute, string University, string Year, string Mark, int count2, string CourseName, string CertificateName, string CertificationFile, int count3, string EmpInst, string Designation, string Expertise, string EmpSdate, string EmpEdate, string empTotal, string Email, string empphoto, string RationCard, string VoterId, DateTime txt_doj, long? txt_Salary, string txt_AcNo, string txt_AcName, string txt_BName, string txt_Pan_Number, long psw)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            FileHelper fh = new FileHelper();
            var emp = dc.TechEmployees.Where(q => q.EmployeeId.Equals(Empid)).Select(s => new { erid = s.EmployeeRegisterId, name = s.EmployeeName }).First();
            var empregid = emp.erid;

            var update = dc.TechEmployees.Where(q => q.EmployeeRegisterId.Equals(empregid)).SingleOrDefault();
                        update.ResumeFile = Resume;
                        update.DegreeCertificateFile = Certificate;
                        update.RationCard = RationCard;
                        update.ImgFile = empphoto;
                update.Experience = txt_Experience;
                update.DOJ = txt_doj;
                update.Salary = txt_Salary;
                update.AccountNo = txt_AcNo;
                update.AccountName = txt_AcName;
                update.BankName = txt_BName;
                update.PAN = txt_Pan_Number;
                update.DOR = date;
                update.Password = FormsAuthentication.HashPasswordForStoringInConfigFile(psw.ToString(), "SHA1");
                dc.SaveChanges();
                string[] Single_Course = Course.Split(',');
                string[] Single_Insitute = Insitute.Split(',');
                string[] Single_University = University.Split(',');
                string[] Single_Year = Year.Split(',');
                string[] Single_Mark = Mark.Split(',');
                for (int i = 0; i < count1; i++)
                {
                    if (Single_Course[i] != "" && Single_Insitute[i] != "" && Single_Year[i] != "" && Single_Mark[i] != "")
                    {
                        TechEmployeeEducation edu = new TechEmployeeEducation()
                        {
                            EmployeeRegisterId = empregid,
                            Cours = Single_Course[i],
                            Institute = Single_Insitute[i],
                            University = Single_University[i],
                            YearOfCompletion = Single_Year[i],
                            Mark = float.Parse(Single_Mark[i])
                        };

                        dc.TechEmployeeEducations.Add(edu);
                        dc.SaveChanges();
                    }
                }
                string[] Single_EmpInst = EmpInst.Split(',');
                string[] Single_Designation = Designation.Split(',');
                string[] Single_Expertise = Expertise.Split(',');
                string[] Single_EmpSdate = EmpSdate.Split(',');
                string[] Single_EmpEdate = EmpEdate.Split(',');
                string[] Single_empTotal = empTotal.Split(',');

                for (int i = 0; i < count3; i++)
                {
                    if (Single_EmpInst[i] != "" && Single_Designation[i] != "" && Single_Expertise[i] != "" && Single_EmpSdate[i] != "" && Single_EmpEdate[i] != "" && Single_empTotal[i] != "")
                    {
                        TechEmployeeEmployment emps = new TechEmployeeEmployment()
                        {
                            EmployeeRegisterId = empregid,
                            Institute = Single_EmpInst[i],
                            Designation = Single_Designation[i],
                            Expertise = Single_Expertise[i],
                            StartDate = DateTime.Parse(Single_EmpSdate[i]),
                            EndDate = DateTime.Parse(Single_EmpEdate[i]),
                            TotalYears = Single_empTotal[i]
                        };

                        dc.TechEmployeeEmployments.Add(emps);
                        dc.SaveChanges();
                    }
                }
                string[] Single_CourseName = CourseName.Split(',');
                string[] Single_CertificateName = CertificateName.Split(',');

                for (int i = 0; i < count2; i++)
                {
                    if (Single_CourseName[i] != "" && Single_CertificateName[i] != "" )
                    {
                            TechEmployeeProfessional pro = new TechEmployeeProfessional()
                            {
                                EmployeeRegisterId = empregid,
                                Cours = Single_CourseName[i],
                                CertificateName = Single_CertificateName[i],
                                CertificateFile = Certificate
                            };

                            dc.TechEmployeeProfessionals.Add(pro);
                            dc.SaveChanges();
                        
                    }
                }
            
            string Message = "True";
            return Message;
        }

        internal void addTechEmployee3(string txt_doj, string txt_Salary, string ClassList, string SubjectList, string txt_AcNo, string txt_AcName, string txt_BName, string Email)
        {
            var emp = dc.TechEmployees.Where(q => q.Email.Equals(Email)).Select(s => new { erid = s.EmployeeRegisterId, name = s.EmployeeName }).First();
            var empregid = emp.erid;
            var update = dc.TechEmployees.Where(q => q.EmployeeRegisterId.Equals(empregid)).SingleOrDefault();
            update.DOJ = DateTime.Parse(txt_doj);
            update.Salary = Decimal.Parse(txt_Salary);
            update.AccountNo = txt_AcNo;
            update.AccountName = txt_AcName;
            update.BankName = txt_BName;
            dc.SaveChanges();
        }

        internal Eprofile getEmployeeListById(int empId)
        {
            var ans = (from a in dc.TechEmployees
                       from b in dc.EmployeeTypes
                       from c in dc.EmployeeDesignations
                       where a.EmployeeRegisterId == empId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId
                       select new Eprofile
                       {
                           Fname = a.EmployeeName,
                           Mname = a.MiddleName,
                           Lname = a.LastName,
                           EmployeeRegisterId = a.EmployeeRegisterId,
                           EmployeeId = a.EmployeeId,
                           EmployeeName = a.EmployeeName + " " + a.LastName,
                           DateOfBirth = SqlFunctions.DateName("day", a.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOB),                           
                           Gender = a.Gender,
                           BloodGroup = a.BloodGroup,
                           Community = a.Community,
                           Religion = a.Religion,
                           Nationality = a.Nationality,
                           Marital = a.MaritalStatus,
                           EmployeeTypeId = b.EmployeeTypeId,
                           EmployeeDesignationId = c.EmployeeDesignationId,
                           emptype = b.EmployeeTypes,
                           empdesign = c.Designation,
                           Email = a.Email,
                           LandlineNo = a.Phone,
                           Contact = a.Mobile,
                           Addressline1 = a.AddressLine1,
                           Addressline2 = a.AddressLine2,
                           State = a.State,
                           City = a.City,
                           Country = a.Country,
                           Pincode = a.ZipCode,
                           PermanentAddressline1 = a.PermanentAddress1,
                           PermanentAddressline2 = a.PermanentAddress2,
                           PermanentCity = a.PermanentCity,
                           PermanentCountry = a.PermanentCountry,
                           PermanentState = a.PermanentState,
                           PermanentPincode = a.PermanentZipCode,
                           DateOfLeaving = SqlFunctions.DateName("day", a.DOL).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOL.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOL),
                           DateOfRegister = SqlFunctions.DateName("day", a.DOR).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOR.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOR) ,
                           Qualification = a.Qualification,
                           EmergencyContactPerson = a.EmergencyContactPerson,
                           EmergencyContactNumber = a.EmergencyContactNumber,
                           EmergencyContactPersonRelationShip = a.EmergencyContactpersonRelationship,
                           EmployeeStatus = a.EmployeeStatus,
                           Experience = a.Experience,
                           Expertise = a.Expertise,
                           ImgFile = a.ImgFile,
                           ResumeFile = a.ResumeFile,
                           RationCardFile = a.RationCard,
                           VoterIdFile = a.VoterId,
                           DegreeFile = a.DegreeCertificateFile,
                           Salary = a.Salary,
                           DateOfJoin = SqlFunctions.DateName("day", a.DOJ).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOJ.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOJ) ,
                           PAN = a.PAN,
                           BankAcName = a.AccountName,
                           BankAcNo = a.AccountNo,
                           BankName = a.BankName,
                           EmpStatus=a.EmployeeStatus
                       }).First();
            return ans;
        }

        internal string updateTechEmployee2(int empRegId, string txt_Experience, HttpPostedFileBase Resume, HttpPostedFileBase Certificate, int count1, string Course, string Insitute, string University, string Year, string Mark, int count2, string CourseName, string CertificateName, HttpPostedFileBase[] CertificationFile, int count3, string EmpInst, string Designation, string Expertise, string EmpSdate, string EmpEdate, string empTotal, string Email, HttpPostedFileBase empphoto, HttpPostedFileBase RationCard, HttpPostedFileBase VoterId, DateTime txt_doj, long? txt_Salary, string txt_AcNo, string txt_AcName, string txt_BName, string txt_Pan_Number, string EducationalDetailsId, string EmployeeExperienceId, string ProfessionalId, long? psw, DateTime? txt_dor)
        {
            FileHelper fh = new FileHelper();
            string name;
            string foldername = "EmployeePhotos";
            long FileSzie = 0;
            string ResumeCertificate = ""; string DegreeCertificate = ""; string RationCardCertificate = ""; string VoterIdCertificate = ""; string EmployeePhoto = ""; string courseCertificate = "";
            var emp = dc.TechEmployees.Where(q => q.EmployeeRegisterId == empRegId).Select(s => new { erid = s.EmployeeRegisterId, name = s.EmployeeName, empid = s.EmployeeId }).First();
            string Empid = emp.empid;

            var update = dc.TechEmployees.Where(q => q.EmployeeRegisterId.Equals(empRegId)).SingleOrDefault();
            if (update != null)
            {
                if (Resume != null)
                {
                    name = "Resume";
                    FileSzie = 512000;
                    var ResumeCertificate1 = fh.CheckEmployeeFile(Resume, Empid, name, FileSzie, foldername);
                    if (ResumeCertificate1.FileSizeId == 98)
                    {
                        string ErrorMessage = "Resume certificate file size exceeds,whithin 512kb";
                        return ErrorMessage;
                    }
                    else if (ResumeCertificate1.FileTypeId == 99)
                    {
                        string ErrorMessage = "Resume certificate file format not supported";
                        return ErrorMessage;
                    }
                    else if (ResumeCertificate1.FileSizeId != 98 && ResumeCertificate1.FileTypeId != 99 && ResumeCertificate1.filename != "")
                    {
                        ResumeCertificate = ResumeCertificate1.filename;
                        update.ResumeFile = ResumeCertificate;
                    }
                }
                if (Certificate != null)
                {
                    name = "DegreeCertificate";
                    FileSzie = 512000;
                    var DegreeCertificate2 = fh.CheckEmployeeFile(Certificate, Empid, name, FileSzie, foldername);
                    if (DegreeCertificate2.FileSizeId == 98)
                    {
                        string ErrorMessage = "Degree certificate file size exceeds,whithin 512kb";
                        return ErrorMessage;
                    }
                    else if (DegreeCertificate2.FileTypeId == 99)
                    {
                        string ErrorMessage = "Degree certificate file format not supported";
                        return ErrorMessage;
                    }
                    else if (DegreeCertificate2.FileSizeId != 98 && DegreeCertificate2.FileTypeId != 99 && DegreeCertificate2.filename != "")
                    {
                        DegreeCertificate = DegreeCertificate2.filename;
                        update.DegreeCertificateFile = DegreeCertificate;
                    }
                }
                if (RationCard != null)
                {
                    name = "RationCard";
                    FileSzie = 512000;
                    var RationCardCertificate1 = fh.CheckEmployeeFile(RationCard, Empid, name, FileSzie, foldername);
                    if (RationCardCertificate1.FileSizeId == 98)
                    {
                        string ErrorMessage = "RationCard certificate file size exceeds,whithin 512kb";
                        return ErrorMessage;
                    }
                    else if (RationCardCertificate1.FileTypeId == 99)
                    {
                        string ErrorMessage = "RationCard certificate file format not supported";
                        return ErrorMessage;
                    }
                    else if (RationCardCertificate1.FileSizeId != 98 && RationCardCertificate1.FileTypeId != 99 && RationCardCertificate1.filename != "")
                    {
                        RationCardCertificate = RationCardCertificate1.filename;
                        update.RationCard = RationCardCertificate;
                    }
                }
                if (VoterId != null)
                {
                    name = "VoterId";
                    FileSzie = 512000;
                    var VoterIdCertificate1 = fh.CheckEmployeeFile(VoterId, Empid, name, FileSzie, foldername);
                    if (VoterIdCertificate1.FileSizeId == 98)
                    {
                        string ErrorMessage = "VoterId certificate file size exceeds,whithin 512kb";
                        return ErrorMessage;
                    }
                    else if (VoterIdCertificate1.FileTypeId == 99)
                    {
                        string ErrorMessage = "VoterId certificate file format not supported";
                        return ErrorMessage;
                    }
                    else if (VoterIdCertificate1.FileSizeId != 98 && VoterIdCertificate1.FileTypeId != 99 && VoterIdCertificate1.filename != "")
                    {
                        VoterIdCertificate = VoterIdCertificate1.filename;
                        update.VoterId = VoterIdCertificate;
                    }
                }
                if (empphoto != null)
                {
                    name = "Photo";
                    FileSzie = 512000;
                    var EmployeePhoto1 = fh.CheckEmployeeImageFile(empphoto, Empid, name, FileSzie, foldername);
                    if (EmployeePhoto1.FileSizeId == 98)
                    {
                        string ErrorMessage = "Employee photo file size exceeds,whithin 512kb";
                        return ErrorMessage;
                    }
                    else if (EmployeePhoto1.FileTypeId == 99)
                    {
                        string ErrorMessage = "Employee photo file format not supported";
                        return ErrorMessage;
                    }
                    else if (EmployeePhoto1.FileSizeId != 98 && EmployeePhoto1.FileTypeId != 99 && EmployeePhoto1.filename != "")
                    {
                        EmployeePhoto = EmployeePhoto1.filename;
                        update.ImgFile = EmployeePhoto;
                    }
                }
                if (txt_Experience != "")
                {
                    update.Experience = txt_Experience;
                }                
                update.DOJ = txt_doj;
                update.Salary = txt_Salary;
                update.AccountNo = txt_AcNo;
                update.AccountName = txt_AcName;
                update.BankName = txt_BName;
                update.PAN = txt_Pan_Number;
                update.DOL = txt_dor;
                DateTime date = DateTimeByZone.getCurrentDate();

                if (update.DOR == null)
                {
                    update.DOR = date.Date;
                }
                if (txt_dor != null)
                {
                    update.EmployeeStatus = false;
                    update.DOL = txt_dor;
                }
                dc.SaveChanges();
            }
            string[] Single_Course = Course.Split(',');
            string[] Single_Insitute = Insitute.Split(',');
            string[] Single_University = University.Split(',');
            string[] Single_Year = Year.Split(',');
            string[] Single_Mark = Mark.Split(',');
            string[] Single_Educationalid = EducationalDetailsId.Split(',');
            ArrayList ids = new ArrayList();
            for (int i = 0; i < count1; i++)
            {
                if (Convert.ToInt16(Single_Educationalid[i]) != 0)
                {
                    int Edu_id = Convert.ToInt16(Single_Educationalid[i]);
                    var updateEmployeeEducation = dc.TechEmployeeEducations.Where(q => q.EmployeeRegisterId.Value.Equals(empRegId) && q.TechEmployeeEducationId.Equals(Edu_id)).First();
                    updateEmployeeEducation.Cours = Single_Course[i];
                    updateEmployeeEducation.Institute = Single_Insitute[i];
                    updateEmployeeEducation.University = Single_University[i];
                    updateEmployeeEducation.YearOfCompletion = Single_Year[i];
                    updateEmployeeEducation.Mark = float.Parse(Single_Mark[i]);
                    dc.SaveChanges();
                    ids.Add(Edu_id);
                }
                else
                {
                    TechEmployeeEducation edu = new TechEmployeeEducation()
                    {
                        EmployeeRegisterId = empRegId,
                        Cours = Single_Course[i],
                        Institute = Single_Insitute[i],
                        University = Single_University[i],
                        YearOfCompletion = Single_Year[i],
                        Mark = float.Parse(Single_Mark[i])
                    };

                    dc.TechEmployeeEducations.Add(edu);
                    dc.SaveChanges();
                }
            }
            string[] Single_EmpInst = EmpInst.Split(',');
            string[] Single_Designation = Designation.Split(',');
            string[] Single_Expertise = Expertise.Split(',');
            string[] Single_EmpSdate = EmpSdate.Split(',');
            string[] Single_EmpEdate = EmpEdate.Split(',');
            string[] Single_empTotal = empTotal.Split(',');
            string[] Single_EmployeeExperienceId = EmployeeExperienceId.Split(',');
            for (int i = 0; i < count3; i++)
            {
                if (Convert.ToInt16(Single_EmployeeExperienceId[i])!= 0)
                {
                    int exp_id = Convert.ToInt16(Single_EmployeeExperienceId[i]);
                    var updateEmployeeEducation = dc.TechEmployeeEmployments.Where(q => q.EmployeeRegisterId.Value.Equals(empRegId) && q.TechEmployeeEmploymentId.Equals(exp_id)).First();
                    updateEmployeeEducation.Institute = Single_EmpInst[i];
                    updateEmployeeEducation.Designation = Single_Designation[i];
                    updateEmployeeEducation.Expertise = Single_Expertise[i];
                    updateEmployeeEducation.StartDate = DateTime.Parse(Single_EmpSdate[i]);
                    updateEmployeeEducation.EndDate = DateTime.Parse(Single_EmpEdate[i]);
                    updateEmployeeEducation.TotalYears = Single_empTotal[i];
                    dc.SaveChanges();
                }
                else
                {
                    TechEmployeeEmployment emps = new TechEmployeeEmployment()
                    {
                        EmployeeRegisterId = empRegId,
                        Institute = Single_EmpInst[i],
                        Designation = Single_Designation[i],
                        Expertise = Single_Expertise[i],
                        StartDate = DateTime.Parse(Single_EmpSdate[i]),
                        EndDate = DateTime.Parse(Single_EmpEdate[i]),
                        TotalYears = Single_empTotal[i]
                    };
                    dc.TechEmployeeEmployments.Add(emps);
                    dc.SaveChanges();
                }
            }
            string[] Single_CourseName = CourseName.Split(',');
            string[] Single_CertificateName = CertificateName.Split(',');
            string[] Single_ProfessionalId = ProfessionalId.Split(',');

            for (int i = 0; i < count2; i++)
            {
                if (Single_CourseName[i] != "" && Single_CertificateName[i] != "")
                {
                    HttpPostedFileBase certu = CertificationFile[i];
                    if (certu != null)
                    {
                        if (Convert.ToInt16(Single_ProfessionalId[i])!=0 )
                        {
                            int exp_id = Convert.ToInt16(Single_ProfessionalId[i]);
                            var updateEmployeeEducation = dc.TechEmployeeProfessionals.Where(q => q.EmployeeRegisterId.Value.Equals(empRegId) && q.TechEmployeeProfessionalId.Equals(exp_id)).First();
                            updateEmployeeEducation.Cours = Single_CourseName[i];
                            updateEmployeeEducation.CertificateName = Single_CertificateName[i];
                            dc.SaveChanges();
                        }
                        else
                        {
                            string Certify = Path.GetFileName(CertificationFile[i].FileName) + Path.GetExtension(CertificationFile[i].FileName);
                            HttpPostedFileBase file = CertificationFile[i];
                            name = "CourseFile(" + (i + 1) + ")";
                            FileSzie = 512000;
                            var courseCertificate1 = fh.CheckEmployeeFile(file, Empid, name, FileSzie, foldername);
                            if (courseCertificate1.FileSizeId == 98)
                            {
                                string ErrorMessage = "CourseFile certificate file size exceeds,whithin 512kb";
                                return ErrorMessage;
                            }
                            else if (courseCertificate1.FileTypeId == 99)
                            {
                                string ErrorMessage = "CourseFile certificate file format not supported";
                                return ErrorMessage;
                            }
                            else if (courseCertificate1.FileSizeId != 98 && courseCertificate1.FileTypeId != 99 && courseCertificate1.filename != "")
                            {
                                courseCertificate = courseCertificate1.filename;
                                TechEmployeeProfessional pro = new TechEmployeeProfessional()
                                {
                                    EmployeeRegisterId = empRegId,
                                    Cours = Single_CourseName[i],
                                    CertificateName = Single_CertificateName[i],
                                    CertificateFile = courseCertificate
                                };
                                dc.TechEmployeeProfessionals.Add(pro);
                                dc.SaveChanges();
                            }

                        }

                    }
                    else
                    {

                        if (Single_ProfessionalId[i] != "")
                        {
                            int exp_id = Convert.ToInt16(Single_ProfessionalId[i]);
                            var updateEmployeeEducation = dc.TechEmployeeProfessionals.Where(q => q.EmployeeRegisterId.Value.Equals(empRegId) && q.TechEmployeeProfessionalId.Equals(exp_id)).First();
                            updateEmployeeEducation.Cours = Single_CourseName[i];
                            updateEmployeeEducation.CertificateName = Single_CertificateName[i];
                            dc.SaveChanges();
                        }
                    }
                }
            }

            var update1 = dc.TechEmployees.Where(q => q.EmployeeRegisterId.Equals(empRegId)).SingleOrDefault();
            if (update1.Password == null || update1.Password == "")
            {
                update1.Password = FormsAuthentication.HashPasswordForStoringInConfigFile(psw.ToString(), "SHA1");
                dc.SaveChanges();
                string message = "True";
                return message;
            }

            string Message = "Success";
            return Message;
        }


        internal void updateTechEmployeeProfessional(int EmpRegId, HttpPostedFileBase Resume, HttpPostedFileBase Certificate, int count2, string ProfessionalId, string CourseName, string CertificateName, HttpPostedFileBase[] CertificationFile)
        {
            string resumeFile = Path.GetFileName(Resume.FileName) + Path.GetExtension(Resume.FileName);
            string certificateFile = Path.GetFileName(Certificate.FileName) + Path.GetExtension(Certificate.FileName);
            var update = dc.TechEmployees.Where(q => q.EmployeeRegisterId.Equals(EmpRegId)).SingleOrDefault();
            update.ResumeFile = resumeFile;
            update.DegreeCertificateFile = certificateFile;
            dc.SaveChanges();
            string[] Single_CourseName = CourseName.Split(',');
            string[] Single_CertificateName = CertificateName.Split(',');
            string[] Single_ProfessionalId = ProfessionalId.Split(',');
            for (int i = 0; i < count2; i++)
            {
                string Certify = Path.GetFileName(CertificationFile[i].FileName) + Path.GetExtension(CertificationFile[i].FileName);
                int Pro_id = int.Parse(Single_ProfessionalId[i]);
                if (Pro_id != 0)
                {
                    var updateProfessional = dc.TechEmployeeProfessionals.Where(q => q.TechEmployeeProfessionalId.Equals(Pro_id) && q.EmployeeRegisterId.Value.Equals(EmpRegId)).First();
                    updateProfessional.Cours = Single_CourseName[i];
                    updateProfessional.CertificateName = Single_CertificateName[i];
                    updateProfessional.CertificateFile = Certify;
                    dc.SaveChanges();
                }
                else
                {
                    TechEmployeeProfessional pro = new TechEmployeeProfessional()
                    {
                        EmployeeRegisterId = EmpRegId,
                        Cours = Single_CourseName[i],
                        CertificateName = Single_CertificateName[i],
                        CertificateFile = Certify
                    };
                    dc.TechEmployeeProfessionals.Add(pro);
                    dc.SaveChanges();
                }
            }

        }

        internal bool checkEmail(int empRegId, string Email)
        {
            var ans = (from t1 in dc.TechEmployees
                       from t2 in dc.OtherEmployees
                       where t1.EmployeeRegisterId != empRegId && t1.Email == Email && t2.Email == Email
                       select t1).ToList();

            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal bool Add_checkEmail(string Email)
        {
            var ans = (from t1 in dc.TechEmployees
                       from t2 in dc.OtherEmployees
                       where t1.Email == Email || t2.Email == Email
                       select new
                       {
                           email1 = t1.Email,
                           email2 = t2.Email
                       }).ToList();

            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal void updateTechEmployee1(int empRegId, string Fname, string Mname, string Lname, DateTime DOB, string Gender, string Bloodgrp, string Religion, string Nationality, string Community, string Marital, int employeetype, int empdesignation, string txt_ClassList1, string txt_SubjectList1, string Contact, long Mobile, string Email, string Address1, string Address2, string City, string State, string Country, string PIN, string PermanentAddress1, string PermanentAddress2, string PermanentCity, string PermanentState, string PermanentCountry, string PermanentPIN, string EmgPerson, long EmgContact, string Relationship)
        {
            var update = (from t1 in dc.TechEmployees
                          where t1.EmployeeRegisterId == empRegId
                          select t1).FirstOrDefault();
            update.EmployeeName = Fname;
            update.MiddleName = Mname;
            update.LastName = Lname;
            update.DOB = DOB;
            update.Gender = Gender;
            update.MaritalStatus = Marital;
            update.BloodGroup = Bloodgrp;
            update.Community = Community;
            update.Religion = Religion;
            update.Nationality = Nationality;
            update.EmployeeTypeId = employeetype;
            update.EmployeeDesignationId = empdesignation;
            update.Phone = Contact;
            update.Mobile = Mobile;
            update.Email = Email;
            update.AddressLine1 = Address1;
            update.AddressLine2 = Address2;
            update.City = City;
            update.State = State;
            update.Country = Country;
            update.ZipCode = PIN;
            update.PermanentAddress1 = PermanentAddress1;
            update.PermanentAddress2 = PermanentAddress2;
            update.PermanentCity = PermanentCity;
            update.PermanentState = PermanentState;
            update.PermanentCountry = PermanentCountry;
            update.PermanentZipCode = PermanentPIN;
            update.EmergencyContactPerson = EmgPerson;
            update.EmergencyContactNumber = EmgContact;
            update.EmergencyContactpersonRelationship = Relationship;
            dc.SaveChanges();

            string[] Class = txt_ClassList1.Split(',');
            string[] Subject = txt_SubjectList1.Split(',');
            for (int i = 0; i < Class.Length; i++)
            {
                if (Class[i] != "")
                {
                    int cid = int.Parse(Class[i]);
                    var check = dc.TechEmployeeClassSubjects.Where(q => q.EmployeeRegisterId == empRegId && q.ClassId == cid).FirstOrDefault();
                    if (check == null)
                    {
                        TechEmployeeClassSubject classSub = new TechEmployeeClassSubject()
                        {
                            ClassId = int.Parse(Class[i]),
                            EmployeeRegisterId = empRegId,
                            Status=true
                        };
                        dc.TechEmployeeClassSubjects.Add(classSub);
                        dc.SaveChanges();
                    }
                }
            }
            var Existclasslist = dc.TechEmployeeClassSubjects.Where(q => q.EmployeeRegisterId == empRegId && q.ClassId != null).ToList();
            foreach (var v in Existclasslist)
            {

                bool ch = false;
                for (int i = 0; i < Class.Length; i++)
                {
                    if (Class[i] != "")
                    {
                        if (v.ClassId == Convert.ToInt16(Class[i]))
                        {
                            ch = true;
                        }

                    }
                }
                if (ch == false)
                {
                    var check = dc.TechEmployeeClassSubjects.Where(q => q.EmployeeRegisterId == empRegId && q.ClassId == v.ClassId).FirstOrDefault();
                    if (check != null)
                    {
                        dc.TechEmployeeClassSubjects.Remove(check);
                        dc.SaveChanges();
                    }
                }
            }

            for (int i = 0; i < Subject.Length; i++)
            {
                if (Subject[i] != "")
                {
                    int sid = int.Parse(Subject[i]);
                    var check = dc.TechEmployeeClassSubjects.Where(q => q.EmployeeRegisterId == empRegId && q.SubjectId == sid).FirstOrDefault();
                    if (check == null)
                    {
                        TechEmployeeClassSubject classSub = new TechEmployeeClassSubject()
                        {
                            EmployeeRegisterId = empRegId,
                            SubjectId = int.Parse(Subject[i]),
                             Status=true
                        };
                        dc.TechEmployeeClassSubjects.Add(classSub);
                        dc.SaveChanges();
                    }
                }
            }
            var Existsubjectlist = dc.TechEmployeeClassSubjects.Where(q => q.EmployeeRegisterId == empRegId && q.SubjectId!= null).ToList();
            foreach (var v in Existsubjectlist)
            {
                bool ch = false;
                for (int i = 0; i < Subject.Length; i++)
                {
                    if (Subject[i] != "")
                    {
                        if (v.SubjectId == Convert.ToInt16(Subject[i]))
                        {
                            ch = true;
                        }
                    }
                }
                if (ch == false)
                {
                    var check = dc.TechEmployeeClassSubjects.Where(q => q.EmployeeRegisterId == empRegId && q.SubjectId == v.SubjectId).FirstOrDefault();
                    if (check != null)
                    {
                        dc.TechEmployeeClassSubjects.Remove(check);
                        dc.SaveChanges();
                    }
                }
            }
        }

        internal void updateTechEmployee3(int EmpRegId, DateTime txt_doj, int txt_Salary, string[] ClassList, string[] SubjectList, string txt_AcNo, string txt_AcName, string txt_BName)
        {
            var update = dc.TechEmployees.Where(q => q.EmployeeRegisterId.Equals(EmpRegId)).SingleOrDefault();
            update.DOJ = txt_doj;
            update.Salary = txt_Salary;
            update.AccountNo = txt_AcNo;
            update.AccountName = txt_AcName;
            update.BankName = txt_BName;

            dc.SaveChanges();

            var delete = dc.TechEmployeeClassSubjects.Where(q => q.EmployeeRegisterId.Value.Equals(EmpRegId)).ToList();
            foreach (var d in delete)
            {
                dc.TechEmployeeClassSubjects.Remove(d);
                dc.SaveChanges();
            }

            for (int i = 0; i < ClassList.Length; i++)
            {
                int cid = int.Parse(ClassList[i]);

                TechEmployeeClassSubject classSub = new TechEmployeeClassSubject()
                {
                    ClassId = cid,
                    EmployeeRegisterId = EmpRegId
                };
                dc.TechEmployeeClassSubjects.Add(classSub);
                dc.SaveChanges();
            }

            for (int i = 0; i < SubjectList.Length; i++)
            {
                int sid = int.Parse(SubjectList[i]);
                TechEmployeeClassSubject classSub = new TechEmployeeClassSubject()
                {
                    EmployeeRegisterId = EmpRegId,
                    SubjectId = sid
                };
                dc.TechEmployeeClassSubjects.Add(classSub);
                dc.SaveChanges();
            }

        }

        public List<EmploeeListByType> getEmploeeListByEmployeeTypeId(int typeId)
        {
            var list = (from a in dc.TechEmployees
                        where a.EmployeeTypeId == typeId
                        select new
                        {
                            EmpRegId = a.EmployeeRegisterId,
                            EmpId = a.EmployeeId,
                            EmpName = a.EmployeeName + " " + a.LastName
                        }).ToList().Select(a => new EmploeeListByType
                            {
                                EmployeeId = a.EmpId,
                                EmployeeRegId = QSCrypt.Encrypt(a.EmpRegId.ToString()),
                                Name = a.EmpName
                            }).ToList();
            return list;
        }
        public List<EmailClassStudentList> getFacultyEmailList(int yearId, int empTypeId)
        {
            var List = (from a in dc.AcademicYears
                        from b in dc.EmployeeTypes
                        from c in dc.TechEmployees
                        where b.EmployeeTypeId == c.EmployeeTypeId &&
                        a.AcademicYearId == yearId &&
                        b.EmployeeTypeId == empTypeId
                        select new EmailClassStudentList
                        {
                            Email = c.Email,
                            MobileNum = c.Mobile
                        }).ToList();
            return List;
        }

        public List<EmailClassStudentList> OtherEmployeeEmailList(int yearId, int empTypeId)
        {
            var List = (from a in dc.AcademicYears
                        from b in dc.EmployeeTypes
                        from c in dc.OtherEmployees
                        where b.EmployeeTypeId == c.EmployeeTypeId &&
                        a.AcademicYearId == yearId &&
                        b.EmployeeTypeId == empTypeId
                        select new EmailClassStudentList
                        {
                            Email = c.Email,
                            MobileNum = c.Mobile
                        }).ToList();
            return List;
        }

        public EmailClassStudentList getFacultyEmail(int YearId, int emptypeId, int eid)
        {
            var List = (from a in dc.AcademicYears
                        from b in dc.EmployeeTypes
                        from c in dc.TechEmployees
                        where b.EmployeeTypeId == c.EmployeeTypeId &&
                        a.AcademicYearId == YearId &&
                        b.EmployeeTypeId == emptypeId &&
                        c.EmployeeRegisterId == eid
                        select new EmailClassStudentList
                        {
                            Email = c.Email,
                            MobileNum = c.Mobile
                        }).FirstOrDefault();
            return List;
        }

        public EmailClassStudentList OtherEmployeeEmail(int YearId, int emptypeId, int eid)
        {
            var List = (from a in dc.AcademicYears
                        from b in dc.EmployeeTypes
                        from c in dc.OtherEmployees
                        where b.EmployeeTypeId == c.EmployeeTypeId &&
                        a.AcademicYearId == YearId &&
                        b.EmployeeTypeId == emptypeId &&
                        c.EmployeeRegisterId == eid
                        select new EmailClassStudentList
                        {
                            Email = c.Email,
                            MobileNum = c.Mobile
                        }).FirstOrDefault();
            return List;
        }

        //jegadeesh
        public IEnumerable GetTechEmployee()
        {
            var ans = (from a in dc.TechEmployees
                       select new { EmployeeRegisterId = a.EmployeeRegisterId, EmployeeName = a.EmployeeName + " (" + a.EmployeeId + ")" }).ToList();// dc.TechEmployees.Select(q => q);
            return ans;
        }
        public IEnumerable GetParticularTechEmployeeName(int id)
        {
            var ans = dc.TechEmployees.Where(q => q.EmployeeRegisterId.Equals(id)).FirstOrDefault().EmployeeName;
            return ans;
        }

        public List<Eprofile> SearchTechEmployeeProfile(int id)
        {
            var q1 = (from a in dc.TechEmployees
                      where a.EmployeeRegisterId == id
                      select new Eprofile
                      {
                          EmployeeRegisterId = a.EmployeeRegisterId,
                          EmployeeId = a.EmployeeId,
                          EmployeeName = a.EmployeeName + " " + a.LastName,
                          Gender = a.Gender,
                          BloodGroup = a.BloodGroup,
                          Community = a.Community,
                          Email = a.Email,
                          Religion = a.Religion,
                          Nationality = a.Nationality,
                          LandlineNo = a.Contact,
                          Contact = a.Mobile,
                          Addressline1 = a.AddressLine1 + " " + a.AddressLine2,
                          State = a.State,
                          //DOB = a.DOB.Value,
                          //DOL = a.DOL.Value,
                          //DOR = a.DOR.Value,
                          //DOJ = a.DOJ.Value,
                          DateOfBirth = SqlFunctions.DateName("day", a.DOB).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOB.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOB),
                          DateOfLeaving = SqlFunctions.DateName("day", a.DOL).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOL.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOL),
                          DateOfRegister = SqlFunctions.DateName("day", a.DOR).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOR.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOR),
                          DateOfJoin = SqlFunctions.DateName("day", a.DOJ).Trim() + "/" + SqlFunctions.StringConvert((double)a.DOJ.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.DOJ),
                          Qualification = a.Qualification,
                          EmergencyContactPerson = a.EmergencyContactPerson,
                          EmergencyContactNumber = a.EmergencyContactNumber,
                          EmployeeStatus = a.EmployeeStatus,
                          Experience = a.Experience,
                          Expertise = a.Expertise,
                          ImgFile = a.ImgFile,
                          City = a.City
                      }).ToList();
                      //.Select(q=>new Eprofile{
                      //    EmployeeRegisterId = q.EmployeeRegisterId,
                      //    EmployeeId =q.EmployeeId,
                      //    EmployeeName = q.EmployeeName,
                      //    Gender = q.Gender,
                      //    BloodGroup = q.BloodGroup,
                      //    Community = q.Community,
                      //    Email = q.Email,
                      //    Religion = q.Religion,
                      //    Nationality = q.Nationality,
                      //    LandlineNo = q.LandlineNo,
                      //    Contact = q.Contact,
                      //    Addressline1 = q.Addressline1,
                      //    State = q.State,
                      //    DateOfBirth=q.DOB.ToString("MMM dd, yyyy"),
                      //    DateOfLeaving=q.DOL.ToString("MMM dd, yyyy"),
                      //    DateOfRegister=q.DOR.ToString("MMM dd, yyyy"),
                      //    DateOfJoin=q.DOJ.ToString("MMM dd, yyyy"),
                      //    Qualification = q.Qualification,
                      //    EmergencyContactPerson = q.EmergencyContactPerson,
                      //    EmergencyContactNumber = q.EmergencyContactNumber,
                      //    EmployeeStatus = q.EmployeeStatus,
                      //    Experience = q.Experience,
                      //    Expertise = q.Expertise,
                      //    ImgFile = q.ImgFile,
                      //    City = q.City
                      //}).ToList();
            return q1;

        }
        public IEnumerable GetTechEmployeename(int yid)
        {
            var ans = (from a in dc.TechEmployees
                       from b in dc.AssignFacultyToSubjects
                       where b.AcademicYearId == yid && b.EmployeeRegisterId == a.EmployeeRegisterId
                       select new { EmployeeRegisterId = a.EmployeeRegisterId, EmployeeName = a.EmployeeName + " (" + a.EmployeeId + ")" }).Distinct().ToList();
            return ans;
        }
        public IEnumerable GetTechEmployeenameId(int eid)
        {
            var ans = dc.TechEmployees.Where(q => q.EmployeeRegisterId.Equals(eid));
            return ans;
        }
        public IEnumerable<Object> ShowTechFaculties()
        {
            var ClassList = (from a in dc.TechEmployees
                             select new
                             {
                                 FacultyName = a.EmployeeName,
                                 FacultyId = a.EmployeeRegisterId
                             }).ToList().AsEnumerable();
            return ClassList;
        }
        public IEnumerable GetjsonTechEmployee()
        {
            var ans = dc.TechEmployees.Select(q => q).Select(s => new { empid = s.EmployeeRegisterId, empname = s.EmployeeName + " " + "(" + s.EmployeeId + ")" }).ToList();
            return ans;
        }
        public IEnumerable GetjsonAyidTechEmployee(int yid)
        {
            var ans = (from a in dc.TechEmployees
                       from b in dc.AssignFacultyToSubjects
                       where b.AcademicYearId == yid && b.EmployeeRegisterId == a.EmployeeRegisterId
                       select new { empid = a.EmployeeRegisterId, empname = a.EmployeeName + " " + "(" + a.EmployeeId + ")" }).Distinct();
            return ans;
        }
        public IEnumerable GetjsonTechEmployeeid(int eid)
        {
            var ans = (from a in dc.EmployeeDesignations
                       from b in dc.TechEmployees
                       where a.EmployeeDesignationId == eid && b.EmployeeDesignationId == a.EmployeeDesignationId
                       select new
                       {
                           empRid = b.EmployeeRegisterId,
                           empid = b.EmployeeId,
                           EmpName=b.EmployeeName+"("+b.EmployeeId+")"
                       }).ToList();
            return ans;
        }

        internal List<TechEmployee> getEmployeeByEmployeeRegisterId(int empRegId)
        {
            var ans = dc.TechEmployees.Where(q => q.EmployeeRegisterId.Equals(empRegId)).ToList();
            return ans;
        }

        internal void UpdatePassword(int empRegId, string psw)
        {
            var update = dc.TechEmployees.Where(q => q.EmployeeRegisterId.Equals(empRegId)).SingleOrDefault();
            if (update != null)
            {
                update.Password = FormsAuthentication.HashPasswordForStoringInConfigFile(psw.ToString(), "SHA1");
                dc.SaveChanges();
            }
        }
        public Eprofile GetlibraryEmployee(string EmployeeId)
        {
            var ans = (from a in dc.TechEmployees
                       from b in dc.EmployeeTypes
                       from c in dc.EmployeeDesignations
                       where a.EmployeeId == EmployeeId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId
                       select new Eprofile
                       {
                           EmployeeRegisterId = a.EmployeeRegisterId,
                           EmployeeName = a.EmployeeName + " " + a.LastName,
                           EmployeeId = a.EmployeeId,
                           EmployeeType = b.EmployeeTypes,
                           EmployeeDesignation = c.Designation,
                           ImgFile = a.ImgFile

                       }).FirstOrDefault();
            return ans;
        }
        public Eprofile GethostelEmployee(string EmployeeId)
        {
            var ans = (from a in dc.TechEmployees
                       from b in dc.EmployeeTypes
                       from c in dc.EmployeeDesignations

                       where a.EmployeeId == EmployeeId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId && a.EmployeeStatus == true
                       select new Eprofile
                       {
                           EmployeeRegisterId = a.EmployeeRegisterId,
                           EmployeeName = a.EmployeeName + " " + a.LastName,
                           EmployeeId = a.EmployeeId,
                           EmployeeType = b.EmployeeTypes,
                           EmployeeDesignation = c.Designation,
                           ImgFile = a.ImgFile,
                           Gender = a.Gender,
                           Etype = "Tech",
                           Salary = a.Salary

                       }).FirstOrDefault();
            return ans;
        }
        public Eprofile GethostelEditEmployee(int EmployeeRegId)
        {
            var ans = (from a in dc.TechEmployees
                       from b in dc.EmployeeTypes
                       from c in dc.EmployeeDesignations

                       where a.EmployeeRegisterId == EmployeeRegId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId && a.EmployeeStatus == true
                       select new Eprofile
                       {
                           EmployeeRegisterId = a.EmployeeRegisterId,
                           EmployeeName = a.EmployeeName + " " + a.LastName,
                           EmployeeId = a.EmployeeId,
                           EmployeeType = b.EmployeeTypes,
                           EmployeeDesignation = c.Designation,
                           ImgFile = a.ImgFile,
                           Gender = a.Gender,
                           Etype = "Tech"

                       }).FirstOrDefault();
            return ans;
        }
        public Eprofile GetEmployeeForSalaryConfig(string EmployeeId)
        {
            var ans = (from a in dc.TechEmployees
                       from b in dc.EmployeeTypes
                       from c in dc.EmployeeDesignations

                       where a.EmployeeId == EmployeeId && b.EmployeeTypeId == a.EmployeeTypeId && c.EmployeeDesignationId == a.EmployeeDesignationId && a.EmployeeStatus == true && !(from d in dc.EmployeeSalaryConfigurations

                                                                                                                                                                                       where d.EmployeeRegisterId == a.EmployeeRegisterId
                                                                                                                                                                                       select d.EmployeeRegisterId).Contains(a.EmployeeRegisterId)
                       select new Eprofile
                       {
                           EmployeeRegisterId = a.EmployeeRegisterId,
                           EmployeeName = a.EmployeeName + " " + a.LastName,
                           EmployeeId = a.EmployeeId,
                           EmployeeType = b.EmployeeTypes,
                           EmployeeDesignation = c.Designation,
                           ImgFile = a.ImgFile,
                           Gender = a.Gender,
                           Etype = "Tech",
                           Salary = a.Salary

                       }).FirstOrDefault();
            return ans;
        }

        public AssignFacultyToSubject checkemproll(int empid, int yearid)
        {
            var roll = (from a in dc.AssignFacultyToSubjects where a.EmployeeRegisterId == empid && a.AcademicYearId == yearid select a).FirstOrDefault();
            return roll;
        }

        public AssignClassTeacher checkempclass(int empid, int yearid)
        {
            var roll = (from a in dc.AssignClassTeachers where a.EmployeeRegisterId == empid && a.AcademicYearId == yearid select a).FirstOrDefault();
            return roll;
        }

        public AssignFacultyToSubject checkemproll(int empid, int yearid, int subjectid)
        {
            var roll = (from a in dc.AssignFacultyToSubjects where a.EmployeeRegisterId == empid && a.AcademicYearId == yearid && a.SubjectId == subjectid select a).FirstOrDefault();
            return roll;
        }

        public AssignClassTeacher checkempclass(int empid, int yearid, int subjectid)
        {
            var roll = (from a in dc.AssignClassTeachers where a.EmployeeRegisterId == empid && a.AcademicYearId == yearid && a.SubjectId == subjectid select a).FirstOrDefault();
            return roll;
        }


        public AssignClassTeacher checkemphandledclass(int empid, int yearid, int classid)
        {
            var roll = (from a in dc.AssignClassTeachers where a.EmployeeRegisterId == empid && a.AcademicYearId == yearid &&a.ClassId==classid select a).FirstOrDefault();
            return roll;
        }
        public AssignFacultyToSubject checkempsubjectclass(int empid, int yearid, int classid)
        {
            var roll = (from a in dc.AssignFacultyToSubjects where a.EmployeeRegisterId == empid && a.AcademicYearId == yearid && a.ClassId == classid select a).FirstOrDefault();
            return roll;
        }
        
        
        public MapRoleUser checkemployeeerror(int empid)
        {
            var roll = (from a in dc.MapRoleUsers where a.EmployeeRegisterId == empid && a.status==true select a).FirstOrDefault();
            return roll;
        }


        public bool Checkalreadysalaryconfigtoemployee(string EmployeeId)
        {
            var ans = (from a in dc.TechEmployees

                       from d in dc.EmployeeSalaryConfigurations
                       where a.EmployeeId == EmployeeId && d.EmployeeRegisterId == a.EmployeeRegisterId && a.EmployeeStatus == true && d.Status == true
                       select d).FirstOrDefault();

            if (ans != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool Checkalreadyhostelroomtoemployee(string EmployeeId)
        {
            var ans = (from a in dc.TechEmployees

                       from d in dc.RoomAllotments
                       where a.EmployeeId == EmployeeId && d.EmployeeregisterId == a.EmployeeRegisterId && a.EmployeeStatus == true && d.status == true
                       select d).FirstOrDefault();

            if (ans != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetTechEmployeeId(string email)
        {
            var record = dc.TechEmployees.Where(q => q.Email == email).FirstOrDefault();
            if(record!=null)
            {
                return record.EmployeeRegisterId;
            }
            else
            {
                return 0;
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}