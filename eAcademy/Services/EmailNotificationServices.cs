﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Services
{
    public class EmailNotificationServices
    {
        EacademyEntities dc = new EacademyEntities();
        public void AddEmailNotification(string PlaceName, string FromEmailId, string ToEmailId, string SendingStatus, string UserName, string UserId)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var date1 = date.Date;
            EmailNotification Email = new EmailNotification()
            {
                Place=PlaceName,
                MailSentDate = date,
                SentDate=date1,
                FromEmailId=   FromEmailId,
                ToEmailId = ToEmailId,
                SendingStatus = SendingStatus,
                UserName = UserName,
                UserId=UserId
            };           
            dc.EmailNotifications.Add(Email);
            dc.SaveChanges();
        }
    }
}