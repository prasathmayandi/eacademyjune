﻿using eAcademy.DataModel;
using System;
using System.Linq;

namespace eAcademy.Services
{
    public class PreAdmissionGuardianRegisterServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();
        public int AddGuardiandetails(string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email,long? income,string gurelation,string gender)
        {
            PreAdmissionGuardianRegister guardian = new PreAdmissionGuardianRegister()
            {
                GuardianFirstname = firstname,
                GuardianLastName = lastname,
                GuardianDOB = dob,
                GuardianQualification = qual,
                GuardianOccupation = occu,
                GuardianMobileNo = mobile,
                GuardianEmail = email,
                GuardianIncome=income,
                GuRelationshipToChild=gurelation,
                GuardianGender=gender
            };
            dc.PreAdmissionGuardianRegisters.Add(guardian);
            dc.SaveChanges();
            return guardian.GuardianAdmissionId;
        }

        public int UpdateGuardianDetails1(int? guardianid, string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email, long? income, string gurelation, string gender) //, 
        {
            var get = dc.PreAdmissionGuardianRegisters.Where(q => q.GuardianAdmissionId == guardianid).FirstOrDefault();

            if (get != null)
            {
                get.GuardianFirstname = firstname;
                get.GuardianLastName = lastname;
                get.GuardianDOB = dob;
                get.GuardianEmail = email;
                get.GuardianQualification = qual;
                get.GuardianOccupation = occu;
                get.GuardianMobileNo = mobile;
                get.GuardianIncome = income;
                get.GuRelationshipToChild = gurelation;
                get.GuardianGender = gender;
                dc.SaveChanges();
            }
            return get.GuardianAdmissionId;
        }

        public int UpdateGuardianDetails(int? guardianid, string qual, string occu, long? mobile,string GEmail,long? income, string gurelation, string gender) //, 
        {
            var get = dc.PreAdmissionGuardianRegisters.Where(q => q.GuardianAdmissionId == guardianid).FirstOrDefault();

            if (get != null)
            {
                get.GuardianEmail = GEmail;
                get.GuardianQualification = qual;
                get.GuardianOccupation = occu;
                get.GuardianMobileNo = mobile;
                get.GuardianIncome = income;
                get.GuRelationshipToChild = gurelation;
                get.GuardianGender = gender;
                dc.SaveChanges();
            }
            return get.GuardianAdmissionId;
        }

        public int CheckGuardianExit(int primaryuserid, int Newstudentid, string firstname, string lastname, DateTime? dob, string qual, string occu, long? mobile, string email, long? guincome, string gurelation, string gender)
        {
            int Guardianid = 0;
            var GetMother = (from a in dc.PreAdmissionTransactions
                             from b in dc.PreAdmissionGuardianRegisters
                             where a.PrimaryUserAdmissionId == primaryuserid && b.GuardianAdmissionId == a.GuardianAdmissionId && b.GuardianFirstname == firstname && b.GuardianLastName == lastname && b.GuardianDOB == dob
                             select b).FirstOrDefault();
            if (GetMother != null)
            {
                Guardianid = GetMother.GuardianAdmissionId;
                UpdateGuardianDetails(Guardianid, qual, occu, mobile,email, guincome, gurelation, gender);
            }
            else
            {
                Guardianid = AddGuardiandetails(firstname, lastname, dob, qual, occu, mobile, email,guincome, gurelation,gender);
            }
            return Guardianid;
        }

        public PreAdmissionGuardianRegister GetEnrolementGuardiandetails(int StudentAdmissionId)
        {
            var ans = (from a in dc.PreAdmissionStudentRegisters
                       from b in dc.PreAdmissionTransactions
                       from c in dc.PreAdmissionGuardianRegisters
                       where a.StudentAdmissionId == StudentAdmissionId && b.StudentAdmissionId == a.StudentAdmissionId && c.GuardianAdmissionId == b.GuardianAdmissionId
                       select c).FirstOrDefault();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}