﻿using eAcademy.DataModel;
using System;
using System.Web;
using System.Web.Mvc;

namespace eAcademy.Services
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class Parent_CheckSessionAttribute : ActionFilterAttribute
    {

        Data db = new Data();
        EacademyEntities ee = new EacademyEntities();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower();
            if (!controllerName.Contains("home"))
            {
                HttpSessionStateBase session = filterContext.HttpContext.Session;
                var user = session["ParentRegId_SessionAttribute"];
                if (Convert.ToString(session["PrimaryUser"]) == "Guardian")
                {
                    if (user != null)
                    {
                        if (session["GuardianStudentName"] == null)
                        {
                            //send them off to the Guardian student credential page
                            var url = new UrlHelper(filterContext.RequestContext);
                            var StudentCredentialUrl = url.Content("~/Guardian/StudentCredential");
                            filterContext.Result = new RedirectResult(StudentCredentialUrl);
                            return;
                        }
                    }
                }
                if (((user == null) && (!session.IsNewSession)) || (session.IsNewSession) || (null == filterContext.HttpContext.Session))
                {
                    //send them off to the login page
                    var url = new UrlHelper(filterContext.RequestContext);
                    var loginUrl = url.Content("~/Home/StudentLogout");
                    filterContext.Result = new RedirectResult(loginUrl);
                    return;
                }
             
            }
        }
    }
}