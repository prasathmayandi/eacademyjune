﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class TechEmployeeClassSubjectServices:IDisposable
    {

        EacademyEntities dc = new EacademyEntities();

        internal List<M_TechEmployeeClassSubject> GetEmployeeClass(int empId)
        {
            var classes = (from t1 in dc.TechEmployeeClassSubjects
                           where t1.EmployeeRegisterId == empId && t1.ClassId != null
                           select new M_TechEmployeeClassSubject { Classid = t1.ClassId }).ToList();
            return classes;
        }
        
        internal List<M_TechEmployeeClassSubject> GetEmployeeSubject(int empId)
        {
            var classes = (from t1 in dc.TechEmployeeClassSubjects
                           where t1.EmployeeRegisterId == empId && t1.SubjectId != null
                           select new M_TechEmployeeClassSubject {Subjectid = t1.SubjectId }).ToList();
            return classes;
        }

        public List<M_TechEmployeeClassSubject> GetEmployeeClassSubject(int Eid)
        {
            var res = (from a in dc.TechEmployeeClassSubjects
                       from b in dc.Classes
                       from c in dc.Subjects
                       where a.EmployeeRegisterId == Eid && b.ClassId == a.ClassId && c.SubjectId == a.SubjectId && a.Status==true
                       select new M_TechEmployeeClassSubject
                       {
                           Classid = a.ClassId,
                           ClassName = b.ClassType,
                           Subjectid = a.SubjectId,
                           SubjectName = c.SubjectName

                       }).ToList();
            return res;
        }

        public IEnumerable<Object> ShowClassFaculties(int classId)
        {
            var FaculityList = (from b in dc.TechEmployees
                                from d in dc.TechEmployeeClassSubjects
                                where d.EmployeeRegisterId == b.EmployeeRegisterId &&
                                d.Status == true &&
                                b.EmployeeStatus == true &&
                                d.ClassId == classId
                                select new
                                {
                                    FacultyName = b.EmployeeName,
                                    FacultyId = b.EmployeeRegisterId
                                }).Distinct().ToList().AsEnumerable();
            return FaculityList;
        }

        public IEnumerable<Object> getFacultyClassSub(int EmployeeId)
        {
            var subList = (from b in dc.TechEmployees
                                from d in dc.TechEmployeeClassSubjects
                                from e in dc.Subjects
                                where d.EmployeeRegisterId == b.EmployeeRegisterId &&
                                d.SubjectId == e.SubjectId &&
                                d.Status == true &&
                                d.EmployeeRegisterId == EmployeeId
                                select new
                                {
                                    SubjectName = e.SubjectName,
                                }).Distinct().ToList().AsEnumerable();
            return subList;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

      
    }
}