﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class ApplySeparationServices:IDisposable
    {
    
        EacademyEntities db = new EacademyEntities();

        public  List<ApplySeparation> getSeparationList(int year)
        {
            var list = (from a in db.ApplySeparations where a.AcademicYearId == year select a).ToList();
            return list;
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}