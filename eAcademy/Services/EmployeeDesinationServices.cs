﻿using eAcademy.DataModel;
using System;
using System.Collections;
using System.Linq;

namespace eAcademy.Services
{
    public class EmployeeDesinationServices:IDisposable
    {

        EacademyEntities dc = new EacademyEntities();

        public IEnumerable GetEmployeeDesinationList(int EmployeeTypeid)
        {
            var ans = (from a in dc.EmployeeDesignations
                       where a.EmployeeTypeId == EmployeeTypeid
                       select new { DesignationId = a.EmployeeDesignationId,DesignationName=a.Designation}).Distinct().ToList();
            return ans;
        }
        public IEnumerable GetEmployeeDesignation()
        {
            var ans = dc.EmployeeDesignations.Select(q => q);
            return ans;
        }

        public IEnumerable EmployeeDesignation()
        {
            var result = dc.EmployeeDesignations.Select(a => new
            {
                DesignationId = a.EmployeeDesignationId,
                Designation = a.Designation
            }).ToList();
            return result;
        }

        public IEnumerable GetparticularEmployeeDesignation(int DesignationId)
        {
            var result = dc.EmployeeDesignations.Where(q => q.EmployeeDesignationId == DesignationId).FirstOrDefault().Designation;
            return result;
        }

        public int GetEmployeeDesignationId(string EmpDesign)
        {
            var record = dc.EmployeeDesignations.Where(q => q.Designation == EmpDesign).FirstOrDefault();
            if (record != null)
            {
                return record.EmployeeDesignationId;
            }
            else
            {
                return 0;
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}