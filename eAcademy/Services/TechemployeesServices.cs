﻿using eAcademy.DataModel;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class TechemployeesServices : IDisposable
    {
        EacademyEntities ee = new EacademyEntities();

        public IEnumerable<Object> ShowTechFaculties()
        {
            var List = (from a in ee.TechEmployees
                        where a.EmployeeStatus == true 
                             select new
                             {
                                 FacultyName = a.EmployeeName,
                                 FacultyId = a.EmployeeRegisterId
                             }).ToList().AsEnumerable();
            return List;
        }
        public List<LeaveRequestStatus> getFacultyLeaveRequestAll(DateTime fdate, DateTime toDate)
        {
            var list = (from a in ee.FacultyLeaveRequests
                        from b in ee.TechEmployees
                        where a.FromDate >= fdate.Date &&
                            a.FromDate <= toDate.Date &&
                            a.EmployeeRegisterId == b.EmployeeRegisterId
                        select new LeaveRequestStatus
                        {
                            RequestId = a.RequestId,
                            ClassIncharge = b.EmployeeName,
                            From_Date = SqlFunctions.DateName("day", a.FromDate).Trim() + " " + SqlFunctions.DateName("month", a.FromDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.FromDate),
                            To_Date = SqlFunctions.DateName("day", a.ToDate).Trim() + " " + SqlFunctions.DateName("month", a.ToDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.ToDate),
                            NumberOfDays = a.NumberOfDays,
                            LeaveReason = a.LeaveReason,
                            Status = a.Status
                        }).ToList();
            return list;
        }
        public List<LeaveRequestStatus> getFacultyLeaveRequestList(DateTime fdate, DateTime toDate, string Status)
        {
            var list = (from a in ee.FacultyLeaveRequests
                        from b in ee.TechEmployees
                        where a.FromDate >= fdate &&
                            a.ToDate <= toDate &&
                            a.Status == Status &&
                            a.EmployeeRegisterId == b.EmployeeRegisterId
                        select new LeaveRequestStatus
                        {
                            RequestId = a.RequestId,
                            ClassIncharge = b.EmployeeName,
                            From_Date = SqlFunctions.DateName("day", a.FromDate).Trim() + " " + SqlFunctions.DateName("month", a.FromDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.FromDate),
                            To_Date = SqlFunctions.DateName("day", a.ToDate).Trim() + " " + SqlFunctions.DateName("month", a.ToDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.ToDate),
                            NumberOfDays = a.NumberOfDays,
                            LeaveReason = a.LeaveReason,
                            Status = a.Status
                        }).ToList();
            return list;
        }



        public IEnumerable<Object> ShowAllTechFaculties(int featureId)
        {
            var FaculityList = (from a in ee.MapRoleUsers
                                from b in ee.TechEmployees
                                from c in ee.MapRoleFeatures
                                where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                                a.RoleId == c.RoleId &&
                                b.EmployeeStatus == true &&
                                c.FeatureId == featureId 
                                select new
                                {
                                    FacultyName = b.EmployeeName,
                                    FacultyId = b.EmployeeRegisterId
                                }).Distinct().ToList().AsEnumerable();

                    return FaculityList;
        }

        public IEnumerable<Object> ShowFaculties(int classId)
        {
            //var FaculityList = (from a in ee.MapRoleUsers
            //                    from b in ee.TechEmployees
            //                    from c in ee.MapRoleFeatures
            //                    where c.FeatureId == featureId &&
            //                    a.EmployeeRegisterId == b.EmployeeRegisterId &&
            //                    a.RoleId == c.RoleId
            //                    select new
            //                    {
            //                        FacultyName = b.EmployeeName,
            //                        FacultyId = b.EmployeeRegisterId
            //                    }).ToList().AsEnumerable();

            var FaculityList = (from b in ee.TechEmployees
                                from d in ee.TechEmployeeClassSubjects
                                where d.EmployeeRegisterId == b.EmployeeRegisterId &&
                                d.Status == true &&
                                b.EmployeeStatus == true &&
                                d.ClassId == classId
                                select new
                                {
                                    FacultyName = b.EmployeeName,
                                    FacultyId = b.EmployeeRegisterId
                                }).ToList().AsEnumerable();
            return FaculityList;
        }

        public TechEmployee getFacultyDetails(int employeeRegId)
        {
            var ans = (from a in ee.TechEmployees
                       where a.EmployeeRegisterId == employeeRegId
                       select a).FirstOrDefault();
            return ans;
        }

        public IEnumerable<Object> FacultyListToMarkAttendance(DateTime passDate)  
        {
            var list = (from a in ee.TechEmployees
                        where a.EmployeeStatus == true &&
                        a.DOJ <= passDate 
                        orderby a.EmployeeId
                        select new
                        {
                            employeeId = a.EmployeeId,
                            employeeName = a.EmployeeName,
                            empRegId = a.EmployeeRegisterId
                        }).OrderBy(x=>x.empRegId).ToList().Select(c => new FacultyListForAttendance
                        {
                            employeeId = c.employeeId,
                            employeeName = c.employeeName,
                            empRegId = QSCrypt.Encrypt(c.empRegId.ToString())
                        }).ToList();
            return list;
        }

        public int GetEmployeeRegId(string empid)
        {
            var list = ee.TechEmployees.Where(q => q.EmployeeId == empid).FirstOrDefault();
            if (list != null)
            {
                return list.EmployeeRegisterId;
            }
            else
            {
                var list2 = ee.OtherEmployees.Where(q => q.EmployeeId == empid).FirstOrDefault();
                if(list2!=null)
                {
                    return list2.EmployeeRegisterId;
                }
                return 1;
            }
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ee.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }  
}