﻿using eAcademy.DataModel;
using System;
using System.Web;
using System.Web.Mvc;

namespace eAcademy.Services
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class RegForm_CheckSessionOutAttribute:ActionFilterAttribute
    {
        EacademyEntities ee = new EacademyEntities();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            if (controllerName.Contains("OnlineRegister"))
            {
                HttpSessionStateBase session = filterContext.HttpContext.Session;
                var user = session["RegForm_SessionAttribute"];
                if (((user == null) && (!session.IsNewSession)) || (session.IsNewSession) || (null == filterContext.HttpContext.Session))
                {
                    //send them off to the login page
                    var url = new UrlHelper(filterContext.RequestContext);
                    var loginUrl = url.Content("~/OnlineRegister/Logout");
                    filterContext.Result = new RedirectResult(loginUrl);
                    return;
                }
            }
        }
    }
}