﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.Helpers;
using System.Collections.Generic;
using System.Linq;
using System;

namespace eAcademy.Services
{
    public class ActivityInchargeServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public ActivityInchargeList getActivityInchargByID(int aid)
        {
            var ans = ((from a in dc.ActivityIncharges
                         from b in dc.AcademicYears
                         from c in dc.TechEmployees
                         from d in dc.ExtraActivities
                         where a.ActivityInchargeId == aid &&
                         a.AcademicYearId == b.AcademicYearId &&
                         a.EmployeeRegisterId == c.EmployeeRegisterId &&
                         a.EmployeeId == c.EmployeeId &&
                         a.ActivityId == d.ActivityId
                         select new ActivityInchargeList { Activity = d.Activity, ActivityId = d.ActivityId, Emp = c.EmployeeName, EmpRegId = c.EmployeeRegisterId, EmpId = c.EmployeeId, AcYear = b.AcademicYear1, AcYearId = a.AcademicYearId, Note = a.Note, status=a.Status  })
                          .Union
                          (from a in dc.ActivityIncharges
                           from b in dc.AcademicYears
                           from c in dc.OtherEmployees
                           from d in dc.ExtraActivities
                           where a.ActivityInchargeId == aid &&
                           a.AcademicYearId == b.AcademicYearId &&
                           a.EmployeeRegisterId == c.EmployeeRegisterId &&
                           a.EmployeeId == c.EmployeeId &&
                           a.ActivityId == d.ActivityId
                           select new ActivityInchargeList { Activity = d.Activity, ActivityId = d.ActivityId, Emp = c.EmployeeName, EmpRegId = c.EmployeeRegisterId, EmpId = c.EmployeeId, AcYear = b.AcademicYear1, AcYearId = a.AcademicYearId, Note = a.Note, status = a.Status })).SingleOrDefault();
            return ans;
        }


        public void addActivityIncharge(int acid, int aid, int empRegId, string Note, string empid)
        {

            eAcademy.DataModel.ActivityIncharge act = new eAcademy.DataModel.ActivityIncharge()
            {
                AcademicYearId = acid,
                ActivityId = aid,
                EmployeeRegisterId = empRegId,
                Note = Note,
                EmployeeId = empid,
                Status=true
            };
            dc.ActivityIncharges.Add(act);
            dc.SaveChanges();
        }

        public List<eAcademy.Models.ActivityIncharge> getAcitityIncharge()
        {
            var list = ((from a in dc.ActivityIncharges
                         from b in dc.TechEmployees
                         from c in dc.AcademicYears
                         from d in dc.ExtraActivities
                         where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                               a.AcademicYearId == c.AcademicYearId &&
                               a.ActivityId == d.ActivityId &&
                               a.EmployeeId == b.EmployeeId
                         select new eAcademy.Models.ActivityIncharge
                         {
                             Fac_Incharge = b.EmployeeName,
                             Fac_AcYear = c.AcademicYear1,
                             Fac_AcYearId=c.AcademicYearId,
                             Activity = d.Activity,
                             EmpId = b.EmployeeId,
                             EmpContact = b.Mobile,
                             Note = a.Note,
                             ActivityInchargeId = a.ActivityInchargeId,
                             Status=a.Status
                         }).ToList().Select(q=>
                         
                         new eAcademy.Models.ActivityIncharge
                         {
                             Fac_Incharge = q.Fac_Incharge,
                             Fac_AcYear = q.Fac_AcYear,
                             Fac_AcYearId=q.Fac_AcYearId,
                             Activity = q.Activity,
                             EmpId = q.EmpId,
                             EmpContact = q.EmpContact,
                             Note = q.Note,
                             ActivityInchargeId = q.ActivityInchargeId,
                            Status=q.Status,
                            Description=q.Note,
                            AcademicYear=q.Fac_AcYear,
                            ActivityIncharges=q.Fac_Incharge,
                             EmployeeContactNo = q.EmpContact,
                            EmployeeId=q.EmpId,
                            AIID=QSCrypt.Encrypt(q.ActivityInchargeId.ToString())
                         })
                        
                    .Union(from a in dc.ActivityIncharges
                           from b in dc.OtherEmployees
                           from c in dc.AcademicYears
                           from d in dc.ExtraActivities
                           where a.EmployeeRegisterId == b.EmployeeRegisterId &&
                                 a.AcademicYearId == c.AcademicYearId &&
                                 a.ActivityId == d.ActivityId &&
                                 a.EmployeeId == b.EmployeeId
                           select new eAcademy.Models.ActivityIncharge
                           {
                               Fac_Incharge = b.EmployeeName,
                               Fac_AcYear = c.AcademicYear1,
                               Fac_AcYearId = c.AcademicYearId,
                               Activity = d.Activity,
                               EmpId = b.EmployeeId,
                               EmpContact = b.Contact,
                               Note = a.Note,
                               ActivityInchargeId = a.ActivityInchargeId,
                               Status=a.Status
                           }).ToList().Select(q=>
                         
                         new eAcademy.Models.ActivityIncharge
                         {
                             Fac_Incharge = q.Fac_Incharge,
                             Fac_AcYear = q.Fac_AcYear,
                             Fac_AcYearId=q.Fac_AcYearId,
                             Activity = q.Activity,
                             EmpId = q.EmpId,
                             EmpContact = q.EmpContact,
                             Note = q.Note,
                             Description = q.Note,
                             AcademicYear = q.Fac_AcYear,
                             ActivityIncharges = q.Fac_Incharge,
                             EmployeeContactNo = q.EmpContact,
                             ActivityInchargeId = q.ActivityInchargeId,
                             EmployeeId = q.EmpId,
                            Status=q.Status,
                            AIID=QSCrypt.Encrypt(q.ActivityInchargeId.ToString())
                         })).ToList();
            return list;
        }




        public bool checkActivityIncharge(int acid, int aid, string empid, int empRegId)
        {
            var ans = dc.ActivityIncharges.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ActivityId.Value.Equals(aid) && q.EmployeeId.Equals(empid) && q.EmployeeRegisterId.Value.Equals(empRegId)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void updateActivityIncharge(int activityId, int acid, int aid, string empid, int empRegId, string note, string Status)
        {
            var update = dc.ActivityIncharges.Where(q => q.ActivityInchargeId.Equals(activityId)).First();
            update.AcademicYearId = acid;
            update.ActivityId = aid;
            update.EmployeeId = empid;
            update.EmployeeRegisterId = empRegId;
            update.Note = note;

            if (Status == "Active")
            {
                update.Status = true;
            }

            else
            {
                update.Status = false;
            }
            dc.SaveChanges();
        }
        public void deleteActivityIncharge(int activityId)
        {
            var delete = dc.ActivityIncharges.Where(q => q.ActivityInchargeId.Equals(activityId)).First();
            dc.ActivityIncharges.Remove(delete);
            dc.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}