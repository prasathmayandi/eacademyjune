﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Security;

namespace eAcademy.Services
{
    public class PrimaryUserRegisterServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public int PrimaryUserCopyfromPreAdmissiontoMaster(PreAdmissionPrimaryUserRegister old)
        {
            var checkprimaryuserexist = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserEmail == old.PrimaryUserEmail).FirstOrDefault();
            if(checkprimaryuserexist != null)
            {
                checkprimaryuserexist.PrimaryUser = old.PrimaryUser;
                checkprimaryuserexist.PrimaryUserName = old.PrimaryUserName;
                checkprimaryuserexist.PrimaryUserEmail = old.PrimaryUserEmail;
                checkprimaryuserexist.EmergencyContactPersonName = old.EmergencyContactPersonName;
                checkprimaryuserexist.ContactPersonRelationship = old.ContactPersonRelationship;
                checkprimaryuserexist.EmergencyContactNumber = old.EmergencyContactNumber;
                checkprimaryuserexist.EmailRequired=old.EmailRequired;
                checkprimaryuserexist.SmsRequired=old.SmsRequired;
                dc.SaveChanges();
                return checkprimaryuserexist.PrimaryUserRegisterId;
            }
            else
            {
                PrimaryUserRegister user = new PrimaryUserRegister()
                {
                    PrimaryUser = old.PrimaryUser,
                    PrimaryUserName = old.PrimaryUserName,
                    PrimaryUserEmail = old.PrimaryUserEmail,
                    CompanyAddress1 = old.CompanyAddress1,
                    CompanyAddress2 = old.CompanyAddress2,
                    CompanyCity = old.CompanyCity,
                    CompanyState = old.CompanyState,
                    CompanyCountry = old.CompanyCountry,
                    CompanyContact = old.CompanyContact,
                    CompanyPostelcode = old.CompanyPostelcode,
                    UserCompanyname = old.UserCompanyname,
                    WorkSameSchool = old.WorkSameSchool,
                    TotalIncome = old.TotalIncome,
                    PrimaryUserContactNo = old.PrimaryUserContactNo,
                    EmergencyContactPersonName = old.EmergencyContactPersonName,
                    EmergencyContactNumber = old.EmergencyContactNumber,
                    ContactPersonRelationship = old.ContactPersonRelationship,
                    SmsRequired=old.SmsRequired,
                    EmailRequired=old.EmailRequired
                };
                dc.PrimaryUserRegisters.Add(user);
                dc.SaveChanges();
                int PrimaryuserRegId= user.PrimaryUserRegisterId;
                return PrimaryuserRegId;
            }
        }
        public void updateParentPassword(int? primaryUserRegId, string Password)
        {
            var getRow = (from a in dc.PrimaryUserRegisters where a.PrimaryUserRegisterId == primaryUserRegId select a).FirstOrDefault();
            getRow.PrimaryUserPwd = Password;
            dc.SaveChanges();
        }
        public Student FindStudent(string sid)
        {
            var check = dc.Students.Where(q => q.StudentId.Equals(sid)).FirstOrDefault();
            return check;
        }
        public PrimaryUserRegister GetMobilePrimaryuserdetails(long Contact)
        {
            var ans = (from a in dc.PrimaryUserRegisters

                       where a.PrimaryUserContactNo.Value.Equals(Contact)
                       select a).FirstOrDefault();
            return ans;
        }
        public string GetStudentPrimarUserEmail(string sid)
        {
            string useremail="";
            var ans = (from a in dc.Students
                       from b in dc.AdmissionTransactions
                       from c in dc.PrimaryUserRegisters
                       where a.StudentId.Equals(sid) && b.StudentRegisterId == a.StudentRegisterId && c.PrimaryUserRegisterId == b.PrimaryUserRegisterId
                       select c).FirstOrDefault();
            if(ans !=null)
            {
                if (ans.PrimaryUserEmail != null)
                {
                    useremail = ans.PrimaryUserEmail;
                    return useremail;
                }
                else if(ans.PrimaryUserContactNo!=null)
                {
                    useremail = ans.PrimaryUserEmail.ToString();
                    return useremail;
                }
                else
                {
                    return useremail;
                }
            }
            else
            {
                return useremail;
            }
            
        }
        public PrimaryUserRegister GetStudentPrimarUserDetails(string sid)
        {
            string useremail = "";
            var ans = (from a in dc.Students
                       from b in dc.AdmissionTransactions
                       from c in dc.PrimaryUserRegisters
                       where a.StudentId.Equals(sid) && b.StudentRegisterId == a.StudentRegisterId && c.PrimaryUserRegisterId == b.PrimaryUserRegisterId
                       select c).FirstOrDefault();
            return ans;
        }
        public PrimaryUserRegister GetParentPrimarUserEmail(string pid)
        {
            var ans = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserId.Equals(pid)).FirstOrDefault();
            return ans;
        }

        public PrimaryUserRegister CheckEmailUserAvaliable(string uname, string pwd)
        {
           
            var checkexistuser = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserEmail.Equals(uname)).FirstOrDefault();
            if(checkexistuser !=null)
            {
              //+ checkexistuser.Salt;
                var salt = checkexistuser.Salt;
                var PasswordWithSalt = salt + pwd;
                string ConvertedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(PasswordWithSalt, "SHA1");
                var a = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserEmail.Equals(uname) && q.PrimaryUserPwd.Equals(ConvertedPassword)).FirstOrDefault();
                return a;
            }
            else
            {
                return checkexistuser;
            }
        }
        public PrimaryUserRegister CheckMobileUserAvaliable(string uname, string pwd)
        {
            long mno = Convert.ToInt64(uname.Trim());
            var checkexistuser = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserContactNo.Value.Equals(mno)).FirstOrDefault();
            if (checkexistuser != null)
            {
                //+ checkexistuser.Salt;
                var salt = checkexistuser.Salt;
                var PasswordWithSalt = salt + pwd;
                string ConvertedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(PasswordWithSalt, "SHA1");
                var a = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserContactNo.Value.Equals(mno) && q.PrimaryUserPwd.Equals(ConvertedPassword)).FirstOrDefault();
                return a;
            }
            else
            {
                return checkexistuser;
            }


        }
        public string GetUsername(string uname)
        {
            string username = "";
            var ans = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserId.Equals(uname)).FirstOrDefault();
            if (ans != null)
               {
                   username = ans.PrimaryUserEmail;
                return username;
               }
            else 
            {
                var ans1 = dc.PreAdmissionPrimaryUserRegisters.Where(q => q.PrimaryUserEmail.Equals(uname)).FirstOrDefault();
                if(ans1!=null)
                {
                    username = ans1.PrimaryUserEmail;
                }
                else
                {
                    return username;
                }
                return username;
            }
        }

        public int FindPrimaryUser(string email)
        {
            string MatchEmailPattern =
                       @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
            string uname = "";
            if (Regex.IsMatch(email, MatchEmailPattern))
                {
                    var ans = (from a in dc.PrimaryUserRegisters

                               where a.PrimaryUserEmail.Equals(email)
                               select a).ToList();
                    return ans.Count;
                }
                else
                {
                    long mno = Convert.ToInt64(email);
                    var ans = (from a in dc.PrimaryUserRegisters
                               where a.PrimaryUserContactNo.Value.Equals(mno)
                               select a).ToList();
                    return ans.Count;
                }
            
           
        }

        public PrimaryUserRegister getPrimaryUserDetail(int? primaryUserRegId)
        {
            var row = (from a in dc.PrimaryUserRegisters where a.PrimaryUserRegisterId == primaryUserRegId select a).FirstOrDefault();
            return row;
        }

        public void updateParentPassword(int? primaryUserRegId,string ParentId,string p_Salt,string Password,string PwdBeforeEncrypt)
        {
            var getRow = (from a in dc.PrimaryUserRegisters where a.PrimaryUserRegisterId == primaryUserRegId select a).FirstOrDefault();
            getRow.PrimaryUserId = ParentId;
            getRow.Salt = p_Salt;
            getRow.PrimaryUserPwd = Password;
            getRow.PrimaryUserPwdBeforeEncrypt = PwdBeforeEncrypt;
            dc.SaveChanges();
        }
            public PrimaryUserRegister GetPrimaryUserDetails(int  pid)
           {
               var ans = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserRegisterId == pid).FirstOrDefault();
               return ans;
           }

           public bool CheckStudentIdandUserEmail(int Sid,string email)
           {
               var ans = (from b in dc.AdmissionTransactions
                          from c in dc.PrimaryUserRegisters
                          where b.StudentRegisterId == Sid && c.PrimaryUserEmail == email && b.PrimaryUserRegisterId == c.PrimaryUserRegisterId
                          select c).FirstOrDefault();
               if( ans!=null)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           public void EditPrimaryUserDetails(int pid, string primaryuser, string fname, string lname, long? mobileno, string email, string companyAdd1, string companyAdd2, string companycity, string companystate, string companycountry, long? companypostel, long? companycontact,
               string companyname, string worksameschool, long? totalincome)
           {
               var getrow = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserRegisterId == pid).FirstOrDefault();
            if(getrow != null)
            {
                getrow.PrimaryUser = primaryuser;
                getrow.PrimaryUserName = fname + " " + lname;
                getrow.PrimaryUserContactNo = mobileno;
                getrow.PrimaryUserEmail = email;
                getrow.CompanyAddress1 = companyAdd1;
                getrow.CompanyAddress2 = companyAdd2;
                getrow.CompanyCity=companycity;
                getrow.CompanyState=companystate;
                getrow.CompanyCountry = companycountry;
                getrow.CompanyPostelcode = companypostel;
                getrow.CompanyContact = companycontact;
                getrow.UserCompanyname = companyname;
                getrow.WorkSameSchool = worksameschool;
                getrow.TotalIncome = totalincome;
                dc.SaveChanges();
            }
           }
        public bool CheckEmailExit(int pid ,string email)
        {
            var ans = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserRegisterId != pid && q.PrimaryUserEmail == email).FirstOrDefault();
            if(ans!=null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public int EditAddPrimaryUser( string primaryuser,string Primaryusername, string primaryuseremail, string companyAdd1, string companyAdd2, string companycity, string companystate, string companycountry, long? companypostel, long? companycontact,
            string companyname, string worksameschool,long? totalincome)
        {
            PrimaryUserRegister user = new PrimaryUserRegister()
            {
                PrimaryUser = primaryuser,
                PrimaryUserName=Primaryusername,
                TotalIncome=totalincome,
                PrimaryUserEmail = primaryuseremail,
                CompanyAddress1 = companyAdd1,
                CompanyAddress2 = companyAdd2,
                CompanyCity = companycity,
                CompanyState = companystate,
                CompanyCountry = companycountry,
                CompanyPostelcode = companypostel,
                CompanyContact = companycontact,
                UserCompanyname = companyname,
                WorkSameSchool = worksameschool
            };
            dc.PrimaryUserRegisters.Add(user);
            dc.SaveChanges();
            
            return user.PrimaryUserRegisterId;
        }

        //check primary user = father is exist
        public AdmissionTransaction CheckPrimaryuserfather(string email,string fafname,string falname,DateTime? fadob,string primaryuser)
        {
            var ans = (from a in dc.AdmissionTransactions
                       from b in dc.PrimaryUserRegisters
                       from c in dc.FatherRegisters
                       where b.PrimaryUserEmail == email && b.PrimaryUser == primaryuser && a.PrimaryUserRegisterId == b.PrimaryUserRegisterId && c.FatherRegisterId == a.FatherRegisterId && c.FatherFirstName == fafname &&
                            c.FatherLastName == falname && c.FatherDOB == fadob
                       select a).FirstOrDefault();
            return ans;
        }

        //check primary user = Mother is exist
        public AdmissionTransaction CheckPrimaryusermother(string email, string fafname, string falname, DateTime? fadob, string primaryuser)
        {
            var ans = (from a in dc.AdmissionTransactions
                       from b in dc.PrimaryUserRegisters
                       from c in dc.MotherRegisters
                       where b.PrimaryUserEmail == email && b.PrimaryUser == primaryuser && a.PrimaryUserRegisterId == b.PrimaryUserRegisterId && c.MotherRegisterId == a.MotherRegisterId && c.MotherFirstname == fafname &&
                            c.MotherLastName == falname && c.MotherDOB == fadob
                       select a).FirstOrDefault();
            return ans;
        }

        //check primary user = Guardian is exist
        public AdmissionTransaction CheckPrimaryuserguardian(string email, string fafname, string falname, DateTime? fadob, string primaryuser)
        {
            var ans = (from a in dc.AdmissionTransactions
                       from b in dc.PrimaryUserRegisters
                       from c in dc.GuardianRegisters
                       where b.PrimaryUserEmail == email && b.PrimaryUser == primaryuser && a.PrimaryUserRegisterId == b.PrimaryUserRegisterId && c.GuardianRegisterId == a.GuardianRegisterId && c.GuardianFirstname == fafname &&
                            c.GuardianLastName == falname && c.GuardianDOB == fadob
                       select a).FirstOrDefault();
            return ans;
        }

        public string GetPrimaryUserEmailUseStudentRegid(int StudentregId)
        {
            var ans = (from a in dc.Students
                       from b in dc.AdmissionTransactions
                       from c in dc.PrimaryUserRegisters
                       where a.StudentRegisterId == StudentregId  && b.StudentRegisterId == a.StudentRegisterId && c.PrimaryUserRegisterId == b.PrimaryUserRegisterId
                       select c).FirstOrDefault().PrimaryUserEmail;

            return ans;
        }

        public PrimaryUserRegister GetPrimaryUserId(string id)
        {
            var ans = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserId == id).FirstOrDefault();
            return ans;
        }

        public List<PrimaryUserMailList> getPrimaryUserMailList(int yearId, int classId, int secId)
        {
            var list = (from a in dc.AssignClassToStudents
                        from b in dc.AdmissionTransactions
                        from c in dc.PrimaryUserRegisters
                        where b.StudentRegisterId == a.StudentRegisterId &&
                            b.PrimaryUserRegisterId == c.PrimaryUserRegisterId &&
                            a.AcademicYearId == yearId &&
                            a.ClassId == classId &&
                            a.SectionId == secId &&
                            a.Status == true
                        select new PrimaryUserMailList
                        {
                            PrimaryUserEmail = c.PrimaryUserEmail,
                            PrimaryUserMobileNum = c.PrimaryUserContactNo
                        }).ToList();
            return list;
        }

        public PrimaryUserRegister GetPrimaryUserdetails(string email)
        {
            var ans = dc.PrimaryUserRegisters.Where(q => q.PrimaryUserEmail == email).FirstOrDefault();
            return ans;
        }
        
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}