﻿using eAcademy.DataModel;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class ClassInchargeStudentRemarkServices : IDisposable
    {
        EacademyEntities db = new EacademyEntities();

        public void addRemark(int EmployeeRegisterId,int StudentRegisterId,int AcademicYearId,int ClassId,int SectionId,DateTime DateOfRemarks,string Remark)
        {
            ClassInchargeStudentRemark add = new ClassInchargeStudentRemark()
            {
                EmployeeRegisterId = EmployeeRegisterId,
                StudentRegisterId = StudentRegisterId,
                AcademicYearId = AcademicYearId,
                ClassId = ClassId,
                SectionId = SectionId,
                DateOfRemarks = DateOfRemarks,
                Remark = Remark
            };
            db.ClassInchargeStudentRemarks.Add(add);
            db.SaveChanges();
        }

        public List<RemarkList> getRemarkList(int yearId,int classId,int secId,int stuId,DateTime fdate,DateTime toDate)
        {
            var List = (from a in db.ClassInchargeStudentRemarks
                        from b in db.TechEmployees
                        where a.AcademicYearId == yearId &&
                        a.ClassId == classId &&
                        a.SectionId == secId &&
                        a.StudentRegisterId == stuId &&
                        a.DateOfRemarks >= fdate &&
                        a.DateOfRemarks <= toDate &&
                        a.EmployeeRegisterId == b.EmployeeRegisterId
                        select new RemarkList
                        {
                            DateOfRemarks = a.DateOfRemarks,
                            Remark = a.Remark,
                            UserName = b.EmployeeName,
                            UserType = "Class Incharge"
                        }).ToList();
            return List;
        }

        public List<getclassandsection> SearchClassIncharge(int id)
        {
            var ans = (from a in db.AssignClassTeachers
                       from b in db.Classes
                       from c in db.Sections
                       from d in db.TechEmployees
                       where a.AcademicYearId == id &&
                       a.ClassId == b.ClassId &&
                       a.SectionId == c.SectionId &&
                       a.EmployeeRegisterId == d.EmployeeRegisterId

                       select new getclassandsection
                       {
                           ClassName = b.ClassType,
                           SectionName = c.SectionName,
                           ClassTeacher = d.EmployeeName
                       }).Distinct().ToList();

            return ans;
        }

        public List<R_StudentRemark> GetStudentRemarks(int? yid, int? cid, int secId, int stuId,DateTime? from1,DateTime? to)
        {
            var ans = (
                (from a in db.StudentDailyAttendances
                        from b in db.TechEmployees

                        where a.AcademicYearId == yid &&
                                       a.ClassId == cid &&
                                       a.SectionId == secId &&
                                       a.StudentRegisterId == stuId && a.CurrentDate >= from1 && a.CurrentDate <= to &&
                                       a.EmployeeRegisterId == b.EmployeeRegisterId
                        select new R_StudentRemark
                        {
                            UserName = b.EmployeeName,
                            Dates = SqlFunctions.DateName("day", a.CurrentDate).Trim() + "/" + SqlFunctions.StringConvert((double)a.CurrentDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.CurrentDate),
                            Remark = a.LeaveReason,
                            
                            RemarkType = "Attendance"
                        })
                                .Union(from a in db.MarkTotals
                                       from b in db.TechEmployees
                                       where a.AcademicYearId == yid &&
                                       a.ClassId == cid &&
                                       a.SectionId == secId &&
                                       a.StudentRegisterId == stuId && a.MarkedDate >= from1 && a.MarkedDate <= to &&
                                       a.EmployeeRegisterId == b.EmployeeRegisterId
                                       select new R_StudentRemark
                                       {
                                           UserName = b.EmployeeName,
                                           Dates = SqlFunctions.DateName("day", a.MarkedDate).Trim() + "/" + SqlFunctions.StringConvert((double)a.MarkedDate.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", a.MarkedDate),
                                           Remark = a.Comment,
                                           
                                           RemarkType = "ProgressCard"
                                       })).ToList();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}