﻿using eAcademy.DataModel;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eAcademy.Services
{
    public class GradeServices : IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public List<GetGrade> getGradeDetail(int yearId, int classId)
        {
            var getGrade = (from a in dc.AssignGradeTypeToClasses
                            from b in dc.Grades
                            where a.AcademicYearId == yearId &&
                            a.ClassId == classId &&
                            a.GradeTypeId == b.GradeTypeId
                            select new GetGrade
                            {
                                grade = b.GradeName,
                                minVal = b.MinRange,
                                maxVal = b.MaxRange
                            }).ToList();
            return getGrade;
        }

        public GradeType Getgradesystemdetails(int gid)
        {
            var ans = dc.GradeTypes.Where(q => q.GradeTypeId == gid).FirstOrDefault();
            return ans;
        }

        public List<Grade> getGrade(int gradeTypeId)
        {
            var ans = dc.Grades.Where(q => q.GradeTypeId.Value.Equals(gradeTypeId)).ToList();
            return ans; 
        }

        public IEnumerable<object> getGradeDetails(int gradeTypeId)
        {
            var ans = dc.Grades.Where(q => q.GradeTypeId.Value.Equals(gradeTypeId)).Select(s => new { g = s.GradeName, m1 = s.MinRange, m2 = s.MaxRange, d = s.GradeDescribtion, gid = s.GradeId }).ToList();
            return ans; 
        }

        public void addgrade(int gTypeId, string gradeName, int minMark, int maxMark, string gradeDes)
        {
            Grade grade = new Grade()
            {
                GradeTypeId = gTypeId,
                GradeName = gradeName,
                MinRange = minMark,
                MaxRange = maxMark,
                GradeDescribtion = gradeDes
            };
            dc.Grades.Add(grade);
            dc.SaveChanges();
        }

        public void updateGrade(int gId, string gradeName, int minMark, int maxMark, string gradeDes)
        {
            var update = dc.Grades.Where(q => q.GradeId.Equals(gId)).First();
            update.GradeName = gradeName;
            update.MinRange = minMark;
            update.MaxRange = maxMark;
            update.GradeDescribtion = gradeDes;
            dc.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}