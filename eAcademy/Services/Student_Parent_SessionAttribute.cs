﻿using eAcademy.DataModel;
using System;
using System.Web;
using System.Web.Mvc;


namespace eAcademy.Services
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class Student_Parent_SessionAttribute : ActionFilterAttribute
    {
        Data db = new Data();
        EacademyEntities ee = new EacademyEntities();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower();
            if (!controllerName.Contains("home"))
            {
                HttpSessionStateBase session = filterContext.HttpContext.Session;
                var Student = session["StudentRegId_SessionAttribute"];
              
                if (((Student == null) && (!session.IsNewSession)) || (session.IsNewSession) || (null == filterContext.HttpContext.Session))
                {
                    var parent = session["ParentRegId_SessionAttribute"];
                    if (parent != null)
                    {
                    if (session["PrimaryUser"] == "Guardian")
                        {                     
                            if (session["GuardianStudentName"] == null)
                            {
                                //send them off to the Guardian student credential page
                                var url = new UrlHelper(filterContext.RequestContext);
                                var StudentCredentialUrl = url.Content("~/Guardian/StudentCredential");
                                filterContext.Result = new RedirectResult(StudentCredentialUrl);
                                return;
                            }
                        }                     
                    }
                    if (((parent == null) && (!session.IsNewSession)) || (session.IsNewSession) || (null == filterContext.HttpContext.Session))
                    {
                        //send them off to the login page
                        var url = new UrlHelper(filterContext.RequestContext);
                        var loginUrl = url.Content("~/Home/Login");
                        //filterContext.HttpContext.Response.Redirect(loginUrl, true);
                        filterContext.Result = new RedirectResult(loginUrl);
                        return;
                    }                 
                }
            }
        }
    }
}