﻿using eAcademy.DataModel;
using eAcademy.Models;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;

namespace eAcademy.Services
{
    public class TermFeeServices:IDisposable
    {
        EacademyEntities dc = new EacademyEntities();

        public List<FeeStructure> getTermFee(int yearId, int classId)
        {
            var getTermFeeStructure = (from a in dc.TermFees
                                       from b in dc.Terms
                                       from c in dc.FeeCategories
                                       where a.AcademicYearId == yearId &&
                                          a.ClassId == classId &&
                                          a.TermId == b.TermId &&
                                          a.FeeCategoryId == c.FeeCategoryId
                                       select new FeeStructure
                                       {
                                           TermId = a.TermId,
                                           TermName = b.TermName,
                                           TermFeeName = c.FeeCategoryName,
                                           TermFeeAmount = a.Amount,
                                           TermFeeLast_Date = SqlFunctions.DateName("day", a.LastDate).Trim() + " " + SqlFunctions.DateName("month", a.LastDate).Remove(3) + ", " + SqlFunctions.DateName("year", a.LastDate),
                                       }).OrderBy(a => a.TermId).ToList();
            return getTermFeeStructure;
        }



        public FeeInformation getTermFeeByID(int acid, int fid, int cid, int tid)
        {
            var ans = (from t1 in dc.TermFees
                       where t1.AcademicYearId == acid &&
                       t1.FeeCategoryId == fid &&
                       t1.ClassId == cid &&
                       t1.TermId == tid
                       select new FeeInformation { FeeAmount = t1.Amount }).SingleOrDefault();

            return ans;
        }

        public IEnumerable<object> getTermFeeByID(int acid, int cid)
        {
            var ans = from t1 in dc.TermFees
                      from t2 in dc.FeeCategories
                      from t3 in dc.Terms
                      where t1.AcademicYearId == acid && t1.ClassId == cid && t1.FeeCategoryId == t2.FeeCategoryId && t1.TermId == t3.TermId
                      select new { feeid = t2.FeeCategoryName, tfee = t1.Amount, t = t3.TermName,
                          ldate = SqlFunctions.DateName("day", t1.LastDate).Trim() + " " + SqlFunctions.DateName("month", t1.LastDate).Remove(3) + ", " + SqlFunctions.DateName("year", t1.LastDate),};
            return ans;
        }

        public bool checkTermFee(int acid, int cid, int fid, int termid)
        {
            var ans = dc.TermFees.Where(q => q.AcademicYearId.Value.Equals(acid) && q.ClassId.Value.Equals(cid) && q.FeeCategoryId.Value.Equals(fid) && q.TermId.Value.Equals(termid)).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }  
        public bool CheckCurrentAcademicTerm(int YearId)
        {
            var ans = (from t1 in dc.Terms
                       where t1.AcademicYearId == YearId       //t1.AcademicYearId.Value == acid &&
                       select t1).ToList();
            int count = ans.Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool deleteAcademicYear(int acid)
        {
            var ans = (from c in dc.TermFees
                       where c.AcademicYearId == acid
                       select c).ToList();
            if (ans.Count == 0)
            {                
                return true;
            }
            else
            {
                return false;
            }
        }          



        public List<TermFeeList> getTermFee(int feeid)
        {
            var ans = (from t1 in dc.AcademicYears
                       from t2 in dc.Classes
                       from t3 in dc.FeeCategories
                       from t4 in dc.TermFees
                       from t5 in dc.Fees
                       from t6 in dc.Terms
                       where t1.AcademicYearId == t4.AcademicYearId && t2.ClassId == t4.ClassId && t3.FeeCategoryId == t4.FeeCategoryId && t5.FeeId == t4.FeeId && t6.TermId == t4.TermId && t5.FeeCategoryId == t4.FeeCategoryId && t5.AcademicYearId == t4.AcademicYearId && t5.ClassId == t4.ClassId && t5.FeeId == feeid
                       select new TermFeeList { AcYearId = t1.AcademicYearId, AcYear = t1.AcademicYear1, TermId = t6.TermId, Term = t6.TermName, ClassId = t2.ClassId, Class = t2.ClassType, FeeCategoryId = t3.FeeCategoryId, FeeCategory = t3.FeeCategoryName, FeeId = t5.FeeId, TermFeeId = t4.TermFeeID, Amount = t4.Amount, LastDate = t4.LastDate.Value, Tax=t4.Tax.Value, Total=t4.Total.Value }).ToList();
            return ans;
        }

        public void addTermFee(int acid, int cid, int fid, int tid, Decimal amt, DateTime ldate, int feeid, Decimal tax, Decimal total)
        {
            int type = 2;
            TermFee fee = new TermFee()
            {
                AcademicYearId = acid,
                TermId = tid,
                ClassId = cid,
                FeeCategoryId = fid,
                Amount = amt,
                LastDate = ldate,
                PaymentTypeId = type,
                FeeId = feeid,
                Tax=tax,
                Total=total
            };
            dc.TermFees.Add(fee);
            dc.SaveChanges();
        }


        public void updateTermFee(int feeid, int amount)
        {
            var update = dc.TermFees.Where(query => query.TermFeeID.Equals(feeid)).First();
            update.Amount = amount;
            dc.SaveChanges();
        }

        public void deleteTermFee(int feeid)
        {
            var delete = dc.TermFees.Where(query => query.TermFeeID.Equals(feeid)).First();
            dc.TermFees.Remove(delete);
            dc.SaveChanges();
        }


        //jegadeesh
        public List<FeeInformation> TermFeedetails(int yid, int cid, int pid, int tid)
        {
            var ans = (from a in dc.TermFees
                       from b in dc.FeeCategories
                       from c in dc.Terms
                       where a.AcademicYearId == yid && a.ClassId == cid && a.FeeCategoryId == b.FeeCategoryId && a.PaymentTypeId == pid && a.TermId == tid && a.TermId == c.TermId
                       select new FeeInformation
                       {
                           term = c.TermName,
                           FeeCategory = b.FeeCategoryName,
                           FeeAmount = a.Amount
                       }).OrderBy(x => x.term).ToList();
            return ans;
        }
        public List<TermFee> GetJsonTermfees(int year, int clas, int tid)
        {
            var ans = (from a in dc.TermFees
                       where a.AcademicYearId == year && a.ClassId == clas && a.TermId == tid
                       select a).ToList();
            return ans;
        }
        public List<TermFee> GetJsonRemoveRedundantFees(List<TermFee> a2, int s)
        {
            var ans = a2.Where(x => x.FeeCategoryId != s).ToList();
            return ans;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dc.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}