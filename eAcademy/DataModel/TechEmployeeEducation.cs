//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eAcademy.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class TechEmployeeEducation
    {
        public int TechEmployeeEducationId { get; set; }
        public Nullable<int> EmployeeRegisterId { get; set; }
        public string Cours { get; set; }
        public string Institute { get; set; }
        public string University { get; set; }
        public string YearOfCompletion { get; set; }
        public Nullable<double> Mark { get; set; }
        public Nullable<int> OtherEmployeeRegisterId { get; set; }
    
        public virtual OtherEmployee OtherEmployee { get; set; }
        public virtual TechEmployee TechEmployee { get; set; }
    }
}
