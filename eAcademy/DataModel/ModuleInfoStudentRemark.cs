//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eAcademy.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ModuleInfoStudentRemark
    {
        public int RemarkId { get; set; }
        public Nullable<int> EmployeeRegisterId { get; set; }
        public Nullable<int> StudentRegisterId { get; set; }
        public Nullable<int> AcademicYearId { get; set; }
        public Nullable<int> ClassId { get; set; }
        public Nullable<int> SectionId { get; set; }
        public Nullable<int> SubjectId { get; set; }
        public Nullable<System.DateTime> DateOfRemarks { get; set; }
        public string Remark { get; set; }
    
        public virtual AcademicYear AcademicYear { get; set; }
        public virtual Class Class { get; set; }
        public virtual TechEmployee TechEmployee { get; set; }
        public virtual Section Section { get; set; }
        public virtual Student Student { get; set; }
        public virtual Subject Subject { get; set; }
    }
}
