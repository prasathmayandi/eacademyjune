//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eAcademy.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class AssignGradeTypeToClass
    {
        public int GradeClassId { get; set; }
        public Nullable<int> AcademicYearId { get; set; }
        public Nullable<int> ClassId { get; set; }
        public Nullable<int> GradeTypeId { get; set; }
    
        public virtual AcademicYear AcademicYear { get; set; }
        public virtual Class Class { get; set; }
        public virtual GradeType GradeType { get; set; }
    }
}
