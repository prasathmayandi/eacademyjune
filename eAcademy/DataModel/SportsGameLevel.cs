//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eAcademy.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class SportsGameLevel
    {
        public SportsGameLevel()
        {
            this.SportsGameLevelConfigs = new HashSet<SportsGameLevelConfig>();
        }
    
        public int LevelId { get; set; }
        public Nullable<int> AcademicYearId { get; set; }
        public Nullable<int> SessionId { get; set; }
        public Nullable<int> GameId { get; set; }
        public bool Status { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<int> Updatedby { get; set; }
    
        public virtual AcademicYear AcademicYear { get; set; }
        public virtual SportsGame SportsGame { get; set; }
        public virtual TechEmployee TechEmployee { get; set; }
        public virtual ICollection<SportsGameLevelConfig> SportsGameLevelConfigs { get; set; }
    }
}
