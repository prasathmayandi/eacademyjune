//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eAcademy.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSibling
    {
        public int Id { get; set; }
        public Nullable<int> StudentRegisterId { get; set; }
        public string SiblingAcademicYear { get; set; }
        public string SiblingRollNumbers { get; set; }
        public string SiblingClass { get; set; }
        public string SiblingSection { get; set; }
        public string PersonName { get; set; }
        public string PersonDepartment { get; set; }
        public Nullable<int> SiblingStudentRegId { get; set; }
        public string StatusFlag { get; set; }
        public Nullable<int> PrimaryUserRegId { get; set; }
    
        public virtual PrimaryUserRegister PrimaryUserRegister { get; set; }
        public virtual Student Student { get; set; }
        public virtual Student Student1 { get; set; }
    }
}
