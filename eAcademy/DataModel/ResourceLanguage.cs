//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eAcademy.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ResourceLanguage
    {
        public ResourceLanguage()
        {
            this.ResourceValues = new HashSet<ResourceValue>();
        }
    
        public int ResourcelanquageId { get; set; }
        public string LanquageDisplayName { get; set; }
        public string LanguageCultureName { get; set; }
        public string LanguageCultureCode { get; set; }
        public string LanguageCountryCode { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<System.DateTime> DateOfCreated { get; set; }
        public Nullable<System.DateTime> DateOfUpdated { get; set; }
    
        public virtual ICollection<ResourceValue> ResourceValues { get; set; }
    }
}
