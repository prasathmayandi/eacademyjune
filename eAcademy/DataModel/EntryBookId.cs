//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eAcademy.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class EntryBookId
    {
        public EntryBookId()
        {
            this.EntryBookReturns = new HashSet<EntryBookReturn>();
            this.StaffBookTakenReturns = new HashSet<StaffBookTakenReturn>();
            this.StudentBookTakenReturns = new HashSet<StudentBookTakenReturn>();
        }
    
        public int BookRegisterId { get; set; }
        public Nullable<int> BookEntryRegisterId { get; set; }
        public string BookId { get; set; }
        public string StatusFlag { get; set; }
        public Nullable<bool> status { get; set; }
    
        public virtual BookEntry BookEntry { get; set; }
        public virtual ICollection<EntryBookReturn> EntryBookReturns { get; set; }
        public virtual ICollection<StaffBookTakenReturn> StaffBookTakenReturns { get; set; }
        public virtual ICollection<StudentBookTakenReturn> StudentBookTakenReturns { get; set; }
    }
}
