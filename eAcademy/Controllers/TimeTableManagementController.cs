﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class TimeTableManagementController : Controller
    {
        //
        // GET: /TimeTableManagement/

        ExamTimetableServices examTt = new ExamTimetableServices();
        ClassServices clas = new ClassServices();
        SectionServices section = new SectionServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        public int FacultyId;

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            FacultyId = Convert.ToInt32(Session["empRegId"]);

            AcademicyearServices AcYear = new AcademicyearServices();
            SubjectServices sub = new SubjectServices();
            ExamServices exam = new ExamServices();
            TechemployeesServices techEmp = new TechemployeesServices();

            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.XallAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.allClasses = clas.ShowClasses();
            ViewBag.XallClasses = clas.ShowClasses();
            ViewBag.allSubjects = sub.ShowSubjects();
            ViewBag.allExams = exam.ShowExams();

            int featureId = 28;  // who has feature as a faculty info
            ViewBag.allFaculties = techEmp.ShowAllTechFaculties(featureId);

            SchoolSettingsServices sett = new SchoolSettingsServices();
            var getSchoolSettings = sett.SchoolSettings();
            if (getSchoolSettings != null)
            {
                int dayCount = getSchoolSettings.DayOrder.Value;
                ViewBag.NumberOfDayOrder = dayCount;
            }
        }

        public JsonResult GetSelectedClassSection(int passClassId)
        {
            try
            {
                var List = section.getSection(passClassId);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSelectedClassSectionSubject(int passYearId, int passClassId, int passSecId)
        {
            try
            {
                AssignSubjectToSectionServices secSub = new AssignSubjectToSectionServices();
                var SubjectList = secSub.ShowSubjects(passYearId, passClassId, passSecId);
                return Json(SubjectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getDate(int acid)
        {
            try
            {
                AcademicyearServices AcYear = new AcademicyearServices();
                var ans = AcYear.date(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                return Json(new { sd = sdate, ed = edate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ClassTimetable()
        {
            return View();
        }

        public ActionResult getPeriodSubject(int passYearId, int passClassId, int passSecId)
        {
            int day;
            int period;
            SchoolSettingsServices sett = new SchoolSettingsServices();
            var settingsExist = sett.SchoolSettings();
            if (settingsExist != null)
            {
                day = settingsExist.DayOrder.Value;
                TimeScheduleServices tschedule = new TimeScheduleServices();
                int count = tschedule.ClassPeriodCount(passClassId).Value;
                period = count;
                var PeriodsList = tschedule.GetClassPeriodTimeTable(passClassId);
                AssignSubjectToSectionServices SecSub = new AssignSubjectToSectionServices();
                var CheckSubAssigned = SecSub.checkSubjectAssigned(passYearId, passClassId, passSecId);
                if (CheckSubAssigned != null)
                {
                    var subList = SecSub.ShowSubjects(passYearId, passClassId, passSecId);

                    AssignSubjectToPeriodServices subPeriod = new AssignSubjectToPeriodServices();
                    var checkRecordExist = subPeriod.checkDataAlreadyExist(passYearId, passClassId, passSecId);
                    if (checkRecordExist != null)
                    {
                        var getList = subPeriod.getExistPeriodList(passYearId, passClassId, passSecId);
                        return Json(new { subList, getList, day, period, PeriodsList }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var getList = "empty";
                        return Json(new { subList, getList, day, period, PeriodsList }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("Please_assign_subject_to_section");
                    return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                string ErrorMessage = ResourceCache.Localize("Please_check_school_settings");
                return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddPeriodSubject(int passYearId, int passClassId, int passSectionId, string passSubject, int passDayId, int passPeriodId)
        {
            AcademicyearServices acyearServ = new AcademicyearServices();
            bool existacademicyear = acyearServ.PastAcademicyear(passYearId);
            if (existacademicyear == true)
            {
                SubjectServices sub = new SubjectServices();
                var getSubId = sub.getSubjectIdBySubjet(passSubject);
                int passSubjectId = getSubId.SubjectId;
                AssignSubjectToPeriodServices subPeriod = new AssignSubjectToPeriodServices();
                var checkDataExist = subPeriod.checkDataAlreadyExist(passYearId, passClassId, passSectionId);
                if (checkDataExist != null)
                {
                    subPeriod.updateSubjectToPeriod(passYearId, passClassId, passSectionId, passDayId, passPeriodId, passSubjectId);
                    string Message = ResourceCache.Localize("RecordsUpdatedSuccessfully");
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdateSubjectToPeriod"));
                    return Json(Message, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    subPeriod.addSubjectToPeriod(passYearId, passClassId, passSectionId, passDayId, passPeriodId, passSubjectId);
                    string Message = ResourceCache.Localize("Records_saved_successfully");
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddSubjectToPeriod"));
                    return Json(Message, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult getDayPeriod(int passYearId, int passClassId, int passSecId)
        {
            SchoolSettingsServices sett = new SchoolSettingsServices();
            var settingsExist = sett.SchoolSettings();
            if (settingsExist != null)
            {
                int day = settingsExist.DayOrder.Value;
                TimeScheduleServices tschedule = new TimeScheduleServices();
                int count = tschedule.ClassPeriodCount(passClassId).Value;
                int period = count;
                return Json(new { day, period }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string ErrorMessage = ResourceCache.Localize("Please_check_school_settings");
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult getSubjectId(string passSubject)
        {
            if (passSubject == "")
            {

                return Json(new { }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                SubjectServices sub = new SubjectServices();
                var getSubject = sub.getSubjectIdBySubjet(passSubject);
                var subjectid = getSubject.SubjectId;
                return Json(new { subjectid }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult IsFacultyFree(int passYearId, int passClassId, int passSectionId, string passSubject, int passDayId, int passPeriodId)
        {
            try
            {
                SubjectServices sub = new SubjectServices();
                var getSubId = sub.getSubjectIdBySubjet(passSubject);
                int passSubjectId = getSubId.SubjectId;
                AssignSubjectToPeriodServices SubjectPeriod = new AssignSubjectToPeriodServices();
                AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
                var getEmployeeId = facultySub.getSubjectFaculty(passYearId, passClassId, passSectionId, passSubjectId);
                int EmployeeId = getEmployeeId.EmployeeRegisterId.Value;
                string IsFacultyFree = "Yes";
                var getEmployeesSub = facultySub.getEmployeesSub(EmployeeId, passYearId);
                for (int i = 0; i < getEmployeesSub.Count; i++)
                {
                    int subId = getEmployeesSub[i].subId.Value;
                    int yearId = 0;
                    int classId = getEmployeesSub[i].classId.Value;
                    int secId = getEmployeesSub[i].secId.Value;

                    if (passClassId == classId && passSectionId == secId)
                    {

                    }
                    else
                    {
                        var checkFacultyFree = SubjectPeriod.checkFacultyFreeInPeriod(passYearId, classId, secId, subId, passDayId, passPeriodId);
                        if (checkFacultyFree != null)
                        {
                            IsFacultyFree = "No";
                            var ClassName = clas.getClassName(classId).ClassType;
                            var secName = section.getSectionName(classId, secId).SectionName;
                            return Json(new { IsFacultyFree, passDayId, passPeriodId, ClassName, secName }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                if (IsFacultyFree == "Yes")
                {
                    return Json(new { IsFacultyFree }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { IsFacultyFree, passDayId, passPeriodId }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult StaffTimetable()
        {
            return View();
        }

        public JsonResult getFacultyPeriodSubject(int passYearId, int passFacultyId)
        {
            int day = 0;
            int period = 0;
            SchoolSettingsServices sett = new SchoolSettingsServices();
            var settingsExist = sett.SchoolSettings();
            if (settingsExist != null)
            {
                day = settingsExist.DayOrder.Value;
                TimeScheduleServices tschedule = new TimeScheduleServices();
                int count = tschedule.GetMaximumPeriod().Value;
                period = count;

                AssignSubjectToPeriodServices SubjectPeriod = new AssignSubjectToPeriodServices();
                var FacultyPeriodSubject = SubjectPeriod.getFacultyPeriodSubject(passYearId, passFacultyId);

                AcademicyearServices AcYear = new AcademicyearServices();
                var getYear = AcYear.AcademicYear(passYearId).AcademicYear1;
                TechemployeesServices techEmp = new TechemployeesServices();
                var getFacultyName = techEmp.getFacultyDetails(passFacultyId).EmployeeName;
                return Json(new { day, period, FacultyPeriodSubject, getYear, getFacultyName }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { day, period }, JsonRequestBehavior.AllowGet);
        }

        // create Exam Timetable (new)      
        public ActionResult CreateExamTimetable()
        {
            return View();
        }

        public JsonResult getSubjectList(int passYearId, int passClassId, int passExamId)
        {
            try
            {
                AcademicyearServices acYear = new AcademicyearServices();
                var ans = acYear.AcademicYears(passYearId);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");

                AssignSubjectToSectionServices subject = new AssignSubjectToSectionServices();
                var List = subject.getClassList(passYearId, passClassId);


                ExamTimeScheduleServices ExSchedule = new ExamTimeScheduleServices();
                var ScheduleList = ExSchedule.ShowSchedule();

                ExamTimetableServices exam = new ExamTimetableServices();
                var checkRecordExist = exam.chexkRecordExist(passYearId, passClassId, passExamId);
                if (checkRecordExist != null)
                {
                    var ExistList = exam.ExistList(passYearId, passClassId, passExamId);
                    int count = ExistList.ToList().Count;
                    string msg = "old";


                    var n = count;
                    var j = 0;
                    int size = count + 1;
                    int val = 0;
                    for (var i = 0; i < n; i++)
                    {
                        var selVal = ExistList[i].Schedule;
                        var defVal = ExistList[0].Schedule.Value;
                        val = defVal;
                        if (selVal == defVal)
                        {
                            j++;
                        }
                        else
                        {
                        }
                    }

                    string schedule = "single";
                    if (j == n)
                    {
                        schedule = "All";
                    }
                    else
                    {
                        schedule = "single";
                    }
                    //

                    SchoolSettingsServices schServ = new SchoolSettingsServices();
                    var GradeOrMark = schServ.SchoolSettings();
                    if (GradeOrMark != null)
                    {
                        var Res = GradeOrMark.MarkFormat;
                        var List1 = subject.getClassSubList(passYearId, passClassId, passExamId);
                        if (Res == "Mark")
                        {
                            MarkTotalServices markTotSer = new MarkTotalServices();
                            var checkTotalCalculated = markTotSer.checkTotalCalculated(passYearId, passClassId, passExamId);
                            if (checkTotalCalculated == null)
                            {
                                string TotalCalculated = "No";
                                return Json(new { ExistList, count, sd = sdate, ed = edate, msg, ScheduleList, List, schedule, val, TotalCalculated, List1 }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                string TotalCalculated = "Yes";
                                return Json(new { ExistList, count, sd = sdate, ed = edate, msg, ScheduleList, List, schedule, val, TotalCalculated }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            GradeCommentServices gradeCmtServ = new GradeCommentServices();
                            var checkCommentExist = gradeCmtServ.checkCommentExist(passYearId, passClassId, passExamId);
                            if (checkCommentExist == null)
                            {
                                string TotalCalculated = "No";
                                return Json(new { ExistList, count, sd = sdate, ed = edate, msg, ScheduleList, List, schedule, val, TotalCalculated, List1 }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                string TotalCalculated = "Yes";
                                return Json(new { ExistList, count, sd = sdate, ed = edate, msg, ScheduleList, List, schedule, val, TotalCalculated }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }


                    //int[] ExistSubject = new int[ExistList.Count];
                    //for (int i = 0; i < ExistList.Count; i++)
                    //{
                    //    int ExistSubjectId = ExistList[i].SubjectId.Value;
                    //    ExistSubject[i] = ExistSubjectId;
                    //}

                    //int[] CurrentSub = new int[List.Count()];
                    //for (int i = 0; i < List.Count(); i++)
                    //{
                    //    int CurrentSubId = List1[i].SubjectId.Value;
                    //    ExistSubject[i] = CurrentSubId;
                    //}

                    //var UnUsedSub = CurrentSub.Except(ExistSubject).ToList();

                    return Json(new { ExistList, count, sd = sdate, ed = edate, msg, ScheduleList, List, schedule, val }, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    int count = List.ToList().Count;
                    string msg = "new";
                    return Json(new { List, count, sd = sdate, ed = edate, msg, ScheduleList }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveExamTimetable(SaveExamTimetable m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AcademicyearServices acyearServ = new AcademicyearServices();
                    bool existacademicyear = acyearServ.PastAcademicyear(m.AcademicYears);
                    if (existacademicyear == true)
                    {
                        AssignClassToStudentServices aces = new AssignClassToStudentServices();
                        var YearEndResult = aces.checkAcademicYearResult(m.AcademicYears, m.Class);
                        if (YearEndResult == null)
                        {
                            var TotalCount = m.TotalCount;
                            int year = m.AcademicYears;
                            int ExamId = m.Exam;
                            int classId = m.Class;
                            int size = TotalCount + 1;
                            int[] SubjectId = new int[size];
                            DateTime[] ExamDate = new DateTime[size];
                            int[] Schedule = new int[size];
                            string[] Rid = new string[size];

                            // checking same time period exist
                            string ErrorSameTimePeriod;
                            DateTime[] target = new DateTime[size];
                            Array.Copy(m.Examdate, target, size);

                            GroupSubjectConfigServices groupSubConfigServ = new GroupSubjectConfigServices();

                            for (int i = 0; i < size; i++)
                            {
                                for (int j = 0; j < size; j++)
                                {
                                    if (i == j)
                                    {

                                    }
                                    else
                                    {
                                        if (m.Examdate[i] == target[j])
                                        {
                                            int schedule1 = Convert.ToInt32(m.ScheduleVal[i]);
                                            int schedule2 = Convert.ToInt32(m.ScheduleVal[j]);
                                            if (schedule1 == schedule2)
                                            {
                                                int subje1 = Convert.ToInt32(m.subjectId[i]);
                                                int subGroupId1 = groupSubConfigServ.getGroupId(subje1);

                                                int subje2 = Convert.ToInt32(m.subjectId[j]);
                                                int subGroupId2 = groupSubConfigServ.getGroupId(subje1);

                                                if (subGroupId1 == 0 || subGroupId2 == 0)
                                                {
                                                    ErrorSameTimePeriod = "SameTimePeriod";
                                                    return Json(new { ErrorSameTimePeriod }, JsonRequestBehavior.AllowGet);
                                                }
                                                if (subGroupId1 != subGroupId2)
                                                {
                                                    ErrorSameTimePeriod = "SameTimePeriod";
                                                    return Json(new { ErrorSameTimePeriod }, JsonRequestBehavior.AllowGet);
                                                }
                                            }
                                            else
                                            {
                                                ExamTimeScheduleServices ExamTS = new ExamTimeScheduleServices();
                                                var getSchedule1Detail = ExamTS.getRow(schedule1);
                                                var schedule1STime = getSchedule1Detail.StartTime;
                                                var schedule1ETime = getSchedule1Detail.EndTime;

                                                var getSchedule2Detail = ExamTS.getRow(schedule2);
                                                var schedule2STime = getSchedule2Detail.StartTime;
                                                var schedule2ETime = getSchedule2Detail.EndTime;

                                                if (schedule1STime > schedule2STime && schedule1STime < schedule2ETime)
                                                {
                                                    int subje1 = Convert.ToInt32(m.subjectId[i]);
                                                    int subGroupId1 = groupSubConfigServ.getGroupId(subje1);

                                                    int subje2 = Convert.ToInt32(m.subjectId[j]);
                                                    int subGroupId2 = groupSubConfigServ.getGroupId(subje1);

                                                    if (subGroupId1 == 0 || subGroupId2 == 0)
                                                    {
                                                        ErrorSameTimePeriod = "SameTimePeriod";
                                                        return Json(new { ErrorSameTimePeriod }, JsonRequestBehavior.AllowGet);
                                                    }
                                                    if (subGroupId1 != subGroupId2)
                                                    {
                                                        ErrorSameTimePeriod = "SameTimePeriod";
                                                        return Json(new { ErrorSameTimePeriod }, JsonRequestBehavior.AllowGet);
                                                    }
                                                }
                                                if (schedule1ETime > schedule2STime && schedule1ETime < schedule2ETime)
                                                {
                                                    int subje1 = Convert.ToInt32(m.subjectId[i]);
                                                    int subGroupId1 = groupSubConfigServ.getGroupId(subje1);

                                                    int subje2 = Convert.ToInt32(m.subjectId[j]);
                                                    int subGroupId2 = groupSubConfigServ.getGroupId(subje1);

                                                    if (subGroupId1 == 0 || subGroupId2 == 0)
                                                    {
                                                        ErrorSameTimePeriod = "SameTimePeriod";
                                                        return Json(new { ErrorSameTimePeriod }, JsonRequestBehavior.AllowGet);
                                                    }
                                                    if (subGroupId1 != subGroupId2)
                                                    {
                                                        ErrorSameTimePeriod = "SameTimePeriod";
                                                        return Json(new { ErrorSameTimePeriod }, JsonRequestBehavior.AllowGet);
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }

                            // Check total already calculated
                            SchoolSettingsServices schServ = new SchoolSettingsServices();
                            var GradeOrMark = schServ.SchoolSettings();
                            if (GradeOrMark != null)
                            {
                                var Res = GradeOrMark.MarkFormat;
                                if (Res == "Mark")
                                {
                                    MarkTotalServices markTotSer = new MarkTotalServices();
                                    var checkTotalCalculated = markTotSer.checkTotalCalculated(year, classId, ExamId);
                                    if (checkTotalCalculated != null)
                                    {
                                        string TotalCalculated = "Yes";
                                        return Json(new { TotalCalculated }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                                else
                                {
                                    GradeCommentServices gradeCmtServ = new GradeCommentServices();
                                    var checkCommentExist = gradeCmtServ.checkCommentExist(year, classId, ExamId);
                                    if (checkCommentExist != null)
                                    {
                                        string TotalCalculated = "Yes";
                                        return Json(new { TotalCalculated }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }

                            ExamTimetableServices exam = new ExamTimetableServices();
                            ExamAttendanceServices examAtt = new ExamAttendanceServices();
                            var checkRecordExist = exam.chexkRecordExist(year, classId, ExamId);
                            if (checkRecordExist != null)
                            {
                                string msg = ""; string x = "No"; string Info = "";
                                for (int i = 0; i <= TotalCount; i++)
                                {
                                    SubjectId[i] = m.subjectId[i];
                                    ExamDate[i] = m.Examdate[i];
                                    int sch = Convert.ToInt32(m.ScheduleVal[i]);
                                    Schedule[i] = sch;
                                    Rid[i] = m.ID[i];
                                    string rid = Rid[i];

                                    if (rid == null)
                                    {
                                        int subId = SubjectId[i];
                                        var checkSubExist = exam.checkDataExistForSubject(year, classId, ExamId, subId);
                                        if (checkSubExist != null)
                                        {
                                            exam.UpdateExamTimetable(year, classId, ExamId, SubjectId[i], ExamDate[i], Schedule[i]);
                                        }
                                        else
                                        {
                                            exam.addExamTimetable(year, classId, ExamId, SubjectId[i], ExamDate[i], Schedule[i]);
                                        }
                                    }
                                    else
                                    {
                                        int id = Convert.ToInt32(QSCrypt.Decrypt(rid));
                                        string checkExamFinished = exam.UpdateExamTimetable(id, year, classId, ExamId, SubjectId[i], ExamDate[i], Schedule[i]);
                                        if (checkExamFinished == "Exam_Already_Finished_So_unable_to_change_date")
                                        {
                                            x = "Yes";
                                        }
                                    }
                                }
                                if (x == "Yes")
                                {
                                    Info = "You_cannot_change_the_details_for_completed_exams";
                                }
                                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Update_exam_timetable"));
                                string Message = ResourceCache.Localize("Exam_Timetable_updated_successfully");
                                return Json(new { Message, Info }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                for (int i = 0; i <= TotalCount; i++)
                                {
                                    SubjectId[i] = m.subjectId[i];
                                    ExamDate[i] = m.Examdate[i];
                                    int schId = Convert.ToInt32(m.ScheduleVal[i]);
                                    Schedule[i] = schId;
                                    exam.addExamTimetable(year, classId, ExamId, SubjectId[i], ExamDate[i], Schedule[i]);
                                }
                                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Add_exam_timetable"));
                                string Message = ResourceCache.Localize("Exam_Timetable_saved_successfully");
                                return Json(new { Message }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            string InfoMessage = ResourceCache.Localize("Academicyear_result_is_Created_So_Unable_to_Create_exam_timetable");
                            return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                        return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSubject(int passYearId, int passClassId)
        {
            AssignSubjectToSectionServices subject = new AssignSubjectToSectionServices();
            var List = subject.getClassList(passYearId, passClassId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }



        //Exam Time Schedule
        public ActionResult ExamTimeSchedule()
        {
            return View();
        }



        public JsonResult ExamScheduleResults(string sidx, string sord, int page, int rows)
        {
            IList<examTimeSchedule> ExamScheduleList = new List<examTimeSchedule>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            ExamTimeScheduleServices examTS = new ExamTimeScheduleServices();
            ExamScheduleList = examTS.ScheduleList();

            int totalRecords = ExamScheduleList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    ExamScheduleList = ExamScheduleList.OrderByDescending(s => s.StartTime).ToList();
                    ExamScheduleList = ExamScheduleList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ExamScheduleList = ExamScheduleList.OrderBy(s => s.EndTime).ToList();
                    ExamScheduleList = ExamScheduleList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = ExamScheduleList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = ExamScheduleList
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ExamTimeSchedule(ExamTimeschedule m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string schName = m.txt_ScheduleName;
                    ExamTimeScheduleServices examTS = new ExamTimeScheduleServices();
                    var checkNameExist = examTS.isNameExist(schName);
                    if (checkNameExist != null)
                    {
                        string ErrorMessage = ResourceCache.Localize("Exam_time_schedue_name_already_exist");
                        return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    if (m.txt_stime > m.txt_etime)
                    {
                        string ErrorMessage = ResourceCache.Localize("End_time_should_be_greater_than_start_time");
                        return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    var checkTimeExist = examTS.isTimeExist(m.txt_stime, m.txt_etime);
                    if (checkTimeExist != null)
                    {
                        string ErrorMessage = ResourceCache.Localize("Same_time_period_already_exist");
                        return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    examTS.addExamTimeSchedule(schName, m.txt_stime, m.txt_etime);
                    string Message = ResourceCache.Localize("Exam_Timetable_saved_successfully");
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddExamTimeSchedule"));
                    return Json(new { Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditExamTimeSchedule(string passId)
        {
            try
            {
                int eid = Convert.ToInt32(QSCrypt.Decrypt(passId));
                ExamTimeScheduleServices examTS = new ExamTimeScheduleServices();
                var getRow = examTS.getRow(eid);
                var scheduleName = getRow.ScheduleName;
                var stime = getRow.StartTime.Value.ToString(@"hh\:mm");
                var etime = getRow.EndTime.Value.ToString(@"hh\:mm");
                var status = getRow.Status;
                return Json(new { scheduleName, stime, etime, status }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult EditExamTimeSchedule(EditExamTimeschedule m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ExamTimeScheduleServices examTS = new ExamTimeScheduleServices();
                    int id = Convert.ToInt32(QSCrypt.Decrypt(m.scheduleId));
                    examTS.EditExamTimeSchedule(id, m.Status);
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Update_exam_time_schedule"));
                    string Message = ResourceCache.Localize("Exam_Timetable_status_updated_successfully");
                    return Json(new { Message }, JsonRequestBehavior.AllowGet);
                }
                return View();
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteExamTimeSchedule(string passId)
        {
            try
            {
                int Did = Convert.ToInt32(QSCrypt.Decrypt(passId));
                ExamTimetableServices ExamTt = new ExamTimetableServices();
                var CheckScheduleUsed = ExamTt.IsScheduleUsed(Did);
                if (CheckScheduleUsed != null)
                {
                    string ErrorMessage = ResourceCache.Localize("Schedule_is_used_by_some_other_function_So_you_cannot_delete_this_schedule");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ExamTimeScheduleServices examTS = new ExamTimeScheduleServices();
                    examTS.delRow(Did);
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Delete_exam_time_schedule"));
                    string Message = ResourceCache.Localize("Schedule_deleted_Successfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExamTimeSchedule_ExportToExcel()
        {
            try
            {
                List<examTimeSchedule> ExamScheduleList = (List<examTimeSchedule>)Session["JQGridList"];
                var list = ExamScheduleList.Select(o => new { Schedule = o.ScheduleName, StartTime = o.StartTime, EndTime = o.EndTime, Status = o.Status }).ToList();
                string fileName = ResourceCache.Localize("ExamTimeScheduleList");
                GenerateExcel.ExportExcel(list, fileName);
                return View("ExamTimeSchedule");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult ExamTimeSchedule_ExportToPdf()
        {
            try
            {
                List<examTimeSchedule> ExamScheduleList = (List<examTimeSchedule>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("ExamTimeScheduleList");
                GeneratePDF.ExportPDF_Portrait(ExamScheduleList, new string[] { "ScheduleName", "StartTime", "EndTime", "Status" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("ExamTimeSchedule.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

    }
}
