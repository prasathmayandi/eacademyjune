﻿using Datatable.Models;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using Excel;
using LinqToExcel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class ResourceController : Controller
    {
        //
        // GET: /Resource/

        EacademyEntities db = new EacademyEntities();
       
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ResourceLanguageServices resoure = new ResourceLanguageServices();
            ResourceTypeServices resourcetype = new ResourceTypeServices();
            ViewBag.RType = resourcetype.GetResourceType();
            ViewBag.CultureType = resoure.GetActiveLanquage();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddCulture()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult AddCulture(AddResourceLanguage r)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ResourceLanguageServices resoure = new ResourceLanguageServices();

                    bool status = resoure.AddResourceLanguage(r);
                    if (status == true)
                    {
                        ViewBag.success = "Language Added";
                        return View();
                    }
                    else
                    {
                       ViewBag.validationmsg = "Details Already Exist";
                        return View();
                    }
                }
                else
                {                   
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult EditCulture(string id)
        {
            int rowid = Convert.ToInt16(id);
            ResourceLanguageServices resoure = new ResourceLanguageServices();
            ViewBag.CultureType = resoure.GetActiveLanquage();
            var record = resoure.Editlanguage(rowid);
            return View(record);
        }
        [HttpPost]
        public ActionResult EditCulture(AddResourceLanguage r)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ResourceLanguageServices resoure = new ResourceLanguageServices();
                    var status = resoure.UpdateResourceLanquage(r);
                    if (status == "success")
                    {
                        ViewBag.success = "Language Updated";
                        var record = resoure.Editlanguage(r.ResourceLanguageId);
                        return View(record);
                    }
                    else
                    {
                        ViewBag.validationmsg = status;
                        return View();
                    }
                }
                else
                {                   
                    return View();
                }

            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult ResourceLanguageDatatable()
        {
            ResourceLanguageServices rl = new ResourceLanguageServices();
            var list = rl.GetAllresourcelanguage();
            return View(list);
        }
        public ActionResult ResourceManagement()
        {
            //return View();
            ResourceValueService resourval = new ResourceValueService();
            var List = resourval.GetResourceList();
            return View(List);
        }
        public ActionResult loaddata()
        {

            ResourceValueService resourval = new ResourceValueService();
            var data = resourval.GetResourceList();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddResource()
        {
            return View();
        }
        private static bool CultureEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["CultureEnabled"]);
        private static bool OrientationEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["OrientationEnabled"]);
        private static bool CountryEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["CountryEnabled"]);

        [HttpPost]
        public ActionResult AddResource(ResourceAdd m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DefaultResourceServices defaultresource = new DefaultResourceServices();
                    ResourceLanguageServices resourcelanq = new ResourceLanguageServices();
                    ResourceValueService resourceval = new ResourceValueService();
                    var checkResourceExist = defaultresource.CheckResourceExist(m.Resource_Name);
                    if (checkResourceExist == null)
                    {
                        long ResourceId = defaultresource.AddDefaultResource(m);
                        var getCultureName = resourcelanq.GetCulturename(m.ResourcelanquageId);
                        resourceval.AddResourcevalue(m, ResourceId, getCultureName);
                        return RedirectToAction("ResourceManagement", "Resource");
                    }
                    else
                    {
                        var checkexistresourcevalue = resourceval.CheckResourceValueExist(checkResourceExist.Id, m.ResourcelanquageId);
                        if (checkexistresourcevalue == null)
                        {
                            var getCultureName = resourcelanq.GetCulturename(m.ResourcelanquageId);
                            resourceval.AddResourcevalue(m, checkResourceExist.Id, getCultureName);
                            return RedirectToAction("ResourceManagement", "Resource");
                        }
                        else
                        {
                            ModelState.AddModelError("Error", "Culture exist");
                            return View();
                        }
                    }
                }
                else
                {
                    
                    return View();
                }
            }
            catch (Exception ex)    
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult EditResource(string id)
        {
            ResourceValueService resourceval = new ResourceValueService();
            ResourceLanguageServices resoure = new ResourceLanguageServices();
            ViewBag.CultureType = resoure.GetActiveLanquage();
            long rowId = Convert.ToInt64(id);
            var row = resourceval.GetparticularRecord(rowId);
            return View(row);
        }

        [HttpPost]
        public ActionResult EditResource(ResourceAdd m)
        {
            try { 
            if (ModelState.IsValid == true)
            {DefaultResourceServices defaultresource = new DefaultResourceServices();
                ResourceValueService resourceval = new ResourceValueService();
                ResourceLanguageServices resourcelanq = new ResourceLanguageServices();
            var CheckResourceNameexist = defaultresource.CheckResourceExist(m.Resource_Name);
                if (CheckResourceNameexist != null)
                {
                    var CheckResourceExist = resourceval.CheckalreadyExit(m.Resource_Value, m.Id,m.ResourcelanquageId,CheckResourceNameexist.Id); 

                    if (CheckResourceExist == null)
                    {
                        defaultresource.UpdateDefaultResource(m);
                        resourceval.UpdateResourcevalue(m);
                        return RedirectToAction("ResourceManagement", "Resource");
                    }
                    else
                    {
                        ModelState.AddModelError("Error", "Culture exist");
                        return View(m);
                    }
                }
                else
                {
                    long ResourceId = defaultresource.AddDefaultResource(m);
                    var getCultureName = resourcelanq.GetCulturename(m.ResourcelanquageId);
                    resourceval.Updateparticularresourcevalue(m, ResourceId, getCultureName);
                    return RedirectToAction("ResourceManagement", "Resource");
                }
            }
            else
            {
               
                return View(m);
            }
        }
          catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        
        public ActionResult UploadExcelLanguage()
        {
            return View();
        }

        //[HttpPost]
        //public ActionResult UploadExcelLanguage(HttpPostedFileBase FileUpload)
        //{
        //    var date = DateTimeByZone.getCurrentDateTime();
        //    try
        //    {
        //        List<string> data = new List<string>();
        //        if (FileUpload != null)
        //        {
        //            // tdata.ExecuteCommand("truncate table OtherCompanyAssets");  
        //            if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "text/csv")
        //            {
        //                string filename = FileUpload.FileName;
        //                string pathToCreate = "~/Excel_Docs/";
        //                string targetpath;
        //                if (Directory.Exists(Server.MapPath(pathToCreate)))
        //                {
        //                    targetpath = Server.MapPath("~/Excel_Docs/");
        //                }
        //                else
        //                {
        //                    Directory.CreateDirectory(Server.MapPath(pathToCreate));
        //                    targetpath = Server.MapPath(pathToCreate);
        //                }
        //                // string targetpath = Server.MapPath("~/Excel_Docs/");
        //                FileUpload.SaveAs(targetpath + filename);
        //                string pathToExcelFile = targetpath + filename;
        //                var excel = new ExcelQueryFactory(pathToExcelFile);
        //                var Resource_language = from a in excel.Worksheet<Excel_LanguageValue>() select a;
        //                var cnt = Resource_language.Count();
        //                var i = 1;
        //                foreach (var a in Resource_language)
        //                {
        //                    try
        //                    {
        //                        if (a.ResourceName == null || a.ResourceValue == null || a.ResourceTypeId == null || a.ResourceCulture == null || a.ResourceOrientation == null)
        //                        {
        //                            ViewBag.success = "upto " + i + " records saved";
        //                            return View();
        //                        }
        //                        else
        //                        {
        //                            DefaultResource TU = new DefaultResource();
        //                            TU.Name = a.ResourceName;
        //                            var b = 0;
        //                            if (a.ResourceTypeId == "UI")
        //                            {
        //                                b = 1;
        //                            }
        //                            var rname = (from e in db.DefaultResources where a.ResourceName == e.Name select e).FirstOrDefault();
        //                            var existlanguage = (from f in db.ResourceLanguages where a.ResourceCulture == f.LanguageCultureName select f.LanguageCultureName).FirstOrDefault();
        //                            var languageid = (from d in db.ResourceLanguages where d.LanguageCultureName == a.ResourceCulture select d.ResourcelanquageId).FirstOrDefault();
        //                            if (existlanguage != null)
        //                            {
        //                                if (rname == null)
        //                                {
        //                                    TU.ResourceTypeId = b;
        //                                    TU.DateCreated = date;
        //                                    TU.DateUpdated = date;
        //                                    TU.Deleted = false;
        //                                    db.DefaultResources.Add(TU);
        //                                    db.SaveChanges();
        //                                    var resourceid = TU.Id;
        //                                    ResourceValue rv = new ResourceValue();
        //                                    //var resourceid = (from c in db.DefaultResources select c.Id).Max();
        //                                    rv.Value = a.ResourceValue;
        //                                    rv.ResourceId = resourceid;
        //                                    rv.CultureName = a.ResourceCulture;
        //                                    rv.TextOrientation = a.ResourceOrientation;
        //                                    rv.Deleted = false;
        //                                    rv.DateCreated = date;
        //                                    rv.DateUpdated = date;
        //                                    rv.ResourcelanquageId = languageid;
        //                                    db.ResourceValues.Add(rv);
        //                                    db.SaveChanges();
        //                                }
        //                                else
        //                                {
        //                                    var langexists = (from g in db.ResourceValues where g.ResourceId == rname.Id && g.ResourcelanquageId == languageid select g).FirstOrDefault();
        //                                    if (langexists != null)
        //                                    {
        //                                        langexists.TextOrientation = a.ResourceOrientation;
        //                                        langexists.Value = a.ResourceValue;
        //                                        langexists.DateUpdated = date;
        //                                        db.SaveChanges();
        //                                    }
        //                                    else
        //                                    {
        //                                        ResourceValue rv = new ResourceValue();
        //                                        rv.Value = a.ResourceValue;
        //                                        rv.ResourceId = rname.Id;
        //                                        rv.CultureName = a.ResourceCulture;
        //                                        rv.TextOrientation = a.ResourceOrientation;
        //                                        rv.Deleted = false;
        //                                        rv.DateCreated = date;
        //                                        rv.DateUpdated = date;
        //                                        rv.ResourcelanquageId = languageid;
        //                                        db.ResourceValues.Add(rv);
        //                                        db.SaveChanges();
        //                                    }
        //                                }
        //                            }
        //                            else
        //                            {
        //                                ViewBag.message = "Language not found";
        //                                return View();
        //                            }
        //                        }
        //                    }

        //                    catch (Exception ex)
        //                    {
        //                        ViewBag.error = ex.Message;
        //                        return View("Error");
        //                    }
        //                    i++;

        //                }
        //                ViewBag.success = "Excel resource is stored successfully";
        //                return View();
        //            }
        //            else
        //            {
        //                //alert message for invalid file format  
        //                ViewBag.message = "please select excel file format";
        //                return View();
        //            }
        //        }
        //        else
        //        {
        //            if (FileUpload == null)
        //                ViewBag.message = "Please choose Excel file";
        //            return View();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.error = ex.Message;
        //        return View("Error");
        //    }
        //}

        [HttpPost]
        public ActionResult UploadExcelLanguage(HttpPostedFileBase FileUpload)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            try
            {
                List<string> data = new List<string>();
                if (FileUpload != null)
                {
                    // tdata.ExecuteCommand("truncate table OtherCompanyAssets");  
                    if (FileUpload.FileName.EndsWith(".xls") || FileUpload.FileName.EndsWith(".xlsx"))
                    {
                        string filename = FileUpload.FileName;
                        string pathToCreate = "~/Excel_Docs/";
                        string targetpath;
                        if (Directory.Exists(Server.MapPath(pathToCreate)))
                        {
                            targetpath = Server.MapPath("~/Excel_Docs/");
                        }
                        else
                        {
                            Directory.CreateDirectory(Server.MapPath(pathToCreate));
                            targetpath = Server.MapPath(pathToCreate);
                        }
                        // string targetpath = Server.MapPath("~/Excel_Docs/");
                        FileUpload.SaveAs(targetpath + filename);
                        string pathToExcelFile = targetpath + filename;
                        Stream stream = FileUpload.InputStream;
                        // We return the interface, so that
                        IExcelDataReader reader = null;
                        //reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        if (FileUpload.FileName.EndsWith(".xls"))
                   {
                       reader = ExcelReaderFactory.CreateBinaryReader(stream);
                   }
                        else if (FileUpload.FileName.EndsWith(".xlsx"))
                   {
                       reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                   }
                        else
                        {
                            ViewBag.message = "please select excel file format";
                            return View();
                        }
                        reader.IsFirstRowAsColumnNames = true;
                        //DataTable schematable=reader.GetSchemaTable();
                        DataSet result = reader.AsDataSet();
                        foreach (DataRow myField in result.Tables[0].Rows)
                        {
                            //For each property of the field...
                            foreach (DataColumn myProperty in result.Tables[0].Columns)
                            {
                                //Display the field name and value.
                                string columnname = myProperty.ColumnName + " = " + myField[myProperty].ToString();
                            }
                        }
                        int rowcount = result.Tables[0].Rows.Count;
                        string columncount = result.Tables[0].Columns[0].ColumnName.ToString();
                        string CellValue = result.Tables[0].Rows[0][1].ToString();
                        for (int i = 0; i <=rowcount; i++)
                        {
                            string ResourceName = result.Tables[0].Rows[i][0].ToString();
                            string ResourceTypeId = result.Tables[0].Rows[i][1].ToString();
                            string ResourceValue = result.Tables[0].Rows[i][2].ToString();
                            string ResourceCulture = result.Tables[0].Rows[i][3].ToString();
                            string ResourceOrientation = result.Tables[0].Rows[i][4].ToString();
                            if (ResourceName == null || ResourceTypeId == null || ResourceValue == null || ResourceCulture == null || ResourceOrientation == null || ResourceName == "" || ResourceTypeId == "" || ResourceValue == "" || ResourceCulture == "" || ResourceOrientation == "")
                             {
                                 ViewBag.success = "upto " + (i+1) + " records saved";
                                 return View();
                             }
                            else
                            {
                                DefaultResource TU = new DefaultResource();
                                TU.Name = ResourceName;
                                var b = 0;
                                if (ResourceTypeId == "UI")
                                {
                                    b = 1;
                                }
                                var rname = (from e in db.DefaultResources where ResourceName == e.Name select e).FirstOrDefault();
                                var existlanguage = (from f in db.ResourceLanguages where ResourceCulture == f.LanguageCultureName select f.LanguageCultureName).FirstOrDefault();
                                var languageid = (from d in db.ResourceLanguages where d.LanguageCultureName == ResourceCulture select d.ResourcelanquageId).FirstOrDefault();
                                if (existlanguage != null)
                                {
                                    if (rname == null)
                                    {
                                        TU.ResourceTypeId = b;
                                        TU.DateCreated = date;
                                        TU.DateUpdated = date;
                                        TU.Deleted = false;
                                        db.DefaultResources.Add(TU);
                                        db.SaveChanges();
                                        var resourceid = TU.Id;
                                        ResourceValue rv = new ResourceValue();
                                        //var resourceid = (from c in db.DefaultResources select c.Id).Max();
                                        rv.Value = ResourceValue;
                                        rv.ResourceId = resourceid;
                                        rv.CultureName = ResourceCulture;
                                        rv.TextOrientation = ResourceOrientation;
                                        rv.Deleted = false;
                                        rv.DateCreated = date;
                                        rv.DateUpdated = date;
                                        rv.ResourcelanquageId = languageid;
                                        db.ResourceValues.Add(rv);
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        var langexists = (from g in db.ResourceValues where g.ResourceId == rname.Id && g.ResourcelanquageId == languageid select g).FirstOrDefault();
                                        if (langexists != null)
                                        {
                                            langexists.TextOrientation = ResourceOrientation;
                                            langexists.Value = ResourceValue;
                                            langexists.DateUpdated = date;
                                            db.SaveChanges();
                                        }
                                        else
                                        {
                                            ResourceValue rv = new ResourceValue();
                                            rv.Value = ResourceValue;
                                            rv.ResourceId = rname.Id;
                                            rv.CultureName = ResourceCulture;
                                            rv.TextOrientation = ResourceOrientation;
                                            rv.Deleted = false;
                                            rv.DateCreated = date;
                                            rv.DateUpdated = date;
                                            rv.ResourcelanquageId = languageid;
                                            db.ResourceValues.Add(rv);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                                else
                                {
                                    ViewBag.message = "Language not found";
                                    return View();
                                }
                            }
                        }
                        ViewBag.success = "Excel resource is stored successfully";
                        return View();
                    }
                    else
                    {
                        //alert message for invalid file format  
                        ViewBag.message = "please select excel file format";
                        return View();
                    }
                }
                else
                {
                    if (FileUpload == null)
                        ViewBag.message = "Please choose Excel file";
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult Resource_Lanaguage_Excel()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Resource_Lanaguage_Excel(HttpPostedFileBase FileUpload)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            try
            {
                List<string> data = new List<string>();
                if (FileUpload != null)
                {
                    // tdata.ExecuteCommand("truncate table OtherCompanyAssets");
                    if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "text/csv")
                    {
                        string filename = FileUpload.FileName;
                        string pathToCreate = "~/Excel_Docs/";
                        string targetpath;
                        if (Directory.Exists(Server.MapPath(pathToCreate)))
                        {
                            targetpath = Server.MapPath("~/Excel_Docs/");
                        }
                        else
                        {
                            Directory.CreateDirectory(Server.MapPath(pathToCreate));
                            targetpath = Server.MapPath(pathToCreate);
                        }
                        // string targetpath = Server.MapPath("~/Excel_Docs/");
                        FileUpload.SaveAs(targetpath + filename);
                        string pathToExcelFile = targetpath + filename;
                        Stream stream = FileUpload.InputStream;
                        // We return the interface, so that
                        IExcelDataReader reader = null;
                        //reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        if (FileUpload.FileName.EndsWith(".xls"))
                        {
                            reader = ExcelReaderFactory.CreateBinaryReader(stream);
                        }
                        else if (FileUpload.FileName.EndsWith(".xlsx"))
                        {
                            reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        }
                        else
                        {
                            ViewBag.message = "please select excel file format";
                            return View();
                        }
                        reader.IsFirstRowAsColumnNames = true;
                        //DataTable schematable=reader.GetSchemaTable();
                        DataSet result = reader.AsDataSet();
                        foreach (DataRow myField in result.Tables[0].Rows)
                        {
                            //For each property of the field...
                            foreach (DataColumn myProperty in result.Tables[0].Columns)
                            {
                                //Display the field name and value.
                                string columnname = myProperty.ColumnName + " = " + myField[myProperty].ToString();
                            }
                        }
                        int rowcount = result.Tables[0].Rows.Count;
                        string columncount = result.Tables[0].Columns[0].ColumnName.ToString();
                        string CellValue = result.Tables[0].Rows[0][1].ToString();
                        for (int i = 0; i <= rowcount; i++)
                        {
                            string LanquageDisplayName = result.Tables[0].Rows[i][0].ToString();
                            string LanguageCultureName = result.Tables[0].Rows[i][1].ToString();
                            string LanguageCultureCode = result.Tables[0].Rows[i][2].ToString();
                            string LanguageCountryCode = result.Tables[0].Rows[i][3].ToString();
                            try
                            {
                                if (LanquageDisplayName == null || LanguageCultureName == null || LanguageCultureCode == null || LanguageCountryCode == null || LanquageDisplayName == "" || LanguageCultureName == "" || LanguageCultureCode == "" || LanguageCountryCode == "")
                                {
                                    ViewBag.success = "upto " + (i) + " records saved";
                                    return View();
                                }
                                else
                                {
                                    var language = (from y in db.ResourceLanguages where LanguageCultureName == y.LanguageCultureName select y).FirstOrDefault();
                                    if (language == null)
                                    {
                                        ResourceLanguage rl = new ResourceLanguage();
                                        rl.LanquageDisplayName = LanquageDisplayName;
                                        rl.LanguageCultureName = LanguageCultureName;
                                        rl.LanguageCultureCode = LanguageCultureCode;
                                        rl.LanguageCountryCode = LanguageCountryCode;
                                        rl.Status = true;
                                        rl.DateOfCreated = date;
                                        rl.DateOfUpdated = date;
                                        db.ResourceLanguages.Add(rl);
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        language.LanguageCultureName = LanguageCultureName;
                                        language.LanguageCountryCode = LanguageCountryCode;
                                        language.LanguageCultureCode = LanguageCultureCode;
                                        language.LanquageDisplayName = LanquageDisplayName;
                                        language.DateOfUpdated = date;
                                        db.SaveChanges();
                                    }

                                }
                            }

                            catch (Exception ex)
                            {
                                ViewBag.error = ex.Message;
                                return View("Error");
                            }
                           
                        }

                        ViewBag.success = "Sucessfully saved languages";
                        return View();
                    }
                    else
                    {
                        ViewBag.messsage = "Only Excel file format is allowed";
                        return View();
                    }
                }
                else
                {
                    
                    ViewBag.ErrorMesssage = "Please choose Excel file";
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        //[HttpPost]
        //public ActionResult Resource_Lanaguage_Excel(HttpPostedFileBase FileUpload)
        //{
        //    var date = DateTimeByZone.getCurrentDateTime();
        //    try { 
        //    List<string> data = new List<string>();
        //    if (FileUpload != null)
        //    {
        //        // tdata.ExecuteCommand("truncate table OtherCompanyAssets");
        //        if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || FileUpload.ContentType == "text/csv")
        //        {
        //            string filename = FileUpload.FileName;
        //            string pathToCreate = "~/Excel_Docs/";
        //            string targetpath;
        //            if (Directory.Exists(Server.MapPath(pathToCreate)))
        //            {
        //                targetpath = Server.MapPath("~/Excel_Docs/");
        //            }
        //            else
        //            {
        //                Directory.CreateDirectory(Server.MapPath(pathToCreate));
        //                targetpath = Server.MapPath(pathToCreate);
        //            }
        //            // string targetpath = Server.MapPath("~/Excel_Docs/");
        //            FileUpload.SaveAs(targetpath + filename);
        //            string pathToExcelFile = targetpath + filename;
        //            var excel = new ExcelQueryFactory(pathToExcelFile);
        //            var Resource_language = from a in excel.Worksheet<Resource_Language_Excelsheet>() select a;

        //            var i = 1;
        //            foreach (var a in Resource_language)
        //            {
        //                try
        //                {
        //                    if (a.LanquageDisplayName == null || a.LanguageCultureName == null || a.LanguageCultureCode == null || a.LanguageCountryCode == null)
        //                    {
        //                        ViewBag.success = "upto " + i + " records saved";
        //                        return View();
        //                    }
        //                    else 
        //                    { 
        //                   var language = (from y in db.ResourceLanguages where a.LanguageCultureName == y.LanguageCultureName select y).FirstOrDefault();
        //                   if (language == null)
        //                    {
        //                        ResourceLanguage rl = new ResourceLanguage();
        //                        rl.LanquageDisplayName = a.LanquageDisplayName;
        //                        rl.LanguageCultureName = a.LanguageCultureName;
        //                        rl.LanguageCultureCode = a.LanguageCultureCode;
        //                        rl.LanguageCountryCode = a.LanguageCountryCode;
        //                        rl.Status = true;
        //                        rl.DateOfCreated = date;
        //                        rl.DateOfUpdated = date;
        //                        db.ResourceLanguages.Add(rl);
        //                        db.SaveChanges(); 
        //                    }
        //                    else
        //                    {
        //                        language.LanguageCultureName = a.LanguageCultureName;
        //                        language.LanguageCountryCode = a.LanguageCountryCode;
        //                        language.LanguageCultureCode = a.LanguageCultureCode;
        //                        language.LanquageDisplayName = a.LanquageDisplayName;
        //                        language.DateOfUpdated= date;
        //                        db.SaveChanges();
        //                    }

        //                    }
        //                }

        //                catch (Exception ex)
        //                {
        //                    ViewBag.error = ex.Message;
        //                    return View("Error");
        //                }
        //                i++;
        //            }

        //            ViewBag.success = "Sucessfully saved languages";
        //            return View();
        //        }
        //        else
        //        {
        //            ViewBag.messsage = "Only Excel file format is allowed";
        //            return View();
        //        }
        //    }
        //    else
        //    {
        //        ViewBag.messsage = "Please choose Excel file";
        //        return View();
        //    }
        //}
        //  catch (Exception ex)
        //    {
        //        ViewBag.error = ex.Message;
        //        return View("Error");
        //    }
        //}

        [HttpPost]
        public JsonResult Activelanguage(string[] arr)
        {
            try
            {
                ResourceLanguageServices resourcelang = new ResourceLanguageServices();
                int count = arr.Count();
                for (int i = 0; i < count; i++)
                {
                    if(arr[i]!="")
                    {
                        int id = Convert.ToInt16(arr[i]);
                        resourcelang.ActivateLanguages(id);
                    }
                }
                return Json("success",JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                return Json(e.Message.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult InActivelanguage(string[] arr)
        {
            try
            {
                ResourceLanguageServices resourcelang = new ResourceLanguageServices();
                int count = arr.Count();
                for (int i = 0; i < count; i++)
                {
                    if (arr[i] != "")
                    {
                        int id = Convert.ToInt16(arr[i]);
                        resourcelang.InActivateLanguages(id);
                    }
                }
                return Json("success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult refreshtable()
        {
            ResourceLanguageServices rl = new ResourceLanguageServices();
            var list = rl.GetAllresourcelanguage();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
                
        public JsonResult Changelanguage(string Language)
        {
            Session["Language"] = Language;
            ResourceCache rc = new ResourceCache();    
         //   ResourceCache.Localize("Date");
            return Json("success", JsonRequestBehavior.AllowGet);
        }
    }
}
