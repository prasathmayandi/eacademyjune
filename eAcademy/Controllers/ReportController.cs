﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    public class ReportController : Controller
    {
        Data db = new Data();
        EacademyEntities ee = new EacademyEntities();
        public int EmployeeRegId;
        public int StudentRegId;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            EmployeeRegId = Convert.ToInt32(Session["empRegId"]);
            StudentRegId = Convert.ToInt32(Session["StudentRegId"]);
        }
        public JsonResult ChangeAcademicyear(int YearId)
        {
            try
            {
                Session["List_ChosenAcademicYearId"] = YearId;
                string ans = "success";
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AddChangeAcademicyear(int YearId)
        {
            try
            {
                Session["Add_ChosenAcademicYearId"] = YearId;
                string ans = "success";
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ChangeCaptchaValue(string Captchavalue)
        {
            try
            {
                Session["Captcha"] = Captchavalue;
                string ans = "success";
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getAcademicyearName(int YearId)
        {
            try
            {
                AcademicyearServices acYear = new AcademicyearServices();
                var yearName = acYear.AcademicYear(YearId).AcademicYear1;
                return Json(yearName, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getSectionList(int clas)
        {
            try
            {
                SectionServices ss = new SectionServices();
                var ans = ss.GetJsonsection(clas);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getStuRegid(int year, int clas, int sec)
        {
            try
            {
                AssignClassToStudentServices acs = new AssignClassToStudentServices();
                var ans = acs.GetJsonStudentRegid(year, clas, sec);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetMonth(int year)
        {
            try
            {
                MonthsServices acs = new MonthsServices();
                var ans = acs.getAcademicyesrMonth(year);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        //public JsonResult getStuRegid(int year, int clas, int sec)
        //{
        //    try
        //    {
        //        AssignClassToStudentServices acs = new AssignClassToStudentServices();
        //        var ans = acs.GetJsonStudentRegid(year, clas, sec);
        //        return Json(ans, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(e.Message, JsonRequestBehavior.AllowGet);
        //    }
        //}
        public JsonResult getExamSubject(int year, int clas, int sec)
        {
            try
            {
                SubjectServices ss = new SubjectServices();
                var ans = ss.GetJsonExamSubject(year, clas, sec);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getTechEmpname()
        {
            try
            {
                TechEmployeeServices ts = new TechEmployeeServices();
                var ans = ts.GetjsonTechEmployee();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AyidTechEmpname(int yid)
        {
            try
            {
                TechEmployeeServices tcs = new TechEmployeeServices();
                var ans = tcs.GetjsonAyidTechEmployee(yid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getOtherEmpname()
        {
            try
            {
                OtherEmployeeServices os = new OtherEmployeeServices();
                var ans = os.GetjsonOtherEmployee();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getTechEmpId(int eid)
        {
            try
            {
                TechEmployeeServices tcs = new TechEmployeeServices();
                var ans = tcs.GetjsonTechEmployeeid(eid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getOtherEmpId(int eid)
        {
            try
            {
                OtherEmployeeServices os = new OtherEmployeeServices();
                var ans = os.GetjsonOtherEmployeeId(eid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getTerm(int year)
        {
            try
            {
                TermServices ts = new TermServices();
                var ans = ts.GetjsonTerm(year);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getfeecategory(int year, int clas, int pid)
        {
            try
            {
                FeeCategoryServices fcs = new FeeCategoryServices();
                if (pid == 1)
                {
                    var ans1 = fcs.GetJsonFee(year, clas, pid);
                    ViewBag.ans = ans1;
                }
                else if (pid == 2)
                {
                    var ans2 = fcs.GetJsonTermFee(year, clas, pid);
                    ViewBag.ans = ans2;
                }
                var ans = ViewBag.ans;
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getpay(int year, int clas, int sec, int stu_id)
        {
            try
            {
                FeeCollectionServices fcs = new FeeCollectionServices();
                var ans = fcs.GetJsonAnnualPay(year, clas, sec, stu_id);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult gettermpay(int year, int clas, int sec, int stu_id, int tid)
        {
            try
            {
                FeeCollectionServices fcs = new FeeCollectionServices();
                var ans = fcs.GetJsonTermPay(year, clas, sec, stu_id, tid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getschoolname()
        {
            try
            {
                SchoolSettingsServices sss = new SchoolSettingsServices();
                var ans = sss.GetJsonSchoolnameLogo();
                return Json(new { name = ans.SchoolName, logo = ans.SchoolLogo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult date()
        {
            try
            {
                EventServices es = new EventServices();
                var ans = ee.Events.Where(a=> a.Status == true).Select(q => new { dat = q.EventDate, name = q.EventName, id = q.EventId, des = q.EventDecribtion }).ToList();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        //public JsonResult Attendance()
        //{
        //    try
        //    {
        //        string Year;
        //        AssignClassToStudentServices classStu = new AssignClassToStudentServices();
        //        var getRow = classStu.getAssigendClass(StudentRegId);
        //        if (getRow != null)
        //        {
        //            int yearId = Convert.ToInt32(getRow.AcademicYearId);
        //            int classId = Convert.ToInt32(getRow.ClassId);
        //            int secId = Convert.ToInt32(getRow.SectionId);
        //            StudentDailyAttendanceServices stuAttendance = new StudentDailyAttendanceServices();
        //            var getAttendanceMonth = stuAttendance.getAttendanceMonth(yearId, classId, secId);
        //            ArrayList month1 = new ArrayList();
        //            ArrayList presentDaysCount = new ArrayList();
        //            ArrayList monthName = new ArrayList();
        //            ArrayList CurrentmonthNameWorkingdays = new ArrayList();

        //            for (int i = 0; i < getAttendanceMonth.Count; i++)
        //            {
        //                var month = getAttendanceMonth[i].month;
        //                var month_Name = getAttendanceMonth[i].monthname.Value.ToString("MMM");
        //                var workingdays = stuAttendance.getCurrentMonthWorkingDays(yearId, classId, secId, month);
        //                var getPresentDays = stuAttendance.getStudentPresentDays(yearId, classId, secId, month, StudentRegId);
        //                presentDaysCount.Add(getPresentDays.Count);
        //                CurrentmonthNameWorkingdays.Add(workingdays);
        //                month1.Add(month);
        //                monthName.Add(month_Name);
        //            }
        //            AcademicyearServices AcYear = new AcademicyearServices();
        //            Year = AcYear.date(yearId).AcademicYear1;
        //            var monthCount = getAttendanceMonth.Count;
        //            return Json(new { month1, presentDaysCount, Year, monthCount, monthName, CurrentmonthNameWorkingdays }, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            var Message = "You are not a class Teacher";
        //            return Json(Message, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(e.Message, JsonRequestBehavior.AllowGet);
        //    }
        //}

        public JsonResult studentAttendanceByMonth()
        {
            AssignClassToStudentServices classStu = new AssignClassToStudentServices();
            StudentDailyAttendanceServices attendance = new StudentDailyAttendanceServices();
            var getRow = classStu.getAssigendClass(StudentRegId);
            if (getRow != null)
            {
                int yearId = Convert.ToInt32(getRow.AcademicYearId);
                int classId = Convert.ToInt32(getRow.ClassId);
                int secId = Convert.ToInt32(getRow.SectionId);
                var data = attendance.getAttendanceMonth(yearId, classId, secId, StudentRegId);
                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var Message = "You are not a class Teacher";
                return Json(Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Skins(string skin)        
        {
            try
            {
               // string skin = "skin1";
                Session["skin"] = skin;
                var x = Session["StrippedBlack_Skin"];
                string Message = ResourceCache.Localize("skin_changed");
                string selVal = skin;
                return Json(new { Message, selVal }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getCurrentAcYear()
        {
            var CurrentAcademicYearId = 2;
            return Json(CurrentAcademicYearId, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CustomeActiveDate(int acid)
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var ans = AcYear.date(acid);
            //string sdate1 = ans.StartDate.ToString("dd/MM/yyyy");
            //string edate1 = ans.EndDate.ToString("dd/MM/yyyy");
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            EventServices holidays = new EventServices();
            var HolidaysList = holidays.getHolidaysList(acid);
            DateTime CurrentDate = DateTimeByZone.getCurrentDate();
            DateTime sdate = ans.StartDate;
            DateTime edate = ans.EndDate;
            if (sdate <= CurrentDate && CurrentDate <= edate)
            {
                StartingEnableDate = sdate.ToString("dd/MM/yyyy");

                return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                EndingEnableDate = edate.ToString("dd/MM/yyyy");
                return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }



        }
    }
}
