﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class SchoolManagementController : Controller
    {
        AcademicyearServices obj_academicYear = new AcademicyearServices();
        AnnouncementServices obj_announcement = new AnnouncementServices();
        TechEmployeeServices obj_employee = new TechEmployeeServices();
        FacilitiesServices obj_facility = new FacilitiesServices();
        FacilityInchargeServices obj_facIncharge = new FacilityInchargeServices();
        ExtraActivitiesServices obj_activity = new ExtraActivitiesServices();
        CircularServices obj_circular = new CircularServices();
        NewsTrendsServices obj_news = new NewsTrendsServices();
        ActivityInchargeServices obj_activityIncharge = new ActivityInchargeServices();
        AssignActivityToStudentServices obj_ActToStd = new AssignActivityToStudentServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ViewBag.acYear = obj_academicYear.AllList_AcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
            ViewBag.Employee = obj_employee.EmployeeList();
            ViewBag.Facility = obj_facility.getFacilities();
            ViewBag.Activity = obj_activity.getExtraActivity();
        }
        public ActionResult Facilities()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        
        public JsonResult FacilityResults(string sidx, string sord, int page, int rows, int? FAcademicYrId)
        {
             IList<FaciltyIncharge> todoListsResults1 = new List<FaciltyIncharge>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults1 = obj_facIncharge.getFacilityIncharge();
            if (FAcademicYrId.HasValue)
            {
                todoListsResults1 = todoListsResults1.Where(p => p.Fac_AcYearId == FAcademicYrId).ToList();
            }
            int totalRecords = todoListsResults1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults1 = todoListsResults1.OrderByDescending(s => s.Fac_AcYear).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults1 = todoListsResults1.OrderBy(s => s.Fac_AcYear).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults1;
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FacilityExportToExcel()
        {
            try
            {
                List<FaciltyIncharge> todoListsResults1 = (List<FaciltyIncharge>)Session["JQGridList"];
                var list = todoListsResults1.Select(o => new { AcademicYear = o.Fac_AcYear, Facility = o.Fac, FacilityIncharge = o.Fac_Incharge, EmployeeId = o.EmpId, InchargeContact = o.EmpContact, Note = o.Note, Status = Convert.ToString(o.Status) }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("Facility&InchargeDetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult FacilityExportToPDF()
        {
            try
            {
                List<FaciltyIncharge> todoListsResults1 = (List<FaciltyIncharge>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("Facility&InchargeDetails");
                GeneratePDF.ExportPDF_Portrait(todoListsResults1, new string[] { "AcademicYear", "Facility", "FacilityIncharge", "EmployeeId", "InchargeContact", "Note", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "FacilityIncharge.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Facilities(FaciltyIncharge model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = model.Fac_AcYearId;
                    int fid = model.FacId;
                    int empRegId = model.EmpRegId;
                    string empId = model.EmpId;
                    string note = model.Note;
                    bool cc = obj_facIncharge.checkFacilityIncharge(acid, fid, empId, empRegId);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Data_is_already_Available");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_facIncharge.addFacilityIncharge(acid, fid, empRegId, note, empId);
                        string Message = eAcademy.Models.ResourceCache.Localize("Facility_Incharge_Added_Successfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("AddFacilityIncharge"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getFacility()
        {
            FacilitiesServices obj_facility = new FacilitiesServices();
            try
            {
                var ans = obj_facility.getFacilities();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getEmployee()
        {
            try
            {
                var ans = obj_employee.getEmployees();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getEmployeeId(string EmpRegid)
        {
            try
            {
                var ans = obj_employee.getEmployeeID(EmpRegid);
                var empid = ans.EmployeeId;
                return Json(new { ans = empid }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditFacilityIncharge(string FacilityId)
        {
            try
            {
                string ids = QSCrypt.Decrypt(FacilityId);
                var idlist = ids.Split('/');
                int FId = Convert.ToInt32(idlist[0]);
                string EmpId = idlist[1];
                var ans = obj_facIncharge.FacilityIncharge(FId, EmpId);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditFacilityIncharge(EditFaciltyIncharge model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    TechemployeesServices tes = new TechemployeesServices();
                    string ids = QSCrypt.Decrypt(model.FacilityInchargeId);
                    var idlist = ids.Split('/');
                    int FId = Convert.ToInt32(idlist[0]);
                    int acid = model.Fac_AcYearId;
                    int fid = model.FacId;
                    string empid = model.EmpId;
                    int empRegId = tes.GetEmployeeRegId(empid);
                    string note = model.Note;
                    string Status = model.Status;
                    obj_facIncharge.updateFacilityIncharge(FId, acid, fid, empid, empRegId, note, Status);
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("UpdateFacilityIncharge"));
                    string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [EncryptedActionParameter]
        public ActionResult DeleteFacilityIncharge(int facId)
        {
            try
            {
                obj_facIncharge.deleteFacilityIncharge(facId);
                TempData["Message"] = eAcademy.Models.ResourceCache.Localize("Removed_Successfully");
                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("DeleteFacilityIncharge"));
                return RedirectToAction("Facilities");
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("Facilities");
            }
        }
        public ActionResult ExtraActivities()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
       
        public JsonResult ExtraActivityResults(string sidx, string sord, int page, int rows, int? FAcademicYrId)
        {
            IList<eAcademy.Models.ActivityIncharge> todoListsResults2 = new List<eAcademy.Models.ActivityIncharge>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults2 = obj_activityIncharge.getAcitityIncharge();
            if (FAcademicYrId.HasValue)
            {
                todoListsResults2 = todoListsResults2.Where(p => p.Fac_AcYearId == FAcademicYrId).ToList();
            }
            int totalRecords = todoListsResults2.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults2 = todoListsResults2.OrderByDescending(s => s.Fac_AcYear).ToList();
                    todoListsResults2 = todoListsResults2.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults2 = todoListsResults2.OrderBy(s => s.Fac_AcYear).ToList();
                    todoListsResults2 = todoListsResults2.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults2;
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults2
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActivityExportToExcel()
        {
            try
            {
                List<eAcademy.Models.ActivityIncharge> todoListsResults2 = (List<eAcademy.Models.ActivityIncharge>)Session["JQGridList"];
                var list = todoListsResults2.Select(o => new { AcademicYear = o.Fac_AcYear, Activity = o.Activity, ActivityIncharge = o.Fac_Incharge, EmployeeId = o.EmpId, EmployeeContactNo = o.EmployeeContactNo, Description = o.Description, Status = Convert.ToString(o.Status) }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("Activity&InchargeDetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ActivityExportToPDF()
        {
            try
            {
                List<eAcademy.Models.ActivityIncharge> todoListsResults2 = (List<eAcademy.Models.ActivityIncharge>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("Activity&InchargeDetails");
                GeneratePDF.ExportPDF_Portrait(todoListsResults2, new string[] { "AcademicYear", "Activity", "ActivityIncharges", "EmployeeId", "EmployeeContactNo", "Description", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "ActivityIncharge.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public JsonResult getActivity()
        {
            try
            {
                var ans = obj_activity.getExtraActivity();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ExtraActivities(eAcademy.Models.ActivityIncharge model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = model.Fac_AcYearId;
                    int aid = model.ActivityId;
                    int empRegId = model.EmpRegId;
                    string empId = model.EmpId;
                    string note = model.Note;
                    bool cc = obj_activityIncharge.checkActivityIncharge(acid, aid, empId, empRegId);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Data_is_already_Available");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_activityIncharge.addActivityIncharge(acid, aid, empRegId, note, empId);
                        string Message = eAcademy.Models.ResourceCache.Localize("Activity_Incharge_Added_Successfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("AddActivityIncharge"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditActivityIncharge(string ActivityId)
        {
            try
            {
                int aid = Convert.ToInt32(QSCrypt.Decrypt(ActivityId));
                var ans = obj_activityIncharge.getActivityInchargByID(aid);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditActivityIncharge(EditActivityIncharge model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int facId1 = Convert.ToInt32(QSCrypt.Decrypt(model.ActivityInchargeId));
                    int acid = model.Fac_AcYearId;
                    int fid = model.FacId;
                    string empid = model.EmpId;
                    int empRegId = model.EmpRegId;
                    string note = model.Note;
                    string Status = model.Status;
                    obj_activityIncharge.updateActivityIncharge(facId1, acid, fid, empid, empRegId, note, Status);
                    string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("UpdateActivityIncharge"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [EncryptedActionParameter]
        public ActionResult DeleteActivityIncharge(int aid)
        {
            try
            {
                obj_activityIncharge.deleteActivityIncharge(aid);
                TempData["Message"] = eAcademy.Models.ResourceCache.Localize("Removed_Successfully");
                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("DeleteActivityIncharge"));
                return RedirectToAction("ExtraActivities");
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("ExtraActivities");
            }
        }
       
        public JsonResult StudentActivityResults(string sidx, string sord, int page, int rows, int? FAcademicYrId1)    //, string Achievement, DateTime? txt_doa_search
        {
             IList<eAcademy.Models.ActivityIncharge> todoListsResults3 = new List<eAcademy.Models.ActivityIncharge>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults3 = obj_ActToStd.getActivityStudent();
            if (FAcademicYrId1.HasValue)
            {
                todoListsResults3 = todoListsResults3.Where(p => p.Fac_AcYearId == FAcademicYrId1).ToList();
            }
            int totalRecords = todoListsResults3.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults3 = todoListsResults3.OrderByDescending(s => s.Fac_AcYear).ToList();
                    todoListsResults3 = todoListsResults3.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults3 = todoListsResults3.OrderBy(s => s.Fac_AcYear).ToList();
                    todoListsResults3 = todoListsResults3.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults3;
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults3
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult StudentAcivityExportToExcel()
        {
            try
            {
                List<eAcademy.Models.ActivityIncharge> todoListsResults3 = (List<eAcademy.Models.ActivityIncharge>)Session["JQGridList"];
                var list = todoListsResults3.Select(o => new { AcademicYear = o.Fac_AcYear, ClassSection = o.ClassSection, Activity = o.Activity, Student = o.Student, Description = o.Description, Status = Convert.ToString(o.Status) }).ToList(); //ActivityIncharge = o.Fac_Incharge,
                string fileName = eAcademy.Models.ResourceCache.Localize("StudentActivityDetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult StudentActivityExportToPDF()
        {
            try
            {
                List<eAcademy.Models.ActivityIncharge> todoListsResults3 = (List<eAcademy.Models.ActivityIncharge>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("StudentActivityDetails");
                GeneratePDF.ExportPDF_Portrait(todoListsResults3, new string[] { "AcademicYear", "ClassSection", "Activity", "Student", "Description", "Status" }, xfilePath, fileName);// "ActivityIncharges",
                return File(xfilePath, "application/pdf", "StudentActivityList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult StudentActivity()
        {
            try
            {
                TempData["Std_List"] = "Student List";
                return RedirectToAction("ExtraActivities");
            }
            catch (Exception e)
            {
                TempData["Std_List"] = "Student List";
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("ExtraActivities");
            }
        }
        public JsonResult getClassList()
        {
            try
            {
                ClassServices obj_class = new ClassServices();
                var ans = obj_class.Classes();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getSectionList(int cid)
        {
            try
            {
                SectionServices obj_section = new SectionServices();
                var ans = obj_section.getSectionByClassId(cid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getStudent(int acid, int cid, int sec_id)
        {
            StudentServices obj_student = new StudentServices();
            try
            {
                var ans = obj_student.getStudentByID(sec_id, cid, acid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveStudentActivity(SaveStudentActivity model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = model.Fac_AcYearId;
                    int cid = model.Act_ClassId;
                    int sec_id = model.Act_SectionId;
                    int std_id = model.Act_StudentId;
                    int aid = model.ActivityId;
                    string note = model.Note;
                    bool cc = obj_ActToStd.checkActivityStudent(acid, cid, sec_id, std_id, aid);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Data_is_already_Available");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_ActToStd.addActivityStudent(acid, cid, sec_id, std_id, aid, note);
                        string Message = eAcademy.Models.ResourceCache.Localize("StudentAddedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("AddActivityToStudent"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditStudentActivity(string std_Activityid)
        {
            try
            {
                StudentServices obj_student = new StudentServices();
                int aid = Convert.ToInt32(QSCrypt.Decrypt(std_Activityid));
                var ans = obj_activity.getActivityByActivityID(aid);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditStudentActivity(EditStudentActivity model)
        {
            try
            {
                StudentServices obj_student = new StudentServices();
                if (ModelState.IsValid)
                {
                    int s_aid = Convert.ToInt32(QSCrypt.Decrypt(model.StudentActivityId));
                    int acid = model.Fac_AcYearId;
                    int std_id = model.Act_StudentId;
                    int cid = model.Act_ClassId;
                    int sec_id = model.Act_SectionId;
                    int aid = model.ActivityId;
                    string note = model.Note;
                    string Status = model.Status;
                    obj_ActToStd.updateActivityStudent(s_aid, acid, cid, sec_id, std_id, aid, note, Status);
                    string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("UpdateActivityToStudent"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [EncryptedActionParameter]
        public ActionResult DeleteEditStudentActivity(int s_aid)
        {
            try
            {
                obj_ActToStd.deleteActivityStudent(s_aid);
                TempData["Message"] = eAcademy.Models.ResourceCache.Localize("Removed_Successfully");
                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("DeleteActivityToStudent"));
                return RedirectToAction("ExtraActivities");
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("ExtraActivities");
            }
        }
        public ActionResult NewsTrends()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
       
        public JsonResult NewsTrendsResults(string sidx, string sord, int page, int rows, int? FAcademicYrId)
        {
              IList<NewsTrends> todoListsResults4 = new List<NewsTrends>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults4 = obj_news.getNewsTrends();
            if (FAcademicYrId.HasValue)
            {
                todoListsResults4 = todoListsResults4.Where(p => p.acYear == FAcademicYrId).ToList();
            }
            int totalRecords = todoListsResults4.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults4 = todoListsResults4.OrderByDescending(s => s.AcademicYear).ToList();
                    todoListsResults4 = todoListsResults4.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults4 = todoListsResults4.OrderBy(s => s.AcademicYear).ToList();
                    todoListsResults4 = todoListsResults4.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults4;
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults4
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult NewsExportToExcel()
        {
            try
            {
                List<NewsTrends> todoListsResults4 = (List<NewsTrends>)Session["JQGridList"];
                var list = todoListsResults4.Select(o => new { AcademicYear = o.AcademicYear, Heading = o.Heading, Date = o.Date, Description = o.Desc, Status = Convert.ToString(o.Status) }).ToList();
                string fileName = "News&TrendsDetails";
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult NewsExportToPDF()
        {
            try
            {
                List<NewsTrends> todoListsResults4 = (List<NewsTrends>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = "News & Trends Details";
                GeneratePDF.ExportPDF_Portrait(todoListsResults4, new string[] { "AcademicYear", "Heading", "Date", "Desc", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "NewsTrends.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult NewsTrends(NewsTrends model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = model.acYear;
                    DateTime? date = model.tacYSD;
                    string heading = model.Heading;
                    string desc = model.Desc;
                    obj_news.addNewsTrends(acid, date, heading, desc);
                    string Message = "News & Trends Added Successfully";
                    useractivity.AddEmployeeActivityDetails("Add news trends");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditNewsTrends(string newsId)
        {
            try
            {
                int NId = Convert.ToInt32(QSCrypt.Decrypt(newsId));
                var ans = obj_news.getNewsTrendByID(NId);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditNewsTrends(EditNews model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DateTime date = model.tacYSD.Value;
                    string heading = model.Heading;
                    string desc = model.Desc;
                    string Status = model.Status;
                    int newsId = Convert.ToInt32(QSCrypt.Decrypt(model.NewsId));
                    obj_news.updateNewsTrends(newsId, date, heading, desc, Status);
                    useractivity.AddEmployeeActivityDetails("Update news trends");
                    string Message = "UpdatedSuccessfully";
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [EncryptedActionParameter]
        public ActionResult DeleteNewsTrends(int newsId)
        {
            try
            {
                obj_news.deleteNewsTrends(newsId);
                useractivity.AddEmployeeActivityDetails("Delete news trends");
                TempData["Message"] = "Removed Successfully";
                return RedirectToAction("NewsTrends");
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("NewsTrends");
            }
        }
        public ActionResult Announcement()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        
        public JsonResult AnnouncementResults(string sidx, string sord, int page, int rows, int? FAcademicYrId)    //, string Achievement, DateTime? txt_doa_search
        {
             IList<Announcements> todoListsResults5 = new List<Announcements>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults5 = obj_announcement.getAnnouncements();
            if (FAcademicYrId.HasValue)
            {
                todoListsResults5 = todoListsResults5.Where(p => p.acYear == FAcademicYrId).ToList();
            }
            int totalRecords = todoListsResults5.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults5 = todoListsResults5.OrderByDescending(s => s.AcademicYear).ToList();
                    todoListsResults5 = todoListsResults5.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults5 = todoListsResults5.OrderBy(s => s.AcademicYear).ToList();
                    todoListsResults5 = todoListsResults5.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults5;
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults5
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AnnouncementExportToExcel()
        {
            try
            {
                List<Announcements> todoListsResults5 = (List<Announcements>)Session["JQGridList"];
                var list = todoListsResults5.Select(o => new { AcademicYear = o.AcademicYear, Heading = o.Heading, Date = o.Date, Description = o.Desc, Status = Convert.ToString(o.Status) }).ToList();
                string fileName = "AnnouncementDetails";
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult AnnouncementExportToPDF()
        {
            try
            {
                List<Announcements> todoListsResults5 = (List<Announcements>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = "Announcement Details";
                GeneratePDF.ExportPDF_Portrait(todoListsResults5, new string[] { "AcademicYear", "Heading", "Date", "Desc", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "Announcement.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Announcement(Announcements model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = model.acYear;
                    DateTime date = model.tacYSD.Value;
                    string heading = model.Heading;
                    string desc = model.Desc;
                    obj_announcement.addAnnouncements(acid, date, heading, desc);
                    useractivity.AddEmployeeActivityDetails("Add announcements");
                    string Message = "Announcement Added Successfully";
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditAnnouncement(string AnnouncementId)
        {
            try
            {
                int AId = Convert.ToInt32(QSCrypt.Decrypt(AnnouncementId));
                var ans = obj_announcement.getAnnouncementByAnnouncementID(AId);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditAnnouncement(EditAnnouncement model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DateTime date = model.txt_Announcement_date;
                    string heading = model.txt_Announcement;
                    string desc = model.txt_Announcement_desc;
                    string Status = model.Status;
                    int newsId = Convert.ToInt32(QSCrypt.Decrypt(model.AnnouncementId));
                    obj_announcement.updateAnnouncement(newsId, date, heading, desc, Status);
                    string Message ="UpdatedSuccessfully";
                    useractivity.AddEmployeeActivityDetails("Update announcements");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [EncryptedActionParameter]
        public ActionResult DeleteAnnouncement(int AnnouncementId)
        {
            try
            {
                obj_announcement.deleteAnnouncement(AnnouncementId);
                useractivity.AddEmployeeActivityDetails("Delete announcements");
                TempData["Message"] = "Removed_Successfully";
                return RedirectToAction("Announcement");
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("Announcement");
            }
        }
        public ActionResult Circular()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        static IList<Circulars> todoListsResults6 = new List<Circulars>();
        public JsonResult CircularResults(string sidx, string sord, int page, int rows, int? FAcademicYrId)    //, string Achievement, DateTime? txt_doa_search
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults6 = obj_circular.getCircular();
            if (FAcademicYrId.HasValue)
            {
                todoListsResults6 = todoListsResults6.Where(p => p.acYear == FAcademicYrId).ToList();
            }
            int totalRecords = todoListsResults6.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults6 = todoListsResults6.OrderByDescending(s => s.AcademicYear).ToList();
                    todoListsResults6 = todoListsResults6.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults6 = todoListsResults6.OrderBy(s => s.AcademicYear).ToList();
                    todoListsResults6 = todoListsResults6.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults6
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CircularExportToExcel()
        {
            try
            {
                var list = todoListsResults6.Select(o => new { AcademicYear = o.AcademicYear, Heading = o.Heading, Date = o.Date, Reason = o.Reason, Status = Convert.ToString(o.Status) }).ToList();
                string fileName = "CircularDetails";
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult CircularExportToPDF()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = "Circular Details";
                GeneratePDF.ExportPDF_Portrait(todoListsResults6, new string[] { "AcademicYear", "Heading", "Date", "Reason", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "Circular.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Circular(Circulars model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = model.acYear;
                    DateTime date = model.tacYSD.Value;
                    string heading = model.Heading;
                    string reason = model.Reason;
                    int issuedBy = Convert.ToInt32(Session["empRegId"]);
                    obj_circular.addCircular(acid, date, heading, reason, issuedBy);
                    useractivity.AddEmployeeActivityDetails("Add circular");
                    string Message = "Circular Added Successfully";
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditCircular(string CircularId)
        {
            try
            {
                int CId = Convert.ToInt32(QSCrypt.Decrypt(CircularId));
                var ans = obj_circular.getCircular(CId);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditCircular(EditCircular model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DateTime date = model.tacYSD.Value;
                    string heading = model.Heading;
                    string reason = model.Reason;
                    string Status = model.Status;
                    int CircularId = Convert.ToInt32(QSCrypt.Decrypt(model.CircularId));
                    int issuedBy = Convert.ToInt32(Session["empRegId"]);
                    obj_circular.updateCircular(CircularId, date, heading, reason, issuedBy, Status);
                    string Message = "UpdatedSuccessfully";
                    useractivity.AddEmployeeActivityDetails("update circular");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [EncryptedActionParameter]
        public ActionResult DeleteCircular(int CircularId)
        {
            try
            {
                obj_circular.deleteCircular(CircularId);
                useractivity.AddEmployeeActivityDetails("Delete circular");
                TempData["Message"] = "Removed_Successfully";
                return RedirectToAction("Circular");
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("Circular");
            }
        }
        public JsonResult getDate(int acid)
        {
            try
            {
                var ans = obj_academicYear.AcademicYears(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                return Json(new { sd = sdate, ed = edate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}