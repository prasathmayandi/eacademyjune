﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class FeeConfigurationController : Controller
    {
        AcademicyearServices obj_academicYear = new AcademicyearServices();
        TermServices obj_term = new TermServices();
        ClassServices obj_class = new ClassServices();
        FeeCategoryServices obj_FeeCategory = new FeeCategoryServices();
        FeeServices obj_Fee = new FeeServices();
        TermFeeServices obj_termFee = new TermFeeServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        public int AdminId;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ViewBag.acYear = obj_academicYear.AllList_AcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
            ViewBag.ClassList = obj_class.Classes();
            ViewBag.feeCategory = obj_FeeCategory.getFeeCategory();
            ViewBag.class_Fees = obj_class.Classes();
            AdminId = Convert.ToInt32(Session["empRegId"]);
        }
        public ActionResult FeeCategory()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        
        public JsonResult FeeCategoryResults(string sidx, string sord, int page, int rows, int? ddFeeCategory)
        {
            IList<FeeCategoryList> todoListsResults1 = new List<FeeCategoryList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults1 = obj_FeeCategory.getFeeCategory();
            if (ddFeeCategory.HasValue)
            {
                todoListsResults1 = todoListsResults1.Where(p => p.FeeCatId == ddFeeCategory).ToList();
            }
            int totalRecords = todoListsResults1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults1 = todoListsResults1.OrderByDescending(s => s.FeeType).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults1 = todoListsResults1.OrderBy(s => s.FeeType).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults1;
           
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FeeCategoryExportToExcel()
        {
            try
            {
                List<FeeCategoryList> todoListsResults1 = (List<FeeCategoryList>)Session["JQGridList"];
                var list = todoListsResults1.Select(o => new { FeeCategory = o.FeeType, Status = Convert.ToString(o.Status) }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("FeeCategoryList");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult FeeCategoryExportToPDF()
        {
            try
            {
                List<FeeCategoryList> todoListsResults1 = (List<FeeCategoryList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("FeeCategoryList");
                GeneratePDF.ExportPDF_Portrait(todoListsResults1, new string[] { "FeeCategory", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "FeeCategoryList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult FeeCategory(SaveFeeCategory model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string ftype = model.txt_feetype;
                    bool cc = obj_FeeCategory.checkFeeCategory(ftype);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Fee_Category_is_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_FeeCategory.addFeeCategory(ftype);
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Addfeecategory"));
                        string Message = eAcademy.Models.ResourceCache.Localize("Fee_Category_Added_Successfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditFeeCategory(int FeeId)
        {
            try
            {
                var ans = obj_FeeCategory.getFeeCategoryByFeeCategoryID(FeeId);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditFeeCategory(SaveFeeCategory model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int fid = model.feeid;
                    string ftype = model.txt_feetype;
                    string status = model.Status;
                    bool cc = obj_FeeCategory.checkFeeCategory(ftype, status, fid);
                    if (cc == true)
                    {
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Updatefeecategory"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Fee_Category_is_already_used,so_never_change_the_status");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [EncryptedActionParameter]
        public ActionResult DeleteFeeCategory(int fid)
        {
            try
            {

                obj_FeeCategory.deleteFeeCategory(fid);
                TempData["Message"] = eAcademy.Models.ResourceCache.Localize("Removed_Successfully");
                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Removefeecategory"));
                return RedirectToAction("FeeCategory");
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("FeeCategory");
            }
        }
        public ActionResult FeeConfig()
        {
            try
            {
                ViewBag.count = obj_Fee.getFeeConfig().Count;
                return View(obj_Fee.getFeeConfig());
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        
        public JsonResult FeeConfigResults(string sidx, string sord, int page, int rows, int? ddAcademicYear)
        {
            IList<ConfigFee> todoListsResults2 = new List<ConfigFee>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults2 = obj_Fee.getFeeConfig();
            if (ddAcademicYear.HasValue)
            {
                todoListsResults2 = todoListsResults2.Where(p => p.AcYearId == ddAcademicYear).ToList();
            }
            int totalRecords = todoListsResults2.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults2 = todoListsResults2.OrderByDescending(s => s.AcYearId).ToList();
                    todoListsResults2 = todoListsResults2.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults2 = todoListsResults2.OrderBy(s => s.AcYearId).ToList();
                    todoListsResults2 = todoListsResults2.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults2;
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults2
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FeeConfigExportToExcel()
        {
            try
            {
                List<ConfigFee> todoListsResults2 = (List<ConfigFee>)Session["JQGridList"];
                var list = todoListsResults2.Select(o => new { AcademicYear = o.AcYear, Class = o.Class, FeeCategory = o.FeeCategory, Amount = o.Amount, Tax = o.Tax, ReFund = o.ReFund, Total = o.Total, LastDate = o.Date }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassFeeList");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult FeeConfigExportToPDF()
        {
            try
            {
                List<ConfigFee> todoListsResults2 = (List<ConfigFee>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassFeeList");
                GeneratePDF.ExportPDF_Portrait(todoListsResults2, new string[] { "AcYear", "Class", "FeeCategory", "Amount", "Tax", "ReFund", "Total", "LastDates" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "FeeConfig.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public JsonResult getTerms(int acid)
        {
            try
            {
                var ans = obj_term.TermOnYear(acid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getTermsCount(int acid)
        {
            try
            {
                var ans = obj_term.TermOnYear(acid);
                var count = ans.Count;
                return Json(count, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult FeeConfig(SaveFeeConfig model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = model.acYear_Fees;
                    int fid = model.feeCategory;
                    Decimal fee = model.txt_fees;
                    Decimal ServicesTax = model.ServicesTax;
                    Decimal total_amount = model.Total;
                    DateTime ldate = model.txt_ldate;
                    string check = model.txt_checkterm;
                    string checkclass = model.txt_checkclass;
                    int count = model.Count;
                    if (checkclass == "on")
                    {
                        var ans = obj_class.getAllClassId();
                        for (int k = 0; k < ans.Count; k++)
                        {
                            int id = ans[k];
                            bool cc = obj_Fee.checkFeeConfig(acid, fid, id);
                            if (cc == true)
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Fee_Category_is_already_Exist");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                obj_Fee.addFeeConfig(acid, fid, id, fee, ldate, ServicesTax, total_amount);
                            }
                            if (check == "on")
                            {
                                int[] termid = new int[count];
                                Decimal[] amt = new Decimal[count];
                                Decimal[] term_tax = new Decimal[count];
                                Decimal[] term_total = new Decimal[count];
                                DateTime[] lastdate = new DateTime[count];
                                for (int i = 0; i < (count); i++)
                                {
                                    termid[i] = model.Termid[i];
                                    amt[i] = model.Fees[i];
                                    term_tax[i] = model.Tax[i];
                                    term_total[i] = model.Term_Total[i];
                                    lastdate[i] = model.LastDate[i];
                                }
                                for (int i = 0; i < (count); i++)
                                {
                                    bool ct = obj_termFee.checkTermFee(acid, id, fid, termid[i]);
                                    if (cc == true)
                                    {
                                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Fee_Category_is_already_Exist");
                                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                    {
                                        var getfeeid = obj_Fee.getFeeIdByID(acid, id, fid);
                                        obj_termFee.addTermFee(acid, id, fid, termid[i], amt[i], lastdate[i], getfeeid.FeeId, term_tax[i], term_total[i]);
                                    }
                                }
                            }
                        }
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Addfees"));
                        string Message = eAcademy.Models.ResourceCache.Localize("FeesAddedSuccessfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string classid = model.class_Fees;
                        string[] c_id = classid.Split(',');
                        for (int a = 0; a < c_id.Length; a++)
                        {
                            int cid = Convert.ToInt32(c_id[a]);
                            bool cc = obj_Fee.checkFeeConfig(acid, fid, cid);
                            if (cc == true)
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Fee_Category_is_already_Exist");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                obj_Fee.addFeeConfig(acid, fid, cid, fee, ldate, ServicesTax, total_amount);
                            }
                            if (check == "on")
                            {
                                int[] termid = new int[count];
                                Decimal[] amt = new Decimal[count];
                                Decimal[] term_tax = new Decimal[count];
                                Decimal[] term_total = new Decimal[count];
                                DateTime[] lastdate = new DateTime[count];
                                for (int i = 0; i < (count); i++)
                                {
                                    termid[i] = model.Termid[i];
                                    amt[i] = model.Fees[i];
                                    term_tax[i] = model.Tax[i];
                                    term_total[i] = model.Term_Total[i];
                                    lastdate[i] = model.LastDate[i];
                                }
                                for (int i = 0; i < (count); i++)
                                {
                                    bool ct = obj_termFee.checkTermFee(acid, cid, fid, termid[i]);
                                    if (cc == true)
                                    {
                                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Fee_Category_is_already_Exist");
                                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                    {
                                        var getfeeid = obj_Fee.getFeeIdByID(acid, cid, fid);
                                        obj_termFee.addTermFee(acid, cid, fid, termid[i], amt[i], lastdate[i], getfeeid.FeeId, term_tax[i], term_total[i]);
                                    }
                                }
                            }
                        }
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Addfees"));
                        string Message = eAcademy.Models.ResourceCache.Localize("FeesAddedSuccessfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditFeeConfig(string FeeId)
        {
            try
            {

               int fId = Convert.ToInt32(QSCrypt.Decrypt(FeeId));
                var ans = obj_Fee.getConfigFee(fId);
                bool existacademicyear = obj_academicYear.PastAcademicyear(ans.AcYearId);
                if (existacademicyear == true)
                {
                    bool CheckFeespaid = obj_Fee.Checkfeesalreadypaid(fId);
                    if (CheckFeespaid == true)
                    {
                       string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Student_paid_selected_fees,so_you_can_use_refund_action");
                       var emp = "";
                       return Json(new { ErrorMessage = ErrorMessage, ans = emp }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
                    }
                else
                {
                    string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                    return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditFeeConfig(EditFeeConfig model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int feeid = Convert.ToInt32(QSCrypt.Decrypt(model.feeid));
                    int acid = model.acYear_Fees;
                    int cid = model.class_Fees;
                    int fid = model.feeCategory;
                    Decimal fee = model.txt_fees;
                    Decimal ServicesTax = model.ServicesTax;
                    Decimal Total = model.Total;
                    DateTime ldate = model.LastDate;
                    bool cc = obj_Fee.checkFeeConfig(acid, fid, cid, fee, ServicesTax, Total, ldate);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Fee_Config_is_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        bool ss = obj_Fee.updateFeeConfig(acid, cid, fid, feeid, fee, ServicesTax, Total, ldate);
                        if (ss == true)
                        {
                            string Message = eAcademy.Models.ResourceCache.Localize("FeesUpdatedSuccessfully");
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Updatefees"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Students_already_paid_to_this_fees,_so_never_updated");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DeleteFee(string passId)
        {
            try
            {
                int Did = Convert.ToInt32(QSCrypt.Decrypt(passId));
                var ans = obj_Fee.getConfigFee(Did);
                bool existacademicyear = obj_academicYear.PastAcademicyear(ans.AcYearId);
                if (existacademicyear == true)
                {
                    FeeServices fee = new FeeServices();
                  
                    bool ss = fee.deleteFeeConfig(Did);
                    if (ss == true)
                    {
                        string Message = eAcademy.Models.ResourceCache.Localize("Fees_details_deleted_Successfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Deletefees"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("This_fees_already_used,so_never_deleted");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                    return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ErrorMessage = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [EncryptedActionParameter]
        public ActionResult DeleteFeeConfig(int feeid)
        {
            try
            {
                obj_FeeCategory.deleteFeeCategory(feeid);
                TempData["Message"] = eAcademy.Models.ResourceCache.Localize("Removed_Successfully");
                ViewBag.count = obj_Fee.getFeeConfig().Count;
                return RedirectToAction("FeeConfig");
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("FeeConfig");
            }
        }
        public ActionResult RefundFeeConfig(string FeeId)
        {
            try
            {
                int fId = Convert.ToInt32(QSCrypt.Decrypt(FeeId));
                // 
                bool CheckFeespaid = obj_Fee.Checkfeesalreadypaid(fId);
                if (CheckFeespaid == true)
                {
                    var ans = obj_Fee.getConfigFee(fId);
                    return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Student_not_paid_selected_fees,so_you_can_use_edit_action");
                    var ans = "";
                    return Json(new { ErrorMessage = ErrorMessage, ans = ans }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveRefundFeeConfig(VM_ReFundFeesConfig model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int feeid = Convert.ToInt32(QSCrypt.Decrypt(model.feeid));
                    int acid = model.acYear_Fees;
                    int cid = model.class_Fees;
                    int fid = model.feeCategory;
                    Decimal Total = model.Total;
                    Decimal refundamt = model.RefundAmount;
                    Decimal netamt = model.NetAmount;
                    string description = model.Description;
                    bool cc = obj_Fee.checkRefundFeeConfig(feeid, Total, refundamt, netamt);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Refund_Fee_Config_is_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        bool ss = obj_Fee.updateRefundFeeConfig(acid, cid, fid, feeid, Total, refundamt, netamt, description, AdminId);
                        if (ss == true)
                        {
                            string Message = eAcademy.Models.ResourceCache.Localize("Refund_Fees_Config_Updated_Successfully");
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_fees_refund_config"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Students_already_paid_to_this_fees,_so_never_updated");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getDate(int acid)
        {
            try
            {
                var ans = obj_academicYear.AcademicYears(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                return Json(new { sd = sdate, ed = edate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CurrentAcademic(int acid)
        {
            try
            {
                var ans = obj_academicYear.AcademicYears(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                EventServices holidays = new EventServices();
                //DateTime CurrentDate =DateTime.Today;
                DateTime CurrentDate = DateTimeByZone.getCurrentDateTime();
                var HolidaysList = holidays.getHolidaysList(acid);

                //bool cc = obj_termFee.CheckCurrentAcademicTerm(acid);
                //if (cc == true)
                //{
                //    string ErrorMessage = "HaveTerms";//eAcademy.Models.ResourceCache.Localize("Term_is_already_Exist");
                //    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                //}
                //else
                //{
                //    string ErrorMessage = "NoTerm";//eAcademy.Models.ResourceCache.Localize("Term_is_already_Exist");
                //    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                   
                //}
                if (CurrentDate <= ans.EndDate)
                {
                    return Json(new { sd = CurrentDate, ed = edate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string message = eAcademy.Models.ResourceCache.Localize("This_is_not_current_year");
                    return Json(new { Message = message }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CurrentAcademicTermCheck(int YearId)
        {
            bool cc = obj_termFee.CheckCurrentAcademicTerm(YearId);
                if (cc == true)
                {
                    string message = "HaveTerm";//eAcademy.Models.ResourceCache.Localize("Term_is_already_Exist");
                    return Json(new { Message = message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string message = "NoTerm";
                    return Json(new { Message = message }, JsonRequestBehavior.AllowGet);
                }              
            }
        
        public JsonResult CurrentAcademicterm(int acid, int tid)
        {
            try
            {
                var ans = obj_academicYear.Termdate(acid, tid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                DateTime CurrentDate = DateTimeByZone.getCurrentDateTime();
                
                if (CurrentDate <= ans.EndDate)
                {
                    if (CurrentDate >= ans.StartDate)
                    {
                        sdate = CurrentDate.ToString("dd/MM/yyyy");
                    }
                    return Json(new { StartingEnableDate = CurrentDate, EndingEnableDate = edate }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string message = eAcademy.Models.ResourceCache.Localize("This_is_not_current_year");
                    return Json(new { Message = message }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getTax()
        {
            try
            {
                SchoolSettingsServices obj_settings = new SchoolSettingsServices();
                var ans = obj_settings.SchoolSettings();
                Decimal? serviceTax = ans.ServicesTax;
                return Json(new { ans = serviceTax }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult TermFee(string id)
        {
            try
            {
                TempData["id"] = id;
                if (TempData["Editid1"] != null)
                {
                    TempData["Editid"] = TempData["Editid1"];
                    int feeid1 = Convert.ToInt32(TempData["Editid1"].ToString());
                    var ans1 = obj_Fee.getTermConfigFee(feeid1);
                    TempData["AcYear"] = ans1.AcYear;
                    TempData["AcYId"] = ans1.AcYearId;
                    TempData["Class"] = ans1.Class;
                    TempData["ClassId"] = ans1.ClassId;
                    TempData["FeeCategory"] = ans1.FeeCategory;
                    TempData["FeeId"] = ans1.FeeCategoryId;
                    TempData["Fee"] = ans1.Amount;
                    TempData["ServicesTax"] = ans1.Tax;
                    TempData["Total"] = ans1.Total;
                    TempData["ldate"] = ans1.LastDate.ToShortDateString();
                    TempData["TermId"] = ans1.TermId;
                    if (ans1.Tax == 0)
                    {
                        TempData["Check"] = 0;
                    }
                }
                int feeid = Convert.ToInt32(QSCrypt.Decrypt(id));
                ViewBag.count = obj_termFee.getTermFee(feeid).Count;
                return View(obj_termFee.getTermFee(feeid));
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        [EncryptedActionParameter]
        public ActionResult EditTermFeeConfig(int feeid)
        {
            string id = TempData["id"].ToString();
            TempData["id1"] = id;
            try
            {
                TempData["Editid1"] = feeid;
                int xy = Convert.ToInt32(TempData["Editid1"]);
                return RedirectToAction("TermFee", new { id = id });
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("TermFee", new { id = id });
            }
        }
        public ActionResult SaveEditTermFeeConfig(FormCollection frm, EditFeeConfig model)
        {
            string id = TempData["id1"].ToString();
            try
            {
                int feeid = Convert.ToInt32(frm["feeid1"]);
                TempData["Editid1"] = feeid;
                if (ModelState.IsValid)
                {
                    int acid = Convert.ToInt32(frm["acYear_Fees"]);
                    int cid = Convert.ToInt32(frm["class_Fees"]);
                    int fid = Convert.ToInt32(frm["feeCategory"]);
                    Decimal fee = Convert.ToDecimal(frm["txt_fees"]);
                    Decimal tax = Convert.ToDecimal(frm["txt_ServicesTax"]);
                    Decimal total = Convert.ToDecimal(frm["txt_Total"]);
                    DateTime ldate = DateTime.Parse(frm["txt_ldate"]);
                    bool cc = obj_Fee.checkTermFeeConfig(acid, fid, cid, fee, tax, total, ldate);
                    if (cc == true)
                    {
                        TempData["ErrorMessage"] = eAcademy.Models.ResourceCache.Localize("Fee_Config_is_already_Exist");
                    }
                    else
                    {
                        bool ss = obj_Fee.updateTermFeeConfig(acid, cid, fid, feeid, fee, tax, total, ldate);
                        if (ss == true)
                        {
                            TempData["Message"] = eAcademy.Models.ResourceCache.Localize("FeesUpdatedSuccessfully");
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Updatetermfees"));
                        }
                        else
                        {
                            TempData["ErrorMessage"] = eAcademy.Models.ResourceCache.Localize("Students_already_paid_to_this_fees,_so_never_updated");
                        }
                    }
                }
                else
                {
                    TempData["modelState"] = eAcademy.Models.ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                }
                return RedirectToAction("TermFee", new { id = id });
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return RedirectToAction("TermFee", new { id = id });
            }
        }
        public ActionResult FeeRefundConfigReport()
        {
            return View();
        }
        static IList<ConfigFee> todoListsResults3 = new List<ConfigFee>();
        public JsonResult FeeRefundConfigResults(string sidx, string sord, int page, int rows, int? acYear_Fees, int? ClassList)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults3 = obj_Fee.getRefundFeeConfig();
            if (acYear_Fees.HasValue && ClassList.HasValue)
            {
                todoListsResults3 = todoListsResults3.Where(p => p.AcYearId == acYear_Fees && p.ClassId == ClassList).ToList();
            }
            else if (acYear_Fees.HasValue)
            {
                todoListsResults3 = todoListsResults3.Where(p => p.AcYearId == acYear_Fees).ToList();
            }
            int totalRecords = todoListsResults3.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults3 = todoListsResults3.OrderByDescending(s => s.AcYearId).ToList();
                    todoListsResults3 = todoListsResults3.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults3 = todoListsResults3.OrderBy(s => s.AcYearId).ToList();
                    todoListsResults3 = todoListsResults3.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults3
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FeeRefundConfigExportToExcel()
        {
            try
            {
                var list = todoListsResults3.Select(o => new { AcademicYear = o.AcYear, Class = o.Class, FeeCategory = o.FeeCategory, Amount = o.Amount, Tax = o.Tax, TotalAmount = o.TotalAmount, ReFund = o.ReFund, PayableAmount = o.PayableAmount, Date = o.Date, EmployeeName = o.EmployeeName }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("FeeRefundConfigList");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult FeeRefundConfigExportToPDF()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("FeeRefundConfigList");
                GeneratePDF.ExportPDF_Portrait(todoListsResults3, new string[] { "AcademicYear", "Class", "FeeCategory", "Amount", "Tax", "TotalAmount", "ReFund", "PayableAmount", "Date", "EmployeeName" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "FeeRefundConfigList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
    }
}