﻿using eAcademy.ApiModels;
using eAcademy.ApiServices;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace eAcademy.Controllers.aceConnect
{
    public class CommunicationController : ApiController
    {
        AssignClassToStudentServices assignstudent = new AssignClassToStudentServices();
        int yearId;
        int classId;
        int secId;
        int studentRegId;

        public object Post(Token t)
        {
            if (t.date == null)
            {
                t.date = DateTimeByZone.getCurrentDate();     
            }
            string tokenId = t.token;
            TokenAuthSservices tok = new TokenAuthSservices();
            var checkValidUser = tok.IsValidUser(tokenId);
            if (checkValidUser != null)
            {
                studentRegId = t.StudentRegId;
                var classDetail = assignstudent.GetClassDetails(studentRegId);
                yearId = classDetail.AcademicYearId.Value;
                classId = classDetail.ClassId.Value;
                secId = classDetail.SectionId.Value;
                CommunicationServices comm = new CommunicationServices();
                DateTime? PrevDate = t.date.Value.Date;
                DateTime? NextDate = t.date.Value.Date.AddDays(1);
                var List = comm.GetCommunicationListByDate(yearId, classId, secId, studentRegId, PrevDate, NextDate);
                string UserValidation = "Success";
                return new { List, UserValidation };
            }
            else
            {
                string UserValidation = "Time expired. Login again";
                return new { UserValidation };
            }
        }
    }
}
