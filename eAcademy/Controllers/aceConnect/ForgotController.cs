﻿using eAcademy.ApiModels;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
 
namespace eAcademy.Controllers.aceConnect
{
    public class ForgotController : ApiController
    {
        EacademyEntities dc = new EacademyEntities();
        public object Post(Parent p)
        {
            var checkValidParent = (from a in dc.PrimaryUserRegisters where a.PrimaryUserId == p.parentId select a).FirstOrDefault();
            string message;
            string UserValidation;

            if (checkValidParent != null)
            {
                var checkFatherHaveEmail = checkValidParent.PrimaryUserEmail;
                if (checkFatherHaveEmail != null)
                {
                    Random random = new Random();
                    int rvalue = random.Next(1111,9999);
                    var uid = Guid.NewGuid();
                    int RegId = checkValidParent.PrimaryUserRegisterId;
                    string userName = checkValidParent.PrimaryUserName;
                    DateTime date = DateTimeByZone.getCurrentDate();

                    var Name = checkValidParent.PrimaryUserName;
                    var ToMail = checkValidParent.PrimaryUserEmail;

                    tblFatherResetPassword RP = new tblFatherResetPassword();
                    RP.Id = uid;
                    RP.UserId = RegId;
                    RP.UserName = userName;
                    RP.ResetRequestDateTime = date;
                    RP.VerifyCode = rvalue;
                    dc.tblFatherResetPasswords.Add(RP);
                    dc.SaveChanges();

                    var CheckRowExist = (from a in dc.tblFatherResetPasswords where a.UserName == userName select a).FirstOrDefault();
                    if (CheckRowExist != null)
                    {
                        var code = rvalue;
                        StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Template/AppResetPassword.html"));
                        string readFile = reader.ReadToEnd();
                        string myString = "";
                        myString = readFile;
                        myString = myString.Replace("$$name$$", Name.ToString());
                        myString = myString.Replace("$$code$$", code.ToString());
                        string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                        string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                        string PlaceName = "AceConnect-Forget-App reset password mail Sent";
                        string UserName="Parent";
                        string UserId = "";

                        Mail.SendMail(EmailDisplay, supportEmail, ToMail, ResourceCache.Localize("ResetPassword"), myString.ToString(), true, PlaceName, UserName, UserId);  
                    }
                }
                else
                {
                    message = "Invalid UserId";
                    UserValidation = "failure";
                    return new { message, UserValidation };
                }
                message = "Please enter the verfication code that was sent to the email address you provided.";
                UserValidation = "success";
                return new { message, UserValidation };
            }
            else {
                message = "Invalid UserId";
                UserValidation = "failure";
                return new { message, UserValidation };
            }          
             
        }                       
        }
    }
    

