﻿using eAcademy.ApiModels;
using eAcademy.ApiServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace eAcademy.Controllers.aceConnect
{
    public class FeeController : ApiController
    {
        FeeServices fee = new FeeServices();
        AssignClassToStudentServices assignstudent = new AssignClassToStudentServices();

        public object Post(Token t)
        {
            int yearId;
            int classId;
            int secId;
            int studentRegId;

            string tokenId = t.token;
            TokenAuthSservices tok = new TokenAuthSservices();
            var checkValidUser = tok.IsValidUser(tokenId);
            studentRegId = t.StudentRegId;
            if (checkValidUser != null)
            {
                var classDetail = assignstudent.GetClassDetails(studentRegId);
                yearId = classDetail.AcademicYearId.Value;
                classId = classDetail.ClassId.Value;
                secId = classDetail.SectionId.Value;
                var List = fee.getFeeDetail(yearId, classId, secId,studentRegId);
                string UserValidation = "Success";
                return new { List, UserValidation };
            }
            else
            {
                string UserValidation = "Time expired. Login again";
                return new { UserValidation };
            }
        }
    }
}
