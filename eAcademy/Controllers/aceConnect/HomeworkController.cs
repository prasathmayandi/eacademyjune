﻿using eAcademy.ApiModels;
using eAcademy.ApiServices;
using eAcademy.HelperClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace eAcademy.Controllers.aceConnect
{
    public class HomeworkController : ApiController
    {
        AssignClassToStudentServices assignstudent = new AssignClassToStudentServices();
        int yearId;
        int classId;
        int secId;
        int studentRegId;

        public object Post(Homework h)
        {
            if (h.hwdate == null)
            {
                h.hwdate = DateTimeByZone.getCurrentDate();                
            }
            string tokenId = h.token;
            TokenAuthSservices tok = new TokenAuthSservices();
            var checkValidUser = tok.IsValidUser(tokenId);
            if (checkValidUser != null)
            {
                studentRegId = h.StudentRegId;
                var classDetail = assignstudent.GetClassDetails(studentRegId);
                yearId = classDetail.AcademicYearId.Value;
                classId = classDetail.ClassId.Value;
                secId = classDetail.SectionId.Value;
                HomeworkServices hw = new HomeworkServices();
                var List = hw.GetDateHomeworkList(yearId, classId, secId, h.hwdate.Value.Date);
                string UserValidation = "Success";
                return new { List, UserValidation };
            }
            else
            {
                string UserValidation = "Time expired. Login again";
                return new { UserValidation };
            }
        }
    }
}
