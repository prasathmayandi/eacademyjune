﻿using eAcademy.ApiModels;
using eAcademy.ApiServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace eAcademy.Controllers.aceConnect
{
    public class LogoutController : ApiController
    {
        TokenAuthSservices tok = new TokenAuthSservices();

        public string PostLogout(Token t)
        {
            tok.Logout(t.token);
            string logout = "Logout successfully";
            return logout;
        }
    }
}
