﻿using eAcademy.ApiModels;
using eAcademy.ApiServices;
using eAcademy.HelperClass;
using Newtonsoft.Json.Linq;
using PushBots.NET;
using PushBots.NET.Enums;
using PushBots.NET.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace eAcademy.Controllers.aceConnect
{
    public class PushNotificationController : ApiController
    {
        private readonly PushBotsClient _pushBotsClient;

        public PushNotificationController()
        {
            var AppId = ConfigurationManager.AppSettings["AppId"]; // Use your Application ID found in the PushBots Dashboard
            var Secret = ConfigurationManager.AppSettings["SecretId"]; // Use your Secret Key found in the PushBots Dashboard
            _pushBotsClient = new PushBotsClient(AppId, Secret);
        }
        public object Post(PushToken t)
        {
            string tokenId = t.token;
            string ntoken = t.ntoken;
            TokenAuthSservices tok = new TokenAuthSservices();
            var checkValidUser = tok.IsValidUser(tokenId);
            if (checkValidUser != null)
            {
                int ParentRegId = checkValidUser.UserId.Value;
                NotificationServices noti = new NotificationServices();
                var Add = noti.addpushtoken(ParentRegId, ntoken);
                string UserValidation = "Success";
                return new { Add, UserValidation };
            }
            else
            {
                string UserValidation = "Time expired. Login again";
                return new { UserValidation };
            }
        }
        public async Task<HttpResponseMessage> get()
        {
            try
            {  
                AnnouncementServices annou = new AnnouncementServices();
                var getMessage = annou.PushNotifi();
                string AnnouncementMessage = getMessage.AnnouncementName + ": " + getMessage.AnnouncementDescription;

                var pushMessageSingle = new BatchPush()
                {
                    Message = AnnouncementMessage,
                    // Token =
                    //"APA91bGE09bhztuOZyFxI2txOOAXrELuVQ38WWC-yrX6MpgNgjylVdXLygkbGbIU9x6aToJl3C5nVGJtdteAyGVbY19TSBWYnYip0-Arjv3-6KRDq9sDobbpc17yxb3OpFO_nxxxxxxxxxxx",
                    // "APA91bGFcbSHKLRyM5oRLqqlHeEMjoItMMaUMgRjSLRh5K2V87uFEGl0_urYw_4IRR8GMSDrHrZA9oUWapctuAxW3MVrmSNj5Mv1kB7FZz2Thpa8NnZq1d3BAftQgZEBZ5ieYCTltYgtYJ31bmGyF3Ssh1YgMcFz0w", 
                    //  Platform = Platform.Android,

                    Platforms = new[] { Platform.Android, Platform.iOS },
                    Badge = "+1",
                    Sound = "",
                    Payload = JObject.Parse(@"{ 'openURL': 'http://www.google.com/' }")


                   // Payload = JObject.Parse(@"{ 'largeIcon': 'https://lh3.googleusercontent.com/-2bMJQbKLNRQ/AAAAAAAAAAI/AAAAAAAA_nc/SYnugOyXboE/s120-c/photo.jpga' }")
                };
                await _pushBotsClient.Push(pushMessageSingle);

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Couldn't find device with alias: ");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetBaseException().Message);
            }
        }

    }
}
