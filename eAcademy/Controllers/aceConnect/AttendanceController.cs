﻿using eAcademy.ApiModels;
using eAcademy.ApiServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace eAcademy.Controllers.aceConnect
{
    public class AttendanceController : ApiController
    {
        StudentDailyAttendanceServices attendance = new StudentDailyAttendanceServices();
        AssignClassToStudentServices assignstudent = new  AssignClassToStudentServices();

        public object Post(Token t)
        {
            int yearId;
            int classId;
            int secId;
            int studentRegId;
            string tokenId = t.token;
            TokenAuthSservices tok = new TokenAuthSservices();
            var checkValidUser = tok.IsValidUser(tokenId);
            if (checkValidUser != null)
            {
                studentRegId = t.StudentRegId;
                var classDetail = assignstudent.GetClassDetails(studentRegId);
                yearId = classDetail.AcademicYearId.Value;
                classId = classDetail.ClassId.Value;
                secId = classDetail.SectionId.Value;
                var getAttendanceMonth = attendance.getAttendanceMonth(yearId, classId, secId, studentRegId);
                string UserValidation = "Success";
                return new { getAttendanceMonth, UserValidation };
            }
            else
            {
                string UserValidation = "Time expired. Login again";
                return new { UserValidation };
            }
        }

    }
}
