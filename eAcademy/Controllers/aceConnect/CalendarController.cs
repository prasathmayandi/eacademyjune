﻿using eAcademy.ApiModels;
using eAcademy.ApiServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace eAcademy.Controllers.aceConnect
{
    public class CalendarController : ApiController
    {
        public object Post(Token t)
        {
            string tokenId = t.token;
            TokenAuthSservices tok = new TokenAuthSservices();
            var checkValidUser = tok.IsValidUser(tokenId);
            if (checkValidUser != null)
            {
                CalendarServices cal = new CalendarServices();
                var List = cal.GetEventsList();
                string UserValidation = "Success";
                return new { List, UserValidation };
            }
            else
            {
                string UserValidation = "Time expired. Login again";
                return new { UserValidation };
            }
        }
    }
}
