﻿using eAcademy.ApiServices;
using eAcademy.ApiModels;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Data.Objects.SqlClient;



namespace eAcademy.Controllers.aceConnect
{
    public class DashboardController : ApiController
    {
        EacademyEntities dc = new EacademyEntities();
        AnnouncementServices announcement = new AnnouncementServices();
        AdmissionTransactionServices Student = new AdmissionTransactionServices();

        public object Post(Token t)
        {
            string tokenId = t.token;
            TokenAuthSservices tok = new TokenAuthSservices();
            var checkValidUser = tok.IsValidUser(tokenId);
            if(checkValidUser != null)
            {
                int ParentRegId = checkValidUser.UserId.Value;
                var AnnouncementList = announcement.GetAnnouncementList();
                var StudentList = Student.GetStudentList(ParentRegId);
                string UserValidation = "Success";
                return new { AnnouncementList, StudentList, UserValidation };
            }
            else
            {
                string UserValidation = "Time expired. Login again";
                return new { UserValidation };
            }
        }




    }
}
