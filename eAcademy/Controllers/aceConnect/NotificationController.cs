﻿using eAcademy.ApiModels;
using eAcademy.ApiServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace eAcademy.Controllers.aceConnect
{
    public class NotificationController : ApiController
    {
        public object Post(Token t)
        {
            string tokenId = Convert.ToString(t.token);
            TokenAuthSservices tok = new TokenAuthSservices();
            var checkValidUser = tok.IsValidUser(tokenId);
            if (checkValidUser != null)
            {
                int ParentRegId = checkValidUser.UserId.Value;
                //int ParentRegId = 1;
                NotificationServices noti = new NotificationServices();
                var UnreadNotificationList = noti.getUnreadNotificationList(ParentRegId);
                int UnreadNotificationCount = UnreadNotificationList.Count;

                string UserValidation = "Success";
                return new { UnreadNotificationList, UserValidation, UnreadNotificationCount };

                //HomeworkServices HW = new HomeworkServices();
                //var HomeworkUnread = HW.getUnreadHomeworkList(ParentRegId);
                //int HomeworkUnreadCount = HomeworkUnread.Count;

                //string UserValidation = "Success";
                //return new { HomeworkUnread, UserValidation, HomeworkUnreadCount };
            }
            else
            {
                string UserValidation = "Time expired. Login again";
                return new { UserValidation };
            }
        }


      


    }
}
