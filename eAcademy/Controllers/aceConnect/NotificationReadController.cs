﻿using eAcademy.ApiModels;
using eAcademy.ApiServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace eAcademy.Controllers.aceConnect
{
    public class NotificationReadController : ApiController
    {

        public object Post(Token t)
        {
            string tokenId = Convert.ToString(t.token);
            TokenAuthSservices tok = new TokenAuthSservices();
            var checkValidUser = tok.IsValidUser(tokenId);
            if (checkValidUser != null)
            {
                int ParentRegId = checkValidUser.UserId.Value;
                NotificationServices ons = new NotificationServices();
                ons.UpdateNotificationListRead(ParentRegId);
                string UserValidation = "Success";
                return new { UserValidation };
            }
            else
            {
                string UserValidation = "Time expired. Login again";
                return new { UserValidation };
            }
        }
    }
}
