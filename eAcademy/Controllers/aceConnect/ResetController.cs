﻿using eAcademy.ApiModels;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;

namespace eAcademy.Controllers.aceConnect
{
    public class ResetController : ApiController
    {
        EacademyEntities dc = new EacademyEntities();
        public object Post(Parent p)
        {
            var checkValidParent = (from a in dc.PrimaryUserRegisters where a.PrimaryUserId == p.parentId select a).FirstOrDefault();
            string message;
            string UserValidation;

            if (checkValidParent != null)
            {
                var allsalt = (from a in dc.PrimaryUserRegisters where a.PrimaryUserId == p.parentId select a).FirstOrDefault();
                var salt = allsalt.Salt;
                var UPassWordSalt = salt + p.password;
                string EncryptedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(UPassWordSalt, "SHA1");
                var update = (from a in dc.PrimaryUserRegisters where a.PrimaryUserId == p.parentId select a).First();
                update.PrimaryUserPwd = EncryptedPassword;
                dc.SaveChanges();
                string pid = p.parentId;
                DeleteCode(pid);
                message = "Your New Password has been changed...";
                UserValidation = "success";
                return new { message, UserValidation };
            }
            else
            {
                message = "Invalid UserId";
                UserValidation = "failure";
                return new { message, UserValidation };
            }

        }
        private void DeleteCode(string pid)
        {
            try
            {

                var delFather = from a in dc.tblFatherResetPasswords
                                 from b in dc.PrimaryUserRegisters
                                 where b.PrimaryUserRegisterId == a.UserId &&
                                 b.PrimaryUserId == pid
                                 select a;               
                if (delFather != null)
                {
                    foreach (var cc in delFather)
                    {
                        dc.tblFatherResetPasswords.Remove(cc);
                    }
                       dc.SaveChanges();                  
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }   
}
