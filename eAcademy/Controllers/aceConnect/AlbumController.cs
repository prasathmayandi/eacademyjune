﻿using eAcademy.ApiModels;
using eAcademy.ApiServices;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace eAcademy.Controllers.aceConnect
{
    public class AlbumController : ApiController
    {
        AssignClassToStudentServices assignstudent = new AssignClassToStudentServices();
        int yearId;
        int classId;
        int secId;
        int studentRegId;
        AlbumServices album = new AlbumServices();             
        public object Post(Token t)
        {
            string tokenId = t.token;
            TokenAuthSservices tok = new TokenAuthSservices();
            var checkValidUser = tok.IsValidUser(tokenId);
            if (checkValidUser != null)
            {
                studentRegId = t.StudentRegId;
                var classDetail = assignstudent.GetClassDetails(studentRegId);
                yearId = classDetail.AcademicYearId.Value;
                classId = classDetail.ClassId.Value;
                secId = classDetail.SectionId.Value;
                  
                var List = album.GetAlbumListByMonth(yearId, classId, secId, studentRegId, t.date);
                string UserValidation = "Success";
                return new { List, UserValidation };
            }
            else
            {
                string UserValidation = "Time expired. Login again";
                return new { UserValidation };
            }
        }
        public IEnumerable<AlbumImage> GET(int id)
        {
            var getAlbum = album.getAlbumImage(id);
            return getAlbum;
        }


    }
}
