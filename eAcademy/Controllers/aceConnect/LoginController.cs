﻿using eAcademy.ApiServices;
using eAcademy.ApiModels;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Data.Objects.SqlClient;

namespace eAcademy.Controllers.aceConnect
{
    public class LoginController : ApiController
    {
        PrimaryUserRegisterServices login = new PrimaryUserRegisterServices();
        EacademyEntities dc = new EacademyEntities();

        public string PostLogin(login p)
        {
            string GetStudentList = login.CheckValidUserId(p.parentid, p.password);
            if (GetStudentList == "Incorrect password")
            {
                return "Incorrect password";
            }
            else if(GetStudentList == "Incorrect username and password")
            {
                return "Incorrect username and password";
            }
            else 
            {
                

                return GetStudentList;
            }       
        }
        public object GET()
        {
            PrimaryUserRegisterServices ss = new PrimaryUserRegisterServices();
            var SchoolName = "Eacademy";
            int Student = ss.totalstudentstrength();
            int Desktop = ss.totalwebLoginCount();
            int App = ss.totalAppLoginCount();
            return new { SchoolName, Student, Desktop, App };
        }


      

    }
}
