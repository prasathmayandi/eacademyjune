﻿using eAcademy.ApiModels;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace eAcademy.Controllers.aceConnect
{
    public class VerifyController : ApiController
    {
        EacademyEntities dc = new EacademyEntities();
        public object Post(Parent p)
        {
            var checkValidParent = (from a in dc.PrimaryUserRegisters where a.PrimaryUserId == p.parentId select a).FirstOrDefault();
            string message;
            string UserValidation;

            if (checkValidParent != null)
            {
                var check = (from a in dc.tblFatherResetPasswords
                             from b in dc.PrimaryUserRegisters
                             where b.PrimaryUserRegisterId == a.UserId &&
                             b.PrimaryUserId == p.parentId &&
                             a.VerifyCode == p.code
                             select a).FirstOrDefault();
                if (check != null)
                {
                    message = "Please enter your new password for your profile";
                    UserValidation = "success";
                    return new { message, UserValidation };
                }
                else
                {
                    message = "Verify code wrong";
                    UserValidation = "failure";
                    return new { message, UserValidation };
                }

            }
            else
            {
                message = "Invalid UserId";
                UserValidation = "failure";
                return new { message, UserValidation };
            }

        }
    }
}
