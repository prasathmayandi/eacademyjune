﻿using eAcademy.ApiModels;
using eAcademy.ApiServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace eAcademy.Controllers.aceConnect
{
    public class LeaveRequestController : ApiController
    {
        ParentLeaveRequestServices leave = new ParentLeaveRequestServices();
        AssignClassToStudentServices assignstudent = new AssignClassToStudentServices();
        int yearId;
        int classId;
        int secId;
        int studentRegId; 
        int ParentRegId;

        public object PostLeaveRequest(LeaveRequest p)
        {
            string tokenId = p.token;
            TokenAuthSservices tok = new TokenAuthSservices();
            AssignClassTeacherServices ClasTeacher = new AssignClassTeacherServices();
            var checkValidUser = tok.IsValidUser(tokenId);
            if (checkValidUser != null)
            {
                studentRegId = p.StudentRegId;
                ParentRegId = checkValidUser.UserId.Value;
                var classDetail = assignstudent.GetClassDetails(studentRegId);
                yearId = classDetail.AcademicYearId.Value;
                classId = classDetail.ClassId.Value;
                secId = classDetail.SectionId.Value;
                var getClassIncharge = ClasTeacher.getClassTeacher(yearId, classId, secId);
                int? EmployeeRegId = getClassIncharge.EmployeeRegisterId;
                var AddLeaveRequest = leave.AddParentLeaveRequest(ParentRegId, studentRegId, yearId, classId, secId, p.fromdate.Date, p.todate.Date, p.reason, EmployeeRegId);
                if (AddLeaveRequest == true)
                {
                    string message = "Successfully applied your leave request";
                    return new { message };
                }
                else
                {
                    string message = "Failure";
                    return new { message };
                }
            }
            else
            {
                string message = "Time expired. Login again";
                return new { message };
            }
        }

        public IEnumerable<LeaveRequest> GET(int id)
        {
            var LeaveReqStatus = leave.getLeaveReqDetail(id);
            return LeaveReqStatus;
        }
    }
}
