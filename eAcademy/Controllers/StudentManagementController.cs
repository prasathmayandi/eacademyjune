﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class StudentManagementController : Controller
    {
        AcademicyearServices obj_academicYear = new AcademicyearServices();
        ClassServices obj_class = new ClassServices();
        SectionServices obj_section = new SectionServices();
        StudentServices obj_student = new StudentServices();
        ExamServices obj_exam = new ExamServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ViewBag.acYear = obj_academicYear.AllList_AcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
            ViewBag.ClassList = obj_class.Classes();
        }
        public ActionResult StudentList()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
       
        public JsonResult ResultStudentList(string sidx, string sord, int page, int rows, int? AcyearStdList, int? ClassStdList, int? SectionStdList)
        {
            IList<ClassStudent> todoListsResults1 = new List<ClassStudent>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (AcyearStdList.HasValue && ClassStdList.HasValue && SectionStdList.HasValue)
            {
                todoListsResults1 = obj_student.StudentDetails(AcyearStdList.Value, ClassStdList.Value, SectionStdList.Value);
            }
            int totalRecords = todoListsResults1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults1 = todoListsResults1.OrderByDescending(s => s.AcademicYearId).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults1 = todoListsResults1.OrderBy(s => s.AcademicYearId).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults1;
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult StudentListExportToExcel()
        {
            try
            {
                List<ClassStudent> todoListsResults1 = (List<ClassStudent>)Session["JQGridList"];
                var list = todoListsResults1.Select(o => new { StudentId = o.StudentId, RollNumber = o.RollNumber, StudentName = o.StudentName, PrimaryUserName = o.PrimaryUserName, PrimaryUserEmail = o.PrimaryUserEmail, EmergencyContactPerson = o.EmergencyContactPerson, EmergencyContactNumber = o.ContactpersonContact }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("StudentListExcelFileName");
                GenerateExcel.ExportExcel(list, fileName);                
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult StudentListExportToPDF()
        {
            try
            {
                List<ClassStudent> todoListsResults1 = (List<ClassStudent>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = eAcademy.Models.ResourceCache.Localize("StudentList");
                GeneratePDF.ExportPDF_Landscape(todoListsResults1, new string[] { "StudentId", "RollNumber", "StudentName", "PrimaryUserName", "PrimaryUserEmail", "EmergencyContactPerson", "ContactpersonContact" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", "StudentList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public JsonResult getClassList()
        {
            try
            {
                var ans = obj_class.Classes();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ClassList()
        {
            try
            {
                var ans = obj_class.Classes();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getClassList1(int cid)
        {
            try
            {
                var ans = obj_class.Classes(cid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getSectionList(int cid)
        {
            try
            {
                var ans = obj_section.getSectionByClassId(cid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        //[EncryptedActionParameter]
        public ActionResult StudentProfile(string id)
        {
            try
            {
                int std_id = Convert.ToInt32(QSCrypt.Decrypt(id));
                var ans = obj_student.getStudentListByRegId(std_id);
                return View(ans);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public ActionResult StudentProfiles()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult getStudentList(int acid, int cid, int sec_id)
        {
            try
            {
                var ans = obj_student.getStudent(acid, cid, sec_id);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult StudentListWhoHaveStudied(int acid, int cid, int sec_id)
        {
            try
            {
                var ans = obj_student.getStudentWhoHaveStudied(acid, cid, sec_id);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StudentProfiles(Std_Profile model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int std_id = Convert.ToInt32(model.StudentId);
                    var ans = obj_student.getStudentDetails(std_id);
                    return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult StudentPerformance()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public ActionResult getExam()
        {
            try
            {
                ExamServices obj_exam = new ExamServices();
                var ans = obj_exam.getExamList();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult StudentPerformance(StdPerformance model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    MarkTotalServices obj_markTotal = new MarkTotalServices();
                    MarkServices obj_mark = new MarkServices();
                    ExamServices obj_exam = new ExamServices();
                    int acid = model.AcademicYearId;
                    int cid = model.ClassId;
                    int sec_id = model.SectionId;
                    int std_id = model.StudentId;
                    int examId = model.ExamId;
                    var ans = obj_mark.getSubjectMark(acid, cid, sec_id, std_id, examId);
                    var MarkTotal = obj_markTotal.getTotalMark(acid, cid, sec_id, examId, std_id);
                    return Json(new { ans = ans, MarkTotal = MarkTotal }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult StudentRollNumber()
        {
            try
            {
                if (TempData["ans"] != null)
                {
                    int cid = Convert.ToInt32(TempData["frm_cid"]);
                    ViewBag.Section = obj_section.getSections(cid);
                    return View(TempData["ans"]);
                }
                else if (TempData["check"] != null)
                {
                    int cid = Convert.ToInt32(TempData["frm_cid"]);
                    ViewBag.Section = obj_section.getSections(cid);
                    return View();
                }
                else
                {
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        [HttpPost]
        [ActionName("RollNumberManagement")]
        [eAcademy.Filters.InitializeSimpleMembershipAttribute.OnAction(ButtonName = "Go")]
        public ActionResult StudentRollNumber(FormCollection frm)
        {
            try
            {
                int acid = Convert.ToInt32(frm["AcyearStdList"]);
                TempData["frm_acid"] = acid;
                int cid = Convert.ToInt32(frm["ClassStdList"]);
                TempData["frm_cid"] = cid;
                int sec_id = Convert.ToInt32(frm["SectionStdList"]);
                TempData["Section"] = obj_section.getSections(cid);
                TempData["frm_secid"] = sec_id;
                var ans = obj_student.getStudentOrderByStudentName(acid, cid, sec_id);
                TempData["count"] = ans.Count;
                TempData["ans"] = ans;
                ViewBag.acyear = acid;
                ViewBag.clas = cid;
                ViewBag.sec = sec_id;
                return RedirectToAction("StudentRollNumber");
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        [HttpPost]
        [ActionName("RollNumberManagement")]
        [eAcademy.Filters.InitializeSimpleMembershipAttribute.OnAction(ButtonName = "RollNumber")]
        public ActionResult AssignRollNumber(FormCollection frm)
        {
            try
            {
                StudentRollNumberServices obj_studentRollNumber = new StudentRollNumberServices();
                RollNumberFormatServices obj_rollNumber = new RollNumberFormatServices();
                string cccc = frm["AcyearStdList"] + "-" + frm["ClassStdList"] + "-" + frm["SectionStdList"];
                int acid = Convert.ToInt32(frm["AcyearStdList"]);
                ViewBag.frm_acid = acid;
                int cid = Convert.ToInt32(frm["ClassStdList"]);
                ViewBag.frm_cid = cid;
                int sec_id = Convert.ToInt32(frm["SectionStdList"]);
                ViewBag.Section = obj_section.getSections(cid);
                ViewBag.frm_secid = sec_id;
                var format = obj_rollNumber.getRollNumberFormat(acid, cid, sec_id);
                TempData["frm_acid"] = acid;
                TempData["frm_cid"] = cid;
                TempData["frm_secid"] = sec_id;
                TempData["check"] = 1;
                if (format != null)
                {
                    string rollNumberFormat = format.RollNumberFormat;
                    int c = rollNumberFormat.Length;
                    var ans = obj_student.getNonRollNumberStudents(acid, cid, sec_id);
                    int count = ans.Count;
                    var rollNumber = obj_studentRollNumber.getRollNumberOrderByID(acid, cid, sec_id);
                    int sss;
                    if (rollNumber != null)
                    {
                        string ss = (rollNumber.RollNumberFormat).Remove(0, c);
                        sss = Convert.ToInt32(ss);
                    }
                    else
                    {
                        sss = 0;
                    }
                    for (int i = 0; i < count; i++)
                    {
                        int n = (i + 1) + sss;
                        string roll_number = rollNumberFormat + n;
                        obj_studentRollNumber.addStudentRollNumber(acid, cid, sec_id, ans[i].StudentRegId, roll_number);
                    }
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_roll_number_to_student"));
                    TempData["Message"] = eAcademy.Models.ResourceCache.Localize("Roll_Number_Added_Successfully");
                }
                else
                {
                    TempData["ErrorMessage"] = eAcademy.Models.ResourceCache.Localize("Roll_Number_Format_Required._Contact_Admin");
                    var ans1 = obj_student.getStudentOrderByStudentName(acid, cid, sec_id);
                    TempData["count"] = ans1.Count;
                    TempData["ans"] = ans1;
                }
                return RedirectToAction("StudentRollNumber");
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("StudentRollNumber");
            }
        }
        public ActionResult AcademicResult()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public ActionResult AcademicGradeResult()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult AcademicGradeResultList(int AcyearStdList, int ClassStdList, int SectionStdList1)    //, string Achievement, DateTime? txt_doa_search
        {
            try
            {
                var ans = obj_student.getStudentwithResult(AcyearStdList, ClassStdList, SectionStdList1);
                var ans_count = ans.Count;
                //var result = obj_exam.getOverAllResultbySection(AcyearStdList, ClassStdList, SectionStdList1, Examids);
                //var count = result.Count;
                var AcademicYearDD = obj_academicYear.getAcademicYear(AcyearStdList);
                //result = result, count = count,
                return Json(new { ans = ans,  Ayear = AcademicYearDD, ans_count = ans_count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AcademicResultColumns()    //, string Achievement, DateTime? txt_doa_search
        {
            try
            {
                var ans = obj_exam.getExamsAsTableColumns();
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ExamColumn()
        {
            try
            {
                var ans = obj_exam.getExamsAsTableColumns();
                if (ans.Count == 0)
                {
                    string ErrorMessage = ResourceCache.Localize("Exam_not_created");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AcademicResultList(int AcyearStdList, int ClassStdList, int SectionStdList1, int[] Examids)    //, string Achievement, DateTime? txt_doa_search
        {
            try
            {
                var ans = obj_student.getStudentwithResult(AcyearStdList, ClassStdList, SectionStdList1);
                var ans_count = ans.Count;
                var result = obj_exam.getOverAllResultbySection(AcyearStdList, ClassStdList, SectionStdList1, Examids);
                var count = result.Count;
                var AcademicYearDD = obj_academicYear.getAcademicYear(AcyearStdList);
                return Json(new { ans = ans, result = result, count = count, Ayear = AcademicYearDD, ans_count = ans_count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult AcademicResult(FormCollection frm)
        {
            try
            {
                int acid = Convert.ToInt32(frm["AcyearStdList"]);
                ViewBag.frm_acid = acid;
                int cid = Convert.ToInt32(frm["ClassStdList"]);
                ViewBag.frm_cid = cid;
                int sec_id = Convert.ToInt32(frm["SectionStdList1"]);
                ViewBag.Section = obj_section.getSections(cid);
                ViewBag.frm_secid = sec_id;
                ViewBag.StudentList = obj_student.getStudent(acid, cid, sec_id);
                int std_id = Convert.ToInt32(frm["Student_List"]);
                ViewBag.frm_stdid = std_id;
                ViewBag.promote = std_id;
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult Promote(SavePromote model)
        {
            try
            {
                AssignClassToStudentServices obj_assignClass = new AssignClassToStudentServices();
                StudentRollNumberServices obj_studentrollnumber = new StudentRollNumberServices();
                string Message = "";
                int acid = model.AcademicYearId;
                int cid = model.ClassId;
                int sec_id = model.SectionId;
                int count = model.StudentRegId.Count();
                int[] std_id = new int[count];
                string std_name = null;
                int old_acid = model.Old_AcademicYearId;
                int old_cid = model.Old_ClassId;
                int old_sec_id = model.Old_SectionId;
                for (int i = 0; i < count; i++)
                {
                    int stuId = model.StudentRegId[i];
                    var FinalResult = obj_assignClass.getFinalResult(old_acid, old_cid, old_sec_id, stuId);
                    if (FinalResult != null)
                    {
                        var classAssignedForNextAcYear = FinalResult.ClassAssignedForNextAcademicYear;
                        if (classAssignedForNextAcYear == "Yes")
                        {
                            var ErrorMessage = ResourceCache.Localize("Class_assined_for_next_academic_year_to_the_selected_student_so_unable_to_do_any_changes");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                for (int i = 0; i < count; i++)
                {
                    std_id[i] = model.StudentRegId[i];
                    var std = obj_student.getStudentByRegId(std_id[i]);
                    obj_assignClass.promoteAssignClassStudent(std_id[i], old_acid, old_cid, old_sec_id); // ok
                   // obj_studentrollnumber.ChangeStudentRollNumberstatus(old_acid, old_cid, old_sec_id, std_id[i]); // ok
                   // obj_assignClass.addAssignClassStudent(acid, cid, sec_id, std_id[i]); // Remove
                    std_name = std_name + std.StudentName + " , ";
                }
                useractivity.AddEmployeeActivityDetails( eAcademy.Models.ResourceCache.Localize("PassedStudent"));
                Message = std_name + "</ br> " + eAcademy.Models.ResourceCache.Localize("Student(s)_Passed_Successfully");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Depromote(SavePromote model)
        {
            try
            {
                AssignClassToStudentServices obj_assignClass = new AssignClassToStudentServices();
                StudentRollNumberServices obj_studentrollnumber = new StudentRollNumberServices();
                string Message = "";
                int acid = model.AcademicYearId;
                int cid = model.ClassId;
                int sec_id = model.SectionId;
                int count = model.StudentRegId.Count();
                int[] std_id = new int[count];
                string std_name = null;
                int old_acid = model.Old_AcademicYearId;
                int old_cid = model.Old_ClassId;
                int old_sec_id = model.Old_SectionId;
                for (int i = 0; i < count; i++)
                {
                    int stuId = model.StudentRegId[i];
                    var FinalResult = obj_assignClass.getFinalResult(old_acid, old_cid, old_sec_id, stuId);
                    if (FinalResult != null)
                    {
                        var classAssignedForNextAcYear = FinalResult.ClassAssignedForNextAcademicYear;
                        if (classAssignedForNextAcYear == "Yes")
                        {
                            var ErrorMessage = ResourceCache.Localize("Class_assined_for_next_academic_year_to_the_selected_student_so_unable_to_do_any_changes");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                for (int i = 0; i < count; i++)
                {
                    std_id[i] = model.StudentRegId[i];
                    var std = obj_student.getStudentByRegId(std_id[i]);
                    obj_assignClass.demoteAssignClassStudent(std_id[i], old_acid, old_cid, old_sec_id); // ok
                 //   obj_studentrollnumber.ChangeStudentRollNumberstatus(old_acid, old_cid, old_sec_id, std_id[i]); // ok
                //    obj_assignClass.addAssignClassStudent(acid, cid, sec_id, std_id[i]);  // Remove
                    std_name = std_name + std.StudentName + " , ";
                }
                useractivity.AddEmployeeActivityDetails( eAcademy.Models.ResourceCache.Localize("FailedStudent"));
                Message = std_name + " " + eAcademy.Models.ResourceCache.Localize("Student(s)_Failed_exam");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Remove(SavePromote model)
        {
            try
            {
                AssignClassToStudentServices obj_assignClass = new AssignClassToStudentServices();
                StudentRollNumberServices obj_studentrollnumber = new StudentRollNumberServices();
                string Message = "";
                int acid = model.AcademicYearId;
                int cid = model.ClassId;
                int sec_id = model.SectionId;
                int count = model.StudentRegId.Count();
                int[] std_id = new int[count];
                string std_name = null;
                int old_acid = model.Old_AcademicYearId;
                int old_cid = model.Old_ClassId;
                int old_sec_id = model.Old_SectionId;
                for (int i = 0; i < count; i++)
                {
                    int stuId = model.StudentRegId[i];
                    var FinalResult = obj_assignClass.getFinalResult(old_acid, old_cid, old_sec_id, stuId);
                    if (FinalResult != null)
                    {
                        var classAssignedForNextAcYear = FinalResult.ClassAssignedForNextAcademicYear;
                        if (classAssignedForNextAcYear == "Yes")
                        {
                            var ErrorMessage = ResourceCache.Localize("Class_assined_for_next_academic_year_to_the_selected_student_so_unable_to_do_any_changes");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                for (int i = 0; i < count; i++)
                {
                    std_id[i] = model.StudentRegId[i];
                    var std = obj_student.getStudentByRegId(std_id[i]);
                    obj_assignClass.RemoveAssignClassStudent(std_id[i], old_acid, old_cid, old_sec_id); // ok
                    std_name = std_name + std.StudentName + " , ";
                }
                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("RemovedStudent"));
                Message = std_name + " " + eAcademy.Models.ResourceCache.Localize("Student(s)_Removed_Successfully");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ReAppear(SavePromote model)
        {
            try
            {
                AssignClassToStudentServices obj_assignClass = new AssignClassToStudentServices();
                StudentRollNumberServices obj_studentrollnumber = new StudentRollNumberServices();
                string Message = "";
                int acid = model.AcademicYearId;
                int cid = model.ClassId;
                int sec_id = model.SectionId;
                int count = model.StudentRegId.Count();
                int[] std_id = new int[count];
                string std_name = null;
                int old_acid = model.Old_AcademicYearId;
                int old_cid = model.Old_ClassId;
                int old_sec_id = model.Old_SectionId;
                for (int i = 0; i < count; i++)
                {
                    int stuId = model.StudentRegId[i];
                    var FinalResult = obj_assignClass.getFinalResult(old_acid, old_cid, old_sec_id, stuId);
                    if(FinalResult != null)
                    {
                        var Result = FinalResult.Result;
                        if(Result == null)
                        {
                            var ErrorMessage = ResourceCache.Localize("Before_Making_the_student_disappear_Please_generate_Final_exam_result_to_all_the_selected_student");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        var classAssignedForNextAcYear = FinalResult.ClassAssignedForNextAcademicYear;
                        if(classAssignedForNextAcYear == "Yes")
                        {
                            var ErrorMessage = ResourceCache.Localize("Class_assined_for_next_academic_year_to_the_selected_student_so_unable_to_do_any_changes");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                for (int i = 0; i < count; i++)
                {
                    std_id[i] = model.StudentRegId[i];
                    var std = obj_student.getStudentByRegId(std_id[i]);
                    obj_assignClass.ReAppearAssignClassStudent(std_id[i], old_acid, old_cid, old_sec_id); // ok
                    std_name = std_name + std.StudentName + " , ";
                }
                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("ReAppearedStudent"));
                Message = std_name + " " + eAcademy.Models.ResourceCache.Localize("Student(s)_ReAppeared_Successfully");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult SendEmail(VM_Email f)
        {
            try
            {
                AdmissionTransactionServices admissionTrans = new AdmissionTransactionServices();
                StudentServices ss = new StudentServices();
                int count = Convert.ToInt16(f.SelectCount);
                string[] regid = new string[count];
                string[] emails = new string[count];
                for (int i = 0; i < count; i++)
                {
                    regid[i] = String.Join(" ", f.RegId[i]);
                    int id = Convert.ToInt16(QSCrypt.Decrypt(regid[i]));
                    int? PrimayUserRegisterId = 0;
                    var GetPrimayUserRegisterId = admissionTrans.getRow(id);
                    var StudentDetails = ss.GetStudent1(id);
                    if (GetPrimayUserRegisterId != null)
                    {
                        PrimayUserRegisterId = GetPrimayUserRegisterId.PrimaryUserRegisterId;
                        int cc = emails.Count();
                        PrimaryUserRegisterServices primaryUser = new PrimaryUserRegisterServices();
                        var primaryUserDetail = primaryUser.getPrimaryUserDetail(PrimayUserRegisterId);
                        int flag = 0;
                        for (int k = 0; k < cc; k++)
                        {
                            if (emails[k] == primaryUserDetail.PrimaryUserEmail)
                            {
                                flag = 1;
                            }
                        }
                        if (primaryUserDetail != null && flag == 0)
                        {
                            for (int k = 0; k < cc; k++)
                            {
                                if (emails[k] == null)
                                {
                                    emails[k] = primaryUserDetail.PrimaryUserEmail;
                                    goto cc;
                                }
                            }
                        cc:
                            var getPrimaryUserId = primaryUserDetail.PrimaryUserId;
                            string p_userName = primaryUserDetail.PrimaryUserName;
                            string p_Salt = p_userName.Substring(0, 3);
                            Random P_random = new Random();
                            string P_randomNumber = P_random.Next(00000, 99999).ToString();
                            var P_pwd = p_Salt + P_randomNumber;
                            var p_PasswordSalt = p_Salt + P_pwd;
                            string Password = FormsAuthentication.HashPasswordForStoringInConfigFile(p_PasswordSalt, "SHA1");
                            primaryUser.updateParentPassword(PrimayUserRegisterId, Password);
                            if (getPrimaryUserId != null && P_pwd != null)
                            {
                                // Email username password
                                StreamReader reader = new StreamReader(Server.MapPath("~/Views/StudentManagement/StudentEmailPage.html"));
                                string readFile = reader.ReadToEnd();
                                string myString = "";
                                myString = readFile;
                                myString = myString.Replace("$$FirstName$$", p_userName);                              
                                myString = myString.Replace("$$StudentName$$", StudentDetails.StuFirstName);
                                myString = myString.Replace("$$class$$", StudentDetails.txt_Class);                              
                                myString = myString.Replace("$$username$$", getPrimaryUserId);
                                myString = myString.Replace("$$password$$", P_pwd.ToString());                             
                                var Email = primaryUserDetail.PrimaryUserEmail;
                                string supportEmail = ConfigurationManager.AppSettings["supportEmail"];

                                string PlaceName = "StudentManagement-Resend primary user login detail";
                                string UserName = "Employee";
                                string UserId = Convert.ToString(Session["username"]);


                                Mail.SendMail("Sprouts Montessori", supportEmail, Email, "Account Credentials", myString.ToString(), true, PlaceName, UserName, UserId);  



                            }

                        }
                    }
                    else
                    {
                        string SuccessMessage = "Send Email up to " + i + " Students";
                        return Json(new { Message = SuccessMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                string Message = "Send Email to all selected students";
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}