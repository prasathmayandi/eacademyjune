﻿using DotNetOpenAuth.AspNet.Clients;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class AcademicsConfigurationController : Controller
    {
        AcademicyearServices obj_academicYear = new AcademicyearServices();
        TermServices obj_term = new TermServices();
        ClassServices obj_class = new ClassServices();
        SectionServices obj_section = new SectionServices();
        SubjectServices obj_subject = new SubjectServices();
        EventServices obj_event = new EventServices();
        AnnouncementServices obj_announcement = new AnnouncementServices();
        FacilitiesServices obj_facility = new FacilitiesServices();
        ExtraActivitiesServices obj_activity = new ExtraActivitiesServices();
        SectionStrengthServices obj_strength = new SectionStrengthServices();
        GroupSubjectServices obj_groupSubject = new GroupSubjectServices();
        GroupSubjectConfigServices obj_groupSubjectConfig = new GroupSubjectConfigServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        AcademicyearServices acservice = new AcademicyearServices();
        TermFeeServices obj_termFee = new TermFeeServices();
        FeeServices obj_Fee = new FeeServices();
        public int AdminId;

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ViewBag.acYear = obj_academicYear.AllList_AcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
            ViewBag.Class = obj_class.Classes();
            ViewBag.ExtraActivity = obj_activity.getExtraActivityList();
            ViewBag.GroupSubject = obj_subject.GroupSubject();
            AdminId = Convert.ToInt32(Session["empRegId"]);
        }
        public ActionResult AcademicYear()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public JsonResult AcademicYearList(string sidx, string sord, int page, int rows, int? yr_search)
        {
            IList<AcademicYearValidation> todoListsResults1 = new List<AcademicYearValidation>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults1 = obj_academicYear.getAcademicYear1();
            if (yr_search.HasValue)
            {
                todoListsResults1 = todoListsResults1.Where(p => p.AcYearId.Equals(yr_search)).ToList();
            }
            int totalRecords = todoListsResults1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults1 = todoListsResults1.OrderByDescending(s => s.AcYear).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults1 = todoListsResults1.OrderBy(s => s.AcYear).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults1;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AcademicYearExportToExcel()
        {
            try
            {
                List<AcademicYearValidation> todoListsResults1 = (List<AcademicYearValidation>)Session["JQGridList"];
                var list = todoListsResults1.Select(o => new { AcademicYear = o.AcademicYear, StartDate = o.StartDate, EndDate = o.EndDate }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("AcademicYearList");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult AcademicYearExportToPDF()
        {
            try
            {
                List<AcademicYearValidation> todoListsResults1 = (List<AcademicYearValidation>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("AcademicYearList");
                GeneratePDF.ExportPDF_Portrait(todoListsResults1, new string[] { "AcademicYear", "StartDate", "EndDate" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "AcademicYear.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public JsonResult AcademicYear(AcademicYearValidation model)
        {
            try
            {               
                if (ModelState.IsValid)
                {
                    DateTime sdate = model.acYSD;
                    DateTime edate = model.acYED;
                    bool YearCheck = obj_academicYear.AcademicYearcheck(sdate);
                    if (YearCheck == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Academic_Year_Start_Date_Sholud_be_greater_than_the_previous_Academic_Year");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string acYear = sdate.Year.ToString() + "-" + edate.Year.ToString();
                        bool cc = obj_academicYear.checkAcademicYear(sdate, edate);
                        if (cc == true)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Selected__Academic_Year_is_already_Available");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            obj_academicYear.addAcademicYear(acYear, sdate, edate);
                            string Message = eAcademy.Models.ResourceCache.Localize("Academic_Year_Added_Successfully");
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_Academicyear"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                  
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult EditAcademicYear(int id)
        {
            try
            {
                bool existacademicyear = obj_academicYear.PastAcademicyear(id);
                if (existacademicyear==true)
                {
                    var ans = obj_academicYear.AcademicYears(id);
                    return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
                }
                else{

                    string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                    return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveEditAcademicYear(EditAcademicYear model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = model.AcademicYearId;
                    DateTime acSD = model.EditacYSD;
                    DateTime acED = model.EditacYED;
                    bool UpdateYearCheck = obj_academicYear.AcademicYearcheck(acSD);
                    if (UpdateYearCheck == true)
                       {
                           string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Academic_Year_Start_Date_Sholud_be_greater_than_the_previous_Academic_Year");
                           return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                       }
                       else
                       {
                           string acYear = acSD.Year.ToString() + "-" + acED.Year.ToString();
                           bool cc = obj_academicYear.checkAcademicYear(acid, acSD, acED);
                           if (cc == true)
                           {
                               string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Selected__Academic_Year_is_already_Available");
                               return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                           }
                           bool cc1 = obj_academicYear.checkAcademicYearAssigned(acid);
                           if (cc1 == true)
                           {
                               string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Academic_year_is_in_use._Should_not_edit");
                               return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                           }
                           else
                           {
                               obj_academicYear.updateAcademicYear(acid, acYear, acSD, acED, model.AcademicStatus);
                               string Message = eAcademy.Models.ResourceCache.Localize("Academic_Year_Updated_Successfully");
                               useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_Academicyear"));
                               return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                           }
                       }                
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [EncryptedActionParameter]
        public ActionResult DeleteAcademicYear(int acid)
        {
            try
            {
                bool CV = obj_academicYear.deleteAcademicYear(acid);
                bool tt = obj_term.deleteAcademicYear(acid);
                bool aa = obj_announcement.deleteAcademicYear(acid);
                bool cs = obj_strength.deleteAcademicYear(acid);
                bool ce = obj_event.deleteAcademicYear(acid);
                bool tf = obj_termFee.deleteAcademicYear(acid);
                bool Fc = obj_Fee.deleteAcademicYear(acid);
                bool Gl = obj_academicYear.deleteAcademicYearGameLevel(acid);
                bool AG = obj_academicYear.deleteAcademicYearAssignGametoStud(acid);
                bool SA = obj_academicYear.deleteAcademicYearstudActicity(acid);
                bool AIn = obj_academicYear.deleteAcademicYearActIncharge(acid);
                bool FIn = obj_academicYear.deleteAcademicYearFincharge(acid);
                bool EAch = obj_academicYear.deleteAcademicYearEAchieve(acid);
                bool SAch = obj_academicYear.deleteAcademicYearSAchieve(acid);
                bool StudAch = obj_academicYear.deleteAcademicYearStudAchieve(acid);
                bool TA = obj_academicYear.deleteAcademicYearTA(acid);
                bool HF = obj_academicYear.deleteAcademicYearHF(acid);
                bool ET = obj_academicYear.deleteAcademicYearET(acid);
                bool AECR = obj_academicYear.deleteAcademicYearAECR(acid);
                bool Rollno = obj_academicYear.deleteAcademicYearRollno(acid);
                bool ACT = obj_academicYear.deleteAcademicYearACT(acid);
                bool ASS = obj_academicYear.deleteAcademicYearASS(acid);

                if (CV == true && tt == true && aa == true && cs == true && ce == true && tf == true && Fc == true && Gl == true && AG == true && SA == true && AIn == true && FIn == true && EAch == true && SAch == true && StudAch == true && TA == true && HF == true && ET == true && AECR == true && Rollno == true && ACT == true && ASS == true)
                {
                    obj_academicYear.deleteACYear(acid);
                    string Message = eAcademy.Models.ResourceCache.Localize("Academic_year_deleted_Successfully");                                   
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Delete_Academicyear"));                   
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Academic_year_already_used,so_never_deleted");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("AcademicYear");
            }
        }
        public ActionResult Term()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public JsonResult TermResults(string sidx, string sord, int page, int rows, int? acYear1)
        {
            IList<TermRecord> todoListsResults2 = new List<TermRecord>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults2 = obj_term.getTermList();
            if (acYear1.HasValue)
            {
                todoListsResults2 = todoListsResults2.Where(p => p.acYear1 == acYear1).ToList();
            }
            int totalRecords = todoListsResults2.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults2 = todoListsResults2.OrderByDescending(s => s.AcademicYear).ToList();
                    todoListsResults2 = todoListsResults2.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults2 = todoListsResults2.OrderBy(s => s.AcademicYear).ToList();
                    todoListsResults2 = todoListsResults2.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults2;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults2
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TermExportToExcel()
        {
            try
            {
                List<TermRecord> todoListsResults2 = (List<TermRecord>)Session["JQGridList"];
                var list = todoListsResults2.Select(o => new { AcademicYear = o.AcademicYear, Term = o.TermName, TermStartDate = o.TermStartDate, TermEndDate = o.TermEndDate }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("AcademicYearTermList");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult TermExportToPDF()
        {
            try
            {
                List<TermRecord> todoListsResults2 = (List<TermRecord>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("AcademicYearTermList");
                GeneratePDF.ExportPDF_Portrait(todoListsResults2, new string[] { "AcademicYear", "TermName", "TermStartDate", "TermEndDate" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "Term.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public JsonResult getDate(int acid)
        {
            try
            {
                var ans = obj_academicYear.AcademicYears(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                return Json(new { sd = sdate, ed = edate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getDate1(int acid)
        {
            try
            {
                var ans = obj_academicYear.AcademicYearsWithTerm(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                return Json(new { sd = sdate, ed = edate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Term(SaveTerm model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = model.acYear;
                    DateTime sdate = model.tacYSD;
                    DateTime edate = model.tacYED;
                    string termname = model.txt_termname;
                    if (sdate == edate)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Start_date_and_End_date_should_not_be_same");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        bool cc = obj_term.checkTerm(acid, sdate.Date, edate.Date, termname);
                       // bool dd = obj_term.checkTermdate(acid, sdate.Date, edate.Date, termname);
                        if (cc == true)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Term_date_is_already_Exist");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var s = obj_term.checktermname(acid, termname);
                            if (s == null) { 
                            obj_term.addTerm(termname, sdate, edate, acid);
                            string Message = eAcademy.Models.ResourceCache.Localize("Term_Added_Successfully");
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_Term"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Term_name_already_exist_in_this_academic_year");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getTermRecord(int acid)
        {
            try
            {
                if (acid == 0)
                {
                    var ans = obj_term.TermList();
                    var count = ans.Count;
                    return Json(new { ans = ans, c = count }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var ans = obj_term.TermList(acid);
                    var count = ans.Count;
                    return Json(new { ans = ans, c = count }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditTerm(string tid)
        {
            try
            {
                int t_id = Convert.ToInt32(QSCrypt.Decrypt(tid));
                var ans = obj_term.Term(t_id);
                 bool existacademicyear = obj_academicYear.PastAcademicyear(ans.AcademicYearId);
                  if (existacademicyear == true)
                  {
                      return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
                  }
                  else
                  {
                      string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                      return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                  }
               
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveEditTerm(SaveTerm model)  //EditTerm
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int tid = Convert.ToInt32(QSCrypt.Decrypt(model.TermID));
                    DateTime acSD = model.tacYSD;
                    DateTime acED = model.tacYED;
                    string tname = model.txt_termname;
                    int acid = model.acYear;
                    bool cc = obj_term.checkTermExitname(acid, tname, tid);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Term_is_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_term.updateTerm(tid, tname, acSD, acED);
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_Term"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DeleteTerm(string passId)
        {
            try
            {
                TermServices ts = new TermServices();
                int Did = Convert.ToInt32(QSCrypt.Decrypt(passId));
                bool ss = ts.deleteTerm(Did);
                if (ss == true)
                {
                    string Message = eAcademy.Models.ResourceCache.Localize("Term_details_deleted_Successfully");
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Delete_Term"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("This_term_already_used,so_never_deleted");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Class()
        {
            try
            {
                var ans = obj_class.Classes();
                ViewBag.count = ans.Count;
                return View(ans);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        [HttpPost]
        public JsonResult getClassName(int classid)
        {
            try
            {
                var cName = obj_class.getClassNameByClassID(classid);
                var ans1 = obj_section.getSectionByClassId(classid);
                ViewBag.SecCount = ans1.Count;
                return Json(new { ans = cName.ClassType, sec = ans1 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult getSectionName(int secid)
        {
            try
            {
                var ans1 = obj_section.getSectionBySectionId(secid);
                return Json(new { ans = ans1.SectionName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeleteSectionName(int secid)
        {
            try
            {
                var ans1 = obj_section.sectioncheckusing(secid);
                var ans2 = obj_strength.sectioncheckusing(secid);
                if (ans1 == null && ans2 == null)
                {
                    obj_section.deletesection(secid);
                    string Message = eAcademy.Models.ResourceCache.Localize("Section_deleted_sucessfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                //else if(ans2 == null){
                //     obj_section.deletesection(secid);
                //    string Message = eAcademy.Models.ResourceCache.Localize("Section_deleted_sucessfully");
                //    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                //    }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Cant_deleted_session_Because_section_was_used_in_assigning_student");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Section(SaveSection model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string ErrorMessage = "", Message = "";
                    string secname = model.section;
                    int cid = model.classid;
                    bool cc = obj_section.checkSection(secname, cid);
                    if (cc == true)
                    {
                        ErrorMessage = eAcademy.Models.ResourceCache.Localize("Section_already_Exist_for_this_Class");
                    }
                    else
                    {
                        obj_section.addSection(secname, cid);
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_Section"));
                        Message = eAcademy.Models.ResourceCache.Localize("Section_Added_Successfully");
                    }
                    return Json(new { ErrorMessage = ErrorMessage, Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveEditSection(SaveSection model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string sectionName = model.section;
                    int cid = model.classid;
                    int sec_id = model.sectionId;
                    bool cc = obj_section.checkSection(sectionName, cid, sec_id);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Section_is__already_Exist_for_this_Class");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_section.updateSection(cid, sec_id, sectionName);
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_Section"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Subject()
        {
            try
            {
                ViewBag.count = obj_subject.getSubject().Count;
                return View(obj_subject.getSubject());
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public JsonResult SubjectResults(string sidx, string sord, int page, int rows, string subjectName)
        {
            IList<SubjectList> todoListsResults3 = new List<SubjectList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults3 = obj_subject.getSubject();
            if (!string.IsNullOrEmpty(subjectName))
            {
                todoListsResults3 = todoListsResults3.Where(p => p.Subject.Contains(subjectName)).ToList();
            }
            int totalRecords = todoListsResults3.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults3 = todoListsResults3.OrderByDescending(s => s.Subject).ToList();
                    todoListsResults3 = todoListsResults3.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults3 = todoListsResults3.OrderBy(s => s.Subject).ToList();
                    todoListsResults3 = todoListsResults3.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults3;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults3
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SubjectExportToExcel()
        {
            try
            {
                List<SubjectList> todoListsResults3 = (List<SubjectList>)Session["JQGridList"];
                var list = todoListsResults3.Select(o => new { Subject = o.Subject, Status = Convert.ToString(o.Status) }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("Subject");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult SubjectExportToPDF()
        {
            try
            {
                List<SubjectList> todoListsResults3 = (List<SubjectList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("SubjectList");
                GeneratePDF.ExportPDF_Portrait(todoListsResults3, new string[] { "Subject", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "Subject.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Subject(SaveSubject model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string sub = model.subject;
                    string GroupSubStatus = model.GroupSubjectStatus;
                    bool cc = obj_subject.checkSubject(sub);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Subject_Name_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        bool SubStatus;
                        if (GroupSubStatus == "Active")
                        {
                            SubStatus = true;
                        }
                        else
                        {
                            SubStatus = false;
                        }
                        obj_subject.addSubject(sub, SubStatus);
                        string Message = eAcademy.Models.ResourceCache.Localize("Subject_Added_Successfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_Subject"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditSubject(string sub_id)
        {
            try
            {
                int sId = Convert.ToInt32(QSCrypt.Decrypt(sub_id));
                var ans = obj_subject.getSubjectBySubjetID(sId);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditSubject(SaveSubject model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string subjectName = model.subject;
                    int sub_id = Convert.ToInt32(QSCrypt.Decrypt(model.subject_id));
                    string status = model.status;
                    string GroupSubStatus = model.GroupSubjectStatus;
                    //bool cc = obj_subject.checkSubject(subjectName, status);
                    var Cyear = acservice.getCurrentAcademicYearId(DateTimeByZone.getCurrentDate());
                    if (Cyear != null)
                    {
                        Session["CurrentAcYearId"] = Cyear.AcademicYearId;
                    }
                    else
                    {
                        Session["CurrentAcYearId"] = "";
                    }
                    int YearId;
                    if (Session["CurrentAcYearId"] != "")
                    {
                        YearId = Convert.ToInt16(Session["CurrentAcYearId"]);
                    }
                    else
                    {
                        YearId = 0;
                    }
                    var row = obj_subject.CheckSubjectAssgin(sub_id, YearId);
                    bool cc = obj_subject.checkExistSubject(subjectName, sub_id);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Subject_Name_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else if (row != null)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Subject_already_assign_to_section_So_Unable_to_Edit");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_subject.updateSubject(sub_id, subjectName, status, GroupSubStatus);
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_Subject"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GroupSubject()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        [HttpPost]
        public ActionResult GroupSubject(SaveGroupSubject model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string GroupName = model.GroupName;
                    //string GroupSubStatus = model.GroupSubjectStatus;
                    bool cc = obj_groupSubject.checkGroupName(GroupName);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("GroupSubject_Name_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        int counter = model.GroupSubjectId.Length;
                        if (counter != 1)
                        {
                            for (int i = 0; i < counter; i++)
                            {
                                string GroupSubStatus = model.GroupSubjectStatus[i];
                                int SubjectId = Convert.ToInt32(model.GroupSubjectId[i]);
                                //bool SubStatus;
                                if (GroupSubStatus == "Active")
                                {
                                    bool cas = obj_groupSubjectConfig.checkSubjectAssignStatus(SubjectId);
                                    if (cas == false)
                                    {
                                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Subject_already_Assign_Another_Group");
                                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }
                            int GroupSubjectId = obj_groupSubject.addGroup(GroupName, AdminId);
                            for (int j = 0; j < counter; j++)
                            {
                                int SubId = Convert.ToInt32(model.GroupSubjectId[j]);
                                string GSubStatus = model.GroupSubjectStatus[j];
                                bool SubStatus;
                                if (GSubStatus == "Active")
                                {
                                    SubStatus = true;
                                    obj_groupSubjectConfig.AddSubjectConfig(GroupSubjectId, SubId, SubStatus, AdminId);
                                }
                            }
                        }
                        else
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Group_should_contain_2_Subject");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        string Message = eAcademy.Models.ResourceCache.Localize("GroupSubject_Added_Successfully");
                        //useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_Subject"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }                       
                    }
                
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GroupResults(string sidx, string sord, int page, int rows, string GroupName)
        {
            IList<GroupList> todoListsResults3 = new List<GroupList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults3 = obj_groupSubject.getGroup();
            if (!string.IsNullOrEmpty(GroupName))
            {
                todoListsResults3 = todoListsResults3.Where(p => p.GroupName.Contains(GroupName)).ToList();
            }
            int totalRecords = todoListsResults3.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults3 = todoListsResults3.OrderByDescending(s => s.GroupName).ToList();
                    todoListsResults3 = todoListsResults3.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults3 = todoListsResults3.OrderBy(s => s.GroupName).ToList();
                    todoListsResults3 = todoListsResults3.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults3;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults3
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditGroup(string GroupSubjectId)
        {
            try
            {
                int gsId = Convert.ToInt32(GroupSubjectId);
                var ans = obj_groupSubject.getGroupByGroupID(gsId);
                var SelectSubList = obj_groupSubjectConfig.getubjectByGroupID(gsId);
                var GroupSubList = obj_subject.GroupSubject();
                return Json(new { ans = ans, SelectSubList = SelectSubList, GroupSubList = GroupSubList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditGroupSubject(EditGroupSubject model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string GroupName = model.GroupName;
                    string GroupStatus = model.GroupStatus;
                    int GroupId = model.GroupId;
                    bool G_Status;
                    var Cyear = acservice.getCurrentAcademicYearId(DateTimeByZone.getCurrentDate());
                    if (Cyear != null)
                    {
                        Session["CurrentAcYearId"] = Cyear.AcademicYearId;
                    }
                    else
                    {
                        Session["CurrentAcYearId"] = "";
                    }
                    int YearId;
                    if (Session["CurrentAcYearId"] != "")
                    {
                        YearId = Convert.ToInt16(Session["CurrentAcYearId"]);
                    }
                    else
                    {
                        YearId = 0;
                    }
                    if (GroupStatus == "Active")
                    {
                        G_Status = true;
                    }
                    else
                    {
                        var CheckGroup = obj_subject.CheckGroupSubjectAssgin(GroupId, YearId);
                        if (CheckGroup != null)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Subject_already_assign_to_section_So_Unable_to_change_status");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            G_Status = false;
                        }
                    }
                    //string GroupSubStatus = model.GroupSubjectStatus;
                    bool cc = obj_groupSubject.checkUpdateGroupName(GroupName, GroupId);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("GroupSubject_Name_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {                       
                        int counter = model.GroupSubjectId.Length;
                        //if (G_Status == true)
                        //{ 
                        if (counter != 1)
                        {

                            obj_groupSubject.UpdateGroup(GroupId, GroupName, G_Status, AdminId);
                            for (int i = 0; i < counter; i++)
                            {

                                string GroupConfigId = model.GroupConfigId[i];
                                string G_ConfigId;
                                if (GroupConfigId=="" )
                                {
                                    G_ConfigId = null;
                                }
                                else
                                {
                                    G_ConfigId = GroupConfigId;
                                }
                                int SubId = model.GroupSubjectId[i];
                                string GroupSubStatus = model.GroupSubjectStatus[i];
                                bool SubStatus;
                                if (G_ConfigId == null && SubId != null && GroupSubStatus == "Active" && GroupStatus == "Active")
                                    {
                                        bool cas = obj_groupSubjectConfig.checkSubjectAssignStatus(SubId);
                                        if (cas == false)
                                        {
                                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Subject_already_Assign_Another_Group");
                                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                        }
                                        else {
                                        SubStatus = true;
                                        obj_groupSubjectConfig.AddSubjectConfig(GroupId, SubId, SubStatus, AdminId);}
                                        
                                    }
                                else if (G_ConfigId != null)
                                    {
                                        if (GroupSubStatus == "Active" && GroupStatus == "Active")
                                        {
                                            SubStatus = true;
                                        }
                                        else
                                        {
                                            SubStatus = false;
                                        }
                                        obj_groupSubjectConfig.UpdateSubjectConfig(Convert.ToInt32(G_ConfigId), SubId, SubStatus, AdminId);
                                    }
                                else
                                {

                                }
                                                               
                            }

                            string Message = eAcademy.Models.ResourceCache.Localize("GroupSubject_Update_Successfully");                           
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("GroupSubject_should_contain_2_Subject");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        //}
                        //else
                        //{
                        //    obj_groupSubject.UpdateGroup(GroupId, GroupName, G_Status, AdminId);
                        //    for (int j = 0; j < counter; j++)
                        //    {
                        //        string GroupSubStatus = model.GroupSubjectStatus[j];
                        //        int SubId = model.GroupSubjectId[j];
                        //        if (GroupSubStatus == "Active")
                        //        {
                        //            obj_groupSubject.UpdateGroupStatus(GroupId, SubId);
                        //        }
                                

                        //    }
                        //    string Message = eAcademy.Models.ResourceCache.Localize("GroupSubject_Update_Successfully");
                        //    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        //}
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CheckSubjectExistAdd(int PassSubId)
        {
            bool cas = obj_groupSubjectConfig.checkSubjectAssignStatus(PassSubId);
            if (cas == false)
            {
                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Subject_already_Assign_Another_Group");
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            else 
            {
                //return Json(new { list = cc }, JsonRequestBehavior.AllowGet);
                string Message = "ok";
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            
        }
        public ActionResult Calender()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public JsonResult HolidayResults(string sidx, string sord, int page, int rows, int? FAcademicYrId)    //, string Achievement, DateTime? txt_doa_search
        {
            IList<EventList> todoListsResults4 = new List<EventList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults4 = obj_event.getEventList();
            if (FAcademicYrId.HasValue)
            {
                todoListsResults4 = todoListsResults4.Where(p => p.AcyearId == FAcademicYrId).ToList();
            }
            int totalRecords = todoListsResults4.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults4 = todoListsResults4.OrderByDescending(s => s.Acyear).ToList();
                    todoListsResults4 = todoListsResults4.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults4 = todoListsResults4.OrderBy(s => s.Acyear).ToList();
                    todoListsResults4 = todoListsResults4.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults4;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults4
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult HolidayExportToExcel()
        {
            try
            {
                IList<EventList> todoListsResults4 = (List<EventList>)Session["JQGridList"];
                var list = todoListsResults4.Select(o => new { AcademicYear = o.Acyear, HolidayName = o.HolidayName, Date = o.Date, Description = o.Description }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("HolidayDetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult HolidayExportToPDF()
        {
            try
            {
                IList<EventList> todoListsResults4 = (List<EventList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("HolidayDetails");
                GeneratePDF.ExportPDF_Portrait(todoListsResults4, new string[] { "Acyear", "HolidayName", "Date", "Description" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "HolidayDetails.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult Calender(SaveCalender model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acYid = model.Event_acYear;
                    string eventtype = model.txt_event;
                    string event_desc = model.txt_event_desc;
                    DateTime event_date = model.txt_event_date;
                    bool cc = obj_event.checkCalendar(eventtype, event_date, event_desc);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Event_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (model.Announcement_option == true)
                        {
                            var eventid = obj_event.addEvent(acYid, event_date, eventtype, event_desc, AdminId);
                            obj_announcement.addAnnouncements(acYid, event_date, eventtype, event_desc, AdminId, eventid);
                            string Message = eAcademy.Models.ResourceCache.Localize("Event_and_Announcement_stored_successfully");
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_Events"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }

                        else
                        {
                            obj_event.addEvent(acYid, event_date, eventtype, event_desc, AdminId);
                            string Message = eAcademy.Models.ResourceCache.Localize("Event_Added_Successfully");
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_Events"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getCalenderRecord(int acid)
        {
            try
            {
                if (acid == 0)
                {
                    var ans = obj_event.getEventList();
                    var count = ans.Count;
                    return Json(new { ans = ans, c = count }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var ans = obj_event.getEventListByAcademicYearID(acid);
                    var count = ans.Count;
                    return Json(new { ans = ans, c = count }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditCalender(string eventId)
        {
            try
            {
                int EId = Convert.ToInt32(QSCrypt.Decrypt(eventId));
                var ans = obj_event.getEventByEventId(EId);
                var EventId = obj_announcement.geteventid(EId);
                bool existacademicyear = obj_academicYear.PastAcademicyear(ans.AcademicYear);
                if (existacademicyear == true)
                {
                    return Json(new { ans = ans, EventId = EventId }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                    return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                }
               
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditCalender(EditCalender model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int eventid = Convert.ToInt32(QSCrypt.Decrypt(model.EventId));
                    string eventName = model.txt_event;
                    string eventDesc = model.txt_event_desc;
                    DateTime date = model.txt_event_date;
                    string status = model.Status;
                    bool s = true;
                    if (status == "Active")
                    {
                        s = true;
                    }
                    else
                    {
                        s = false;
                    }
                    bool cc = obj_event.checkCalendar(eventid, eventName, date, eventDesc, status);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Event_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {

                        obj_event.updateEvent(eventid, eventName, date, eventDesc, status, AdminId);

                        if (model.Editannouncementstatus == false)
                        {
                            //update announcement status is false
                            var eid = obj_announcement.geteventid(eventid);//get announcement event id
                            if (eid != null)
                            {
                                obj_announcement.updateEventAnnouncement(eid.eventid, model.Editannouncementstatus, eventName, date, eventDesc, AdminId);
                            }

                        }
                        else
                        {
                            var eid = obj_announcement.geteventid(eventid);

                            if (eid == null)
                            {
                                //add announcement
                                var ans = obj_event.getEventdetails(eventid);
                                obj_announcement.addAnnouncements(ans.AcademicYearId, ans.EventDate, ans.EventName, ans.EventDecribtion, AdminId, eventid);
                            }
                            else
                            {
                                //update announcement status is true
                                obj_announcement.updateEventAnnouncement(eid.eventid, s, eventName, date, eventDesc, AdminId);
                            }
                        }

                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_Events"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Announcement()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public JsonResult AnnouncementResults1(string sidx, string sord, int page, int rows, int? FAcademicYrId)    //, string Achievement, DateTime? txt_doa_search
        {
            IList<Announcements> todoListsResults5 = new List<Announcements>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults5 = obj_announcement.getAnnouncements();
            if (FAcademicYrId.HasValue)
            {
                todoListsResults5 = todoListsResults5.Where(p => p.acYear == FAcademicYrId).ToList();
            }
            int totalRecords = todoListsResults5.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults5 = todoListsResults5.OrderByDescending(s => s.AcademicYear).ToList();
                    todoListsResults5 = todoListsResults5.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults5 = todoListsResults5.OrderBy(s => s.AcademicYear).ToList();
                    todoListsResults5 = todoListsResults5.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults5;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults5
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AnnouncementExportToExcel()
        {
            try
            {
                IList<Announcements> todoListsResults5 = (List<Announcements>)Session["JQGridList"];
                var list = todoListsResults5.Select(o => new { AcademicYear = o.AcademicYear, AnnouncementDate = o.AnnouncementDate, AnnouncementName = o.AnnouncementName, Description = o.Desc, Status = o.Status.ToString() }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("AnnouncementDetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult AnnouncementExportToPDF()
        {
            try
            {
                IList<Announcements> todoListsResults5 = (List<Announcements>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("AnnouncementDetails");
                GeneratePDF.ExportPDF_Portrait(todoListsResults5, new string[] { "AcademicYear", "AnnouncementDate", "AnnouncementName", "Description", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "Announcement.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Announcement(SaveAnnouncement model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acYid = model.Announcement_acYear;
                    string Announcement_type = model.txt_Announcement;
                    string Announcement_desc = model.txt_Announcement_desc;
                    DateTime Announcement_date = model.txt_Announcement_date;
                    bool pushnotification = Convert.ToBoolean(model.Push_Notification);
                    bool cc = obj_announcement.checkAnnouncement(Announcement_type, Announcement_date, Announcement_desc);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Announcement_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_announcement.addAnnouncements(acYid, Announcement_date, Announcement_type, Announcement_desc, AdminId, pushnotification);
                        string Message = eAcademy.Models.ResourceCache.Localize("Announcement_Added_Successfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_Announcement"));
                       
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);

                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getAnnouncementRecord(int acid)
        {
            try
            {
                if (acid == 0)
                {
                    var ans = obj_announcement.getAnnouncementList();
                    var count = ans.Count;
                    return Json(new { ans = ans, c = count }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var ans = obj_announcement.getAnnouncementListByAcademicYear(acid);
                    var count = ans.Count;
                    return Json(new { ans = ans, c = count }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditAnnouncement(string AnnouncementId)
        {
            try
            {
                int AId = Convert.ToInt32(QSCrypt.Decrypt(AnnouncementId));
                var ans = obj_announcement.getAnnouncementByAnnouncementID(AId);
                bool existacademicyear = obj_academicYear.PastAcademicyear(ans.acYear);
                if (existacademicyear == true)
                {
                    return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                    return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                }
               
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditAnnouncement(EditAnnouncement model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int aid = Convert.ToInt32(QSCrypt.Decrypt(model.AnnouncementId));
                    string aName = model.txt_Announcement;
                    string aDesc = model.txt_Announcement_desc;
                    DateTime date = model.txt_Announcement_date;
                    string status = model.Status;
                    bool Edit_push = Convert.ToBoolean(model.Push_notification);
                    bool cc = obj_announcement.checkAnnouncement(aid, aName, date, aDesc, status);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Announcement_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_announcement.updateAnnouncement(aid, date, aName, aDesc, status, Edit_push);
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_Announcement"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [EncryptedActionParameter]
        public ActionResult DeleteAnnouncement(int AnnouncementId)
        {
            try
            {
                obj_announcement.deleteAnnouncement(AnnouncementId);
                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Delete_Announcement"));
                TempData["Message"] = eAcademy.Models.ResourceCache.Localize("Removed_Successfully");
                return RedirectToAction("Announcement");
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("Announcement");
            }
        }
        public ActionResult Facility()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public JsonResult FacilityResults(string sidx, string sord, int page, int rows)
        {
            IList<FacilityList> todoListsResults6 = new List<FacilityList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults6 = obj_facility.getFacility();
            int totalRecords = todoListsResults6.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults6 = todoListsResults6.OrderByDescending(s => s.Facility).ToList();
                    todoListsResults6 = todoListsResults6.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults6 = todoListsResults6.OrderBy(s => s.Facility).ToList();
                    todoListsResults6 = todoListsResults6.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults6;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults6
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FacilityExportToExcel()
        {
            try
            {
                IList<FacilityList> todoListsResults6 = (List<FacilityList>)Session["JQGridList"];
                var list = todoListsResults6.Select(o => new { Facility = o.Facility, Description = o.Desc, Status = Convert.ToString(o.Status).ToLower() }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("FacilityDetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult FacilityExportToPDF()
        {
            try
            {
                IList<FacilityList> todoListsResults6 = (List<FacilityList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("FacilityDetails");
                GeneratePDF.ExportPDF_Portrait(todoListsResults6, new string[] { "Facility", "Description", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "FacilityDetails.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Facility(SaveFacility model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string fac = model.txt_facility;
                    string fac_desc = model.txt_fac_desc;
                    bool cc = obj_facility.checkFacility(fac, fac_desc);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Facility_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_facility.addFacility(fac, fac_desc);
                        string Message = eAcademy.Models.ResourceCache.Localize("Facility_Added_Successfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_Facility"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditFacility(string FacilityId)
        {
            try
            {
                int FId = Convert.ToInt32(QSCrypt.Decrypt(FacilityId));
                var ans = obj_facility.getFacilitesByFaculityId(FId);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditFacility(EditFacility model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int fid = Convert.ToInt32(QSCrypt.Decrypt(model.FacilityId));
                    string fac = model.txt_facility;
                    string fac_desc = model.txt_fac_desc;
                    string status = model.Status;
                    bool cc = obj_facility.checkFacility(fid, fac, fac_desc, status);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Facility_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_facility.updateFacility(fid, fac, fac_desc, status);
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_Facility"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [EncryptedActionParameter]
        public ActionResult DeleteFacility(int FacilityId)
        {
            try
            {
                obj_facility.deleteFacility(FacilityId);
                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Delete_Facility"));
                TempData["Message"] = eAcademy.Models.ResourceCache.Localize("Removed_Successfully");
                return RedirectToAction("Facility");
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("Facility");
            }
        }
        public ActionResult ExtraAcitivity()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public JsonResult ExtraAcitivityResults(string sidx, string sord, int page, int rows, int? ddExtraActivity)
        {
            IList<ActivityList> todoListsResults7 = new List<ActivityList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults7 = obj_activity.getActivity();
            if (ddExtraActivity.HasValue)
            {
                todoListsResults7 = todoListsResults7.Where(p => p.ActivityId == ddExtraActivity).ToList();
            }
            int totalRecords = todoListsResults7.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults7 = todoListsResults7.OrderByDescending(s => s.Activity).ToList();
                    todoListsResults7 = todoListsResults7.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults7 = todoListsResults7.OrderBy(s => s.Activity).ToList();
                    todoListsResults7 = todoListsResults7.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults7;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults7
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActivityExportToExcel()
        {
            try
            {
                List<ActivityList> todoListsResults7 = (List<ActivityList>)Session["JQGridList"];
                var list = todoListsResults7.Select(o => new { Activity = o.Activity, Description = o.Desc, Status = o.Status.ToString().ToLower() }).ToList();//Convert.ToString(o.Status).ToLower()
                string fileName = eAcademy.Models.ResourceCache.Localize("ActivityDetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ActivityExportToPDF()
        {
            try
            {
                List<ActivityList> todoListsResults7 = (List<ActivityList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ActivityDetails");
                GeneratePDF.ExportPDF_Portrait(todoListsResults7, new string[] { "Activity", "Description", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "ExtraActivity.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult ExtraAcitivity(SaveActivity model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string activity = model.txt_activity;
                    string activity_desc = model.txt_activity_desc;
                    bool cc = obj_activity.checkAcitivity(activity, activity_desc);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Activity_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_activity.addActivity(activity, activity_desc);
                        string Message = eAcademy.Models.ResourceCache.Localize("Activity_Added_Successfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_Activity"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditExtraAcitivity(string AcitivityId)
        {
            try
            {
                int AId = Convert.ToInt32(QSCrypt.Decrypt(AcitivityId));
                var ans = obj_activity.ExtraActivitiesByActivityID(AId);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditExtraAcitivity(EditActivity model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int aid = Convert.ToInt32(QSCrypt.Decrypt(model.ActivityId));
                    string activity = model.txt_activity;
                    string activity_desc = model.txt_activity_desc;
                    string status = model.Status;
                    bool cc = obj_activity.checkAcitivity(aid, activity, activity_desc, status);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Activity_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_activity.updateAcitivity(aid, activity, activity_desc, status);
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_Activity"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [EncryptedActionParameter]
        public ActionResult DeleteExtraAcitivity(int AcitivityId)
        {
            try
            {
                obj_activity.deleteAcitivity(AcitivityId);
                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Delete_Activity"));
                TempData["Message"] = eAcademy.Models.ResourceCache.Localize("Removed_Successfully");
                return RedirectToAction("ExtraAcitivity");
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("ExtraAcitivity");
            }
        }
        public ActionResult ClassStrength()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult getClass()
        {
            try
            {
                var ans = obj_class.Classes();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getSectionList(int acid, int cid)
        {
            try
            {
                var ans = obj_section.getSectionByClassId(acid, cid);
                var Count = ans.Count;
                if (Count == 0)
                {
                    string sectionCount = "empty";
                    return Json(new { ans, sectionCount }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { ans }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ClassStrengthResults(string sidx, string sord, int page, int rows, int? Strength_Acid, int? Strent_Cid)
        {
            IList<StrengthList> todoListsResults8 = new List<StrengthList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults8 = obj_strength.getStrength();
            if (Strength_Acid.HasValue)
            {
                todoListsResults8 = todoListsResults8.Where(p => p.Acid == Strength_Acid).ToList();
            }
            if (Strength_Acid.HasValue && Strent_Cid.HasValue)
            {
                todoListsResults8 = todoListsResults8.Where(p => p.Acid == Strength_Acid && p.Classid == Strent_Cid).ToList();
            }
            int totalRecords = todoListsResults8.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults8 = todoListsResults8.OrderByDescending(s => s.Classid).ToList();
                    todoListsResults8 = todoListsResults8.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults8 = todoListsResults8.OrderBy(s => s.Classid).ToList();
                    todoListsResults8 = todoListsResults8.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults8;

            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults8
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ClassStrengthExportToExcel()
        {
            try
            {
                List<StrengthList> todoListsResults8 = (List<StrengthList>)Session["JQGridList"];
                var list = todoListsResults8.Select(o => new { AcademicYear = o.AcademicYear, Class_Section = o.ClassSection, Strength = o.Strength, Status = o.Status.Value.ToString() }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassStrengthDetailsxls");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ClassStrengthExportToPDF()
        {
            try
            {
                List<StrengthList> todoListsResults8 = (List<StrengthList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassStrengthDetailsPdf");
                GeneratePDF.ExportPDF_Portrait(todoListsResults8, new string[] { "AcademicYear", "ClassSection", "Strength", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "ClassStrengthDetails.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public JsonResult ClassStrength(SaveStrength model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = model.Acid;
                    int cid = model.Cid;
                    int count = model.Count;
                    int[] secid = new int[count];
                    int[] strength = new int[count];
                    string[] Section = new string[count];
                    string Message = "", ErrorMessage = "";
                    for (int i = 0; i < count; i++)
                    {
                        secid[i] = model.Secid[i];
                        strength[i] = model.Strength[i];
                        Section[i] = model.Section[i];
                        bool cc = obj_strength.checkStrength(acid, cid, secid[i]);
                        if (cc == true)
                        {
                            ErrorMessage = eAcademy.Models.ResourceCache.Localize("Data_Exist_for_Section ") + Section[i];
                        }
                        else
                        {
                            obj_strength.addStrength(acid, cid, secid[i], strength[i]);
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_ClassStrength"));
                            Message = eAcademy.Models.ResourceCache.Localize("Strength_Added_Successfully");
                        }
                    }
                    return Json(new { ErrorMessage = ErrorMessage, Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditClassStrength(string CSid)
        {
            try
            {
                int cs_Id = Convert.ToInt32(QSCrypt.Decrypt(CSid));
                var ans = obj_strength.getStrength(cs_Id);
                bool existacademicyear = obj_academicYear.PastAcademicyear(ans.Acid);
                if (existacademicyear == true)
                {
                    return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                    return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditClassStrength(EditStrength model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string status = model.Status1;
                    int Strength = model.Strength;
                    int CSid = Convert.ToInt32(QSCrypt.Decrypt(model.Strength_Id));
                    int acyear = model.Acid;
                    var existstudentcount = obj_strength.checkstudentstrength(CSid, acyear);
                    if (existstudentcount > Strength)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Cant_Decrease_class_strength_Because_Students_already_assigned") + existstudentcount;
                        //string ErrorMessage = "existstudentcount";
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_strength.updatClassStrength(CSid, Strength, status);
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_ClassStrength"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}