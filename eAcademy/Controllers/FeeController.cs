﻿using eAcademy.DataModel;
using eAcademy.ViewModel;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eAcademy.HelperClass;

namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class FeeController : Controller
    {

        // GET: /Fee/
        Data ee = new Data();
        EacademyEntities db = new EacademyEntities();

        public int ParentRegId = 6;
        public int StudentRegId = 19;

        UserActivityHelper useractivity = new UserActivityHelper();

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ViewBag.allAcademicYears = ShowAcademicYears();
            ViewBag.allFee = ShowFee();
        }

        public IEnumerable<Object> ShowAcademicYears()
        {
            var AcademicYearList = (from a in db.AcademicYears
                                    select new
                                    {
                                        AcademicYear = a.AcademicYear1,
                                        AcademicYearId = a.AcademicYearId
                                    }).ToList().AsEnumerable();
            return AcademicYearList;
        }

        public IEnumerable<Object> ShowFee()
        {
            var allFeeList = (from a in db.FeeCategories
                              select new
                              {
                                  FeeName = a.FeeCategoryName,
                                  FeeId = a.FeeCategoryId
                              }).ToList().AsEnumerable();
            return allFeeList;
        }

        public ActionResult FeeDetails()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FeeDetails(FeeDetails fd)
        {
            if (ModelState.IsValid)
            {
                var getRow = (from a in db.AssignClassToStudents where a.StudentRegisterId == StudentRegId && a.AcademicYearId == fd.AcademicYearId select a).FirstOrDefault();
                int classId = Convert.ToInt32(getRow.ClassId);
                int secId = Convert.ToInt32(getRow.SectionId);

                var list = (from a in db.TermFees
                            from b in db.Terms
                            from c in db.FeeCollections
                            where a.AcademicYearId == fd.AcademicYearId &&
                            a.FeeCategoryId == fd.FeeCategoryId &&
                            a.TermId == b.TermId &&
                            c.AcademicYearId == fd.AcademicYearId &&
                            c.ClassId == classId &&
                            c.SectionId == secId &&
                            c.StudentRegisterId == StudentRegId
                            //c.FeeCategoryId == a.FeeCategoryId 
                            //c.PaymentTypeId == b.TermId

                            select new FeeDetails
                            {
                                FeeName = b.TermName,
                                FeeAmount = a.Amount,
                                DateToPay = a.LastDate,
                                PaidDate = c.CollectionDate
                            }).ToList();

                var list1 = (from a in db.FeeCollections
                             from b in db.Fees
                             where a.AcademicYearId == fd.AcademicYearId &&
                                   a.ClassId == classId &&
                                   a.SectionId == secId &&
                                   a.StudentRegisterId == StudentRegId &&
                                 //a.FeeCategoryId == b.FeeCategoryId &&
                                   b.ClassId == classId
                             select new FeeDetails
                             {
                                 FeeName = "Annual",
                                 FeeAmount = b.Amount,
                                 DateToPay = b.LastDate,
                                 PaidDate = a.CollectionDate
                             }).ToList();

                ViewBag.count = list.Count;
                ViewBag.yearId = fd.AcademicYearId.ToString("dd/mm/yyyy");
                return View(list);
            }
            return View();
        }
    }
}
