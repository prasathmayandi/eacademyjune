﻿using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class SchoolConfigurationController : Controller
    {
        SchoolSettingsServices obj_schoolSettings = new SchoolSettingsServices();
        TimeScheduleServices obj_timeSettings = new TimeScheduleServices();
        PeriodsScheduleServices obj_periodSettings = new PeriodsScheduleServices();
        ClassServices obj_class = new ClassServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        static string fname;
        static string extention;
        public ActionResult Settings()
        {
            try
            {
                tblCountryServices obj_country = new tblCountryServices();
                ViewBag.Country = obj_country.CountryList();
                ViewBag.Currency = obj_country.Currency();
                ViewBag.DayOrderList = obj_country.dayOrderList();
                var schoolsetting = obj_schoolSettings.getSchoolSettings();
                if (schoolsetting.Count > 0)
                {
                    var ans1 = obj_schoolSettings.SchoolSettings();
                    ViewBag.EditId = ans1.SettingsId;
                    ViewBag.schoolName = ans1.SchoolName;
                    ViewBag.Address1 = ans1.AddressLine1;
                    ViewBag.Address2 = ans1.AddressLine2;
                    ViewBag.City = ans1.City;
                    ViewBag.State = ans1.State;
                    ViewBag.Zip = ans1.ZIP;
                    ViewBag.Country1 = ans1.Country;
                    ViewBag.Phone1 = ans1.Phone1;
                    ViewBag.Phone2 = ans1.Phone2;
                    ViewBag.Mobile1 = ans1.Mobile1;
                    ViewBag.Mobile12 = ans1.Mobile2;
                    ViewBag.Fax = ans1.Fax;
                    ViewBag.Email = ans1.Email;
                    ViewBag.Website = ans1.Website;
                    ViewBag.Currency1 = ans1.CurrencyType;
                    ViewBag.dayOrder = ans1.DayOrder;
                    ViewBag.markFormat = ans1.MarkFormat;
                    ViewBag.stdIdFormat = ans1.StudentIdFormat;
                    ViewBag.StaffIdFormat = ans1.StaffIDFormat;
                    ViewBag.Non_StaffIdFormat = ans1.Non_StaffIDFormat;
                    ViewBag.schoolLogo = ans1.SchoolLogo;
                    ViewBag.tax = Math.Round(ans1.ServicesTax.Value, 2, MidpointRounding.AwayFromZero);
                    ViewBag.VAT = Math.Round(ans1.VAT.Value, 2, MidpointRounding.AwayFromZero);
                    ViewBag.formformat = ans1.FormFormat;
                    ViewBag.schoolCode = ans1.SchoolCode;
                    ViewBag.parentIdFormat = ans1.ParentIDFormat;
                    ViewBag.Transport = ans1.TransportRequired;
                    ViewBag.hostel = ans1.HostelRequired;
                    ViewBag.Email1 = ans1.EmailRequired;
                    ViewBag.Sms = ans1.SmsRequired;
                    ViewBag.App = ans1.AppRequired;

                }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        [HttpPost]
        public ContentResult UploadFiles()
        {
            var r = new List<UploadFilesResult>();
            foreach (string file in Request.Files)
            {
                HttpPostedFileBase uploadfile = Request.Files[file] as HttpPostedFileBase;
                fname = "logo";
                extention = Path.GetExtension(uploadfile.FileName);
                r.Add(new UploadFilesResult()
                {
                    Name = uploadfile.FileName,
                    Length = uploadfile.ContentLength,
                    Type = uploadfile.ContentType
                });
            }
            return Content("{\"name\":\"" + r[0].Name + "\",\"type\":\"" + r[0].Type + "\",\"size\":\"" + string.Format("{0} bytes", r[0].Length) + "\"}", "application/json");
        }
        [HttpPost]
        public JsonResult Settings(SaveSchoolSettings model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveSettings()
        {
            try
            {
                HttpPostedFileBase uploadfile = Request.Files["uploadfile"];
                fname = "logo";
                extention = Path.GetExtension(uploadfile.FileName);
                var r = new List<UploadFilesResult>();
                string schoolCode = Request.Form["txt_schoolcode"];
                string schoolName = Request.Form["txt_schoolName"];
                string schoolLogo = fname + extention;
                string Adress1 = Request.Form["txt_Address1"];
                string Adress2 = Request.Form["txt_Address2"];
                string City = Request.Form["txt_City"];
                string State = Request.Form["txt_State"];
                string Zip = Request.Form["txt_ZIP"];
                string Country = Request.Form["txt_Country"];
                string Phone1 = Request.Form["txt_Phone1"];
                string Phone2 = Request.Form["txt_Phone2"];
                long Mobile1 = long.Parse(Request.Form["txt_Mobile1"]);
                long Mobile2 = long.Parse(Request.Form["txt_Mobile2"]);
                string Website = Request.Form["txt_Website"];
                string Email = Request.Form["txt_Email"];
                string Fax = Request.Form["txt_Fax"];
                string parentIdFormat = Request.Form["txt_parentIdFormat"];
                string stdIdFormat = Request.Form["txt_stdIdFormat"];
                string staffIdFormat = Request.Form["txt_staffIdFormat"];
                string non_staffIdFormat = Request.Form["txt_nonStaffIdFormat"];
                int Currency = int.Parse(Request.Form["txt_Currency"]);
                int dayOrder = int.Parse(Request.Form["txt_dayOrder"]);
                string markFormat = Request.Form["txt_markFormat"];
                Decimal? tax = Decimal.Parse(Request.Form["txt_Tax"]);
                Decimal? VAT = 0;
                if (!String.IsNullOrEmpty(Request.Form["txt_VAT"].ToString()))
                {
                    VAT = Decimal.Parse(Request.Form["txt_VAT"].ToString());
                }
                bool transport =Convert.ToBoolean(Request.Form["TransportRequired"]);
                bool Hostel = Convert.ToBoolean(Request.Form["HostelRequired"]);
                bool Email1 = Convert.ToBoolean(Request.Form["EmailRequired"]);
                bool Sms = Convert.ToBoolean(Request.Form["SmsRequired"]);
                bool App = Convert.ToBoolean(Request.Form["AppRequired"]);
                string formFormat = Request.Form["txt_formFormat"];
                obj_schoolSettings.addSchoolSetting(schoolName, schoolLogo, Adress1, Adress2, City, State, Zip, Country, Phone1, Phone2, Mobile1, Mobile2, Email, Fax, Website, stdIdFormat, staffIdFormat, non_staffIdFormat, Currency, dayOrder, markFormat, tax, VAT, formFormat, schoolCode, parentIdFormat,transport,Hostel,Email1,Sms,App);
                if (uploadfile != null && uploadfile.ContentLength > 0 && extention == ".png")
                {
                    string filenamewithoutextention = Path.GetFileNameWithoutExtension(uploadfile.FileName);
                    filenamewithoutextention = fname;
                    string path = Path.Combine(Server.MapPath("~/img"), filenamewithoutextention + extention);
                    uploadfile.SaveAs(path);
                    r.Add(new UploadFilesResult()
                    {
                        Name = uploadfile.FileName,
                        Length = uploadfile.ContentLength,
                        Type = uploadfile.ContentType
                    });
                }
                var ans = obj_schoolSettings.SchoolSettings();
                Session["schoolName"] = ans.SchoolName;
                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Addschoolsettings"));
                string Message = eAcademy.Models.ResourceCache.Localize("SettingsSavedSuccessfully");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult EditSettings(SaveSchoolSettings model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveEditSettings()
        {
            try
            {
                SchoolSettingServices school = new SchoolSettingServices();
                HttpPostedFileBase uploadfile = null;
                string schoolLogo = "";
                if (Request.Files["uploadfile"] != null)
                {
                    uploadfile = Request.Files["uploadfile"];
                    fname = "logo";
                    extention = Path.GetExtension(uploadfile.FileName);
                    schoolLogo = fname + extention;
                }
                else
                {
                    var getschool = school.getSchoolSettings();
                    schoolLogo = getschool.SchoolLogo;
                }
                var r = new List<UploadFilesResult>();
                int id = int.Parse(Request.Form["sid"]); ;
                string schoolCode = Request.Form["txt_schoolcode"];
                string schoolName = Request.Form["txt_schoolName"];
                string Adress1 = Request.Form["txt_Address1"];
                string Adress2 = Request.Form["txt_Address2"];
                string City = Request.Form["txt_City"];
                string State = Request.Form["txt_State"];
                string Zip = Request.Form["txt_ZIP"];
                string Country = Request.Form["txt_Country"];
                string Phone1 = Request.Form["txt_Phone1"];
                string Phone2 = Request.Form["txt_Phone2"];
                long Mobile1 = long.Parse(Request.Form["txt_Mobile1"]);
                long Mobile2 = long.Parse(Request.Form["txt_Mobile2"]);
                string Website = Request.Form["txt_Website"];
                string Email = Request.Form["txt_Email"];
                string Fax = Request.Form["txt_Fax"];
                string parentIdFormat = Request.Form["txt_parentIdFormat"];
                string stdIdFormat = Request.Form["txt_stdIdFormat"];
                string staffIdFormat = Request.Form["txt_staffIdFormat"];
                string non_staffIdFormat = Request.Form["txt_nonStaffIdFormat"];
                int Currency = int.Parse(Request.Form["txt_Currency"]);
                int dayOrder = int.Parse(Request.Form["txt_dayOrder"]);
                string markFormat = Request.Form["txt_markFormat"];
                Decimal? tax = Decimal.Parse(Request.Form["txt_Tax"]);
                Decimal? VAT = 0;
                if (!String.IsNullOrEmpty(Request.Form["txt_VAT"].ToString()))
                {
                    VAT = Decimal.Parse(Request.Form["txt_VAT"].ToString());
                }
                string formFormat = Request.Form["txt_formFormat"];
                bool transport = Convert.ToBoolean(Request.Form["TransportRequired"]);
                bool Hostel = Convert.ToBoolean(Request.Form["HostelRequired"]);
                bool Email1 = Convert.ToBoolean(Request.Form["EmailRequired"]);
                bool Sms = Convert.ToBoolean(Request.Form["SmsRequired"]);
                bool App = Convert.ToBoolean(Request.Form["AppRequired"]);
                obj_schoolSettings.UpdateSchoolSetting(id, schoolName, schoolLogo, Adress1, Adress2, City, State, Zip, Country, Phone1, Phone2, Mobile1, Mobile2, Email, Fax, Website, stdIdFormat, staffIdFormat, non_staffIdFormat, Currency, dayOrder, markFormat, tax, VAT, formFormat, schoolCode, parentIdFormat,transport,Hostel,Email1,Sms,App);
                if (uploadfile != null)
                {
                    if (uploadfile != null && uploadfile.ContentLength > 0 && extention == ".png")
                    {
                        string filenamewithoutextention = Path.GetFileNameWithoutExtension(uploadfile.FileName);
                        filenamewithoutextention = fname;
                        string path = Path.Combine(Server.MapPath("~/img"), filenamewithoutextention + extention);
                        uploadfile.SaveAs(path);
                        r.Add(new UploadFilesResult()
                        {
                            Name = uploadfile.FileName,
                            Length = uploadfile.ContentLength,
                            Type = uploadfile.ContentType
                        });
                    }
                }
                var ans = obj_schoolSettings.SchoolSettings();
                Session["schoolName"] = ans.SchoolName;
                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Updateschoolsettings"));
                string Message = eAcademy.Models.ResourceCache.Localize("SettingsSavedSuccessfully");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult TimeSettings()
        {
            try
            {
                ViewBag.count = obj_schoolSettings.getSchoolSettings().Count();
                return View(obj_timeSettings.getTimeSchedule());
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public ActionResult JsonTimeSettings()
        {
            try
            {
                var ans = obj_timeSettings.getTimeSchedule();
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        [HttpPost]
        public JsonResult TimeSettings(TimePeriodSchedule model)
        {
            try
            {



                if (ModelState.IsValid)
                {
                    int TotalCount = model.TotalCount;
                    string Schedule = model.txt_Schedule;
                    Boolean Status = model.Status.Value;
                    int NumberPeriod = model.NumberPeriod;
                    string[] Periods = new string[TotalCount];
                    int?[] PeriodNumber = new int?[TotalCount];
                    string[] Type = new string[TotalCount];
                    TimeSpan[] StartTime = new TimeSpan[TotalCount];
                    TimeSpan[] EndTime = new TimeSpan[TotalCount];
                    bool cc = obj_timeSettings.checkTimeSchedule(Schedule);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("ScheduleisalreadyAvailable");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string[] PeriodsName = model.Periods;
                        for (int i = 0; i < PeriodsName.Length; i++)
                        {
                            for (int j = 0; j < PeriodsName.Length; j++)
                            {
                                if (i == j)
                                {

                                }
                                else
                                {
                                    if (model.Periods[i] == PeriodsName[j])
                                    {
                                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_Do_not_repeat_same_Period_Break_name");
                                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                    }

                                }
                            }
                        }



                        obj_timeSettings.addTimeSchedule(Schedule, NumberPeriod, Status);
                        var ans = obj_timeSettings.getTimeSchedule(Schedule, NumberPeriod, Status);
                        for (int i = 0; i < TotalCount; i++)
                        {
                            Periods[i] = model.Periods[i];
                            PeriodNumber[i] = model.period_No[i];
                            Type[i] = model.Type[i];
                            StartTime[i] = model.StartTime[i];
                            EndTime[i] = model.EndTime[i];
                            obj_periodSettings.addPeriodsSchedule(ans.TimeScheduleId, Periods[i], Type[i], StartTime[i], EndTime[i], PeriodNumber[i]);
                        }
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Addtimesettings"));
                        string Message = eAcademy.Models.ResourceCache.Localize("SettingsSavedSuccessfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t }, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult EditTimeSettings(int ScheduleId)
        {
            try
            {
                var schedule = obj_timeSettings.getTimeSchedule(ScheduleId);
                var ans = obj_periodSettings.getPeriodsById(schedule.ScheduleID);
                return Json(new { Schedule = schedule, ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult UpdateTimeSettings(TimePeriodSchedule model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int TotalCount = model.TotalCount;
                    int TimeScheduleId = model.TimeScheduleId;
                    string Schedule = model.txt_Schedule;
                    Boolean Status = model.Status.Value;
                    int NumberPeriod = model.NumberPeriod;
                    int[] period_ScheduleId = new int[TotalCount];
                    int?[] PeriodNumber = new int?[TotalCount];
                    string[] Periods = new string[TotalCount];
                    string[] Type = new string[TotalCount];
                    TimeSpan[] StartTime = new TimeSpan[TotalCount];
                    TimeSpan[] EndTime = new TimeSpan[TotalCount];
                    string[] PeriodsName = model.Periods;
                    for (int i = 0; i < PeriodsName.Length; i++)
                    {
                        for (int j = 0; j < PeriodsName.Length; j++)
                        {
                            if (i == j)
                            {

                            }
                            else
                            {
                                if (model.Periods[i] == PeriodsName[j])
                                {
                                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_Do_not_repeat_same_Period_Break_name");
                                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }

                            }
                        }
                    }
                    obj_timeSettings.updateTimeSchedule(TimeScheduleId, Schedule, NumberPeriod, Status);
                    var ans = obj_timeSettings.getTimeSchedule(Schedule, NumberPeriod, Status);
                    for (int i = 0; i < TotalCount; i++)
                    {

                        period_ScheduleId[i] = model.period_ScheduleId[i];
                        PeriodNumber[i] = model.period_No[i];
                        var periodid = period_ScheduleId[i];
                        Periods[i] = model.Periods[i];
                        Type[i] = model.Type[i];
                        StartTime[i] = model.StartTime[i];
                        EndTime[i] = model.EndTime[i];
                        if (periodid == 0)
                        {
                            obj_periodSettings.addPeriodsSchedule(ans.TimeScheduleId, Periods[i], Type[i], StartTime[i], EndTime[i], PeriodNumber[i]);
                        }
                        else
                        {
                            obj_periodSettings.updatePeriodsSchedule(ans.TimeScheduleId, period_ScheduleId[i], Periods[i], Type[i], StartTime[i], EndTime[i], PeriodNumber[i]);
                        }
                    }
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Updatetimesettings"));
                    string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t }, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ClassSettings()
        {
            try
            {
                var count1 = obj_schoolSettings.getSchoolSettings().Count();
                ViewBag.count1 = count1;
                if (count1 > 0)
                {
                    ViewBag.count = obj_timeSettings.getTimeSchedule().Count();
                }
                ViewBag.TimeSchedule = obj_timeSettings.getTimeScheduleList();
                var list = obj_class.getClasswithSchedule();
                return View(list);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        [HttpPost]
        public JsonResult ClassSettings(ClassTime model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Boolean[] status = new Boolean[15];
                    int[] classid = new int[15];
                    int[] scheduleid = new int[15];
                    for (int i = 0; i < 15; i++)
                    {
                        classid[i] =Convert.ToInt32(model.ClassID[i]);
                        scheduleid[i] = Convert.ToInt32(model.ScheduleID[i]);
                        status[i] = model.Status[i];
                        if (scheduleid[i] != 0)
                        {
                            obj_class.updateClassSchedule(classid[i], scheduleid[i], status[i]);
                        }
                    }
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Addclasssettings"));
                    string Message = eAcademy.Models.ResourceCache.Localize("SettingsSavedSuccessfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t }, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }


        //public JsonResult DeletePeriod(int periodid)
        //{
        //    var result = obj_periodSettings.checkperiodusingdayorder(periodid);
        //    if (result != null)
        //    {
        //        string roomallotment = ResourceCache.Localize("This_room_allotted_student_or_staff_Can't_delete_this_room");
        //        return Json(new { Errormessage = roomallotment, JsonRequestBehavior.AllowGet });
        //    }
        //    else
        //    {
        //        Hostel_room.deleteroom(drid);
        //        string DeleteSucess = ResourceCache.Localize("Room_Deleted_sucessfully ");
        //        return Json(new { Message = DeleteSucess, JsonRequestBehavior.AllowGet });
        //    }
        //} 
    }
}