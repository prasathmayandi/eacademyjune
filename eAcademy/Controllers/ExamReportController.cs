﻿using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class ExamReportController : Controller
    {
        //
        // GET: /ExamReport/
        public static IList<ExamTable> ExamTimeTables = new List<ExamTable>();
       
       
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            ExamServices Es = new ExamServices();
            EmployeeTypeServices Ets = new EmployeeTypeServices();
            MonthsServices ms = new MonthsServices();
            ViewBag.allAcademicYears = As.ShowAcademicYears();
            ViewBag.allClasses = Cs.ShowClasses();
            ViewBag.allExams = Es.ShowExams();
            List<SelectListItem> Sort = new List<SelectListItem>();
            Sort.Add(new SelectListItem { Text = "Select Status", Value = "" });
            Sort.Add(new SelectListItem { Text = "Both", Value = "All" });
            Sort.Add(new SelectListItem { Text = "Pass", Value = "Pass" });
            Sort.Add(new SelectListItem { Text = "Fail", Value = "fail" });
            ViewBag.SortList = Sort;
            ViewBag.academicyear = As.GetAcademicYear();
            ViewBag.Classes = Cs.GetClass();
            ViewBag.emptype = Ets.GetEmployeeType();
            ViewBag.reporttype = getrtype();
            ViewBag.month = ms.GetMonths();
            ViewBag.FeePaticular = getFeeParticulars();
            ViewBag.exam = Es.GetExamName();
            ViewBag.date = DateTimeByZone.getCurrentDateTime();
        }
        public IEnumerable getrtype()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select", Value = "" });
            li.Add(new SelectListItem { Text = "Daily", Value = "1" });
            li.Add(new SelectListItem { Text = "From/To", Value = "2" });
            li.Add(new SelectListItem { Text = "Monthly", Value = "3" });
            return li;
        }
        public IEnumerable getFeeParticulars()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select", Value = "" });
            li.Add(new SelectListItem { Text = "Paid", Value = "1" });
            li.Add(new SelectListItem { Text = "Due", Value = "2" });
            return li;
        }
        public ActionResult ExamTimeTable()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ExamTimeTable(Mandatory m)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            ExamServices Es = new ExamServices();
            SectionServices Ss = new SectionServices();
            ExamTimetableServices etts = new ExamTimetableServices();
            try
            {
                if (ModelState.IsValid)
                {
                    ArrayList sub = new ArrayList();
                    ArrayList rgid = new ArrayList();
                    ArrayList marks = new ArrayList();
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.exam = Es.GetExamName();
                    ViewBag.Classes = Cs.GetClass();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    var cid = Convert.ToInt32(m.Class);
                    ViewBag.cls = cid;
                    var sid = Convert.ToInt32(m.Rpt_Section);
                    ViewBag.section = Ss.GetSection(cid);
                    ViewBag.sec = sid;
                    var eid = Convert.ToInt32(m.ExamTypeId);
                    ViewBag.e = eid;
                    var ans1 = etts.GetExamTimeTable(yid, cid, sid, eid);
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(cid);
                    ViewBag.ss = Ss.GetParticularSection(sid);
                    ViewBag.ee = Es.GetParticularExamName(eid);
                    TempData["ans"] = ans1;
                    ViewBag.view = ans1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        //jQgridtable for ClassSIncharge
        public JsonResult JQgridExamTimeTable(string sidx, string sord, int page, int rows, int AcademicYear, int Class, int Rpt_Section, int ExamTypeId)//
        {
            IList<ExamTable> ExamTimeTables = new List<ExamTable>();
            ExamTimetableServices etts = new ExamTimetableServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (AcademicYear != null && Class != null && Rpt_Section != null && ExamTypeId != null)
            {
                ExamTimeTables = etts.GetExamTimeTable(AcademicYear, Class, Rpt_Section, ExamTypeId);
            }
            int totalRecords = ExamTimeTables.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    ExamTimeTables = ExamTimeTables.OrderByDescending(s => s.ExamDates).ToList();
                    ExamTimeTables = ExamTimeTables.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ExamTimeTables = ExamTimeTables.OrderBy(s => s.ExamDates).ToList();
                    ExamTimeTables = ExamTimeTables.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = ExamTimeTables;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = ExamTimeTables
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExamTimeTableReport_ExportToExcel()
        {
            try
            {
                List<ExamTable> ExamTimeTables = (List<ExamTable>)Session["JQGridList"];
                var list = ExamTimeTables.Select(o => new { ExamDates = o.ExamDates, SubjectName = o.SubjectName, StartTime = o.StartTime, EndTime = o.EndTime }).ToList();//, subjectid = o.subjectid
                string fileName = eAcademy.Models.ResourceCache.Localize("ExamTimetableReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("ExamTimeTable");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ExamTimeTableReport_ExportToPdf()
        {
            try
            {
                List<ExamTable> ExamTimeTables = (List<ExamTable>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ExamTimetableReport");
                GeneratePDF.ExportPDF_Portrait(ExamTimeTables, new string[] { "ExamDates", "SubjectName", "StartTime", "EndTime" }, xfilePath, fileName);//, "subjectid"
                return File(xfilePath, "application/pdf", "ExamTimetableReport.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ExamAttendance()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ExamAttendance(Mandatory m)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            ExamServices Es = new ExamServices();
            SectionServices Ss = new SectionServices();
            AssignSubjectToSectionServices astss = new AssignSubjectToSectionServices();
            ExamAttendanceServices eas = new ExamAttendanceServices();
            AssignClassToStudentServices actss = new AssignClassToStudentServices();
            SubjectServices SS = new SubjectServices();
            try
            {
                if (ModelState.IsValid)
                {
                    ArrayList sub = new ArrayList();
                    ArrayList rgid = new ArrayList();
                    ArrayList marks = new ArrayList();
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.exam = Es.GetExamName();
                    ViewBag.Classes = Cs.GetClass();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    var cid = Convert.ToInt32(m.Class);
                    ViewBag.cls = cid;
                    var sid = Convert.ToInt32(m.Rpt_Section);
                    ViewBag.section = Ss.GetSection(cid);
                    ViewBag.sec = sid;
                    var eid = Convert.ToInt32(m.ExamTypeId);
                    ViewBag.e = eid;
                    var subid = Convert.ToInt32(m.Examsubject);
                    ViewBag.subject = astss.GetSubject(yid, cid, sid);
                    ViewBag.s = subid;
                    var ans1 = eas.GetExamAttendance(yid, cid, sid, eid, subid);
                    var p = eas.GetExamPresent(yid, cid, sid, eid, subid);
                    ViewBag.NOP = p.Count;
                    var a1 = eas.GetExamAbsent(yid, cid, sid, eid, subid);
                    ViewBag.NOA = a1.Count;
                    var tt = actss.GetClassStudent(yid, cid, sid);
                    ViewBag.total = tt.Count;
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(cid);
                    ViewBag.ss = Ss.GetParticularSection(sid);
                    ViewBag.ee = Es.GetParticularExamName(eid);
                    ViewBag.sub = SS.GetParticularSubject(subid);
                    TempData["ans"] = ans1;
                    ViewBag.view = ans1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        public JsonResult JQgridExamAttendance(string sidx, string sord, int page, int rows, int AcademicYear, int Class, int Rpt_Section, int ExamTypeId, int Examsubject)//
        {
           IList<ExamTable> ExamAttendances = new List<ExamTable>();
            ExamAttendanceServices eas = new ExamAttendanceServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (AcademicYear != null && Class != null && Rpt_Section != null && ExamTypeId != null && Examsubject != null)
            {
                ExamAttendances = eas.GetExamAttendance(AcademicYear, Class, Rpt_Section, ExamTypeId, Examsubject);
            }
            int totalRecords = ExamAttendances.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    ExamAttendances = ExamAttendances.OrderByDescending(s => s.ExamDates).ToList();
                    ExamAttendances = ExamAttendances.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ExamAttendances = ExamAttendances.OrderBy(s => s.ExamDates).ToList();
                    ExamAttendances = ExamAttendances.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = ExamAttendances;
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = ExamAttendances
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExamAttendanceReport_ExportToExcel()
        {
            try
            {
                List<ExamTable> ExamAttendances = (List<ExamTable>)Session["JQGridList"];
                var list = ExamAttendances.Select(o => new { ExamType = o.ExamType, ExamDates = o.ExamDates, SubjectName = o.SubjectName, RollNumber = o.RollNumber, studentname = o.studentname, AttendanceStatus = o.AttendanceStatus }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("ExamAttendanceReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("ExamAttendance");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ExamAttendanceReport_ExportToPdf()
        {
            try
            {
                List<ExamTable> ExamAttendances = (List<ExamTable>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ExamAttendanceReport");
                GeneratePDF.ExportPDF_Portrait(ExamAttendances, new string[] { "ExamType", "ExamDates", "SubjectName", "RollNumber", "studentname", "AttendanceStatus" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "ExamAttendanceReport.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public JsonResult GetSelectedClassSection(int passClassId)
        {
            SectionServices secc = new SectionServices();
            var List = secc.ShowSection(passClassId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExamResult()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ExamResult(R_ExamResult r)
        {
            MarkTotalServices mts = new MarkTotalServices();
            try
            {
                if (ModelState.IsValid)
                {
                    SectionServices secc = new SectionServices();
                    int secId = Convert.ToInt32(r.R_ExamResult_Section);
                    if (r.sort == "Pass")
                    {
                        var list = mts.GetPassStudentList(r.R_ExamResult_allAcademicYears, r.R_ExamResult_allClass, secId, r.R_ExamResult_allExams);
                        ViewBag.count = list;
                        int CId = r.R_ExamResult_allClass.Value;
                        ViewBag.AllSection = secc.ShowSection(CId);
                        ViewBag.sec = secId;
                        return View(list);
                    }
                    else if (r.sort == "fail")
                    {
                        var list = mts.GetFailStudentList(r.R_ExamResult_allAcademicYears, r.R_ExamResult_allClass, secId, r.R_ExamResult_allExams);
                        ViewBag.count = list;
                        int CId = r.R_ExamResult_allClass.Value;
                        ViewBag.AllSection = secc.ShowSection(CId);
                        ViewBag.sec = secId;
                        return View(list);
                    }
                    else
                    {
                        var list = mts.GetBothStudentList(r.R_ExamResult_allAcademicYears, r.R_ExamResult_allClass, secId, r.R_ExamResult_allExams);
                        ViewBag.count = list;
                        int CId = r.R_ExamResult_allClass.Value;
                        ViewBag.AllSection = secc.ShowSection(CId);
                        ViewBag.sec = secId;
                        return View(list);
                    }
                }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult JQgridExamResult(string sidx, string sord, int page, int rows, int R_ExamResult_allAcademicYears, int R_ExamResult_allClass, int R_ExamResult_Section, int R_ExamResult_allExams, string sort)//
        {
            MarkTotalServices mts = new MarkTotalServices();
         IList<R_ExamResult> ExamResults = new List<R_ExamResult>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (R_ExamResult_allAcademicYears != null && R_ExamResult_allClass != null && R_ExamResult_Section != null && R_ExamResult_allExams != null && sort == "Pass")
            {
                ExamResults = mts.GetPassStudentList(R_ExamResult_allAcademicYears, R_ExamResult_allClass, Convert.ToInt16(R_ExamResult_Section), R_ExamResult_allExams);
            }
            if (R_ExamResult_allAcademicYears != null && R_ExamResult_allClass != null && R_ExamResult_Section != null && R_ExamResult_allExams != null && sort == "fail")
            {
                ExamResults = mts.GetFailStudentList(R_ExamResult_allAcademicYears, R_ExamResult_allClass, Convert.ToInt16(R_ExamResult_Section), R_ExamResult_allExams);
            }
            if (R_ExamResult_allAcademicYears != null && R_ExamResult_allClass != null && R_ExamResult_Section != null && R_ExamResult_allExams != null && sort == "All")
            {
                ExamResults = mts.GetBothStudentList(R_ExamResult_allAcademicYears, R_ExamResult_allClass, Convert.ToInt16(R_ExamResult_Section), R_ExamResult_allExams);
            }
            int totalRecords = ExamResults.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    ExamResults = ExamResults.OrderByDescending(s => s.RollNumber).ToList();
                    ExamResults = ExamResults.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ExamResults = ExamResults.OrderBy(s => s.RollNumber).ToList();
                    ExamResults = ExamResults.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = ExamResults;
           
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = ExamResults
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExamResultReport_ExportToExcel()
        {
            try
            {
                List<R_ExamResult> ExamResults = (List<R_ExamResult>)Session["JQGridList"];
                var list = ExamResults.Select(o => new { StudentId = o.StudentId, RollNumber = o.RollNumber, StudentName = o.StudentName, Total = o.Total, Result = o.Result, Rank = o.Rank, Comment = o.Comment }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("ExamResultReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("ExamResult");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ExamResultReport_ExportToPdf()
        {
            try
            {
                List<R_ExamResult> ExamResults = (List<R_ExamResult>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ExamResultReport");
                GeneratePDF.ExportPDF_Portrait(ExamResults, new string[] { "StudentId", "RollNumber", "Name", "Total", "Result", "Rank", "Comment" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "ExamResultReport.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        //Shobana
        public ActionResult GradeResult()
        {
            return View();
        }
        StudentServices obj_student = new StudentServices();
        ExamServices obj_sub = new ExamServices();

        public JsonResult SubjectList()
        {
            try
            {
                var ans = obj_sub.getSubjectsAsTableColumns();
                if (ans.Count == 0)
                {
                    string ErrorMessage = ResourceCache.Localize("Subject_not_created");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GradeResultList(int AcademicYears, int Class, int Section, int Exam, int[] Subject)    
        {
            AcademicyearServices obj_academicYear = new AcademicyearServices();
            try
            {
                var ans = obj_sub.getStudentwithResult(AcademicYears, Class, Section);
                var ans_count = ans.Count;
                var result = obj_sub.getGradeResultbySection(AcademicYears, Class, Section, Exam, Subject);
                var count = result.Count;
                return Json(new { ans = ans, result = result, count = count, ans_count = ans_count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }


    }
}
