﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class ModulesInfoController : Controller
    {
        //
        // GET: /Modules/

        SectionServices sec = new SectionServices();
        AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
        AssignClassToStudentServices stu = new AssignClassToStudentServices();
        SubjectnotesServices notes = new SubjectnotesServices();
        HomeworkServices HW = new HomeworkServices();
        AssignmentServices assign = new AssignmentServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        AcademicyearServices acservice = new AcademicyearServices();
        public int FacultyId;

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            FacultyId = Convert.ToInt32(Session["empRegId"]);
            AcademicyearServices AcYear = new AcademicyearServices();
            ClassServices clas = new ClassServices();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.allClasses = clas.ShowClasses(FacultyId);
            ViewBag.CurrentDateAcYear = AcYear.CurrentDateAcademicYear();
        }


        public JsonResult showSection(int passClassId)
        {
            try
            {
                var SecList = sec.getSection(passClassId, FacultyId);
                return Json(SecList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult showSubjects(int passSecId)
        {
            try
            {
                var SubjectList = facultySub.ShowSubjects(passSecId, FacultyId);
                return Json(SubjectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult showSubjects1(int passYearId, string passClassSec)
        {
            try
            {
                string clasSec = passClassSec;
                string[] words = clasSec.Split(' ');
                int classId = Convert.ToInt32(words[0]);
                int secId = Convert.ToInt32(words[1]);
                var SubjectList = facultySub.ShowSubjects(passYearId, classId, secId, FacultyId);
                Session["CurrentClassIdSecId"] = classId; 
                return Json(SubjectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult showStudents(int passYearId, int passClassId, int passSecId)
        {
            try
            {
                var StudentList = stu.getSecStudents(passSecId);
                return Json(StudentList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getClassWithSection(int passYearId)
        {
            var list = facultySub.getClassWithSection(FacultyId, passYearId);
            Session["CurrentAcYearId"] = passYearId;
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        // Student List
        public ActionResult StudentList()
        {
            return View();
        }

        

        public JsonResult StudentsListResults(string sidx, string sord, int page, int rows, int? MI_allAcademicYears, string MI_ClassWithSection)
        {
           IList<MI_Students> StudentsList = new List<MI_Students>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            if (MI_allAcademicYears != 0 && MI_ClassWithSection != null)
            {
                string clasSec = MI_ClassWithSection;
                string[] words = clasSec.Split(' ');
                int classId = Convert.ToInt32(words[0]);
                int secId = Convert.ToInt32(words[1]);
                StudentsList = stu.getFacultyStudents(MI_allAcademicYears, classId, secId);
            }

            int totalRecords = StudentsList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    StudentsList = StudentsList.OrderByDescending(s => s.RollNumber).ToList();
                    StudentsList = StudentsList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    StudentsList = StudentsList.OrderBy(s => s.RollNumber).ToList();
                    StudentsList = StudentsList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = StudentsList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = StudentsList
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StudentList_ExportToExcel()
        {
            try
            {
                List<MI_Students> StudentsList = (List<MI_Students>)Session["JQGridList"];
                var list = StudentsList.Select(o => new { RollNumber = o.RollNumber, Name = o.StudentName, Gender = o.Gender, ContactPerson = o.EmergencyContactPerson, ContactNumber = o.EmergencyContactNumber }).ToList();
                string fileName = ResourceCache.Localize("StudentListExcelFileName");
                GenerateExcel.ExportExcel(list, fileName);
                return View("StudentList");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult StudentList_ExportToPdf()
        {
            try
            {
                List<MI_Students> StudentsList = (List<MI_Students>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("StudentList");
                GeneratePDF.ExportPDF_Portrait(StudentsList, new string[] { "RollNumber", "StudentName", "Gender", "EmergencyContactPerson", "EmergencyContactNumber" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("StudentList.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public string fileName1;
        public ActionResult SubjectNotes()
        {
            return View();
        }

       

        public JsonResult NotesListResults(string sidx, string sord, int page, int rows, int? MI_allAcademicYears, string MI_List_ClassWithSection, int? MI_NotesList_allSubjects, DateTime? MI_notes_List_txt_fromDate, DateTime? MI_notes_List_txt_toDate)
        {
              IList<SubjectNotesList> NotesList1 = new List<SubjectNotesList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            if (MI_allAcademicYears != 0 && MI_List_ClassWithSection != null && MI_NotesList_allSubjects != 0 && MI_notes_List_txt_fromDate != null && MI_notes_List_txt_toDate != null)
            {
                string clasSec = MI_List_ClassWithSection;
                string[] words = clasSec.Split(' ');
                int classId = Convert.ToInt32(words[0]);
                int secId = Convert.ToInt32(words[1]);
                NotesList1 = notes.getSubjectNotesList(MI_allAcademicYears, classId, secId, MI_NotesList_allSubjects, MI_notes_List_txt_fromDate, MI_notes_List_txt_toDate, FacultyId);
            }
            int totalRecords = NotesList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    NotesList1 = NotesList1.OrderByDescending(s => s.Topic).ToList();
                    NotesList1 = NotesList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    NotesList1 = NotesList1.OrderBy(s => s.Topic).ToList();
                    NotesList1 = NotesList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = NotesList1;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = NotesList1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AddSubjectNotes()
        {
            try
            {
                var year = Request["year"];
                var clasSec = Request["classSection"];
                var subject = Request["subject"];
              
                string topic = Request["topic"];
                string desc = Request["desc"];

                string Email = Request["email"];
                string Sms = Request["sms"];
                var Cyear = acservice.getCurrentAcademicYearId(DateTimeByZone.getCurrentDate());
                if (Cyear != null)
                {
                    Session["CurrentAcYearId"] = Cyear.AcademicYearId;
                }
                else
                {
                    Session["CurrentAcYearId"] = "";
                }
                int CurrentYearId;
                if (Session["CurrentAcYearId"] != "")
                {
                    CurrentYearId = Convert.ToInt16(Session["CurrentAcYearId"]);
                }
                else
                {
                    CurrentYearId = 0;
                }

                
                if (year != null && clasSec != null && subject != null && topic != null && desc != null)
                {
                    int yearId = Convert.ToInt32(year);
                    if (CurrentYearId == yearId)
                    {

                        string[] words = clasSec.Split(' ');
                        int classId = Convert.ToInt32(words[0]);
                        int secId = Convert.ToInt32(words[1]);
                        int subId = Convert.ToInt32(subject);
                        

                        string NFileName = null;

                        if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                        {
                            var file = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                            string filenamewithoutextention;
                            Random random = new Random();
                            string fname = random.Next(00000, 99999).ToString();
                            string extention = System.IO.Path.GetExtension(file.FileName);

                            var fileSize = file.ContentLength;
                            fileSize = file.ContentLength / 1048576;

                            if (extention == ".pdf" || extention == ".docx" || extention == ".doc" || extention == ".txt")
                            {
                                if (file != null && file.ContentLength > 0 && fileSize <= 3)
                                {
                                    filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file.FileName);
                                    NFileName = "N" + fname + extention;
                                    string path = System.IO.Path.Combine(Server.MapPath("~/Documents/SubjectNotes"), NFileName);
                                    file.SaveAs(path);
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = "File size exceeds";
                                }
                            }
                            else
                            {
                                string ErrorMessage = ResourceCache.Localize("FileFormatDoesNotSupport");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        notes.addSubjectNotes(yearId, classId, secId, subId, topic, desc, NFileName, FacultyId);
                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddSubjectNotes"));
                        string Message = ResourceCache.Localize("NotesSavedSuccessfully");

                        // Email & SMS Notification
                        PrimaryUserRegisterServices primaryUser = new PrimaryUserRegisterServices();
                        var studentEmailList = primaryUser.getPrimaryUserMailList(yearId, classId, secId);

                        // get academicYear, class, section
                        AcademicyearServices AcYear = new AcademicyearServices();
                        ClassServices clas = new ClassServices();
                        SectionServices sec = new SectionServices();
                        SubjectServices subSer = new SubjectServices();

                        var getYear = AcYear.AcademicYear(yearId).AcademicYear1;
                        var getClass = clas.getClassNameByClassID(classId).ClassType;
                        var getSection = sec.getSectionName(classId, secId).SectionName;
                        var getSubject = subSer.getSubjectName(subId).SubjectName;

                        if (Email != null)
                        {
                            int count = studentEmailList.Count;
                            string[] email = new string[count];
                            for (int i = 0; i < count; i++)
                            {
                                email[i] = studentEmailList[i].PrimaryUserEmail;
                            }
                            string Tomail = string.Join(",", email);

                            StreamReader reader = new StreamReader(Server.MapPath("~/Template/Homework.html"));
                            string readFile = reader.ReadToEnd();
                            string myString = "";
                            myString = readFile;

                            myString = myString.Replace("$$year$$", getYear);
                            myString = myString.Replace("$$class$$", getClass);
                            myString = myString.Replace("$$section$$", getSection);

                            myString = myString.Replace("$$subject$$", getSubject);
                            myString = myString.Replace("$$title$$", topic.ToString());
                            myString = myString.Replace("$$description$$", desc.ToString());

                            string sub = ResourceCache.Localize("SubjectNotes");

                            string path = "";
                            if (NFileName != null)
                            {
                                path = System.IO.Path.Combine(Server.MapPath("~/Documents/SubjectNotes"), NFileName);
                            }
                            string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                            string PlaceName = "ModulesInfo-AddSubjectNotes-Subject notes ";
                            string UserName = "Employee";
                            string UserId = Convert.ToString(Session["username"]);

                            Mail.SendMultipleMail(ResourceCache.Localize("AceClassRoomSubjectNotes"), supportEmail, Tomail, sub, myString.ToString(), true, path, PlaceName, UserName, UserId);  
                        }
                        if (Sms != null)
                        {
                            int count = studentEmailList.Count;
                            string[] mobileNumber = new string[count];
                            for (int i = 0; i < count; i++)
                            {
                                mobileNumber[i] = Convert.ToString(studentEmailList[i].PrimaryUserMobileNum);
                            }
                            string ToNumber = string.Join(",", mobileNumber);
                            string sub = ResourceCache.Localize("SubjectNotes");
                            string Description = desc;
                            SMS.Main(ResourceCache.Localize("AceClassRoomSubjectNotes"), ToNumber, sub, Description);
                        }
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = ResourceCache.Localize("PleaseSelectCurrentAcademicYear");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("PleaseFillAllMandatoryFields");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult EditSubjectNotes(string passId)
        {
            try
            {
                int eid = Convert.ToInt32(QSCrypt.Decrypt(passId));
                var getRow = notes.getStudentNotesFile(eid);
                var date = getRow.DateOfNotes.Value.ToString("dd/mm/yyyy");
                var topic = getRow.Topic;
                var desc = getRow.Descriptions;
                var status = getRow.NotesStatus;
                var ExistFile = getRow.NotesFileName;
                return Json(new { date, topic, desc, status, ExistFile }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult EditSubjectNotes1()
        {
            try
            {
                string NFileName = null;
                var date = Request["dat"];
                string topic = Request["topic"];
                string desc = Request["desc"];
                var Status = Request["status"];
                var NotesId = Request["notesId"];

                if (date != null && topic != null && desc != null && Status != null && NotesId != null)
                {
                    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                    {
                        var file = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                        string filenamewithoutextention;
                        Random random = new Random();
                        string fname = random.Next(00000, 99999).ToString();
                        string extention = System.IO.Path.GetExtension(file.FileName);

                        var fileSize = file.ContentLength;
                        fileSize = file.ContentLength / 1048576;

                        if (extention == ".pdf" || extention == ".docx" || extention == ".doc" || extention == ".txt")
                        {
                            if (file != null && file.ContentLength > 0 && fileSize <= 3)
                            {
                                filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file.FileName);
                                NFileName = "N" + fname + extention;
                                string path = System.IO.Path.Combine(Server.MapPath("~/Documents/SubjectNotes"), NFileName);
                                file.SaveAs(path);
                            }
                            else
                            {
                                ViewBag.ErrorMessage = "File size exceeds";
                            }
                        }
                        else
                        {
                            string ErrorMessage = ResourceCache.Localize("FileFormatDoesNotSupport");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    int eid = Convert.ToInt32(QSCrypt.Decrypt(NotesId));
                    DateTime notesDate = Convert.ToDateTime(date);
                    bool status = Convert.ToBoolean(Status);

                    if (NFileName != null)
                    {
                        var getOldFile = notes.getStudentNotesFile(eid);
                        var OldFileName = getOldFile.NotesFileName;
                        string path1 = Server.MapPath("~/Documents/SubjectNotes/" + OldFileName);
                        FileInfo file = new FileInfo(path1);
                        file.Delete();
                        notes.updateSubjectNotes(notesDate, topic, desc, status, NFileName, eid);
                    }
                    else
                    {
                        notes.updateSubjectNotes(notesDate, topic, desc, status, eid);
                    }
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdateSubjectNotes"));
                    string Message = ResourceCache.Localize("NotesUpdatedSuccessfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("PleaseFillAllMandatoryFields");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteSubjectNotes(string passId)
        {
            try
            {
                int Did = Convert.ToInt32(QSCrypt.Decrypt(passId));
                notes.delSubjectNotes(Did);
                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("DeleteSubjectNotes"));
                string Message = ResourceCache.Localize("NotesDetailsDeletedSuccessfully");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public void OpenSubjectNotesFile(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                var getNotesFile = notes.getStudentNotesFile(fid).NotesFileName;
                string file = getNotesFile;
                if (file != null)
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Documents/SubjectNotes/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {
                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + file);
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }

        public ActionResult Note_ExportToExcel()
        {
            try
            {
                List<SubjectNotesList> NotesList1 = (List<SubjectNotesList>)Session["JQGridList"];
                var list = NotesList1.Select(o => new { DateOf_Notes = o.DateOf_Notes, Topic = o.Topic, Descriptions = o.Descriptions }).ToList();
                string fileName = ResourceCache.Localize("SubjectNotesListExcelFileName");
                GenerateExcel.ExportExcel(list, fileName);
                return View("SubjectNotes");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Note_ExportToPdf()
        {
            try
            {
                List<SubjectNotesList> NotesList1 = (List<SubjectNotesList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("Subject_Notes_List");
                GeneratePDF.ExportPDF_Portrait(NotesList1, new string[] { "DateOf_Notes", "Topic", "Descriptions"}, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("SubjectNotesList.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }



        // E-Learning
        public ActionResult ELearning()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddELearn()
        {
            try
            {

                string FileName = null;

                var year = Request["year"];
                string clasSec = Request["classSection"];
                string title = Request["title"];
                string desc = Request["desc"];
                DateTime submissionDate = Convert.ToDateTime(Request["dat"]);

                string Email = Request["email"];
                string Sms = Request["sms"];
                var Cyear = acservice.getCurrentAcademicYearId(DateTimeByZone.getCurrentDate());
                if (Cyear != null)
                {
                    Session["CurrentAcYearId"] = Cyear.AcademicYearId;
                }
                else
                {
                    Session["CurrentAcYearId"] = "";
                }
                int CurrentYearId;
                if (Session["CurrentAcYearId"] != "")
                {
                    CurrentYearId = Convert.ToInt16(Session["CurrentAcYearId"]);
                }
                else
                {
                    CurrentYearId = 0;
                }
                if (year != null && clasSec != null && title != null && desc != null && submissionDate != null)
                {
                    int yearId = Convert.ToInt32(year);
                    if (CurrentYearId == yearId)
                    {
                    string[] words = clasSec.Split(' ');
                    int classId = Convert.ToInt32(words[0]);
                    int secId = Convert.ToInt32(words[1]);
                    

                    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                    {
                        var file1 = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                        string filenamewithoutextention;
                        Random random = new Random();
                        string fname = random.Next(00000, 99999).ToString();
                        string extention = System.IO.Path.GetExtension(file1.FileName);

                        var fileSize = file1.ContentLength;
                        fileSize = file1.ContentLength / 1048576;

                        if (extention == ".pdf" || extention == ".docx" || extention == ".doc")
                        {
                            if (file1 != null && file1.ContentLength > 0 && fileSize <= 3)
                            {
                                filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file1.FileName);
                                FileName = "ELearn" + fname + extention;
                                string path = System.IO.Path.Combine(Server.MapPath("~/Documents/ELearning"), FileName);
                                file1.SaveAs(path);
                            }
                            else
                            {
                                string ErrorMessage = ResourceCache.Localize("FileSizeExceeds");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            string ErrorMessage = ResourceCache.Localize("FileFormatDoesNotSupport");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    ELearningServices ELearn = new ELearningServices();
                    ELearn.addELearn(yearId, classId, secId, submissionDate, title, desc, FileName, FacultyId);
                    string Message = ResourceCache.Localize("ELearningSavedSuccessfully");
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddELearning"));
                    PrimaryUserRegisterServices primaryUser = new PrimaryUserRegisterServices();
                    var studentEmailList = primaryUser.getPrimaryUserMailList(yearId, classId, secId);

                    // get academicYear, class, section
                    AcademicyearServices AcYear = new AcademicyearServices();
                    ClassServices clas = new ClassServices();
                    SectionServices sec = new SectionServices();
                    SubjectServices subSer = new SubjectServices();
                    TechEmployeeServices techEmpSer = new TechEmployeeServices();

                    var getYear = AcYear.AcademicYear(yearId).AcademicYear1;
                    var getClass = clas.getClassNameByClassID(classId).ClassType;
                    var getSection = sec.getSectionName(classId, secId).SectionName;
                    var getFacultyNmae = techEmpSer.EmployeeListByRegId(FacultyId);
                    string EmployeeName = getFacultyNmae.EmployeeName + " " + getFacultyNmae.LastName;
                    if (Email != null)
                    {
                        int count = studentEmailList.Count;
                        string[] email = new string[count];
                        for (int i = 0; i < count; i++)
                        {
                            email[i] = studentEmailList[i].PrimaryUserEmail;
                        }
                        string Tomail = string.Join(",", email);

                        StreamReader reader = new StreamReader(Server.MapPath("~/Template/Homework.html"));
                        string readFile = reader.ReadToEnd();
                        string myString = "";
                        myString = readFile;

                        myString = myString.Replace("$$year$$", getYear);
                        myString = myString.Replace("$$class$$", getClass);
                        myString = myString.Replace("$$section$$", getSection);

                        myString = myString.Replace("$$subject$$", ResourceCache.Localize("ELearningTaskFrom") + EmployeeName);
                        myString = myString.Replace("$$title$$", title.ToString());
                        myString = myString.Replace("$$description$$", desc.ToString());

                        string sub = "E-Learning on" + submissionDate.ToString("dd-MM-yyyy");

                        string path = "";
                        if (FileName != null)
                        {
                            path = System.IO.Path.Combine(Server.MapPath("~/Documents/ELearning"), FileName);
                        }
                        string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                        string PlaceName = "ModulesInfo-AddELearn-Online test";
                        string UserName = "Employee";
                        string UserId = Convert.ToString(Session["username"]);
                        Mail.SendMultipleMail(ResourceCache.Localize("AceClassRoomELearning"), supportEmail, Tomail, sub, myString.ToString(), true, path, PlaceName, UserName, UserId);  
                    }
                    if (Sms != null)
                    {
                        int count = studentEmailList.Count;
                        string[] mobileNumber = new string[count];
                        for (int i = 0; i < count; i++)
                        {
                            mobileNumber[i] = Convert.ToString(studentEmailList[i].PrimaryUserMobileNum);
                        }
                        string ToNumber = string.Join(",", mobileNumber);
                        string sub = ResourceCache.Localize("ELearningOn") + submissionDate.ToString("dd-MM-yyyy");
                        string Description = desc;
                        SMS.Main(ResourceCache.Localize("AceClassRoomELearning"), ToNumber, sub, Description);
                    }
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = ResourceCache.Localize("PleaseSelectCurrentAcademicYear");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("PleaseFillAllMandatoryFields");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }


        private IList<ELearningList> ELearningList = new List<ELearningList>();

        public JsonResult ELearningListResults(string sidx, string sord, int page, int rows, int? ELearn_List_allAcademicYears, string ELearn_List_ClassWithSection, DateTime? ELearn_List_txt_fromDate, DateTime? ELearn_List_txt_toDate)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            if (ELearn_List_allAcademicYears != 0 && ELearn_List_ClassWithSection != null && ELearn_List_txt_fromDate != null && ELearn_List_txt_toDate != null)
            {
                string clasSec = ELearn_List_ClassWithSection;
                string[] words = clasSec.Split(' ');
                int classId = Convert.ToInt32(words[0]);
                int secId = Convert.ToInt32(words[1]);
                ELearningServices ELearn = new ELearningServices();
                ELearningList = ELearn.getELearningList(ELearn_List_allAcademicYears, classId, secId, ELearn_List_txt_fromDate, ELearn_List_txt_toDate, FacultyId);
            }

            int totalRecords = ELearningList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    ELearningList = ELearningList.OrderByDescending(s => s.Title).ToList();
                    ELearningList = ELearningList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ELearningList = ELearningList.OrderBy(s => s.Title).ToList();
                    ELearningList = ELearningList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = ELearningList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = ELearningList
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ELearn_ExportToExcel()
        {
            try
            {
                List<ELearningList> ELearningList = (List<ELearningList>)Session["JQGridList"];
                var list = ELearningList.Select(o => new { PostedtDate = o.PostedtDate, Title = o.Title, Description = o.Description, LastDate = o.LastDate }).ToList(); //, Status = o.Status 
                string fileName = ResourceCache.Localize("ELearningExcelFileName");
                GenerateExcel.ExportExcel(list, fileName);
                return View("ELearning");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult ELearn_ExportToPdf()
        {
            try
            {
                List<ELearningList> ELearningList = (List<ELearningList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("ELearningList");
                GeneratePDF.ExportPDF_Portrait(ELearningList, new string[] { "PostedtDate", "Title", "Description", "LastDate" }, xfilePath, heading);  //, "Status"
                return File(xfilePath, "application/pdf", ResourceCache.Localize("ELearningList.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public void OpenELearnFile(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                ELearningServices ELearn = new ELearningServices();
                var getFile = ELearn.getELearnFile(fid).FileName;
                string file = getFile;
                if (file != null)
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Documents/ELearning/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {
                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + file);
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }


        public ActionResult ELearnAnswer(string id)
        {
            int ELearnId = Convert.ToInt32(QSCrypt.Decrypt(id));
            ELearningServices ELearn = new ELearningServices();
            var classDetails = ELearn.getELearnFile(ELearnId);
            int YearId = classDetails.AcademicYearId.Value;
            int classId = classDetails.ClassId.Value;
            int secId = classDetails.SectionId.Value;
            ELearningReplyFromStudentServices ElearnReplySer = new ELearningReplyFromStudentServices();
            var studentList = ElearnReplySer.getStudentList(ELearnId, YearId, classId, secId);
            if (studentList.Count == 0)
            {
                ViewBag.Student = "Empty";
            }
            return View(studentList);
        }


        public void OpenELearnAnswerFile(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                ELearningReplyFromStudentServices ElearnReplySer = new ELearningReplyFromStudentServices();
                var File = ElearnReplySer.getELearnAnswerFile(fid).AnswerFileName;
                string file = File;
                if (file != null)
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Documents/ELearnReply/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {
                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + file);
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }


        public ActionResult SaveELearnRemarks(ELearnReplyList m)
        {
            var TotalCount = m.TotalCount;
            int size = TotalCount;
            string[] Id = new string[size];
            string[] ELearn_comment = new string[size];


            ELearningReplyFromStudentServices ElearnReplySer = new ELearningReplyFromStudentServices();

            for (var i = 0; i < TotalCount; i++)
            {
                Id[i] = QSCrypt.Decrypt(m.Id[i]);
                int ELearnAnswerId = Convert.ToInt32(Id[i]);
                ELearn_comment[i] = m.ELearn_comment[i];
                ElearnReplySer.UpdateRemark(ELearnAnswerId, ELearn_comment[i], FacultyId);
            }
            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddElearningRemarks"));
            string Message = ResourceCache.Localize("RemarksSavedSuccessfully");
            return Json(new { Message }, JsonRequestBehavior.AllowGet);
        }

        // Homework
        public ActionResult Homework()
        {
            return View();
        }

        private IList<HomeworkList> HomeworkList1 = new List<HomeworkList>();

        public JsonResult HomeworkListResults(string sidx, string sord, int page, int rows, int? MI_HW_List_allAcademicYears, string MI_List_ClassWithSection, int? MI_HW_List_allSubjects, DateTime? MI_HW_List_txt_fromDate, DateTime? MI_HW_List_txt_toDate)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            if (MI_HW_List_allAcademicYears != 0 && MI_List_ClassWithSection != null && MI_HW_List_allSubjects != 0 && MI_HW_List_txt_fromDate != null && MI_HW_List_txt_toDate != null)
            {
                string clasSec = MI_List_ClassWithSection;
                string[] words = clasSec.Split(' ');
                int classId = Convert.ToInt32(words[0]);
                int secId = Convert.ToInt32(words[1]);
                HomeworkList1 = HW.getHomeworkList(MI_HW_List_allAcademicYears, classId, secId, MI_HW_List_allSubjects, MI_HW_List_txt_fromDate, MI_HW_List_txt_toDate, FacultyId);
            }

            int totalRecords = HomeworkList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    HomeworkList1 = HomeworkList1.OrderByDescending(s => s.HomeWork1).ToList();
                    HomeworkList1 = HomeworkList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    HomeworkList1 = HomeworkList1.OrderBy(s => s.HomeWork1).ToList();
                    HomeworkList1 = HomeworkList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = HomeworkList1;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = HomeworkList1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Homework_ExportToExcel()
        {
            try
            {
                List<HomeworkList> HomeworkList1 = (List<HomeworkList>)Session["JQGridList"];
                var list = HomeworkList1.Select(o => new { PostedtDate = o.PostedtDate, HomeWork1 = o.HomeWork1, Descriptions = o.Descriptions, LastDate = o.LastDate }).ToList();  //, Status = o.HomeWorkStatus
                string fileName = ResourceCache.Localize("HomeworkExcelFileName");
                GenerateExcel.ExportExcel(list, fileName);
                return View("Homework");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Homework_ExportToPdf()
        {
            try
            {
                List<HomeworkList> HomeworkList1 = (List<HomeworkList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("HomeworkList");
                GeneratePDF.ExportPDF_Portrait(HomeworkList1, new string[] { "PostedtDate", "HomeWork1", "Descriptions", "LastDate" }, xfilePath, heading);  //, "HomeWorkStatus"
                return File(xfilePath, "application/pdf", ResourceCache.Localize("HomeworkList.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult AddHomework()
        {
            try
            {

                string HWFileName = null;

                var year = Request["year"];
                string clasSec = Request["classSection"];
                var subject = Request["subject"];
                string homework = Request["hw"];
                string desc = Request["desc"];
                DateTime submissionDate = Convert.ToDateTime(Request["dat"]);

                string Email = Request["email"];
                string Sms = Request["sms"];
                var Cyear = acservice.getCurrentAcademicYearId(DateTimeByZone.getCurrentDate());
                if (Cyear != null)
                {
                    Session["CurrentAcYearId"] = Cyear.AcademicYearId;
                }
                else
                {
                    Session["CurrentAcYearId"] = "";
                }
                int CurrentYearId;
                if (Session["CurrentAcYearId"] != "")
                {
                    CurrentYearId = Convert.ToInt16(Session["CurrentAcYearId"]);
                }
                else
                {
                    CurrentYearId = 0;
                }
                

                if (year != null && clasSec != null && subject != null && homework != null && desc != null && submissionDate != null)
                {
                    int yearId = Convert.ToInt32(year);
                    if (CurrentYearId == yearId)
                    {
                        string[] words = clasSec.Split(' ');
                        int classId = Convert.ToInt32(words[0]);
                        int secId = Convert.ToInt32(words[1]);

                        int subId = Convert.ToInt32(subject);

                        if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                        {
                            var file1 = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                            string filenamewithoutextention;
                            Random random = new Random();
                            string fname = random.Next(00000, 99999).ToString();
                            string extention = System.IO.Path.GetExtension(file1.FileName);

                            var fileSize = file1.ContentLength;
                            fileSize = file1.ContentLength / 1048576;

                            if (extention == ".pdf" || extention == ".docx" || extention == ".doc")
                            {
                                if (file1 != null && file1.ContentLength > 0 && fileSize <= 3)
                                {
                                    filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file1.FileName);
                                    HWFileName = "HW" + fname + extention;
                                    string path = System.IO.Path.Combine(Server.MapPath("~/Documents/Homework"), HWFileName);
                                    file1.SaveAs(path);
                                }
                                else
                                {
                                    string ErrorMessage = ResourceCache.Localize("FileSizeExceeds");
                                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                string ErrorMessage = ResourceCache.Localize("FileFormatDoesNotSupport");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        HW.addHomework(yearId, classId, secId, subId, submissionDate, homework, desc, HWFileName, FacultyId);
                        string Message = ResourceCache.Localize("HomeworkSavedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddHomeworkToStudent"));
                        PrimaryUserRegisterServices primaryUser = new PrimaryUserRegisterServices();
                        var studentEmailList = primaryUser.getPrimaryUserMailList(yearId, classId, secId);

                        // get academicYear, class, section
                        AcademicyearServices AcYear = new AcademicyearServices();
                        ClassServices clas = new ClassServices();
                        SectionServices sec = new SectionServices();
                        SubjectServices subSer = new SubjectServices();

                        var getYear = AcYear.AcademicYear(yearId).AcademicYear1;
                        var getClass = clas.getClassNameByClassID(classId).ClassType;
                        var getSection = sec.getSectionName(classId, secId).SectionName;
                        var getSubject = subSer.getSubjectName(subId).SubjectName;

                        if (Email != null)
                        {
                            int count = studentEmailList.Count;
                            string[] email = new string[count];
                            for (int i = 0; i < count; i++)
                            {
                                email[i] = studentEmailList[i].PrimaryUserEmail;
                            }
                            string Tomail = string.Join(",", email);

                            StreamReader reader = new StreamReader(Server.MapPath("~/Template/Homework.html"));
                            string readFile = reader.ReadToEnd();
                            string myString = "";
                            myString = readFile;

                            myString = myString.Replace("$$year$$", getYear);
                            myString = myString.Replace("$$class$$", getClass);
                            myString = myString.Replace("$$section$$", getSection);

                            myString = myString.Replace("$$subject$$", getSubject);
                            myString = myString.Replace("$$title$$", homework.ToString());
                            myString = myString.Replace("$$description$$", desc.ToString());

                            string sub = ResourceCache.Localize("HomeworkOn") + submissionDate.ToString("dd-MM-yyyy");

                            string path = "";
                            if (HWFileName != null)
                            {
                                path = System.IO.Path.Combine(Server.MapPath("~/Documents/Homework"), HWFileName);
                            }
                            string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                            string PlaceName = "ModulesInfo-AddHomework-Homework ";
                            string UserName = "Employee";
                            string UserId = Convert.ToString(Session["username"]);
                            Mail.SendMultipleMail(ResourceCache.Localize("AceClassRoomHomework"), supportEmail, Tomail, sub, myString.ToString(), true, path, PlaceName, UserName, UserId);  
                        }
                        if (Sms != null)
                        {
                            int count = studentEmailList.Count;
                            string[] mobileNumber = new string[count];
                            for (int i = 0; i < count; i++)
                            {
                                mobileNumber[i] = Convert.ToString(studentEmailList[i].PrimaryUserMobileNum);
                            }
                            string ToNumber = string.Join(",", mobileNumber);
                            string sub = ResourceCache.Localize("HomeworkOn") + submissionDate.ToString("dd-MM-yyyy");
                            string Description = desc;
                            SMS.Main(ResourceCache.Localize("AceClassRoomHomework"), ToNumber, sub, Description);
                        }
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = ResourceCache.Localize("PleaseSelectCurrentAcademicYear");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("PleaseFillAllMandatoryFields");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public void OpenHomeworkFile(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                var getNotesFile = HW.getStudentHomeworkFile(fid).HomeWorkFileName;
                string file = getNotesFile;
                if (file != null)
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Documents/Homework/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {
                            
                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename="+file);
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }
        public ActionResult EditHomework(string passId)
        {
            try
            {
                int eid = Convert.ToInt32(QSCrypt.Decrypt(passId));
                var getRow = HW.getStudentHomeworkFile(eid);
                var CompleionDate = getRow.DateToCompleteWork.Value.ToString("dd/mm/yyyy");
                var Homework = getRow.HomeWork1;
                var desc = getRow.Descriptions;
                var status = getRow.HomeWorkStatus;
                var ExistFile = getRow.HomeWorkFileName;
                return Json(new { CompleionDate, Homework, desc, status, ExistFile }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult EditHomework()
        {
            try
            {
                string HWFileName = null;
                string Homework = Request["hw"];
                string desc = Request["desc"];
                string status1 = Request["status"];
                var submissionDate = Request["dat"];
                string HomeworkId = Request["homeworkid"];
                if (Homework != null && desc != null && status1 != null && submissionDate != null && HomeworkId != null)
                {
                    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                    {
                        var file1 = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                        string filenamewithoutextention;
                        Random random = new Random();
                        string fname = random.Next(00000, 99999).ToString();
                        string extention = System.IO.Path.GetExtension(file1.FileName);

                        var fileSize = file1.ContentLength;
                        fileSize = file1.ContentLength / 1048576;

                        if (extention == ".pdf" || extention == ".docx" || extention == ".doc")
                        {
                            if (file1 != null && file1.ContentLength > 0 && fileSize <= 3)
                            {
                                filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file1.FileName);
                                HWFileName = "HW" + fname + extention;
                                string path = System.IO.Path.Combine(Server.MapPath("~/Documents/Homework"), HWFileName);
                                file1.SaveAs(path);
                            }
                            else
                            {
                                string ErrorMessage = ResourceCache.Localize("FileSizeExceeds");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            string ErrorMessage = ResourceCache.Localize("FileFormatDoesNotSupport");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    int eid = Convert.ToInt32(QSCrypt.Decrypt(HomeworkId));
                    DateTime Date = Convert.ToDateTime(submissionDate);
                    bool status = Convert.ToBoolean(status1);
                    if (HWFileName != null)
                    {
                        var getOldFile = HW.getHomework(eid);
                        var OldFileName = getOldFile.HomeWorkFileName;
                        string path1 = Server.MapPath("~/Documents/Homework/" + OldFileName);
                        FileInfo file = new FileInfo(path1);
                        file.Delete();
                        HW.updateHomework(Date, Homework, desc, status, HWFileName, eid);
                    }
                    else
                    {
                        HW.updateHomework(Date, Homework, desc, status, eid);
                    }
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdateHomeworkToStudent"));
                    string Message = ResourceCache.Localize("HomeworkUpdatedSuccessfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("PleaseFillAllMandatoryFields");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteHomework(string passId)
        {
            try
            {
                int Did = Convert.ToInt32(QSCrypt.Decrypt(passId));
                HW.delHomework(Did);
                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("DeleteHomeworkToStudent"));
                string Message = ResourceCache.Localize("Homework_details_deleted_Successfully");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult HomeworkAnswer(string id)
        {
            int HomeworkId = Convert.ToInt32(QSCrypt.Decrypt(id));
            HomeworkServices HomeworkServices = new HomeworkServices();
            var classDetails = HomeworkServices.GetClassDetails(HomeworkId);
            int YearId = classDetails.AcademicYearId.Value;
            int classId = classDetails.ClassId.Value;
            int secId = classDetails.SectionId.Value;
            HomeworkReplyFromStudentServices HomeworkreplySer = new HomeworkReplyFromStudentServices();
            var studentList = HomeworkreplySer.getStudentList(HomeworkId, YearId, classId, secId);
            if (studentList.Count == 0)
            {
                ViewBag.Student = "Empty";
            }
            return View(studentList);
        }


        public void OpenHomeworkAnswerFile(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                HomeworkReplyFromStudentServices HomeworkreplySer = new HomeworkReplyFromStudentServices();
                var getFile = HomeworkreplySer.getHomeworkAnswerFile(fid);
                if(getFile != null)
                {
                    var File = getFile.AnswerFileName;
                    string file = File;
                    if (file != null)
                    {
                        string extention = System.IO.Path.GetExtension(file);
                        string FilePath = Server.MapPath("~/Documents/HomeworkReply/" + file);
                        WebClient User = new WebClient();
                        Byte[] FileBuffer = User.DownloadData(FilePath);
                        if (FileBuffer != null)
                        {
                            if (extention == ".pdf")
                            {
                                Response.ContentType = "application/pdf";
                            }
                            else if (extention == ".doc" || extention == ".docx")
                            {
                                Response.ContentType = "application/vnd.ms-word";
                            }
                            Response.AddHeader("Content-Disposition", "attachment; filename=" + file);
                            Response.BinaryWrite(FileBuffer);
                            Response.Flush();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }

        public ActionResult SaveHomeworkRemarks(HomeworkReplyList m)
        {
            var TotalCount = m.TotalCount;
            int size = TotalCount;
            string[] Id = new string[size];
            string[] Homework_comment = new string[size];

            HomeworkReplyFromStudentServices HomeworkreplySer = new HomeworkReplyFromStudentServices();

            for (var i = 0; i < TotalCount; i++)
            {
                Id[i] = QSCrypt.Decrypt(m.Id[i]);
                int HomeworkAnswerId = Convert.ToInt32(Id[i]);
                Homework_comment[i] = m.Homework_comment[i];
                HomeworkreplySer.UpdateRemark(HomeworkAnswerId, Homework_comment[i], FacultyId);
            }
            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Add_home_work_remarks"));
            string Message = ResourceCache.Localize("RemarksSavedSuccessfully");
            return Json(new { Message }, JsonRequestBehavior.AllowGet);
        }

        // Assignment
        public ActionResult Assignment()
        {
            return View();
        }

        private IList<AssignmentList> AssignmentList1 = new List<AssignmentList>();

        public JsonResult AssignmentListResults(string sidx, string sord, int page, int rows, int? MI_Assign_List_allAcademicYears, string MI_List_ClassWithSection, int? MI_Assign_List_allSubjects, DateTime? MI_Assign_List_txt_fromDate, DateTime? MI_Assign_List_txt_toDate)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            if (MI_Assign_List_allAcademicYears != 0 && MI_List_ClassWithSection != null && MI_Assign_List_allSubjects != 0 && MI_Assign_List_txt_fromDate != null && MI_Assign_List_txt_toDate != null)
            {
                string clasSec = MI_List_ClassWithSection;
                string[] words = clasSec.Split(' ');
                int classId = Convert.ToInt32(words[0]);
                int secId = Convert.ToInt32(words[1]);
                AssignmentList1 = assign.getAssignmentList(MI_Assign_List_allAcademicYears, classId, secId, MI_Assign_List_allSubjects, MI_Assign_List_txt_fromDate, MI_Assign_List_txt_toDate, FacultyId);
            }

            int totalRecords = AssignmentList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    AssignmentList1 = AssignmentList1.OrderByDescending(s => s.Assignment).ToList();
                    AssignmentList1 = AssignmentList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    AssignmentList1 = AssignmentList1.OrderBy(s => s.Assignment).ToList();
                    AssignmentList1 = AssignmentList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = AssignmentList1;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = AssignmentList1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Assignment_ExportToExcel()
        {
            try
            {
                List<AssignmentList> AssignmentList1 = (List<AssignmentList>)Session["JQGridList"];
                var list = AssignmentList1.Select(o => new { PostedtDate = o.PostedtDate, Assignment = o.Assignment, Descriptions = o.Descriptions, LastDate = o.LastDate }).ToList(); //, Status = o.Status
                string fileName = ResourceCache.Localize("AssignmentExcelFileName");
                GenerateExcel.ExportExcel(list, fileName);
                return View("Assignment");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Assignment_ExportToPdf()
        {
            try
            {
                List<AssignmentList> AssignmentList1 = (List<AssignmentList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("AssignmentList");
                GeneratePDF.ExportPDF_Portrait(AssignmentList1, new string[] { "PostedtDate", "Assignment", "Descriptions", "LastDate" }, xfilePath, heading);  //, "Status"
                return File(xfilePath, "application/pdf", ResourceCache.Localize("AssignmentList.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult AddAssignment1()
        {
            try
            {
                var year = Request["year"];
                var classSection = Request["classSection"];
                var subject = Request["subject"];
                var date = Request["dat"];
                string Assignment = Request["assignment"];
                string desc = Request["desc"];

                string Email = Request["email"];
                string Sms = Request["sms"];
                var Cyear = acservice.getCurrentAcademicYearId(DateTimeByZone.getCurrentDate());
                if (Cyear != null)
                {
                    Session["CurrentAcYearId"] = Cyear.AcademicYearId;
                }
                else
                {
                    Session["CurrentAcYearId"] = "";
                }

                int CurrentYearId;
                if (Session["CurrentAcYearId"] != "")
                {
                    CurrentYearId = Convert.ToInt16(Session["CurrentAcYearId"]);
                }
                else
                {
                    CurrentYearId = 0;
                }
                
                if (year != null && classSection != null && subject != null && date != null && Assignment != null && desc != null)
                {
                    int yearId = Convert.ToInt32(year);
                    if (CurrentYearId == yearId)
                    {

                        string[] words = classSection.Split(' ');
                        int classId = Convert.ToInt32(words[0]);
                        int secId = Convert.ToInt32(words[1]);
                        int subId = Convert.ToInt32(subject);
                        DateTime submissionDate = Convert.ToDateTime(date);

                        string AssFileName = null;

                        if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                        {
                            var file = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                            string filenamewithoutextention;
                            Random random = new Random();
                            string fname = random.Next(00000, 99999).ToString();
                            string extention = System.IO.Path.GetExtension(file.FileName);

                            var fileSize = file.ContentLength;
                            fileSize = file.ContentLength / 1048576;

                            if (extention == ".pdf" || extention == ".docx" || extention == ".doc" || extention == ".txt")
                            {
                                if (file != null && file.ContentLength > 0 && fileSize <= 3)
                                {
                                    filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file.FileName);
                                    AssFileName = "Ass" + fname + extention;
                                    string path = System.IO.Path.Combine(Server.MapPath("~/Documents/Assignment"), "Ass" + fname + extention);
                                    file.SaveAs(path);
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = ResourceCache.Localize("FileSizeExceeds");
                                }
                            }
                            else
                            {
                                string ErrorMessage = ResourceCache.Localize("FileFormatDoesNotSupport");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        assign.addAssignment(yearId, classId, secId, subId, submissionDate, Assignment, desc, AssFileName, FacultyId);
                        string Message = ResourceCache.Localize("AssignmentSavedSuccessfully");

                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddAssignmentsToStudent"));
                        // Email & SMS Notification

                        PrimaryUserRegisterServices primaryUser = new PrimaryUserRegisterServices();
                        var studentEmailList = primaryUser.getPrimaryUserMailList(yearId, classId, secId);

                        // get academicYear, class, section
                        AcademicyearServices AcYear = new AcademicyearServices();
                        ClassServices clas = new ClassServices();
                        SectionServices sec = new SectionServices();
                        SubjectServices subSer = new SubjectServices();

                        var getYear = AcYear.AcademicYear(yearId).AcademicYear1;
                        var getClass = clas.getClassNameByClassID(classId).ClassType;
                        var getSection = sec.getSectionName(classId, secId).SectionName;
                        var getSubject = subSer.getSubjectName(subId).SubjectName;

                        if (Email != null)
                        {
                            int count = studentEmailList.Count;
                            string[] email = new string[count];
                            for (int i = 0; i < count; i++)
                            {
                                email[i] = studentEmailList[i].PrimaryUserEmail;
                            }
                            string Tomail = string.Join(",", email);

                            StreamReader reader = new StreamReader(Server.MapPath("~/Template/Homework.html"));
                            string readFile = reader.ReadToEnd();
                            string myString = "";
                            myString = readFile;

                            myString = myString.Replace("$$year$$", getYear);
                            myString = myString.Replace("$$class$$", getClass);
                            myString = myString.Replace("$$section$$", getSection);

                            myString = myString.Replace("$$subject$$", getSubject);
                            myString = myString.Replace("$$title$$", Assignment.ToString());
                            myString = myString.Replace("$$description$$", desc.ToString());

                            string sub = ResourceCache.Localize("AssignmentOn") + submissionDate.ToString("dd-MM-yyyy");

                            string path = "";
                            if (AssFileName != null)
                            {
                                path = System.IO.Path.Combine(Server.MapPath("~/Documents/Assignment"), AssFileName);
                            }
                            string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                            string PlaceName = "ModulesInfo-AddAssignment-Assignment";
                            string UserName = "Employee";
                            string UserId = Convert.ToString(Session["username"]);
                            Mail.SendMultipleMail(ResourceCache.Localize("AceClassRoomAssignment"), supportEmail, Tomail, sub, myString.ToString(), true, path, PlaceName, UserName, UserId); 
                        }
                        if (Sms != null)
                        {
                            int count = studentEmailList.Count;
                            string[] mobileNumber = new string[count];
                            for (int i = 0; i < count; i++)
                            {
                                mobileNumber[i] = Convert.ToString(studentEmailList[i].PrimaryUserMobileNum);
                            }
                            string ToNumber = string.Join(",", mobileNumber);
                            string sub = ResourceCache.Localize("AssignmentOn") + submissionDate.ToString("dd-MM-yyyy");
                            string Description = desc;
                            SMS.Main(ResourceCache.Localize("AceClassRoomAssignment"), ToNumber, sub, Description);
                        }
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = ResourceCache.Localize("PleaseSelectCurrentAcademicYear");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("PleaseFillAllMandatoryFields");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public void OpenAssignmentFile(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                var getAssignmentFile = assign.getStudentAssignmentFile(fid).AssignmentFileName;
                string file = getAssignmentFile;
                if (file != null)
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Documents/Assignment/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {
                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + file);
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();

            }
        }

        public ActionResult EditAssignment(string passId)
        {
            try
            {
                int eid = Convert.ToInt32(QSCrypt.Decrypt(passId));
                var getRow = assign.getStudentAssignmentFile(eid);
                var CompletionDate = getRow.DateOfSubmission.Value.ToString("dd/mm/yyyy");
                var Assignment = getRow.Assignment1;
                var desc = getRow.Descriptions;
                var status = getRow.AssignmentStatus;
                var ExistFile = getRow.AssignmentFileName;
                return Json(new { CompletionDate, Assignment, desc, status, ExistFile }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult EditAssignment1()
        {
            try
            {
                var date = Request["dat"];
                string Assignment = Request["assignment"];
                string desc = Request["desc"];
                var Status = Request["status"];
                var AssignmentId = Request["AssignmentId"];


                if (date != null && Assignment != null && desc != null && Status != null && AssignmentId != null)
                {
                    string AssFileName = null;

                    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                    {
                        var file = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                        string filenamewithoutextention;
                        Random random = new Random();
                        string fname = random.Next(00000, 99999).ToString();
                        string extention = System.IO.Path.GetExtension(file.FileName);

                        var fileSize = file.ContentLength;
                        fileSize = file.ContentLength / 1048576;

                        if (extention == ".pdf" || extention == ".docx" || extention == ".doc" || extention == ".txt")
                        {
                            if (file != null && file.ContentLength > 0 && fileSize <= 3)
                            {
                                filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file.FileName);
                                AssFileName = "Ass" + fname + extention;
                                string path = System.IO.Path.Combine(Server.MapPath("~/Documents/Assignment"), "Ass" + fname + extention);
                                file.SaveAs(path);
                            }
                            else
                            {
                                ViewBag.ErrorMessage = ResourceCache.Localize("FileSizeExceeds");
                            }
                        }
                        else
                        {
                            string ErrorMessage = ResourceCache.Localize("FileFormatDoesNotSupport");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    int eid = Convert.ToInt32(QSCrypt.Decrypt(AssignmentId));
                    DateTime Date = Convert.ToDateTime(date);
                    bool status = Convert.ToBoolean(Status);

                    if (AssFileName != null)
                    {
                        var getOldFile = assign.getStudentAssignmentFile(eid);
                        var OldFileName = getOldFile.AssignmentFileName;
                        string path1 = Server.MapPath("~/Documents/Assignment/" + OldFileName);
                        FileInfo file = new FileInfo(path1);
                        file.Delete();
                        assign.updateAssignment(Date, Assignment, desc, status, AssFileName, eid);
                    }
                    else
                    {
                        assign.updateAssignment(Date, Assignment, desc, status, eid);
                    }
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdateAssignmentsToStudent"));
                    string Message = ResourceCache.Localize("Assignment_updated_successfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("PleaseFillAllMandatoryFields");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteAssignment(string passId)
        {
            try
            {
                int Did = Convert.ToInt32(QSCrypt.Decrypt(passId));
                assign.delAssignment(Did);
                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Delete_assignments_to_student"));
                string Message = ResourceCache.Localize("AssignmentDetailsDeletedSuccessfully");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AssignmentAnswer(string id)
        {
            int AssignmentId = Convert.ToInt32(QSCrypt.Decrypt(id));
            AssignmentServices AssignServices = new AssignmentServices();
            var classDetails = AssignServices.GetClassDetails(AssignmentId);
            int YearId = classDetails.AcademicYearId.Value;
            int classId = classDetails.ClassId.Value;
            int secId = classDetails.SectionId.Value;
            AssignmentReplyFromStudentServices AssignmentreplySer = new AssignmentReplyFromStudentServices();
            var studentList = AssignmentreplySer.getStudentList(AssignmentId, YearId, classId, secId);
            if (studentList.Count == 0)
            {
                ViewBag.Student = "Empty";
            }
            return View(studentList);
        }
        public void OpenAssignmentAnswerFile(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                HomeworkReplyFromStudentServices HomeworkreplySer = new HomeworkReplyFromStudentServices();
                var getFile = HomeworkreplySer.getHomeworkAnswerFile(fid);
                if(getFile != null)
                {
                    var File = getFile.AnswerFileName;
                    string file = File;
                    if (file != null)
                    {
                        string extention = System.IO.Path.GetExtension(file);
                        string FilePath = Server.MapPath("~/Documents/AssignmentReply/" + file);
                        WebClient User = new WebClient();
                        Byte[] FileBuffer = User.DownloadData(FilePath);
                        if (FileBuffer != null)
                        {
                            if (extention == ".pdf")
                            {
                                Response.ContentType = "application/pdf";
                            }
                            else if (extention == ".doc" || extention == ".docx")
                            {
                                Response.ContentType = "application/vnd.ms-word";
                            }
                            Response.AddHeader("Content-Disposition", "attachment; filename=" + file);
                            Response.BinaryWrite(FileBuffer);
                            Response.Flush();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }

        public ActionResult SaveAssignmentRemarks(HomeworkReplyList m)
        {
            var TotalCount = m.TotalCount;
            int size = TotalCount;
            string[] Id = new string[size];
            string[] Homework_comment = new string[size];
            AssignmentReplyFromStudentServices AssignmentkreplySer = new AssignmentReplyFromStudentServices();
            for (var i = 0; i < TotalCount; i++)
            {
                Id[i] = QSCrypt.Decrypt(m.Id[i]);
                int HomeworkAnswerId = Convert.ToInt32(Id[i]);
                Homework_comment[i] = m.Homework_comment[i];
                AssignmentkreplySer.UpdateRemark(HomeworkAnswerId, Homework_comment[i], FacultyId);
            }
            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Add_assignments_remarks"));
            string Message = ResourceCache.Localize("RemarksSavedSuccessfully");
            return Json(new { Message }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Queries()
        {
            return View();
        }

        private  IList<MI_Queries> QueryList1 = new List<MI_Queries>();

        public JsonResult QueryListResults(string sidx, string sord, int page, int rows, int? MI_Queries_allAcademicYears, string MI_Queries_ClassWithSection, int? MI_Queries_allSubjects1, DateTime? R_Query_FromDate, DateTime? R_Query_ToDate)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            StudentfacultyquriesServices fq = new StudentfacultyquriesServices();
            if (MI_Queries_allAcademicYears != 0 && MI_Queries_ClassWithSection != null && MI_Queries_allSubjects1 != null && MI_Queries_allSubjects1 != 0 && R_Query_FromDate != null && R_Query_ToDate != null)
            {
                string clasSec = MI_Queries_ClassWithSection;
                string[] words = clasSec.Split(' ');
                int classId = Convert.ToInt32(words[0]);
                int secId = Convert.ToInt32(words[1]);
                QueryList1 = fq.getFaculyQueryList(MI_Queries_allAcademicYears, classId, secId, MI_Queries_allSubjects1, R_Query_FromDate, R_Query_ToDate, FacultyId).ToList();
            }

            if (MI_Queries_allAcademicYears != 0 && MI_Queries_ClassWithSection == "all" && R_Query_FromDate != null && R_Query_ToDate != null)
            {
                QueryList1 = fq.getFaculyQueryListForClass(MI_Queries_allAcademicYears, R_Query_FromDate, R_Query_ToDate, FacultyId).ToList();
                Session["JQGridList"] = QueryList1;
            }

            int totalRecords = QueryList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    QueryList1 = QueryList1.OrderByDescending(s => s.R_Query_FromDate).ToList();
                    QueryList1 = QueryList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    QueryList1 = QueryList1.OrderBy(s => s.R_Query_FromDate).ToList();
                    QueryList1 = QueryList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = QueryList1;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = QueryList1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult showReplyFormWithStudentName(string id)
        {
            StudentfacultyquriesServices fq = new StudentfacultyquriesServices();
            int Qid = Convert.ToInt32(QSCrypt.Decrypt(id));
            var checkReplySent = fq.checkReplySent(Qid);
            var reply = checkReplySent.Reply;
            if (reply != null)
            {
                var txt = "ReplySent";
                return Json(new { txt }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var studentDetail = fq.getStudentDetails(Qid);
                var academicYearId = studentDetail.academicYearId;
                var Cyear = acservice.getCurrentAcademicYearId(DateTimeByZone.getCurrentDate());
                if (Cyear != null)
                {
                    Session["CurrentAcYearId"] = Cyear.AcademicYearId;
                }
                else
                {
                    Session["CurrentAcYearId"] = "";
                }
                int CurrentYearId;
                if (Session["CurrentAcYearId"] != "")
                {
                    CurrentYearId = Convert.ToInt16(Session["CurrentAcYearId"]);
                }
                else
                {
                    CurrentYearId = 0;
                }
                if (CurrentYearId == academicYearId)
                {
                var studentId = studentDetail.StudentId;
                var studentName = studentDetail.FName + " " + studentDetail.LName;
                var classsSec = studentDetail.classs + " " + studentDetail.Sec;
                var sub = studentDetail.subject;
                var Query = studentDetail.Query;
                return Json(new { studentId, studentName, classsSec, sub, Query, academicYearId }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("You_Can_Reply_for_Current_AcademicYear_Query_Only");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult AddReply()
        {
            try
            {
                string ReplyFileName = null;
                var Qid = Request["QId"];
                string reply = Request["Reply"];

                if (Qid != null && Qid != "" && reply != null)
                {
                    int qId = Convert.ToInt32(QSCrypt.Decrypt(Qid));

                    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                    {
                        var file1 = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                        string filenamewithoutextention;
                        Random random = new Random();
                        string fname = random.Next(00000, 99999).ToString();
                        string extention = System.IO.Path.GetExtension(file1.FileName);

                        var fileSize = file1.ContentLength;
                        fileSize = file1.ContentLength / 1048576;

                        if (extention == ".pdf" || extention == ".docx" || extention == ".doc" || extention == "txt")
                        {
                            if (file1 != null && file1.ContentLength > 0 && fileSize <= 3)
                            {
                                filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file1.FileName);
                                ReplyFileName = "Reply" + fname + extention;
                                string path = System.IO.Path.Combine(Server.MapPath("~/Documents/QueryReply"), ReplyFileName);
                                file1.SaveAs(path);
                            }
                            else
                            {
                                string ErrorMessage = ResourceCache.Localize("FileSizeExceeds");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            string ErrorMessage = ResourceCache.Localize("FileFormatDoesNotSupport");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    StudentfacultyquriesServices FQ = new StudentfacultyquriesServices();
                    FQ.addReply(qId, reply, ReplyFileName, FacultyId);
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("ReplyStudentQuery"));
                    string Message = ResourceCache.Localize("ReplySentSuccessfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("PleaseFillAllMandatoryFields");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public void OpenQueryFile(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                StudentfacultyquriesServices FQ = new StudentfacultyquriesServices();
                var getQueryFile = FQ.getQueryFile(fid).QueryFile;
                string file = getQueryFile;
                if (file != null)
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Documents/Query/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {
                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + file);
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }

        public void OpenReplyFile(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                StudentfacultyquriesServices FQ = new StudentfacultyquriesServices();
                var getReplyFile = FQ.getQueryFile(fid).ReplyFile;
                string file = getReplyFile;
                if (file != null)
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Documents/QueryReply/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {
                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + file);
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }

        public JsonResult getDate(int acid)
        {
            try
            {
                AcademicyearServices AcYear = new AcademicyearServices();
                var ans = AcYear.date(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                EventServices holidays = new EventServices();
                var HolidaysList = holidays.getHolidaysList(acid);
                return Json(new { sd = sdate, ed = edate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult FutureActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    EventServices holidays = new EventServices();
                    int acid = CurrentAcYear[i].AcademicYearId;
                    var HolidaysList = holidays.getHolidaysList(acid);
                    return Json(new { StartingEnableDate, EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { StartingEnableDate, EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PastActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    EventServices holidays = new EventServices();
                    int acid = CurrentAcYear[i].AcademicYearId;
                    var HolidaysList = holidays.getHolidaysList(acid);
                    return Json(new { StartingEnableDate, EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { StartingEnableDate, EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CurrentActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");

            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    int acid = CurrentAcYear[i].AcademicYearId;
                    return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CustomeActiveDate(int acid)
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var ans = AcYear.date(acid);
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            EventServices holidays = new EventServices();
            var HolidaysList = holidays.getHolidaysList(acid);
            DateTime CurrentDate = DateTimeByZone.getCurrentDate();
            DateTime sdate = ans.StartDate;
            DateTime edate = ans.EndDate;
            if (sdate <= CurrentDate && CurrentDate <= edate)
            {
                StartingEnableDate = sdate.ToString("dd/MM/yyyy");

                return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                EndingEnableDate = edate.ToString("dd/MM/yyyy");
                return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
        }
      
    }
}
