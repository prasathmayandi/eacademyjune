﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class MarkConfigurationController : Controller
    {
        AcademicyearServices obj_academicYear = new AcademicyearServices();
        MarkServices obj_Mark = new MarkServices();
        ClassServices obj_class = new ClassServices();
        SectionServices obj_section = new SectionServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ViewBag.acYear = obj_academicYear.AllList_AcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
        }
        public ActionResult MarkConfig()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult getClassList()
        {
            try
            {
                var ans = obj_class.Classes();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getSectionList(int cid)
        {
            try
            {
                var ans = obj_section.getSectionByClassId(cid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        
        public JsonResult MarkResults(string sidx, string sord, int page, int rows, int? acYear_mark, int? class_mark, int? section_mark)
        {
           IList<MarkDetails> todoListsResults1 = new List<MarkDetails>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults1 = obj_Mark.getMark();
            if (acYear_mark.HasValue && class_mark.HasValue && section_mark.HasValue)
            {
                todoListsResults1 = todoListsResults1.Where(p => p.AcYearId == acYear_mark && p.ClassId == class_mark && p.SectionId == section_mark).ToList();
            }
            int totalRecords = todoListsResults1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults1 = todoListsResults1.OrderByDescending(s => s.Subject).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults1 = todoListsResults1.OrderBy(s => s.Subject).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults1;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MarkExportToExcel()
        {
            try
            {
                List<MarkDetails> todoListsResults1 = (List<MarkDetails>)Session["JQGridList"];
                var list = todoListsResults1.Select(o => new { AcademicYear = o.AcademicYear, Class = o.Class, Section = o.Section, Subject = o.Subject, MaximumMark = o.MaximumMark, MinimumMark = o.MinimumMark }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("MarkDetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult MarkExportToPDF()
        {
            try
            {
                List<MarkDetails> todoListsResults1 = (List<MarkDetails>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("MarkDetails");
                GeneratePDF.ExportPDF_Portrait(todoListsResults1, new string[] { "AcademicYear", "Class", "Section", "Subject", "MaximumMark", "MinimumMark" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "MarkDetails.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public JsonResult getClass()
        {
            ClassServices obj_class = new ClassServices();
            try
            {
                var ans = obj_class.Classes();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getSection(int cid)
        {
            SectionServices obj_section = new SectionServices();
            try
            {
                var ans = obj_section.getSectionByClassId(cid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getSubject(int acid, int cid, int sec_id)
        {
            SubjectServices obj_subject = new SubjectServices();
            try
            {
                var ans = obj_subject.getSubject(acid, cid, sec_id);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult MarkConfig(SaveMark model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = model.acYear_Sub;
                    int min = model.txt_minMark;
                    int max = model.txt_maxMark;
                    int cid = model.class_Sub;
                    int sec_id = model.Section_sub;
                    int sid = model.Subjects;
                    bool cc = obj_Mark.checkMark(acid, cid, sec_id, sid);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Mark_for_this_Subject_is_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_Mark.addMark(acid, cid, sec_id, sid, min, max);
                        string Message = eAcademy.Models.ResourceCache.Localize("Mark_Details_Added_Successfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_mark_to_subject"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditMarkDetails(string markId)
        {
            try
            {
                int mId = Convert.ToInt32(QSCrypt.Decrypt(markId));
                var ans = obj_Mark.getMarkDetails(mId);

                bool existacademicyear = obj_academicYear.PastAcademicyear(ans.AcYearId);
                if (existacademicyear == true)
                {
                    return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                    return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                }
               
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditMarkDetails(EditMark model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int markId = Convert.ToInt32(QSCrypt.Decrypt(model.mid));
                    int min = model.txt_minMark;
                    int max = model.txt_maxMark;
                    int acid = model.acid;
                    int cid = model.cid;
                    int sec_id = model.sec_id;
                    int sid = model.sid;
                    bool cc = obj_Mark.checkMark(acid, cid, sec_id, sid, min, max);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Mark_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_Mark.updateMark(markId, max, min);
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_mark_to_subject"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}