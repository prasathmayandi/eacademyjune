﻿using eAcademy.DataModel;
using eAcademy.Services;
using System;
using System.Linq;
using System.Web.Mvc;

namespace eAcademy.Controllers
{
    [Student_Parent_SessionAttribute]
    public class TimetableController : Controller
    {
        //
        // GET: /Timetable/

        public int StudentRegId;
        public int yearId;
        public int classId;
        public int secId;

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            StudentRegId = Convert.ToInt32(Session["StudentRegId"]);
            ExamServices Exam = new ExamServices();
            AssignClassToStudentServices clas = new AssignClassToStudentServices();
            ViewBag.allExams = Exam.ShowExams();
            var getRow = clas.getAssigendClass(StudentRegId);
            if (getRow != null)
            {
                yearId = Convert.ToInt32(getRow.AcademicYearId);
                classId = Convert.ToInt32(getRow.ClassId);
                secId = Convert.ToInt32(getRow.SectionId);
            }         
        }
     
        public ActionResult ClassTimetable()
        {
            return View();
        }

        public JsonResult getStudentTimetable()
        {
            int day = 0;
            int? period = 0;
            SchoolSettingsServices sett = new SchoolSettingsServices();
            TimeScheduleServices tss = new TimeScheduleServices();
            var settingsExist = sett.SchoolSettings();
            if (settingsExist != null)
            {
                day = settingsExist.DayOrder.Value;
                TimeScheduleServices tschedule = new TimeScheduleServices();
                period = tschedule.GetClassPeriodCount(classId);
                //period = count;
                var PeriodsList = tss.GetClassPeriodTimeTable(classId);
                AssignSubjectToPeriodServices SubjectPeriod = new AssignSubjectToPeriodServices();
                var StudentPeriodSubject = SubjectPeriod.StudentTimetable(yearId, classId, secId);
                return Json(new { day, period, StudentPeriodSubject, PeriodsList }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { day, period }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExamTimetable()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ExamTimetable(int passExamId)
        {
            try
            {             
                ExamTimetableServices exam = new ExamTimetableServices();
                var ExamList = exam.getExamList(yearId, classId,secId,  passExamId);
                var count = ExamList.ToList().Count;
                return Json(new { ExamList, count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
