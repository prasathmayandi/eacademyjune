﻿using eAcademy.DataModel;
using eAcademy.ViewModel;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using eAcademy.Helpers;
using eAcademy.HelperClass;
using eAcademy.Models;

namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class MarksInfoController : Controller
    {
        //
        // GET: /MarksInfo/

        AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
        AssignClassToStudentServices stu = new AssignClassToStudentServices();
        StudentMarkServices stuMark = new StudentMarkServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        public int FacultyId;

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            FacultyId = Convert.ToInt32(Session["empRegId"]);
            AcademicyearServices AcYear = new AcademicyearServices();
            ClassServices clas = new ClassServices();
            ExamServices exam = new ExamServices();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.allClasses = clas.ShowClasses();
            ViewBag.allExams = exam.ShowExams();
        }

        public JsonResult GetSelectedClassSection(int passClassId)
        {
            try
            {
                SectionServices sec = new SectionServices();
                var List = sec.getSection(passClassId);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult showSection(int passClassId)
        {
            try
            {
                var SecList = facultySub.ShowSection(FacultyId, passClassId);
                return Json(SecList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult showSubjects(int passSecId)
        {
            try
            {
                var SubjectList = facultySub.ShowSubjects(passSecId, FacultyId);
                return Json(SubjectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult showSubjects1(int passYearId, int passClassId, int passSecId)
        {
            try
            {
                var SubjectList = facultySub.ShowSubjects(passYearId, passClassId, passSecId, FacultyId);
                return Json(SubjectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getClassWithSection(int passYearId)
        {
            var list = facultySub.getClassWithSection(FacultyId, passYearId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public IEnumerable<Object> ShowClassWithSection(int yearId)
        {
            var list = facultySub.getClassWithSection(FacultyId, yearId);
            return list;
        }

        public JsonResult GetStudents(int passYearId, int passClassId, int passSecId)
        {
            try
            {
                AssignClassToStudentServices stu = new AssignClassToStudentServices();
                var StudentList = stu.getStudents1(passYearId, passClassId, passSecId);
                return Json(StudentList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult showStudents(int passSecId)
        {
            try
            {
                var StudentList = stu.getStudentsList(passSecId);
                return Json(StudentList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public IEnumerable<Object> ShowStudents1(int yearId, int classId, int secId)
        {
            AssignClassToStudentServices stu = new AssignClassToStudentServices();
            var StudentList = stu.getStudents1(yearId, classId, secId);
            return StudentList;
        }

        public ActionResult Mark()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getStudentListForMark(int passYearId, int passClassId, int passSecId, int passSubId, int passExamId)
        {
            try
            {
                MarkServices mark = new MarkServices();
                var getMaxMark = mark.getMaxMark(passYearId, passClassId, passSecId, passSubId);
                if (getMaxMark == null)
                {
                    string msg1 = "Error";
                    return Json(new { msg1 }, JsonRequestBehavior.AllowGet);
                }
                ExamAttendanceServices examAtt = new ExamAttendanceServices();
                var checkAttendanceTaken = examAtt.CheckExamAttendanceExist(passYearId, passClassId, passSecId, passSubId, passExamId);
                if (checkAttendanceTaken == null)
                {
                    string msg1 = "Error1";
                    return Json(new { msg1 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    tblAttendanceStatusServices attStatus = new tblAttendanceStatusServices();
                    var attStatusList = attStatus.AttendanceStatusList();
                    var checkMarkExist = stuMark.checkMarkExist(passYearId, passClassId, passSecId, passSubId, passExamId);
                    if (checkMarkExist != null)
                    {
                        var getMarkExist = stuMark.getExistMark(passYearId, passClassId, passSecId, passSubId, passExamId);
                        string msg = "old";
                        var MaxMark = getMaxMark.MaxMark;
                        return Json(new { msg, getMarkExist, MaxMark, attStatusList }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var studentList = stu.getStudentsToGiveMark(passYearId, passClassId, passSecId, passSubId, passExamId);
                        var studentCount = stu.getStudentscount(passYearId, passClassId, passSecId);
                        string msg = "new";
                        var MaxMark = getMaxMark.MaxMark;
                        return Json(new { msg, studentList, MaxMark, studentCount, attStatusList }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Mark(Mark1 m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int YearId = Convert.ToInt32(m.Mark_allAcademicYears);
                    int currentAcYearId = Convert.ToInt32(Session["CurrentAcYearId"]);
                    if (YearId == currentAcYearId)
                    {
                        int yearId = m.Mark_allAcademicYears;
                        int classId = m.Mark_allClass;
                        int secId = m.Mark_Section;
                        int subId = m.Mark_allSubjects;
                        int examId = m.Mark_Exam;

                        MarkTotalServices markTotal = new MarkTotalServices();
                        var IsTotalCalculated = markTotal.checkTotalCalculated(yearId, classId, secId, examId);
                        if (IsTotalCalculated != null)
                        {
                            string ErrorMessage = ResourceCache.Localize("You_cannot_change_the_mark_total_had_been_calculated");
                            return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            int TotalCount = Convert.ToInt32(m.TotalCount);
                            int size = TotalCount + 1;
                            string[] Id = new string[size];
                            string[] mark = new string[size];
                            string[] comment = new string[size];

                            MarkServices Mmark = new MarkServices();
                            var maxMark = Mmark.getMaxMark(yearId, classId, secId, subId).MaxMark;

                            var checkMarkExist = stuMark.checkMarkExist(yearId, classId, secId, subId, examId);
                            if (checkMarkExist != null)
                            {
                                for (int i = 0; i <= TotalCount; i++)
                                {
                                    Id[i] = QSCrypt.Decrypt(m.Id[i]);
                                    int StudentRegisterId = Convert.ToInt32(Id[i]);
                                    int Mark = Convert.ToInt32(m.Mark[i]);
                                    string Comment = m.Comment[i];

                                    if (Mark > maxMark)
                                    {
                                        string ErrorMessage = ResourceCache.Localize("Mark_should_not_exceed_maximum_mark");
                                        return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                                    }

                                    int EmployeeRegisterId = FacultyId;
                                    stuMark.updateStudentMark(yearId, classId, secId, examId, subId, StudentRegisterId, Mark, EmployeeRegisterId, Comment);
                                }
                                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Update_mark_to_student"));
                                string Message = ResourceCache.Localize("Marks_updated_successfully");
                                return Json(new { Message }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                for (int i = 0; i <= TotalCount; i++)
                                {
                                    Id[i] = QSCrypt.Decrypt(m.Id[i]);
                                    int StudentRegisterId = Convert.ToInt32(Id[i]);
                                    int Mark = Convert.ToInt32(m.Mark[i]);
                                    string Comment = m.Comment[i];

                                    if (Mark > maxMark)
                                    {
                                        string ErrorMessage = ResourceCache.Localize("Mark_should_not_exceed_maximum_mark");
                                        return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                                    }

                                    int EmployeeRegisterId = FacultyId;
                                    stuMark.addStudentMark(yearId, classId, secId, examId, subId, StudentRegisterId, Mark, EmployeeRegisterId, Comment);
                                }
                                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Add_mark_to_student"));
                                string Message = ResourceCache.Localize("Marks_saved_successfully");
                                return Json(new { Message }, JsonRequestBehavior.AllowGet);
                            }

                        }
                    }
                    else
                    {
                        string InfoMessage = ResourceCache.Localize("Records_can_save_for_current_academic_year_only");
                        return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult ProgressCard()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ProgressCard(FacultyProgressCard pc)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int yearId = pc.PG_allAcademicYears;
                    var CI_ClassWithSection = pc.CI_ClassWithSection;
                    int stuId = pc.PG_allStudents;
                    int examId = pc.PG_Exam;

                    string clasSec = CI_ClassWithSection;
                    string[] words = clasSec.Split(' ');
                    int classId = Convert.ToInt32(words[0]);
                    int secId = Convert.ToInt32(words[1]);

                    var list = stuMark.getMark(yearId, classId, secId, examId, stuId);

                    int count = list.Count;

                    MarkTotalServices markTotal = new MarkTotalServices();
                    var MarkTotal = markTotal.getMarkTotal(yearId, classId, secId, examId, stuId);

                    if (MarkTotal != null)
                    {
                        var Total = MarkTotal.TotalMark;
                        var Result = MarkTotal.Result;
                        var Rank = MarkTotal.StudentRank;
                        var comment = MarkTotal.Comment;
                        return Json(new { list, Total, Result, Rank, comment, count }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var status = ResourceCache.Localize("Till_total_is_not_calculated");
                        return Json(new { status, list, count }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
