﻿using eAcademy.ViewModel;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.HelperClass;

namespace eAcademy.Controllers
{
     [CheckSessionOutAttribute]
    public class EmployeeManagementController : Controller
    {
        //
        // GET: /EmployeeManagement/

        StudentFacultyFeedback_service fb = new StudentFacultyFeedback_service();
        TechemployeesServices techEmp = new TechemployeesServices();
        FacultyleaverequestServices leave = new FacultyleaverequestServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        public int ApproverId;

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ApproverId = Convert.ToInt32(Session["empRegId"]);
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "All", Value = "All" });
            li.Add(new SelectListItem { Text = "Pending", Value = "Pending" });
            li.Add(new SelectListItem { Text = "Approved", Value = "Approved" });
            li.Add(new SelectListItem { Text = "Declined", Value = "Declined" });
            ViewBag.StatusList = li;
            ViewBag.allTechFaculties = techEmp.ShowTechFaculties();
            AcademicyearServices AcYear = new AcademicyearServices();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
        }

        public ActionResult LeaveRequest()
        {
            ViewBag.success = TempData["Success"];
            ViewBag.InfoMsg = TempData["InfoMessage"];
            return View();
        }

        [HttpPost]
        public ActionResult LeaveRequest(LeaveRequestListForCI l)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    DateTime fdate = l.SM_txt_FromDate_List;
                    DateTime toDate = l.SM_txt_ToDate_List;
                    if (l.Status == "All")
                    {
                        var list = techEmp.getFacultyLeaveRequestAll(fdate, toDate);
                        ViewBag.list = list;
                        ViewBag.count = list.Count;
                        ViewBag.fDate = fdate.ToString("dd/MM/yyyy");
                        ViewBag.toDate = toDate.ToString("dd/MM/yyyy");
                        return View(list);
                    }
                    else
                    {
                        var list = techEmp.getFacultyLeaveRequestList(fdate, toDate, l.Status);
                        ViewBag.list = list;
                        ViewBag.count = list.Count;
                        ViewBag.fDate = fdate.ToString("dd/MM/yyyy");
                        ViewBag.toDate = toDate.ToString("dd/MM/yyyy");
                        return View(list);
                    }

                }
                else
                {
                    ViewBag.fDate = l.SM_txt_FromDate_List;
                    ViewBag.toDate = l.SM_txt_ToDate_List;
                    ViewBag.status = l.Status;
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }        
        }

        [HttpPost]
        [ActionName("Decision")]
        [eAcademy.Filters.InitializeSimpleMembershipAttribute.OnAction(ButtonName = "BtnApprove")]
        public ActionResult Decision_1(FormCollection fm)
        {
          try
          {
                AcademicyearServices acyearServ = new AcademicyearServices();
                int DateWithInCurrentAcYear = acyearServ.IsDateWithInCurrentAcYear();
                if (DateWithInCurrentAcYear != 0)
                {
                    int currentAcYearId = Convert.ToInt32(Session["CurrentAcYearId"]);
                    if (DateWithInCurrentAcYear == currentAcYearId)
                    {
                        int count = Convert.ToInt32(fm["count"]);
                        var k = "selectStudent";
                        var p = "Req_id";

                        for (int i = 1; i <= count; i++)
                        {
                            var k2 = k + i;
                            var p2 = p + i;
                            var status = fm[k2];
                            if (status == "on")
                            {
                                int Req_id = Convert.ToInt32(fm[p2]);
                                string decision = "Approved";
                                leave.updateFacultyLeaveReuest(Req_id, ApproverId, decision);
                                var getLeaveDates = leave.getLeaveDates(Req_id, decision);
                                int? empId = getLeaveDates.EmployeeRegisterId;
                                DateTime fromDate = Convert.ToDateTime(getLeaveDates.FromDate);
                                DateTime toDate = Convert.ToDateTime(getLeaveDates.ToDate);
                                var Reason = getLeaveDates.LeaveReason;
                                var AttStatus = "L";
                                DateTime markedDate = DateTimeByZone.getCurrentDate();
                                FacultyDailyAttendanceServices faculytAttendance = new FacultyDailyAttendanceServices();
                                for (DateTime fdate = fromDate; toDate.CompareTo(fdate) >= 0; fdate = fdate.AddDays(1.0))
                                {
                                    faculytAttendance.addFacultyDailyAttendanceOnLeaveAccept(empId, fdate, AttStatus, Reason, markedDate);
                                }
                            }
                        }
                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("ApprovedEmployeeLeaveRequest"));
                        TempData["Success"] = ResourceCache.Localize("Leave_request_approved_successfully");
                        return RedirectToAction("LeaveRequest");
                    }
                    else
                    {
                        TempData["InfoMessage"] = ResourceCache.Localize("Records_can_save_for_current_academic_year_only");
                        return RedirectToAction("LeaveRequest");
                    }
                }
                else
                {
                    TempData["InfoMessage"] = ResourceCache.Localize("Records_can_save_for_current_academic_year_only");
                    return RedirectToAction("LeaveRequest");
                }
          }
          catch (Exception ex)
          {
              ViewBag.error = ex.Message;
              return View("Error");
          }   
        }

        [HttpPost]
        [ActionName("Decision")]
        [eAcademy.Filters.InitializeSimpleMembershipAttribute.OnAction(ButtonName = "BtnReject")]
        public ActionResult Decision_2(FormCollection fm)
        {
            try
            {
                AcademicyearServices acyearServ = new AcademicyearServices();
                int DateWithInCurrentAcYear = acyearServ.IsDateWithInCurrentAcYear();
                if (DateWithInCurrentAcYear != 0)
                {
                    int currentAcYearId = Convert.ToInt32(Session["CurrentAcYearId"]);
                    if (DateWithInCurrentAcYear == currentAcYearId)
                    {
                        int count = Convert.ToInt32(fm["count"]);
                        var k = "selectStudent";
                        var p = "Req_id";
                        for (int i = 1; i <= count; i++)
                        {
                            var k2 = k + i;
                            var p2 = p + i;
                            var status = fm[k2];
                            if (status == "on")
                            {
                                int Req_id = Convert.ToInt32(fm[p2]);
                                string decision = "Declined";
                                leave.updateFacultyLeaveReuest(Req_id, ApproverId, decision);
                            }
                        }
                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("DeclinedEmployeeLeaveRequest"));
                        TempData["Success"] = ResourceCache.Localize("Leave_request_Declined_successfully");
                        return RedirectToAction("LeaveRequest");
                    }
                    else
                    {
                        TempData["InfoMessage"] = ResourceCache.Localize("Records_can_save_for_current_academic_year_only");
                        return RedirectToAction("LeaveRequest");
                    }
                }
                else
                {
                    TempData["InfoMessage"] = ResourceCache.Localize("Records_can_save_for_current_academic_year_only");
                    return RedirectToAction("LeaveRequest");
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }   
        }

        public ActionResult FacultyFeedback()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FacultyFeedback(R_FacultyFeedback123 f)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var FeedbackList = fb.getFaculyFeedbackList(f.FacultyId, f.R_Feedback_FromDate, f.R_Feedback_ToDate);
                    ViewBag.action = "post";
                    if (FeedbackList.Count == 0)
                    {
                        ViewBag.val = "empty";
                        ViewBag.FacultyId = f.FacultyId;
                        ViewBag.fDate = f.R_Feedback_FromDate.ToString("dd/MM/yyyy");
                        ViewBag.toDate = f.R_Feedback_ToDate.ToString("dd/MM/yyyy");
                        return View();
                    }
                    ViewBag.List = FeedbackList.ToList();
                    ViewBag.count = FeedbackList;
                    ViewBag.FacultyId = f.FacultyId;
                    ViewBag.fDate = f.R_Feedback_FromDate.ToString("dd/MM/yyyy");
                    ViewBag.toDate = f.R_Feedback_ToDate.ToString("dd/MM/yyyy");
                    return View(FeedbackList);
                }
                else
                {
                    return View();
                }

            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }


        public ActionResult AssignSubstitudeFaculty()
        {
            return View();
        }

        public JsonResult getLeaveFacultyList(DateTime passDate)
        {
            try
            {
                FacultyDailyAttendanceServices faculytAttendance = new FacultyDailyAttendanceServices();
                var getLeaveFacultyList = faculytAttendance.getLeaveFaculty(passDate);
                var count = getLeaveFacultyList.ToList().Count;
                return Json(new { getLeaveFacultyList, count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }          
        }


        public JsonResult getFacultyPeriodList(DateTime passDate, string passEmpRegId, int passYear)     
        {
            try
            {
                int EmpRegId = Convert.ToInt32(QSCrypt.Decrypt(passEmpRegId));
                TechEmployeeServices techEmp = new TechEmployeeServices();
                var getEmployeeName = techEmp.EmployeeListByRegId(EmpRegId);
                var EmployeeName = getEmployeeName.EmployeeName;
                var getDay = passDate.DayOfWeek.ToString();
                var getDayNo = passDate.Day;
                int DayOrder = 0;
                DateTime today = DateTimeByZone.getCurrentDate();
                string dateStatus="";
                if (passDate < today)
                {
                    dateStatus = "DateEnded";
                }
                if (getDay == "Monday")
                {
                    DayOrder = 1;
                }
                else if (getDay == "Tuesday")
                {
                    DayOrder = 2;
                }
                else if (getDay == "Wednesday")
                {
                    DayOrder = 3;
                }
                else if (getDay == "Thursday")
                {
                    DayOrder = 4;
                }
                else if (getDay == "Friday")
                {
                    DayOrder = 5;
                }
                else if (getDay == "Saturday")
                {
                    DayOrder = 6;
                }
                else if (getDay == "Sunday")
                {
                    DayOrder = 7;
                }
                if (DayOrder != 0)
                {
                    AssignSubstitudeFacultyServices Substitude = new AssignSubstitudeFacultyServices();
                    var checkRecordExist = Substitude.checkSubstitudeExist(passDate, EmpRegId, DayOrder);
                    if(checkRecordExist != null)
                    {
                        string msg = "old";
                        var getExistFacultyPeriodList = Substitude.getExistFacultyPeriodList(passDate, EmpRegId, DayOrder);
                        return Json(new { msg, passDate, DayOrder, EmployeeName, getExistFacultyPeriodList, dateStatus }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string msg = "new";
                        AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
                        var FacultyPeriodList = facultySub.getTimetable1(passYear, EmpRegId, DayOrder);
                        var periodCount = FacultyPeriodList.Count;
                        return Json(new { msg, passDate, DayOrder, EmployeeName, FacultyPeriodList, periodCount, dateStatus }, JsonRequestBehavior.AllowGet);
                    } 
                }

                var FacultyPeriodList1 = "";
                return Json(new { passDate, DayOrder, EmployeeName, FacultyPeriodList1, dateStatus }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }          
        }

        public JsonResult GetAllFaculty()
        {
            TechEmployeeServices techEmp = new TechEmployeeServices();
            var FacultyList = techEmp.getEmployeeList();
            return Json(FacultyList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AssignSubstitude(SubstitudeFaculty m)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    int YearId = Convert.ToInt32(m.YearId);
                    int currentAcYearId = Convert.ToInt32(Session["CurrentAcYearId"]);
                    if (YearId == currentAcYearId)
                    {
                        int TotalCount = Convert.ToInt32(m.TotalCount);
                        int size = TotalCount + 1;
                        int yearId = m.YearId;
                        DateTime date = m.DateOfSubstitude;
                        int dayOrder = m.DayOrder;
                        int actualEmpRegId = Convert.ToInt32(QSCrypt.Decrypt(m.ActualEmployeeRegId));

                        AssignSubstitudeFacultyServices Substitude = new AssignSubstitudeFacultyServices();
                        string[] classId = new string[size];
                        string[] secId = new string[size];
                        string[] subId = new string[size];
                        string[] facultyId = new string[size];
                        string[] periodId = new string[size];

                        var checkRecordExist = Substitude.checkSubstitudeExist(date, actualEmpRegId, dayOrder);
                        if (checkRecordExist != null)
                        {

                            for (int i = 0; i <= TotalCount; i++)
                            {
                                classId[i] = QSCrypt.Decrypt(m.ClassId[i].ToString());
                                int classId1 = Convert.ToInt32(classId[i]);
                                secId[i] = QSCrypt.Decrypt(m.SectionId[i].ToString());
                                int secId1 = Convert.ToInt32(secId[i]);
                                subId[i] = QSCrypt.Decrypt(m.SubjectId[i].ToString());
                                int subId1 = Convert.ToInt32(subId[i]);
                                facultyId[i] = m.FacultyId[i];
                                int facultyId1 = Convert.ToInt32(facultyId[i]);
                                periodId[i] = QSCrypt.Decrypt(m.PeriodId[i].ToString());
                                int periodId1 = Convert.ToInt32(periodId[i]);
                                Substitude.updateSubstitude(yearId, classId1, secId1, subId1, periodId1, date, dayOrder, facultyId1, ApproverId, actualEmpRegId);
                            }
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdateSubstitudeEmployee"));
                            string Message = ResourceCache.Localize("SubstitudeUpdatedSuccessfully");
                            return Json(new { Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            for (int i = 0; i <= TotalCount; i++)
                            {
                                classId[i] = QSCrypt.Decrypt(m.ClassId[i].ToString());
                                int classId1 = Convert.ToInt32(classId[i]);
                                secId[i] = QSCrypt.Decrypt(m.SectionId[i].ToString());
                                int secId1 = Convert.ToInt32(secId[i]);
                                subId[i] = QSCrypt.Decrypt(m.SubjectId[i].ToString());
                                int subId1 = Convert.ToInt32(subId[i]);
                                facultyId[i] = m.FacultyId[i];
                                int facultyId1 = Convert.ToInt32(facultyId[i]);
                                periodId[i] = QSCrypt.Decrypt(m.PeriodId[i].ToString());
                                int periodId1 = Convert.ToInt32(periodId[i]);
                                Substitude.addSubstitude(yearId, classId1, secId1, subId1, periodId1, date, dayOrder, facultyId1, ApproverId, actualEmpRegId);
                            }
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Add_substitude_employee"));
                            string Message = ResourceCache.Localize("Substitude_saved_successfully");
                            return Json(new { Message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string InfoMessage = ResourceCache.Localize("Records_can_save_for_current_academic_year_only");
                        return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                    }           
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }          
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getDate(int acid)
        {
            try
            {
                AcademicyearServices obj_academicYear = new AcademicyearServices();
                var ans = obj_academicYear.AcademicYears(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                return Json(new { sd = sdate, ed = edate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult IsFacultyFree(int passEmployeeId, string pasDdayOrder, string passPeriodId, int facultyDropdownId, string passDate, int passYearId)
        {
            try
            {
                if (passEmployeeId != 0 && pasDdayOrder != null && passPeriodId != null && passDate != null)
                {
                    AssignSubjectToPeriodServices SubjectPeriod = new AssignSubjectToPeriodServices();
                    AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
                    FacultyDailyAttendanceServices facultyAttendance = new FacultyDailyAttendanceServices();

                    int dayId = Convert.ToInt32(pasDdayOrder);
                    string IsFacultyFree = "Yes";
                    int periodId = Convert.ToInt32(QSCrypt.Decrypt(passPeriodId));
                    DateTime Date1 = Convert.ToDateTime(passDate);

                    var CheckIsFacultyLeave = facultyAttendance.IsFacultyLeaveOnDate(passEmployeeId, Date1);
                    if (CheckIsFacultyLeave != null)
                    {
                        IsFacultyFree = "No";
                        string IsFacultyLeave = "Yes";
                        return Json(new { IsFacultyFree, facultyDropdownId, IsFacultyLeave }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var getEmployeesSub = facultySub.getEmployeesSub(passEmployeeId, passYearId);
                        for (int i = 0; i < getEmployeesSub.Count; i++)
                        {
                            int subId = getEmployeesSub[i].subId.Value;
                            int classId = getEmployeesSub[i].classId.Value;
                            int secId = getEmployeesSub[i].secId.Value;

                            var checkFacultyFree = SubjectPeriod.checkFacultyFreeInPeriod(passYearId, classId, secId, subId, dayId, periodId);
                            if (checkFacultyFree != null)
                            {
                                IsFacultyFree = "No";
                                return Json(new { IsFacultyFree, facultyDropdownId }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        if (IsFacultyFree == "Yes")
                        {
                            return Json(new { IsFacultyFree }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { IsFacultyFree }, JsonRequestBehavior.AllowGet);
                        }
                    }                                  
                }
                else
                {
                    var ErrorMessage = ResourceCache.Localize("WrongInput");
                    return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult CurrentActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");

            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    int acid = CurrentAcYear[i].AcademicYearId;
                    return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }

    }
}
