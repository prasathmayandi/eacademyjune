﻿using eAcademy.Areas.HostelManagement.Services;
using eAcademy.Areas.Transport.Services;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOut]
    public class AdmissionController : Controller
    {
        //
        // GET: /Admission/
        public static IList<AdminSelectionList> SelectionList = new List<AdminSelectionList>();
        public static IList<TransportList> Transportlist = new List<TransportList>();
        UserActivityHelper useractivity = new UserActivityHelper();
        //public int y;
        public int AdminId;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            Admission1Services As1 = new Admission1Services();
            DateTime currentdate = Convert.ToDateTime(DateTimeByZone.getCurrentDate());
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            ViewBag.academicyear = As.getvacancyAcademicYear();
            //ViewBag.Classes = Cs.GetClass();
            ViewBag.classess = Cs.GetClassvacauncy();
            ViewBag.vacancyclassess = Cs.GetClassvacauncy();
            AdminId = Convert.ToInt32(Session["empRegId"]);
        }
        private static void ExportPDF<TSource>(IList<TSource> customerList, string[] columns, string filePath)
        {
            iTextSharp.text.Font headerFont = FontFactory.GetFont("Verdana", 10, iTextSharp.text.BaseColor.WHITE);
            iTextSharp.text.Font rowfont = FontFactory.GetFont("Verdana", 10, iTextSharp.text.BaseColor.BLUE);
            iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.OpenOrCreate));
            document.Open();
            PdfPTable table = new PdfPTable(columns.Length);
            foreach (var column in columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column, headerFont));
                cell.BackgroundColor = BaseColor.BLACK;
                table.AddCell(cell);
            }
            foreach (var item in customerList)
            {
                foreach (var column in columns)
                {
                    string value = item.GetType().GetProperty(column).GetValue(item).ToString();
                    PdfPCell cell5 = new PdfPCell(new Phrase(value, rowfont));
                    table.AddCell(cell5);
                }
            }
            document.Add(table);
            document.Close();
        }
        public JsonResult JsonvacuncyClass(int yearid)
        {
            try
            {
                AsignClassVacancyServices Acs = new AsignClassVacancyServices();
                var ans = Acs.ShowVacancyClassList(yearid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        //public JsonResult getpreclass(int classid)
        //{
        //    try
        //    {
        //        AsignClassVacancyServices Acs = new AsignClassVacancyServices();
        //        var ans = Acs.Showpreclass(classid);
        //        return Json(ans, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(e.Message, JsonRequestBehavior.AllowGet);
        //    }
        //}
        public ActionResult Configuration()
        {
            try
            {
                Admission1Services As1 = new Admission1Services();
                AcademicyearServices As = new AcademicyearServices();
                ViewBag.academicyear = As.GetAcademicYear();
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
        }
        [HttpGet]
        public JsonResult GetActiveClass()
        {
            try
            {
                ClassServices cs = new ClassServices();
                var result = cs.GetClassIdandName();
                if (result.Count != 0)
                {
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("No_Class_are_Active");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ClassVacancySave(VM_ClassVacancy s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AsignClassVacancyServices acs = new AsignClassVacancyServices();
                    var count = s.count;
                    string[] classid = new string[count];
                    string[] vacancy = new string[count];
                    int year = Convert.ToInt16(s.AcademicYearId);
                    if (count != 0)
                    {
                        for (int i = 0; i < count; i++)
                        {
                            classid[i] = String.Join(" ", s.clssid[i]);
                            vacancy[i] = String.Join(" ", s.vacancy[i]);
                            int cid = Convert.ToInt16(classid[i]);
                            int vacun = Convert.ToInt16(vacancy[i]);
                            acs.SaveClassVacancy(year, cid, vacun);
                        }
                    }
                    string Message = eAcademy.Models.ResourceCache.Localize("Sucessfully_Class_Vacancy_are_Added");
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("AddClassvacancy"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_Required_Files");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetEditVacancyClass(VM_SelectionProcess s)
        {
            try
            {
                AsignClassVacancyServices acs = new AsignClassVacancyServices();
                AcademicyearServices ayear = new AcademicyearServices();
                var result = acs.GetClassVacancy(s.AcademicYear);
                bool pastAcademicyear = ayear.PastAcademicyear(s.AcademicYear);
                if (result.Count != 0)
                {
                    return Json(new { result, pastAcademicyear }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Records_are_not_Available_So_Refresh_page_and__first_Create_Vacancy_for_class");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ClassVacancyUpdate(VM_ClassVacancy s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AsignClassVacancyServices acs = new AsignClassVacancyServices();
                    var count = s.count;
                    string[] classid = new string[count];
                    string[] vacancy = new string[count];
                    string[] status = new string[count];
                    //string[] agelimitdate = new string[count];
                    int year = Convert.ToInt16(s.AcademicYearId);
                    if (count != 0)
                    {
                        for (int i = 0; i < count; i++)
                        {
                            DateTime? date = null;
                            classid[i] = String.Join(" ", s.clssid[i]);
                            vacancy[i] = String.Join(" ", s.vacancy[i]);
                            status[i] = String.Join(" ", s.Status[i]);
                            int cid = Convert.ToInt16(classid[i]);
                            int vacun = Convert.ToInt16(vacancy[i]);
                            bool ss = Convert.ToBoolean(status[i]);
                            if (s.AgeLimitDate[i] != "")
                            {
                                date = DateTime.Parse(s.AgeLimitDate[i]);
                                //DateTime.ParseExact(s.AgeLimitDate[i], "dd/MM/yyyy", null);
                            }
                            else
                            {
                                date = null;
                            }
                            var checkexistclassvacuncy = acs.checkexistvacuncy(year, cid);
                            if (checkexistclassvacuncy <= vacun)
                            {
                                acs.UpdateClassVacancy(year, cid, vacun, ss, date);
                            }
                            else
                            {
                                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                               // int FeePaidStudent = PstudentRegister.getFeePaidstudents(year, cid);
                                int Acceptedstudent = PstudentRegister.getcAcceptedstudents(year, cid);
                                int FeePaidStudent = PstudentRegister.getFeePaidstudents(year, cid);
                                int totalvacuncyfillstudent = Acceptedstudent + FeePaidStudent;
                                if (totalvacuncyfillstudent == vacun)
                                {
                                    acs.UpdateClassVacancy(year, cid, vacun, ss, date);
                                }
                                else if(totalvacuncyfillstudent > vacun)
                                {
                                    ClassServices clsobj=new ClassServices();
                                    var classname = clsobj.getClassNameByClassID(cid);

                                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Cannot_decrease_vacuncy_Already_FeePaid_student_is") + " " +totalvacuncyfillstudent+ " " +"in" +" "+classname.ClassType;
                                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                              
                                //int decreaevacuncy = Convert.ToInt32(checkexistclassvacuncy) - FeePaidStudent;
                                //if (decreaevacuncy == vacun || decreaevacuncy >= vacun)
                                //{
                                //    acs.UpdateClassVacancy(year, cid, vacun, ss, date);
                                //}
                            }
                        }
                    }
                    string Message = eAcademy.Models.ResourceCache.Localize("Sucessfully_Class_Vacancy_are_Updated");
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("UpdateClassvacancy"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_Required_Files");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SelectionProcess(bool? pdf)
        {
            try
            {
                if (!pdf.HasValue)
                {
                    ClassServices Cs = new ClassServices();
                    ViewBag.classess = Cs.GetClassvacauncy();
                    return View();
                }
                else
                {
                    string filePath = Server.MapPath("~/Views/") + "Sample.pdf";
                    ExportPDF(SelectionList, new string[] { "OnlineRegisterId", "PrimaryUser", "StuFirstName", "AdmissionClass", "Distance", "NoOfSibling", "WorkSameSchool", "statusFlag" }, filePath);//
                    return File(filePath, "application/pdf", "SelectionList.pdf");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
        }
        //jqrid for SelectionProcess
        [HttpGet]
        public JsonResult JQgridSelectionProcess(string sidx, string sord, int page, int rows, string Sort, string FilterDistance, string NoOfSbling, string WorkSameSchool, string StatusFlag, int? Admission_AcademicYear, int? Admission_Class) //, int AcademicYear, int Class, string Distance, string NoOfSbling, string WorkSameSchool, string StatusFlag)
        {
            IList<AdminSelectionList> SelectionList = new List<AdminSelectionList>();
            PreAdminServices ps = new PreAdminServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (Admission_AcademicYear != null && Admission_Class != null && FilterDistance == "Select" && NoOfSbling == "Select" && WorkSameSchool == "Select" && StatusFlag == "Select" && Sort == "All")
            {
                SelectionList = ps.GetAllList(Admission_AcademicYear, Admission_Class);
            }
            else if (Admission_AcademicYear != null && Admission_Class != null && FilterDistance != "Select")
            {
                SelectionList = ps.GetDistanceList(Admission_AcademicYear, Admission_Class, Convert.ToInt32(FilterDistance));
            }
            else if (Admission_AcademicYear != null && Admission_Class != null && NoOfSbling != "Select")
            {
                SelectionList = ps.GetSiblingList(Admission_AcademicYear, Admission_Class, NoOfSbling);
            }
            else if (Admission_AcademicYear != null && Admission_Class != null && WorkSameSchool != "Select")
            {
                SelectionList = ps.GetWorkSameSchoolList(Admission_AcademicYear, Admission_Class, WorkSameSchool);
            }
            else if (Admission_AcademicYear != null && Admission_Class != null && StatusFlag != "Select")
            {
                if (StatusFlag == "InterView" || StatusFlag == "InterView&Entrance")
                {
                    SelectionList = ps.GetInterViewStatusflagList(Admission_AcademicYear, Admission_Class, StatusFlag);
                }
                else if (StatusFlag == "NotFeePaid")
                {
                    SelectionList = ps.GetNotFeePaidList(Admission_AcademicYear, Admission_Class, StatusFlag);
                }
                else
                {
                    SelectionList = ps.GetParticularStatusflagList(Admission_AcademicYear, Admission_Class, StatusFlag);
                }
            }
            else if (Admission_AcademicYear != null && Admission_Class != null && Sort == "Pending")
            {
                SelectionList = ps.GetPendingList(Admission_AcademicYear, Admission_Class, Sort);
            }
            int totalRecords = SelectionList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    SelectionList = SelectionList.OrderByDescending(s => s.PrimaryUserEmail).ToList();
                    SelectionList = SelectionList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    SelectionList = SelectionList.OrderBy(s => s.PrimaryUserEmail).ToList();
                    SelectionList = SelectionList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = SelectionList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = SelectionList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SelectionProcess_ExportToExcel()
        {
            try
            {
                List<AdminSelectionList> StudentList1 = (List<AdminSelectionList>)Session["JQGridList"];
                var list = StudentList1.Select(o => new { StudentAdmissionId = o.StudentAdmissionId, PrimaryUserName = o.PrimaryUserName, StuFirstName = o.StuFirstName, ApplyClass = o.txt_AdmissionClass, Distance = o.Distance, SiblingStatus = o.SiblingStatus, Parentworkingsameschool = o.WorkSameSchool, ApplicationStatus = o.statusFlag, ApplicationSource = o.ApplicationSource }).ToList();//, subjectid = o.subjectid
                string fileName = eAcademy.Models.ResourceCache.Localize("EnrolledStudentReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("SelectionProcess");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult SelectionProcess_ExportToPdf()
        {
            try
            {
                List<AdminSelectionList> StudentList1 = (List<AdminSelectionList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("EnrolledStudentReport");
                GeneratePDF.ExportPDF_Landscape(StudentList1, new string[] { "StudentAdmissionId", "PrimaryUserName", "StudentName", "ApplyClass", "Distance", "SiblingStatus", "WorkSameSchool", "statusFlag", "ApplicationSource" }, xfilePath, fileName);//, "subjectid"
                return File(xfilePath, "application/pdf", "SelectionProcess.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        //json for Selection Process
        [HttpPost]
        public JsonResult GetSelectionProcess(VM_SelectionProcess d)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string Message = "";
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult StudentBioData(string id)
        {
            int Regid = Convert.ToInt16(QSCrypt.Decrypt(id));
            PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
            PreAdmissionTransactionServices Ptrans = new PreAdmissionTransactionServices();
            BoardofSchoolServices board = new BoardofSchoolServices();
            ClassServices Cs = new ClassServices();
            SchoolSettingsServices ss = new SchoolSettingsServices();
            TransportDestinationServices destination = new TransportDestinationServices();
            TransportPickPointServices pickpoint = new TransportPickPointServices();
            HostelConfigSubCategoriesServices hostelsubcategory = new HostelConfigSubCategoriesServices();
            var ans = ss.getSchoolSettings();
            ViewBag.Email = ans.FirstOrDefault().EmailRequired;
            ViewBag.Sms = ans.FirstOrDefault().SmsRequired;
            ViewBag.App = ans.FirstOrDefault().AppRequired;

            var trans = Ptrans.FindPrimaryUserId(Regid);
            var result = PstudentRegister.GetStudentAlldetails(Convert.ToInt16(trans.PrimaryUserAdmissionId), Convert.ToInt16(trans.StudentAdmissionId));
            if (result.FirstOrDefault().TransportDestination != null && result.FirstOrDefault().TransportPickpoint != null)
            {
                int destinationid = Convert.ToInt16(result.FirstOrDefault().TransportDestination);
                int PickPointId = Convert.ToInt16(result.FirstOrDefault().TransportPickpoint);
                ViewBag.TransportDestination = destination.getDestination(destinationid).DestinationName;
                ViewBag.TransportPickpoint = pickpoint.GetPickPoint(destinationid, PickPointId);
            }
            if (result.FirstOrDefault().TransportDestination != null && result.FirstOrDefault().TransportPickpoint != null)
            {
                int destinationid = Convert.ToInt16(result.FirstOrDefault().TransportDestination);
                int PickPointId = Convert.ToInt16(result.FirstOrDefault().TransportPickpoint);
                ViewBag.TransportDestination = destination.getDestination(destinationid).DestinationName;
                ViewBag.TransportPickpoint = pickpoint.GetPickPoint(destinationid, PickPointId);
            }
            if (result.FirstOrDefault().FoodFeeCategoryId != null)
            {
                int foodFeesId = Convert.ToInt16(result.FirstOrDefault().FoodFeeCategoryId);
                int AccomFeesid = Convert.ToInt16(result.FirstOrDefault().AccommodationFeeCategoryId);
                if (result.FirstOrDefault().FoodSubFeeCategoryId != null)
                {
                    int foodsubFeesId = Convert.ToInt16(result.FirstOrDefault().FoodSubFeeCategoryId);
                    ViewBag.FoodSubCategory = hostelsubcategory.GetSubCategory(foodFeesId, foodsubFeesId);
                }
                if (result.FirstOrDefault().AccommodationSubFeeCategoryId != null)
                {
                    int AccomsubFeesId = Convert.ToInt16(result.FirstOrDefault().AccommodationSubFeeCategoryId);
                    ViewBag.AccomSubCategory = hostelsubcategory.GetSubCategory(AccomFeesid, AccomsubFeesId);
                }
            }
            if (result.FirstOrDefault().PreClass != null)
            {
                ViewBag.previousclass_text = Cs.GetParticularClass(Convert.ToInt32(result.FirstOrDefault().PreClass));
            }
            if (result.FirstOrDefault().Pre_BoardofSchool != null)
            {
                ViewBag.previousBoardtxt = board.Showpreviousmedium(Convert.ToInt32(result.FirstOrDefault().Pre_BoardofSchool));
            }
            ViewBag.regid = Convert.ToInt16(trans.StudentAdmissionId);
            return View(result);
        }
        [HttpPost]
        public JsonResult UpdateStatusFlag(VM_UpdateApplicationStatus d)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionRegisterStatusFlagServices rss = new PreAdmissionRegisterStatusFlagServices();
                    PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                    AsignClassVacancyServices acs = new AsignClassVacancyServices();
                    int regid = d.Stuadmissionid;
                    string statusflag = d.StatusFlag;
                    string description = d.Descriptions;
                    string flag = "Conditionaloffer";
                    string Message = "";
                    DateTime? date = null;
                    if (d.FeePaidLastDate != null)
                    {
                        date = DateTime.Parse(d.FeePaidLastDate);
                    }
                    else
                    {
                        date = null;
                    }

                    if (statusflag == flag)
                    {
                        int vacancycount = Convert.ToInt16(acs.getParticularClassVacancy(Convert.ToInt16(d.AcademicYearId), Convert.ToInt16(d.Classid)));
                        var Account = Pstudent.GetParticularStatusFlagCount(Convert.ToInt16(d.AcademicYearId), Convert.ToInt16(d.Classid), statusflag);
                        int AcceptCount = Account.Count;
                        int count = vacancycount - AcceptCount;
                        if (count != 0 && count >= 1)
                        {
                            rss.AddRegisterStatusFlag(regid, statusflag, description);
                            bool result = Pstudent.UpdateStudentRegisterStatusSelectionProcess(regid, statusflag, date);
                            if (result == true)
                            {
                                Message = eAcademy.Models.ResourceCache.Localize("Application_status_updated");
                                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_Enrollement_application_status"));
                            }
                        }
                        else
                        {
                            Message = eAcademy.Models.ResourceCache.Localize("Conditional_Offer_limit_is_Over.Vacancy_Limit_Is_Over,So_set_statusflag_is_pending");
                        }
                    }
                    else if (statusflag != flag)
                    {
                        rss.AddRegisterStatusFlag(regid, statusflag, description);
                        bool result = Pstudent.UpdateStudentRegisterStatusSelectionProcess(regid, statusflag, date);
                        if (result == true)
                        {
                            Message = eAcademy.Models.ResourceCache.Localize("Application_status_updated");
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_Enrollement_application_status"));
                        }
                    }
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult SendEmailFlag(VM_SendEmailFlag f)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                    StudentRegisterServices srs = new StudentRegisterServices();
                    PreAdmissionRegisterStatusFlagServices rss = new PreAdmissionRegisterStatusFlagServices();
                    int count = Convert.ToInt16(f.SelectCount);
                    string[] regid = new string[count];
                    string Message = "";
                    string ErrorMessage = "";
                    int flag = 0;
                    for (int i = 0; i < count; i++)
                    {
                        regid[i] = String.Join(" ", f.RegId[i]);
                        int id = Convert.ToInt16(QSCrypt.Decrypt(regid[i]));
                        var r = Pstudent.GetPrimaryUserMaildetails(id);
                        var des = rss.GetDescription(id, r.statusFlag);
                        int Ayid = Convert.ToInt16(r.AcademicyearId);
                        int cid = Convert.ToInt16(r.ClassId);
                        string description = Convert.ToString(des);
                        DateTime? date = null;
                        if (f.FeePaidLastDate != null)
                        {
                            date = DateTime.Parse(f.FeePaidLastDate);
                        }
                        else
                        {
                            date = null;
                        }
                        bool result = ViaEmail(id, r.PrimaryUserName, r.PrimaryUserEmail, r.txt_AcademicYear, r.txt_AdmissionClass, Ayid, cid, r.statusFlag, description, r.StuFirstName, date);
                        if (result == true)
                        {
                            flag++;
                            Message = "Success";
                        }
                        else
                        {
                            flag--;
                            ErrorMessage = eAcademy.Models.ResourceCache.Localize("Failed_to_email_send_for") + "=" + r.PrimaryUserEmail;
                        }
                    }
                    if (flag == count)
                    {
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Send_Email_to_Enrollement_application"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Offered(VM_SendEmailFlag f)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                    StudentRegisterServices srs = new StudentRegisterServices();
                    PreAdmissionRegisterStatusFlagServices rss = new PreAdmissionRegisterStatusFlagServices();
                    AsignClassVacancyServices acs = new AsignClassVacancyServices();
                    int count = Convert.ToInt16(f.SelectCount);
                    string[] regid = new string[count];
                    string Message = "";
                    string ErrorMessage = "";
                    int flag = 0;
                    for (int i = 0; i < count; i++)
                    {
                        regid[i] = String.Join(" ", f.RegId[i]);
                        int id = Convert.ToInt16(QSCrypt.Decrypt(regid[i]));
                        var r = Pstudent.GetPrimaryUserMaildetails(id);
                        var des = rss.GetDescription(id, r.statusFlag);
                        int Ayid = Convert.ToInt16(r.AcademicyearId);
                        int cid = Convert.ToInt16(r.ClassId);
                        string description = f.description;
                        string statusflag = "Conditionaloffer";
                        DateTime? date = null;
                        if (f.FeePaidLastDate != null)
                        {
                            date = DateTime.Parse(f.FeePaidLastDate);
                        }
                        else
                        {
                            date = null;
                        }
                        int vacancycount = Convert.ToInt16(acs.getParticularClassVacancy(Convert.ToInt16(Ayid), Convert.ToInt16(cid)));
                        var Account = Pstudent.GetParticularStatusFlagCount(Convert.ToInt16(Ayid), Convert.ToInt16(cid), statusflag);
                        var Feespaid = Pstudent.GetParticularStatusFlagCount(Convert.ToInt16(Ayid), Convert.ToInt16(cid), "FeesPaid");
                        var Accept = Pstudent.GetParticularStatusFlagCount(Convert.ToInt16(Ayid), Convert.ToInt16(cid), "Accepted");
                        int FeesCount = Feespaid.Count;
                        int CurrentStatusCount = Account.Count;
                        int AcceptCount = Accept.Count;
                        int count1 = vacancycount - CurrentStatusCount - AcceptCount - FeesCount;
                        if (count1 >= 1)
                        {
                            bool result = ViaEmail(id, r.PrimaryUserName, r.PrimaryUserEmail, r.txt_AcademicYear, r.txt_AdmissionClass, Ayid, cid, statusflag, description, r.StuFirstName, date);
                            if (result == true)
                            {
                                flag++;
                                Message = "Success";
                                rss.AddRegisterStatusFlag(id, statusflag, description);
                                bool result1 = Pstudent.UpdateStudentRegisterStatusSelectionProcess(id, statusflag, date);
                                if (result1 == true)
                                {
                                    Message = eAcademy.Models.ResourceCache.Localize("Application_status_updated_and_email_send_successfully");
                                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_Enrollement_application_status"));
                                }
                            }
                            else
                            {
                                flag--;
                                ErrorMessage = eAcademy.Models.ResourceCache.Localize("Failed_to_email_send_for") + "=" + r.PrimaryUserEmail;
                            }
                        }
                        else
                        {
                            Message = eAcademy.Models.ResourceCache.Localize("Conditional_Offer_limit_is_Over.Vacancy_Limit_Is_Over,So_set_statusflag_is_pending");
                        }
                    }
                    if (flag == count)
                    {
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Send_Email_to_Enrollement_application"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public bool ViaEmail(int id, string PrimaryUserName, string primaryuseremail, string txt_Ayear, string txt_Applyclass, int academicyearid, int classid, string statusflag, string description, string sname, DateTime? ldate)
        {
            try
            {
                PreAdminServices ps = new PreAdminServices();
                PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                SchoolSettingServices schoolsetting = new SchoolSettingServices();
                PreAdmissionPrimaryUserRegisterServices primary = new PreAdmissionPrimaryUserRegisterServices();
                var list = primary.GetPrimaryuserdetails(primaryuseremail);
                int regid = id;
                var Fname = list.PrimaryUserName;
                var Femail = primaryuseremail;
                var AppNo = id;
                var Description = description;
                var StuName = sname;
                var txt_class = txt_Applyclass;
                int Acyear = academicyearid;
                var classId = Convert.ToInt16(classid);
                int flag = 0;
                var school = schoolsetting.getSchoolSettings();
                var SchoolName = school.SchoolName;
                var SchoolLogo = school.SchoolLogo;
                string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                if (statusflag == "Pending")
                {
                    StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/PendingEmail.html"));
                    string readFile2 = reader2.ReadToEnd();
                    string myString2 = "";
                    myString2 = readFile2;
                    myString2 = myString2.Replace("$$surname$$", Fname.ToString());
                    myString2 = myString2.Replace("$$appno$$", AppNo.ToString());
                    myString2 = myString2.Replace("$$description$$", Description.ToString());
                    myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
                    myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
                    myString2 = myString2.Replace("$$StudentName$$", sname.ToString());
                    myString2 = myString2.Replace("$$ApplyClass$$", txt_Applyclass.ToString());
                    myString2 = myString2.Replace("$$AcdemicYear$$", txt_Ayear.ToString());
                    string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];

                    string PlaceName = "Admission-ViaEmail-Sent application status";
                    string UserName = "Employee";
                    string UserId = Convert.ToString(Session["username"]);
                    Mail.SendMultipleMail(EmailDisplay, supportEmail, Femail, "Account Credentials", myString2.ToString(), true, PlaceName, UserName, UserId);  

                    flag = 1;
                    Pstudent.UpdateSendEmailStatus(regid);
                }
                else if (statusflag == "AddMore")
                {
                    StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/AddMoreDetailsEmail.html"));
                    string readFile2 = reader2.ReadToEnd();
                    string myString2 = "";
                    myString2 = readFile2;
                    myString2 = myString2.Replace("$$surname$$", Fname.ToString());
                    myString2 = myString2.Replace("$$appno$$", AppNo.ToString());
                    myString2 = myString2.Replace("$$description$$", Description.ToString());
                    myString2 = myString2.Replace("$$stuname$$", StuName.ToString());
                    myString2 = myString2.Replace("$$applyclass$$", txt_class.ToString());
                    myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
                    myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());

                    string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                    string PlaceName = "AdmissionController-Sending Application Status";
                    string UserName = "Employee";
                    string UserId = Convert.ToString(Session["username"]);
                    Mail.SendMultipleMail(EmailDisplay, supportEmail, Femail, "Account Credentials", myString2.ToString(), true, PlaceName, UserName, UserId); 
                   
                    Pstudent.UpdateSendEmailStatus(regid);
                    flag = 1;
                }
                else if (statusflag == "Reject")
                {
                    StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/RejectEmail.html"));
                    string readFile2 = reader2.ReadToEnd();
                    string myString2 = "";
                    myString2 = readFile2;
                    myString2 = myString2.Replace("$$surname$$", Fname.ToString());
                    myString2 = myString2.Replace("$$appno$$", AppNo.ToString());
                    myString2 = myString2.Replace("$$description$$", Description.ToString());
                    myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
                    myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
                    myString2 = myString2.Replace("$$StudentName$$", sname.ToString());
                    myString2 = myString2.Replace("$$ApplyClass$$", txt_Applyclass.ToString());
                    myString2 = myString2.Replace("$$AcdemicYear$$", txt_Ayear.ToString());
                    string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                     string PlaceName = "AdmissionController-Sending Application Status";
                    string UserName = "Employee";
                    string UserId = Convert.ToString(Session["username"]);
                    Mail.SendMultipleMail(EmailDisplay, supportEmail, Femail, "Account Credentials", myString2.ToString(), true, PlaceName, UserName, UserId); 
                   
                    flag = 1;
                    Pstudent.UpdateSendEmailStatus(regid);
                }
                else if (statusflag == "InterView")
                {
                    StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/InterViewEmail.html"));
                    string readFile2 = reader2.ReadToEnd();
                    string myString2 = "";
                    myString2 = readFile2;
                    myString2 = myString2.Replace("$$surname$$", Fname.ToString());
                    myString2 = myString2.Replace("$$appno$$", AppNo.ToString());
                    myString2 = myString2.Replace("$$description$$", Description.ToString());
                    myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
                    myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
                    myString2 = myString2.Replace("$$StudentName$$", sname.ToString());
                    myString2 = myString2.Replace("$$ApplyClass$$", txt_Applyclass.ToString());
                    myString2 = myString2.Replace("$$AcdemicYear$$", txt_Ayear.ToString());
                    string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                     string PlaceName = "AdmissionController-Sending Application Status";
                    string UserName = "Employee";
                    string UserId = Convert.ToString(Session["username"]);
                    Mail.SendMultipleMail(EmailDisplay, supportEmail, Femail, "Account Credentials", myString2.ToString(), true, PlaceName, UserName, UserId); 
                   
                    flag = 1;
                    Pstudent.UpdateSendEmailStatus(regid);
                }
                else if (statusflag == "InterView&Entrance")
                {
                    StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/InterViewEntranceEmail.html"));
                    string readFile2 = reader2.ReadToEnd();
                    string myString2 = "";
                    myString2 = readFile2;
                    myString2 = myString2.Replace("$$surname$$", Fname.ToString());
                    myString2 = myString2.Replace("$$appno$$", AppNo.ToString());
                    myString2 = myString2.Replace("$$description$$", Description.ToString());
                    myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
                    myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
                    myString2 = myString2.Replace("$$StudentName$$", sname.ToString());
                    myString2 = myString2.Replace("$$ApplyClass$$", txt_Applyclass.ToString());
                    myString2 = myString2.Replace("$$AcdemicYear$$", txt_Ayear.ToString());
                    string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                     string PlaceName = "AdmissionController-Sending Application Status";
                    string UserName = "Employee";
                    string UserId = Convert.ToString(Session["username"]);
                    Mail.SendMultipleMail(EmailDisplay, supportEmail, Femail, "Account Credentials", myString2.ToString(), true, PlaceName, UserName, UserId); 
                   
                    flag = 1;
                    Pstudent.UpdateSendEmailStatus(regid);
                }
                else if (statusflag == "Conditionaloffer")
                {
                    var result = ps.getallFeesByID(Acyear, classId);
                    var feeid = 0;
                    var lastdate = ldate.Value.ToString("dd/MM/yyyy");
                    string item_body = "";
                    string item_body2 = "";
                    var transportFee = "";
                    var hostelfee = "";
                    if (result.Count != 0)
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            if (result[i].TermId == 0)
                            {
                                string Sub_body = "";
                                feeid = result[i].FeeId;
                                for (int j = i; j < result.Count; j++)
                                {
                                    if (feeid == result[j].FeeId && result[j].TermId != 0)
                                    {
                                        Sub_body = Sub_body + "<tr class='tr-content' style='text-align: center;FONT-SIZE: 13px;'><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" +
                                            result[j].PaymentType + "</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" +
                                            result[j].Amount + "</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" +
                                            result[j].ServicesTax + "</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" +
                                            result[j].Total + "</td></tr>";
                                    }
                                }
                                item_body = item_body + "<tr class='tr-content' style='text-align: center;FONT-SIZE:13px;'><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" +
                                   result[i].FeeCategory + "<table class='table table-bordered table-hover table-striped' style='width: 100%;max-width: 100%;margin-bottom: 15px;padding-left: 15px;padding-right: 15px;display: table;border-collapse: separate;border-spacing: 8px;border-color: gray;background-color: transparent;'><tbody> " +
                                  Sub_body + " </tbody></table></td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" + result[i].Amount
                                    + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" + result[i].ServicesTax +
                                    "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" + result[i].Total
                                    + "</td></tr>";
                            }
                        }
                        if (result.Count != 0)
                        {
                            var tid = ps.GetAcademictermId(academicyearid);
                            if (tid.Count == 0)
                            {
                                for (int i = 0; i < result.Count; i++)
                                {
                                    if (result[i].TermId == 0)
                                    {
                                        string Sub_body = "";
                                        feeid = result[i].FeeId;
                                        for (int j = i; j < result.Count; j++)
                                        {
                                            if (feeid == result[j].FeeId && result[j].TermId != 0)
                                            {
                                                Sub_body = Sub_body + "<tr class='tr-content' style='text-align: center;FONT-SIZE: 13px;'><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" +
                                                    result[j].PaymentType + "</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" +
                                                    result[j].Amount + "</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" +
                                                    result[j].ServicesTax + "</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" +
                                                    result[j].Total + "</td> </tr>";
                                            }
                                        }
                                        item_body2 = item_body2 + "<tr class='tr-content' style='text-align: center;FONT-SIZE:13px;'><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" +
                                           result[i].FeeCategory + "<table class='table table-bordered table-hover table-striped' style='width: 100%;max-width: 100%;margin-bottom: 15px;padding-left: 15px;padding-right: 15px;display: table;border-collapse: separate;border-spacing: 8px;border-color: gray;background-color: transparent;'><tbody> " +
                                      Sub_body + " </tbody></table></td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" + result[i].Amount
                                            + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" + result[i].ServicesTax +
                                            "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #AAA;'>" + result[i].Total
                                            + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" + lastdate + "</td></tr>";
                                    }
                                }
                            }
                            else
                            {
                                var termid = tid.FirstOrDefault().TermId;
                                for (int i = 0; i < result.Count; i++)
                                {
                                    if (result[i].TermId == 0)
                                    {
                                        string Sub_body2 = "";
                                        feeid = result[i].FeeId;
                                        bool s = false;
                                        for (int j = i; j < result.Count; j++)
                                        {
                                            if (feeid == result[j].FeeId && result[j].TermId == termid)
                                            {
                                                s = true;
                                                Sub_body2 = Sub_body2 + "<tr class='tr-content' style='text-align: center;FONT-SIZE: 13px;'><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                                    result[j].FeeCategory + " " + result[j].PaymentType + "</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                                    result[j].Amount + "</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                                    result[j].ServicesTax + "</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                                    result[j].Total + "</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                                    lastdate + "</td></tr>";
                                            }
                                        }
                                        if (s == false)
                                        {
                                            item_body2 = item_body2 + "<tr class='tr-content' style='text-align: center;FONT-SIZE:13px;'><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                            result[i].FeeCategory + "<table class='table table-bordered table-hover table-striped' style='width: 100%;max-width: 100%;margin-bottom: 15px;padding-left: 15px;padding-right: 15px;display: table;border-collapse: separate;border-spacing: 8px;border-color: gray;background-color: transparent;'><tbody> " +
                                            Sub_body2 + " </tbody></table></td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" + result[i].Amount
                                             + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" + result[i].ServicesTax +
                                             "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" + result[i].Total
                                             + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" + lastdate + "</td></tr>";
                                        }
                                        else if (s == true)
                                        {
                                            item_body2 = item_body2 + Sub_body2;
                                        }
                                    }
                                }
                            }
                        }
                        var getstudentdetails = Pstudent.StudentDetails(id);
                        //StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/ConditionalOfferEmail.html"));
                        //string readFile2 = reader2.ReadToEnd();
                        //string myString2 = "";
                        //myString2 = readFile2;
                        //myString2 = myString2.Replace("$$surname$$", Fname.ToString());
                        //myString2 = myString2.Replace("$$appno$$", AppNo.ToString());
                        //myString2 = myString2.Replace("$$stuname$$", StuName.ToString());
                        //myString2 = myString2.Replace("$$applyclass$$", txt_class.ToString());
                        //myString2 = myString2.Replace("$$AcdemicYear$$", txt_Ayear.ToString());
                        //myString2 = myString2.Replace("$$tables1$$", item_body.ToString());
                        //myString2 = myString2.Replace("$$tables2$$", item_body2.ToString());
                        //myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
                        //myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
                        //myString2 = myString2.Replace("$$LastDate$$", lastdate.ToString());
                        //string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                        if (getstudentdetails.TransportRequired == true && getstudentdetails.HostelRequired == false)
                        {
                            StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/ConditionalOfferTransport.html"));
                            string readFile2 = reader2.ReadToEnd();
                            string myString2 = "";
                            myString2 = readFile2;
                            myString2 = myString2.Replace("$$surname$$", Fname.ToString());
                            myString2 = myString2.Replace("$$appno$$", AppNo.ToString());
                            myString2 = myString2.Replace("$$stuname$$", StuName.ToString());
                            myString2 = myString2.Replace("$$applyclass$$", txt_class.ToString());
                            myString2 = myString2.Replace("$$AcdemicYear$$", txt_Ayear.ToString());
                            myString2 = myString2.Replace("$$tables1$$", item_body.ToString());
                            myString2 = myString2.Replace("$$tables2$$", item_body2.ToString());
                            myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
                            myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
                            myString2 = myString2.Replace("$$LastDate$$", lastdate.ToString());
                            string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                            TransportPickPointServices Tp = new TransportPickPointServices();
                            var getTransportfeeid = Tp.gettransfortfeestructure(getstudentdetails.TransportDesignation, getstudentdetails.TransportPickpoint);
                            transportFee = transportFee + "<td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                            getTransportfeeid.DestinationName + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" + getTransportfeeid.Pickpoint +
                            "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                            getTransportfeeid.TransportFee + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                            lastdate + "</td></tr>";
                            myString2 = myString2.Replace("$$TransportFee$$", transportFee.ToString());
                            EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                            string PlaceName = "Admission-ViaEmail-Sent fess structure";
                            string UserName = "Employee";
                            string UserId = Convert.ToString(Session["username"]);
                            Mail.SendMultipleMail(EmailDisplay, supportEmail, Femail, "Account Credentials", myString2.ToString(), true, PlaceName, UserName, UserId); 
                         
                            Pstudent.UpdateSendEmailStatus(regid);
                            flag = 1;

                        }
                        if (getstudentdetails.HostelRequired == true && getstudentdetails.TransportRequired == false)
                        {
                            StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/ConditionalOfferHostel.html"));
                            string readFile2 = reader2.ReadToEnd();
                            string myString2 = "";
                            myString2 = readFile2;
                            myString2 = myString2.Replace("$$surname$$", Fname.ToString());
                            myString2 = myString2.Replace("$$appno$$", AppNo.ToString());
                            myString2 = myString2.Replace("$$stuname$$", StuName.ToString());
                            myString2 = myString2.Replace("$$applyclass$$", txt_class.ToString());
                            myString2 = myString2.Replace("$$AcdemicYear$$", txt_Ayear.ToString());
                            myString2 = myString2.Replace("$$tables1$$", item_body.ToString());
                            myString2 = myString2.Replace("$$tables2$$", item_body2.ToString());
                            myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
                            myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
                            myString2 = myString2.Replace("$$LastDate$$", lastdate.ToString());
                            string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                            HostelFeesCategoryServices HP = new HostelFeesCategoryServices();
                            var gethostelfee = HP.getHostelfee(getstudentdetails.AcademicyearId, getstudentdetails.AccommodationFeeCategoryId, getstudentdetails.AccommodationSubFeeCategoryId, getstudentdetails.FoodFeeCategoryId, getstudentdetails.FoodSubFeeCategoryId);
                            for (int i = 0; i < gethostelfee.Count; i++)
                            {
                                hostelfee = hostelfee + "<tr><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                   gethostelfee[i].CategoryName + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                  gethostelfee[i].SubCategoryName + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                  gethostelfee[i].hostelfee + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                  lastdate + "</td><tr>";
                            }
                            myString2 = myString2.Replace("$$hostelfee$$", hostelfee.ToString());
                            EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                            string PlaceName = "AdmissionController-Sending Fess Structure";
                            string UserName = "Employee";
                            string UserId = Convert.ToString(Session["username"]);
                            Mail.SendMultipleMail(EmailDisplay, supportEmail, Femail, "Account Credentials", myString2.ToString(), true, PlaceName, UserName, UserId); 
                         
                           
                            Pstudent.UpdateSendEmailStatus(regid);
                            flag = 1;
                        }
                        if (getstudentdetails.HostelRequired == true && getstudentdetails.TransportRequired == true)
                        {
                            StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/ConditionalOffer.html"));
                            string readFile2 = reader2.ReadToEnd();
                            string myString2 = "";
                            myString2 = readFile2;
                            myString2 = myString2.Replace("$$surname$$", Fname.ToString());
                            myString2 = myString2.Replace("$$appno$$", AppNo.ToString());
                            myString2 = myString2.Replace("$$stuname$$", StuName.ToString());
                            myString2 = myString2.Replace("$$applyclass$$", txt_class.ToString());
                            myString2 = myString2.Replace("$$AcdemicYear$$", txt_Ayear.ToString());
                            myString2 = myString2.Replace("$$tables1$$", item_body.ToString());
                            myString2 = myString2.Replace("$$tables2$$", item_body2.ToString());
                            myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
                            myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
                            myString2 = myString2.Replace("$$LastDate$$", lastdate.ToString());
                            string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                            TransportPickPointServices Tp = new TransportPickPointServices();
                            var getTransportfeeid = Tp.gettransfortfeestructure(getstudentdetails.TransportDesignation, getstudentdetails.TransportPickpoint);
                            transportFee = transportFee + "<td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                            getTransportfeeid.DestinationName + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" + getTransportfeeid.Pickpoint +
                            "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                            getTransportfeeid.TransportFee + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                            lastdate + "</td></tr>";
                            myString2 = myString2.Replace("$$TransportFee$$", transportFee.ToString());
                            HostelFeesCategoryServices HP = new HostelFeesCategoryServices();
                            var gethostelfee = HP.getHostelfee(getstudentdetails.AcademicyearId, getstudentdetails.AccommodationFeeCategoryId, getstudentdetails.AccommodationSubFeeCategoryId, getstudentdetails.FoodFeeCategoryId, getstudentdetails.FoodSubFeeCategoryId);
                            for (int i = 0; i < gethostelfee.Count; i++)
                            {
                                hostelfee = hostelfee + "<tr><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                   gethostelfee[i].CategoryName + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                  gethostelfee[i].SubCategoryName + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                  gethostelfee[i].hostelfee + "</td><td style='padding: 8px;font-size: 13px; font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                  lastdate + "</td><tr>";
                            }
                            myString2 = myString2.Replace("$$hostelfee$$", hostelfee.ToString());
                            EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                            string PlaceName = "AdmissionController-Sending Fess Structure";
                            string UserName = "Employee";
                            string UserId = Convert.ToString(Session["username"]);
                            Mail.SendMultipleMail(EmailDisplay, supportEmail, Femail, "Account Credentials", myString2.ToString(), true, PlaceName, UserName, UserId);
                            
                            Pstudent.UpdateSendEmailStatus(regid);
                            flag = 1;
                        }

                        if (getstudentdetails.HostelRequired == false && getstudentdetails.TransportRequired == false)
                        {
                            StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/ConditionalOfferSchoolFee.html"));
                            string readFile2 = reader2.ReadToEnd();
                            string myString2 = "";
                            myString2 = readFile2;
                            myString2 = myString2.Replace("$$surname$$", Fname.ToString());
                            myString2 = myString2.Replace("$$appno$$", AppNo.ToString());
                            myString2 = myString2.Replace("$$stuname$$", StuName.ToString());
                            myString2 = myString2.Replace("$$applyclass$$", txt_class.ToString());
                            myString2 = myString2.Replace("$$AcdemicYear$$", txt_Ayear.ToString());
                            myString2 = myString2.Replace("$$tables1$$", item_body.ToString());
                            myString2 = myString2.Replace("$$tables2$$", item_body2.ToString());
                            myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
                            myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
                            myString2 = myString2.Replace("$$LastDate$$", lastdate.ToString());
                            string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                            myString2 = myString2.Replace("$$hostelfee$$", hostelfee.ToString());
                            EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                            string PlaceName = "AdmissionController-Sending Fess Structure";
                            string UserName = "Employee";
                            string UserId = Convert.ToString(Session["username"]);
                            Mail.SendMultipleMail(EmailDisplay, supportEmail, Femail, "Account Credentials", myString2.ToString(), true, PlaceName, UserName, UserId);
                           
                            Pstudent.UpdateSendEmailStatus(regid);
                            flag = 1;
                        }

                        //Mail.SendMultipleMail(EmailDisplay, supportEmail, Femail, "Account Credentials", myString2.ToString(),true);
                        //Pstudent.UpdateSendEmailStatus(regid);
                        //flag = 1;
                    }
                }
                else if (statusflag == "Accepted")
                {
                    StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/AcceptEmail.html"));
                    string readFile2 = reader2.ReadToEnd();
                    string myString2 = "";
                    myString2 = readFile2;
                    myString2 = myString2.Replace("$$surname$$", Fname.ToString());
                    myString2 = myString2.Replace("$$appno$$", AppNo.ToString());
                    myString2 = myString2.Replace("$$description$$", Description.ToString());
                    myString2 = myString2.Replace("$$StudentName$$", sname.ToString());
                    myString2 = myString2.Replace("$$ApplyClass$$", txt_Applyclass.ToString());
                    myString2 = myString2.Replace("$$AcdemicYear$$", txt_Ayear.ToString());
                    myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
                    myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
                    string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                    string PlaceName = "AdmissionController-Sending Application Status";
                    string UserName = "Employee";
                    string UserId = Convert.ToString(Session["username"]);
                    Mail.SendMultipleMail(EmailDisplay, supportEmail, Femail, "Account Credentials", myString2.ToString(), true, PlaceName, UserName, UserId);                    
                    Pstudent.UpdateSendEmailStatus(regid);
                    flag = 1;
                }
                if (flag == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
        [HttpPost]
        public JsonResult Email(VM_SendEmailSingle d)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int regid = d.StudentAdmissionId;
                    string statusflag = d.StatusFlag;
                    string description = d.Descriptions;
                    string Message = "";
                    string ErrorMessage = "";
                    var Fname = d.PrimaryUserName;
                    var Femail = d.PrimaryuserEmail;
                    var AppNo = d.StudentAdmissionId;
                    var Description = d.Descriptions;
                    var StuName = d.StudentName;
                    var txt_class = d.txt_ApplyClass;
                    int Acyear = Convert.ToInt16(d.AcyearId);
                    string txt_Academicyear = d.txt_Academicyear;
                    var classId = Convert.ToInt16(d.ApplyClass);
                    DateTime? date = null;
                    if (d.feepaylastdate != null)
                    {
                        date = DateTime.Parse(d.feepaylastdate);
                    }
                    else
                    {
                        date = null;
                    }
                    bool res = ViaEmail(regid, Fname, Femail, txt_Academicyear, txt_class, Acyear, classId, d.StatusFlag, description, d.StudentName, date);
                    if (res == true)
                    {
                        Message = eAcademy.Models.ResourceCache.Localize("SuccessFully_Mail_Send");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Send_Email_to_Enrollement_application"));
                    }
                    else
                    {
                        ErrorMessage = eAcademy.Models.ResourceCache.Localize("Error_for_Mail_Send");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Confirmation(bool? pdf)
        {
            try
            {
                ViewBag.Records = "No";
                if (!pdf.HasValue)
                {
                    ClassServices Cs = new ClassServices();
                    ViewBag.vacancyclassess = Cs.GetClassvacauncy();
                    return View();
                }
                else
                {
                    List<M_AdmissionConfirmationList> ConfirmationList = (List<M_AdmissionConfirmationList>)Session["JQGridList"];
                    string filePath = Server.MapPath("~/Views/") + "Sample.pdf";
                    ExportPDF(ConfirmationList, new string[] { "OnlineRegisterId", "PrimaryUser", "StuFirstName", "AdmissionClass", "statusFlag" }, filePath);//
                    return File(filePath, "application/pdf", "ConfirmationList.pdf");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
        }
        [HttpGet]
        public JsonResult JQgridConfirmationProcess(string sidx, string sord, int page, int rows, int? Admission_AcademicYear, int? Admission_Class)
        {
            IList<M_AdmissionConfirmationList> ConfirmationList = new List<M_AdmissionConfirmationList>();
            PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (Admission_AcademicYear != null && Admission_Class != null)
            {
                ConfirmationList = Pstudent.GetFeesPaidList(Admission_AcademicYear, Admission_Class);
            }
            int totalRecords = ConfirmationList.Count();
            if (totalRecords == 0)
            {
                ViewBag.Records = "No";
            }
            else
            {
                ViewBag.Records = "Yes";
            }
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    ConfirmationList = ConfirmationList.OrderByDescending(s => s.PrimaryUserEmail).ToList();
                    ConfirmationList = ConfirmationList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ConfirmationList = ConfirmationList.OrderBy(s => s.PrimaryUserEmail).ToList();
                    ConfirmationList = ConfirmationList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = ConfirmationList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = ConfirmationList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ConfirmationProcess_ExportToExcel()
        {
            try
            {
                List<M_AdmissionConfirmationList> ConfirmationList = (List<M_AdmissionConfirmationList>)Session["JQGridList"];
                var list = ConfirmationList.Select(o => new { AdmissionId = o.AdmissionId, PrimaryUserName = o.PrimaryUserName, StudentName = o.StudentName, ApplyClass = o.ApplyClass, Applicationstatus = o.Applicationstatus, Source = o.Source }).ToList();//, subjectid = o.subjectid
                string fileName = eAcademy.Models.ResourceCache.Localize("FeespaidStudentReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("Confirmation");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ConfirmationProcess_ExportToPdf()
        {
            try
            {
                List<M_AdmissionConfirmationList> ConfirmationList = (List<M_AdmissionConfirmationList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("FeespaidStudentReport");
                GeneratePDF.ExportPDF_Portrait(ConfirmationList, new string[] { "AdmissionId", "PrimaryUserName", "StudentName", "ApplyClass", "statusFlag", "Source" }, xfilePath, fileName);//, "subjectid"
                return File(xfilePath, "application/pdf", "FeespaidStudentList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public JsonResult ConfirmationAccept(VM_SendEmailFlag f)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionPrimaryUserRegisterServices Pprimaryuser = new PreAdmissionPrimaryUserRegisterServices();
                    PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                    PreAdmissionFatherRegisterServices Pfather = new PreAdmissionFatherRegisterServices();
                    PreAdmissionMotherRegisterServices Pmother = new PreAdmissionMotherRegisterServices();
                    PreAdmissionGuardianRegisterServices Pguardian = new PreAdmissionGuardianRegisterServices();
                    AsignClassVacancyServices acs = new AsignClassVacancyServices();
                    PreAdmissionRegisterStatusFlagServices rss = new PreAdmissionRegisterStatusFlagServices();
                    PrimaryUserRegisterServices primary = new PrimaryUserRegisterServices();
                    StudentServices s_student = new StudentServices();
                    AdmissionTransactionServices admintrans = new AdmissionTransactionServices();
                    FatherRegisterServices father = new FatherRegisterServices();
                    MotherRegisterServices mother = new MotherRegisterServices();
                    GuardianRegisterServices guardian = new GuardianRegisterServices();
                    tblSiblingServices s_sbling = new tblSiblingServices();
                    FeeCollectionServices s_feecollection = new FeeCollectionServices();
                    FeeCollectionCategoryServices s_feecollectioncategory = new FeeCollectionCategoryServices();
                    int count = Convert.ToInt16(f.SelectCount);
                    string[] regid = new string[count];
                    string Message = "";
                    string statusflag = "Accepted";
                    int vacancycount = Convert.ToInt16(acs.getParticularClassVacancy(Convert.ToInt16(f.AcademicYearId), Convert.ToInt16(f.Classid)));
                    var Account = Pstudent.GetStatusFlagCountforClass(Convert.ToInt16(f.AcademicYearId), Convert.ToInt16(f.Classid), statusflag);
                    int AcceptCount = Account.Count;
                    DateTime? date = null;
                    if (f.FeePaidLastDate != null)
                    {
                        date = DateTime.Parse(f.FeePaidLastDate);
                    }
                    else
                    {
                        date = null;
                    }
                    if (count <= vacancycount - AcceptCount)
                    {
                        for (int i = 0; i < count; i++)
                        {
                            regid[i] = String.Join(" ", f.RegId[i]);
                            int id = Convert.ToInt16(QSCrypt.Decrypt(regid[i]));
                            int NewprimaryUserId = 0; int NewStudentRegisterId = 0; int NewFatherRegisterId = 0; int NewMotherRegisterId = 0; int NewGuardianRegisterId = 0;
                            rss.AddRegisterStatusFlag(id, statusflag, f.description);
                            Pstudent.UpdateStudentRegisterStatusSelectionProcess(id, statusflag, date);
                            var r = Pstudent.GetPrimaryUserMaildetails(id);
                            int Ayid = Convert.ToInt16(r.AcademicyearId);
                            int cid = Convert.ToInt16(r.ClassId);
                            bool result = ViaEmail(id, r.PrimaryUserName, r.PrimaryUserEmail, r.txt_AcademicYear, r.txt_AdmissionClass, Ayid, cid, r.statusFlag, f.description, r.StuFirstName, date);
                            if (result == true)
                            {
                                var Enrole_Primaryuserdetails = Pprimaryuser.getprimaryuserusingstudentadmissionid(id);
                                var Enrole_StudentDetails = Pstudent.StudentDetails(id);
                                var Enrole_FatherDetails = Pfather.GetEnrolementFatherDetails(id);
                                var Enrole_MotherDetails = Pmother.GetEnrolementMotherdetails(id);
                                var Enrole_GuardianDetails = Pguardian.GetEnrolementGuardiandetails(id);
                                NewprimaryUserId = primary.PrimaryUserCopyfromPreAdmissiontoMaster(Enrole_Primaryuserdetails);
                                NewStudentRegisterId = s_student.CopyFromStudentRegToStudent(Enrole_StudentDetails);
                                admintrans.AddStudentPrimaryuser(NewprimaryUserId, NewStudentRegisterId);
                                if (Enrole_FatherDetails != null)
                                {
                                    NewFatherRegisterId = father.FatherdetailsCopyfromPreAdmissiontoFather(Enrole_FatherDetails, NewprimaryUserId, NewStudentRegisterId);
                                    admintrans.EditUpdateFatherPrimaryUserId(NewprimaryUserId, NewStudentRegisterId, NewFatherRegisterId);
                                }
                                if (Enrole_MotherDetails != null)
                                {
                                    NewMotherRegisterId = mother.MotherdetailsCopyfromPreAdmissiontomother(Enrole_MotherDetails, NewprimaryUserId, NewStudentRegisterId);
                                    admintrans.EditUpdateMotherPrimaryUserId(NewprimaryUserId, NewStudentRegisterId, NewMotherRegisterId);
                                }
                                if (Enrole_GuardianDetails != null)
                                {
                                    NewGuardianRegisterId = guardian.GuardiandetailsCopyfromPreAdmissiontoguardian(Enrole_GuardianDetails, NewprimaryUserId, NewStudentRegisterId);
                                    admintrans.EditUpdateGuardianPrimaryUserId(NewprimaryUserId, NewStudentRegisterId, NewGuardianRegisterId);
                                }
                                s_sbling.CopysiblingfromPreAdmissiontoMaster(NewprimaryUserId, NewStudentRegisterId, NewFatherRegisterId, NewMotherRegisterId, NewGuardianRegisterId);
                                s_feecollection.UpdateStudentDetails(Convert.ToInt16(Enrole_StudentDetails.StudentAdmissionId), NewStudentRegisterId, Convert.ToInt16(Enrole_StudentDetails.AcademicyearId), Convert.ToInt16(Enrole_StudentDetails.AdmissionClass));
                                s_feecollectioncategory.UpdateStudentDetails(Convert.ToInt16(Enrole_StudentDetails.StudentAdmissionId), NewStudentRegisterId, Convert.ToInt16(Enrole_StudentDetails.AcademicyearId), Convert.ToInt16(Enrole_StudentDetails.AdmissionClass));
                                Message = eAcademy.Models.ResourceCache.Localize("Selected_application_are_successfully_accepted"); //+ id;
                            }
                            else
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Application_Number") + regid[i] + eAcademy.Models.ResourceCache.Localize("status_not_accepted,_email_sending_problem");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_student_from_PreAdmission_to_School"));
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("You_are_selected_out_of_vacancy,_vacancy_limit_is") + "=" + vacancycount;
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string Message = eAcademy.Models.ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                    return Json(new { ErrorMessage = Message }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ConfirmationReject(VM_SendEmailFlag f)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                    AsignClassVacancyServices acs = new AsignClassVacancyServices();
                    PreAdmissionRegisterStatusFlagServices rss = new PreAdmissionRegisterStatusFlagServices();
                    int count = Convert.ToInt16(f.SelectCount);
                    string[] regid = new string[count];
                    string Message = "";
                    string statusflag = "";
                    DateTime? date = null;
                    if (f.FeePaidLastDate != null)
                    {
                        date = DateTime.Parse(f.FeePaidLastDate);
                    }
                    else
                    {
                        date = null;
                    }
                    for (int i = 0; i < count; i++)
                    {
                        regid[i] = String.Join(" ", f.RegId[i]);
                        int id = Convert.ToInt16(QSCrypt.Decrypt(regid[i]));
                        statusflag = "Reject";
                        rss.AddRegisterStatusFlag(id, statusflag, f.description);
                        Pstudent.UpdateStudentRegisterStatusSelectionProcess(id, statusflag, date);
                        var r = Pstudent.GetPrimaryUserMaildetails(id);
                        int Ayid = Convert.ToInt16(r.AcademicyearId);
                        int cid = Convert.ToInt16(r.ClassId);
                        bool result = ViaEmail(id, r.PrimaryUserName, r.PrimaryUserEmail, r.txt_AcademicYear, r.txt_AdmissionClass, Ayid, cid, r.statusFlag, f.description, r.StuFirstName, date);
                        if (result == true)
                        {
                            Message = eAcademy.Models.ResourceCache.Localize("Selected_application_are_successfully_quieted");
                        }
                        else
                        {
                            Message = eAcademy.Models.ResourceCache.Localize("Application_are_not_quieted");
                            return Json(new { ErrorMessage = Message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Quiet_Enrollement_Application"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ConfirmationPending(VM_SendEmailFlag f)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                    AsignClassVacancyServices acs = new AsignClassVacancyServices();
                    PreAdmissionRegisterStatusFlagServices rss = new PreAdmissionRegisterStatusFlagServices();
                    int count = Convert.ToInt16(f.SelectCount);
                    string[] regid = new string[count];
                    string Message = "";
                    string statusflag = "";
                    DateTime? date = null;
                    if (f.FeePaidLastDate != null)
                    {
                        date = DateTime.Parse(f.FeePaidLastDate);
                    }
                    else
                    {
                        date = null;
                    }
                    for (int i = 0; i < count; i++)
                    {
                        regid[i] = String.Join(" ", f.RegId[i]);
                        int id = Convert.ToInt16(QSCrypt.Decrypt(regid[i]));
                        statusflag = "Pending";
                        rss.AddRegisterStatusFlag(id, statusflag, f.description);
                        Pstudent.UpdateStudentRegisterStatusSelectionProcess(id, statusflag, date);
                        var r = Pstudent.GetPrimaryUserMaildetails(id);
                        int Ayid = Convert.ToInt16(r.AcademicyearId);
                        int cid = Convert.ToInt16(r.ClassId);
                        bool result = ViaEmail(id, r.PrimaryUserName, r.PrimaryUserEmail, r.txt_AcademicYear, r.txt_AdmissionClass, Ayid, cid, r.statusFlag, f.description, r.StuFirstName, date);
                        if (result == true)
                        {
                            Message = eAcademy.Models.ResourceCache.Localize("Selected_application_are_Successfully_added_PendingList");
                        }
                        else
                        {
                            Message = eAcademy.Models.ResourceCache.Localize("Application_are_not_Pending");
                            return Json(new { ErrorMessage = Message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Pending_Enrollement_Application"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult FlagCount(VM_AcademicYearId a)
        {
            try
            {
                PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                DateTime currentdate = Convert.ToDateTime(DateTimeByZone.getCurrentDate());
                int Current_AcademicyearId = Convert.ToInt16(a.AcademicYearId);
                var Conditional = Pstudent.GetStatusFlagCount(Current_AcademicyearId, "Conditionaloffer");
                int Conditional_count = Conditional.Count;
                var Pending = Pstudent.GetStatusFlagCount(Current_AcademicyearId, "Pending");
                int Pending_count = Pending.Count;
                var Reject = Pstudent.GetStatusFlagCount(Current_AcademicyearId, "Reject");
                int Reject_count = Reject.Count;
                var AddMore = Pstudent.GetStatusFlagCount(Current_AcademicyearId, "AddMore");
                int AddMore_count = AddMore.Count;
                var Enrolled = Pstudent.GetStatusFlagCount(Current_AcademicyearId, "Enrolled");
                int Enrolled_count = Enrolled.Count;
                var Accept = Pstudent.GetStatusFlagCount(Current_AcademicyearId, "Accepted");
                int Accept_count = Accept.Count;
                var FeesPaid = Pstudent.GetStatusFlagCount(Current_AcademicyearId, "FeesPaid");
                int FeesPaid_count = FeesPaid.Count;
                var InComplete = Pstudent.GetStatusFlagCount(Current_AcademicyearId, "InComplete");
                int InComplete_count = InComplete.Count;
                var InterView = Pstudent.GetStatusFlagCount(Current_AcademicyearId, "InterView");
                int InterView_count = InterView.Count;
                var InterViewEntrance = Pstudent.GetStatusFlagCount(Current_AcademicyearId, "InterView&Entrance");
                int InterViewEntrance_count = InterViewEntrance.Count;
                return Json(new { Conditional = Conditional_count, Pending = Pending_count, Reject = Reject_count, AddMore = AddMore_count, Enrolled = Enrolled_count, Accept = Accept_count, FeesPaid = FeesPaid_count, InComplete = InComplete_count, InterView = InterView_count, InterViewEntrance = InterViewEntrance_count }, JsonRequestBehavior.AllowGet); // InterView = InterView, InterViewEntrance = InterViewEntrance_count
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Vacancytable(VM_AcademicYearId a)
        {
            try
            {
                PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                AsignClassVacancyServices Avs = new AsignClassVacancyServices();
                DateTime currentdate = Convert.ToDateTime(DateTimeByZone.getCurrentDate());
                int Current_AcademicyearId = Convert.ToInt16(a.AcademicYearId);
                var vacancyList = Avs.GetonlyActiveClassVacancy(Current_AcademicyearId);
                int[] Conditional_count = new int[vacancyList.Count];
                int[] Pending_count = new int[vacancyList.Count];
                int[] Reject_count = new int[vacancyList.Count];
                int[] AddMore_count = new int[vacancyList.Count];
                int[] Enrolled_count = new int[vacancyList.Count];
                int[] Accept_count = new int[vacancyList.Count];
                for (int k = 0; k < vacancyList.Count; k++)
                {
                    int classid = Convert.ToInt16(vacancyList[k].classid);
                    var Conditional = Pstudent.GetParticularStatusFlagCount(Current_AcademicyearId, classid, "Conditionaloffer");
                    Conditional_count[k] = Conditional.Count;
                    var Pending = Pstudent.GetParticularStatusFlagCount(Current_AcademicyearId, classid, "Pending");
                    Pending_count[k] = Pending.Count;
                    var Reject = Pstudent.GetParticularStatusFlagCount(Current_AcademicyearId, classid, "Reject");
                    Reject_count[k] = Reject.Count;
                    var AddMore = Pstudent.GetParticularStatusFlagCount(Current_AcademicyearId, classid, "AddMore");
                    AddMore_count[k] = AddMore.Count;
                    var Enrolled = Pstudent.GetParticularStatusFlagCount(Current_AcademicyearId, classid, "Enrolled");
                    Enrolled_count[k] = Enrolled.Count;
                    var Accept = Pstudent.GetParticularStatusFlagCount(Current_AcademicyearId, classid, "Accepted");
                    Accept_count[k] = Accept.Count;
                }
                return Json(new { vacancy = vacancyList, Conditional = Conditional_count, Pending = Pending_count, Reject = Reject_count, AddMore = AddMore_count, Enrolled = Enrolled_count, Accept = Accept_count }, JsonRequestBehavior.AllowGet);// 
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult StatusFlagDataList(int? AcyearId, string flag)
        {
            try
            {
                AcademicyearServices ays = new AcademicyearServices();
                ViewBag.yearid = AcyearId;
                ViewBag.yy = ays.GetParticularAcdemicYear(AcyearId);
                ViewBag.flag = flag;
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
        }
        [HttpGet]
        public JsonResult StatusFlagApplicationList(string sidx, string sord, int page, int rows, string Status_flag, int? Admission_AcademicYear)
        {
            PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (Admission_AcademicYear != null && Status_flag != "")
            {
                SelectionList = Pstudent.GetStatusflagApplicationList(Admission_AcademicYear, Status_flag);
            }
            int totalRecords = SelectionList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    SelectionList = SelectionList.OrderByDescending(s => s.PrimaryUserEmail).ToList();
                    SelectionList = SelectionList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    SelectionList = SelectionList.OrderBy(s => s.PrimaryUserEmail).ToList();
                    SelectionList = SelectionList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = SelectionList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = SelectionList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult StatusFlagDatalist_ExportToExcel()
        {
            try
            {
                List<AdminSelectionList> SelectionList = (List<AdminSelectionList>)Session["JQGridList"];
                var list = SelectionList.Select(o => new { StudentAdmissionId = o.StudentAdmissionId, PrimaryUserName = o.PrimaryUserName, StuFirstName = o.StuFirstName, ApplyClass = o.txt_AdmissionClass, Distance = o.Distance, SiblingStatus = o.SiblingStatus, ParentWorkSameSchool = o.WorkSameSchool, ApplicationStatus = o.statusFlag, ApplicationSource = o.ApplicationSource }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("StudentCurrentStatusList");
                GenerateExcel.ExportExcel(list, fileName);
                return View("TransportStudent");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult StatusFlagDatalist_ExportToPdf()
        {
            try
            {
                List<AdminSelectionList> SelectionList = (List<AdminSelectionList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("StudentCurrentStatusList");
                GeneratePDF.ExportPDF_Portrait(SelectionList, new string[] { "StudentAdmissionId", "PrimaryUserName", "StudentName", "ApplyClass", "Distance", "SiblingStatus", "WorkSameSchool", "statusFlag", "ApplicationSource" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "StatusFlagDetails.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult AdmissionFeesCollection()
        {
            try
            {
                ClassServices Cs = new ClassServices();
                ViewBag.classess = Cs.GetClassvacauncy();
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View("error");
            }
        }
        [HttpPost]
        public JsonResult CheckStatus(VM_FeesApplicationStatus a)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                    var date = DateTimeByZone.getCurrentDateTime().Date;
                    var result = Pstudent.GetFeesApplicationStatus(a.ApplicationID, a.AcademicYearId, a.AdmissionClassId);//,a.Source
                    if (result == null)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Application_details_not_found");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else if (result.Statusflag != "Conditionaloffer")
                    {
                        string info = eAcademy.Models.ResourceCache.Localize("ApplicationStatus") + " " + result.Statusflag;
                        return Json(new { Info = info }, JsonRequestBehavior.AllowGet);
                    }
                    else if (result.Statusflag == "Conditionaloffer" && result.FeePayLastDate >= date)
                    {
                        string Message = "Success";
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string info = eAcademy.Models.ResourceCache.Localize("Fee_Pay_Date_was_expired_So_cant_pay_Fee");
                        return Json(new { Info = info }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetPayFeesStructure(VM_GetFeeStructure s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdminServices ps = new PreAdminServices();
                    int yearid = Convert.ToInt16(s.AcademicYearId);
                    int classid = Convert.ToInt16(s.ClassId);
                    var result = ps.getallFeesByID(yearid, classid);
                    var tid = ps.GetAcademicfirsttermId(yearid);
                    int termid = Convert.ToInt16(tid);
                    return Json(new { result = result, termid = termid }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_Required_Files");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult getFeesStructure(VM_GetFeeStructure s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdminServices ps = new PreAdminServices();
                    int yearid = Convert.ToInt16(s.AcademicYearId);
                    int classid = Convert.ToInt16(s.ClassId);
                    var result = ps.getallFeesByID(yearid, classid);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_Required_Files");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult BillReceipt(string id)
        {
            try
            {
                FeeCollectionServices Fee_Collection = new FeeCollectionServices();
                int FeecolletionId = Convert.ToInt16(QSCrypt.Decrypt(id));
                var list = Fee_Collection.GetPreAdmissionFeeCollectDetails(FeecolletionId);
                return View(list);
            }
            catch (Exception e)
            {
                return View();
            }
        }
        [HttpGet]
        public JsonResult GetFeeCategory(int feecollectionId)
        {
            try
            {
                FeeCollectionServices FeeCollect = new FeeCollectionServices();
                FeeCollectionCategoryServices FeeColleectCategory = new FeeCollectionCategoryServices();
                var list = FeeColleectCategory.GetFeeCollectionCategory(feecollectionId);
                var data = FeeCollect.GetCollectionDetails(feecollectionId);
                var Fine = data.Fine;
                var discount = data.Discount;
                var totalamount = data.AmountCollected;
                var vat = data.VAT;
                return Json(new { list = list, Fine = Fine, discount = discount, totalamount = totalamount, vat = vat }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        private Dictionary<string, Object> GetErrorsFromModelState()
        {
            var errors = new Dictionary<string, Object>();
            foreach (var key in ModelState.Keys)
            {
                if (ModelState[key].Errors.Count > 0)
                {
                    errors[key] = ModelState[key].Errors;
                }
            }
            return errors;
        }

        public JsonResult GetFeestructure(string YearId, string classId)
        {
            try
            {
                PreAdminServices ps = new PreAdminServices();
                var year = Convert.ToInt32(YearId);
                var classid = Convert.ToInt32(classId);
                var result = ps.getallFeesByID(year, classid).Count();
                if (result == 0)
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Fee_Structure_didnt_created_in_this_class._So_can't_send_Fee_structure");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string Message = eAcademy.Models.ResourceCache.Localize("Fee_Structure_created");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Getconditionalofferdetails(string YearId, string classId)
        {
            try
            {
                PreAdminServices ps = new PreAdminServices();
                AsignClassVacancyServices Acs = new AsignClassVacancyServices();
                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                var year = Convert.ToInt32(YearId);
                var classid = Convert.ToInt32(classId);
                var conditionalOfferStudent = PstudentRegister.getconditionalofferstudents(year, classid);
                var Acceptedstudent = PstudentRegister.getcAcceptedstudents(year, classid);
                var  FeePaidStudent = PstudentRegister.getFeePaidstudents(year, classid);
                var totalcfastudentcount = Convert.ToInt32(conditionalOfferStudent) + Convert.ToInt32(FeePaidStudent) + Convert.ToInt32(Acceptedstudent);
                var classvacuncy = Acs.getParticularClassVacancy(year, classid);
                var balancevacuncy = Convert.ToInt32(classvacuncy) - Convert.ToInt32(totalcfastudentcount);
                if (balancevacuncy == 0)
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Class_vacuncy_is") + " " + classvacuncy + " ." + eAcademy.Models.ResourceCache.Localize("Already_conditionaloffer_gave") + totalcfastudentcount + " " + eAcademy.Models.ResourceCache.Localize("students_You_cant_give_conditional_offer");
                    return Json(new { ErrorMessage = ErrorMessage, balancevacuncy = balancevacuncy }, JsonRequestBehavior.AllowGet);
                }
                else if (balancevacuncy > 0)
                {
                    string Message = eAcademy.Models.ResourceCache.Localize("Class_vacuncy_is") + " " + classvacuncy + " ." + eAcademy.Models.ResourceCache.Localize("Already_conditionaloffer_gave") + " " + totalcfastudentcount + " " + eAcademy.Models.ResourceCache.Localize("students_You_can_give_conditional_offer") + " " + balancevacuncy + eAcademy.Models.ResourceCache.Localize("only");
                    return Json(new { Message = Message, balancevacuncy = balancevacuncy }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Class_vacuncy_is") + " " + classvacuncy + " ." + eAcademy.Models.ResourceCache.Localize("Already_conditionaloffer_gave") + " " + totalcfastudentcount + " " + eAcademy.Models.ResourceCache.Localize("students_You_cant_give_conditional_offer");
                    return Json(new { ErrorMessage = ErrorMessage, balancevacuncy = balancevacuncy }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PreAdmissionFeeRefund()
        {
            ClassServices Cs = new ClassServices();
            ViewBag.classess = Cs.GetClassvacauncy();
            return View();
        }
        public JsonResult GetAdmissionPaidFeesRefund(PreadmissionFeeRefund cs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PrimaryUserRegisterServices primary = new PrimaryUserRegisterServices();
                    AssignClassToStudentServices assignclass = new AssignClassToStudentServices();
                    FeeCollectionCategoryServices Fee_CollectionCategory = new FeeCollectionCategoryServices();
                    StudentServices S_Student = new StudentServices();
                    PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                    var CheckStudentStatus = PstudentRegister.GetFeesApplicationStatus(cs.ApplicationId, cs.AcademicYearId, cs.ClassId);
                    if (CheckStudentStatus != null)
                    {
                        if (CheckStudentStatus.Statusflag == "Refunded_beforeassignclass")
                        {
                            string info = eAcademy.Models.ResourceCache.Localize("Already_admission_fees_refunded_to_this_user");
                            return Json(new { info = info }, JsonRequestBehavior.AllowGet);
                        }
                        else if (CheckStudentStatus.Statusflag != "FeesPaid" || CheckStudentStatus.Statusflag != "Accepted")
                        {
                            string info = "Fee didn't pay, So can't Refund";
                            return Json(new { info = info }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var classAssigned = assignclass.CheckClassAssignedtoAdmissionStudent(cs.ApplicationId);
                            if (classAssigned == true)
                            {
                                string info = eAcademy.Models.ResourceCache.Localize("Class_assigned_to_this_student,so_fees_can't_refunded");
                                return Json(new { info = info }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                var list = Fee_CollectionCategory.GetAdmissionpaidFees(cs.ApplicationId);
                                var result = S_Student.GetPreAdmissionfeeStudentDetails(cs.ApplicationId);
                                return Json(new { list, result }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("PleaseEnterCorrectAdmissionidAcademicyearAndClass");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult PreAdmissionFeeRefunds(VM_AdmissionFeeRefundToStudent cs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    FeesRefundToStudentServices Fr = new FeesRefundToStudentServices();
                    PreAdmissionStudentRegisterServices Ps = new PreAdmissionStudentRegisterServices();
                    Fr.addpreaddmissionstudentdetails(cs.FeeCollectionidstudent, cs.TotalAmount, cs.AmountRefunded, cs.reason, cs.ReceivedBy, AdminId, cs.StudentApplicationid);
                    Ps.updatepreadmissionfeerefundstatus(cs.StudentApplicationid);
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("RefundAdmissionFee"));
                    string Message = eAcademy.Models.ResourceCache.Localize("Successfully_refund_fees_to_student");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}