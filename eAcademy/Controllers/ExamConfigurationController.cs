﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class ExamConfigurationController : Controller
    {
        ExamServices obj_exam = new ExamServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        public ActionResult ExamConfig()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
         
        public JsonResult ExamResults(string sidx, string sord, int page, int rows, string txtExamName)
        {
            IList<ExamList> todoListsResults1 = new List<ExamList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults1 = obj_exam.getExam();
            if (!string.IsNullOrEmpty(txtExamName))
            {
                todoListsResults1 = todoListsResults1.Where(p => p.Exam.Contains(txtExamName)).ToList();
            }
            int totalRecords = todoListsResults1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults1 = todoListsResults1.OrderByDescending(s => s.Exam).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults1 = todoListsResults1.OrderBy(s => s.Exam).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults1;
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExamExportToExcel()
        {
            try
            {
                List<ExamList> todoListsResults1 = (List<ExamList>)Session["JQGridList"];
                var list = todoListsResults1.Select(o => new { ExamName = o.Exam, status = o.status }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("ExamDetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ExamExportToPDF()
        {
            try
            {
                List<ExamList> todoListsResults1 = (List<ExamList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ExamDetails");
                GeneratePDF.ExportPDF_Portrait(todoListsResults1, new string[] { "Exam", "status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "Exam.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public JsonResult ExamConfig(SaveExam model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string ename = model.txt_exam;
                    bool cc = obj_exam.checkExam1(ename);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("ExamalreadyExist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_exam.addExam(ename);
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Addexam"));
                        string Message = eAcademy.Models.ResourceCache.Localize("ExamAddedSuccessfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [EncryptedActionParameter]
        public ActionResult EditExam(string ExamId)
        {
            try
            {
                int eid = Convert.ToInt32(QSCrypt.Decrypt(ExamId));
                var ans = obj_exam.getExamByExamID(eid);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditExam(SaveExam model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int examid = Convert.ToInt32(QSCrypt.Decrypt(model.examid));
                    string ename = model.txt_exam;
                    bool cc = obj_exam.checkExam(examid, ename);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("ExamalreadyExist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_exam.updateExam(examid, ename,model.status);
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Updateexam"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}