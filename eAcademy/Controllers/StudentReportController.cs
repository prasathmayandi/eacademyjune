﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
     [CheckSessionOutAttribute]
    public class StudentReportController : Controller
    {
        EacademyEntities ee = new EacademyEntities();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            ExamServices Es = new ExamServices();
            MonthsServices Ms = new MonthsServices();
            ViewBag.allAcademicYears = As.ShowAcademicYears();
            ViewBag.allClasses = Cs.ShowClasses();
            ViewBag.allExams = Es.ShowExams();
            List<SelectListItem> Sort = new List<SelectListItem>();
            Sort.Add(new SelectListItem { Text = "Select Status", Value = "" });
            Sort.Add(new SelectListItem { Text = "Both", Value = "All" });
            Sort.Add(new SelectListItem { Text = "Pass", Value = "Pass" });
            Sort.Add(new SelectListItem { Text = "Fail", Value = "fail" });
            ViewBag.SortList = Sort;
            ViewBag.academicyear = As.GetAcademicYear();
            ViewBag.Classes = Cs.GetClass();
            ViewBag.reporttype = getrtype();
            ViewBag.month = Ms.GetMonths();
            ViewBag.FeePaticular = getFeeParticulars();
            ViewBag.ExamType = Es.GetExamName();
            ViewBag.exam = Es.GetExamName();
            ViewBag.paytype = getPaymentType();
            ViewBag.date = DateTimeByZone.getCurrentDateTime();
        }
        public ActionResult Studentprofile()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Studentprofile(Mandatory m)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            SectionServices Ss = new SectionServices();
            AssignClassToStudentServices acss = new AssignClassToStudentServices();
            StudentServices StuSer= new StudentServices();
            try
            {
                if (ModelState.IsValid)
                {
                    TempData["show"] = "show";
                    var c = Convert.ToInt32(m.Class);
                    ViewBag.c = c;
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.Classes = Cs.GetClass();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    ViewBag.section = Ss.GetSection(c);
                    var sec = Convert.ToInt32(m.Rpt_Section);
                    ViewBag.sec = sec;
                    var stu_id = Convert.ToInt32(m.StudentId);
                    var tt = acss.GetClassStudentName(yid, c, sec);
                    ViewBag.stuname = tt;
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(c);
                    ViewBag.ss = Ss.GetParticularSection(sec);
                    ViewBag.nn = StuSer.GetParticularStudentName(stu_id);
                    var id = Convert.ToInt32(m.StudentId);
                    ViewBag.na = id;
                    var ans1 = StuSer.SearchStudentProfile(id);
                    TempData["res"] = ans1;
                    ViewBag.view = ans1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["res"];
            return View(ans);
        }
         [HttpPost]
         public JsonResult JsonStudentProfile(VM_StudentProfileValidation s)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    StudentServices StuSer = new StudentServices();
                    int Acyear = s.AcademicYear;
                    int class1 = s.Class;
                    int sec = s.Section;
                    int stuid = s.StudentId;
                    var ans1 = StuSer.getStudentProfile(stuid);
                    return Json(ans1, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
             catch(Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public IEnumerable getrtype()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select", Value = "" });
            li.Add(new SelectListItem { Text = "Daily", Value = "1" });
            //li.Add(new SelectListItem { Text = "From/To", Value = "2" });
            li.Add(new SelectListItem { Text = "Monthly", Value = "3" });
            return li;
        }
        public IEnumerable getPaymentType()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select", Value = "" });
            li.Add(new SelectListItem { Text = "Annual", Value = "1" });
            li.Add(new SelectListItem { Text = "Term", Value = "2" });
            return li;
        }
        public IEnumerable getFeeParticulars()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select", Value = "" });
            li.Add(new SelectListItem { Text = "Paid", Value = "1" });
            li.Add(new SelectListItem { Text = "Due", Value = "2" });
            return li;
        }
        public ActionResult Parentprofile()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Parentprofile(Mandatory m)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            SectionServices Ss = new SectionServices();
            AssignClassToStudentServices acss = new AssignClassToStudentServices();
            StudentServices StuSer = new StudentServices();
            ParentDetailServices  pds = new ParentDetailServices();
            try
            {
                if (ModelState.IsValid)
                {
                    TempData["show"] = "show";
                    var c = Convert.ToInt32(m.Class);
                    ViewBag.c = c;
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.Classes = Cs.GetClass();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    ViewBag.section = Ss.GetSection(c);
                    var sec = Convert.ToInt32(m.Rpt_Section);
                    ViewBag.sec = sec;
                    var stu_id = Convert.ToInt32(m.StudentId);
                    var tt = acss.GetClassStudentName(yid, c, sec);
                    ViewBag.stuname = tt;
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(c);
                    ViewBag.ss = Ss.GetParticularSection(sec);
                    ViewBag.nn = StuSer.GetParticularStudentName(stu_id);
                    var id = Convert.ToInt32(m.StudentId);
                    ViewBag.na = id;
                    var ans1 = "n"; //pds.SearchParentProfile(id);
                   TempData["ans"] = ans1;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
         public JsonResult JsonParentProfile(VM_StudentProfileValidation s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    StudentServices s_student = new StudentServices();
                    PrimaryUserRegisterServices primary = new PrimaryUserRegisterServices();
                    int Acyear = s.AcademicYear;
                    int class1 = s.Class;
                    int sec = s.Section;
                    int stuid = s.StudentId;
                    var uname = primary.GetPrimaryUserEmailUseStudentRegid(stuid);
                    var ans1 = s_student.GetStudentalldetails(stuid, uname);
                    return Json(ans1, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult StudentProgressCard()
        {
            return View();
        }
        [HttpPost]
        public ActionResult StudentProgressCard(Mandatory m)
        {
            try
            {
                AcademicyearServices As = new AcademicyearServices();
                ClassServices Cs = new ClassServices();
                SectionServices Ss = new SectionServices();
                AssignClassToStudentServices acss = new AssignClassToStudentServices();
                StudentServices StuSer = new StudentServices();
                ExamServices Es = new ExamServices();
                AssignSubjectToSectionServices astss = new AssignSubjectToSectionServices();
                StudentMarkServices sms = new StudentMarkServices();
                MarkTotalServices mts = new MarkTotalServices();
                if (ModelState.IsValid)
                {
                    ArrayList s1 = new ArrayList();
                    ArrayList tot = new ArrayList();
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.Classes = Cs.GetClass();
                    ViewBag.ExamType = Es.GetExamName();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    var cid = Convert.ToInt32(m.Class);
                    ViewBag.cls = cid;
                    var sid = Convert.ToInt32(m.Rpt_Section);
                    ViewBag.section = Ss.GetSection(cid);
                    ViewBag.sec = sid;
                    var stu_id = Convert.ToInt32(m.StudentId);
                    var tt = acss.GetClassStudentName(yid, cid, sid);
                    ViewBag.stuname = tt;
                    ViewBag.na = stu_id;
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(cid);
                    ViewBag.ss = Ss.GetParticularSection(sid);
                    ViewBag.nn = StuSer.GetParticularStudentName(stu_id);
                    var s = astss.SearchClassSubject(yid, cid, sid);
                    foreach (var v in s)
                    {
                        s1.Add(v.subjectName);
                    }
                    ViewBag.subject = s1;
                   var ans1 = sms.StudentProgress(yid, cid, sid, stu_id);
                    TempData["ans"] = ans1;
                    ViewBag.view = ans1.Count;
                    var t = mts.GetParticularStudentTotalMark(yid, cid, sid, stu_id);
                    foreach (var v in t)
                    {
                        tot.Add(v.Totalmark);
                    }
                    ViewBag.total = tot;
                    List<string> subj = new List<string>();
                    var e = Es.GetExamNameList();
                    foreach (var v in e)
                    {
                        subj.Add(v.ExamName);
                    }
                    ViewBag.subj = subj;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        [HttpPost]
        public JsonResult JsonStudentProgressCard(GradeProgressCard p)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    StudentMarkServices sms = new StudentMarkServices();
                    AssignSubjectToSectionServices astss = new AssignSubjectToSectionServices();
                    MarkTotalServices mts = new MarkTotalServices();
                    var ans1 = sms.StudentProgress(p.Acdemicyear, p.Class, p.Section, p.StudentId);
                    var s = astss.SearchClassSubject(p.Acdemicyear, p.Class, p.Section);
                    string[] SubjectList = new string[s.Count];
                    int[] SubjectIdList = new int[s.Count];
                    string[] ExamList = new string[20];
                    string[,] examwrittensubjectlist = new string[ans1.Count, ans1.Count];
                    int k = 0;
                    var t = mts.GetParticularStudentTotalMark(p.Acdemicyear,p.Class, p.Section, p.StudentId);
                    foreach (var v in s)
                    {
                        SubjectList[k] = v.subjectName;
                        SubjectIdList[k] = v.subjectId.Value;
                        k++;
                    }
                    int y = 0;
                    foreach (var b in ans1)
                    {
                        if (!ExamList.Contains(b.ExamType))
                        {
                            ExamList[y] = b.ExamType;
                            y++;
                        }
                    }

                    int[] arr = new int[5] { 5, 6, 2, 4, 1 };

                    int[] ascOrderedArray = (from i in arr orderby i ascending select i).ToArray();

                    var conductedExamList = mts.ConductedExam(p.Acdemicyear, p.Class, p.Section, p.StudentId);
                  
                 //   var subListOrder = SubjectIdList;
                    int[] subListOrder = (from i in SubjectIdList orderby i ascending select i).ToArray();
                    //int j = 0;
                    //foreach (var b in ans1)
                    //{
                    //    for (int s = 0; s <= b.subjectName.Count(); s++)
                    //    {
                    //        examwrittensubjectlist = b.subjectName[];
                    //    }
                    //    j++;
                    //}

                    return Json(new { Grades = ans1, Subjectlist = SubjectList, ExamList = ExamList, ExamList_Count = y, Total_mark = t, subListOrder, conductedExamList }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult StudentFeeDetails()
        {
            return View();
        }
        [HttpPost]
        public ActionResult StudentFeeDetails(Mandatory m,FormCollection fm)
        {
            try
            {
                AcademicyearServices As = new AcademicyearServices();
                ClassServices Cs = new ClassServices();
                SectionServices Ss = new SectionServices();
                AssignClassToStudentServices acss = new AssignClassToStudentServices();
                StudentServices StuSer = new StudentServices();
                ExamServices Es = new ExamServices();
                FeeServices Fs = new FeeServices();
                TermServices Ts = new TermServices();
                TermFeeServices tfs = new TermFeeServices();
                if (ModelState.IsValid)
                {
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.Classes = Cs.GetClass();
                    ViewBag.ExamType = Es.GetExamName();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    var cid = Convert.ToInt32(m.Class);
                    ViewBag.cls = cid;
                    var sid = Convert.ToInt32(m.Rpt_Section);
                    var payid = Convert.ToInt32(m.PaymentType);
                    ViewBag.p = payid;
                    ViewBag.paytype = getPaymentType();
                    var e = Ss.GetSection(cid);
                    ViewBag.section = e;
                    ViewBag.sec = sid;
                    var stu_id = Convert.ToInt32(m.StudentId);
                    var tt = acss.GetClassStudentName(yid, cid, sid);
                    ViewBag.stuname = tt;
                    ViewBag.na = stu_id;
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(cid);
                    ViewBag.ss = Ss.GetParticularSection(sid);
                    ViewBag.nn = StuSer.GetParticularStudentName(stu_id);
                    if (payid == 1)
                    {
                        ViewBag.pp = "Annual";
                        var ans1 = Fs.Feedetails(yid, cid);
                        TempData["res"] = ans1;
                        ViewBag.view = ans1.Count;
                    }
                    else if (payid == 2)
                    {
                        ViewBag.pp = "Term";
                        var tid = Convert.ToInt32(fm["Termtype"]);
                        ViewBag.termid = Ts.GetParticularTerm(tid);
                        ViewBag.term = Ts.GetTermList(yid);
                        ViewBag.t = tid;
                        var ans2 = tfs.TermFeedetails(yid, cid, payid, tid);
                        TempData["res"] = ans2;
                        ViewBag.view = ans2.Count;
                    }
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["res"];
            return View(ans);
        }
         [HttpPost]
        public JsonResult getFeesStructure(VM_GetFeeStructure s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdminServices ps = new PreAdminServices();
                    int yearid = Convert.ToInt16(s.AcademicYearId);
                    int classid = Convert.ToInt16(s.ClassId);
                    var result = ps.getallFeesByID(yearid, classid);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
         [HttpPost]
         public JsonResult getpaidFeesStructure(VM_GetPaidFeeStructure s)
         {
             try
             {
                 if (ModelState.IsValid)
                 {
                     PreAdminServices ps = new PreAdminServices();
                     int yearid = Convert.ToInt16(s.AcademicYearId);
                     int classid = Convert.ToInt16(s.ClassId);
                     int secid = Convert.ToInt16(s.SectionId);
                     int sturegid=Convert.ToInt16(s.StudentRegisterid);
                     var result = ps.getallFeesPaidByID(yearid, classid,secid,sturegid);
                     return Json(result, JsonRequestBehavior.AllowGet);
                 }
                 else
                 {
                     string totalError = "";
                     string[] t = new string[ModelState.Values.Count];
                     int i = 0;
                     foreach (var obj in ModelState.Values)
                     {
                         foreach (var error in obj.Errors)
                         {
                             if (!string.IsNullOrEmpty(error.ErrorMessage))
                             {
                                 totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                 t[i] = error.ErrorMessage;
                                 i++;
                             }
                         }
                     }
                     return Json(new { Success = 0, ex = t });
                 }
             }
             catch (Exception e)
             {
                 String ErrorMessage = e.Message;
                 return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
             }
         }
         [HttpPost]
         public JsonResult getDueFeesStructure(VM_GetPaidFeeStructure s)
         {
             try
             {
                 if (ModelState.IsValid)
                 {
                     PreAdminServices ps = new PreAdminServices();
                     int yearid = Convert.ToInt16(s.AcademicYearId);
                     int classid = Convert.ToInt16(s.ClassId);
                     int secid = Convert.ToInt16(s.SectionId);
                     int sturegid = Convert.ToInt16(s.StudentRegisterid);
                     var result = ps.getallFeesDueByID(yearid, classid, secid, sturegid);
                     return Json(result, JsonRequestBehavior.AllowGet);
                 }
                 else
                 {
                     string totalError = "";
                     string[] t = new string[ModelState.Values.Count];
                     int i = 0;
                     foreach (var obj in ModelState.Values)
                     {
                         foreach (var error in obj.Errors)
                         {
                             if (!string.IsNullOrEmpty(error.ErrorMessage))
                             {
                                 totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                 t[i] = error.ErrorMessage;
                                 i++;
                             }
                         }
                     }
                     return Json(new { Success = 0, ex = t });
                 }
             }
             catch (Exception e)
             {
                 String ErrorMessage = e.Message;
                 return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
             }
         }
        public ActionResult IndividualStudentAttendance()
        {
            return View();
        }
        [HttpPost]
        public ActionResult IndividualStudentAttendance(Mandatory m, FormCollection fm)
        {
            try
            {
                AcademicyearServices As = new AcademicyearServices();
                ClassServices Cs = new ClassServices();
                SectionServices Ss = new SectionServices();
                AssignClassToStudentServices acss = new AssignClassToStudentServices();
                MonthsServices Ms = new MonthsServices();
                  StudentServices StuSer = new StudentServices();
                  StudentDailyAttendanceServices sds = new StudentDailyAttendanceServices();
                if (ModelState.IsValid)
                {
                    TempData["show"] = "show";
                    ArrayList s1 = new ArrayList();
                    ArrayList roll = new ArrayList();
                    ArrayList r1 = new ArrayList();
                    ArrayList name = new ArrayList();
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.Classes = Cs.GetClass();
                    ViewBag.month = Ms.GetMonths();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    var cid = Convert.ToInt32(m.Class);
                    ViewBag.cls = cid;
                    var sid = Convert.ToInt32(m.Rpt_Section);
                    var e = Ss.GetSection(cid);
                    ViewBag.section = e;
                    ViewBag.sec = sid;
                    var stu_id = Convert.ToInt32(m.StudentId);
                    var tt = acss.GetClassStudentName(yid, cid, sid);
                    ViewBag.stuname = tt;
                    ViewBag.na = stu_id;
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(cid);
                    ViewBag.ss = Ss.GetParticularSection(sid);
                    ViewBag.nn = StuSer.GetParticularStudentName(stu_id);
                    var rid = Convert.ToInt32(m.ReportType);
                    ViewBag.reporttype = getrtype();
                    ViewBag.rid = rid;
                    var type = Convert.ToInt32(m.ReportType);
                    if (type == 1)
                    {
                        ViewBag.type = "daily";
                        DateTime da = Convert.ToDateTime(fm["Dates"]);
                        var ans1 = sds.GetParticularStudentDailyAttendance(yid, cid, sid, da, stu_id);
                        ViewBag.total = ans1.Count;
                        ViewBag.view = ans1.Count;
                        TempData["ans"] = ans1;
                        var p = sds.GetParticularStudentDailyPresentStatus(yid, cid, sid, da,stu_id);
                        ViewBag.NOP = p.Count;
                        var a1 = sds.GetParticularStudentDailyAbsentStatus(yid, cid, sid, da, stu_id);
                        ViewBag.NOA = a1.Count;
                        ViewBag.da = da.ToShortDateString();
                    }
                    else if (type == 2)
                    {
                        ViewBag.type = "from/to";
                        DateTime from1 = Convert.ToDateTime(fm["Fromdate"]);
                        DateTime to = Convert.ToDateTime(fm["Todate"]);
                        var ans1 = sds.GetParticularStudentFromToDateAttendance(yid, cid, sid, from1, to, stu_id);
                        foreach (var v in ans1)
                        {
                            string r = v.rollnumber;
                            string d = v.date.Day.ToString();
                            string na = v.stuname;
                            if (!roll.Contains(r))
                            { roll.Add(r); }
                            if (!s1.Contains(d))
                            {
                                s1.Add(d);
                            }
                            if (!name.Contains(na))
                            {
                                name.Add(na);
                            }
                        }
                        for (int i = 0; i < roll.Count; i++)
                        {
                            string r = roll[i].ToString();
                            var count = sds.GetParticularStudentFromToDatePresentStatus(yid, cid, sid, from1, to, r, stu_id);
                            r1.Add(count.Count);
                        }
                        ViewBag.NOP = r1;
                        ViewBag.count = name.Count;
                        ViewBag.date = s1;
                        ViewBag.from1 = from1.ToShortDateString();
                        ViewBag.to = to.ToShortDateString();
                        TempData["ans"] = ans1;
                        ViewBag.view = ans1.Count;
                    }
                    else if (type == 3)
                    {
                        ViewBag.type = "month";
                        var id = Convert.ToInt32(fm["month"]);
                        var ans1 = sds.GetParticularStudentMonthAttendance(yid, cid, sid, id, stu_id);
                        ViewBag.id = id;
                        foreach (var v in ans1)
                        {
                            string r = v.rollnumber;
                            string d = v.date.Day.ToString();
                            string na = v.stuname;
                            if (!roll.Contains(r))
                            { roll.Add(r); }
                            if (!s1.Contains(d))
                            {
                                s1.Add(d);
                            }
                            if (!name.Contains(na))
                            {
                                name.Add(na);
                            }
                        }
                        for (int i = 0; i < roll.Count; i++)
                        {
                            string r = roll[i].ToString();
                            var count = sds.GetParticularStudentMonthPresentStatus(yid, cid, sid, id,r, stu_id);
                            r1.Add(count.Count);
                        }
                        ViewBag.NOP = r1;
                        ViewBag.count = name.Count;
                        ViewBag.date1 = s1;
                        ViewBag.mid = Ms.GetParticularMonth(id);
                        TempData["ans"] = ans1;
                        ViewBag.view = ans1.Count;
                    }
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
         [HttpPost]
         public JsonResult JsonIndividualStudentAttendance(VM_IndividualStudentAttendance v)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    StudentDailyAttendanceServices sds = new StudentDailyAttendanceServices();
                    AcademicyearServices acyear = new AcademicyearServices();
                    if(v.ReportType ==1)
                    {
                        var ans1 = sds.GetParticularStudentDailyAttendance(v.AcademicYear, v.Class, v.Rpt_Section, v.Dates, v.StudentId);
                        return Json(new { AttendanceList = ans1 }, JsonRequestBehavior.AllowGet);
                    }
                    else if (v.ReportType == 2)
                    {
                        var ans1 = sds.GetParticularStudentFromToDateAttendance(v.AcademicYear, v.Class, v.Rpt_Section, v.FromDates, v.Todates, v.StudentId);
                        string[] roll = new string[100];
                        string[] s1 = new string[100];
                        int Roll_Count = 0;
                        int date_Count = 0;
                        foreach (var v1 in ans1)
                        {
                            string r = v1.rollnumber;
                            string d = v1.date.Day.ToString();
                            string na = v1.stuname;
                            if (!roll.Contains(r))
                            {
                                roll[Roll_Count]=r;
                                Roll_Count++;
                            }
                            if (!s1.Contains(d))
                            {
                                s1[date_Count] = d;
                                date_Count++;
                            }
                        }
                            string rr = roll[0].ToString();
                            var count = sds.GetParticularStudentFromToDatePresentStatus(v.AcademicYear, v.Class, v.Rpt_Section, v.FromDates, v.Todates, rr, v.StudentId);
                        return Json(new { AttendanceList = ans1, rollNumber = roll, Attendancedates = s1, Roll_Count = Roll_Count, date_Count = date_Count,Nop=count.Count }, JsonRequestBehavior.AllowGet);
                    }
                    else if (v.ReportType == 3)
                    {
                        var ans1 = sds.GetParticularStudentMonthAttendance(v.AcademicYear, v.Class, v.Rpt_Section,v.month,v.StudentId);
                        string[] roll = new string[100];
                        string[] s1 = new string[100];
                        int Roll_Count = 0;                        
                        foreach (var v1 in ans1)
                        {
                            string r = v1.rollnumber;
                            string d = v1.date.Day.ToString();
                            string na = v1.stuname;
                            if (!roll.Contains(r))
                            {
                                roll[Roll_Count] = r;
                                Roll_Count++;
                            }
                           
                        }

                        int y = acyear.GetmonthDates(Convert.ToInt16(v.month), v.AcademicYear);
                        int date_Count = DateTime.DaysInMonth(y, Convert.ToInt16(v.month));

                        for (int i = 0; i < date_Count; i++)
                        {
                            string date = v.month + "/" + (i + 1) + "/" + y;
                            DateTime dateValue = DateTime.Parse(date, CultureInfo.InvariantCulture);
                             DateTimeOffset dateOffsetValue = new DateTimeOffset(dateValue, 
                                      TimeZoneInfo.Local.GetUtcOffset(dateValue));
                            string ss;
                             ss = dateValue.ToString("ddd");
                             s1[i] = ss.Substring(0,2);
                        }
                            string rr = roll[0].ToString();
                            var count = sds.GetParticularStudentMonthPresentStatus(v.AcademicYear, v.Class, v.Rpt_Section,v.month, rr, v.StudentId);
                            return Json(new { AttendanceList = ans1, rollNumber = roll, Attendancedates = s1, Roll_Count = Roll_Count, date_Count = date_Count, Nop = count.Count }, JsonRequestBehavior.AllowGet);
                    }
                    string Success = "Success";
                    return Json(new { Success = Success }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
             catch(Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult StudentGradeProgressCard()
        {
            return View();
        }
        [HttpPost]
        public ActionResult StudentGradeProgressCard(Mandatory m)
        {
            try
            {
                AcademicyearServices As = new AcademicyearServices();
                ClassServices Cs = new ClassServices();
                SectionServices Ss = new SectionServices();
                AssignClassToStudentServices acss = new AssignClassToStudentServices();
                MonthsServices Ms = new MonthsServices();
                StudentServices StuSer = new StudentServices();
                AssignSubjectToSectionServices astss = new AssignSubjectToSectionServices();
                StudentGradeServices sgs = new StudentGradeServices();
                ExamServices Es = new ExamServices();
                if (ModelState.IsValid)
                {
                    ArrayList s1 = new ArrayList();
                    ArrayList tot = new ArrayList();
                    List<string> subj = new List<string>();
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.Classes = Cs.GetClass();
                    ViewBag.ExamType = Ms.GetMonths();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    var cid = Convert.ToInt32(m.Class);
                    ViewBag.cls = cid;
                    var sid = Convert.ToInt32(m.Rpt_Section);
                    ViewBag.section = Ss.GetSection(cid);
                    ViewBag.sec = sid;
                    var stu_id = Convert.ToInt32(m.StudentId);
                    var tt = acss.GetClassStudentName(yid, cid, sid);
                    ViewBag.stuname = tt;
                    ViewBag.na = stu_id;
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(cid);
                    ViewBag.ss = Ss.GetParticularSection(sid);
                    ViewBag.nn = StuSer.GetParticularStudentName(stu_id);
                    var s = astss.SearchClassSubject(yid, cid, sid);
                    foreach (var v in s)
                    {
                        s1.Add(v.subjectName);
                    }
                    ViewBag.subject = s1;
                    var ans1 = sgs.StudentGradeProgress(yid, cid, sid, stu_id);
                    TempData["ans"] = ans1;
                    ViewBag.view = ans1.Count;
                    var e = Es.GetExamNameList();
                    foreach (var v in e)
                    {
                        subj.Add(v.ExamName);
                    }
                    ViewBag.subj = subj;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
      [HttpPost]
         public JsonResult StudentProgressCardGrde(GradeProgressCard p)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    StudentGradeServices sgs = new StudentGradeServices();
                    AssignSubjectToSectionServices astss = new AssignSubjectToSectionServices();
                    
                    var ans1 = sgs.StudentGradeProgress(p.Acdemicyear, p.Class, p.Section, p.StudentId);
                    var s = astss.SearchClassSubject(p.Acdemicyear, p.Class, p.Section);
                    string[] SubjectList = new string[s.Count];
                    string[] ExamList= new string[10];
                    int k = 0;
                    foreach (var v in s)
                    {
                        SubjectList[k]= v.subjectName;
                        k++;
                    }
                    int y = 0;
                    foreach(var b in ans1)
                    {
                        if(!ExamList.Contains(b.ExamType))
                        {
                            ExamList[y] = b.ExamType;
                            y++;
                        }
                    }
                    return Json(new { Grades = ans1, Subjectlist = SubjectList, ExamList = ExamList,ExamList_Count=y }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
          catch(Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetSelectedClassSection(int passClassId)
        {
            SectionServices secc = new SectionServices();
            var List = secc.ShowSection(passClassId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getSectionStudent(int passYearId, int passClassId, int passSecId)
        {
            AssignClassToStudentServices clasStu = new AssignClassToStudentServices();
            var StudentList = clasStu.GetFacultyClassStudents(passYearId, passClassId, passSecId);
            return Json(StudentList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult StudentRemark()
        {
            return View();
        }
        [HttpPost]
        public ActionResult StudentRemark(R_StudentRemark s)
        {
            try
            {
                ClassInchargeStudentRemarkServices cis = new ClassInchargeStudentRemarkServices();
                SectionServices secc = new SectionServices();
                AssignClassToStudentServices clasStu = new AssignClassToStudentServices();
                if (ModelState.IsValid)
                {
                    int yid = s.R_Remarks_allAcademicYears;
                    int cId = s.R_Remarks_allClass;
                    int secId = Convert.ToInt32(s.R_Remarks_Section);
                    int stuId = Convert.ToInt32(s.R_Remarks_students);
                    DateTime from = s.From_date;
                    DateTime to = s.To_date;
                    var list = cis.GetStudentRemarks(s.R_Remarks_allAcademicYears, s.R_Remarks_allClass, secId, stuId, from, to);
                    ViewBag.secId = secId;
                    ViewBag.AllSection = secc.ShowSection(cId);
                    ViewBag.AllStudents = clasStu.GetFacultyClassStudents(s.R_Remarks_allAcademicYears, cId, secId);
                    ViewBag.stuId = stuId;
                    ViewBag.count = list;
                    return View(list);
                }
                else
                {
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
         [HttpPost]
         public JsonResult JsonStudentRemark(R_StudentRemark s)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    ClassInchargeStudentRemarkServices cis = new ClassInchargeStudentRemarkServices();
                    int yid = s.R_Remarks_allAcademicYears;
                    int cId = s.R_Remarks_allClass;
                    int secId = Convert.ToInt32(s.R_Remarks_Section);
                    int stuId = Convert.ToInt32(s.R_Remarks_students);
                    DateTime from = s.From_date;
                    DateTime to = s.To_date;
                    var list = cis.GetStudentRemarks(s.R_Remarks_allAcademicYears, s.R_Remarks_allClass, secId, stuId, from, to);
                    return Json(list, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
             catch(Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        protected override void Dispose(bool disposing)
        {
            ee.Dispose();
            base.Dispose(disposing);
        }
    }
}