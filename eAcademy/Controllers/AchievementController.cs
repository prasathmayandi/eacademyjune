﻿using eAcademy.DataModel;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eAcademy.ViewModel;
using iTextSharp;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using eAcademy.Helpers;
using System.ComponentModel.DataAnnotations;
using eAcademy.HelperClass;
using eAcademy.Models;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class AchievementController : Controller
    {
        AcademicyearServices AcYear = new AcademicyearServices();
        StudentAchievementServices stuAchievement = new StudentAchievementServices();
        EmployeeAchievementServices empAchievement = new EmployeeAchievementServices();
        SchoolAchievementServices schoolAchievement = new SchoolAchievementServices();
        UserActivityHelper useractivity = new UserActivityHelper();

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {         
            ClassServices clas = new ClassServices();
            TechemployeesServices emp = new TechemployeesServices();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.XallAcademicYears = AcYear.AddFormAcademicYear();
            ViewBag.allClasses = clas.ShowClasses();
            ViewBag.allEmployees = emp.ShowTechFaculties();
        }
        public JsonResult GetSelectedClassSection(int passClassId)
        {
            SectionServices sec = new SectionServices();
            var List = sec.getSection(passClassId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSectionStudents(int YearId,int passClassId, int passSectionId)
        {
            AssignClassToStudentServices stu = new AssignClassToStudentServices();
            var StudentList = stu.getStudents(YearId, passClassId,passSectionId);
            return Json(StudentList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDate(int acid)
        {
            try
            {
                var ans = AcYear.date(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                return Json(new { sd = sdate, ed = edate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CurrentActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    int acid = CurrentAcYear[i].AcademicYearId;                    
                    return Json(new { sd = StartingEnableDate, ed = EndingEnableDate}, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AchievementResults(string sidx, string sord, int page, int rows, string Achievement, DateTime? txt_doa_search, int? Ach_allAcademicYearsA1)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var StudentAchievementList = stuAchievement.getStudentAchievement();
            if (txt_doa_search.HasValue)
            {
                StudentAchievementList = StudentAchievementList.Where(p => p.txt_doa_search >= txt_doa_search).ToList();
            }
            if (!string.IsNullOrEmpty(Achievement))
            {
                StudentAchievementList = StudentAchievementList.Where(p => p.Achievement.Contains(Achievement)).ToList();
            }
            if (Ach_allAcademicYearsA1.HasValue)
            {
                StudentAchievementList = StudentAchievementList.Where(p => p.Ach_allAcademicYearsA1 == Ach_allAcademicYearsA1).ToList();
            }
            int totalRecords = StudentAchievementList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    StudentAchievementList = StudentAchievementList.OrderByDescending(s => s.Achievement).ToList();
                    StudentAchievementList = StudentAchievementList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    StudentAchievementList = StudentAchievementList.OrderBy(s => s.Achievement).ToList();
                    StudentAchievementList = StudentAchievementList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = StudentAchievementList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = StudentAchievementList
            };
            
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StudentAchievement(bool? pdf)
        {
            try
            {
                if (!pdf.HasValue)
                {
                    return View();
                }
                else
                {
                    List<AchievementList> StudentAchievementList = (List<AchievementList>)Session["JQGridList"];
                    string filePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                    string heading =  ResourceCache.Localize("StudentAchievementList");
                    GeneratePDF.ExportPDF_Portrait(StudentAchievementList, new string[] { "Date", "Achievement", "Descriptions", "Name", "Class", "Section" }, filePath, heading);
                    return File(filePath, "application/pdf", ResourceCache.Localize("StudentAchievement.pdf"));
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public JsonResult StudentAchievement(AddStudentAchievement a)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int yearId = a.Ach_allAcademicYears;
                    int classId = a.Ach_allClass;
                    int secId = Convert.ToInt32(a.Ach_Section);
                    int studentId = Convert.ToInt32(a.Ach_SectionStudent);
                    DateTime doa = Convert.ToDateTime(a.txt_doa);
                    string achievement = a.txt_achievement;
                    string desc = a.txt_description;
                    stuAchievement.addStudentAchievement(yearId, classId, secId, studentId, doa, achievement, desc);
                    string Message = ResourceCache.Localize("StudentAchievementSavedSuccessfully");
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddStudentAchievement"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }      
        public JsonResult EditStudentAchievement(string passId)
        {
            try
            {
                string eid = QSCrypt.Decrypt(passId);
                int id = Convert.ToInt32(eid);
                var list = stuAchievement.getAchievementById(id);
                var date = list.DateOfAchievement.Value.ToString("dd/MM/yyyy");
                var achievement = list.Achievement;
                var desc = list.Descriptions;
                return Json(new { date, achievement, desc }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult EditStudentAchievement(EditStudentAchievement b)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DateTime doa = Convert.ToDateTime(b.txt_EditDoa);
                    string achievement = b.txt_EditAchievement;
                    string desc = b.txt_EditDescription;
                    string eid = QSCrypt.Decrypt(b.AchievementEId);
                    int id = Convert.ToInt32(eid);
                    int AchievementEId = id;
                    stuAchievement.updateStudentAchievement(doa, achievement, desc, AchievementEId);
                    string Message = ResourceCache.Localize("StudentAchievementDetailsUpdatedSuccessfully");
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdateStudentAchievement"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DeleteStudentAchievement(string passId)
        {
            try
            {
                string Did = QSCrypt.Decrypt(passId);
                int id = Convert.ToInt32(Did);
                stuAchievement.delStudentAchievement(id);
                string Message = ResourceCache.Localize("StudentAchievementDetailsDeletedSuccessfully");
                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("DeleteStudentAchievement"));
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ExportToExcel()
        {
            List<AchievementList> StudentAchievementList = (List<AchievementList>)Session["JQGridList"];
            var list = StudentAchievementList.Select(o => new { Date = o.Date, Achievement = o.Achievement, Description = o.Descriptions, Name = o.Name, Class = o.Class, section = o.Section}).ToList();
            GenerateExcel.ExportExcel(list);
            return View("StudentAchievement");
        }
        public JsonResult FacultyAchievementResults(string sidx, string sord, int page, int rows, string Achievement, DateTime? txt_doa_search, int? Ach_allAcademicYearsA1)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var FacultyAchievementList = empAchievement.getFacultyAchievement();
            if (txt_doa_search.HasValue)
            {
                FacultyAchievementList = FacultyAchievementList.Where(p => p.txt_doa_search >= txt_doa_search).ToList();
            }
            if (!string.IsNullOrEmpty(Achievement))
            {
                FacultyAchievementList = FacultyAchievementList.Where(p => p.Achievement.Contains(Achievement)).ToList();
            }
            if (Ach_allAcademicYearsA1.HasValue)
            {
                FacultyAchievementList = FacultyAchievementList.Where(p => p.Ach_allAcademicYearsA1 == Ach_allAcademicYearsA1).ToList();
            }
            int totalRecords = FacultyAchievementList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    FacultyAchievementList = FacultyAchievementList.OrderByDescending(s => s.Achievement).ToList();
                    FacultyAchievementList = FacultyAchievementList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    FacultyAchievementList = FacultyAchievementList.OrderBy(s => s.Achievement).ToList();
                    FacultyAchievementList = FacultyAchievementList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = FacultyAchievementList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = FacultyAchievementList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FacultyAchievement(bool? pdf)
        {
            try
            {
                if (!pdf.HasValue)
                {
                    return View();
                }
                else
                {
                    List<AchievementList> FacultyAchievementList = (List<AchievementList>)Session["JQGridList"];
                    string filePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                    string heading = ResourceCache.Localize("FacultyAchievementList");
                    GeneratePDF.ExportPDF_Portrait(FacultyAchievementList, new string[] { "Date", "Achievement", "Descriptions", "Name", "EmployeeId" }, filePath, heading);
                    return File(filePath, "application/pdf", ResourceCache.Localize("FacultyAchievement.pdf"));
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult AddFacultyAchievement(AddFacultyAchievement m)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    int yearId = m.Ach_allAcademicYears;
                    int EmployeeId = m.allEmployees;
                    DateTime doa = m.txt_doa.Value;
                    string achievement = m.txt_achievement;
                    string desc = m.txt_description;
                    empAchievement.addFacultyAchievement(yearId, EmployeeId, doa, achievement, desc);
                    string Message = ResourceCache.Localize("FacultyAchievementSavedSuccessfully");
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddFacultyAchievement"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }              
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditFacultyAchievement(string passId)
        {
            try
            {
                string eid = QSCrypt.Decrypt(passId);
                int id = Convert.ToInt32(eid);
                var list = empAchievement.getAchievementById(id);
                var date = list.DateOfAchievement.Value.ToString("dd/MM/yyyy");
                var achievement = list.Achievement;
                var desc = list.Descriptions;
                return Json(new { date, achievement, desc }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult EditFacultyAchievement(EditFacultyAchievement m)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    DateTime doa = Convert.ToDateTime(m.txt_EditDoa);
                    string achievement = m.txt_EditAchievement;
                    string desc = m.txt_EditDescription;
                    string eid = QSCrypt.Decrypt(m.AchievementEId);
                    int id = Convert.ToInt32(eid);
                    int AchievementEId = id;
                    empAchievement.updateFacultyAchievement(doa, achievement, desc, AchievementEId);
                    string Message = ResourceCache.Localize("FacultyAchievementDetailsUpdatedSuccessfully");
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdateFacultyAchievement"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }     
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DeleteFacultyAchievement(string passId)
        {
            try
            {
                string Did = QSCrypt.Decrypt(passId);
                int id = Convert.ToInt32(Did);
                empAchievement.delFacultyAchievement(id);
                string Message = ResourceCache.Localize("FacultyAchievementDetailsDeletedSuccessfully");
                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("DeleteFacultyAchievement"));
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }        
        }
        public ActionResult FacultyAchievementExportToExcel()
        {
            List<AchievementList> FacultyAchievementList = (List<AchievementList>)Session["JQGridList"];
            var list = FacultyAchievementList.Select(o => new { Date = o.Date, Achievement = o.Achievement, Description = o.Descriptions, Name = o.Name }).ToList();
            GenerateExcel.ExportExcel(list);
            return View("FacultyAchievement");
        }
        public JsonResult SchoolAchievementResults(string sidx, string sord, int page, int rows, string Achievement, DateTime? txt_doa_search, int? Ach_allAcademicYearsA1)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var SchoolAchievementList = schoolAchievement.getSchoolAchievement();
            if (txt_doa_search.HasValue)
            {
                SchoolAchievementList = SchoolAchievementList.Where(p => p.txt_doa_search >= txt_doa_search).ToList();
            }
            if (!string.IsNullOrEmpty(Achievement))
            {
                SchoolAchievementList = SchoolAchievementList.Where(p => p.Achievement.Contains(Achievement)).ToList();
            }
            if (Ach_allAcademicYearsA1.HasValue)
            {
                SchoolAchievementList = SchoolAchievementList.Where(p => p.Ach_allAcademicYearsA1 == Ach_allAcademicYearsA1).ToList();
            }
            int totalRecords = SchoolAchievementList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    SchoolAchievementList = SchoolAchievementList.OrderByDescending(s => s.Achievement).ToList();
                    SchoolAchievementList = SchoolAchievementList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    SchoolAchievementList = SchoolAchievementList.OrderBy(s => s.Achievement).ToList();
                    SchoolAchievementList = SchoolAchievementList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = SchoolAchievementList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = SchoolAchievementList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SchoolAchievement(bool? pdf)
        {
            try
            {
                if (!pdf.HasValue)
                {
                    return View();
                }
                else
                {
                    List<AchievementList> SchoolAchievementList = (List<AchievementList>)Session["JQGridList"];
                    string filePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                    string heading = ResourceCache.Localize("SchoolAchievementList");
                    GeneratePDF.ExportPDF_Portrait(SchoolAchievementList, new string[] { "Date", "Achievement", "Descriptions", "Name" }, filePath,heading);
                    return File(filePath, "application/pdf", ResourceCache.Localize("SchoolAchievement.pdf"));
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult AddSchoolAchievement(AddSchoolAchievement m)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    int yearId = m.Ach_allAcademicYears;
                    DateTime doa = m.txt_doa.Value;
                    string achievement = m.txt_achievement;
                    string desc = m.txt_description;
                    string people = m.txt_people;
                    schoolAchievement.addSchoolAchievement(yearId, doa, achievement, desc, people);
                    string Message = ResourceCache.Localize("SchoolAchievementSavedSuccessfully");
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddSchoolAchievement"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }       
        }
        public ActionResult EditSchoolAchievement(string passId)
        {
            try
            {
                string eid = QSCrypt.Decrypt(passId);
                int id = Convert.ToInt32(eid);
                var list = schoolAchievement.getAchievementById(id);
                var date = list.DateOfAchievement.Value.ToString("dd/MM/yyyy");
                var achievement = list.Achievement;
                var desc = list.Descriptions;
                var peopleInvolved = list.PeopleInvolved;
                return Json(new { date, achievement, desc, peopleInvolved }, JsonRequestBehavior.AllowGet);
               
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult EditSchoolAchievement(EditSchoolAchievement m)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    DateTime doa = Convert.ToDateTime(m.txt_EditDoa);
                    string achievement = m.txt_EditAchievement;
                    string desc = m.txt_EditDescription;
                    string people = m.txt_EditPeople;
                    string eid = QSCrypt.Decrypt(m.AchievementEId);
                    int id = Convert.ToInt32(eid);
                    int AchievementEId = id;
                    schoolAchievement.updateSchoolAchievement(doa, achievement, desc, AchievementEId, people);
                    string Message = ResourceCache.Localize("SchoolAchievementDetailsUpdatedSuccessfully");
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdateSchoolAchievement"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }      
        }
        public ActionResult DeleteSchoolAchievement(string passId)
        {
            try
            {
                string Did = QSCrypt.Decrypt(passId);
                int id = Convert.ToInt32(Did);
                schoolAchievement.delSchoolAchievement(id);
                string Message = ResourceCache.Localize("StudentAchievementDetailsDeletedSuccessfully");
                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("DeleteSchoolAchievement")); 
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SchoolAchievementExportToExcel()
        {
            List<AchievementList> SchoolAchievementList = (List<AchievementList>)Session["JQGridList"];
            var list = SchoolAchievementList.Select(o => new { Date = o.Date, Achievement = o.Achievement, Description = o.Descriptions, Name = o.Name }).ToList();
            GenerateExcel.ExportExcel(list);
            return View("FacultyAchievement");
        }
    }
}