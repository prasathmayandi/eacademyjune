﻿using eAcademy.DataModel;
using eAcademy.ViewModel;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Collections;
using eAcademy.Models;
using eAcademy.Helpers;
using eAcademy.HelperClass;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class ClassInchargeController : Controller
    {      
        // GET: /ClassIncharge/

        AssignClassTeacherServices clasTeacher = new AssignClassTeacherServices();
        ParentLeaveRequestServices LeavReq = new ParentLeaveRequestServices();
        public int ClassInchargeId;
        UserActivityHelper useractivity = new UserActivityHelper();

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ClassInchargeId = Convert.ToInt32(Session["empRegId"]);
            AcademicyearServices AcYear = new AcademicyearServices();
            ExamServices Exam = new ExamServices();
            ClassServices clas = new ClassServices();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.allExams = Exam.ShowExams();


            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = ResourceCache.Localize("All"), Value = "All" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("Pending"), Value = "Pending" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("Approved"), Value = "Approved" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("Declined"), Value = "Declined" });
            ViewBag.StatusList = li;
            ViewBag.allClasses = clas.ShowClasses(ClassInchargeId);
        }

        public JsonResult GetStudents(int passYearId, int passClassId, int passSecId)
        {
            try
            {
                AssignClassToStudentServices stu = new AssignClassToStudentServices();
                var StudentList = stu.getStudents1(passYearId, passClassId, passSecId);
                return Json(StudentList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult showSection(int passClassId)
        {
            try
            {
                AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
                var SecList = facultySub.ShowSections(ClassInchargeId);
                return Json(SecList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public IEnumerable<Object> ShowStudents(int yearId)
        {
            var list = clasTeacher.getClassTeacherSection(ClassInchargeId, yearId);

            int classId = Convert.ToInt32(list.ClassId);
            int secId = Convert.ToInt32(list.SectionId);

            AssignClassToStudentServices stu = new AssignClassToStudentServices();
            var StudentList = stu.getStudents1(yearId, classId, secId);
            return StudentList;
        }

        public IEnumerable<Object> ShowStudents1(int yearId, int classId, int secId)
        {
            AssignClassToStudentServices stu = new AssignClassToStudentServices();
            var StudentList = stu.getStudents1(yearId, classId, secId);
            return StudentList;
        }

        public JsonResult getSectionStudent(int passYearId, int passClassId, int passSecId)
        {
            AssignClassToStudentServices stu = new AssignClassToStudentServices();
            var StudentList = stu.getStudents1(passYearId, passClassId, passSecId);
            return Json(StudentList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getClassWithSection(int passYearId)
        {
            var list = clasTeacher.getClassWithSection(ClassInchargeId, passYearId);
            Session["CurrentAcYearId"] = passYearId;
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public IEnumerable<Object> ShowClassWithSection(int yearId)
        {
            var list = clasTeacher.getClassWithSection(ClassInchargeId, yearId);
            return list;
        }

        // student List
        public ActionResult StudentList()
        {
            return View();
        }


        public JsonResult StudentListResults(string sidx, string sord, int page, int rows, int? CI_allAcademicYears, string CI_ClassWithSection)
        {
            IList<ClassInchargeStudentsList> StudentList1 = new List<ClassInchargeStudentsList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (CI_allAcademicYears != 0 && CI_ClassWithSection != null)
            {
                string clasSec = CI_ClassWithSection;
                string[] words = clasSec.Split(' ');
                int classId = Convert.ToInt32(words[0]);
                int secId = Convert.ToInt32(words[1]);
                AssignClassToStudentServices stu = new AssignClassToStudentServices();
                StudentList1 = stu.getClassInchargeStudents(CI_allAcademicYears, classId, secId);
            }
            int totalRecords = StudentList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    StudentList1 = StudentList1.OrderByDescending(s => s.RollNumber).ToList();
                    StudentList1 = StudentList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    StudentList1 = StudentList1.OrderBy(s => s.RollNumber).ToList();
                    StudentList1 = StudentList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = StudentList1;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = StudentList1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StudentList_ExportToExcel()
        {
            try
            {
                List<ClassInchargeStudentsList> StudentList1 = (List<ClassInchargeStudentsList>)Session["JQGridList"];
                var list = StudentList1.Select(o => new { RollNumber = o.RollNumber, StudentName = o.StudentName, Gender = o.Gender, EmergencyContactPerson = o.EmergencyContactPerson, EmergencyContactNumber = o.EmergencyContactNumber, Address = o.Address }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("StudentListXlsx");
                GenerateExcel.ExportExcel(list, fileName);
                return View("StudentList");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult StudentList_ExportToPdf()
        {
            try
            {
                List<ClassInchargeStudentsList> StudentList1 = (List<ClassInchargeStudentsList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("StudentList");
                GeneratePDF.ExportPDF_Landscape(StudentList1, new string[] { "RollNumber", "StudentName", "Gender", "EmergencyContactPerson", "EmergencyContactNumber", "Address" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("StudentList.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult StudentDetails(string id)
        {
            try
            {
                int StudentRegId = Convert.ToInt32(QSCrypt.Decrypt(id));
                AdmissionTransactionServices admissionTransact = new AdmissionTransactionServices();
                var ParentRegId = admissionTransact.getPrimaryUserId(StudentRegId).PrimaryUserRegisterId.Value;

                var getStudentParentDetails = admissionTransact.checkStudentParentProfileExist(ParentRegId, StudentRegId);
                if (getStudentParentDetails != null)
                {
                    int? FatherRegId = getStudentParentDetails.FatherRegisterId;
                    int? MotherRegId = getStudentParentDetails.MotherRegisterId;
                    int? GuardianRegId = getStudentParentDetails.GuardianRegisterId;
                    var list = admissionTransact.getStudentParentDetails(ParentRegId, StudentRegId);

                    StudentServices stuser = new StudentServices();
                    var row = stuser.getStudentRow(StudentRegId);
                    if (row != null)
                    {
                        int? bloodGroupid = row.StudentBloodGroup;
                        if (bloodGroupid != null)
                        {
                            var blood = admissionTransact.getBloodGroup(bloodGroupid);
                            if (blood != null)
                            {
                                var BloodGroup = blood.BloodGroupName;
                                ViewBag.StudentBloodGroup = BloodGroup;
                            }
                        }
                        string localCountryId = row.LocalCountry;
                        if (localCountryId != null)
                        {
                            
                            ViewBag.StudentCountryname = localCountryId;
                        }
                    }
                    if (FatherRegId != null)
                    {
                        var getFatherDetail = admissionTransact.getFatherDetail(ParentRegId, StudentRegId, FatherRegId);
                        ViewBag.FatherName = getFatherDetail.FatherName;
                        ViewBag.FatherDOB = getFatherDetail.FatherDob;
                        ViewBag.FatherEmail = getFatherDetail.FatherEmail;
                        ViewBag.FatherMobile = getFatherDetail.FatherMobile;
                        ViewBag.FatherOccupation = getFatherDetail.FatherOccupation;
                        ViewBag.FatherQualification = getFatherDetail.FatherQualification;
                    }
                    if (MotherRegId != null)
                    {
                        var getMotherDetail = admissionTransact.getMotherDetail(ParentRegId, StudentRegId, MotherRegId);
                        ViewBag.MotherName = getMotherDetail.MotherName;
                        ViewBag.MotherDOB = getMotherDetail.MotherDob;
                        ViewBag.MotherEmail = getMotherDetail.MotherEmail;
                        ViewBag.MotherMobile = getMotherDetail.MotherMobile;
                        ViewBag.MotherOccupation = getMotherDetail.MotherOccupation;
                        ViewBag.MotherQualification = getMotherDetail.MotherQualification;
                    }
                    if (GuardianRegId != null)
                    {
                        var getGuardianDetail = admissionTransact.getGuardianDetail(ParentRegId, StudentRegId, GuardianRegId);
                        ViewBag.GuardianName = getGuardianDetail.GuardianName;
                        ViewBag.GuardianGender = getGuardianDetail.GuardianGender;
                        ViewBag.GuardianRelationship = getGuardianDetail.GuardianRelationship;
                        ViewBag.GuardianDob = getGuardianDetail.GuardianDob;
                        ViewBag.GuardianEmail = getGuardianDetail.GuardianEmail;
                        ViewBag.GuardianMobile = getGuardianDetail.GuardianMobile;
                        ViewBag.GuardianQualification = getGuardianDetail.GuardianQualification;
                        ViewBag.GuardianIncome = getGuardianDetail.GuardianIncome;
                    }
                    return View(list);
                }
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        // Class Attendance
        tblAttendanceStatusServices attStatus = new tblAttendanceStatusServices();

        public JsonResult GetAttendanceStatusList()
        {
            try
            {
                var attStatusList = attStatus.AttendanceStatusList();
                return Json(attStatusList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ClassAttendance()
        {
            return View();
        }

        public JsonResult GetStudentListToMarkAttendance(int passYearId, int passClassId, int passSecId, DateTime passDate)
        {
            try
            {
                StudentDailyAttendanceServices stuAttendance = new StudentDailyAttendanceServices();
                var checkDataExist = stuAttendance.AttendanceExist(passYearId, passClassId, passSecId, passDate);
                if (checkDataExist != null)
                {
                    var ExistList = stuAttendance.getAttendanceExistList(passYearId, passClassId, passSecId, passDate);
                    string status = "P";
                    for (int i = 0; i < ExistList.Count; i++)
                    {
                        string AttStatus = ExistList[i].status;
                        if (AttStatus != "P")
                        {
                            status = "not_P";
                        }
                    }
                    return Json(new { msg = "old", ExistList = ExistList, status }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    AssignClassToStudentServices stu = new AssignClassToStudentServices();
                    var NewList = stu.getStudentListToMarkAttendance(passYearId, passClassId, passSecId);
                    int Count = NewList.ToList().Count;
                    if (Count == 0)
                    {
                        string StudentCount = "empty";
                        return Json(new { StudentCount }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { msg = "new", list = NewList }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult AddStudentAttendance(AM_AddStudentAttendance am)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int YearId = Convert.ToInt32(am.Att_allAcademicYears);
                    int currentAcYearId = Convert.ToInt32(Session["CurrentAcYearId"]);
                    if (YearId == currentAcYearId)
                    {
                        var TotalCount = am.TotalCount;
                        int year = am.Att_allAcademicYears;
                        int classId = am.Att_class;
                        int secId = am.Att_sec;
                        DateTime date = Convert.ToDateTime(am.txt_AttendanceDate);
                        int size = TotalCount + 1;
                        string[] Id = new string[size];
                        string[] atn_comment = new string[size];
                        string[] atn_status = new string[size];

                        StudentDailyAttendanceServices stuAttendance = new StudentDailyAttendanceServices();
                        var checkDataExist = stuAttendance.AttendanceExist(year, classId, secId, date);
                        if (checkDataExist != null)
                        {
                            for (int i = 0; i <= TotalCount; i++)
                            {
                                Id[i] = QSCrypt.Decrypt(am.Id[i]);
                                int RegId = Convert.ToInt32(Id[i]);
                                atn_status[i] = am.atn_status[i];
                                atn_comment[i] = am.atn_comment[i];
                                DateTime updatedDate = DateTimeByZone.getCurrentDate();
                                stuAttendance.updateStudentDailyAttendance(RegId, year, classId, secId, date, atn_status[i], atn_comment[i], updatedDate, ClassInchargeId);
                            }
                            string Message = ResourceCache.Localize("AttendanceUpdatedSuccessfully");
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Update_student_daily_attendance"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            for (int i = 0; i <= TotalCount; i++)
                            {
                                Id[i] = QSCrypt.Decrypt(am.Id[i]);
                                int StuRegId = Convert.ToInt32(Id[i]);
                                atn_status[i] = am.atn_status[i];
                                atn_comment[i] = am.atn_comment[i];
                                DateTime markedDate = DateTimeByZone.getCurrentDate();
                                stuAttendance.addStudentDailyAttendance(StuRegId, year, classId, secId, date, atn_status[i], atn_comment[i], ClassInchargeId);
                            }
                            string Message = ResourceCache.Localize("Attendance_saved _Successfully");
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Add_student_daily_attendance"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string InfoMessage = ResourceCache.Localize("Records_can_save_for_current_academic_year_only");
                        return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AttendanceReport()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AttendanceReport(CI_AttendanceReport ar)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int yearId = ar.CI_allAcademicYears;
                    string clasSec = ar.CI_ClassWithSection;
                    string[] words = clasSec.Split(' ');
                    int classId = Convert.ToInt32(words[0]);
                    int secId = Convert.ToInt32(words[1]);
                    int studentRegId = Convert.ToInt32(ar.CI_allStudents);
                    DateTime fromDate = ar.txt_FromDate;
                    DateTime toDate = ar.txt_ToDate;
                    string PresentStatus = "P";
                    string AbsentStatus = "A";
                    string Halfday = "H";

                    
                    eAcademy.Services.StudentDailyAttendanceServices stuAttendance = new eAcademy.Services.StudentDailyAttendanceServices();
                    var NumberOfWorkingDays = stuAttendance.getWorkingDaysCountNew(yearId, classId, secId, fromDate, toDate);

                    var getPresentlist = stuAttendance.getPresentStudent(yearId, classId, secId, studentRegId, fromDate, toDate, PresentStatus).ToList();
                    double PresentDays = getPresentlist.Count;
                    var PresentCount = getPresentlist.Count;

                    var getAbsentlist = stuAttendance.getPresentStudent(yearId, classId, secId, studentRegId, fromDate, toDate, AbsentStatus).ToList();
                    var AbsentCount = getAbsentlist.Count;

                    int HalfDaysCount = stuAttendance.getPresentStudent(yearId, classId, secId, studentRegId, fromDate, toDate, Halfday).ToList().Count;
                    var HalfDaysCountVal = HalfDaysCount;
                    double totalPresentDays; double Half_FulldayCount = 0.0; double remainingHalfDay = 0.0; double HalfDayCount;

                    if (HalfDaysCount > 1)
                    {
                        Half_FulldayCount = (HalfDaysCount / 2);
                        HalfDayCount = (HalfDaysCount % 2);
                        if (HalfDayCount == 1)
                        {
                            remainingHalfDay = 0.5;
                        }
                    }
                    else if (HalfDaysCount == 1)
                    {
                        remainingHalfDay = 0.5;
                    }
                    totalPresentDays = PresentDays + Half_FulldayCount + remainingHalfDay;
                    double attendancePercent = ((double)totalPresentDays / (double)NumberOfWorkingDays) * 100;
                    var attendancePercentage = attendancePercent.ToString("##.##");
                    if (attendancePercentage == "NaN")
                    {
                        attendancePercentage = "0";
                    }
                    var f = attendancePercent.ToString("##.##");
                    string Message = "success";
                    return Json(new { Message, PresentCount, AbsentCount, HalfDaysCountVal, attendancePercentage, NumberOfWorkingDays }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LeaveRequest()
        {
            ViewBag.Message = TempData["Success"];
            ViewBag.InfoMsg = TempData["InfoMessage"];
            return View();
        }

        [HttpPost]
        public ActionResult LeaveRequest(LeaveRequestListForCI l)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DateTime fdate = l.SM_txt_FromDate_List;
                    DateTime toDate = l.SM_txt_ToDate_List;
                    ViewBag.OnSubmit = "X";
                    if (l.Status == "All")
                    {
                        var list = LeavReq.getParentLeaveRequest(ClassInchargeId, fdate, toDate).ToList();
                        var count = list.Count;
                        ViewBag.list = count;
                        if (count == 0)
                        {
                            ViewBag.val = "empty";
                        }
                        ViewBag.fDate = fdate.ToString("dd/MM/yyyy");
                        ViewBag.toDate = toDate.ToString("dd/MM/yyyy");
                        return View(list);
                    }
                    else
                    {
                        var list = LeavReq.getParentLeaveRequest(ClassInchargeId, fdate, toDate, l.Status).ToList();
                        var count = list.Count;
                        ViewBag.list = count;
                        if (count == 0)
                        {
                            ViewBag.val = "empty";
                        }
                        ViewBag.fDate = fdate.ToString("dd/MM/yyyy");
                        ViewBag.toDate = toDate.ToString("dd/MM/yyyy");
                        return View(list);
                    }
                }
                ViewBag.fDate = l.SM_txt_FromDate_List.ToString("dd/MM/yyyy");
                ViewBag.toDate = l.SM_txt_ToDate_List.ToString("dd/MM/yyyy");
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        [HttpPost]
        [ActionName("LeaveRequestDecision")]
        [eAcademy.Filters.InitializeSimpleMembershipAttribute.OnAction(ButtonName = "BtnApprove")]
        public ActionResult Decision_1(FormCollection fm)
        {
            try
            {
                int count = Convert.ToInt32(fm["count"]);
                var k = "selectStudent";
                var p = "Req_id";

                for (int i = 1; i <= count; i++)
                {
                    var k2 = k + i;
                    var p2 = p + i;
                    var status = fm[k2];

                    if (status == "on")
                    {
                        int Req_id = Convert.ToInt32(fm[p2]);
                        string Status = "Approved";
                        LeavReq.updateParentLeaveRequest(Req_id, Status);

                        var getLeaveDates = LeavReq.getLeaveDates(Req_id, Status);
                        int? stuRegId = getLeaveDates.StudentRegisterId;
                        int? yearId = getLeaveDates.AcademicYearId;
                        int? classId = getLeaveDates.ClassId;
                        int? secId = getLeaveDates.SectionId;
                        DateTime fromDate = Convert.ToDateTime(getLeaveDates.FromDate);
                        DateTime toDate = Convert.ToDateTime(getLeaveDates.ToDate);
                        var Reason = getLeaveDates.LeaveReason;
                        var AttStatus = "L";
                        DateTime markedDate = DateTimeByZone.getCurrentDate();

                        StudentDailyAttendanceServices stuAttendance = new StudentDailyAttendanceServices();
                        for (DateTime fdate = fromDate; toDate.CompareTo(fdate) >= 0; fdate = fdate.AddDays(1.0))
                        {
                            stuAttendance.addStudentDailyAttendanceOnLeaveAccept(stuRegId, yearId, classId, secId, fdate, AttStatus, Reason, markedDate);
                        }
                    }
                }
                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Approved_student_leave_request"));
                TempData["Success"] = ResourceCache.Localize("Leave_request_approved_successfully");
                return RedirectToAction("LeaveRequest");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        [HttpPost]
        [ActionName("LeaveRequestDecision")]
        [eAcademy.Filters.InitializeSimpleMembershipAttribute.OnAction(ButtonName = "BtnReject")]
        public ActionResult Decision_2(FormCollection fm)
        {
            try
            {
                int count = Convert.ToInt32(fm["count"]);
                var k = "selectStudent";
                var p = "Req_id";

                for (int i = 1; i <= count; i++)
                {
                    var k2 = k + i;
                    var p2 = p + i;
                    var status = fm[k2];

                    if (status == "on")
                    {
                        int Req_id = Convert.ToInt32(fm[p2]);
                        string Status = "Declined";
                        LeavReq.updateParentLeaveRequest(Req_id, Status);
                    }
                }
                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Declined_student_leave_request"));
                TempData["Success"] = ResourceCache.Localize("Leave_request_Declined_successfully");
                return RedirectToAction("LeaveRequest");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult CreateProgressCard()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateProgressCard(CreateProgressCard cp)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    int yearId = cp.PC_allAcademicYears;
                    string clasSec = cp.CI_ClassWithSection;
                    string[] words = clasSec.Split(' ');
                    int classId = Convert.ToInt32(words[0]);
                    int secId = Convert.ToInt32(words[1]);
                    int examId = cp.allExams;

                    StudentMarkServices mark = new StudentMarkServices();
                    MarkTotalServices markTotal = new MarkTotalServices();

                    var getMarkList = mark.getMarkList(yearId, classId, secId, examId);
                    ViewBag.MarkListCount = getMarkList.Count;

                    var IsTotalCalculated = markTotal.MarkTotalExist(yearId, classId, secId, examId);
                    if (IsTotalCalculated == null)
                    {
                        AssignSubjectToSectionServices sub = new AssignSubjectToSectionServices();
                        var subjectList = sub.getSubjectList(yearId, classId, secId).ToList();

                        if (subjectList.Count == getMarkList.Count)
                        {
                            string Message = ResourceCache.Localize("Calculate_Total");
                            return Json(new { Message, getMarkList }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string Message = ResourceCache.Localize("Please_ask_all_the_faculties_to_fill_marks");
                            return Json(new { Message, getMarkList }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        var checkRankExist = markTotal.ExistRankList(yearId, classId, secId, examId).StudentRank;
                        if (checkRankExist != null)
                        {
                            string Message = ResourceCache.Localize("Rank_already_generated");
                            return Json(new { Message, getMarkList }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string Message = ResourceCache.Localize("TotalAlreadyCalculated");
                            return Json(new { Message, getMarkList }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CalculateTotal(int passYearId, string passClassWithSection, int passexamId)
        {
            int YearId = Convert.ToInt32(passYearId);
            int currentAcYearId = Convert.ToInt32(Session["CurrentAcYearId"]);
            if (YearId == currentAcYearId)
            {
                int yearId = passYearId;
                string clasSec = passClassWithSection;
                string[] words = clasSec.Split(' ');
                int classId = Convert.ToInt32(words[0]);
                int secId = Convert.ToInt32(words[1]);
                int examId = passexamId;

                StudentMarkServices mark = new StudentMarkServices();
                MarkTotalServices markTotal = new MarkTotalServices();

                var getAllStudentId = mark.getStudentId(yearId, classId, secId, examId);

                for (int z = 0; z < getAllStudentId.Count; z++)
                {
                    int? sId = getAllStudentId[z].stuRegId;

                    // to calculate total
                    var getStudentMarkList = mark.getMarkList(yearId, classId, secId, examId, sId);
                    ProgressCard[] mrk = getStudentMarkList.ToArray();
                    int? Total1 = 0;
                    for (int i = 0; i < getStudentMarkList.Count; i++)
                    {
                        Total1 = mrk[i].Mark + Total1;
                    }

                    var ProgressCardList = mark.getMark(yearId, classId, secId, examId, sId.Value);
                    ViewBag.progressCardCount = ProgressCardList.Count;
                    ProgressCard[] min = ProgressCardList.ToArray();

                    // to check pass or fail
                    string result = "";
                    int AcademicYearId = yearId;
                    int ClassId = classId;
                    int SectionId = secId;
                    int ExamId = examId;
                    int? StudentRegisterId = sId;
                    int? TotalMark = Total1;

                    for (int i = 0; i < getStudentMarkList.Count; i++)
                    {
                        if (mrk[i].Mark < min[i].MinMark)
                        {
                            result = "Fail";
                        }
                    }

                    if (result != "Fail")
                    {
                        result = "Pass";
                    }
                    markTotal.addMarkTotal(AcademicYearId, ClassId, SectionId, ExamId, StudentRegisterId, TotalMark, result);
                }
                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Calculate_all_student_total"));
                string Message = ResourceCache.Localize("Calculated_total_successfully_to_all_students");
                return Json(new { Message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string InfoMessage = ResourceCache.Localize("Records_can_save_for_current_academic_year_only");
                return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateRank(int passYearId, string passClassWithSection, int passexamId)
        {
            int YearId = Convert.ToInt32(passYearId);
            int currentAcYearId = Convert.ToInt32(Session["CurrentAcYearId"]);
            if (YearId == currentAcYearId)
            {
                int yearId = passYearId;
                string clasSec = passClassWithSection;
                string[] words = clasSec.Split(' ');
                int classId = Convert.ToInt32(words[0]);
                int secId = Convert.ToInt32(words[1]);
                int examId = passexamId;

                MarkTotalServices markTotal = new MarkTotalServices();
                var RankListOrder = markTotal.getRankList(yearId, classId, secId, examId);
                var s = RankListOrder.ToList();

                for (int j = 0; j < RankListOrder.ToList().Count; j++)
                {
                    int Id = s[j].MarkTotalId;
                    int rank = s[j].Rank;
                    markTotal.updateRank(Id, rank);
                }

                markTotal.UpdateFailList(yearId, classId, secId, examId);
                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Generate_rank_to_all_student"));
                string Message = ResourceCache.Localize("Generated_rank_successfully_to _all_students");
                return Json(new { Message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string InfoMessage = ResourceCache.Localize("Records_can_save_for_current_academic_year_only");
                return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AllStudentRank(int passYearId, string passClassWithSection, int passexamId)
        {
            int yearId = passYearId;
            string clasSec = passClassWithSection;
            string[] words = clasSec.Split(' ');
            int classId = Convert.ToInt32(words[0]);
            int secId = Convert.ToInt32(words[1]);
            int examId = passexamId;
            MarkTotalServices markTotal = new MarkTotalServices();
            var StudentRanklist = markTotal.getStudentRankList(yearId, classId, secId, examId);
            return Json(new { StudentRanklist }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RankComment(CI_RankComments m)
        {
            var TotalCount = m.TotalCount;
            int size = TotalCount + 1;
            string[] Id = new string[size];
            string[] rank_comment = new string[size];
            MarkTotalServices markTotal = new MarkTotalServices();

            int YearId = m.Att_allAcademicYears;
            string clasSec = m.Att_classWithSection;
            string[] words = clasSec.Split(' ');
            int classId = Convert.ToInt32(words[0]);
            int secId = Convert.ToInt32(words[1]);
            int examId = m.Exam;
            string checkCommentsExist = markTotal.checkCommentsExist(YearId, classId, secId, examId);
            if (checkCommentsExist == "" || checkCommentsExist == null)
            {
                for (var i = 0; i <= TotalCount; i++)
                {
                    Id[i] = QSCrypt.Decrypt(m.Id[i]);
                    int MarkTotalId = Convert.ToInt32(Id[i]);
                    rank_comment[i] = m.rank_comment[i];

                    markTotal.updateMarkTotal(MarkTotalId, rank_comment[i]);
                }
                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Add_rank_comments_to_student"));
                string Message = ResourceCache.Localize("CommentsSavedSuccessfully");
                return Json(new { Message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string ErrorMessage = "Progress_card_already_generated";
                return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GradeProgressCard()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GradeProgressCard(CreateProgressCard cp)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int yearId = cp.PC_allAcademicYears;
                    string clasSec = cp.CI_ClassWithSection;
                    string[] words = clasSec.Split(' ');
                    int classId = Convert.ToInt32(words[0]);
                    int secId = Convert.ToInt32(words[1]);
                    int examId = cp.allExams;

                    StudentGradeServices grade = new StudentGradeServices();
                    var GradeList = grade.getGradeList(yearId, classId, secId, examId);
                    ViewBag.MarkListCount = GradeList.Count;
                    AssignSubjectToSectionServices sub = new AssignSubjectToSectionServices();
                    var subjectList = sub.getSubjectList(yearId, classId, secId).ToList();

                    if (subjectList.Count == GradeList.Count)
                    {
                        string Message = ResourceCache.Localize("Grade_generated_to_all_subjects");
                        return Json(new { Message, GradeList, subjectList }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string Message = ResourceCache.Localize("Please_ask_all_the_faculties_to_fill_marks");
                        return Json(new { Message, GradeList, subjectList }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult AllStudentGrade(int passYearId, string passClassWithSection, int passexamId)
        {
            int yearId = passYearId;
            string clasSec = passClassWithSection;
            string[] words = clasSec.Split(' ');
            int classId = Convert.ToInt32(words[0]);
            int secId = Convert.ToInt32(words[1]);
            int examId = passexamId;

            AssignSubjectToSectionServices sub = new AssignSubjectToSectionServices();
            var subjectList = sub.GetSectionSub(yearId, classId, secId).ToList();
            var SubjectCount = subjectList.Count;

            AssignClassToStudentServices stu = new AssignClassToStudentServices();
            var StudentList = stu.getClassTeacherStudentListForGrade(yearId, classId, secId);
            var StudentCount = StudentList.Count;

            StudentGradeServices StuGrade = new StudentGradeServices();
            var GradeList = StuGrade.getStudentGradeList(yearId, classId, secId, examId);

            GradeCommentServices gradeCmt = new GradeCommentServices();
            var ExistCmt = gradeCmt.getExistComment(yearId, classId, secId, examId);
            var ExistCmtCount = ExistCmt.Count;

            return Json(new { subjectList, SubjectCount, StudentList, StudentCount, GradeList, ExistCmt, ExistCmtCount }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GradeComment(CI_RankComments m)
        {
            int YearId = Convert.ToInt32(m.Att_allAcademicYears);
            int currentAcYearId = Convert.ToInt32(Session["CurrentAcYearId"]);
            if (YearId == currentAcYearId)
            {
                var TotalCount = m.TotalCount;
                int size = TotalCount;
                string[] Id = new string[size];
                string[] grade_comment = new string[size];
                int yearId = m.Att_allAcademicYears;
                string clasSec = m.Att_classWithSection;
                string[] words = clasSec.Split(' ');
                int classId = Convert.ToInt32(words[0]);
                int secId = Convert.ToInt32(words[1]);
                int exam = m.Exam;

                GradeCommentServices gradeCmt = new GradeCommentServices();
                var checkDataExist = gradeCmt.GradeExist(yearId, classId, secId, exam);
                if (checkDataExist == null)
                {
                    for (var i = 0; i < TotalCount; i++)
                    {
                        Id[i] = QSCrypt.Decrypt(m.Id[i]);
                        int StuRegId = Convert.ToInt32(Id[i]);
                        grade_comment[i] = m.rank_comment[i];


                        gradeCmt.addGradeComment(yearId, classId, secId, exam, StuRegId, grade_comment[i], ClassInchargeId);
                    }
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Add_grade_comments_to_student"));
                    string Message = ResourceCache.Localize("CommentsSavedSuccessfully");
                    return Json(new { Message }, JsonRequestBehavior.AllowGet);
                }
                string ErrorMessage = ResourceCache.Localize("Progress_card_already_generated");
                return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string InfoMessage = ResourceCache.Localize("Records_can_save_for_current_academic_year_only");
                return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult StudentRemark(string id)
        {
            ViewBag.Id = id;
            TempData["id"] = id;
            int studentRegId = Convert.ToInt32(QSCrypt.Decrypt(id.ToString()));
            StudentServices stu = new StudentServices();
            var getStudentName = stu.getStudentRow(studentRegId);
            ViewBag.Name = getStudentName.FirstName + " " + getStudentName.LastName;
            return View();
        }

        private IList<StudentRemark> stuRemarkList = new List<StudentRemark>();

        public JsonResult RemarkListResults(string sidx, string sord, int page, int rows, string id, DateTime? CI_Remark_fdate, DateTime? CI_Remark_toDate)
        {
            var id1 = TempData["id"];
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            StudentDailyAttendanceServices stuAttendance = new StudentDailyAttendanceServices();
            if (id1 != null)
            {
                int studentRegId = Convert.ToInt32(QSCrypt.Decrypt(id1.ToString()));
                stuRemarkList = stuAttendance.getStudentRemark(studentRegId);
            }
            if (id != null && id != "" && CI_Remark_fdate != null && CI_Remark_toDate != null)
            {
                int studentRegId = Convert.ToInt32(QSCrypt.Decrypt(id));
                stuRemarkList = stuAttendance.getStudentRemark(studentRegId, CI_Remark_fdate, CI_Remark_toDate);
            }
            int totalRecords = stuRemarkList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    stuRemarkList = stuRemarkList.OrderByDescending(s => s.date).ToList();
                    stuRemarkList = stuRemarkList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    stuRemarkList = stuRemarkList.OrderBy(s => s.date).ToList();
                    stuRemarkList = stuRemarkList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = stuRemarkList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = stuRemarkList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StudentRemark_ExportToExcel()
        {
            try
            {
                List<StudentRemark> stuRemarkList = (List<StudentRemark>)Session["JQGridList"];
                var list = stuRemarkList.Select(o => new { Date = o.date, CommentFrom = o.CommentFrom, Remark = o.Remark }).ToList();
                string fileName = ResourceCache.Localize("StudentRemark");
                GenerateExcel.ExportExcel(list, fileName);
                return View("StudentList");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult StudentRemark_ExportToPdf()
        {
            try
            {
                List<StudentRemark> stuRemarkList = (List<StudentRemark>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("StudentRemarkList");
                GeneratePDF.ExportPDF_Portrait(stuRemarkList, new string[] { "date", "CommentFrom", "Remark" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("StudentRemark.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }


        public JsonResult getDate(int acid)
        {
            try
            {
                AcademicyearServices obj_academicYear = new AcademicyearServices();
                var ans = obj_academicYear.AcademicYears(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                EventServices holidays = new EventServices();
                var HolidaysList = holidays.getHolidaysList(acid);
                return Json(new { sd = sdate, ed = edate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        //Datepicker Shows Current Year without filter academic year option
        public JsonResult CurrentActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");

            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EventServices holidays = new EventServices();
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    int acid = CurrentAcYear[i].AcademicYearId;
                    var HolidaysList = holidays.getHolidaysList(acid);
                    return Json(new { StartingEnableDate, EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { StartingEnableDate, EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getStudentList(int passYearId, string passClassIdSecId, DateTime passDate)
        {
            string clasSec = passClassIdSecId;
            string[] words = clasSec.Split(' ');
            int classId = Convert.ToInt32(words[0]);
            int secId = Convert.ToInt32(words[1]);

            AssignClassToStudentServices stu = new AssignClassToStudentServices();
            var studentList = stu.getStudentsList(passYearId, classId, secId);
            return Json(studentList, JsonRequestBehavior.AllowGet);
        }

        // Graph
        public ActionResult Graph()
        {
            return View();
        }

        public JsonResult MaleFemale(int passYearId, string passClassWithSec)
        {
            try
            {
                if (passYearId != 0 && passClassWithSec != "" && passClassWithSec != null)
                {
                    AssignClassTeacherServices classTeacher = new AssignClassTeacherServices();
                    int yearId = passYearId;
                    string clasSec = passClassWithSec;
                    string[] words = clasSec.Split(' ');
                    int classId = Convert.ToInt32(words[0]);
                    int secId = Convert.ToInt32(words[1]);
                    var getClass = classTeacher.CheckClassTeacher(ClassInchargeId, yearId, classId, secId);
                    if (getClass != null)
                    {
                        AssignClassToStudentServices clasStu = new AssignClassToStudentServices();
                        var getStudents = clasStu.getClassTeacherStudents(yearId, classId, secId).ToList();

                        if (getStudents.Count != 0) //1 == 2
                        {
                            int maleCount = 0;
                            int FemaleCount = 0;
                            for (int i = 0; i < getStudents.Count; i++)
                            {
                                int? sId = getStudents[i].stuRegId;

                                StudentServices stu = new StudentServices();
                                var Male = stu.getMale(sId);
                                if (Male != null)
                                {
                                    maleCount++;
                                }
                                var Female = stu.getFemale(sId);
                                if (Female != null)
                                {
                                    FemaleCount++;
                                }
                            }
                            var ErrorMessage = "NoError";
                            return Json(new { maleCount, FemaleCount, ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var ErrorMessage = "NoDetailsFound";
                            return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        var ErrorMessage = "You_are_not_a_class_Teacher";
                        return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    var ErrorMessage = "Please_select_all_the_mandatory_fields";
                    return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DailyAttendance(int passYearId, string passClassWithSec)
        {
            try
            {
                if (passYearId != 0 && passClassWithSec != "" && passClassWithSec != null)
                {
                    AssignClassTeacherServices classTeacher = new AssignClassTeacherServices();
                    int yearId = passYearId;
                    string clasSec = passClassWithSec;
                    string[] words = clasSec.Split(' ');
                    int classId = Convert.ToInt32(words[0]);
                    int secId = Convert.ToInt32(words[1]);
                    var getClass = classTeacher.CheckClassTeacher(ClassInchargeId, yearId, classId, secId);
                    if (getClass != null)
                    {
                        var currentMonth = DateTimeByZone.getCurrentDate().Month;
                        DateTime now = DateTimeByZone.getCurrentDateTime();
                        var month = now.ToString("MMM");

                        StudentDailyAttendanceServices stuAttendance = new StudentDailyAttendanceServices();
                        var getDate = stuAttendance.getAttendanceDate(yearId, classId, secId, currentMonth);
                        var dateCount = getDate.Count;
                        if (dateCount != 0)
                        {
                            DateTime? date1;

                            ArrayList Day = new ArrayList();
                            foreach (var d in getDate)
                            {
                                var day = d.date.Value;
                                var day1 = day.ToString("dd");
                                Day.Add(day1);
                            }

                            ArrayList PresentCount = new ArrayList();
                            ArrayList PercentageList = new ArrayList();
                            for (int i = 0; i < getDate.Count; i++)
                            {
                                date1 = getDate[i].date;
                                var getPresentStudents = stuAttendance.getPresentStudents(yearId, classId, secId, date1);
                                PresentCount.Add(getPresentStudents.Count);

                                // Percentage calculation
                                AssignClassToStudentServices clasStu = new AssignClassToStudentServices();
                                var getTotalStudent = clasStu.getNumberOfStudents(yearId, classId, secId);
                                int TolatlStudentCount = getTotalStudent.Count;
                                int PresentStudentCount = getPresentStudents.Count;
                                int Percentage = (int)Math.Round((double)(100 * PresentStudentCount) / TolatlStudentCount);
                                PercentageList.Add(Percentage);
                            }
                            var ErrorMessage = "NoError";
                            return Json(new { dateCount, Day, PresentCount, month, PercentageList, ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var ErrorMessage = "NoDetailsFound";
                            return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        var ErrorMessage = "You_are_not_a_class_Teacher";
                        return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    var ErrorMessage = "Please_select_all_the_mandatory_fields";
                    return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult MonthlyAttendance(int passYearId, string passClassWithSec)
        {
            try
            {
                if (passYearId != 0 && passClassWithSec != "" && passClassWithSec != null)
                {
                    string Year;
                    AssignClassTeacherServices classTeacher = new AssignClassTeacherServices();
                    int yearId = passYearId;
                    string clasSec = passClassWithSec;
                    string[] words = clasSec.Split(' ');
                    int classId = Convert.ToInt32(words[0]);
                    int secId = Convert.ToInt32(words[1]);
                    var getClass = classTeacher.CheckClassTeacher(ClassInchargeId, yearId, classId, secId);
                    if (getClass != null)
                    {
                        StudentDailyAttendanceServices stuAttendance = new StudentDailyAttendanceServices();
                        var getAttendanceMonth = stuAttendance.getAttendanceMonth(yearId, classId, secId);
                        if (getAttendanceMonth.Count != 0)
                        {
                            ArrayList month1 = new ArrayList();
                            ArrayList presentDaysCount = new ArrayList();
                            ArrayList monthName = new ArrayList();
                            ArrayList PercentageList = new ArrayList();

                            for (int i = 0; i < getAttendanceMonth.Count; i++)
                            {
                                var month = getAttendanceMonth[i].month;
                                var month_Name = getAttendanceMonth[i].monthname.Value.ToString("MMM");

                                var getPresentDays = stuAttendance.getPresentDays(yearId, classId, secId, month);
                                presentDaysCount.Add(getPresentDays.Count);
                                month1.Add(month);
                                monthName.Add(month_Name);

                                // Percentage calculation
                                AssignClassToStudentServices clasStu = new AssignClassToStudentServices();
                                var getTotalStudent = clasStu.getNumberOfStudents(yearId, classId, secId);
                                int TotalStudentCount = getTotalStudent.Count;
                                var getWorkingDays = stuAttendance.getWorkingDays(yearId, classId, secId, month);
                                int TotalWorkingDays = getWorkingDays.Count;
                                int PresentStudentCount = getPresentDays.Count;
                                int Percentage = (int)Math.Round((double)(100 * PresentStudentCount) / (TotalStudentCount * TotalWorkingDays));
                                PercentageList.Add(Percentage);
                            }

                            AcademicyearServices AcYear = new AcademicyearServices();
                            Year = AcYear.date(yearId).AcademicYear1;
                            var monthCount = getAttendanceMonth.Count;
                            var ErrorMessage = "NoError";
                            return Json(new { month1, presentDaysCount, Year, monthCount, monthName, PercentageList, ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {                           
                            var ErrorMessage = "NoDetailsFound";
                            return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        var ErrorMessage = "You_are_not_a_class_Teacher";
                        return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    var ErrorMessage = "Please_select_all_the_mandatory_fields";
                    return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult PastActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    EventServices holidays = new EventServices();
                    int acid = CurrentAcYear[i].AcademicYearId;
                    var HolidaysList = holidays.getHolidaysList(acid);
                    return Json(new { StartingEnableDate, EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { StartingEnableDate, EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GradeResult()
        {
            return View();
        }


    }
}
