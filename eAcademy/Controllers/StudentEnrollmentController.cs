﻿using eAcademy.Areas.HostelManagement.Services;
using eAcademy.Areas.Transport.Services;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Rotativa;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class StudentEnrollmentController : Controller
    {
        SectionServices section = new SectionServices();
        StudentServices stu = new StudentServices();
        ParentDetailServices parent = new ParentDetailServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        ClassServices clas = new ClassServices();
        BoardofSchoolServices board = new BoardofSchoolServices();
        public int AdminId;

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AdminId = Convert.ToInt32(Session["empRegId"]);
            AcademicyearServices AcYear = new AcademicyearServices();
            //ClassServices clas = new ClassServices();
            CountriesServices country = new CountriesServices();
            BloodGroupServices bg = new BloodGroupServices();
            CommunityServices com = new CommunityServices();
            BoardofSchoolServices bos = new BoardofSchoolServices();
            EmployeeDesinationServices EmployeeDesignation = new EmployeeDesinationServices();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.allClasses = clas.ShowClasses();
            ViewBag.allCountries = country.ShowCountries();
            ViewBag.academicyear = AcYear.getvacancyAcademicYearList();
            ViewBag.country = country.GetCountry_Admission();
            ViewBag.bloodgroups = bg.GetBloodGroup();
            ViewBag.communities = com.GetCommunity();
            ViewBag.Classes = clas.GetClassvacauncy();
            ViewBag.Distances = GetDistance();
            ViewBag.PreviousBoard = bos.Showpreviousmedium();
            ViewBag.EmployeeDesignation = EmployeeDesignation.EmployeeDesignation();
        }
        public IEnumerable GetDistance()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select Distance", Value = "" });
            li.Add(new SelectListItem { Text = "1km", Value = "1" });
            li.Add(new SelectListItem { Text = "2km", Value = "2" });
            li.Add(new SelectListItem { Text = "3km", Value = "3" });
            li.Add(new SelectListItem { Text = "4km", Value = "4" });
            li.Add(new SelectListItem { Text = "5km", Value = "5" });
            li.Add(new SelectListItem { Text = "6km", Value = "6" });
            li.Add(new SelectListItem { Text = "7km", Value = "7" });
            li.Add(new SelectListItem { Text = "8km", Value = "8" });
            li.Add(new SelectListItem { Text = "9km", Value = "9" });
            li.Add(new SelectListItem { Text = "10km", Value = "10" });
            li.Add(new SelectListItem { Text = "11km", Value = "11" });
            li.Add(new SelectListItem { Text = "12km", Value = "12" });
            li.Add(new SelectListItem { Text = "13km", Value = "13" });
            li.Add(new SelectListItem { Text = "14km", Value = "14" });
            li.Add(new SelectListItem { Text = "15km", Value = "15" });
            li.Add(new SelectListItem { Text = "16km", Value = "16" });
            li.Add(new SelectListItem { Text = "17km", Value = "17" });
            li.Add(new SelectListItem { Text = "18km", Value = "18" });
            li.Add(new SelectListItem { Text = "19km", Value = "19" });
            li.Add(new SelectListItem { Text = "20km", Value = "20" });
            return li;
        }
        public JsonResult GetSelectedClassSection(int passClassId)
        {
            var List = section.getSection(passClassId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSectionStudents(int passSectionId)
        {
            AssignClassToStudentServices assignStu = new AssignClassToStudentServices();
            var StudentList = assignStu.getStudents(passSectionId);
            return Json(StudentList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getSelectedSectionRollNumber(int passYearId, int passClassId, int passSecId)
        {
            StudentRollNumberServices RollNum = new StudentRollNumberServices();
            var RollNumberList = RollNum.GetRollNumber1(passYearId, passClassId, passSecId);
            return Json(RollNumberList, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult getAgelimitdate(int AdminssionClassId, int AcademicyearId)
        //{
        //    try
        //    {
        //        AsignClassVacancyServices acs = new AsignClassVacancyServices();
        //        var Getdate = acs.getdate(AdminssionClassId, AcademicyearId);
        //        if (Getdate == null)
        //        {
        //            Getdate = DateTime.Today.ToString("M/d/yyyy"); ;
        //        }
        //        return Json(new { Getdate = Getdate }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        String ErrorMessage = e.Message;
        //        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        public ActionResult SingleFormEnrollment()
        {
            try
            {
                Admission1Services As1 = new Admission1Services();
                AcademicyearServices AcYear = new AcademicyearServices();
                SchoolSettingServices ss = new SchoolSettingServices();
                SchoolSettingsServices ss1 = new SchoolSettingsServices();
                TransportDestinationServices TrDestnServ = new TransportDestinationServices();
                var formtype = ss.getSchoolSettings();
                if (formtype != null)
                {
                    var commingAcademicyear = AcYear.GetAcademicYear(); ;
                    if (commingAcademicyear != null)
                    {
                        var Settings = ss1.getSchoolSettings();
                        ViewBag.Email = Settings.FirstOrDefault().EmailRequired;
                        ViewBag.Sms = Settings.FirstOrDefault().SmsRequired;
                        ViewBag.App = Settings.FirstOrDefault().AppRequired;
                        ViewBag.Transport = Settings.FirstOrDefault().TransportRequired;
                        ViewBag.Hostel = Settings.FirstOrDefault().HostelRequired;
                        ViewBag.allDestination = TrDestnServ.DestinationList();
                        return View();
                    }
                    else
                    {
                        ViewBag.ErrorMessage = eAcademy.Models.ResourceCache.Localize("Settings_not_completed");
                        return View();
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = eAcademy.Models.ResourceCache.Localize("Settings_not_completed");
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View();
            }
        }
        [HttpPost]
        public ActionResult ApplicationRegister(VM_Pre_AdmissionValidation a)
        {
            try
            {
                PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                PreAdmissionPrimaryUserRegisterServices Pprimaryuser = new PreAdmissionPrimaryUserRegisterServices();
                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                SchoolSettingServices schoolsetting = new SchoolSettingServices();
                SchoolSettingsServices ss1 = new SchoolSettingsServices();
                string OfflineId = a.OfflineApplicationId;
                var UserExit = Pstudent.GetOfflineStudentdetails(OfflineId);
                var stuid = Convert.ToInt16(UserExit.StudentAdmissionId);
                var Student = Pstudent.findParticularStudentDetailsOffline(OfflineId, UserExit.StudentAdmissionId);
                string PrimaryEmail = "";
                string ErrorMessage = "";
                var Settings = ss1.getSchoolSettings();
                ViewBag.Email = Settings.FirstOrDefault().EmailRequired;
                ViewBag.Sms = Settings.FirstOrDefault().SmsRequired;
                ViewBag.App = Settings.FirstOrDefault().AppRequired;
                if (Student.Count != 0)
                {
                    if (ModelState.IsValid)
                    {
                        int count = 0;
                        string Id = "";
                        string Sid = "";
                        int applyclass = Convert.ToInt16(a.AdmissionClass);
                        if (a.Edit != "Edit")
                        {
                            if (a.StudentPhoto != null && a.CommunityCertificate != null && a.BirthCertificate != null)
                            {
                                count++;
                            }
                            else
                            {
                                ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_load_Student_Photo_,Community_Certificate_and_Birth_Certificate");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            if (a.StudentPhoto1 != null && a.CommunityCertificate1 != null && a.BirthCertificate1 != null)
                            {
                                count++;
                            }
                            else
                            {
                                ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_load_Student_Photo_,Community_Certificate_and_Birth_Certificate");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        if (applyclass == 1)
                        {
                            count++;
                        }
                        else if (applyclass != 1)
                        {
                            if (a.TransferCertificate != null)
                            {
                                if (a.PreSchool != null && a.PreBoardOfSchool != null && a.PreClass != null) //&& a.PreMarks != null && a.PreFromDate != null && a.PreToDate != null
                                {
                                    count++;
                                    if (a.TransferCertificate != null)
                                    {
                                    }
                                    else
                                    {
                                        ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_load_Transfer_Certificate");
                                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                                else
                                {
                                    ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_fill_all_previous_school_information");
                                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else if (a.TransferCertificate1 != null)
                            {
                                if (a.PreSchool != null && a.PreBoardOfSchool != null && a.PreClass != null)// && a.PreMarks != null && a.PreFromDate != null && a.PreToDate != null
                                {
                                    count++;
                                    if (a.TransferCertificate1 != null)
                                    { 
                                    }
                                    else
                                    {
                                        ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_load_Transfer_Certificate");
                                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                                else
                                {
                                    ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_fill_all_previous_school_information");
                                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_load_Transfer_Certificate");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        if (a.EnrollmentType == "Email")
                        {
                            if (a.PrimaryUser == "Father")
                            {
                                if (a.FatherFirstName != null && a.FatherLastName != null && a.FatherDOB != null && a.FatherEmail != null) // a.FatherMobileNo!=null && && a.FatherMobileNo != null
                                {
                                    PrimaryEmail = a.FatherEmail;
                                    count++;
                                }
                                else
                                {
                                    ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_fill_Father_All_Information");
                                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else if (a.PrimaryUser == "Mother")
                            {
                                if (a.MotherFirstname != null && a.MotherLastName != null && a.MotherDOB != null && a.MotherEmail != null) // a.MotherMobileNo!=null &&&& a.MotherMobileNo != null
                                {
                                    PrimaryEmail = a.MotherEmail;
                                    count++;
                                }
                                else
                                {
                                    ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_fill_Mother_All_Information");
                                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else if (a.PrimaryUser == "Guardian")
                            {
                                if (a.GuardianFirstname != null && a.GuardianLastname != null && a.GuardianEmail != null && a.GuardianDOB != null)//&& a.GuardianMobileNo != null
                                {
                                    PrimaryEmail = a.GuardianEmail;
                                    count++;
                                }
                                else
                                {
                                    ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_fill_Guardian_All__Information");
                                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                            }
                        }
                        else
                        {
                            if (a.PrimaryUser == "Father")
                            {
                                if (a.FatherFirstName != null && a.FatherLastName != null && a.FatherDOB != null && a.FatherMobileNo != null) // a.FatherMobileNo!=null &&&& a.FatherEmail != null
                                {
                                    PrimaryEmail = a.FatherEmail;
                                    count++;
                                }
                                else
                                {
                                    ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_fill_Father_All_Information");
                                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else if (a.PrimaryUser == "Mother")
                            {
                                if (a.MotherFirstname != null && a.MotherLastName != null && a.MotherDOB != null && a.MotherMobileNo != null) // a.MotherMobileNo!=null &&&& a.MotherEmail != null
                                {
                                    PrimaryEmail = a.MotherEmail;
                                    count++;
                                }
                                else
                                {
                                    ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_fill_Mother_All_Information");
                                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else if (a.PrimaryUser == "Guardian")
                            {
                                if (a.GuardianFirstname != null && a.GuardianLastname != null && a.GuardianEmail != null && a.GuardianMobileNo != null)//&& a.GuardianDOB != null
                                {
                                    PrimaryEmail = a.GuardianEmail;
                                    count++;
                                }
                                else
                                {
                                    ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_fill_Guardian_All__Information");
                                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                            }
                        }

                        if (count == 3)
                        {
                            var ss = Pstudent.GetStudentApplicationdetails(Convert.ToInt16(stuid));
                            var Primary = new PreAdmissionPrimaryUserRegister();
                            if (a.EnrollmentType == "Email")
                            {
                                Primary = Pprimaryuser.GetPrimaryuserdetails(PrimaryEmail);
                            }
                            else
                            {
                                long contact = Convert.ToInt64(a.PrimaryContact);
                                Primary = Pprimaryuser.GetMobilePrimaryuserdetails(contact);
                            }
                            var school = schoolsetting.getSchoolSettings();
                            var SchoolName = school.SchoolName;
                            var SchoolLogo = school.SchoolLogo;
                            var Primaryuser = Primary.PrimaryUserName.ToString();

                            var Studentname = a.StuFirstName + " " + a.StuLastname;

                            var AppNo = stuid.ToString();
                            var year = ss.Academicyear.ToString();
                            var classs = ss.classs.ToString();
                            // For sending Email to Father                            
                            if ((ViewBag.Email == true && ViewBag.Sms == true && ViewBag.App == true) || (ViewBag.Email == true && ViewBag.Sms == true) || (ViewBag.Sms == true && ViewBag.App == true))
                            {
                                if (Primary.EmailRequired == true)
                                {
                                    var Femail = PrimaryEmail.ToString();
                                    var Username = Primary.PrimaryUserEmail.ToString();
                                    PreAdmissionEmail(Primaryuser, AppNo, Username, year, classs, Studentname, SchoolName, SchoolLogo);
                                }
                                if (Primary.SmsRequired == true)
                                {
                                    var contact = Primary.PrimaryUserContactNo.Value.ToString().Trim();
                                    PreAdmissionSMS(contact, Primaryuser, AppNo, Studentname, year, classs);
                                }
                            }
                            else if (ViewBag.Email == true || ViewBag.App == true)
                            {
                                // For sending Email to Father
                                if (PrimaryEmail != "")
                                {
                                    var Femail = PrimaryEmail.ToString();
                                    var Username = Primary.PrimaryUserEmail.ToString();
                                    PreAdmissionEmail(Primaryuser, AppNo, Username, year, classs, Studentname, SchoolName, SchoolLogo);

                                }
                            }
                            else if (ViewBag.Sms == true)
                            {
                                var contact = Primary.PrimaryUserContactNo.Value.ToString().Trim();
                                PreAdmissionSMS(contact, Primaryuser, AppNo, Studentname, year, classs);
                            }
                            Id = QSCrypt.Encrypt(Primary.PrimaryUserAdmissionId.ToString());
                            Sid = QSCrypt.Encrypt(stuid.ToString());
                        }
                        string statusflag = "Enrolled";
                        bool result1 = PstudentRegister.UpdateStudentRegisterStatusOffline(stuid, statusflag, AdminId);

                        string Message = eAcademy.Models.ResourceCache.Localize("Successfully_all_details_are_registered,click_below_link_to_show_entered_all_details_and_generate_pdf");
                        return Json(new { Message = Message, Id, Sid }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string totalError = "";
                        string[] t = new string[ModelState.Values.Count];
                        int i = 0;
                        foreach (var obj in ModelState.Values)
                        {
                            foreach (var error in obj.Errors)
                            {
                                if (!string.IsNullOrEmpty(error.ErrorMessage))
                                {
                                    totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                    t[i] = error.ErrorMessage;
                                    i++;
                                }
                            }
                        }
                        return Json(new { Success = 0, ex = t });
                    }
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        int count = 0;
                        int applyclass = Convert.ToInt16(a.AdmissionClass);
                        if (applyclass == 1)
                        {
                            count++;
                        }
                        else if (applyclass != 1)
                        {
                            if (a.PreSchool != "" || a.PreBoardOfSchool != null || a.PreClass != null || a.PreMarks != "" || a.PreFromDate != null || a.PreToDate != null)
                            {
                                count++;
                            }
                            else
                            {
                                ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_fill_all_previous_school_information_And_load_Transfer_Certificate");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        string Message = "";
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string totalError = "";
                        string[] t = new string[ModelState.Values.Count];
                        int i = 0;
                        foreach (var obj in ModelState.Values)
                        {
                            foreach (var error in obj.Errors)
                            {
                                if (!string.IsNullOrEmpty(error.ErrorMessage))
                                {
                                    totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                    t[i] = error.ErrorMessage;
                                    i++;
                                }
                            }
                        }
                        return Json(new { Success = 0, ex = t });
                    }
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public void PreAdmissionEmail(string PrimaryUser, string ApplicationNumber, string UserName, string AcademicYear, string Class, string Studentname, string SchoolName, string SchoolLogo)
        {
            StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/SuperUserEmailPage.html"));
            string readFile2 = reader2.ReadToEnd();
            string myString2 = "";
            myString2 = readFile2;
            myString2 = myString2.Replace("$$PrimaryUser$$", PrimaryUser.ToString());
            myString2 = myString2.Replace("$$appno$$", ApplicationNumber.ToString());
            myString2 = myString2.Replace("$$username$$", UserName.ToString());
            myString2 = myString2.Replace("$$AcdemicYear$$", AcademicYear.ToString());
            myString2 = myString2.Replace("$$ApplyClass$$", Class.ToString());
            myString2 = myString2.Replace("$$StudentName$$", Studentname.ToString());
            myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
            myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
            string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
            string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];

            string PlaceName = "StudentEnrollment-PreAdmissionEmail-Application enrolled successfully";
            string User_Name = "Employee";
            string UserId = Convert.ToString(Session["username"]);

            Mail.SendMail(EmailDisplay, supportEmail, UserName, "Account Credentials", myString2.ToString(), true, PlaceName, User_Name, UserId);
        }
        public void PreAdmissionSMS(string number, string PrimaryUser, string ApplicationNumber, string Studentname, string AcademicYear, string Class)
        {
            string ToNumber = number;
            string sub = "Apllication Status";
            string Description = "Hi " + PrimaryUser.ToString() + ", Your application Number=" + ApplicationNumber.ToString() + ", Student name=" + Studentname.ToString() + ", Academic Year=" + AcademicYear.ToString() + ", Apply Class=" + Class.ToString() + ", Successfully registered";
            string fromDisplayName = ConfigurationManager.AppSettings["EmailFromDisplayName"];
            SMS.Main(fromDisplayName, ToNumber, sub, Description);
        }
        public JsonResult Newuser(M_NewUser s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                    PreAdmissionPrimaryUserRegisterServices Pprimary = new PreAdmissionPrimaryUserRegisterServices();
                    PreAdmissionTransactionServices Ptrans = new PreAdmissionTransactionServices();
                    EnrollmentUserConfirmationServices euserconfirm = new EnrollmentUserConfirmationServices();
                    PrimaryUserRegisterServices purs = new PrimaryUserRegisterServices();
                    var id = s.OfflineApplicationId;
                    var uname = s.PrimaryEmail;
                    int Flag = 0;
                    var UserExit = new PreAdmissionStudentRegister();
                    var Enrolltype = s.enrolltype;
                    if (s.enrolltype == "Email")
                    {
                        UserExit = Ptrans.CheckEmailuserExit(id, uname);
                    }
                    else
                    {
                        UserExit = Ptrans.CheckMobileluserExit(id, uname);
                    }

                    var result = Pstudent.GetPrevivousApplicationOffline(uname);
                    var emailAvailable = purs.FindPrimaryUser(uname);
                    var oldstatus = "no";
                    if (result.Count > 0)
                    {
                        if (result.FirstOrDefault().PreApplicationStatus != "InComplete" && result.FirstOrDefault().PreApplicationStatus != "")
                        {
                            oldstatus = "yes";
                        }
                        else
                        {
                            oldstatus = "no";
                        }
                    }
                    else
                    {
                        oldstatus = "no";
                        if (emailAvailable > 0)
                        {
                            oldstatus = "yes";
                        }
                    }
                    if (UserExit != null)
                    {
                        if (UserExit.Statusflag == "InComplete" || UserExit.Statusflag == "AddMore")
                        {
                            Flag = 2;
                            var Student1 = Pstudent.findParticularStudentDetailsOffline(id, UserExit.StudentAdmissionId);
                            return Json(new { Flag, Student1, oldstatus, Enrolltype }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string Message = eAcademy.Models.ResourceCache.Localize("Application_already_enrolled_and_it's_status_is = ") + UserExit.Statusflag;
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        var CheckEmailExit = Pprimary.GetExitUsers(uname);
                        var CheckMasterEmail = Pprimary.GetMasterExitUsers(uname);
                        var checkApplication = Pstudent.GetOfflineStudentdetails(id);
                        if (CheckEmailExit.Count != 0 && checkApplication == null)
                        {
                            var ExitStudentStatus = Pprimary.CheckEmail(uname);
                            if (ExitStudentStatus.Statusflag != "InComplete")
                            {
                                Flag = 1;
                                var PreApplication = Pstudent.GetPrevivousApplicationOffline(uname);
                                string PrimaryUser = CheckEmailExit.FirstOrDefault().PrimaryUser;
                                string PrimaryUsername = CheckEmailExit.FirstOrDefault().PrimaryUserName;
                                string PrimaryUseremail = CheckEmailExit.FirstOrDefault().PrimaryUserEmail;
                                var contact = CheckEmailExit.FirstOrDefault().PrimaryUserContactNo;
                                var olddetails = euserconfirm.CheckApplication(id);
                                return Json(new { PrimaryUser, PrimaryUsername, PrimaryUseremail, contact, PreApplication, olddetails }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_previous_application_status_is_Incomplete,so_fill_previous_application_no ") + " " + ExitStudentStatus.OfflineApplicationID + " " + eAcademy.Models.ResourceCache.Localize("and_add_new_student");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else if (CheckEmailExit.Count == 0 && checkApplication != null)
                        {
                            string Message = eAcademy.Models.ResourceCache.Localize("Application_number_already_enrolled_and_its_status_is ") + checkApplication.Statusflag + eAcademy.Models.ResourceCache.Localize("but_email_address_is_wrong");
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else if (CheckEmailExit.Count != 0 && checkApplication != null)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Application_number_and_Email_address_is_wrong");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (CheckMasterEmail.Count != 0)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Give_parentid_or_studentId_to_enroll_new_student");
                            return Json(new { ErrorMessage1 = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            Flag = 0;
                            return Json(new { Flag, Enrolltype }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ExitUser(M_Exituser s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PrimaryUserRegisterServices purs = new PrimaryUserRegisterServices();
                    PreAdmissionTransactionServices Ptrans = new PreAdmissionTransactionServices();
                    PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                    PreAdmissionPrimaryUserRegisterServices Pprimary = new PreAdmissionPrimaryUserRegisterServices();
                    EnrollmentUserConfirmationServices euserconfirm = new EnrollmentUserConfirmationServices();
                    string offlineid = s.OfflineApplicationId;
                    string stuentid = s.StudentId;
                    string parentid = s.ParentId;
                    string MatchEmailPattern =
                @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
         + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
         + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
         + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
                    string uname = "";
                    var Enrolltype = "";
                    if (s.ParentId != null)
                    {
                        if (Regex.IsMatch(s.ParentId, MatchEmailPattern))
                        {
                            uname = s.ParentId;
                        }
                        else
                        {
                            uname = purs.GetUsername(s.ParentId);
                            if (uname == "")
                            {
                                uname = purs.GetStudentPrimarUserEmail(s.ParentId);
                            }
                        }
                    }
                    else
                    {
                        string Errormessage = eAcademy.Models.ResourceCache.Localize("Enter_studentid_or_parentId");
                        return Json(new { ErrorMessage = Errormessage }, JsonRequestBehavior.AllowGet);
                    }
                    if (uname != "")
                    {
                        var checkemail = Pprimary.GetExitUsers(uname);
                        var result = Pstudent.GetPrevivousApplicationOffline(uname);
                        var emailAvailable = purs.FindPrimaryUser(uname);
                        var oldstatus = "no";
                        if (result.Count > 0)
                        {
                            if (result.FirstOrDefault().PreApplicationStatus != "InComplete" && result.FirstOrDefault().PreApplicationStatus != "")
                            {
                                oldstatus = "yes";
                            }
                            else
                            {
                                oldstatus = "no";
                            }
                        }
                        else
                        {
                            oldstatus = "no";
                            if (emailAvailable > 0)
                            {
                                oldstatus = "yes";
                            }
                        }

                        if (checkemail.FirstOrDefault() != null)
                        {
                            var UserExit = new PreAdmissionStudentRegister();
                            if (Regex.IsMatch(uname, MatchEmailPattern))
                            {
                                Enrolltype = "Email";
                                UserExit = Ptrans.CheckEmailuserExit(offlineid, uname);
                            }
                            else
                            {
                                Enrolltype = "Mobile";
                                UserExit = Ptrans.CheckMobileluserExit(offlineid, uname);
                            }
                            var Flag = 0;
                            if (UserExit != null)
                            {
                                if (UserExit.Statusflag == "InComplete" || UserExit.Statusflag == "AddMore")
                                {
                                    Flag = 2;
                                    var Student1 = Pstudent.findParticularStudentDetailsOffline(offlineid, UserExit.StudentAdmissionId);
                                    return Json(new { Flag, Student1, oldstatus, Enrolltype }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    string Message = eAcademy.Models.ResourceCache.Localize("Application_already_enrolled_and_it's_now_status_is= ") + UserExit.Statusflag;
                                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                var checkApplication = Pstudent.GetOfflineStudentdetails(offlineid);
                                if (checkApplication != null)
                                {
                                    string Message = eAcademy.Models.ResourceCache.Localize("Application_number_already_enrolled_and_its_status_is ") + checkApplication.Statusflag + eAcademy.Models.ResourceCache.Localize(",but_email_address_is_wrong");
                                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    var CheckEmailExit = Pprimary.GetExitUsers(uname);
                                    var ExitStudentStatus = Pprimary.CheckEmail(uname);
                                    if (ExitStudentStatus.Statusflag != "InComplete")
                                    {
                                        Flag = 1;
                                        var PreApplication = Pstudent.GetPrevivousApplicationOffline(uname);
                                        string PrimaryUser = CheckEmailExit.FirstOrDefault().PrimaryUser;
                                        string PrimaryUsername = CheckEmailExit.FirstOrDefault().PrimaryUserName;
                                        string PrimaryUseremail = CheckEmailExit.FirstOrDefault().PrimaryUserEmail;
                                        var contact = CheckEmailExit.FirstOrDefault().PrimaryUserContactNo;
                                        var olddetails = euserconfirm.CheckApplication(offlineid);
                                        return Json(new { PrimaryUser, PrimaryUsername, PrimaryUseremail, contact, PreApplication, olddetails, Enrolltype }, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                    {
                                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_previous_application_status_is_Incomplete,so_fill_previous_application_no ") + " " + ExitStudentStatus.OfflineApplicationID + " " + eAcademy.Models.ResourceCache.Localize("and_add_new_student");
                                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }
                        }
                        else if (emailAvailable != 0)
                        {
                            if (Regex.IsMatch(uname, MatchEmailPattern))
                            {
                                Enrolltype = "Email";
                            }
                            else
                            {
                                Enrolltype = "Mobile";
                            }
                            string id = Ptrans.AddExistDetailsToNewtableOffline(uname, s.OfflineApplicationId);
                            var list = Pstudent.GetPreviousApplicationDetails(uname);
                            return Json(new { list, Enrolltype }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string Message = eAcademy.Models.ResourceCache.Localize("StudentId_or_ParentId_not_found");
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("StudentId_or_ParentId_not_found");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    string Message1 = "";
                    return Json(new { Message = Message1 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SendEmail1(M_SendEmail s)
        {
            try
            {
                PreAdmissionPrimaryUserRegisterServices Pprimary = new PreAdmissionPrimaryUserRegisterServices();
                SchoolSettingServices schoolsetting = new SchoolSettingServices();

                var Primary = Pprimary.GetExitUsers(s.UserName);
                if (Primary != null)
                {
                    var school = schoolsetting.getSchoolSettings();
                    var SchoolName = school.SchoolName;
                    var SchoolLogo = school.SchoolLogo;
                    var Fname = Primary.FirstOrDefault().PrimaryUserName;
                    var Fusername = Primary.FirstOrDefault().PrimaryUserEmail;
                    var Femail = s.ExitUserEmail;
                    var AppNo = s.OfflineApplicationId;
                    var des = s.Descriptions;
                    StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/RequestEmail.html"));
                    string readFile2 = reader2.ReadToEnd();
                    string myString2 = "";
                    myString2 = readFile2;
                    myString2 = myString2.Replace("$$Surname$$", Fname.ToString());
                    myString2 = myString2.Replace("$$appno$$", AppNo.ToString());
                    myString2 = myString2.Replace("$$description$$", des.ToString());
                    myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
                    myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
                    string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                    string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                    string PlaceName = "StudentEntrollment-SendEmail1-Asking confirmation to add new student";
                    string UserName = "Employee";
                    string UserId = Convert.ToString(Session["username"]);


                    Mail.SendMail(EmailDisplay, supportEmail, Femail, "Account Credentials", myString2.ToString(), true, PlaceName, UserName, UserId);
                }

                string Message = eAcademy.Models.ResourceCache.Localize("Sucessfully_Email_Send");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UserConfirm(VM_UserConfirmation s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionPrimaryUserRegisterServices Pprimary = new PreAdmissionPrimaryUserRegisterServices();
                    PrimaryUserRegisterServices primary = new PrimaryUserRegisterServices();
                    PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                    PreAdmissionTransactionServices pars = new PreAdmissionTransactionServices();
                    EnrollmentUserConfirmationServices usercon = new EnrollmentUserConfirmationServices();
                    string status = s.StatusFlag;
                    if (status == "RequestConfirm")
                    {
                        string MatchEmailPattern =
                      @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
               + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
               + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
               + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
                        string Enrolltype = "";
                        if (Regex.IsMatch(s.UserName, MatchEmailPattern))
                        {
                            Enrolltype = "Email";
                        }
                        else
                        {
                            Enrolltype = "Mobile";
                        }
                        var checkemail = Pprimary.GetExitUsers(s.UserName);
                        var Exitemail = primary.FindPrimaryUser(s.UserName);
                        if (checkemail.FirstOrDefault() != null)
                        {
                            var list = Pstudent.GetPreviousApplicationDetails(s.UserName);
                            var checkofflineid = usercon.CheckApplication(s.OfflineApplicationId);
                            if (checkofflineid != null)
                            {
                                usercon.UpdateUseConfirmation(s.OfflineApplicationId, s.ExitUserEmail, s.RequestSend, s.ExitUserMobileNo, s.StatusFlag, s.Descriptions);
                            }
                            else
                            {
                                usercon.AddUseConfirmation(s.OfflineApplicationId, s.ExitUserEmail, s.RequestSend, s.ExitUserMobileNo, s.StatusFlag, s.Descriptions);
                            }
                            return Json(new { list, Enrolltype }, JsonRequestBehavior.AllowGet);
                        }
                        else if (Exitemail != 0)
                        {
                            int? OnlineRegisterId = 0;
                            if (Enrolltype == "Email")
                            {
                                OnlineRegisterId = pars.AddEmailExistDetailsToNewtable(s.UserName);
                            }
                            else
                            {
                                long? contact = Convert.ToInt64(s.UserName);
                                OnlineRegisterId = pars.AddMobileExistDetailsToNewtable(contact);
                            }
                            var list = Pstudent.GetPreviousApplicationDetails(s.UserName);
                            var checkofflineid = usercon.CheckApplication(s.OfflineApplicationId);
                            if (checkofflineid != null)
                            {
                                usercon.UpdateUseConfirmation(s.OfflineApplicationId, s.ExitUserEmail, s.RequestSend, s.ExitUserMobileNo, s.StatusFlag, s.Descriptions);
                            }
                            else
                            {
                                usercon.AddUseConfirmation(s.OfflineApplicationId, s.ExitUserEmail, s.RequestSend, s.ExitUserMobileNo, s.StatusFlag, s.Descriptions);
                            }
                            return Json(new { list, Enrolltype }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else if (status == "RequestWait")
                    {
                        //usercon.AddUseConfirmation(s.OfflineApplicationId, s.ExitUserEmail, s.RequestSend, s.ExitUserMobileNo, s.StatusFlag, s.Descriptions);
                        var checkofflineid = usercon.CheckApplication(s.OfflineApplicationId);
                        if (checkofflineid != null)
                        {
                            usercon.UpdateUseConfirmation(s.OfflineApplicationId, s.ExitUserEmail, s.RequestSend, s.ExitUserMobileNo, s.StatusFlag, s.Descriptions);
                        }
                        else
                        {
                            usercon.AddUseConfirmation(s.OfflineApplicationId, s.ExitUserEmail, s.RequestSend, s.ExitUserMobileNo, s.StatusFlag, s.Descriptions);
                        }
                        string Message = eAcademy.Models.ResourceCache.Localize("Successfully_application_status_updated");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else if (status == "RequestReject")
                    {
                        //usercon.AddUseConfirmation(s.OfflineApplicationId, s.ExitUserEmail, s.RequestSend, s.ExitUserMobileNo, s.StatusFlag, s.Descriptions);
                        var checkofflineid = usercon.CheckApplication(s.OfflineApplicationId);
                        if (checkofflineid != null)
                        {
                            usercon.UpdateUseConfirmation(s.OfflineApplicationId, s.ExitUserEmail, s.RequestSend, s.ExitUserMobileNo, s.StatusFlag, s.Descriptions);
                        }
                        else
                        {
                            usercon.AddUseConfirmation(s.OfflineApplicationId, s.ExitUserEmail, s.RequestSend, s.ExitUserMobileNo, s.StatusFlag, s.Descriptions);
                        }
                        string Message = eAcademy.Models.ResourceCache.Localize("Successfully_application_status_updated");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    string Messages = eAcademy.Models.ResourceCache.Localize("Successfully_application_status_updated");
                    return Json(new { Message = Messages }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult form1(M_StudentRegisteration r)
        {
            try
            {
                PreAdmissionOnlineRegisterServices PonlineRegister = new PreAdmissionOnlineRegisterServices();
                PreAdmissionPrimaryUserRegisterServices Pprimaryuser = new PreAdmissionPrimaryUserRegisterServices();
                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                PreAdmissionTransactionServices PAdmissionTransaction = new PreAdmissionTransactionServices();
                PreAdmissionFatherRegisterServices PFatherRegister = new PreAdmissionFatherRegisterServices();
                PreAdmissionMotherRegisterServices Pmotherregister = new PreAdmissionMotherRegisterServices();
                PreAdmissionGuardianRegisterServices PguardianRegister = new PreAdmissionGuardianRegisterServices();
                PreAdmissionParentsSblingServices Pparentssibling = new PreAdmissionParentsSblingServices();
                SchoolSettingServices schoolsetting = new SchoolSettingServices();
                string Offlineid = r.OfflineApplicationId;
                int count = Convert.ToInt16(r.NoOfSbling);
                string SiblingStatus = "";
                if (count != 0)
                {
                    SiblingStatus = "Yes";
                }
                else if (count == 0)
                {
                    SiblingStatus = "No";
                }
                string MatchEmailPattern =
                       @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
                string Enrolltype = "";
                if (Regex.IsMatch(r.UserName, MatchEmailPattern))
                {
                    Enrolltype = "Email";
                }
                else
                {
                    Enrolltype = "Mobile";
                }

                int PrimaryAdmissionId = 0; int StudentAdmissionId = 0; int FatherAdmissionId = 0; int MotherAdmissionId = 0; int GuardianAdmissionId = 0;
                //PrimaryUser Details
                var CheckPrimaryUser = new PreAdmissionPrimaryUserRegister();
                if (Enrolltype == "Email")
                {
                    CheckPrimaryUser = Pprimaryuser.GetPrimaryuserdetails(r.primaryUserEmail);
                }
                else
                {
                    long contat = Convert.ToInt64(r.primaryUserContact);
                    CheckPrimaryUser = Pprimaryuser.GetMobilePrimaryuserdetails(contat);
                }
                if (CheckPrimaryUser == null)
                {
                    PrimaryAdmissionId = Pprimaryuser.AddPrimaryUserOffline(r.PrimaryUser, r.primaryUserEmail, r.primaryUserContact, r.CompanyAddress1, r.CompanyAddress2, r.CompanyCity, r.CompanyState, r.CompanyCountry, r.CompanyPostelcode, r.CompanyContact, r.UserCompanyname, r.WorkSameSchool, r.EmployeeDesignationId, r.EmployeeId, r.EmailRequired, r.SmsRequired);
                }
                else
                {
                    PrimaryAdmissionId = Pprimaryuser.UpdatePrimaryUserOffline(CheckPrimaryUser.PrimaryUserAdmissionId, r.PrimaryUser, r.primaryUserEmail, r.primaryUserContact, r.CompanyAddress1, r.CompanyAddress2, r.CompanyCity, r.CompanyState, r.CompanyCountry, r.CompanyPostelcode, r.CompanyContact, r.UserCompanyname, r.WorkSameSchool, r.EmployeeDesignationId, r.EmployeeId, r.EmailRequired, r.SmsRequired);
                }
                //student details
                var student = PstudentRegister.GetOfflineStudentdetails(Offlineid);
                if (student == null)
                {
                    bool exist = PstudentRegister.GetStudentDetailsExist(PrimaryAdmissionId, r.StuFirstName, r.StuLastname, r.DOB);
                    if (exist == false)
                    {
                        StudentAdmissionId = PstudentRegister.AddStudentdetailsFormOffline1(Offlineid, r.StuFirstName, r.StuMiddlename, r.StuLastname, r.Gender, r.DOB, r.PlaceOfBirth, r.Community, r.Religion, r.Nationality, r.BloodGroup, r.AdmissionClass, r.LocAddress1, r.LocAddress2, r.LocCity, r.LocState, r.LocCountry,
                            r.LocPostelcode, r.NoOfSbling, r.AcademicyearId, r.Distance, SiblingStatus, r.GuardianRequried, r.EmergencyContactPersonName, r.EmergencyContactNumber, r.ContactPersonRelationship, r.Email, r.Contact);
                        int name = StudentAdmissionId;
                        var path = Server.MapPath("~/Documents/" + name);
                        System.IO.Directory.CreateDirectory(path);
                        //Directory.CreateDirectory(path);
                        var checkprimaryuserintransaction = PAdmissionTransaction.FindPrimaryuser(PrimaryAdmissionId);
                        if (checkprimaryuserintransaction != null)
                        {
                            if (checkprimaryuserintransaction.PrimaryUserAdmissionId == PrimaryAdmissionId && checkprimaryuserintransaction.StudentAdmissionId == null)
                            {
                                PAdmissionTransaction.UpdateStudentId(PrimaryAdmissionId, StudentAdmissionId);
                            }
                            else if (StudentAdmissionId != 0)
                            {
                                PAdmissionTransaction.AddPrimaryUserandStudentId(PrimaryAdmissionId, StudentAdmissionId);
                            }
                        }
                        else
                        {
                            PAdmissionTransaction.AddPrimaryUserandStudentId(PrimaryAdmissionId, StudentAdmissionId);
                        }
                    }
                    else
                    {
                        String ErrorMessage = ResourceCache.Localize("Student_details_already_exist_Dont_repeat_same_student_details");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    bool exist = PstudentRegister.GetStudentDetailsExist(PrimaryAdmissionId, student.StudentAdmissionId, r.StuFirstName, r.StuLastname, r.DOB);
                    if (exist == false)
                    {
                        StudentAdmissionId = PstudentRegister.UpdateStudentdetailsFormOffline1(Offlineid, r.StuFirstName, r.StuMiddlename, r.StuLastname, r.Gender, r.DOB, r.PlaceOfBirth, r.Community, r.Religion, r.Nationality, r.BloodGroup, r.AdmissionClass, r.LocAddress1, r.LocAddress2, r.LocCity, r.LocState, r.LocCountry,
                            r.LocPostelcode, r.NoOfSbling, r.AcademicyearId, r.Distance, SiblingStatus, r.GuardianRequried, r.EmergencyContactPersonName, r.EmergencyContactNumber, r.ContactPersonRelationship, r.Email, r.Contact);
                    }
                    else
                    {
                        String ErrorMessage = ResourceCache.Localize("Student_details_already_exist_Dont_repeat_same_student_details");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                //parents details
                PstudentRegister.UpdateStudentTransportDetails(StudentAdmissionId, r.TransportRequired, r.TransportDestination, r.TransportPickpoint, r.TransportFeeAmount);
                PstudentRegister.UpdateStudentHostelDetails(StudentAdmissionId, r.HostelRequired, r.AccommodationFeeCategoryId, r.AccommodationSubFeeCategoryId, r.FoodFeeCategoryId, r.FoodSubFeeCategoryId);
                int AdmissionClass = Convert.ToInt16(r.AdmissionClass);
                if (AdmissionClass != 1 && AdmissionClass != 0)
                {
                    PstudentRegister.UpdatePreviousSchool(StudentAdmissionId, r.PreSchool, r.PreClass, r.Pre_BoardofSchool, r.PreMarks, r.PreFromDate1, r.PreToDate);
                }
                var NoOfStudents = PAdmissionTransaction.GetRemainingStudent(PrimaryAdmissionId, StudentAdmissionId);
                if (NoOfStudents.Count == 0)
                {
                    var checkuser = PAdmissionTransaction.GetPrimaryUser(PrimaryAdmissionId, StudentAdmissionId);
                    if (r.PrimaryUser == "Father")
                    {
                        if (checkuser.FatherAdmissionId == null)
                        {
                            if (r.FatherFirstName != null && r.FatherLastName != null && r.FatherDOB != null)//&& r.FatherEmail != null && r.FatherMobileNo != null
                            {
                                FatherAdmissionId = PFatherRegister.AddFatherdetails(r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                                PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                            }
                        }
                        else
                        {
                            Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                            PFatherRegister.UpdatefatherDetails(checkuser.FatherAdmissionId, r.FatherQualification, r.FatherOccupation, r.FatherEmail, r.FatherMobileNo);
                        }
                        if (checkuser.MotherAdmissionId == null)
                        {
                            if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                            {
                                MotherAdmissionId = Pmotherregister.AddMotherdetails(r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                                PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                            }
                        }
                        else
                        {
                            Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                        }
                        if (checkuser.GuardianAdmissionId == null)
                        {
                            if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                            {
                                GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                            }
                        }
                        else
                        {
                            PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                        }
                    }
                    else if (r.PrimaryUser == "Mother")
                    {
                        if (checkuser.MotherAdmissionId == null)
                        {
                            if (r.MotherFirstname != null && r.MotherLastName != null && r.MotherDOB != null)//&& r.MotherEmail != null && r.MotherMobileNo != null
                            {
                                MotherAdmissionId = Pmotherregister.AddMotherdetails(r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                                PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                            }
                        }
                        else
                        {
                            Pmotherregister.UpdatemotherDetails(checkuser.MotherAdmissionId, r.MotherQualification, r.MotherOccupation, r.MotherEmail, r.MotherMobileNo);
                            Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                        }
                        if (checkuser.FatherAdmissionId == null)
                        {
                            if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                            {
                                FatherAdmissionId = PFatherRegister.AddFatherdetails(r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                                PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                            }
                        }
                        else
                        {
                            PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                        }
                        if (checkuser.GuardianAdmissionId == null)
                        {
                            if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                            {
                                GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                            }
                        }
                        else
                        {
                            PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                        }
                    }
                    else if (r.PrimaryUser == "Guardian")
                    {
                        if (checkuser.GuardianAdmissionId == null)
                        {
                            if (r.GuardianFirstname != null && r.GuardianLastName != null && r.GuardianDOB != null)//&& r.GuardianEmail != null && r.GuardianMobileNo != null
                            {
                                GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                            }
                        }
                        else
                        {
                            PguardianRegister.UpdateGuardianDetails(checkuser.GuardianAdmissionId, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                            Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                        }
                        if (checkuser.MotherAdmissionId == null)
                        {
                            if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                            {
                                MotherAdmissionId = Pmotherregister.AddMotherdetails(r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                                PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                            }
                        }
                        else
                        {
                            Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                        }
                        if (checkuser.FatherAdmissionId == null)
                        {
                            if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                            {
                                FatherAdmissionId = PFatherRegister.AddFatherdetails(r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                                PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                            }
                        }
                        else
                        {
                            PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                        }
                    }
                }
                else
                {
                    var checkuser = PAdmissionTransaction.GetPrimaryUser(PrimaryAdmissionId, StudentAdmissionId);
                    if (r.PrimaryUser == "Father")
                    {
                        if (checkuser.FatherAdmissionId == null)
                        {
                            if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                            {
                                FatherAdmissionId = PFatherRegister.CheckFatherExit(PrimaryAdmissionId, StudentAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                                PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                            }
                        }
                        else
                        {
                            PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                            Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                        }
                        if (checkuser.MotherAdmissionId == null)
                        {
                            if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                            {
                                MotherAdmissionId = Pmotherregister.CheckMotherExit(PrimaryAdmissionId, StudentAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                                PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                            }
                        }
                        else
                        {
                            Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                        }
                        if (checkuser.GuardianAdmissionId == null)
                        {
                            if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                            {
                                GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                            }
                        }
                        else
                        {
                            PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                        }
                    }
                    else if (r.PrimaryUser == "Mother")
                    {
                        if (checkuser.MotherAdmissionId == null)
                        {
                            if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                            {
                                MotherAdmissionId = Pmotherregister.CheckMotherExit(PrimaryAdmissionId, StudentAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                                PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                            }
                        }
                        else
                        {
                            Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                            Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                        }
                        if (checkuser.FatherAdmissionId == null)
                        {
                            if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                            {
                                FatherAdmissionId = PFatherRegister.CheckFatherExit(PrimaryAdmissionId, StudentAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                                PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                            }
                        }
                        else
                        {
                            PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                        }
                        if (checkuser.GuardianAdmissionId == null)
                        {
                            if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                            {
                                GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                            }
                        }
                        else
                        {
                            PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                        }
                    }
                    else if (r.PrimaryUser == "Guardian")
                    {
                        if (checkuser.GuardianAdmissionId == null)
                        {
                            if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                            {
                                GuardianAdmissionId = PguardianRegister.CheckGuardianExit(PrimaryAdmissionId, StudentAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                            }
                        }
                        else
                        {
                            PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                            Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                        }
                        if (checkuser.FatherAdmissionId == null)
                        {
                            if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                            {
                                FatherAdmissionId = PFatherRegister.CheckFatherExit(PrimaryAdmissionId, StudentAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                                PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                            }
                        }
                        else
                        {
                            PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                        }
                        if (checkuser.MotherAdmissionId == null)
                        {
                            if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                            {
                                MotherAdmissionId = Pmotherregister.CheckMotherExit(PrimaryAdmissionId, StudentAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                                PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                            }
                        }
                        else
                        {
                            Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                        }
                    }
                }
                //Sibling details
                if (count != 0)//NoOfSibling Count
                {
                    Pparentssibling.UpdateSblingdetailsOffline(Offlineid, StudentAdmissionId);
                }

                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_student_all_details"));
                string Message = eAcademy.Models.ResourceCache.Localize("Sucessfully_records_are_added ");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult StepFormEnrollment()
        {
            try
            {
                Admission1Services As1 = new Admission1Services();
                AcademicyearServices AcYear = new AcademicyearServices();
                SchoolSettingServices ss = new SchoolSettingServices();
                SchoolSettingsServices ss1 = new SchoolSettingsServices();
                TransportDestinationServices TrDestnServ = new TransportDestinationServices();
                var formtype = ss.getSchoolSettings();
                if (formtype != null)
                {
                    var commingAcademicyear = AcYear.GetAcademicYear(); ;
                    if (commingAcademicyear != null)
                    {
                        var Settings = ss1.getSchoolSettings();
                        ViewBag.Email = Settings.FirstOrDefault().EmailRequired;
                        ViewBag.Sms = Settings.FirstOrDefault().SmsRequired;
                        ViewBag.App = Settings.FirstOrDefault().AppRequired;
                        ViewBag.Transport = Settings.FirstOrDefault().TransportRequired;
                        ViewBag.Hostel = Settings.FirstOrDefault().HostelRequired;
                        ViewBag.allDestination = TrDestnServ.DestinationList();
                        return View();
                    }
                    else
                    {
                        ViewBag.ErrorMessage = eAcademy.Models.ResourceCache.Localize("Settings_not_completed");
                        return View();
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = eAcademy.Models.ResourceCache.Localize("Settings_not_completed");
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View();
            }
        }
        [HttpPost]
        public JsonResult Save1(VM_StepStudent r)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionOnlineRegisterServices PonlineRegister = new PreAdmissionOnlineRegisterServices();
                    PreAdmissionPrimaryUserRegisterServices Pprimaryuser = new PreAdmissionPrimaryUserRegisterServices();
                    PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                    PreAdmissionTransactionServices PAdmissionTransaction = new PreAdmissionTransactionServices();
                    PreAdmissionFatherRegisterServices PFatherRegister = new PreAdmissionFatherRegisterServices();
                    PreAdmissionMotherRegisterServices Pmotherregister = new PreAdmissionMotherRegisterServices();
                    PreAdmissionGuardianRegisterServices PguardianRegister = new PreAdmissionGuardianRegisterServices();
                    PreAdmissionParentsSblingServices Pparentssibling = new PreAdmissionParentsSblingServices();
                    string Offlineid = r.OfflineApplicationId;
                    int count = Convert.ToInt16(r.NoOfSbling);
                    int AdmissionClass = Convert.ToInt16(r.AdmissionClass);
                    string SiblingStatus = "";
                    if (r.NoOfSbling != 0)
                    {
                        SiblingStatus = "Yes";
                    }
                    else if (r.NoOfSbling == 0)
                    {
                        SiblingStatus = "No";
                    }
                    string MatchEmailPattern =
                      @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
               + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
               + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
               + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
                    string Enrolltype = "";
                    if (Regex.IsMatch(r.UserName, MatchEmailPattern))
                    {
                        Enrolltype = "Email";
                    }
                    else
                    {
                        Enrolltype = "Mobile";
                    }
                    int PrimaryAdmissionId = 0; int StudentAdmissionId = 0; int FatherAdmissionId = 0; int MotherAdmissionId = 0; int GuardianAdmissionId = 0;
                    var CheckPrimaryUser = new PreAdmissionPrimaryUserRegister();
                    if (Enrolltype == "Email")
                    {
                        CheckPrimaryUser = Pprimaryuser.GetPrimaryuserdetails(r.primaryUserEmail);
                    }
                    else
                    {
                        long contat = Convert.ToInt64(r.primaryUserContact);
                        CheckPrimaryUser = Pprimaryuser.GetMobilePrimaryuserdetails(contat);
                    }
                    if (CheckPrimaryUser == null)
                    {
                        PrimaryAdmissionId = Pprimaryuser.AddPrimaryUserOffline3(r.PrimaryUser, r.primaryUserEmail, r.TotalIncome, r.EmployeeDesignationId, r.EmployeeId, r.WorkSameSchool, r.primaryUserContact, r.EmailRequired, r.SmsRequired);
                    }
                    else
                    {
                        PrimaryAdmissionId = Pprimaryuser.UpdatePrimaryUserOffline3(CheckPrimaryUser.PrimaryUserAdmissionId, r.PrimaryUser, r.primaryUserEmail, r.TotalIncome, r.EmployeeDesignationId, r.EmployeeId, r.WorkSameSchool, r.primaryUserContact, r.EmailRequired, r.SmsRequired);
                    }
                    var student = PstudentRegister.GetOfflineStudentdetails(Offlineid);
                    if (student == null)
                    {
                        bool exist = PstudentRegister.GetStudentDetailsExist(PrimaryAdmissionId, r.StuFirstName, r.StuLastname, r.DOB);
                        if (exist == false)
                        {
                            StudentAdmissionId = PstudentRegister.AddStudentdetailsFormOffline3(Offlineid, r.StuFirstName, r.StuMiddlename, r.StuLastname, r.Gender, r.DOB, r.PlaceOfBirth, r.Community, r.Religion, r.Nationality, r.AdmissionClass, r.NoOfSbling, r.AcademicyearId, SiblingStatus, r.GuardianRequried);
                            int name = StudentAdmissionId;
                            var path = Server.MapPath("~/Documents/" + name);
                            System.IO.Directory.CreateDirectory(path);
                            //Directory.CreateDirectory(path);
                            var checkprimaryuserintransaction = PAdmissionTransaction.FindPrimaryuser(PrimaryAdmissionId);
                            if (checkprimaryuserintransaction != null)
                            {
                                if (checkprimaryuserintransaction.PrimaryUserAdmissionId == PrimaryAdmissionId && checkprimaryuserintransaction.StudentAdmissionId == null)
                                {
                                    PAdmissionTransaction.UpdateStudentId(PrimaryAdmissionId, StudentAdmissionId);
                                }
                                else if (StudentAdmissionId != 0)
                                {
                                    PAdmissionTransaction.AddPrimaryUserandStudentId(PrimaryAdmissionId, StudentAdmissionId);
                                }
                            }
                            else
                            {
                                PAdmissionTransaction.AddPrimaryUserandStudentId(PrimaryAdmissionId, StudentAdmissionId);
                            }
                        }
                        else
                        {
                            String ErrorMessage = ResourceCache.Localize("Student_details_already_exist_Dont_repeat_same_student_details");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        bool exist = PstudentRegister.GetStudentDetailsExist(PrimaryAdmissionId, student.StudentAdmissionId, r.StuFirstName, r.StuLastname, r.DOB);
                        if (exist == false)
                        {
                            StudentAdmissionId = PstudentRegister.UpdateStudentdetailsFormOffline3(Offlineid, r.StuFirstName, r.StuMiddlename, r.StuLastname, r.Gender, r.DOB, r.PlaceOfBirth, r.Community,
                                                 r.Religion, r.Nationality, r.AdmissionClass, r.NoOfSbling, r.AcademicyearId, SiblingStatus, r.GuardianRequried);
                        }
                        else
                        {
                            String ErrorMessage = ResourceCache.Localize("Student_details_already_exist_Dont_repeat_same_student_details");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }

                    }
                    if (AdmissionClass != 1 && AdmissionClass != 0)
                    {
                        PstudentRegister.UpdatePreviousSchool(StudentAdmissionId, r.PreSchool, r.PreClass, r.PreBoardOfSchool, r.PreMarks, r.PreFromDate1, r.PreToDate);
                    }
                    var NoOfStudents = PAdmissionTransaction.GetRemainingStudent(PrimaryAdmissionId, StudentAdmissionId);
                    if (NoOfStudents.Count == 0)
                    {
                        var checkuser = PAdmissionTransaction.GetPrimaryUser(PrimaryAdmissionId, StudentAdmissionId);
                        if (r.PrimaryUser == "Father")
                        {
                            if (checkuser.FatherAdmissionId == null)
                            {
                                if (r.FatherFirstName != null && r.FatherLastName != null && r.FatherDOB != null)//&& r.FatherEmail != null && r.FatherMobileNo != null
                                {
                                    FatherAdmissionId = PFatherRegister.AddFatherdetails(r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                                    PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                    Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                                }
                            }
                            else
                            {
                                PFatherRegister.UpdatefatherDetails(checkuser.FatherAdmissionId, r.FatherQualification, r.FatherOccupation, r.FatherEmail, r.FatherMobileNo);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                            }
                            if (checkuser.MotherAdmissionId == null)
                            {
                                if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                                {
                                    MotherAdmissionId = Pmotherregister.AddMotherdetails(r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                                    PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                }
                            }
                            else
                            {
                                Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                            }
                            if (checkuser.GuardianAdmissionId == null)
                            {
                                if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                                {
                                    GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                    PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                }
                            }
                            else
                            {
                                PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                            }
                        }
                        else if (r.PrimaryUser == "Mother")
                        {
                            if (checkuser.MotherAdmissionId == null)
                            {
                                if (r.MotherFirstname != null && r.MotherLastName != null && r.MotherDOB != null)//&& r.MotherEmail != null && r.MotherMobileNo != null
                                {
                                    MotherAdmissionId = Pmotherregister.AddMotherdetails(r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                                    PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                    Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                                }
                            }
                            else
                            {
                                Pmotherregister.UpdatemotherDetails(checkuser.MotherAdmissionId, r.MotherQualification, r.MotherOccupation, r.MotherEmail, r.MotherMobileNo);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                            }
                            if (checkuser.FatherAdmissionId == null)
                            {
                                if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                                {
                                    FatherAdmissionId = PFatherRegister.AddFatherdetails(r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                                    PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                }
                            }
                            else
                            {
                                PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                            }
                            if (checkuser.GuardianAdmissionId == null)
                            {
                                if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                                {
                                    GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                    PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                }
                            }
                            else
                            {
                                PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                            }
                        }
                        else if (r.PrimaryUser == "Guardian")
                        {
                            if (checkuser.GuardianAdmissionId == null)
                            {
                                if (r.GuardianFirstname != null && r.GuardianLastName != null && r.GuardianDOB != null)//&& r.GuardianEmail != null && r.GuardianMobileNo != null
                                {
                                    GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                    PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                    Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                                }
                            }
                            else
                            {
                                PguardianRegister.UpdateGuardianDetails(checkuser.GuardianAdmissionId, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                            }
                            if (checkuser.MotherAdmissionId == null)
                            {
                                if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                                {
                                    MotherAdmissionId = Pmotherregister.AddMotherdetails(r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                                    PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                }
                            }
                            else
                            {
                                Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                            }
                            if (checkuser.FatherAdmissionId == null)
                            {
                                if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                                {
                                    FatherAdmissionId = PFatherRegister.AddFatherdetails(r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                                    PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                }
                            }
                            else
                            {
                                PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                            }
                        }
                    }
                    else
                    {
                        var checkuser = PAdmissionTransaction.GetPrimaryUser(PrimaryAdmissionId, StudentAdmissionId);
                        if (r.PrimaryUser == "Father")
                        {
                            if (checkuser.FatherAdmissionId == null)
                            {
                                if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                                {
                                    FatherAdmissionId = PFatherRegister.CheckFatherExit(PrimaryAdmissionId, StudentAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                                    PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                    Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                                }
                            }
                            else
                            {
                                PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                            }
                            if (checkuser.MotherAdmissionId == null)
                            {
                                if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                                {
                                    MotherAdmissionId = Pmotherregister.CheckMotherExit(PrimaryAdmissionId, StudentAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                                    PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                }
                            }
                            else
                            {
                                Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                            }
                            if (checkuser.GuardianAdmissionId == null)
                            {
                                if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                                {
                                    GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                    PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                }
                            }
                            else
                            {
                                PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                            }
                        }
                        else if (r.PrimaryUser == "Mother")
                        {
                            if (checkuser.MotherAdmissionId == null)
                            {
                                if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                                {
                                    MotherAdmissionId = Pmotherregister.CheckMotherExit(PrimaryAdmissionId, StudentAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                                    PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                    Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                                }
                            }
                            else
                            {
                                Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                            }
                            if (checkuser.FatherAdmissionId == null)
                            {
                                if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                                {
                                    FatherAdmissionId = PFatherRegister.CheckFatherExit(PrimaryAdmissionId, StudentAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                                    PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                }
                            }
                            else
                            {
                                PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                            }
                            if (checkuser.GuardianAdmissionId == null)
                            {
                                if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                                {
                                    GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                    PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                }
                            }
                            else
                            {
                                PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                            }
                        }
                        else if (r.PrimaryUser == "Guardian")
                        {
                            if (checkuser.GuardianAdmissionId == null)
                            {
                                if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                                {
                                    GuardianAdmissionId = PguardianRegister.CheckGuardianExit(PrimaryAdmissionId, StudentAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                    PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                    Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                                }
                            }
                            else
                            {
                                PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                            }
                            if (checkuser.FatherAdmissionId == null)
                            {
                                if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                                {
                                    FatherAdmissionId = PFatherRegister.CheckFatherExit(PrimaryAdmissionId, StudentAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                                    PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                }
                            }
                            else
                            {
                                PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                            }
                            if (checkuser.MotherAdmissionId == null)
                            {
                                if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                                {
                                    MotherAdmissionId = Pmotherregister.CheckMotherExit(PrimaryAdmissionId, StudentAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                                    PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                }
                            }
                            else
                            {
                                Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                            }
                        }
                    }

                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_student_all_details"));
                    string Message = eAcademy.Models.ResourceCache.Localize("Sucessfully_student_details_added");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Save2(VM_StepParents r)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionPrimaryUserRegisterServices Pprimaryuser = new PreAdmissionPrimaryUserRegisterServices();
                    PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                    PreAdmissionParentsSblingServices Pparentssibling = new PreAdmissionParentsSblingServices();
                    string offlineid = r.OfflineApplicationId;
                    int count = Convert.ToInt16(r.NoOfSbling);
                    string SiblingStatus = "";
                    int PrimaryAdmissionId = 0; int StudentAdmissionId = 0;

                    if (r.NoOfSbling != 0 && r.NoOfSbling != null)
                    {
                        SiblingStatus = "Yes";
                    }
                    else if (r.NoOfSbling == 0)
                    {
                        SiblingStatus = "No";
                    }
                    var student = PstudentRegister.GetOfflineStudentdetails(offlineid);
                    if (student != null)
                    {
                        StudentAdmissionId = PstudentRegister.UpdatestudentAddressothersDetailsForm4(r.LocAddress1, r.LocAddress2, r.LocCity, r.LocState, r.LocCountry, r.LocPostelcode, r.NoOfSbling, r.Height, r.Weights,
                            r.IdentificationMark, r.Email, r.Contact, r.PerAddress1, r.PerAddress2, r.PerCity, r.PerState, r.PerCountry, r.PerPostelcode, r.Distance, SiblingStatus, student.StudentAdmissionId, r.BloodGroup, r.EmergencyContactPersonName, r.EmergencyContactNumber, r.ContactPersonRelationship);
                    }
                    var CheckPrimaryUser = Pprimaryuser.getprimaryuserusingstudentadmissionid(StudentAdmissionId);
                    if (CheckPrimaryUser != null)
                    {
                        PrimaryAdmissionId = Pprimaryuser.UpdatePrimaryUserOfflinestep(r.PrimaryUser, r.primaryUserEmail, r.primaryUserContact, r.CompanyAddress1, r.CompanyAddress2, r.CompanyCity, r.CompanyState, r.CompanyCountry, r.CompanyPostelcode, r.CompanyContact, r.UserCompanyname);
                    }
                    if (count != 0)//NoOfSibling Count
                    {
                        Pparentssibling.UpdateSblingdetailsOffline(offlineid, StudentAdmissionId);
                    }
                    PstudentRegister.UpdateStudentTransportDetails(StudentAdmissionId, r.TransportRequired, r.TransportDestination, r.TransportPickpoint, r.TransportFeeAmount);
                    PstudentRegister.UpdateStudentHostelDetails(StudentAdmissionId, r.HostelRequired, r.AccommodationFeeCategoryId, r.AccommodationSubFeeCategoryId, r.FoodFeeCategoryId, r.FoodSubFeeCategoryId);
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_student_all_details"));
                    string Message = eAcademy.Models.ResourceCache.Localize("Sucessfully_Parents_details_added ");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult UploadFile()
        {
            try
            {
                string offlineId = HttpContext.Request["OfflineId"];
                PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                var stuadmissionid = Pstudent.getStudentidOffline(offlineId);
                var stuid = stuadmissionid.StudentAdmissionId;
                int pathname = stuadmissionid.StudentAdmissionId;
                string name = ""; long FileSzie = 0;
                string BirthCertificate = "";
                string IncomeCertificate = ""; string TransferCertificate = ""; string CommunityCertificate = "";
                string studentphoto = "";
                FileHelper fh = new FileHelper();
                if (stuadmissionid != null)
                {
                    var Birth = HttpContext.Request.Files["UploadBirth"];
                    if (Birth != null)
                    {
                        name = "BC";
                        FileSzie = 512000;
                        var BirthCertificate1 = fh.CheckFile(Birth, pathname, name, FileSzie);
                        if (BirthCertificate1.FileSizeId == 98)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Birth_certificate_file_size_exceeds,whithin_512kb");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (BirthCertificate1.FileTypeId == 99)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Birth_certificate_file_format_not_supported");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (BirthCertificate1.FileSizeId != 98 && BirthCertificate1.FileTypeId != 99 && BirthCertificate1.filename != "")
                        {
                            BirthCertificate = BirthCertificate1.filename;
                            Pstudent.updateBirthcertificateOffline(offlineId, stuid, BirthCertificate);
                        }
                    }
                    var community = HttpContext.Request.Files["UploadCommunity"];
                    if (community != null)
                    {
                        name = "CC";
                        FileSzie = 512000;
                        var CommunityCertificate1 = fh.CheckFile(community, pathname, name, FileSzie);
                        if (CommunityCertificate1.FileSizeId == 98)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Community_certificate_file_size_exceeds,whithin_512kb");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (CommunityCertificate1.FileTypeId == 99)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Community_certificate_file_format_not_supported ");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (CommunityCertificate1.FileSizeId != 98 && CommunityCertificate1.FileTypeId != 99 && CommunityCertificate1.filename != "")
                        {
                            CommunityCertificate = CommunityCertificate1.filename;
                            Pstudent.updateCommunitycertificateOffline(offlineId, stuid, CommunityCertificate);
                        }
                    }
                    var Income = HttpContext.Request.Files["UploadIncome"];
                    if (Income != null)
                    {
                        name = "IC";
                        FileSzie = 512000;
                        var IncomeCertificate1 = fh.CheckFile(Income, pathname, name, FileSzie);
                        if (IncomeCertificate1.FileSizeId == 98)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Income_certificate_file_size_exceeds,whithin_512kb ");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (IncomeCertificate1.FileTypeId == 99)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Income_certificate_file_format_not_supported");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (IncomeCertificate1.FileSizeId != 98 && IncomeCertificate1.FileTypeId != 99 && IncomeCertificate1.filename != "")
                        {
                            IncomeCertificate = IncomeCertificate1.filename;
                            Pstudent.updateIncomecertificateOffline(offlineId, stuid, IncomeCertificate);
                        }
                    }
                    var Transfer = HttpContext.Request.Files["UploadTransfer"];
                    if (Transfer != null)
                    {
                        name = "TC";
                        FileSzie = 512000;
                        var TransferCertificate1 = fh.CheckFile(Transfer, pathname, name, FileSzie);
                        if (TransferCertificate1.FileSizeId == 98)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Transfer_certificate_file_size_exceeds,whithin_512kb");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (TransferCertificate1.FileTypeId == 99)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Transfer_certificate_file_format_not_supported ");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (TransferCertificate1.FileSizeId != 98 && TransferCertificate1.FileTypeId != 99 && TransferCertificate1.filename != "")
                        {
                            TransferCertificate = TransferCertificate1.filename;
                            Pstudent.updateTransfercertificateOffline(offlineId, stuid, TransferCertificate);
                        }
                    }
                    var Image = HttpContext.Request.Files["UploadImage"];
                    if (Image != null)
                    {
                        name = "Photo";
                        FileSzie = 512000;
                        var Photo1 = fh.CheckImageFile(Image, pathname, name, FileSzie);
                        if (Photo1.FileSizeId == 98)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Student_photo_file_size_exceeds,whithin_512kb");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (Photo1.FileTypeId == 99)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Student_photo_file_format_not_supported ");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (Photo1.FileSizeId != 98 && Photo1.FileTypeId != 99 && Photo1.filename != "")
                        {
                            studentphoto = Photo1.filename;
                            Pstudent.updateStudentPhotoOffline(offlineId, stuid, studentphoto);
                        }
                    }
                    string Message = eAcademy.Models.ResourceCache.Localize("Sucessfully_all_records_are_added") + " " + ResourceCache.Localize("Please") + " " + ResourceCache.Localize("toconfirmyourdetails");
                    return Json(new { Message = Message, IncomeCertificate = IncomeCertificate, BirthCertificate = BirthCertificate, CommunityCertificate = CommunityCertificate, TransferCertificate = TransferCertificate, studentphoto = studentphoto }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_upload_Required_Files");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;

                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SingleReportOffline(string Id, string Sid)
        {
            try
            {
                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                EmployeeDesinationServices employee_Design = new EmployeeDesinationServices();
                SchoolSettingsServices ss2 = new SchoolSettingsServices();
                TransportDestinationServices destination = new TransportDestinationServices();
                TransportPickPointServices pickpoint = new TransportPickPointServices();
                HostelConfigSubCategoriesServices hostelsubcategory = new HostelConfigSubCategoriesServices();

                var ans = ss2.getSchoolSettings();
                ViewBag.Email = ans.FirstOrDefault().EmailRequired;
                ViewBag.Sms = ans.FirstOrDefault().SmsRequired;
                ViewBag.App = ans.FirstOrDefault().AppRequired;
                var primaryuserid = Convert.ToInt16(QSCrypt.Decrypt(Id));
                var studentid = Convert.ToInt16(QSCrypt.Decrypt(Sid));
                var result = PstudentRegister.GetStudentAlldetails(primaryuserid, studentid);
                if (result.FirstOrDefault().Pre_BoardofSchool != null)
                {
                    ViewBag.previousBoardtxt = board.Showpreviousmedium(Convert.ToInt32(result.FirstOrDefault().Pre_BoardofSchool));
                }
                if (result.FirstOrDefault().PreClass != null)
                {
                    ViewBag.previousclass_text = clas.GetParticularClass(Convert.ToInt32(result.FirstOrDefault().PreClass));
                }

                if (result.FirstOrDefault().TransportDestination != null && result.FirstOrDefault().TransportPickpoint != null)
                {
                    int destinationid = Convert.ToInt16(result.FirstOrDefault().TransportDestination);
                    int PickPointId = Convert.ToInt16(result.FirstOrDefault().TransportPickpoint);
                    ViewBag.TransportDestination = destination.getDestination(destinationid).DestinationName;
                    ViewBag.TransportPickpoint = pickpoint.GetPickPoint(destinationid, PickPointId);
                }
                if (result.FirstOrDefault().FoodFeeCategoryId != null)
                {
                    int foodFeesId = Convert.ToInt16(result.FirstOrDefault().FoodFeeCategoryId);
                    int AccomFeesid = Convert.ToInt16(result.FirstOrDefault().AccommodationFeeCategoryId);
                    if (result.FirstOrDefault().FoodSubFeeCategoryId != null)
                    {
                        int foodsubFeesId = Convert.ToInt16(result.FirstOrDefault().FoodSubFeeCategoryId);
                        ViewBag.FoodSubCategory = hostelsubcategory.GetSubCategory(foodFeesId, foodsubFeesId);
                    }
                    if (result.FirstOrDefault().AccommodationSubFeeCategoryId != null)
                    {
                        int AccomsubFeesId = Convert.ToInt16(result.FirstOrDefault().AccommodationSubFeeCategoryId);
                        ViewBag.AccomSubCategory = hostelsubcategory.GetSubCategory(AccomFeesid, AccomsubFeesId);
                    }
                }
                if (result.FirstOrDefault().EmployeeDesignationId != null)
                {
                    int id = Convert.ToInt16(result.FirstOrDefault().EmployeeDesignationId);
                    ViewBag.EmployeeDesignation = employee_Design.GetparticularEmployeeDesignation(id);
                }
                return View(result);
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
                return View("error");
            }
        }
        [HttpPost]
        public ActionResult PDF1Offline(M_StudentRegisteration r)
        {
            try
            {
                var studentid = Convert.ToInt16(r.Stuadmissionid);
                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                PreAdmissionTransactionServices Ptrans = new PreAdmissionTransactionServices();
                EmployeeDesinationServices employee_Design = new EmployeeDesinationServices();
                SchoolSettingsServices ss2 = new SchoolSettingsServices();
                TransportDestinationServices destination = new TransportDestinationServices();
                TransportPickPointServices pickpoint = new TransportPickPointServices();
                HostelConfigSubCategoriesServices hostelsubcategory = new HostelConfigSubCategoriesServices();
                var primaryuserid = Convert.ToInt16(Ptrans.FindPrimaryUserId(studentid).PrimaryUserAdmissionId);
                var ans = ss2.getSchoolSettings();
                ViewBag.Email = ans.FirstOrDefault().EmailRequired;
                ViewBag.Sms = ans.FirstOrDefault().SmsRequired;
                ViewBag.App = ans.FirstOrDefault().AppRequired;
                var result = PstudentRegister.GetStudentAlldetails(primaryuserid, studentid);
                if (result.FirstOrDefault().Pre_BoardofSchool != null)
                {
                    ViewBag.previousBoardtxt = board.Showpreviousmedium(Convert.ToInt32(result.FirstOrDefault().Pre_BoardofSchool));
                }
                if (result.FirstOrDefault().PreClass != null)
                {
                    ViewBag.previousclass_text = clas.GetParticularClass(Convert.ToInt32(result.FirstOrDefault().PreClass));
                }
                string cusomtSwitches = string.Format("--print-media-type  --header-html {0}  ",
               Url.Action("Header", "OnlineRegister", new { area = "" }, "http"));
                if (result.FirstOrDefault().TransportDestination != null && result.FirstOrDefault().TransportPickpoint != null)
                {
                    int destinationid = Convert.ToInt16(result.FirstOrDefault().TransportDestination);
                    int PickPointId = Convert.ToInt16(result.FirstOrDefault().TransportPickpoint);
                    ViewBag.TransportDestination = destination.getDestination(destinationid).DestinationName;
                    ViewBag.TransportPickpoint = pickpoint.GetPickPoint(destinationid, PickPointId);
                }
                if (result.FirstOrDefault().FoodFeeCategoryId != null)
                {
                    int foodFeesId = Convert.ToInt16(result.FirstOrDefault().FoodFeeCategoryId);
                    int AccomFeesid = Convert.ToInt16(result.FirstOrDefault().AccommodationFeeCategoryId);
                    if (result.FirstOrDefault().FoodSubFeeCategoryId != null)
                    {
                        int foodsubFeesId = Convert.ToInt16(result.FirstOrDefault().FoodSubFeeCategoryId);
                        ViewBag.FoodSubCategory = hostelsubcategory.GetSubCategory(foodFeesId, foodsubFeesId);
                    }
                    if (result.FirstOrDefault().AccommodationSubFeeCategoryId != null)
                    {
                        int AccomsubFeesId = Convert.ToInt16(result.FirstOrDefault().AccommodationSubFeeCategoryId);
                        ViewBag.AccomSubCategory = hostelsubcategory.GetSubCategory(AccomFeesid, AccomsubFeesId);
                    }
                }
                if (result.FirstOrDefault().EmployeeDesignationId != null)
                {
                    int id = Convert.ToInt16(result.FirstOrDefault().EmployeeDesignationId);
                    ViewBag.EmployeeDesignation = employee_Design.GetparticularEmployeeDesignation(id);
                }
                return new ViewAsPdf("~/Views/OnlineRegister/Single_GeneratePDF.cshtml", result)
                {
                    FileName = "ApplicationNumber" + studentid + ".pdf",
                    // CustomSwitches = cusomtSwitches
                };
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
                return View("error");

            }
        }
        public ActionResult StepReportOffline(string Id, string Sid)
        {
            try
            {
                var primaryuserid = Convert.ToInt16(QSCrypt.Decrypt(Id));
                var studentid = Convert.ToInt16(QSCrypt.Decrypt(Sid));
                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                EmployeeDesinationServices employee_Design = new EmployeeDesinationServices();
                SchoolSettingsServices ss2 = new SchoolSettingsServices();
                TransportDestinationServices destination = new TransportDestinationServices();
                TransportPickPointServices pickpoint = new TransportPickPointServices();
                HostelConfigSubCategoriesServices hostelsubcategory = new HostelConfigSubCategoriesServices();
                var stu = 0;
                var ss = PstudentRegister.getOnlineregid(studentid);
                ViewBag.Regid = QSCrypt.Encrypt(ss.OnlineRegisterId.ToString());
                ViewBag.sid = QSCrypt.Encrypt(stu.ToString());
                var ans = ss2.getSchoolSettings();
                ViewBag.Email = ans.FirstOrDefault().EmailRequired;
                ViewBag.Sms = ans.FirstOrDefault().SmsRequired;
                ViewBag.App = ans.FirstOrDefault().AppRequired;
                var result = PstudentRegister.GetStudentAlldetails(primaryuserid, studentid);
                if (result.FirstOrDefault().Pre_BoardofSchool != null)
                {
                    ViewBag.previousBoardtxt = board.Showpreviousmedium(Convert.ToInt32(result.FirstOrDefault().Pre_BoardofSchool));
                }
                if (result.FirstOrDefault().PreClass != null)
                {
                    ViewBag.previousclass_text = clas.GetParticularClass(Convert.ToInt32(result.FirstOrDefault().PreClass));
                }
                if (result.FirstOrDefault().TransportDestination != null && result.FirstOrDefault().TransportPickpoint != null)
                {
                    int destinationid = Convert.ToInt16(result.FirstOrDefault().TransportDestination);
                    int PickPointId = Convert.ToInt16(result.FirstOrDefault().TransportPickpoint);
                    ViewBag.TransportDestination = destination.getDestination(destinationid).DestinationName;
                    ViewBag.TransportPickpoint = pickpoint.GetPickPoint(destinationid, PickPointId);
                }
                if (result.FirstOrDefault().FoodFeeCategoryId != null)
                {
                    int foodFeesId = Convert.ToInt16(result.FirstOrDefault().FoodFeeCategoryId);
                    int AccomFeesid = Convert.ToInt16(result.FirstOrDefault().AccommodationFeeCategoryId);
                    if (result.FirstOrDefault().FoodSubFeeCategoryId != null)
                    {
                        int foodsubFeesId = Convert.ToInt16(result.FirstOrDefault().FoodSubFeeCategoryId);
                        ViewBag.FoodSubCategory = hostelsubcategory.GetSubCategory(foodFeesId, foodsubFeesId);
                    }
                    if (result.FirstOrDefault().AccommodationSubFeeCategoryId != null)
                    {
                        int AccomsubFeesId = Convert.ToInt16(result.FirstOrDefault().AccommodationSubFeeCategoryId);
                        ViewBag.AccomSubCategory = hostelsubcategory.GetSubCategory(AccomFeesid, AccomsubFeesId);
                    }
                }
                if (result.FirstOrDefault().EmployeeDesignationId != null)
                {
                    int id = Convert.ToInt16(result.FirstOrDefault().EmployeeDesignationId);
                    ViewBag.EmployeeDesignation = employee_Design.GetparticularEmployeeDesignation(id);
                }
                return View(result);
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
                return View("error");
            }
        }
        [HttpPost]
        public ActionResult PDF2Offline(M_StudentRegisteration r)
        {
            try
            {
                var studentid = Convert.ToInt16(r.Stuadmissionid);
                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                PreAdmissionTransactionServices Ptrans = new PreAdmissionTransactionServices();
                EmployeeDesinationServices employee_Design = new EmployeeDesinationServices();
                SchoolSettingsServices ss2 = new SchoolSettingsServices();
                TransportDestinationServices destination = new TransportDestinationServices();
                TransportPickPointServices pickpoint = new TransportPickPointServices();
                HostelConfigSubCategoriesServices hostelsubcategory = new HostelConfigSubCategoriesServices();
                var primaryuserid = Convert.ToInt16(Ptrans.FindPrimaryUserId(studentid).PrimaryUserAdmissionId);
                var result = PstudentRegister.GetStudentAlldetails(primaryuserid, studentid);
                var ans = ss2.getSchoolSettings();
                if (result.FirstOrDefault().Pre_BoardofSchool != null)
                {
                    ViewBag.previousBoardtxt = board.Showpreviousmedium(Convert.ToInt32(result.FirstOrDefault().Pre_BoardofSchool));
                }
                if (result.FirstOrDefault().PreClass != null)
                {
                    ViewBag.previousclass_text = clas.GetParticularClass(Convert.ToInt32(result.FirstOrDefault().PreClass));
                }
                ViewBag.Email = ans.FirstOrDefault().EmailRequired;
                ViewBag.Sms = ans.FirstOrDefault().SmsRequired;
                ViewBag.App = ans.FirstOrDefault().AppRequired;
                string cusomtSwitches = string.Format("--print-media-type  --header-html {0}  ",
               Url.Action("Header", "OnlineRegister", new { area = "" }, "http"));
                if (result.FirstOrDefault().EmployeeDesignationId != null)
                {
                    int id = Convert.ToInt16(result.FirstOrDefault().EmployeeDesignationId);
                    ViewBag.EmployeeDesignation = employee_Design.GetparticularEmployeeDesignation(id);
                }
                if (result.FirstOrDefault().TransportDestination != null && result.FirstOrDefault().TransportPickpoint != null)
                {
                    int destinationid = Convert.ToInt16(result.FirstOrDefault().TransportDestination);
                    int PickPointId = Convert.ToInt16(result.FirstOrDefault().TransportPickpoint);
                    ViewBag.TransportDestination = destination.getDestination(destinationid).DestinationName;
                    ViewBag.TransportPickpoint = pickpoint.GetPickPoint(destinationid, PickPointId);
                }
                if (result.FirstOrDefault().FoodFeeCategoryId != null)
                {
                    int foodFeesId = Convert.ToInt16(result.FirstOrDefault().FoodFeeCategoryId);
                    int AccomFeesid = Convert.ToInt16(result.FirstOrDefault().AccommodationFeeCategoryId);
                    if (result.FirstOrDefault().FoodSubFeeCategoryId != null)
                    {
                        int foodsubFeesId = Convert.ToInt16(result.FirstOrDefault().FoodSubFeeCategoryId);
                        ViewBag.FoodSubCategory = hostelsubcategory.GetSubCategory(foodFeesId, foodsubFeesId);
                    }
                    if (result.FirstOrDefault().AccommodationSubFeeCategoryId != null)
                    {
                        int AccomsubFeesId = Convert.ToInt16(result.FirstOrDefault().AccommodationSubFeeCategoryId);
                        ViewBag.AccomSubCategory = hostelsubcategory.GetSubCategory(AccomFeesid, AccomsubFeesId);
                    }
                }
                return new ViewAsPdf("~/Views/StudentEnrollment/Step_GeneratePDF.cshtml", result)
                {
                    FileName = "ApplicationNumber" + studentid + ".pdf",
                    //CustomSwitches = cusomtSwitches
                };
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
                return View("error");
            }
        }

        public ActionResult OldStudents()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
                return View("error");
            }

        }
        [HttpPost]
        public ActionResult OldStudents(OldStudents1 o)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SeparateStudentServices separate = new SeparateStudentServices();
                    var oldStudentList = separate.getOldStudentList(o.allAcademicYears, o.allClass, o.Section);
                    ViewBag.year = o.allAcademicYears;
                    ViewBag.class1 = o.allClass;
                    ViewBag.sec = o.Section;
                    SectionServices sec = new SectionServices();
                    ViewBag.secList = sec.getSection(o.allClass);
                    ViewBag.count = oldStudentList.Count;
                    return View(oldStudentList);
                }
                else
                {
                    ViewBag.year = o.allAcademicYears;
                    ViewBag.class1 = o.allClass;
                    if (o.allClass != 0)
                    {
                        SectionServices sec = new SectionServices();
                        ViewBag.secList = sec.getSection(o.allClass);
                    }
                    ViewBag.sec = o.Section;
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult AppliedSeparationForms()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
                return View("error");
            }
        }
        [HttpPost]
        public ActionResult AppliedSeparationForms(FormCollection fm)
        {
            try
            {
                int year = Convert.ToInt32(fm["allAcademicYears"]);
                ViewBag.year = fm["allAcademicYears"];
                ApplySeparationServices applySeparation = new ApplySeparationServices();
                var list = applySeparation.getSeparationList(year);
                ViewBag.count = list.Count;
                return View(list);
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
                return View("error");
            }
        }

        // Assign Class
        public ActionResult AssignClass()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
                return View("error");
            }
        }
        public JsonResult GetStudentListToAssignClass(int passYearId, int passClassId)
        {
            var list = stu.getStudentListToAssignClass(passYearId, passClassId);
            var count = list.ToList().Count;
            return Json(new { list, count }, JsonRequestBehavior.AllowGet);
        }
        public int FeeCollectionId;
        public int? PrimayUserRegisterId;
        public ActionResult Add_AssignClass(AssignClass m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SchoolSettingsServices sett = new SchoolSettingsServices();
                    var getStudentIdFormat = sett.SchoolSettings();
                    if (getStudentIdFormat == null)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Student_Id_format_is_not_availabe,_Please_perform_school_configuration");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    var TotalCount = m.TotalCount;
                    int year = m.ToAcademicYears;
                    int classId = m.ToClass;
                    int secId = m.ToSection;
                    int size = TotalCount + 1;
                    string[] stuRegisterId = new string[size];
                    string[] checkboxId = new string[size];
                    string[] studentStatus = new string[size];
                    StudentRegisterServices stuReg = new StudentRegisterServices();
                    PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                    ParentRegisterServices parentReg = new ParentRegisterServices();
                    AssignClassToStudentServices assignStu = new AssignClassToStudentServices();
                    FeeCollectionServices feeCollect = new FeeCollectionServices();
                    FeeCollectionCategoryServices feeCollectionCategory = new FeeCollectionCategoryServices();
                    int selectCount = 0;
                    for (int i = 0; i <= TotalCount; i++)
                    {
                        var check1 = m.checkboxId[i];
                        if (check1 == "on")
                        {
                            selectCount = 1;
                            selectCount++;
                        }
                    }
                    SectionStrengthServices sectionStrength = new SectionStrengthServices();
                    var classRoomStrength = sectionStrength.TotalStrength(year, classId, secId);
                    var classAssignedStudentStrength = assignStu.GetClassStudent(year, classId, secId);
                    if (classRoomStrength != null && classAssignedStudentStrength != null)
                    {
                        var totalStrength = classRoomStrength.Strength;
                        var AssignedStudentStrength = classAssignedStudentStrength.Count;
                        var vacantStrength = totalStrength - AssignedStudentStrength;
                        int Vacancy = Convert.ToInt32(vacantStrength);
                        if (Vacancy < selectCount)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Selected_student_count_exceeds_from_vacant_of_selected_section_vacancy");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    for (int i = 0; i <= TotalCount; i++)
                    {
                        var check1 = m.checkboxId[i];
                        var studentStatus1 = m.StudentStatus[i];
                        if (check1 == "on")
                        {
                            stuRegisterId[i] = QSCrypt.Decrypt(m.StuRegisterId[i]);
                            int stuRegId = Convert.ToInt32(stuRegisterId[i]);
                            string studentStatusVal = m.StudentStatus[i];
                            if (stuRegId != 0)
                            {
                                int studentRegisterId = stuRegId;
                                var getStudentRow = stu.getStudentRow(stuRegId);
                                if (getStudentRow != null)
                                {
                                    assignStu.AddAssignClassToStudent(studentRegisterId, year, classId, secId, AdminId, studentStatusVal);      // To class assign
                                    stu.updateStatusFlag(stuRegId);    // To update status as class assigned in student table
                                }
                                var CheckPayment = feeCollect.checkDataExist(studentRegisterId, year, classId);
                                if (CheckPayment != null)
                                {
                                    FeeCollectionId = CheckPayment.FeeCollectionId;
                                    feeCollect.updateSectionWithRegId(year, classId, studentRegisterId, secId);        // update sectionid in fee collection
                                }
                                var checkFeeCategory = feeCollectionCategory.checkDataExist(FeeCollectionId, year, classId);
                                if (checkFeeCategory != null)
                                {
                                    feeCollectionCategory.updateSectionWithRegId(FeeCollectionId, year, classId, studentRegisterId, secId);  // update sectionid in fee collection category
                                }

                                if (studentStatus1 == "New Entry")
                                {
                                    AcademicyearServices AcYear = new AcademicyearServices();
                                    ClassServices clas = new ClassServices();
                                    SectionServices sec = new SectionServices();
                                    var getYear = AcYear.AcademicYear(year).AcademicYear1;
                                    var getClass = clas.getClassNameByClassID(classId).ClassType;
                                    var getSection = sec.getSectionName(classId, secId).SectionName;
                                    var SchoolCodeFormat = getStudentIdFormat.SchoolCode;
                                    var StudentIdFormat = getStudentIdFormat.StudentIdFormat;
                                    var ParentIdFormat = getStudentIdFormat.ParentIDFormat;
                                    var studentcurrentId = "";
                                    var studentcurrentPwd = "";
                                    var studentFullName = "";
                                    var SchoolName = ConfigurationManager.AppSettings["SchoolName"];
                                    if (StudentIdFormat != null && getStudentRow != null)
                                    {
                                        var StuId = StudentIdFormat + studentRegisterId;
                                        string StudentId = StuId;
                                        studentcurrentId = StuId;
                                        var StudentFirstName = getStudentRow.FirstName;
                                        studentFullName = getStudentRow.FirstName + " " + getStudentRow.LastName;
                                        string Salt = StudentFirstName.Substring(0, 3);
                                        Random S_random = new Random();
                                        string S_randomNumber = S_random.Next(00000, 99999).ToString();
                                        var S_pwd = Salt + S_randomNumber;
                                        studentcurrentPwd = S_pwd;
                                        var PasswordSalt = Salt + S_pwd;
                                        string Password = FormsAuthentication.HashPasswordForStoringInConfigFile(PasswordSalt, "SHA1");
                                        stu.UpdateStudentIdWithPassword(studentRegisterId, StudentId, Salt, Password);
                                        // to rename folder
                                        string path = Server.MapPath("~/Documents/");
                                        int admissionId = getStudentRow.AdmissionId.Value;
                                        string oldPath = path + admissionId;
                                        string newPath = path + StudentId;
                                        if (getStudentRow.EnrollmentType == "Excel")
                                        {
                                            Directory.CreateDirectory(newPath); 
                                        }
                                        else
                                        {
                                            Directory.Move(oldPath, newPath);
                                        }
                                    
                                        // Email username password
                                        StreamReader reader = new StreamReader(Server.MapPath("~/Views/StudentEnrollment/StudentEmailPage.html"));
                                        string readFile = reader.ReadToEnd();
                                        string myString = "";
                                        myString = readFile;
                                        myString = myString.Replace("$$FirstName$$", studentFullName.ToString());
                                        //  myString = myString.Replace("$$admissionId$$", admissionId.ToString());
                                        myString = myString.Replace("$$year$$", getYear);
                                        myString = myString.Replace("$$class$$", getClass);
                                        myString = myString.Replace("$$section$$", getSection);
                                        myString = myString.Replace("$$username$$", StudentId);
                                        myString = myString.Replace("$$password$$", S_pwd.ToString());
                                        var getStudentEmail = getStudentRow.Email;
                                        var Semail = "";
                                        if (getStudentEmail != null && getStudentEmail != "")
                                        {
                                            Semail = getStudentRow.Email;
                                        }
                                        else
                                        {
                                            Semail = ConfigurationManager.AppSettings["stdEmail"];
                                        }
                                        string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                                        string PlaceName = "StudentEntrollment-Add_AssignClass-Login details sent";
                                        string UserName = "Admin";
                                        string UserId = Convert.ToString(Session["empRegId"]);
                                        Mail.SendMail("Admin", supportEmail, Semail, "Account Credentials", myString.ToString(), true, PlaceName, UserName, UserId);
                                    }
                                    // to update  PrimaryUserName, salt ,password
                                    AdmissionTransactionServices admissionTrans = new AdmissionTransactionServices();
                                    var GetPrimayUserRegisterId = admissionTrans.getRow(stuRegId);
                                    if (GetPrimayUserRegisterId != null)
                                    {
                                        PrimayUserRegisterId = GetPrimayUserRegisterId.PrimaryUserRegisterId;
                                    }
                                    PrimaryUserRegisterServices primaryUser = new PrimaryUserRegisterServices();
                                    var primaryUserDetail = primaryUser.getPrimaryUserDetail(PrimayUserRegisterId);
                                    if (primaryUserDetail != null)
                                    {
                                        var getPrimaryUserId = primaryUserDetail.PrimaryUserId;
                                        var getPrimaryUSerPwd = primaryUserDetail.PrimaryUserPwd;
                                        if (getPrimaryUserId != null && getPrimaryUSerPwd != null)
                                        {
                                            // Email username password
                                            StreamReader reader = new StreamReader(Server.MapPath("~/Views/StudentEnrollment/ParentEmailPage.html"));
                                            string readFile = reader.ReadToEnd();
                                            string myString = "";
                                            myString = readFile;
                                            string Url = ConfigurationManager.AppSettings["Url"];
                                            var link = Url;
                                            myString = myString.Replace("$$Link$$", link.ToString());
                                            string p_userName = primaryUserDetail.PrimaryUserName;
                                            myString = myString.Replace("$$FirstName$$", p_userName);
                                            //myString = myString.Replace("$$admissionId$$", admissionId.ToString());
                                            //myString = myString.Replace("$$year$$", getYear);
                                            myString = myString.Replace("$$StudentName$$", studentFullName);
                                            myString = myString.Replace("$$class$$", getClass);
                                            //myString = myString.Replace("$$section$$", getSection);
                                            myString = myString.Replace("$$username$$", getPrimaryUserId);
                                            var PwdBeforeEncrypt = primaryUserDetail.PrimaryUserPwdBeforeEncrypt;
                                            myString = myString.Replace("$$password$$", PwdBeforeEncrypt.ToString());
                                            myString = myString.Replace("$$SchoolName$$", SchoolName.ToString());
                                            //myString = myString.Replace("$$Studentusername$$", studentcurrentId);
                                            //myString = myString.Replace("$$Studentpassword$$", studentcurrentPwd.ToString());
                                            var Email = primaryUserDetail.PrimaryUserEmail;
                                            string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                                            string fromDisplayName = ConfigurationManager.AppSettings["EmailFromDisplayName"];

                                            string PlaceName = "StudentEntrollment-Assign Class-Login Details";
                                            string UserName = "Employee";
                                            string UserId = Convert.ToString(Session["username"]);
                                            Mail.SendMail(fromDisplayName, supportEmail, Email, "Account Credentials", myString.ToString(), true, PlaceName, UserName, UserId);
                                        }
                                        else
                                        {
                                            string ParentId = SchoolCodeFormat + ParentIdFormat + PrimayUserRegisterId; //Parent Id Generated place
                                            string p_userName = primaryUserDetail.PrimaryUserName;
                                            string p_Salt = p_userName.Substring(0, 3);
                                            Random P_random = new Random();
                                            string P_randomNumber = P_random.Next(00000, 99999).ToString();
                                            var P_pwd = p_Salt + P_randomNumber;
                                            var p_PasswordSalt = p_Salt + P_pwd;
                                            string Password = FormsAuthentication.HashPasswordForStoringInConfigFile(p_PasswordSalt, "SHA1");
                                            var PwdBeforeEncrypt = P_pwd;
                                            primaryUser.updateParentPassword(PrimayUserRegisterId, ParentId, p_Salt, Password, PwdBeforeEncrypt);
                                            // Email username password
                                            StreamReader reader = new StreamReader(Server.MapPath("~/Views/StudentEnrollment/ParentEmailPage.html"));
                                            string readFile = reader.ReadToEnd();
                                            string myString = "";
                                            myString = readFile;
                                            string Url = ConfigurationManager.AppSettings["Url"];
                                            var link = Url;
                                            myString = myString.Replace("$$Link$$", link.ToString());
                                            myString = myString.Replace("$$StudentName$$", studentFullName);
                                            myString = myString.Replace("$$FirstName$$", p_userName);
                                            //      myString = myString.Replace("$$admissionId$$", admissionId.ToString());
                                            myString = myString.Replace("$$year$$", getYear);
                                            myString = myString.Replace("$$class$$", getClass);
                                            // myString = myString.Replace("$$section$$", getSection);
                                            myString = myString.Replace("$$username$$", ParentId);
                                            myString = myString.Replace("$$password$$", P_pwd.ToString());
                                            myString = myString.Replace("$$SchoolName$$", SchoolName.ToString());
                                            //myString = myString.Replace("$$Studentusername$$", studentcurrentId);
                                            //myString = myString.Replace("$$Studentpassword$$", studentcurrentPwd.ToString());
                                            var Email = primaryUserDetail.PrimaryUserEmail;
                                            string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                                            string fromDisplayName = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                                            string PlaceName = "StudentEntrollment-Assign Class-Login Details";
                                            string UserName = "Admin";
                                            string UserId = Convert.ToString(Session["empRegId"]);
                                            Mail.SendMail(fromDisplayName, supportEmail, Email, "Account Credentials", myString.ToString(), true, PlaceName, UserName, UserId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Class_Assigned_to_student "));
                    string Message = eAcademy.Models.ResourceCache.Localize("Class_Assigned_Successfully ");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetSelectedClassSection1(int passYearId, int passClassId)
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            ClassServices clas = new ClassServices();
            var getYear = AcYear.AcademicYear(passYearId).AcademicYear1;
            var getClass = clas.getClassNameByClassID(passClassId).ClassType;
            var List = section.getSection(passClassId);
            return Json(new { List, getYear, getClass }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSelectedClassSectionForChangeClass(int passYearId, int passClassId, int passSectionId)
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            ClassServices clas = new ClassServices();
            var getYear = AcYear.AcademicYear(passYearId).AcademicYear1;
            var getClass = clas.getClassNameByClassID(passClassId).ClassType;
            var List = section.getSectionWithoutSelectedSection(passClassId, passSectionId);
            return Json(new { List, getYear, getClass }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetClassRoomStrength(int passYearId, int passClassId, int passSectionId)
        {
            SectionStrengthServices sectionStrength = new SectionStrengthServices();
            var classRoomStrength = sectionStrength.TotalStrength(passYearId, passClassId, passSectionId);
            AssignClassToStudentServices assignStu = new AssignClassToStudentServices();
            var classAssignedStudentStrength = assignStu.GetClassStudentBeforeAssigningRollNumber(passYearId, passClassId, passSectionId);
            if (classRoomStrength != null && classAssignedStudentStrength != null)
            {
                var totalStrength = classRoomStrength.Strength;
                var AssignedStudentStrength = classAssignedStudentStrength.Count;
                return Json(new { totalStrength, AssignedStudentStrength }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Student_strength_is_not_defined ");
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        // change section
        public ActionResult ChangeSection()
        {
            return View();
        }
        public JsonResult getStudentListToChangeSection(int passYearId, int passClassId, int passSectionId)
        {
            AssignClassToStudentServices assignStu = new AssignClassToStudentServices();
            var List = assignStu.getStudentListToChangeSection(passYearId, passClassId, passSectionId);
            var count = List.ToList().Count;
            return Json(new { List, count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Edit_ChangeSection(AssignClass m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var TotalCount = m.TotalCount;
                    int year = m.ToAcademicYears;
                    int classId = m.ToClass;
                    int secId = m.ToSection;
                    int size = TotalCount + 1;
                    string[] StudentRegisterId = new string[size];
                    string[] checkboxId = new string[size];
                    AssignClassToStudentServices assignStu = new AssignClassToStudentServices();
                    FeeCollectionServices feeCollect = new FeeCollectionServices();
                    FeeCollectionCategoryServices feeCollectionCategory = new FeeCollectionCategoryServices();
                    int selectCount = 0;
                    for (int i = 0; i <= TotalCount; i++)
                    {
                        var check1 = m.checkboxId[i];
                        if (check1 == "on")
                        {
                            selectCount++;
                        }
                    }
                    SectionStrengthServices sectionStrength = new SectionStrengthServices();
                    var classRoomStrength = sectionStrength.TotalStrength(year, classId, secId);
                    var classAssignedStudentStrength = assignStu.GetClassStudent(year, classId, secId);
                    if (classRoomStrength != null && classAssignedStudentStrength != null)
                    {
                        var totalStrength = classRoomStrength.Strength;
                        var AssignedStudentStrength = classAssignedStudentStrength.Count;
                        var vacantStrength = totalStrength - AssignedStudentStrength;
                        int Vacancy = Convert.ToInt32(vacantStrength);
                        if (Vacancy < selectCount)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Selected_student_count_exceeds_from_vacant_of_selected_section_vacancy");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    for (int i = 0; i <= TotalCount; i++)
                    {
                        var check1 = m.checkboxId[i];
                        if (check1 == "on")
                        {
                            StudentRegisterId[i] = QSCrypt.Decrypt(m.StudentRegisterId[i]);
                            int studentRegisterId = Convert.ToInt32(StudentRegisterId[i]);
                            assignStu.EditChangeSectionToStudent(studentRegisterId, year, classId, secId, AdminId);
                            var CheckPayment = feeCollect.checkDataExist_ChangeSection(studentRegisterId, year, classId);
                            if (CheckPayment != null)
                            {
                                feeCollect.updateSectionWithRegId_ChangeSection(year, classId, studentRegisterId, secId);
                                int FeeCollectionId = CheckPayment.FeeCollectionId;
                                var checkFeeCategory = feeCollectionCategory.checkDataExist(FeeCollectionId, year, classId);
                                if (checkFeeCategory != null)
                                {
                                    feeCollectionCategory.updateSectionWithRegId(FeeCollectionId, year, classId, studentRegisterId, secId);
                                }
                            }
                            StudentRollNumberServices rollnum = new StudentRollNumberServices();
                            var deactivateRollnumber = rollnum.checkRollNumberExist(year, classId, studentRegisterId);
                            if (deactivateRollnumber != null)
                            {
                                rollnum.updateRollNumberStatus(year, classId, studentRegisterId);
                            }
                            // get academicYear, class, section
                            AcademicyearServices AcYear = new AcademicyearServices();
                            ClassServices clas = new ClassServices();
                            SectionServices sec = new SectionServices();
                            var getYear = AcYear.AcademicYear(year).AcademicYear1;
                            var getClass = clas.getClassNameByClassID(classId).ClassType;
                            var getSection = sec.getSectionName(classId, secId).SectionName;
                            var getStudentDetail = stu.getStudentRow(studentRegisterId);
                            if (getStudentDetail != null)
                            {
                                string StudentFirstName = getStudentDetail.FirstName;
                                int admissionId = getStudentDetail.AdmissionId.Value;
                                // Email class details
                                StreamReader reader = new StreamReader(Server.MapPath("~/Views/StudentEnrollment/ChangeSection_StudentEmailPage.html"));
                                string readFile = reader.ReadToEnd();
                                string myString = "";
                                myString = readFile;
                                myString = myString.Replace("$$FirstName$$", StudentFirstName.ToString());
                                myString = myString.Replace("$$admissionId$$", admissionId.ToString());
                                myString = myString.Replace("$$year$$", getYear);
                                myString = myString.Replace("$$class$$", getClass);
                                myString = myString.Replace("$$section$$", getSection);
                                var getStudentEmail = getStudentDetail.Email;
                                var Semail = "";
                                if (getStudentEmail != null)
                                {
                                    Semail = getStudentEmail;
                                }
                                else
                                {
                                    Semail = ConfigurationManager.AppSettings["stdEmail"];
                                }
                                string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                                string PlaceName = "StudentEntrollment-Edit_ChangeSection-Section details sent";
                                string UserName = "Employee";
                                string UserId = Convert.ToString(Session["username"]);
                                Mail.SendMail("Admin", supportEmail, Semail, "Account Credentials", myString.ToString(), true, PlaceName, UserName, UserId);
                            }
                            AdmissionTransactionServices admissionTrans = new AdmissionTransactionServices();
                            var GetPrimayUserRegisterId = admissionTrans.getRow(studentRegisterId);
                            if (GetPrimayUserRegisterId != null)
                            {
                                PrimayUserRegisterId = GetPrimayUserRegisterId.PrimaryUserRegisterId;
                            }
                            PrimaryUserRegisterServices primaryUser = new PrimaryUserRegisterServices();
                            var primaryUserDetail = primaryUser.getPrimaryUserDetail(PrimayUserRegisterId);
                            if (primaryUserDetail != null)
                            {
                                string FirstName = primaryUserDetail.PrimaryUserName;
                                StreamReader reader = new StreamReader(Server.MapPath("~/Views/StudentEnrollment/ChangeSection_StudentEmailPage.html"));
                                string readFile = reader.ReadToEnd();
                                string myString = "";
                                myString = readFile;
                                myString = myString.Replace("$$FirstName$$", FirstName);
                                //  myString = myString.Replace("$$admissionId$$", getParent.StudentAdmissionId.ToString());
                                myString = myString.Replace("$$year$$", getYear);
                                myString = myString.Replace("$$class$$", getClass);
                                myString = myString.Replace("$$section$$", getSection);
                                var Email = primaryUserDetail.PrimaryUserEmail;
                                string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                                string fromDisplayName = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                                string PlaceName = "StudentEntrollment-Change Section-Section Details";
                                string UserName = "Employee";
                                string UserId = Convert.ToString(Session["username"]);
                                Mail.SendMail(fromDisplayName, supportEmail, Email, "Account Credentials", myString.ToString(), true, PlaceName, UserName, UserId);
                            }
                        }
                    }
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Change_secton_to_student "));
                    string Message = eAcademy.Models.ResourceCache.Localize("Section_updated_Successfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }


        // student separation
        public ActionResult SeparateStudents()
        {
            return View();
        }
        public JsonResult getStudentListToGenerate_CC(int passYearId, int passClassId, int passSectionId)
        {
            AssignClassToStudentServices assignStu = new AssignClassToStudentServices();
            var studentAvailableCountWithoutAssigningRollnumber = stu.getStudentOrderByStudentName(passYearId, passClassId, passSectionId).ToList().Count;
            var List = assignStu.getStudentListToGenerate_CC(passYearId, passClassId, passSectionId);
            var count = List.ToList().Count;
            return Json(new { studentAvailableCountWithoutAssigningRollnumber, List, count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetConductStatusList()
        {
            tblConductStatusServices conduct = new tblConductStatusServices();
            var list = conduct.getList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTransferStatusList()
        {
            tblTransferStatusServices transfer = new tblTransferStatusServices();
            var list = transfer.getList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Generate_CC(AssignClass m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var TotalCount = m.TotalCount;
                    int year = m.ToAcademicYears;
                    int classId = m.ToClass;
                    int secId = m.ToSection;
                    int size = TotalCount + 1;
                    string[] StudentRegisterId = new string[size];
                    string[] checkboxId = new string[size];
                    string[] conduct = new string[size];
                    for (int i = 0; i <= TotalCount; i++)
                    {
                        var check1 = m.checkboxId[i];
                        if (check1 == "on")
                        {
                            conduct[i] = m.Conduct[i];
                            var conductt = conduct[i];
                            if (conductt == "0" || conductt == "" || conductt == null)
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_select_conduct_to_all_the_selected_student");
                                return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    AssignClassToStudentServices assignClass = new AssignClassToStudentServices();
                    for (int i = 0; i <= TotalCount; i++)
                    {
                        var check1 = m.checkboxId[i];
                        if (check1 == "on")
                        {
                            StudentRegisterId[i] = QSCrypt.Decrypt(m.StudentRegisterId[i]);
                            int studentRegisterId = Convert.ToInt32(StudentRegisterId[i]);

                            var getConductStatus = assignClass.StudentCurrentlyStudyingSelectedClass(year, classId, secId, studentRegisterId);
                            if (getConductStatus == null)
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("One_of_the_selected_student_currently_not_studying_in_the_selected_class");
                                return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    for (int i = 0; i <= TotalCount; i++)
                    {
                        var check1 = m.checkboxId[i];
                        if (check1 == "on")
                        {
                            StudentRegisterId[i] = QSCrypt.Decrypt(m.StudentRegisterId[i]);
                            int studentRegisterId = Convert.ToInt32(StudentRegisterId[i]);
                            conduct[i] = m.Conduct[i];

                            var checkConductStatusExist = assignClass.checkConductStatusExist(studentRegisterId).ConductStatus;
                            string fileName = "CC_" + studentRegisterId + ".pdf";
                            if (checkConductStatusExist == null)
                            {
                                assignClass.UpdateConductStatus(studentRegisterId, conduct[i], fileName);
                            }
                            SchoolSettingsServices sett = new SchoolSettingsServices();
                            var getSchoolName = sett.SchoolSettings();
                            string schoolName1 = getSchoolName.SchoolName;
                            string SchoolAddress = getSchoolName.AddressLine1;
                            string SchoolConduct = getSchoolName.Phone1;
                            var result1 = assignClass.generateCC(studentRegisterId);
                            var result2 = assignClass.getJoinedClass(year, classId, secId, studentRegisterId);
                            var result3 = assignClass.getCurrentClass(studentRegisterId);
                            var x = (from a in result1
                                     from b in result2
                                     from c in result3
                                     where a.StudentRegId == b.StudentRegId &&
                                     a.StudentRegId == c.StudentRegId
                                     select new CC_Pdf
                                     {
                                         AdmissionId = a.AdmissionId,
                                         FirstName = a.FirstName,
                                         LastName = a.LastName,
                                         PrimaryUserFirstName = a.PrimaryUserFirstName,
                                         FromYear = a.FromYear,
                                         FromClass = a.FromClass,
                                         toYear = c.toYear,
                                         toClass = c.toClass,
                                         ConductStatus = b.ConductStatus,
                                         Gender = a.Gender
                                     }).ToList().FirstOrDefault();
                            int? aid = x.AdmissionId;
                            string path = Server.MapPath("~/Documents/Generated_CC/");
                            string imagepath = Server.MapPath("~/img/logo.png");


                            string ToYear = ""; string ToClass = ""; int previousClassId = 0;
                            var res = assignClass.getResult(year, classId, secId, studentRegisterId);
                            ExamAttendanceServices examAtt = new ExamAttendanceServices();
                            AcademicyearServices acYear = new AcademicyearServices();
                            if (res == "")
                            {
                                var AttentedMidTerm = examAtt.CheckAttentedExam(year, classId, secId, studentRegisterId);
                                if (AttentedMidTerm == null)
                                {
                                    if (classId > 1)
                                    {
                                        string PrvToClass = clas.getPreviousClass(classId).ClassType;
                                        int PrvClassId = clas.getPreviousClass(classId).ClassId;
                                        var PrvToYearVal = acYear.getPreviousAcYear(classId, studentRegisterId);
                                        string PrvToYear = "";
                                        if(PrvToYearVal != null)
                                        {
                                            PrvToYear = acYear.getPreviousAcYear(classId, studentRegisterId).AcademicYear1;
                                        }
                                        else
                                        {
                                            ToClass = x.toClass;
                                            ToYear = x.toYear;
                                        }

                                        string PreviousYearResult = assignClass.getPreviousYearResult(PrvClassId, studentRegisterId);
                                        if (PreviousYearResult == "studentNotExist")
                                        {
                                            ToClass = x.toClass;
                                            ToYear = x.toYear;
                                        }
                                        else
                                        {
                                            ToClass = PrvToClass;
                                            ToYear = PrvToYear;
                                        }
                                    }
                                    else
                                    {
                                        ToClass = x.toClass;
                                        ToYear = x.toYear;
                                    }
                                }
                                else
                                {
                                    ToClass = x.toClass;
                                    ToYear = x.toYear;
                                }
                            }
                            else
                            {
                                ToClass = x.toClass;
                                ToYear = x.toYear;
                            }

                            string ToClass1 = ToClass; string ToYear1 = ToYear;

                            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(imagepath);
                            logo.ScaleAbsolute(100, 50);
                            iTextSharp.text.Font Font = FontFactory.GetFont("Verdana", 20, iTextSharp.text.BaseColor.BLUE);
                            using (FileStream fs = new FileStream(path + fileName, FileMode.Create, FileAccess.Write, FileShare.None))
                            using (Document doc = new Document(PageSize.A4, 80, 50, 30, 65))
                            using (PdfWriter writer = PdfWriter.GetInstance(doc, fs))
                            {
                                doc.Open();
                                string Mr = ""; string so = ""; string his = ""; string His = "";
                                if (x.Gender == "Male" || x.Gender == "male")
                                {
                                    Mr = "Mr.";
                                    so = "S/O.";
                                    His = "His";
                                    his = "his";
                                }
                                else
                                {
                                    Mr = "Mrs.";
                                    so = "D/O.";
                                    His = "Her";
                                    his = "her";
                                }
                                var schoolName = "<div style=text-align:center><font color=#0000FF size=5><b>" + schoolName1 + "</b></font></div><br>";
                                var Address = "<div style=text-align:center><font color=#0000FF size=4>" + SchoolAddress + SchoolConduct + "</font></div><br>";
                                var certificateName = "<div style=text-align:center><font color=green size=5> <u>Conduct Certificate</u></font></div><br><br><br>";
                                var id = "<font size=5 > <u>Admission No:" + aid + "</u></font><br><br>";
                                var para1 = "<div><p style=line-height:30 text-indent: 50px><font size=4 > This is to certify that " + Mr + " <u>" + x.FirstName + " " + x.LastName + "</u> " + so + " <u>" + x.PrimaryUserFirstName + "</u> studied in this institution from class <u>  " + x.FromClass + "  </u> to <u>  " + ToClass + "  </u> during the academic year from <u> " + x.FromYear + "</u> to <u>" + ToYear + ".</u></font></p></div></br></br>";
                                var line = "<br><br>";
                                var para2 = "<div><p style=line-height:30 text-indent: 500000px><font size=4 > " + His + " character and conduct during the above period of " + his + " study in this institution was found <u>" + x.ConductStatus + ".</u></font></p></div>";
                                var station = "<div style=width:100%><b>Place :</b> Chennai</div>";
                                var date = "<div>  <p style=text-align:left><b>Date :  </b>" + DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy") + "</p></div><br><br>";
                                var headSign = "<div> <p style=text-align:right><b>Headmaster</b></p></div>";
                                logo.Border = iTextSharp.text.Rectangle.BOX;
                                //   logo.BorderColor = iTextSharp.text.BaseColor.BLACK;
                                //  logo.BorderWidth = 3f;
                                //doc.Add(logo);
                                //     doc.Add(new Paragraph(schoolName));
                                iTextSharp.text.html.simpleparser.HTMLWorker hw = new iTextSharp.text.html.simpleparser.HTMLWorker(doc);
                                hw.Parse(new StringReader(schoolName));
                                hw.Parse(new StringReader(Address));
                                hw.Parse(new StringReader(certificateName));
                                hw.Parse(new StringReader(id));
                                hw.Parse(new StringReader(para1));
                                hw.Parse(new StringReader(line));
                                hw.Parse(new StringReader(para2));
                                hw.Parse(new StringReader(line));
                                hw.Parse(new StringReader(station));
                                hw.Parse(new StringReader(date));
                                hw.Parse(new StringReader(headSign));
                                doc.Close();
                            }
                        }
                    }
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Generate_conduct_certificate "));
                    string Message = eAcademy.Models.ResourceCache.Localize("Conduct_certificate_generated_successfully");
                    return Json(new { Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Generate_TC(AssignClass m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var TotalCount = m.TotalCount;
                    int year = m.ToAcademicYears;
                    int classId = m.ToClass;
                    int secId = m.ToSection;
                    int size = TotalCount + 1;
                    string[] StudentRegisterId = new string[size];
                    string[] checkboxId = new string[size];
                    string[] transfer = new string[size];
                    for (int i = 0; i <= TotalCount; i++)
                    {
                        var check1 = m.checkboxId[i];
                        if (check1 == "on")
                        {
                            transfer[i] = m.TransferReason[i];
                            var transferr = transfer[i];
                            if (transferr == "0" || transferr == "" || transferr == null)
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_select_transfer_reason_to_all_the_selected_student ");
                                return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    AssignClassToStudentServices assignClass = new AssignClassToStudentServices();
                    SchoolSettingsServices sett = new SchoolSettingsServices();
                    ExamAttendanceServices examAtt = new ExamAttendanceServices();
                    AcademicyearServices acYear = new AcademicyearServices();
                    for (int i = 0; i <= TotalCount; i++)
                    {
                        var check1 = m.checkboxId[i];
                        if (check1 == "on")
                        {
                            StudentRegisterId[i] = QSCrypt.Decrypt(m.StudentRegisterId[i]);
                            int studentRegisterId = Convert.ToInt32(StudentRegisterId[i]);

                            var getConductStatus = assignClass.checkConductStatusExist(studentRegisterId);
                            if (getConductStatus != null)
                            {
                                string conductStatus = getConductStatus.ConductStatus;
                                if (conductStatus == null)
                                {
                                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_generate_conduct_certificate_before_generating_Transfer_Certificate");
                                    return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                            }
                        }
                    }

                    for (int i = 0; i <= TotalCount; i++)
                    {
                        var check1 = m.checkboxId[i];
                        if (check1 == "on")
                        {
                            StudentRegisterId[i] = QSCrypt.Decrypt(m.StudentRegisterId[i]);
                            int studentRegisterId = Convert.ToInt32(StudentRegisterId[i]);

                            var getConductStatus = assignClass.StudentCurrentlyStudyingSelectedClass(year, classId, secId, studentRegisterId);
                            if (getConductStatus == null)
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("One_of_the_selected_student_currently_not_studying_in_the_selected_class");
                                return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }

                    for (int i = 0; i <= TotalCount; i++)
                    {
                        var check1 = m.checkboxId[i];
                        if (check1 == "on")
                        {
                            StudentRegisterId[i] = QSCrypt.Decrypt(m.StudentRegisterId[i]);
                            int studentRegisterId = Convert.ToInt32(StudentRegisterId[i]);

                            var getConductStatus = assignClass.checkConductStatusExist(studentRegisterId);
                            string fileName = "TC_" + studentRegisterId + ".pdf";
                            if (getConductStatus != null)
                            {
                                string conductStatus = getConductStatus.ConductStatus;
                                if (conductStatus == null)
                                {
                                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_generate_conduct_certificate_before_generating_Transfer_Certificate");
                                    return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    assignClass.UpdateTransferStatus(studentRegisterId, transfer[i], fileName, year, classId, secId);
                                }
                            }
                            transfer[i] = m.TransferReason[i];
                            var getSchoolName = sett.SchoolSettings();
                            string schoolName1 = getSchoolName.SchoolName;
                            string SchoolAddress = getSchoolName.AddressLine1;
                            string SchoolConduct = getSchoolName.Phone1;
                            var result1 = assignClass.generateTC(studentRegisterId);
                            var result2 = assignClass.getJoinedClass(year, classId, secId, studentRegisterId);
                            var result3 = assignClass.getCurrentClass(studentRegisterId);
                            var x = (from a in result1
                                     from b in result2
                                     from c in result3
                                     where a.StudentRegId == b.StudentRegId &&
                                     a.StudentRegId == c.StudentRegId
                                     select new TC_Pdf
                                     {
                                         AdmissionId = a.AdmissionId,
                                         FirstName = a.FirstName,
                                         LastName = a.LastName,
                                         PrimaryUserFirstName = a.PrimaryUserFirstName,
                                         FromYear = a.FromYear,
                                         FromClass = a.FromClass,
                                         toYear = c.toYear,
                                         toClass = c.toClass,
                                         ConductStatus = b.ConductStatus,
                                         Nationality = a.Nationality,
                                         Religion = a.Religion,
                                         caste = a.caste,
                                         DOB = a.DOB,
                                         DateOfAdmission = a.DateOfAdmission,
                                         MediumOfStudy = a.MediumOfStudy,
                                     }).ToList().FirstOrDefault();
                            int? aid = x.AdmissionId;
                            string path = Server.MapPath("~/Documents/Generated_TC/");
                            string imagepath = Server.MapPath("~/img/logo.png");

                            string Result = ""; string PreviousAcYear = ""; string previousClass = ""; int previousClassId = 0; string leavingSchoolBeforeExam = "";  //int PreviousAcYearId=0; 
                            var res = assignClass.getResult(year, classId, secId, studentRegisterId);
                            if (res == "")
                            {
                                var AttentedMidTerm = examAtt.CheckAttentedExam(year, classId, secId, studentRegisterId);
                                if (AttentedMidTerm == null)
                                {
                                    // Result = "NotAttendAnyExam";
                                    //PreviousAcYear = acYear.getPreviousAcYear(year).AcademicYear1;
                                    //PreviousAcYearId = acYear.getPreviousAcYear(year).AcademicYearId;
                                    if (classId > 1)
                                    {
                                        previousClass = clas.getPreviousClass(classId).ClassType;
                                        previousClassId = clas.getPreviousClass(classId).ClassId;

                                        var PreviousAcYearRow = acYear.getPreviousAcYear(classId, studentRegisterId);
                                        if (PreviousAcYearRow != null)
                                        {
                                            PreviousAcYear = acYear.getPreviousAcYear(classId, studentRegisterId).AcademicYear1;
                                        }
                                    }

                                    string PreviousYearResult = assignClass.getPreviousYearResult(previousClassId, studentRegisterId);
                                    if (PreviousYearResult == "studentNotExist")
                                    {
                                        var JoinedClassDetail = stu.getJoinedClassDetail(studentRegisterId);
                                        PreviousAcYear = JoinedClassDetail.PreviousAcYear;
                                        previousClass = JoinedClassDetail.PreviousAcClass;
                                        Result = "Incomplete";
                                    }
                                    else
                                    {
                                        leavingSchoolBeforeExam = "Yes";
                                        Result = PreviousYearResult;
                                    }
                                }
                                else
                                {
                                    Result = "Incomplete";
                                }
                            }
                            else
                            {
                                Result = res;
                            }
                            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(imagepath);
                            logo.ScaleAbsolute(100, 50);
                            iTextSharp.text.Font Font = FontFactory.GetFont("Verdana", 20, iTextSharp.text.BaseColor.BLUE);
                            using (FileStream fs = new FileStream(path + fileName, FileMode.Create, FileAccess.Write, FileShare.None))
                            using (Document doc = new Document(PageSize.A4, 80, 50, 30, 65))
                            using (PdfWriter writer = PdfWriter.GetInstance(doc, fs))
                            {
                                doc.Open();
                                var schoolName = "<div style=text-align:center><font color=#0000FF size=5><b>" + schoolName1 + "</b></font></div><br>";
                                var Address = "<div style=text-align:center><font color=#0000FF size=4>" + SchoolAddress + SchoolConduct + "</font></div><br>";
                                var certificateName = "<div style=text-align:center><font color=green size=5> <u>Transfer Certificate</u></font></div><br><br><br>";
                                var line = "<br><br>";
                                List list = new List(List.ORDERED);
                                list.IndentationLeft = 20f;
                                list.SetListSymbol("\u2022");
                                list.Add(new ListItem("Student Name                                 : " + x.FirstName + " " + x.LastName));
                                list.Add("Admission Id / Registration Id         : " + x.AdmissionId + " / " + studentRegisterId);
                                list.Add("Name of Parent / Guardian             : " + x.PrimaryUserFirstName);
                                list.Add("Nationality / Religion / Community  : " + x.Nationality + " / " + x.Religion + " / " + x.caste);
                                list.Add("Date of Birth                                    : " + x.DOB.Value.ToString("dd/MM/yyyy"));
                                list.Add("Date Of Admission                          : " + x.DateOfAdmission.Value.ToString("dd/MM/yyyy"));
                                //  list.Add("Medium of Study                             : " + x.MediumOfStudy);
                                list.Add("Joined class / Academic Year         : " + x.FromClass + " / " + x.FromYear);

                                if (leavingSchoolBeforeExam == "Yes")
                                {
                                    list.Add("Leaving class / Academic Year       : " + previousClass + " / " + PreviousAcYear);
                                }
                                else
                                {
                                    list.Add("Leaving class / Academic Year       : " + x.toClass + " / " + x.toYear);
                                }
                                list.Add("Status                                              : " + Result);
                                list.Add("Conduct                                           : " + x.ConductStatus);
                                var station = "<div><b>Station :</b> Chennai</div><br>";
                                // var date = "<div>  <p style=text-align:left><b>Date :  </b>" + DateTime.Today.ToString("dd/MM/yyyy") + "</p>  <p style=text-align:right><b>Headmaster</b></p></div>";
                                var date = "<div>  <p style=text-align:left><b>Date :  </b>" + DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy") + "</p></div><br><br>";
                                var headSign = "<div> <p style=text-align:right><b>Headmaster</b></p></div>";
                                // logo.Border = iTextSharp.text.Rectangle.BOX;
                                // doc.Add(logo);
                                iTextSharp.text.html.simpleparser.HTMLWorker hw = new iTextSharp.text.html.simpleparser.HTMLWorker(doc);
                                hw.Parse(new StringReader(schoolName));
                                hw.Parse(new StringReader(Address));
                                hw.Parse(new StringReader(certificateName));
                                doc.Add(list);
                                hw.Parse(new StringReader(line));
                                hw.Parse(new StringReader(line));
                                hw.Parse(new StringReader(station));
                                hw.Parse(new StringReader(date));
                                hw.Parse(new StringReader(headSign));
                                doc.Close();
                            }
                        }
                    }
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Generate_transfer_certificate "));
                    string Message = eAcademy.Models.ResourceCache.Localize("Transfer_certificate_generated_successfully");
                    return Json(new { Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Separate_Student(AssignClass m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var TotalCount = m.TotalCount;
                    int year = m.ToAcademicYears;
                    int classId = m.ToClass;
                    int secId = m.ToSection;
                    int size = TotalCount + 1;
                    string[] StudentRegisterId = new string[size];
                    string[] checkboxId = new string[size];
                    AssignClassToStudentServices assignClass = new AssignClassToStudentServices();
                    for (int i = 0; i <= TotalCount; i++)
                    {
                        var check1 = m.checkboxId[i];
                        if (check1 == "on")
                        {
                            StudentRegisterId[i] = QSCrypt.Decrypt(m.StudentRegisterId[i]);
                            int studentRegisterId = Convert.ToInt32(StudentRegisterId[i]);

                            var getConductStatus = assignClass.StudentCurrentlyStudyingSelectedClass(year, classId, secId, studentRegisterId);
                            if (getConductStatus == null)
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("One_of_the_selected_student_currently_not_studying_in_the_selected_class");
                                return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    for (int i = 0; i <= TotalCount; i++)
                    {
                        var check1 = m.checkboxId[i];
                        if (check1 == "on")
                        {
                            StudentRegisterId[i] = QSCrypt.Decrypt(m.StudentRegisterId[i]);
                            int studentRegisterId = Convert.ToInt32(StudentRegisterId[i]);

                            var getConductStatus = assignClass.checkConductStatusExist(studentRegisterId);
                            if (getConductStatus != null)
                            {
                                string conductStatus = getConductStatus.ConductStatus;
                                string transferReason = getConductStatus.TransferStatus;
                                if (conductStatus == null)
                                {
                                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_generate_conduct_certificate_before_generating_Transfer_Certificate");
                                    return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                                else if (transferReason == null)
                                {
                                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_generate_Transfer_certificate_before_separating_all_the_selected_student ");
                                    return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            stu.updateSeparateStudentStatus(studentRegisterId, AdminId, year, classId, secId);
                        }
                    }
                    string Message = eAcademy.Models.ResourceCache.Localize("All_selected_students_separated_successfully");
                    return Json(new { Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public void Open_CC(string id)
        {
            try
            {
                int studentRegisterId = Convert.ToInt32(QSCrypt.Decrypt(id));
                AssignClassToStudentServices assignClass = new AssignClassToStudentServices();
                var CC_File = assignClass.checkConductStatusExist(studentRegisterId);
                if (CC_File != null)
                {
                    string file = CC_File.ConductCertificate;
                    if (file != null)
                    {
                        string extention = System.IO.Path.GetExtension(file);
                        string FilePath = Server.MapPath("~/Documents/Generated_CC/" + file);
                        WebClient User = new WebClient();
                        Byte[] FileBuffer = User.DownloadData(FilePath);
                        if (FileBuffer != null)
                        {
                            if (extention == ".pdf")
                            {
                                Response.ContentType = "application/pdf";
                            }
                            Response.AddHeader("Content-Disposition", "attachment; filename=" + file);
                            Response.BinaryWrite(FileBuffer);
                            Response.Flush();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }
        public void Open_TC(string id)
        {
            try
            {
                int studentRegisterId = Convert.ToInt32(QSCrypt.Decrypt(id));
                AssignClassToStudentServices assignClass = new AssignClassToStudentServices();
                var TC_File = assignClass.checkConductStatusExist(studentRegisterId);
                if (TC_File != null)
                {
                    string file = TC_File.TransferCertificate;
                    if (file != null)
                    {
                        string extention = System.IO.Path.GetExtension(file);
                        string FilePath = Server.MapPath("~/Documents/Generated_TC/" + file);
                        WebClient User = new WebClient();
                        Byte[] FileBuffer = User.DownloadData(FilePath);
                        if (FileBuffer != null)
                        {
                            if (extention == ".pdf")
                            {
                                Response.ContentType = "application/pdf";
                            }
                            Response.AddHeader("Content-Disposition", "attachment; filename=" + file);
                            Response.BinaryWrite(FileBuffer);
                            Response.Flush();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }

        //Edit Student & Parent details
        public ActionResult EditStudentBioData()
        {
            try
            {
                CountriesServices country = new CountriesServices();
                BloodGroupServices bg = new BloodGroupServices();
                CommunityServices com = new CommunityServices();
                Admission1Services As1 = new Admission1Services();
                ViewBag.country = country.GetCountry();
                ViewBag.bloodgroups = bg.GetBloodGroup();
                ViewBag.communities = com.GetCommunity();
                return View();
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
                return View("error");
            }
        }
        [HttpPost]
        public JsonResult GetStudentBiodata(VM_Biodata s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PrimaryUserRegisterServices primary = new PrimaryUserRegisterServices();
                    StudentServices S_Student = new StudentServices();
                    tblSiblingServices s_sibling = new tblSiblingServices();
                    string MatchEmailPattern =
                @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
         + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
         + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
         + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
                    string uname = "";
                    int Sid = 0;
                    if (Regex.IsMatch(s.UserName, MatchEmailPattern))
                    {
                        uname = s.UserName;
                    }
                    else
                    {
                        uname = primary.GetUsername(s.UserName);
                    }
                    Regex regex = new Regex("^[0-9]*$");
                    if (regex.IsMatch(s.StudentId))
                    {
                        var ss = S_Student.GetStudentRegisterIdbyapplicationNumber(Convert.ToInt16(s.StudentId));
                        if (ss != null)
                        {
                            Sid = Convert.ToInt16(ss.StudentRegisterId);
                        }
                        else
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Enter_correct_student_id");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        var ss = S_Student.GetStudentRegisterIdbyStudentid(s.StudentId);
                        if (ss != null)
                        {
                            Sid = ss.StudentRegisterId;
                        }
                        else
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Enter_correct_student_id");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (uname != null && Sid != 0)
                    {
                        var CheckStudentDetails = primary.CheckStudentIdandUserEmail(Sid, uname);
                        if (CheckStudentDetails == true)
                        {
                            var list = S_Student.GetStudentalldetails(Sid, uname);
                            var sibling = s_sibling.GetJsonSibling(Sid);
                            return Json(new { list, sibling }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Enter_correct_student_id_and_user_name");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Enter_correct_student_id_and_user_name");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UpdateStudentParentsDetails(VM_UpdateStudentDetails s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    StudentServices s_student = new StudentServices();
                    AdmissionTransactionServices Admintrans = new AdmissionTransactionServices();
                    PrimaryUserRegisterServices primaryreg = new PrimaryUserRegisterServices();
                    FatherRegisterServices Fatherreg = new FatherRegisterServices();
                    MotherRegisterServices Motherreg = new MotherRegisterServices();
                    GuardianRegisterServices Guardianreg = new GuardianRegisterServices();
                    tblSiblingServices s_sbling = new tblSiblingServices();
                    SchoolSettingsServices sett = new SchoolSettingsServices();
                    var getStudentIdFormat = sett.SchoolSettings();
                    var SchoolCodeFormat = getStudentIdFormat.SchoolCode;
                    var ParentIdFormat = getStudentIdFormat.ParentIDFormat;
                    int stuid = Convert.ToInt16(s.Stuadmissionid);
                    int StudentRegId = stuid; int FatherRegId = 0; int MotherRegId = 0; int GuardianRegId = 0; int oldprimaryuserid = 0;
                    if (stuid != 0)
                    {
                        s_student.UpdateStudentinformation(stuid, s.StuFirstName, s.StuMiddlename, s.StuLastname, s.Gender, s.DOB, s.PlaceOfBirth, s.Community, s.Religion,
                                                           s.Nationality, s.BloodGroup, s.LocAddress1, s.LocAddress2, s.LocCity, s.LocState, s.LocCountry, s.LocPostelcode, s.Distance, s.PerAddress1,
                                                           s.PerAddress2, s.PerCity, s.PerState, s.PerCountry, s.PerPostelcode, s.GuardianRequried, s.Height, s.Weights, s.IdentificationMark, s.Email,
                                                           s.Contact, s.EmergencyContactPersonName, s.EmergencyContactNumber, s.ContactPersonRelationship, s.hostel_required, s.transport_required);
                        oldprimaryuserid = Convert.ToInt16(Admintrans.getRow(stuid).PrimaryUserRegisterId);
                    }
                    var studentdetails = s_student.getStudentRow(StudentRegId);
                    var NoOfStudents = Admintrans.GetRemainingStudent(oldprimaryuserid, StudentRegId); // get no of student in same primary user
                    var checkuser = Admintrans.GetPrimaryUser(oldprimaryuserid, StudentRegId); //get current edit student father,mother,guardian,primary user register id
                    var oldprimaryuser = primaryreg.GetPrimaryUserDetails(oldprimaryuserid);
                    int flag = 0;
                    if (s.PrimaryUser == "Father")
                    {
                        if (s.PrimaryUser == oldprimaryuser.PrimaryUser) // new primary user  is SAME as old primary user 
                        {
                            if (NoOfStudents.Count == 0)  // primary user contain ONE student
                            {
                                if (checkuser.FatherRegisterId != null)
                                {
                                    var fid = Convert.ToInt16(checkuser.FatherRegisterId);
                                    if (s.FatherFirstName != null && s.FatherLastName != null && s.FatherDOB != null && s.FatherEmail != null && s.FatherMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.FatherEmail) // new primary user email is SAME as old primary user email
                                            {
                                                primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.FatherFirstName, s.FatherLastName, s.FatherMobileNo, s.FatherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                Fatherreg.EditFatherDetails(fid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                { //email exist then get user confirmation
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize("email_exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code update primary user details and father details in table(PrimaryUserRegister,FatherRegister)
                                                {
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.FatherFirstName, s.FatherLastName, s.FatherMobileNo, s.FatherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Fatherreg.EditFatherDetails(fid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //user confirmation is yes then update exist primary user id
                                            var existuser = primaryreg.CheckPrimaryuserfather(s.FatherEmail, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                Fatherreg.ChangeFatherStatusFlag(fid, StudentRegId);
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdfatherId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.FatherRegisterId));
                                                s_sbling.ChangeSiblingfromNewusertoExistUser(oldprimaryuserid, StudentRegId, studentdetails.StudentId);
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                if (checkuser.MotherRegisterId == null)
                                {
                                    if (s.MotherFirstname != null || s.MotherLastName != null || s.MotherDOB != null || s.MotherQualification != null || s.MotherOccupation != null || s.MotherMobileNo != null || s.MotherEmail != null)
                                    {
                                        MotherRegId = Motherreg.EditAddMotherdetails(s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                        Admintrans.UpdateMothertId(oldprimaryuserid, StudentRegId, MotherRegId);
                                    }
                                }
                                else
                                {
                                    int motherid = Convert.ToInt16(checkuser.MotherRegisterId);
                                    Motherreg.EditmotherDetails1(motherid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail); //update mother details in same motherregisterid
                                }
                                if (checkuser.GuardianRegisterId == null)
                                {
                                    if (s.GuardianFirstname != null || s.GuardianLastName != null || s.GuardianDOB != null || s.GuardianQualification != null || s.GuardianOccupation != null || s.GuardianMobileNo != null || s.GuardianEmail != null || s.GuardianIncome != null || s.GuRelationshipToChild != null)
                                    {
                                        GuardianRegId = Guardianreg.EditAddGuardiandetails(s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                        Admintrans.UpdateGuardiantId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                    }
                                }
                                else
                                {
                                    int gid = Convert.ToInt16(checkuser.GuardianRegisterId);
                                    Guardianreg.EditGuardianDetails(gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender); //update Guardian details in same motherregisterid
                                }
                            }
                            else  // primary user contain MORE student
                            {
                                if (checkuser.FatherRegisterId != null)
                                {
                                    var fid = Convert.ToInt16(checkuser.FatherRegisterId);
                                    if (s.FatherFirstName != null && s.FatherLastName != null && s.FatherDOB != null && s.FatherEmail != null && s.FatherMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.FatherEmail) // new primary user email is SAME as old primary user email
                                            {
                                                primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.FatherFirstName, s.FatherLastName, s.FatherMobileNo, s.FatherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                Fatherreg.EditFatherDetails(fid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                { //email exist then get user confirmation
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize("email_exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code add primary user details and father details,update student primary userid in table(PrimaryUserRegister,FatherRegister,admissiontransactions) 
                                                {
                                                    s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                    oldprimaryuserid = primaryreg.EditAddPrimaryUser(s.PrimaryUser, s.FatherFirstName, s.primaryUserEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    SendEmail(SchoolCodeFormat, ParentIdFormat, oldprimaryuserid, s.FatherFirstName, s.primaryUserEmail);
                                                    Fatherreg.EditFatherDetails(fid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                                    Admintrans.EditUpdateFatherPrimaryUserId(oldprimaryuserid, StudentRegId, fid);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //user confirmation is yes then update exist primary user id
                                            var existuser = primaryreg.CheckPrimaryuserfather(s.FatherEmail, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdfatherId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.FatherRegisterId));
                                                s_sbling.ChangeSiblingfromNewusertoExistUser(oldprimaryuserid, StudentRegId, studentdetails.StudentId);
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email ");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                if (checkuser.MotherRegisterId == null)
                                {
                                    if (s.MotherFirstname != null || s.MotherLastName != null || s.MotherDOB != null || s.MotherQualification != null || s.MotherOccupation != null || s.MotherMobileNo != null || s.MotherEmail != null)
                                    {
                                        MotherRegId = Motherreg.CheckMotherExit(oldprimaryuserid, StudentRegId, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail, s.TotalIncome);
                                        Admintrans.UpdateMothertId(oldprimaryuserid, StudentRegId, MotherRegId);
                                    }
                                }
                                else
                                {
                                    int motherid = Convert.ToInt16(checkuser.MotherRegisterId);
                                    //check mother details exist,then update exist mother id into transaction table
                                    MotherRegId = Motherreg.CheckEditMotherdetailsExit(oldprimaryuserid, StudentRegId, motherid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail); //update mother details in same motherregisterid
                                    Admintrans.UpdateMothertId(oldprimaryuserid, StudentRegId, MotherRegId);
                                }
                                if (checkuser.GuardianRegisterId == null)
                                {
                                    if (s.GuardianFirstname != null || s.GuardianLastName != null || s.GuardianDOB != null || s.GuardianQualification != null || s.GuardianOccupation != null || s.GuardianMobileNo != null || s.GuardianEmail != null || s.GuardianIncome != null || s.GuRelationshipToChild != null)
                                    {
                                        GuardianRegId = Guardianreg.CheckGuardianExit(oldprimaryuserid, StudentRegId, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                        Admintrans.UpdateGuardiantId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                    }
                                }
                                else
                                {
                                    int gid = Convert.ToInt16(checkuser.GuardianRegisterId);
                                    GuardianRegId = Guardianreg.CheckEditGuardiandetailsExit(oldprimaryuserid, StudentRegId, gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender); //update Guardian details in same motherregisterid
                                    Admintrans.UpdateGuardiantId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                }
                            }
                        }
                        else // new primary user  is Not SAME as old primary user 
                        {
                            if (NoOfStudents.Count == 0)  // primary user contain ONE student
                            {
                                if (checkuser.FatherRegisterId != null)
                                {
                                    var fid = Convert.ToInt16(checkuser.FatherRegisterId);
                                    if (s.FatherFirstName != null && s.FatherLastName != null && s.FatherDOB != null && s.FatherEmail != null && s.FatherMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.FatherEmail) // new primary user email is SAME as old primary user email
                                            {
                                                primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.FatherFirstName, s.FatherLastName, s.FatherMobileNo, s.FatherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                Fatherreg.EditFatherDetails(fid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    string dialogbox = ("email_exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code update primary user details and father details in table(PrimaryUserRegister,FatherRegister)
                                                {
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.FatherFirstName, s.FatherLastName, s.FatherMobileNo, s.FatherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Fatherreg.EditFatherDetails(fid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //user confirmation is yes then update exist primary user id
                                            var existuser = primaryreg.CheckPrimaryuserfather(s.FatherEmail, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                Fatherreg.ChangeFatherStatusFlag(fid, StudentRegId);
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdfatherId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.FatherRegisterId));
                                                s_sbling.ChangeSiblingfromNewusertoExistUser(oldprimaryuserid, StudentRegId, studentdetails.StudentId);
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                else //father id null
                                {
                                    if (s.FatherFirstName != null && s.FatherLastName != null && s.FatherDOB != null && s.FatherEmail != null && s.FatherMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.FatherEmail) // new primary user email is SAME as old primary user email
                                            {
                                                primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.FatherFirstName, s.FatherLastName, s.FatherMobileNo, s.FatherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                FatherRegId = Fatherreg.EditAddFatherdetails(s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                                Admintrans.EditUpdateFatherPrimaryUserId(oldprimaryuserid, StudentRegId, FatherRegId);
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    string dialogbox = ("email exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code add primary user details and father details,update student primary userid in table(PrimaryUserRegister,FatherRegister,admissiontransactions) 
                                                {
                                                    FatherRegId = Fatherreg.EditAddFatherdetails(s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.FatherFirstName, s.FatherLastName, s.FatherMobileNo, s.FatherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Admintrans.EditUpdateFatherPrimaryUserId(oldprimaryuserid, StudentRegId, FatherRegId);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //user confirmation is yes then update exist primary user id
                                            var existuser = primaryreg.CheckPrimaryuserfather(s.FatherEmail, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdfatherId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.FatherRegisterId));
                                                s_sbling.ChangeSiblingfromNewusertoExistUser(oldprimaryuserid, StudentRegId, studentdetails.StudentId);
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                if (checkuser.MotherRegisterId == null)
                                {
                                    if (s.MotherFirstname != null || s.MotherLastName != null || s.MotherDOB != null || s.MotherQualification != null || s.MotherOccupation != null || s.MotherMobileNo != null || s.MotherEmail != null)
                                    {
                                        MotherRegId = Motherreg.EditAddMotherdetails(s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                        Admintrans.UpdateMothertId(oldprimaryuserid, StudentRegId, MotherRegId);
                                    }
                                }
                                else
                                {
                                    int motherid = Convert.ToInt16(checkuser.MotherRegisterId);
                                    Motherreg.EditmotherDetails1(motherid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail); //update mother details in same motherregisterid
                                }
                                if (checkuser.GuardianRegisterId == null)
                                {
                                    if (s.GuardianFirstname != null || s.GuardianLastName != null || s.GuardianDOB != null || s.GuardianQualification != null || s.GuardianOccupation != null || s.GuardianMobileNo != null || s.GuardianEmail != null || s.GuardianIncome != null || s.GuRelationshipToChild != null)
                                    {
                                        GuardianRegId = Guardianreg.EditAddGuardiandetails(s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                        Admintrans.UpdateGuardiantId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                    }
                                }
                                else
                                {
                                    int gid = Convert.ToInt16(checkuser.GuardianRegisterId);
                                    Guardianreg.EditGuardianDetails(gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender); //update Guardian details in same motherregisterid
                                }
                            }
                            else // primary user contain MORE student
                            {
                                if (checkuser.FatherRegisterId != null)
                                {
                                    var fid = Convert.ToInt16(checkuser.FatherRegisterId);
                                    if (s.FatherFirstName != null && s.FatherLastName != null && s.FatherDOB != null && s.FatherEmail != null && s.FatherMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.FatherEmail) // new primary user email is SAME as old primary user email
                                            {
                                                var RemaningStudentlist = Admintrans.GetRemainingStudent(oldprimaryuserid, StudentRegId);
                                                bool checkfathersame = Fatherreg.CheckFatherSameinRemaingstudent(oldprimaryuserid, StudentRegId, RemaningStudentlist, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                                if (checkfathersame == true)
                                                {
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.FatherFirstName, s.FatherLastName, s.FatherMobileNo, s.FatherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Fatherreg.EditFatherDetails(fid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                                }
                                                else
                                                {
                                                    string Mismatchdetails = eAcademy.Models.ResourceCache.Localize("Remaining_student_father_details_is_mismatch,so_use_another_email");
                                                    return Json(new { Mismatchdetails = Mismatchdetails }, JsonRequestBehavior.AllowGet);
                                                }
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize("email_exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code update primary user details and father details in table(PrimaryUserRegister,FatherRegister)
                                                {
                                                    s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                    oldprimaryuserid = primaryreg.EditAddPrimaryUser(s.PrimaryUser, s.FatherFirstName, s.primaryUserEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    SendEmail(SchoolCodeFormat, ParentIdFormat, oldprimaryuserid, s.FatherFirstName, s.primaryUserEmail);
                                                    Fatherreg.EditFatherDetails(fid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                                    Admintrans.EditUpdatePrimaryUserIdfatherId(oldprimaryuserid, StudentRegId, fid);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //user confirmation is yes then update exist primary user id
                                            var existuser = primaryreg.CheckPrimaryuserfather(s.FatherEmail, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdfatherId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.FatherRegisterId));
                                                s_sbling.ChangeSiblingfromNewusertoExistUser(oldprimaryuserid, StudentRegId, studentdetails.StudentId);
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                else //father id null
                                {
                                    if (s.FatherFirstName != null && s.FatherLastName != null && s.FatherDOB != null && s.FatherEmail != null && s.FatherMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.FatherEmail) // new primary user email is SAME as old primary user email
                                            {
                                                FatherRegId = Fatherreg.CheckFatherExit(oldprimaryuserid, StudentRegId, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                                Admintrans.UpdateFathertId(oldprimaryuserid, StudentRegId, FatherRegId);
                                                var RemaningStudentlist = Admintrans.GetRemainingStudent(oldprimaryuserid, StudentRegId);
                                                bool checkfathersame = Fatherreg.CheckFatherSameinRemaingstudent(oldprimaryuserid, StudentRegId, RemaningStudentlist, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                                if (checkfathersame == true)
                                                {
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.FatherFirstName, s.FatherLastName, s.FatherMobileNo, s.FatherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Fatherreg.EditFatherDetails(FatherRegId, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                                }
                                                else
                                                {
                                                    string Mismatchdetails = ("Remaining_student_father_details_is_mismatch,so_use_another_email");
                                                    return Json(new { Mismatchdetails = Mismatchdetails }, JsonRequestBehavior.AllowGet);
                                                }
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize("email_exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code update primary user details and father details in table(PrimaryUserRegister,FatherRegister)
                                                {
                                                    s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                    oldprimaryuserid = primaryreg.EditAddPrimaryUser(s.PrimaryUser, s.FatherFirstName, s.primaryUserEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    SendEmail(SchoolCodeFormat, ParentIdFormat, oldprimaryuserid, s.FatherFirstName, s.primaryUserEmail);
                                                    FatherRegId = Fatherreg.EditAddFatherdetails(s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                                    Admintrans.EditUpdateFatherPrimaryUserId(oldprimaryuserid, StudentRegId, FatherRegId);
                                                    // s_sbling.ChangeSiblingfromNewusertoExistUser(oldprimaryuserid, StudentRegId, studentdetails.StudentId);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //user confirmation is yes then update exist primary user id
                                            var existuser = primaryreg.CheckPrimaryuserfather(s.FatherEmail, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdfatherId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.FatherRegisterId));
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                if (checkuser.MotherRegisterId == null)
                                {
                                    if (s.MotherFirstname != null || s.MotherLastName != null || s.MotherDOB != null || s.MotherQualification != null || s.MotherOccupation != null || s.MotherMobileNo != null || s.MotherEmail != null)
                                    {
                                        MotherRegId = Motherreg.CheckMotherExit(oldprimaryuserid, StudentRegId, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail, s.TotalIncome);
                                        Admintrans.UpdateMothertId(oldprimaryuserid, StudentRegId, MotherRegId);
                                    }
                                }
                                else
                                {
                                    int motherid = Convert.ToInt16(checkuser.MotherRegisterId);
                                    //check mother details exist,then update exist mother id into transaction table
                                    MotherRegId = Motherreg.CheckEditMotherdetailsExit(oldprimaryuserid, StudentRegId, motherid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail); //update mother details in same motherregisterid
                                    Admintrans.UpdateMothertId(oldprimaryuserid, StudentRegId, MotherRegId);
                                }
                                if (checkuser.GuardianRegisterId == null)
                                {
                                    if (s.GuardianFirstname != null || s.GuardianLastName != null || s.GuardianDOB != null || s.GuardianQualification != null || s.GuardianOccupation != null || s.GuardianMobileNo != null || s.GuardianEmail != null || s.GuardianIncome != null || s.GuRelationshipToChild != null)
                                    {
                                        GuardianRegId = Guardianreg.CheckGuardianExit(oldprimaryuserid, StudentRegId, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                        Admintrans.UpdateGuardiantId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                    }
                                }
                                else
                                {
                                    int gid = Convert.ToInt16(checkuser.GuardianRegisterId);
                                    GuardianRegId = Guardianreg.CheckEditGuardiandetailsExit(oldprimaryuserid, StudentRegId, gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender); //update Guardian details in same motherregisterid
                                    Admintrans.UpdateGuardiantId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                }
                            }
                        }
                    }
                    else if (s.PrimaryUser == "Mother")
                    {
                        if (s.PrimaryUser == oldprimaryuser.PrimaryUser) // new primary user  is SAME as old primary user 
                        {
                            if (NoOfStudents.Count == 0)  // primary user contain ONE student
                            {
                                if (checkuser.MotherRegisterId != null)
                                {
                                    var mid = Convert.ToInt16(checkuser.MotherRegisterId);
                                    if (s.MotherFirstname != null && s.MotherLastName != null && s.MotherDOB != null && s.MotherEmail != null && s.MotherMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.MotherEmail) // new primary user email is SAME as old primary user email
                                            {
                                                primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.MotherFirstname, s.MotherLastName, s.MotherMobileNo, s.MotherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                Motherreg.EditmotherDetails1(mid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    //email exist then get user confirmation
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize(("email_exits"));
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code update primary user details and father details in table(PrimaryUserRegister,FatherRegister)
                                                {
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.MotherFirstname, s.MotherLastName, s.MotherMobileNo, s.MotherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Motherreg.EditmotherDetails1(mid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var existuser = primaryreg.CheckPrimaryusermother(s.MotherEmail, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                Motherreg.ChangeMotherStatusFlag(mid, StudentRegId);
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdMotherId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.MotherRegisterId));
                                                s_sbling.ChangeSiblingfromNewusertoExistUser(oldprimaryuserid, StudentRegId, studentdetails.StudentId);
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                if (checkuser.FatherRegisterId == null)
                                {
                                    if (s.FatherFirstName != null || s.FatherLastName != null || s.FatherDOB != null || s.FatherQualification != null || s.FatherOccupation != null || s.FatherMobileNo != null || s.FatherEmail != null)
                                    {
                                        FatherRegId = Fatherreg.EditAddFatherdetails(s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                        Admintrans.UpdateFathertId(oldprimaryuserid, StudentRegId, FatherRegId);
                                    }
                                }
                                else
                                {
                                    int fatherid = Convert.ToInt16(checkuser.FatherRegisterId);
                                    Fatherreg.EditFatherDetails(fatherid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail); //update mother details in same motherregisterid
                                }
                                if (checkuser.GuardianRegisterId == null)
                                {
                                    if (s.GuardianFirstname != null || s.GuardianLastName != null || s.GuardianDOB != null || s.GuardianQualification != null || s.GuardianOccupation != null || s.GuardianMobileNo != null || s.GuardianEmail != null || s.GuardianIncome != null || s.GuRelationshipToChild != null)
                                    {
                                        GuardianRegId = Guardianreg.EditAddGuardiandetails(s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                        Admintrans.UpdateGuardiantId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                    }
                                }
                                else
                                {
                                    int gid = Convert.ToInt16(checkuser.GuardianRegisterId);
                                    Guardianreg.EditGuardianDetails(gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender); //update Guardian details in same motherregisterid
                                }
                            }
                            else  // primary user contain MORE student
                            {
                                if (checkuser.MotherRegisterId != null)
                                {
                                    var mid = Convert.ToInt16(checkuser.MotherRegisterId);
                                    if (s.MotherFirstname != null && s.MotherLastName != null && s.MotherDOB != null && s.MotherEmail != null && s.MotherMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.MotherEmail) // new primary user email is SAME as old primary user email
                                            {
                                                primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.MotherFirstname, s.MotherLastName, s.MotherMobileNo, s.MotherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                Motherreg.EditmotherDetails1(mid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    //email exist then get user confirmation
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize("email_exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code add primary user details and father details,update student primary userid in table(PrimaryUserRegister,FatherRegister,admissiontransactions) 
                                                {
                                                    s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                    oldprimaryuserid = primaryreg.EditAddPrimaryUser(s.PrimaryUser, s.MotherFirstname, s.MotherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    SendEmail(SchoolCodeFormat, ParentIdFormat, oldprimaryuserid, s.MotherFirstname, s.primaryUserEmail);
                                                    MotherRegId = Motherreg.EditmotherDetails1(mid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.MotherFirstname, s.MotherLastName, s.MotherMobileNo, s.MotherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Admintrans.EditUpdateMotherPrimaryUserId(oldprimaryuserid, StudentRegId, MotherRegId);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //user confirmation is yes then update exist primary user id
                                            var existuser = primaryreg.CheckPrimaryusermother(s.MotherEmail, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdMotherId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.MotherRegisterId));
                                                s_sbling.ChangeSiblingfromNewusertoExistUser(oldprimaryuserid, StudentRegId, studentdetails.StudentId);
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                if (checkuser.FatherRegisterId == null)
                                {
                                    if (s.FatherFirstName != null || s.FatherLastName != null || s.FatherDOB != null || s.FatherQualification != null || s.FatherOccupation != null || s.FatherMobileNo != null || s.FatherEmail != null)
                                    {
                                        FatherRegId = Fatherreg.CheckFatherExit(oldprimaryuserid, StudentRegId, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                        Admintrans.UpdateFathertId(oldprimaryuserid, StudentRegId, FatherRegId);
                                    }
                                }
                                else
                                {
                                    int fatherid = Convert.ToInt16(checkuser.FatherRegisterId);
                                    //check father details exist,then update exist father id into transaction table
                                    FatherRegId = Fatherreg.CheckEditFatherdetailsExit(oldprimaryuserid, StudentRegId, fatherid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                    Admintrans.UpdateFathertId(oldprimaryuserid, StudentRegId, FatherRegId);
                                }
                                if (checkuser.GuardianRegisterId == null)
                                {
                                    if (s.GuardianFirstname != null || s.GuardianLastName != null || s.GuardianDOB != null || s.GuardianQualification != null || s.GuardianOccupation != null || s.GuardianMobileNo != null || s.GuardianEmail != null || s.GuardianIncome != null || s.GuRelationshipToChild != null)
                                    {
                                        GuardianRegId = Guardianreg.CheckGuardianExit(oldprimaryuserid, StudentRegId, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                        Admintrans.UpdateGuardiantId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                    }
                                }
                                else
                                {
                                    int gid = Convert.ToInt16(checkuser.GuardianRegisterId);
                                    GuardianRegId = Guardianreg.CheckEditGuardiandetailsExit(oldprimaryuserid, StudentRegId, gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender); //update Guardian details in same motherregisterid
                                    Admintrans.UpdateGuardiantId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                }
                            }
                        }
                        else // new primary user  is Not SAME as old primary user 
                        {
                            if (NoOfStudents.Count == 0)  // primary user contain ONE student
                            {
                                if (checkuser.MotherRegisterId != null)
                                {
                                    var mid = Convert.ToInt16(checkuser.MotherRegisterId);
                                    if (s.MotherFirstname != null && s.MotherLastName != null && s.MotherDOB != null && s.MotherEmail != null && s.MotherMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.MotherEmail) // new primary user email is SAME as old primary user email
                                            {
                                                primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.MotherFirstname, s.MotherLastName, s.MotherMobileNo, s.MotherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                Motherreg.EditmotherDetails1(mid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    //email exist then get user confirmation
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize("email_exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code update primary user details and father details in table(PrimaryUserRegister,FatherRegister)
                                                {
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.MotherFirstname, s.MotherLastName, s.MotherMobileNo, s.MotherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Motherreg.EditmotherDetails1(mid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var existuser = primaryreg.CheckPrimaryusermother(s.MotherEmail, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                Motherreg.ChangeMotherStatusFlag(mid, StudentRegId);
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdMotherId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.MotherRegisterId));
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                else //Mother id null
                                {
                                    if (s.MotherFirstname != null && s.MotherLastName != null && s.MotherDOB != null && s.MotherEmail != null && s.MotherMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.MotherEmail) // new primary user email is SAME as old primary user email
                                            {
                                                primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.MotherFirstname, s.MotherLastName, s.MotherMobileNo, s.MotherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                MotherRegId = Motherreg.EditAddMotherdetails(s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                                Admintrans.EditUpdateMotherPrimaryUserId(oldprimaryuserid, StudentRegId, MotherRegId);
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize("email_exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code update primary user details and mother details in table(PrimaryUserRegister,MotherRegister)
                                                {
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.MotherFirstname, s.MotherLastName, s.MotherMobileNo, s.MotherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    MotherRegId = Motherreg.EditAddMotherdetails(s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                                    Admintrans.EditUpdateMotherPrimaryUserId(oldprimaryuserid, StudentRegId, MotherRegId);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var existuser = primaryreg.CheckPrimaryusermother(s.MotherEmail, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdMotherId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.MotherRegisterId));
                                                s_sbling.ChangeSiblingfromNewusertoExistUser(oldprimaryuserid, StudentRegId, studentdetails.StudentId);
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                if (checkuser.FatherRegisterId == null)
                                {
                                    if (s.FatherFirstName != null || s.FatherLastName != null || s.FatherDOB != null || s.FatherQualification != null || s.FatherOccupation != null || s.FatherMobileNo != null || s.FatherEmail != null)
                                    {
                                        FatherRegId = Fatherreg.EditAddFatherdetails(s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                        Admintrans.UpdateFathertId(oldprimaryuserid, StudentRegId, FatherRegId);
                                    }
                                }
                                else
                                {
                                    int fatherid = Convert.ToInt16(checkuser.FatherRegisterId);
                                    Fatherreg.EditFatherDetails(fatherid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail); //update mother details in same motherregisterid
                                }
                                if (checkuser.GuardianRegisterId == null)
                                {
                                    if (s.GuardianFirstname != null || s.GuardianLastName != null || s.GuardianDOB != null || s.GuardianQualification != null || s.GuardianOccupation != null || s.GuardianMobileNo != null || s.GuardianEmail != null || s.GuardianIncome != null || s.GuRelationshipToChild != null)
                                    {
                                        GuardianRegId = Guardianreg.EditAddGuardiandetails(s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                        Admintrans.UpdateGuardiantId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                    }
                                }
                                else
                                {
                                    int gid = Convert.ToInt16(checkuser.GuardianRegisterId);
                                    Guardianreg.EditGuardianDetails(gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender); //update Guardian details in same motherregisterid
                                }
                            }
                            //    else // primary user contain MORE student
                            else
                            {
                                if (checkuser.MotherRegisterId != null)
                                {
                                    var mid = Convert.ToInt16(checkuser.MotherRegisterId);
                                    if (s.MotherFirstname != null && s.MotherLastName != null && s.MotherDOB != null && s.MotherEmail != null && s.MotherMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.MotherEmail) // new primary user email is SAME as old primary user email
                                            {
                                                var RemaningStudentlist = Admintrans.GetRemainingStudent(oldprimaryuserid, StudentRegId);
                                                bool checkmothersame = Motherreg.CheckMotherSameinRemaingstudent(oldprimaryuserid, StudentRegId, RemaningStudentlist, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                                if (checkmothersame == true)
                                                {
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.MotherFirstname, s.MotherLastName, s.MotherMobileNo, s.MotherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Motherreg.EditmotherDetails1(mid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                                }
                                                else
                                                {
                                                    string Mismatchdetails = eAcademy.Models.ResourceCache.Localize("Remaining_student_father_details_is_mismatch,so_use_another_email");
                                                    return Json(new { Mismatchdetails = Mismatchdetails }, JsonRequestBehavior.AllowGet);
                                                }
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize("email_exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code update primary user details and father details in table(PrimaryUserRegister,FatherRegister)
                                                {
                                                    s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                    oldprimaryuserid = primaryreg.EditAddPrimaryUser(s.PrimaryUser, s.MotherFirstname, s.primaryUserEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    SendEmail(SchoolCodeFormat, ParentIdFormat, oldprimaryuserid, s.MotherFirstname, s.primaryUserEmail);
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.MotherFirstname, s.MotherLastName, s.MotherMobileNo, s.MotherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Motherreg.EditmotherDetails1(mid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                                    Admintrans.EditUpdatePrimaryUserIdMotherId(oldprimaryuserid, StudentRegId, mid);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var existuser = primaryreg.CheckPrimaryusermother(s.MotherEmail, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdMotherId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.MotherRegisterId));
                                                s_sbling.ChangeSiblingfromNewusertoExistUser(oldprimaryuserid, StudentRegId, studentdetails.StudentId);
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                else //Mother id null
                                {
                                    if (s.MotherFirstname != null && s.MotherLastName != null && s.MotherDOB != null && s.MotherEmail != null && s.MotherMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.MotherEmail) // new primary user email is SAME as old primary user email
                                            {
                                                MotherRegId = Motherreg.EditAddMotherdetails(s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                                Admintrans.UpdateMothertId(oldprimaryuserid, StudentRegId, MotherRegId);
                                                var RemaningStudentlist = Admintrans.GetRemainingStudent(oldprimaryuserid, StudentRegId);
                                                bool checkmothersame = Motherreg.CheckMotherSameinRemaingstudent(oldprimaryuserid, StudentRegId, RemaningStudentlist, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                                if (checkmothersame == true)
                                                {
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.MotherFirstname, s.MotherLastName, s.MotherMobileNo, s.MotherEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Motherreg.EditmotherDetails1(MotherRegId, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                                }
                                                else
                                                {
                                                    string Mismatchdetails = eAcademy.Models.ResourceCache.Localize("Remaining_student_mother_details_is_mismatch,so_use_another_email");
                                                    return Json(new { Mismatchdetails = Mismatchdetails }, JsonRequestBehavior.AllowGet);
                                                }
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    string dialogbox = ("email exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code update primary user details and father details in table(PrimaryUserRegister,FatherRegister)
                                                {
                                                    s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                    oldprimaryuserid = primaryreg.EditAddPrimaryUser(s.PrimaryUser, s.MotherFirstname, s.primaryUserEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    SendEmail(SchoolCodeFormat, ParentIdFormat, oldprimaryuserid, s.MotherFirstname, s.primaryUserEmail);
                                                    MotherRegId = Motherreg.EditAddMotherdetails(s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                                    Admintrans.EditUpdateMotherPrimaryUserId(oldprimaryuserid, StudentRegId, MotherRegId);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var existuser = primaryreg.CheckPrimaryusermother(s.MotherEmail, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdMotherId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.MotherRegisterId));
                                                s_sbling.ChangeSiblingfromNewusertoExistUser(oldprimaryuserid, StudentRegId, studentdetails.StudentId);
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                if (checkuser.FatherRegisterId == null)
                                {
                                    if (s.FatherFirstName != null || s.FatherLastName != null || s.FatherDOB != null || s.FatherQualification != null || s.FatherOccupation != null || s.FatherMobileNo != null || s.FatherEmail != null)
                                    {
                                        FatherRegId = Fatherreg.CheckFatherExit(oldprimaryuserid, StudentRegId, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                        Admintrans.UpdateFathertId(oldprimaryuserid, StudentRegId, FatherRegId);
                                    }
                                }
                                else
                                {
                                    int fatherid = Convert.ToInt16(checkuser.FatherRegisterId);
                                    //check father details exist,then update exist father id into transaction table
                                    FatherRegId = Fatherreg.CheckEditFatherdetailsExit(oldprimaryuserid, StudentRegId, fatherid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                    Admintrans.UpdateFathertId(oldprimaryuserid, StudentRegId, FatherRegId);
                                }
                                if (checkuser.GuardianRegisterId == null)
                                {
                                    if (s.GuardianFirstname != null || s.GuardianLastName != null || s.GuardianDOB != null || s.GuardianQualification != null || s.GuardianOccupation != null || s.GuardianMobileNo != null || s.GuardianEmail != null || s.GuardianIncome != null || s.GuRelationshipToChild != null)
                                    {
                                        GuardianRegId = Guardianreg.CheckGuardianExit(oldprimaryuserid, StudentRegId, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                        Admintrans.UpdateGuardiantId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                    }
                                }
                                else
                                {
                                    int gid = Convert.ToInt16(checkuser.GuardianRegisterId);
                                    GuardianRegId = Guardianreg.CheckEditGuardiandetailsExit(oldprimaryuserid, StudentRegId, gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender); //update Guardian details in same motherregisterid
                                    Admintrans.UpdateGuardiantId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                }
                            }
                        }
                    }
                    else if (s.PrimaryUser == "Guardian")
                    {
                        if (s.PrimaryUser == oldprimaryuser.PrimaryUser) // new primary user  is SAME as old primary user 
                        {
                            if (NoOfStudents.Count == 0)  // primary user contain ONE student
                            {
                                if (checkuser.GuardianRegisterId != null)
                                {
                                    var gid = Convert.ToInt16(checkuser.GuardianRegisterId);
                                    if (s.GuardianFirstname != null && s.GuardianLastName != null && s.GuardianDOB != null && s.GuardianEmail != null && s.GuardianMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.GuardianEmail) // new primary user email is SAME as old primary user email
                                            {
                                                primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.GuardianFirstname, s.GuardianLastName, s.GuardianMobileNo, s.GuardianEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                Guardianreg.EditGuardianDetails(gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    //email exist then get user confirmation
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize("email_exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code update primary user details and father details in table(PrimaryUserRegister,FatherRegister)
                                                {
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.GuardianFirstname, s.GuardianLastName, s.GuardianMobileNo, s.GuardianEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Guardianreg.EditGuardianDetails(gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var existuser = primaryreg.CheckPrimaryuserguardian(s.GuardianEmail, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                Guardianreg.ChangeGuardianStatusFlag(gid, StudentRegId);
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdGuardianId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.GuardianRegisterId));
                                                flag = 1;
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                if (checkuser.FatherRegisterId == null)
                                {
                                    if (s.FatherFirstName != null || s.FatherLastName != null || s.FatherDOB != null || s.FatherQualification != null || s.FatherOccupation != null || s.FatherMobileNo != null || s.FatherEmail != null)
                                    {
                                        FatherRegId = Fatherreg.EditAddFatherdetails(s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                        Admintrans.UpdateFathertId(oldprimaryuserid, StudentRegId, FatherRegId);
                                    }
                                }
                                else
                                {
                                    int fatherid = Convert.ToInt16(checkuser.FatherRegisterId);
                                    FatherRegId = Fatherreg.EditFatherDetails(fatherid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail); //update mother details in same motherregisterid
                                }
                                if (checkuser.MotherRegisterId == null)
                                {
                                    if (s.MotherFirstname != null || s.MotherLastName != null || s.MotherDOB != null || s.MotherQualification != null || s.MotherOccupation != null || s.MotherMobileNo != null || s.MotherEmail != null)
                                    {
                                        MotherRegId = Motherreg.EditAddMotherdetails(s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                        Admintrans.UpdateMothertId(oldprimaryuserid, StudentRegId, MotherRegId);
                                    }
                                }
                                else
                                {
                                    int motherid = Convert.ToInt16(checkuser.MotherRegisterId);
                                    MotherRegId = Motherreg.EditmotherDetails1(motherid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail); //update mother details in same motherregisterid
                                }
                                if (flag == 1)
                                {
                                    if (FatherRegId != 0 && MotherRegId != 0)
                                    {
                                        s_sbling.CheckFatherMotherSameinSibling(oldprimaryuserid, StudentRegId, studentdetails.StudentId, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.MotherFirstname, s.MotherLastName, s.MotherDOB);
                                    }
                                    else if (FatherRegId != 0 && MotherRegId == 0)
                                    {
                                        s_sbling.CheckFatherSameinSibling(oldprimaryuserid, StudentRegId, studentdetails.StudentId, s.FatherFirstName, s.FatherLastName, s.FatherDOB);
                                    }
                                    else if (FatherRegId == 0 && MotherRegId != 0)
                                    {
                                        s_sbling.CheckMotherSameinSibling(oldprimaryuserid, StudentRegId, studentdetails.StudentId, s.MotherFirstname, s.MotherLastName, s.MotherDOB);
                                    }
                                }
                            }
                            else  // primary user contain MORE student
                            {
                                if (checkuser.GuardianRegisterId != null)
                                {
                                    var gid = Convert.ToInt16(checkuser.GuardianRegisterId);
                                    if (s.GuardianFirstname != null && s.GuardianLastName != null && s.GuardianDOB != null && s.GuardianEmail != null && s.GuardianMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.GuardianEmail) // new primary user email is SAME as old primary user email
                                            {
                                                primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.GuardianFirstname, s.GuardianLastName, s.GuardianMobileNo, s.GuardianEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                Guardianreg.EditGuardianDetails(gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    //email exist then get user confirmation
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize("email_exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code add primary user details and father details,update student primary userid in table(PrimaryUserRegister,FatherRegister,admissiontransactions) 
                                                {
                                                    s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                    oldprimaryuserid = primaryreg.EditAddPrimaryUser(s.PrimaryUser, s.GuardianFirstname, s.GuardianEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.GuardianIncome);
                                                    SendEmail(SchoolCodeFormat, ParentIdFormat, oldprimaryuserid, s.GuardianFirstname, s.primaryUserEmail);
                                                    GuardianRegId = Guardianreg.EditAddGuardiandetails(s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.GuardianFirstname, s.GuardianLastName, s.GuardianMobileNo, s.GuardianEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Admintrans.EditUpdateGuardianPrimaryUserId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //user confirmation is yes then update exist primary user id
                                            var existuser = primaryreg.CheckPrimaryuserguardian(s.GuardianEmail, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdGuardianId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.GuardianRegisterId));
                                                flag = 1;
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                if (checkuser.FatherRegisterId == null)
                                {
                                    if (s.FatherFirstName != null || s.FatherLastName != null || s.FatherDOB != null || s.FatherQualification != null || s.FatherOccupation != null || s.FatherMobileNo != null || s.FatherEmail != null)
                                    {
                                        FatherRegId = Fatherreg.CheckFatherExit(oldprimaryuserid, StudentRegId, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                        Admintrans.UpdateFathertId(oldprimaryuserid, StudentRegId, FatherRegId);
                                    }
                                }
                                else
                                {
                                    int fatherid = Convert.ToInt16(checkuser.FatherRegisterId);
                                    //check father details exist,then update exist father id into transaction table
                                    FatherRegId = Fatherreg.CheckEditFatherdetailsExit(oldprimaryuserid, StudentRegId, fatherid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                    Admintrans.UpdateFathertId(oldprimaryuserid, StudentRegId, FatherRegId);
                                }
                                if (checkuser.MotherRegisterId == null)
                                {
                                    if (s.MotherFirstname != null || s.MotherLastName != null || s.MotherDOB != null || s.MotherQualification != null || s.MotherOccupation != null || s.MotherMobileNo != null || s.MotherEmail != null)
                                    {
                                        MotherRegId = Motherreg.CheckMotherExit(oldprimaryuserid, StudentRegId, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail, s.TotalIncome);
                                        Admintrans.UpdateMothertId(oldprimaryuserid, StudentRegId, MotherRegId);
                                    }
                                }
                                else
                                {
                                    int motherid = Convert.ToInt16(checkuser.MotherRegisterId);
                                    //check mother details exist,then update exist mother id into transaction table
                                    MotherRegId = Motherreg.CheckEditMotherdetailsExit(oldprimaryuserid, StudentRegId, motherid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail); //update mother details in same motherregisterid
                                    Admintrans.UpdateMothertId(oldprimaryuserid, StudentRegId, MotherRegId);
                                }
                                if (flag == 1)
                                {
                                    if (FatherRegId != 0 && MotherRegId != 0)
                                    {
                                        s_sbling.CheckFatherMotherSameinSibling(oldprimaryuserid, StudentRegId, studentdetails.StudentId, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.MotherFirstname, s.MotherLastName, s.MotherDOB);
                                    }
                                    else if (FatherRegId != 0 && MotherRegId == 0)
                                    {
                                        s_sbling.CheckFatherSameinSibling(oldprimaryuserid, StudentRegId, studentdetails.StudentId, s.FatherFirstName, s.FatherLastName, s.FatherDOB);
                                    }
                                    else if (FatherRegId == 0 && MotherRegId != 0)
                                    {
                                        s_sbling.CheckMotherSameinSibling(oldprimaryuserid, StudentRegId, studentdetails.StudentId, s.MotherFirstname, s.MotherLastName, s.MotherDOB);
                                    }
                                }
                            }
                        }
                        else // new primary user  is Not SAME as old primary user 
                        {
                            if (NoOfStudents.Count == 0)  // primary user contain ONE student
                            {
                                if (checkuser.GuardianRegisterId != null)
                                {
                                    var gid = Convert.ToInt16(checkuser.GuardianRegisterId);
                                    if (s.GuardianFirstname != null && s.GuardianLastName != null && s.GuardianDOB != null && s.GuardianEmail != null && s.GuardianMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.GuardianEmail) // new primary user email is SAME as old primary user email
                                            {
                                                primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.GuardianFirstname, s.GuardianLastName, s.GuardianMobileNo, s.GuardianEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                Guardianreg.EditGuardianDetails(gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    //email exist then get user confirmation
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize("email_exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code update primary user details and father details in table(PrimaryUserRegister,FatherRegister)
                                                {
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.GuardianFirstname, s.GuardianLastName, s.GuardianMobileNo, s.GuardianEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Guardianreg.EditGuardianDetails(gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var existuser = primaryreg.CheckPrimaryuserguardian(s.GuardianEmail, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                Guardianreg.ChangeGuardianStatusFlag(gid, StudentRegId);
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdGuardianId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.GuardianRegisterId));
                                                flag = 1;
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                else //Guardian id null
                                {
                                    if (s.GuardianFirstname != null && s.GuardianLastName != null && s.GuardianDOB != null && s.GuardianEmail != null && s.GuardianMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.GuardianEmail) // new primary user email is SAME as old primary user email
                                            {
                                                primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.GuardianFirstname, s.GuardianLastName, s.GuardianMobileNo, s.GuardianEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                GuardianRegId = Guardianreg.EditAddGuardiandetails(s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                                Admintrans.EditUpdateGuardianPrimaryUserId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize("email_exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code update primary user details and Guardian details in table(PrimaryUserRegister,GuardianRegister)
                                                {
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.GuardianFirstname, s.GuardianLastName, s.GuardianMobileNo, s.GuardianEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    GuardianRegId = Guardianreg.EditAddGuardiandetails(s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                                    Admintrans.EditUpdateGuardianPrimaryUserId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var existuser = primaryreg.CheckPrimaryuserguardian(s.GuardianEmail, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdGuardianId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.GuardianRegisterId));
                                                flag = 1;
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                if (checkuser.FatherRegisterId == null)
                                {
                                    if (s.FatherFirstName != null || s.FatherLastName != null || s.FatherDOB != null || s.FatherQualification != null || s.FatherOccupation != null || s.FatherMobileNo != null || s.FatherEmail != null)
                                    {
                                        FatherRegId = Fatherreg.EditAddFatherdetails(s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                        Admintrans.UpdateFathertId(oldprimaryuserid, StudentRegId, FatherRegId);
                                    }
                                }
                                else
                                {
                                    int fatherid = Convert.ToInt16(checkuser.FatherRegisterId);
                                    FatherRegId = Fatherreg.EditFatherDetails(fatherid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail); //update mother details in same motherregisterid
                                }
                                if (checkuser.MotherRegisterId == null)
                                {
                                    if (s.MotherFirstname != null || s.MotherLastName != null || s.MotherDOB != null || s.MotherQualification != null || s.MotherOccupation != null || s.MotherMobileNo != null || s.MotherEmail != null)
                                    {
                                        MotherRegId = Motherreg.EditAddMotherdetails(s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail);
                                        Admintrans.UpdateMothertId(oldprimaryuserid, StudentRegId, MotherRegId);
                                    }
                                }
                                else
                                {
                                    int motherid = Convert.ToInt16(checkuser.MotherRegisterId);
                                    MotherRegId = Motherreg.EditmotherDetails1(motherid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail); //update mother details in same motherregisterid
                                }
                                if (flag == 1)
                                {
                                    if (FatherRegId != 0 && MotherRegId != 0)
                                    {
                                        s_sbling.CheckFatherMotherSameinSibling(oldprimaryuserid, StudentRegId, studentdetails.StudentId, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.MotherFirstname, s.MotherLastName, s.MotherDOB);
                                    }
                                    else if (FatherRegId != 0 && MotherRegId == 0)
                                    {
                                        s_sbling.CheckFatherSameinSibling(oldprimaryuserid, StudentRegId, studentdetails.StudentId, s.FatherFirstName, s.FatherLastName, s.FatherDOB);
                                    }
                                    else if (FatherRegId == 0 && MotherRegId != 0)
                                    {
                                        s_sbling.CheckMotherSameinSibling(oldprimaryuserid, StudentRegId, studentdetails.StudentId, s.MotherFirstname, s.MotherLastName, s.MotherDOB);
                                    }
                                }
                            }
                            //    else // primary user contain MORE student
                            else
                            {
                                if (checkuser.GuardianRegisterId != null)
                                {
                                    var gid = Convert.ToInt16(checkuser.GuardianRegisterId);
                                    if (s.GuardianFirstname != null && s.GuardianLastName != null && s.GuardianDOB != null && s.GuardianEmail != null && s.GuardianMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.GuardianEmail) // new primary user email is SAME as old primary user email
                                            {
                                                var RemaningStudentlist = Admintrans.GetRemainingStudent(oldprimaryuserid, StudentRegId);
                                                bool checkguardiansame = Guardianreg.CheckGuardianSameinRemaingstudent(oldprimaryuserid, StudentRegId, RemaningStudentlist, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                                if (checkguardiansame == true)
                                                {
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.GuardianFirstname, s.GuardianLastName, s.GuardianMobileNo, s.GuardianEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Guardianreg.EditGuardianDetails(gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                                }
                                                else
                                                {
                                                    string Mismatchdetails = eAcademy.Models.ResourceCache.Localize("Remaining_student_guardian_details_is_mismatch,so_use_another_email ");
                                                    return Json(new { Mismatchdetails = Mismatchdetails }, JsonRequestBehavior.AllowGet);
                                                }
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize("email exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code update primary user details and father details in table(PrimaryUserRegister,FatherRegister)
                                                {
                                                    s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                    oldprimaryuserid = primaryreg.EditAddPrimaryUser(s.PrimaryUser, s.GuardianFirstname, s.primaryUserEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.GuardianIncome);
                                                    SendEmail(SchoolCodeFormat, ParentIdFormat, oldprimaryuserid, s.GuardianFirstname, s.primaryUserEmail);
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.GuardianFirstname, s.GuardianLastName, s.GuardianMobileNo, s.GuardianEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Guardianreg.EditGuardianDetails(gid, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                                    Admintrans.EditUpdateGuardianPrimaryUserId(oldprimaryuserid, StudentRegId, gid);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var existuser = primaryreg.CheckPrimaryuserguardian(s.GuardianEmail, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdGuardianId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.GuardianRegisterId));
                                                flag = 1;
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                else //Guardian id null
                                {
                                    if (s.GuardianFirstname != null && s.GuardianLastName != null && s.GuardianDOB != null && s.GuardianEmail != null && s.GuardianMobileNo != null)
                                    {
                                        if (s.userconfirmation == 0)
                                        {
                                            if (oldprimaryuser.PrimaryUserEmail == s.GuardianEmail) // new primary user email is SAME as old primary user email
                                            {
                                                GuardianRegId = Guardianreg.EditAddGuardiandetails(s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                                Admintrans.UpdateGuardiantId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                                var RemaningStudentlist = Admintrans.GetRemainingStudent(oldprimaryuserid, StudentRegId);
                                                bool checkguardiansame = Guardianreg.CheckGuardianSameinRemaingstudent(oldprimaryuserid, StudentRegId, RemaningStudentlist, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                                if (checkguardiansame == true)
                                                {
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.GuardianFirstname, s.GuardianLastName, s.GuardianMobileNo, s.GuardianEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    Guardianreg.EditGuardianDetails(GuardianRegId, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                                }
                                                else
                                                {
                                                    string Mismatchdetails = eAcademy.Models.ResourceCache.Localize("Remaining_student_guardian_details_is_mismatch,so_use_another_email");
                                                    return Json(new { Mismatchdetails = Mismatchdetails }, JsonRequestBehavior.AllowGet);
                                                }
                                            }
                                            else // new primary user email is Not SAME as old primary user email
                                            {
                                                var checkemail = primaryreg.CheckEmailExit(oldprimaryuserid, s.primaryUserEmail);
                                                if (checkemail == true)
                                                {
                                                    string dialogbox = eAcademy.Models.ResourceCache.Localize("email_exits");
                                                    return Json(new { dialogbox = dialogbox }, JsonRequestBehavior.AllowGet);
                                                }
                                                else //below code update primary user details and father details in table(PrimaryUserRegister,GuardianRegister)
                                                {
                                                    s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                    oldprimaryuserid = primaryreg.EditAddPrimaryUser(s.PrimaryUser, s.GuardianFirstname, s.primaryUserEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.GuardianIncome);
                                                    SendEmail(SchoolCodeFormat, ParentIdFormat, oldprimaryuserid, s.GuardianFirstname, s.primaryUserEmail);
                                                    primaryreg.EditPrimaryUserDetails(oldprimaryuserid, s.PrimaryUser, s.GuardianFirstname, s.GuardianLastName, s.GuardianMobileNo, s.GuardianEmail, s.CompanyAddress1, s.CompanyAddress2, s.CompanyCity, s.CompanyState, s.CompanyCountry, s.CompanyPostelcode, s.CompanyContact, s.UserCompanyname, s.WorkSameSchool, s.TotalIncome);
                                                    GuardianRegId = Guardianreg.EditAddGuardiandetails(s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.GuardianQualification, s.GuardianOccupation, s.GuardianMobileNo, s.GuardianEmail, s.GuardianIncome, s.GuRelationshipToChild, s.GuardianGender);
                                                    Admintrans.EditUpdateGuardianPrimaryUserId(oldprimaryuserid, StudentRegId, GuardianRegId);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var existuser = primaryreg.CheckPrimaryuserguardian(s.GuardianEmail, s.GuardianFirstname, s.GuardianLastName, s.GuardianDOB, s.PrimaryUser);
                                            if (existuser != null)
                                            {
                                                //set old sibling student status flag is false
                                                s_sbling.ChangeOldsiblingstatus(oldprimaryuserid, StudentRegId);
                                                oldprimaryuserid = Convert.ToInt16(existuser.PrimaryUserRegisterId);
                                                Admintrans.EditUpdatePrimaryUserIdGuardianId(oldprimaryuserid, StudentRegId, Convert.ToInt16(existuser.GuardianRegisterId));
                                                flag = 1;
                                            }
                                            else
                                            {
                                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Primary_user_details_mismatch,so_use_another_email");
                                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }
                                }
                                if (checkuser.FatherRegisterId == null)
                                {
                                    if (s.FatherFirstName != null || s.FatherLastName != null || s.FatherDOB != null || s.FatherQualification != null || s.FatherOccupation != null || s.FatherMobileNo != null || s.FatherEmail != null)
                                    {
                                        FatherRegId = Fatherreg.CheckFatherExit(oldprimaryuserid, StudentRegId, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                        Admintrans.UpdateFathertId(oldprimaryuserid, StudentRegId, FatherRegId);
                                    }
                                }
                                else
                                {
                                    int fatherid = Convert.ToInt16(checkuser.FatherRegisterId);
                                    //check father details exist,then update exist father id into transaction table
                                    FatherRegId = Fatherreg.CheckEditFatherdetailsExit(oldprimaryuserid, StudentRegId, fatherid, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.FatherQualification, s.FatherOccupation, s.FatherMobileNo, s.FatherEmail);
                                    Admintrans.UpdateFathertId(oldprimaryuserid, StudentRegId, FatherRegId);
                                }
                                if (checkuser.MotherRegisterId == null)
                                {
                                    if (s.MotherFirstname != null || s.MotherLastName != null || s.MotherDOB != null || s.MotherQualification != null || s.MotherOccupation != null || s.MotherMobileNo != null || s.MotherEmail != null)
                                    {
                                        MotherRegId = Motherreg.CheckMotherExit(oldprimaryuserid, StudentRegId, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail, s.TotalIncome);
                                        Admintrans.UpdateMothertId(oldprimaryuserid, StudentRegId, MotherRegId);
                                    }
                                }
                                else
                                {
                                    int motherid = Convert.ToInt16(checkuser.MotherRegisterId);
                                    //check mother details exist,then update exist mother id into transaction table
                                    MotherRegId = Motherreg.CheckEditMotherdetailsExit(oldprimaryuserid, StudentRegId, motherid, s.MotherFirstname, s.MotherLastName, s.MotherDOB, s.MotherQualification, s.MotherOccupation, s.MotherMobileNo, s.MotherEmail); //update mother details in same motherregisterid
                                    Admintrans.UpdateMothertId(oldprimaryuserid, StudentRegId, MotherRegId);
                                }
                                if (flag == 1)
                                {
                                    if (FatherRegId != 0 && MotherRegId != 0)
                                    {
                                        s_sbling.CheckFatherMotherSameinSibling(oldprimaryuserid, StudentRegId, studentdetails.StudentId, s.FatherFirstName, s.FatherLastName, s.FatherDOB, s.MotherFirstname, s.MotherLastName, s.MotherDOB);
                                    }
                                    else if (FatherRegId != 0 && MotherRegId == 0)
                                    {
                                        s_sbling.CheckFatherSameinSibling(oldprimaryuserid, StudentRegId, studentdetails.StudentId, s.FatherFirstName, s.FatherLastName, s.FatherDOB);
                                    }
                                    else if (FatherRegId == 0 && MotherRegId != 0)
                                    {
                                        s_sbling.CheckMotherSameinSibling(oldprimaryuserid, StudentRegId, studentdetails.StudentId, s.MotherFirstname, s.MotherLastName, s.MotherDOB);
                                    }
                                }
                            }
                        }
                    }
                    if (s.PreSchool != null && s.PreMedium != null && s.PreClass != null && s.PreMarks != null && s.PreFromDate1 != null && s.PreToDate != null)
                    {
                        s_student.Edit_UpdatePreviousSchoolInformation(StudentRegId, s.PreSchool, s.PreMedium, s.PreClass, s.PreMarks, s.PreFromDate1, s.PreToDate);
                    }
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_Student_details"));
                    string Message = eAcademy.Models.ResourceCache.Localize("Successfullyedited");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public void SendEmail(string SchoolCodeFormat, string ParentIdFormat, int PrimayUserRegisterId, string PrimaryUserName, string Email)
        {
            PrimaryUserRegisterServices primaryUser = new PrimaryUserRegisterServices();
            string ParentId = SchoolCodeFormat + ParentIdFormat + PrimayUserRegisterId; //Parent Id Generated place
            string p_userName = PrimaryUserName;
            string p_Salt = p_userName.Substring(0, 3);
            Random P_random = new Random();
            string P_randomNumber = P_random.Next(00000, 99999).ToString();
            var P_pwd = p_Salt + P_randomNumber;
            var p_PasswordSalt = p_Salt + P_pwd;
            string Password = FormsAuthentication.HashPasswordForStoringInConfigFile(p_PasswordSalt, "SHA1");
            primaryUser.updateParentPassword(PrimayUserRegisterId, ParentId, p_Salt, Password, P_pwd);
            StreamReader reader = new StreamReader(Server.MapPath("~/Views/StudentEnrollment/StudentEmailPage.html"));
            string readFile = reader.ReadToEnd();
            string myString = "";
            myString = readFile;
            myString = myString.Replace("$$FirstName$$", p_userName);
            //      myString = myString.Replace("$$admissionId$$", admissionId.ToString());
            myString = myString.Replace("$$username$$", ParentId);
            myString = myString.Replace("$$password$$", P_pwd.ToString());
            string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
            string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];

            string PlaceName = "StudentEntrollment-SendEmail-Primary user login detail sent";
            string UserName = "Admin";
            string UserId = Convert.ToString(Session["empRegId"]);
            Mail.SendMail(EmailDisplay, supportEmail, Email, "Account Credentials", myString.ToString(), true, PlaceName, UserName, UserId);
        }
        [HttpPost]
        public JsonResult UploadEditStudentFile()
        {
            try
            {
                int stuid = Convert.ToInt16(Request["OnlineRegId"]);
                StudentServices s_student = new StudentServices();
                var stuadmissionid = s_student.getStudentProfile(stuid);
                string pathname = stuadmissionid.FirstOrDefault().studentId;
                string studentId = pathname;
                string name = ""; long FileSzie = 0;
                string BirthCertificate = "";
                string IncomeCertificate = ""; string TransferCertificate = ""; string CommunityCertificate = "";
                string studentphoto = "";
                FileHelper fh = new FileHelper();
                if (stuadmissionid.Count != 0)
                {
                    var Birth = HttpContext.Request.Files["UploadBirth"];
                    if (Birth != null)
                    {
                        name = "BC";
                        FileSzie = 512000;
                        var BirthCertificate1 = fh.Edit_CheckFile(Birth, pathname, name, FileSzie);
                        if (BirthCertificate1.FileSizeId == 98)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("BirthCertificate_file_size_exceeds");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (BirthCertificate1.FileTypeId == 99)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("BirthCertificate_file_format_not_supported ");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (BirthCertificate1.FileSizeId != 98 && BirthCertificate1.FileTypeId != 99 && BirthCertificate1.filename != "")
                        {
                            BirthCertificate = BirthCertificate1.filename;
                            s_student.Edit_updateBirthcertificate(studentId, BirthCertificate);
                        }
                    }
                    var community = HttpContext.Request.Files["UploadCommunity"];
                    if (community != null)
                    {
                        name = "CC";
                        FileSzie = 512000;
                        var CommunityCertificate1 = fh.Edit_CheckFile(community, pathname, name, FileSzie);
                        if (CommunityCertificate1.FileSizeId == 98)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("CommunityCertificate_file_size_exceeds ");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (CommunityCertificate1.FileTypeId == 99)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("CommunityCertificate_file_format_not_supported");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (CommunityCertificate1.FileSizeId != 98 && CommunityCertificate1.FileTypeId != 99 && CommunityCertificate1.filename != "")
                        {
                            CommunityCertificate = CommunityCertificate1.filename;
                            s_student.Edit_updateCommunitycertificate(studentId, CommunityCertificate);
                        }
                    }
                    var Income = HttpContext.Request.Files["UploadIncome"];
                    if (Income != null)
                    {
                        name = "IC";
                        FileSzie = 512000;
                        var IncomeCertificate1 = fh.Edit_CheckFile(Income, pathname, name, FileSzie);
                        if (IncomeCertificate1.FileSizeId == 98)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("IncomeCertificate_file_size_exceeds");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (IncomeCertificate1.FileTypeId == 99)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("IncomeCertificate_file_format_not_supported");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (IncomeCertificate1.FileSizeId != 98 && IncomeCertificate1.FileTypeId != 99 && IncomeCertificate1.filename != "")
                        {
                            IncomeCertificate = IncomeCertificate1.filename;
                            s_student.Edit_updateIncomecertificate(studentId, IncomeCertificate);
                        }
                    }
                    var Transfer = HttpContext.Request.Files["UploadTransfer"];
                    if (Transfer != null)
                    {
                        name = "TC";
                        FileSzie = 512000;
                        var TransferCertificate1 = fh.Edit_CheckFile(Transfer, pathname, name, FileSzie);
                        if (TransferCertificate1.FileSizeId == 98)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("TransferCertificate_file_size_exceeds ");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (TransferCertificate1.FileTypeId == 99)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("TransferCertificate_file_format_not_supported ");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (TransferCertificate1.FileSizeId != 98 && TransferCertificate1.FileTypeId != 99 && TransferCertificate1.filename != "")
                        {
                            TransferCertificate = TransferCertificate1.filename;
                            s_student.Edit_updateTransfercertificate(studentId, TransferCertificate);
                        }
                    }
                    var Image = HttpContext.Request.Files["UploadImage"];
                    if (Image != null)
                    {
                        name = "Photo";
                        FileSzie = 512000;
                        var Photo1 = fh.Edit_CheckImageFile(Image, pathname, name, FileSzie);
                        if (Photo1.FileSizeId == 98)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Student_Photo_file_size_exceeds");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (Photo1.FileTypeId == 99)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Student_Photo_file_format_not_supported");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (Photo1.FileSizeId != 98 && Photo1.FileTypeId != 99 && Photo1.filename != "")
                        {
                            studentphoto = Photo1.filename;
                            s_student.Edit_updateStudentPhoto(studentId, studentphoto);
                        }
                    }
                    string Message = eAcademy.Models.ResourceCache.Localize("Sucessfully_records_are_added");
                    return Json(new { Message = Message, BirthCertificate = BirthCertificate, CommunityCertificate = CommunityCertificate, TransferCertificate = TransferCertificate, studentphoto = studentphoto, IncomeCertificate = IncomeCertificate }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Please_upload_Required_Files");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage, }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}