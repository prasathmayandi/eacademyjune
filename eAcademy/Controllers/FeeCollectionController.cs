﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class FeeCollectionController : Controller
    {
        AcademicyearServices obj_academicYear = new AcademicyearServices();
        TermServices obj_term = new TermServices();
        ClassServices obj_class = new ClassServices();
        StudentServices obj_student = new StudentServices();
        FeeServices obj_Fee = new FeeServices();
        FeesRefundToStudentServices obj_Refundtostudent = new FeesRefundToStudentServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        public int AdminId;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ViewBag.acYear = obj_academicYear.AllList_AcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
            ViewBag.ClassList = obj_class.Classes();
            AdminId = Convert.ToInt32(Session["empRegId"]);
        }
        public ActionResult CollectFee()
        {
            FeeCollectionServices obj_feeCollection = new FeeCollectionServices();
            FeeCategoryServices obj_feeCategory = new FeeCategoryServices();
            try
            {
                var result = obj_feeCollection.getFeeCollection();
                if (result.Count != 0)
                {
                    var ans = obj_feeCollection.getFeeCollectionOrderByFeeCollectionID();
                    ViewBag.recID = ans + 1;
                }
                else
                {
                    ViewBag.recID = 1;
                }
                ViewBag.FeeCatList = obj_feeCategory.getFeeCategory();
                ViewBag.sysDate = DateTimeByZone.getCurrentDateTime().Date.ToString("dd/MM/yyyy");
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult getStudentFeeDetailsByRollNumber(string RegNo)
        {
            try
            {
                var ans = obj_student.getStudentByStudentId(RegNo);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getStudentFeeDetails(AcademicId model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = model.AcYearId;
                    int cid = model.ClassId;
                    int sec_id = model.SectionId;
                    int std_id = model.StudentId;
                    var ans = obj_student.getStudentId(acid, cid, sec_id, std_id);
                    return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult CollectFee(SaveFeeCollection model)
        {
            FeeCollectionServices obj_feeCollection = new FeeCollectionServices();
            FeeCategoryServices obj_feeCategory = new FeeCategoryServices();
            try
            {
                DateTime FCDate = DateTime.ParseExact(model.FeeCollectiondate, "dd/MM/yyyy", null); 
                int acid = model.AcademicYearId;
                int cid = model.ClassId;
                int sec_id = model.SectionId;
                int stu_id = model.StudentRegId;
                Decimal feeAmount = model.FeeAmount;
                feeAmount = Decimal.Parse(string.Format("{0:0.00}", feeAmount));
                Decimal fine = model.Fine;
                fine = Decimal.Parse(string.Format("{0:0.00}", fine));
                Decimal amountCollected = model.AmountCollected;
                amountCollected = Decimal.Parse(string.Format("{0:0.00}", amountCollected));
                Decimal discount = model.Discount;
                discount = Decimal.Parse(string.Format("{0:0.00}", discount));
                Decimal VAT = model.VAT;
                VAT = Decimal.Parse(string.Format("{0:0.00}", VAT));
                string mode = model.PaymentMode;
                string bankName = model.BankName;
                string chequeNumber = model.ChequeNumber;
                string DDNumber = model.DDNumber;
                string remark = model.Remark;
                string paymentMode = "";
                if (mode == "2")
                {
                    paymentMode = "Cheque";
                }
                else if (mode == "3")
                {
                    paymentMode = "DD";
                }
                else
                {
                    paymentMode = "Cash";
                }
               int collectionid= obj_feeCollection.collectFeeAmount(FCDate, acid, cid, sec_id, stu_id, feeAmount, fine, amountCollected, discount, VAT, paymentMode, bankName, chequeNumber, DDNumber, remark, AdminId);
                int count1 = 0, count2 = 0;
                if (model.FeeCategoryId != null)
                {
                    count1 = model.FeeCategoryId.Count();
                    int[] FeeCatId = new int[count1];
                    int[] paymentId = new int[count1];
                    if (count1 > 0)
                    {
                        for (int i = 0; i < count1; i++)
                        {
                            FeeCatId[i] = model.FeeCategoryId[i];
                            paymentId[i] = model.PaymentTypeId[i];
                            obj_feeCollection.collectFeeAmount(FCDate, acid, cid, sec_id, stu_id, feeAmount, fine, amountCollected, discount, VAT, paymentMode, bankName, chequeNumber, DDNumber, remark, FeeCatId[i], paymentId[i], AdminId, collectionid);
                        }
                    }
                }
                if (model.SubFeeCategoryId != null)
                {
                    count2 = model.SubFeeCategoryId.Count();
                    int[] SubFeeCatId = new int[count2];
                    int[] SubpaymentId = new int[count2];
                    if (count2 > 0)
                    {
                        for (int i = 0; i < count2; i++)
                        {
                            SubFeeCatId[i] = model.SubFeeCategoryId[i];
                            SubpaymentId[i] = model.SubPaymentTypeId[i];
                            if (model.FeeCategoryId != null)
                            {
                                if (model.FeeCategoryId.Contains(SubFeeCatId[i]))
                                {
                                }
                                else
                                {
                                    obj_feeCollection.collectFeeAmount(FCDate, acid, cid, sec_id, stu_id, feeAmount, fine, amountCollected, discount, VAT, paymentMode, bankName, chequeNumber, DDNumber, remark, SubFeeCatId[i], SubpaymentId[i], AdminId, collectionid);
                                }
                            }
                            else
                            {
                                obj_feeCollection.collectFeeAmount(FCDate, acid, cid, sec_id, stu_id, feeAmount, fine, amountCollected, discount, VAT, paymentMode, bankName, chequeNumber, DDNumber, remark, SubFeeCatId[i], SubpaymentId[i], AdminId, collectionid);
                            }
                        }
                    }
                }
                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("CollectFee"));
                string Message = eAcademy.Models.ResourceCache.Localize("FeeCollectedSuccessfully");
                string encrptycollectionid = QSCrypt.Encrypt(collectionid.ToString());
                return Json(new { Message = Message, collectionid = collectionid, encrptycollectionid = encrptycollectionid }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult BillReceipt(string id)
        {
            try
            {
                FeeCollectionServices Fee_Collection = new FeeCollectionServices();
                SchoolSettingServices schoolsetting = new SchoolSettingServices();
                FeeCollectionServices FeeCollect = new FeeCollectionServices();
                FeeCollectionCategoryServices FeeColleectCategory = new FeeCollectionCategoryServices();
                PrimaryUserRegisterServices puser = new PrimaryUserRegisterServices();
                int FeecolletionId = Convert.ToInt16(QSCrypt.Decrypt(id));
                var list = Fee_Collection.GetFeeCollectDetails(FeecolletionId);
                
                var Email = puser.GetStudentPrimarUserEmail(list.StudentId);
                var primaryuserinfo = puser.GetPrimaryUserdetails(Email);
                var school = schoolsetting.getSchoolSettings();
                var SchoolName = school.SchoolName;
                var SchoolLogo = school.SchoolLogo;
                string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                var Feeslist = FeeColleectCategory.GetFeeCollectionCategory(list.ReceiptNo);
                var data = FeeCollect.GetCollectionDetails(list.ReceiptNo);
                var Fine = data.Fine;
                var discount = data.Discount;
                var totalamount = data.AmountCollected;
                var vat = data.VAT;
                string body_txt = "";
                string footer_txt = "";
                int i = 1;
                string sname = list.StuFirstName;
                string txt_cls = list.txt_Class;
                foreach(var v in Feeslist)
                {
                    body_txt = body_txt + "<tr class='tr-content' style='text-align: center;FONT-SIZE: 13px;'><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                               i + "</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                               v.FeeCategory + "</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                               v.Amount + "</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                               v.ServicesTax + "</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                               v.Total + "</td></tr>";
                    i = i + 1;
                }
                footer_txt = "<tr class='tr-content' style='text-align: center;FONT-SIZE: 13px;'><td  colspan='4' style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>Fine</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                               Fine + "</td></tr><tr class='tr-content' style='text-align: center;FONT-SIZE: 13px;'><td  colspan='4' style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>Discount</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                               discount + "</td></tr><tr class='tr-content' style='text-align: center;FONT-SIZE: 13px;'><td  colspan='4' style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>VAT</td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                               vat + "</td></tr><tr class='tr-content' style='text-align: center;FONT-SIZE: 13px;'><td  colspan='4' style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'><b>TotalAmount</b></td><td style='padding: 8px;font-size: 13px;font-weight: 600;font-family: monospace;line-height: 1.42857143;vertical-align: middle;color: #3c763d;border: 1px solid #337ab7;'>" +
                                               totalamount + "</td></tr>";

                StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/FeeCollection.html"));
                string readFile2 = reader2.ReadToEnd();
                string myString2 = "";
                myString2 = readFile2;
                myString2 = myString2.Replace("$$surname$$", primaryuserinfo.PrimaryUserName.ToString());
                myString2 = myString2.Replace("$$appno$$", list.ReceiptNo.ToString());                
                myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
                myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
                myString2 = myString2.Replace("$$StudentName$$", sname.ToString());
                myString2 = myString2.Replace("$$ApplyClass$$", txt_cls.ToString());
                myString2 = myString2.Replace("$$AcdemicYear$$", list.txt_academicyear.ToString());
                myString2 = myString2.Replace("$$tables1$$", body_txt.ToString());
                myString2 = myString2.Replace("$$tables2$$", footer_txt.ToString());
                string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];

                string PlaceName = "FeeCollection-BillReceipt-Bill receipt sent to parent";
                string UserName = "Employee";
                string UserId = Convert.ToString(Session["username"]);


                Mail.SendMultipleMail(EmailDisplay, supportEmail, Email, "Account Credentials", myString2.ToString(), true, PlaceName, UserName, UserId); 

                return View(list);
            }
            catch(Exception e)
            {
                return View();
            }
        }
        [HttpGet]
        public JsonResult GetFeeCategory(int feecollectionId)
        {
            try
            {
                FeeCollectionServices FeeCollect = new FeeCollectionServices();
                FeeCollectionCategoryServices FeeColleectCategory = new FeeCollectionCategoryServices();
                var list=FeeColleectCategory.GetFeeCollectionCategory(feecollectionId);
                var data = FeeCollect.GetCollectionDetails(feecollectionId);
                var Fine = data.Fine;
                var discount = data.Discount;
                var totalamount = data.AmountCollected;
                var vat = data.VAT;
                return Json(new { list = list, Fine = Fine, discount = discount, totalamount = totalamount, vat = vat }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getClass()
        {
            try
            {
                var ans = obj_class.Classes();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getTermCount(int acid)
        {
            try
            {
                var ans = obj_term.getTerm(acid);
                var count = ans.Count();
                return Json(count, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getSection(int cid)
        {
            SectionServices obj_section = new SectionServices();
            try
            {
                var ans = obj_section.getSectionByClassId(cid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getStudent(int acid, int sec_id, int cid)
        {
            StudentServices obj_student = new StudentServices();
            try
            {
                var ans = obj_student.getStudentByIDs(sec_id, cid, acid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getFees(int acid, int cid, int secid, int stdid)
        {
            FeeServices obj_fee = new FeeServices();
            try
            {
                var ans = obj_fee.getallFeesByID(acid, cid);
                var result = obj_fee.getPaidFeesByID(acid, cid, secid, stdid);
                return Json(new { ans = ans, result = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getFeeAmount(int acid, int fid, int cid)
        {
            FeeServices obj_fee = new FeeServices();
            try
            {
                var ans = obj_fee.getFeeByID(acid, fid, cid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getTerms(int acid)
        {
            try
            {
                var ans = obj_term.TermOnYear(acid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getVAT()
        {
            try
            {
                SchoolSettingsServices obj_settings = new SchoolSettingsServices();
                var ans = obj_settings.SchoolSettings();
                string VAT = ans.VAT.ToString();
                return Json(VAT, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getTermFeeAmount(int acid, int fid, int cid, int tid)
        {
            TermFeeServices obj_termFee = new TermFeeServices();
            try
            {
                var ans = obj_termFee.getTermFeeByID(acid, fid, cid, tid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetRefundDetails(int CollectionId)
        {
            FeeCollectionCategoryServices FeeCollection = new FeeCollectionCategoryServices();
            try
            {
                var ans = FeeCollection.GetFeeCollectionDetails(CollectionId);
                var result = FeeCollection.AlreadyRefundAmount(CollectionId);
                var amount = FeeCollection.Existmanagementrefund(CollectionId, ans.RefundAmount, ans.FeeId);
                return Json(new { ans = ans, result = result, amount = amount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult FeesRefund(VM_FeeRefund cs)
        {
            FeeCollectionCategoryServices FeeCollection = new FeeCollectionCategoryServices();
            try
            {
                if (ModelState.IsValid)
                {
                    if (cs.refundamt <= cs.ExistCollectedamt)
                    {
                         int feerefundid= FeeCollection.FeesRefundupdate(Convert.ToInt16(cs.CollectionId), cs.ExistCollectedamt, cs.refundamt, cs.netamount, cs.Reason, cs.ReceivedBy, AdminId, Convert.ToInt16(cs.FeeId));
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("RefundFee"));
                    string Message = eAcademy.Models.ResourceCache.Localize("SuccessfullyRefundedAmount");
                   string refundid = QSCrypt.Encrypt(feerefundid.ToString());
                   return Json(new { Message = Message, refundid = refundid }, JsonRequestBehavior.AllowGet);
                    }
                    else {
                        string ErrorMge = eAcademy.Models.ResourceCache.Localize("Please_enter_refund_amount_is_less_than_collected_amount");
                        return Json(new { ErrorMessage = ErrorMge });
                    }
                   
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult FeeRefundReports()
        {
            return View();
        }
      
        public JsonResult FeeRefundResults(string sidx, string sord, int page, int rows, int? acYear_Fees, int? ClassList, int? SectionList, string SortOption, string Sort, int? FeecategoryList)
        {
             IList<FeesRefundList> todoListsResults4 = new List<FeesRefundList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (acYear_Fees.HasValue && ClassList.HasValue && SectionList.HasValue && SortOption == "ManagementRefund")
            {
                if (Sort == "Paid")
                {
                    todoListsResults4 = obj_Refundtostudent.getRefundFeePaidStudentList(Convert.ToInt16(acYear_Fees), Convert.ToInt16(ClassList), Convert.ToInt16(SectionList), Convert.ToInt16(FeecategoryList));
                }
                else if (Sort == "Due")
                {
                    todoListsResults4 = obj_Refundtostudent.getRefundFeeDueStudentList(Convert.ToInt16(acYear_Fees), Convert.ToInt16(ClassList), Convert.ToInt16(SectionList), Convert.ToInt16(FeecategoryList));
                }
            }
            else if (acYear_Fees.HasValue && ClassList.HasValue && SectionList.HasValue && SortOption == "StudentRefund")
            {
                todoListsResults4 = obj_Refundtostudent.getStudentRefundFeePaidStudentList(Convert.ToInt16(acYear_Fees), Convert.ToInt16(ClassList), Convert.ToInt16(SectionList));
            }
            else if (acYear_Fees.HasValue && ClassList.HasValue && SectionList.HasValue && SortOption == "AdmissionRefund")
            {
                todoListsResults4 = obj_Refundtostudent.getAdmissionFeerefund(Convert.ToInt16(acYear_Fees), Convert.ToInt16(ClassList));
            }
            int totalRecords = todoListsResults4.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults4 = todoListsResults4.OrderByDescending(s => s.StudentName).ToList();
                    todoListsResults4 = todoListsResults4.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults4 = todoListsResults4.OrderBy(s => s.StudentName).ToList();
                    todoListsResults4 = todoListsResults4.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults4;
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults4
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FeesRefundlistReport_ExportToExcel()
        {
            try
            {
                List<FeesRefundList> todoListsResults4 = (List<FeesRefundList>)Session["JQGridList"];
                var list = todoListsResults4.Select(o => new { AcademicYear = o.AcademicYear, Class = o.Class, StudentId = o.StudentId, StudentName = o.StudentName, FeeCategory = o.FeeCategory, Refund = o.Refund, NetAmount = o.NetAmount, Reason = o.Reason, Date = o.Date }).ToList();
                string fileName =  eAcademy.Models.ResourceCache.Localize("FeesRefundlist");
                GenerateExcel.ExportExcel(list, fileName);
                return View("FunctionTimeTable");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult FeesRefundlistReport_ExportToPdf()
        {
            try
            {
                List<FeesRefundList> todoListsResults4 = (List<FeesRefundList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading =  eAcademy.Models.ResourceCache.Localize("FeesRefundlist");
                GeneratePDF.ExportPDF_Portrait(todoListsResults4, new string[] { "AcademicYear", "Class", "StudentId", "StudentName", "FeeCategory", "Refund", "NetAmount", "Reason", "Date" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", "FeesRefundlist.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public JsonResult GetFeeCategory1(int year, int clas)
        {
            try
            {
                FeeServices Fee_Services = new FeeServices();
                var ans = Fee_Services.GetRefundfeeCategory(year, clas);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult AdmissionFeeRefund()
        {
            return View();
        }
        public JsonResult GetAdmissionPaidFeesdetails(VM_AdmissionFeeRefund cs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PrimaryUserRegisterServices primary = new PrimaryUserRegisterServices();
                    AssignClassToStudentServices assignclass = new AssignClassToStudentServices();
                    FeeCollectionCategoryServices Fee_CollectionCategory = new FeeCollectionCategoryServices();
                    StudentServices S_Student = new StudentServices();
                    var ss = S_Student.GetStudentRegisterIdbyapplicationNumber(Convert.ToInt16(cs.AdmissionId));
                    if (ss != null)
                    {
                        var CheckStudentDetails = primary.CheckStudentIdandUserEmail(ss.StudentRegisterId, cs.EmailId);
                        if (CheckStudentDetails == true)
                        {
                            var CheckAlreadyRefund = Fee_CollectionCategory.CheckAlreadyrefundAdmissionFee(cs.AdmissionId);
                            if (CheckAlreadyRefund == false)
                            {
                                var classAssigned = assignclass.CheckClassAssignedtoAdmissionStudent(cs.AdmissionId);
                                if (classAssigned == true)
                                {
                                    string info = eAcademy.Models.ResourceCache.Localize("Class_assigned_to_this_student,so_fees_can't_refunded");
                                    return Json(new { info = info }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    var list = Fee_CollectionCategory.GetAdmissionpaidFees(cs.AdmissionId);
                                    var result = S_Student.GetAdmissionStudentDetails(cs.AdmissionId);
                                    return Json(new { list, result }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                string info = eAcademy.Models.ResourceCache.Localize("Already_admission_fees_refunded_to_this_user");
                                return Json(new { info = info }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Enter_correct_admission_id_and_primary_user_email_address");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Enter_correct_admission_id ");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AdmissionFeeRefunds(VM_AdmissionFeeRefundToStudent cs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    FeeCollectionCategoryServices Fee_ColletCategory = new FeeCollectionCategoryServices();
                    FeeCollectionServices Fee_Collection = new FeeCollectionServices();
                    StudentServices Stu = new StudentServices();
                    string[] CollectedFeeCat = new string[cs.NoOfFees];
                    if (cs.NoOfFees != 0)
                    {
                        int collectionid = 0;
                        for (int i = 0; i < cs.NoOfFees; i++)
                        {
                            CollectedFeeCat[i] = String.Join(" ", cs.CollectionCategoryId[i]);
                            collectionid = Convert.ToInt16(CollectedFeeCat[i]);
                            Fee_ColletCategory.AdmissionFessRefundtostudent(collectionid, AdminId, cs.ReceivedBy, cs.reason);
                        }
                        Fee_Collection.UpdateFeeRefundCollection(collectionid, AdminId);
                        Stu.updatestudentstatusforRefundAdmissionfees(collectionid);
                    }
                    useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("RefundAdmissionFee"));
                    string Message = eAcademy.Models.ResourceCache.Localize("Successfully_refund_fees_to_student");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}