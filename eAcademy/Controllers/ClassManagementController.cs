﻿using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using eAcademy.Services;
using eAcademy.HelperClass;
using eAcademy.Models;

namespace eAcademy.Controllers
{

    [CheckSessionOutAttribute]
    public class ClassManagementController : Controller
    {
        //
        // GET: /ClassManagement/

        public int facultyId; int featureId;
        TechemployeesServices techEmp = new TechemployeesServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            facultyId = Convert.ToInt32(Session["empRegId"]);
            AcademicyearServices AcYear = new AcademicyearServices();
            ClassServices clas = new ClassServices();
            SubjectServices sub = new SubjectServices();
            ViewBag.XallAcademicYears = AcYear.AddFormAcademicYear();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.allClasses = clas.ShowClasses();
            featureId = 28;  // who has feature as a faculty info
            ViewBag.allSubjects = sub.ShowSubjects();
            
        }

        public JsonResult GetSelectedClassSection(int passClassId)
        {
            try
            {
                SectionServices sec = new SectionServices();
                var List = sec.getSection(passClassId);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSelectedClassFaculty(int passClassId)
        {
            try
            {
                TechEmployeeClassSubjectServices FacultyClassSub = new TechEmployeeClassSubjectServices();
                var List = FacultyClassSub.ShowClassFaculties(passClassId);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getFaculties(int passClassId)
        {
            try
            {
                var list = techEmp.ShowFaculties(passClassId);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AssignSubjectWithFaculty()
        {
            return View();
        }

        public JsonResult SubjectFacultyListResults(string sidx, string sord, int page, int rows, int? allAcademicYears_List, int? allClass_List, int? allSection_List)
        {
           IList<ListSubjectWithFaculty> SubjectFacultyList = new List<ListSubjectWithFaculty>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (SubjectFacultyList.Count > 0)
            {
                int count=SubjectFacultyList.Count;
                for (int i = 0; i < count;i++ )
                {
                    var dsd = SubjectFacultyList.OrderBy(s => s.ACI_Id).FirstOrDefault();
                    SubjectFacultyList.Remove(dsd);
                }
            }
            if (allAcademicYears_List != null && allClass_List != null && allSection_List != null )
            {
                AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
                SubjectFacultyList = facultySub.getSubjectFacultyList(allAcademicYears_List, allClass_List, allSection_List);
            }
            int totalRecords = SubjectFacultyList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    SubjectFacultyList = SubjectFacultyList.OrderByDescending(s => s.Subject).ToList();
                    SubjectFacultyList = SubjectFacultyList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                    
                }
                else
                {
                    SubjectFacultyList = SubjectFacultyList.OrderBy(s => s.Subject).ToList();
                    SubjectFacultyList = SubjectFacultyList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = SubjectFacultyList; 
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = SubjectFacultyList
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubjectFacultyList_ExportToExcel()
        {
            try
            {
                List<ListSubjectWithFaculty> SubjectFacultyList = (List<ListSubjectWithFaculty>)Session["JQGridList"];
                var list = SubjectFacultyList.Select(o => new { Subject = o.Subject, Faculty = o.Faculty, ClassIncharge = o.ClassIncharge, SubjectStaus =Convert.ToString( o.SubjectStatus) }).ToList();
                string fileName = ResourceCache.Localize("SubjectFacultyListXlsx");
                GenerateExcel.ExportExcel(list, fileName);
                return View("FacultyAttendanceReport");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult SubjectFacultyList_ExportToPdf()
        {
            try
            {
                List<ListSubjectWithFaculty> SubjectFacultyList = (List<ListSubjectWithFaculty>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("SubjectFacultyListXlsx");
                GeneratePDF.ExportPDF_Portrait(SubjectFacultyList, new string[] { "Subject", "Faculty", "ClassIncharge", "SubjectStatus" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("SubjectFacultyList.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult AddSubjectWithFaculty(AddSubjectWithFaculty m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int AcademicYearId = m.allAcademicYears;
                    int ClassId = m.AssignFacultyToSubject_allClass;
                    int SectionId = m.AssignFacultyToSubject_Section;
                    int SubjectId = m.AssignFacultyToSubjects;
                    int EmployeeRegisterId = m.allFaculties;
                    var IsCI = m.CI_SubjectWithFaculty;

                    AssignSubjectToSectionServices secSub = new AssignSubjectToSectionServices();
                    int? GroupSubjectId = secSub.GetGroupSubject(SubjectId);

                    var checkGroupSubjectExist1 = secSub.checkSubjectExist1(AcademicYearId, ClassId, SectionId, GroupSubjectId);
                    var checkSubjectExist = secSub.checkSubjectExist(AcademicYearId, ClassId, SectionId, SubjectId);
                    if (checkSubjectExist == null && checkGroupSubjectExist1 == null)
                    {
                        secSub.addSubjectToSection(AcademicYearId, ClassId, SectionId, SubjectId, GroupSubjectId);
                    }
                    else if (checkGroupSubjectExist1 != null)
                    {
                        string ErrorMessage = ResourceCache.Localize("This_is_Elective_Subject_It_is_Already_Assgined_to_Section");
                        return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = ResourceCache.Localize("Subject_already_Exist");
                        return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }

                    AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
                    var checkRecordExist = facultySub.getSubjectFaculty(AcademicYearId, ClassId, SectionId, SubjectId);
                    if (checkRecordExist == null)
                    {
                        facultySub.addFacultyToSubject(AcademicYearId, ClassId, SectionId, SubjectId, EmployeeRegisterId);
                    }
                    else
                    {
                        string ErrorMessage = ResourceCache.Localize("Faculty_already_exist_for_selected_subject");
                        return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }

                    AssignClassTeacherServices clasTeacher = new AssignClassTeacherServices();
                    if (IsCI == "true")
                    {
                        var CheckRecordExist = clasTeacher.checkRecord(AcademicYearId, ClassId, SectionId, "Yes");
                        if (CheckRecordExist == null)
                        {
                            clasTeacher.addClassTeacher(AcademicYearId, ClassId, SectionId, EmployeeRegisterId, "Yes", SubjectId);
                        }
                        else
                        {
                            clasTeacher.addClassTeacher(AcademicYearId, ClassId, SectionId, EmployeeRegisterId, "No", SubjectId);
                            string ErrorMessage = ResourceCache.Localize("Subject_and_faculty_assigned.But_class_incharge_is_already_exist");
                            return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        clasTeacher.addClassTeacher(AcademicYearId, ClassId, SectionId, EmployeeRegisterId, "No", SubjectId);
                    }
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Add_subject_with_faculty"));
                    string Message = ResourceCache.Localize("Saved_successfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditSubjectWithFaculty(string passId)
        {
            try
            {
                AcademicyearServices acyearServ = new AcademicyearServices();
                int eid = Convert.ToInt32(passId);
                AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
                var getRow = facultySub.checkFacultyAssigned(eid);
                int yearId = getRow.AcademicYearId.Value;
                int classId = getRow.ClassId.Value;
                int secId = getRow.SectionId.Value;
                int subId = getRow.SubjectId.Value;
                var empRegid = getRow.EmployeeRegisterId.Value;
                bool existacademicyear = acyearServ.PastAcademicyear(yearId);
                if (existacademicyear == true)
                {
                    SubjectServices sub = new SubjectServices();
                    var getRow3 = sub.getSubjectBySubjetID(subId);
                    var subject = getRow3.Subject;

                    AssignSubjectToSectionServices secSub = new AssignSubjectToSectionServices();
                    var getRow1 = secSub.checkSubjectExist(yearId, classId, secId, subId);
                    var subStatus = getRow1.Status;
                    string subStatus1 = "";
                    if (subStatus == true)
                    {
                        subStatus1 = "true";
                    }

                    AssignClassTeacherServices clasTeacher = new AssignClassTeacherServices();
                    var getRow2 = clasTeacher.IsClassTeacher(yearId, classId, secId, empRegid, subId);
                    var IsCI = getRow2.IsClassTeacher;

                    return Json(new { subject, empRegid, IsCI, subStatus1 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string info = "Cannot_edit_past_acamedicYear_records";
                    return Json(new { info }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult EditSubjectWithFaculty(EditSubjectWithFaculty e)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int eid = Convert.ToInt32(e.SubjectWithFacultyId);
                    AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
                    var getRow = facultySub.checkFacultyAssigned(eid);
                    int yearId = getRow.AcademicYearId.Value;
                    int classId = getRow.ClassId.Value;
                    int secId = getRow.SectionId.Value;
                    int subId = getRow.SubjectId.Value;
                    int facultyId = getRow.EmployeeRegisterId.Value;

                    string StatusSuccessMsg = "";
                    string info_Message = "";
                    string ClassInchargeErrorMessage = "";
                    string ClassInchargeSuccessMessage = "";
                    string FacultySuccessMessage = "";



                    // update subject status
                    AssignSubjectToSectionServices secSub = new AssignSubjectToSectionServices();
                    if (e.status == true)
                    {
                        //AssignSubjectToSections
                        secSub.updateSectionSubjectStatus(yearId, classId, secId, subId, e.status);
                        StatusSuccessMsg = ResourceCache.Localize("StatusUpdatedSuccessfully");
                    }
                    else
                    {
                        secSub.updateSectionSubjectStatus(yearId, classId, secId, subId, e.status);
                        // check timetable created
                        AssignSubjectToPeriodServices period = new AssignSubjectToPeriodServices();
                        var checkTimetableCreated = period.checkDataAlreadyExist(yearId, classId, secId);
                        if (checkTimetableCreated != null)
                        {
                            StatusSuccessMsg = ResourceCache.Localize("StatusUpdatedSuccessfully");
                            info_Message = ResourceCache.Localize("Info:Please_check_timetable_management_module");
                        }
                    }

                    // update Class Incharge
                    AssignClassTeacherServices clasTeacher = new AssignClassTeacherServices();
                    if (e.Edit_CI_SubjectWithFaculty == "true")
                    {
                        var CheckRecordExist = clasTeacher.checkRecord(yearId, classId, secId, "Yes");
                        if (CheckRecordExist == null)
                        {
                            clasTeacher.UpdateClassTeacher(yearId, classId, secId, facultyId, e.Edit_AssignFaculty, "Yes", subId);
                            ClassInchargeSuccessMessage = ResourceCache.Localize("Class_Incharge_updated");
                        }
                        else
                        {
                            ClassInchargeErrorMessage = ResourceCache.Localize("Class_incharge_already_exist");
                        }
                    }
                    else
                    {
                        clasTeacher.UpdateClassTeacher(yearId, classId, secId, facultyId, e.Edit_AssignFaculty, "No", subId);
                        ClassInchargeSuccessMessage = ResourceCache.Localize("Class_Incharge_updated");
                    }

                    // update AssignFacultyToSubjects
                    facultySub.updateFaculty1(yearId, classId, secId, subId, e.Edit_AssignFaculty);
                    // update AssignClassTeachers
                    clasTeacher.updateFaculty(yearId, classId, secId, subId, e.Edit_AssignFaculty);
                    FacultySuccessMessage = ResourceCache.Localize("Faculty_updated");
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Update_subject_with_faculty_class_Incharge"));
                    // Message = ClassInchargeSuccessMessage + info_Message;
                    return Json(new { StatusSuccessMsg, info_Message, ClassInchargeSuccessMessage, ClassInchargeErrorMessage, FacultySuccessMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
