﻿using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using eAcademy.ViewModel;
using System.Net;
using eAcademy.Models;
using eAcademy.Helpers;
using eAcademy.HelperClass;

namespace eAcademy.Controllers
{
    [Student_Parent_SessionAttribute]
    public class ModulesController : Controller
    {
        //
        // GET: /Modules/

        StudentFacultyFeedback_service fb = new StudentFacultyFeedback_service();
        AssignClassToStudentServices clas = new AssignClassToStudentServices();
        AssignSubjectToSectionServices sub = new AssignSubjectToSectionServices();
        AssignFacultyToSubjectServices faculSub = new AssignFacultyToSubjectServices();
        HomeworkServices HW = new HomeworkServices();
        AssignmentServices Assgn = new AssignmentServices();
        SubjectnotesServices SubNote = new SubjectnotesServices();
        StudentfacultyquriesServices fQuery = new StudentfacultyquriesServices();
        

        public int StudentRegId;
        public int yearId;
        public int classId;
        public int secId;

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            StudentRegId = Convert.ToInt32(Session["StudentRegId"]);
            var getRow = clas.getAssigendClass(StudentRegId);
            if (getRow != null)
            {
                yearId = Convert.ToInt32(getRow.AcademicYearId);
                classId = Convert.ToInt32(getRow.ClassId);
                secId = Convert.ToInt32(getRow.SectionId);
                ViewBag.AllSubjects1 = sub.ShowSubjects(yearId, classId, secId);
            }
            AcademicyearServices AcYear = new AcademicyearServices();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
        }

        public ActionResult SubjectNotes()
        {
            return View();
        }

        public JsonResult NotesListResults(string sidx, string sord, int page, int rows, int? SM_AllSubjects, DateTime? SM_Notes_FromDate, DateTime? SM_Notes_ToDate)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            IList<SubjectNotes> NotesList = new List<SubjectNotes>();
            if (SM_AllSubjects == 0 && SM_Notes_FromDate != null && SM_Notes_ToDate != null)
            {
                NotesList = SubNote.getStudentNotes(yearId, classId, secId, SM_Notes_FromDate.Value, SM_Notes_ToDate.Value);

            }
            if (SM_AllSubjects != 0 && SM_Notes_FromDate != null && SM_Notes_ToDate != null)
            {
                NotesList = SubNote.getStudentNotes(yearId, classId, secId, SM_AllSubjects.Value, SM_Notes_FromDate.Value, SM_Notes_ToDate.Value);
            }
            int totalRecords = NotesList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    NotesList = NotesList.OrderByDescending(s => s.Subject).ToList();
                    NotesList = NotesList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    NotesList = NotesList.OrderBy(s => s.Subject).ToList();
                    NotesList = NotesList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = NotesList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = NotesList
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CurrentActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");


            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    int acid = CurrentAcYear[i].AcademicYearId;
                    return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Notes_ExportToExcel()
        {
            try
            {
                List<SubjectNotes> NotesList = (List<SubjectNotes>)Session["JQGridList"];
                var list = NotesList.Select(o => new { Subject = o.Subject, NotesPosted_Date = o.NotesPosted_Date, Topic = o.Topic, Descriptions = o.Descriptions }).ToList();
                string fileName = ResourceCache.Localize("Notes_List");
                GenerateExcel.ExportExcel(list, fileName);
                return View("SubjectNotes");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Notes_ExportToPdf()
        {
            try
            {
                List<SubjectNotes> NotesList = (List<SubjectNotes>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("NotesList");
                GeneratePDF.ExportPDF_Portrait(NotesList, new string[] { "Subject", "NotesPosted_Date", "Topic", "Descriptions" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("NotesList.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public void OpenSubjectNotesFile(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                var getSubjectNotesFile = SubNote.getStudentNotesFile(fid).NotesFileName;
                string file = getSubjectNotesFile;
                if (file != "No file")
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Documents/SubjectNotes/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {
                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("content-length", FileBuffer.Length.ToString());
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
            }
        }

        public JsonResult getDate(int acid)
        {
            try
            {
                AcademicyearServices AcYear = new AcademicyearServices();
                var ans = AcYear.date(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                EventServices holidays = new EventServices();
                var HolidaysList = holidays.getHolidaysList(acid);
                return Json(new { sd = sdate, ed = edate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult StudentRemark()
        {           
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = ResourceCache.Localize("All") , Value = "All" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("ClassRemark"), Value = "ClassRemark" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("ProgressCardRemark"), Value = "ProgressCardRemark" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("ExamSubjectRemark"), Value = "ExamSubjectRemark" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("HomeworkRemark"), Value = "HomeworkRemark" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("AssignmentRemark"), Value = "AssignmentRemark" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("ELearningRemark"), Value = "ELearningRemark" });
            ViewBag.StatusList = li;           
            return View();
        }


        public JsonResult RemarkListResults(string sidx, string sord, int page, int rows, int Remark_allAcademicYears, string Status, DateTime? Remark_fdate, DateTime? Remark_toDate) 
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            StudentDailyAttendanceServices stuAttendance = new StudentDailyAttendanceServices();
            IList<StudentRemark> stuRemarkList = new List<StudentRemark>();
            SchoolSettingServices schoolSetSer = new SchoolSettingServices();
            string Mark;
            var gradeType = schoolSetSer.getSchoolSettings().MarkFormat;
            if (gradeType == "Mark")
            {
                Mark = gradeType;
            }
            else
            {
                Mark = gradeType;
            }

            if (Remark_allAcademicYears != 0 && Status != "" && Remark_fdate != null && Remark_toDate != null)
            {                
                stuRemarkList = stuAttendance.getStudentRemark(Remark_allAcademicYears, classId, secId, StudentRegId, Status, Remark_fdate, Remark_toDate, Mark);
            }

            int totalRecords = stuRemarkList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    stuRemarkList = stuRemarkList.OrderByDescending(s => s.Date).ToList();
                    stuRemarkList = stuRemarkList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    stuRemarkList = stuRemarkList.OrderBy(s => s.Date).ToList();
                    stuRemarkList = stuRemarkList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = stuRemarkList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = stuRemarkList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StudentRemark_ExportToExcel()
        {
            try
            {
                List<StudentRemark> stuRemarkList = (List<StudentRemark>)Session["JQGridList"];
                var list = stuRemarkList.Select(o => new { Date = o.date, CommentFrom = o.CommentFrom, Name = o.Name, Remark = o.Remark }).ToList();
                string fileName = ResourceCache.Localize("Student_Remark");
                GenerateExcel.ExportExcel(list, fileName);
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult StudentRemark_ExportToPdf()
        {
            try
            {
                List<StudentRemark> stuRemarkList = (List<StudentRemark>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("StudentRemark");
                GeneratePDF.ExportPDF_Portrait(stuRemarkList, new string[] { "date", "CommentFrom", "Name", "Remark" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("StudentRemark.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

    }
}
