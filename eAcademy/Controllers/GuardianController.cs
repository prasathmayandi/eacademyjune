﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eAcademy.Controllers
{
    [Guardian_CheckSessionAttribute]
    public class GuardianController : Controller
    {
        //
        // GET: /Guardian/

        public int ParentRegId;
        public string ParentUserName;
        public int StudentRegId;

        UserActivityHelper useractivity = new UserActivityHelper();

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ParentRegId = Convert.ToInt32(Session["ParentRegId"]);
            ParentUserName = Convert.ToString(Session["ParentUserName"]);
            StudentRegId = Convert.ToInt32(Session["StudentRegId"]);
        }

        public ActionResult StudentCredential()
        {
            if (Session["GuardianStudentName"] != null)
            {
               return RedirectToAction("Dashboard", "DashboardParent");
            }
            AdmissionTransactionServices transaction = new AdmissionTransactionServices();
            var StudentList = transaction.GetStudentRegId(ParentRegId);
            int count = StudentList.ToList().Count;
            ViewBag.count = count;
            Session["Guardian_menu"] = "DontShow";
            return View(StudentList);
        }

        [HttpPost]
        public ActionResult StudentCredential(GuardianStudentList m)
        {
            try
            {
                ViewBag.stuReg = m.sel_StudentRegId;
                if (ModelState.IsValid)
                {
                    int StudentRegId = Convert.ToInt32(QSCrypt.Decrypt(m.sel_StudentRegId));
                    StudentServices stu = new StudentServices();
                    var checkValidStudent = stu.CheckGuardianStudent_Credential(StudentRegId, m.txt_StudentId, m.txt_dob);
                    if (checkValidStudent != null)
                    {
                        Session["StudentRegId"] = checkValidStudent.StudentRegisterId;
                        var fname = checkValidStudent.FirstName;
                        var lname = checkValidStudent.LastName;
                        Session["GuardianStudentName"] = fname + " " + lname;
                        StudentRegId = Convert.ToInt32(Session["StudentRegId"]);
                        AssignClassToStudentServices Assclas = new AssignClassToStudentServices();
                        var getRow = Assclas.getCurrentActiveClass(StudentRegId);
                        int classId = getRow.ClassId.Value;
                        int secId = getRow.SectionId.Value;
                        ClassServices clas = new ClassServices();
                        SectionServices sec = new SectionServices();
                        var getClass = clas.getClassNameByClassID(classId).ClassType;
                        var getSection = sec.getSectionName(classId, secId).SectionName;
                        Session["GuardianStudentClass"] = getClass + " " + getSection;
                        Session["Guardian_menu"] = "Show";
                        return RedirectToAction("Dashboard","DashboardParent");
                    }
                    else
                    {
                        ViewBag.StudentId = m.txt_StudentId;
                        ViewBag.DOB = m.txt_dob;
                        ViewBag.ErrorMessage = ResourceCache.Localize("InvalidStudentCredential");
                        AdmissionTransactionServices transaction = new AdmissionTransactionServices();
                        var StudentList = transaction.GetStudentRegId(ParentRegId);
                        int count = StudentList.ToList().Count;
                        ViewBag.count = count;
                        return View(StudentList);
                    }
                }
                else
                {
                    ViewBag.StudentId = m.StudentId;
                    if (m.DOB != null)
                    {
                        ViewBag.DOB = m.DOB.ToString("dd/MM/yyyy");
                    }
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }


        public ActionResult ChangeStudent()
        {
            Session["StudentRegId"] = null;
            Session["GuardianStudentName"] = null;
            Session["Guardian_menu"] = "DontShow";
            return RedirectToAction("StudentCredential");
        }


    }
}
