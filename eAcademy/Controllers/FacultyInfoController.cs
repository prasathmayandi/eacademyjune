﻿using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class FacultyInfoController : Controller
    {
        //
        // GET: /Faculty/
        UserActivityHelper useractivity = new UserActivityHelper();

        public int FacultyId;

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            FacultyId = Convert.ToInt32(Session["empRegId"]);
            AcademicyearServices AcYear = new AcademicyearServices();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            SchoolSettingsServices sett = new SchoolSettingsServices();
            var getSchoolSettings = sett.SchoolSettings();
            if (getSchoolSettings != null)
            {
                int dayCount = getSchoolSettings.DayOrder.Value;
                ViewBag.NumberOfDays = dayCount;
            }

            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = ResourceCache.Localize("All"), Value = "All" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("ClassRemark"), Value = "ClassRemark" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("ProgressCardRemark"), Value = "ProgressCardRemark" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("ExamSubjectRemark"), Value = "ExamSubjectRemark" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("HomeworkRemark"), Value = "HomeworkRemark" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("AssignmentRemark"), Value = "AssignmentRemark" });
            li.Add(new SelectListItem { Text = ResourceCache.Localize("ELearningRemark"), Value = "ELearningRemark" });
            ViewBag.StatusList = li;
        }

        public ActionResult ClassAssigned()
        {         
            return View();
        }

        [HttpPost]
        public ActionResult ClassAssigned(FI_ClassAssigned ca)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int yearId = ca.Fac_allAcademicYears;
                    AssignClassTeacherServices clasTeacher = new AssignClassTeacherServices();
                    var list = clasTeacher.checkClassAssigned_ClassTeacher(FacultyId, yearId);
                    if (list != null)
                    {
                        var list1 = clasTeacher.getClassAssigned(yearId, FacultyId);
                        ViewBag.list = list1;
                        return View(list1);
                    }
                    else
                    {
                        ViewBag.NoRecords = ResourceCache.Localize("NoRecordsFound");
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult ModulesAssigned()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ModulesAssigned(FI_ClassAssigned ca)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int yearId = ca.Fac_allAcademicYears;
                    AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
                    var list = facultySub.getModulesAssigned(yearId, FacultyId);
                    ViewBag.moduleCount = list.Count;
                    return View(list);
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult ClassTimetable()
        {
            return View();
        }

        public JsonResult getFacultyPeriodSubject(int passYearId)
        {
            int day = 0;
            int period = 0;
            int passFacultyId = FacultyId;
            SchoolSettingsServices sett = new SchoolSettingsServices();
            var settingsExist = sett.SchoolSettings();
            if (settingsExist != null)
            {
                day = settingsExist.DayOrder.Value;
                TimeScheduleServices tschedule = new TimeScheduleServices();
                int count = tschedule.GetMaximumPeriod().Value;
                period = count;

                AssignSubjectToPeriodServices SubjectPeriod = new AssignSubjectToPeriodServices();
                var FacultyPeriodSubject = SubjectPeriod.getFacultyPeriodSubject(passYearId, passFacultyId);
                return Json(new { day, period, FacultyPeriodSubject }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { day, period }, JsonRequestBehavior.AllowGet);
        }

        // student Remark
        public JsonResult getClassWithSection(int passYearId)
        {
            AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
            var list = facultySub.getClassWithSection(FacultyId, passYearId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudents(int passYearId, string passClassSecId)
        {
            try
            {
                string clasSec = passClassSecId;
                string[] words = clasSec.Split(' ');
                int classId = Convert.ToInt32(words[0]);
                int secId = Convert.ToInt32(words[1]);
                AssignClassToStudentServices stu = new AssignClassToStudentServices();
                var StudentList = stu.getStudents1(passYearId, classId, secId);
                return Json(StudentList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult StudentRemark()
        {
            return View();
        }

       

        public JsonResult RemarkListResults(string sidx, string sord, int page, int rows, int Remark_allAcademicYears, string Remark_ClassWithSection, string Remark_Student, string Status, DateTime? Remark_fdate, DateTime? Remark_toDate) //
        {
          IList<StudentRemark> stuRemarkList = new List<StudentRemark>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            StudentDailyAttendanceServices stuAttendance = new StudentDailyAttendanceServices();

            SchoolSettingServices schoolSetSer = new SchoolSettingServices();
            string Mark;
            var gradeType = schoolSetSer.getSchoolSettings().MarkFormat;
            if (gradeType == "Mark")
            {
                Mark = gradeType;
            }
            else
            {
                Mark = gradeType;
            }

            if (Remark_allAcademicYears != 0 && Remark_ClassWithSection != "" && Remark_Student != "" && Status != "" && Remark_fdate != null && Remark_toDate != null)
            {
                string clasSec = Remark_ClassWithSection;
                string[] words = clasSec.Split(' ');
                int classId = Convert.ToInt32(words[0]);
                int secId = Convert.ToInt32(words[1]);
                int studentRegId = Convert.ToInt32(Remark_Student);
                stuRemarkList = stuAttendance.getStudentRemark(Remark_allAcademicYears, classId, secId, studentRegId, Status, Remark_fdate, Remark_toDate, Mark);
            }

            int totalRecords = stuRemarkList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    stuRemarkList = stuRemarkList.OrderByDescending(s => s.Date).ToList();
                    stuRemarkList = stuRemarkList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    stuRemarkList = stuRemarkList.OrderBy(s => s.Date).ToList();
                    stuRemarkList = stuRemarkList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = stuRemarkList;
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = stuRemarkList
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StudentRemark_ExportToExcel()
        {
            try
            {
                List<StudentRemark> stuRemarkList = (List<StudentRemark>)Session["JQGridList"];
                var list = stuRemarkList.Select(o => new { Date = o.date, CommentFrom = o.CommentFrom, Name = o.Name, Remark = o.Remark }).ToList();
                string fileName = ResourceCache.Localize("StudentRemarkXlsx");
                GenerateExcel.ExportExcel(list, fileName);
                return View("StudentRemark");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public JsonResult getDate(int acid)
        {
            try
            {
                List<StudentRemark> stuRemarkList = (List<StudentRemark>)Session["JQGridList"];
                AcademicyearServices obj_academicYear = new AcademicyearServices();
                var ans = obj_academicYear.AcademicYears(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                return Json(new { sd = sdate, ed = edate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult StudentRemark_ExportToPdf()
        {
            try
            {
                List<StudentRemark> stuRemarkList = (List<StudentRemark>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("StudentRemark");
                GeneratePDF.ExportPDF_Portrait(stuRemarkList, new string[] { "Date", "CommentFrom", "Name", "Remark" }, xfilePath, heading);
                return File(xfilePath, "application/pdf",  ResourceCache.Localize("StudentRemark.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

    }
}
