﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class UserConfigurationController : Controller
    {
        RoleServices obj_role = new RoleServices();
        FeatureServices obj_feature = new FeatureServices();
        MapRoleFeatureServices obj_mapRole = new MapRoleFeatureServices();
        MapRoleUserServices obj_mapUser = new MapRoleUserServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ViewBag.RoleId = obj_role.getRole();
            ViewBag.FeatureId = obj_feature.getFeature();
            ViewBag.RoleId1 = obj_role.getallRole();
        }
        public ActionResult CreateRole()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
                return View();
            }
        }
       
        public JsonResult RoleResults(string sidx, string sord, int page, int rows, int? txtRoleType, Boolean? txtRoleStatus)    //, string Achievement, DateTime? txt_doa_search
        {
              IList<RoleList> todoListsResults1 = new List<RoleList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults1 = obj_role.getRoles();
            if (txtRoleType.HasValue && txtRoleStatus.HasValue)
            {
                todoListsResults1 = todoListsResults1.Where(p => p.RoleId == txtRoleType && p.Status == txtRoleStatus).ToList();
            }
            else if (txtRoleType.HasValue)
            {
                todoListsResults1 = todoListsResults1.Where(p => p.RoleId == txtRoleType).ToList();
            }
            else if (txtRoleStatus.HasValue)
            {
                todoListsResults1 = todoListsResults1.Where(p => p.Status == txtRoleStatus).ToList();
            }
            else
            {
            }
            int totalRecords = todoListsResults1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults1 = todoListsResults1.OrderByDescending(s => s.Role).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults1 = todoListsResults1.OrderBy(s => s.Role).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults1;
           
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RoleExportToExcel()
        {
            try
            {
                List<RoleList> todoListsResults1 = (List<RoleList>)Session["JQGridList"];
                var list = todoListsResults1.Select(o => new { Role = o.Role, Status = Convert.ToString(o.Status.Value) }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("Roles");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult RoleExportToPDF()
        {
            try
            {
                List<RoleList> todoListsResults1 = (List<RoleList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("Roles");
                GeneratePDF.ExportPDF_Portrait(todoListsResults1, new string[] { "Role", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "Role.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public JsonResult GetFeatureslist(int roles)
        {
            try
            {
                MapRoleFeatureServices cls = new MapRoleFeatureServices();
                var records = cls.GetRemainFeaturelist(roles);
                return Json(records, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(e.Message.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult CreateRole(Roles model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string rname = model.role;
                    bool cc = obj_role.checkRole(rname);
                    if (cc == true)
                    {
                        string ErrorMessage =eAcademy.Models.ResourceCache.Localize("Role_is_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_role.addRole(rname);
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Create_role"));
                        string Message = eAcademy.Models.ResourceCache.Localize("Role_Added_Successfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditRole(string rid)
        {
            try
            {
                int roleid = Convert.ToInt32(QSCrypt.Decrypt(rid));
                var ans = obj_role.getRole(roleid);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditRole(UpdateRole model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int rid = Convert.ToInt32(QSCrypt.Decrypt(model.rid));
                    string rolename = model.role;
                    string status = model.status;
                    bool cc = obj_role.checkRole(rolename, status);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Role_is_already_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                       
                        obj_role.updateRole(rid, rolename, status);
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_role"));
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        [EncryptedActionParameter]
        public ActionResult DeleteRole(int rid)
        {
            try
            {
                obj_role.deleteRole(rid);
                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Delete role"));
                return Json(new { ans = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UserLog()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public static IList<UserLogList> todoListsResults2 = new List<UserLogList>();
        public JsonResult UserResults(string sidx, string sord, int page, int rows, DateTime? Fromdate, DateTime? Todate, string Source)
        {
            UserLogServices obj_userLog = new UserLogServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults2 = obj_userLog.getUserlog();
            if (Fromdate.HasValue && Todate.HasValue && Source != "")
            {
                todoListsResults2 = obj_userLog.getUserlogDate(Fromdate, Todate);
                todoListsResults2 = todoListsResults2.Where(q => q.LoginSource == Source).ToList();
                //todoListsResults2 = todoListsResults2.Where(q=>q.Login >=Fromdate && q.logout<= Todate && q.LoginSource == Source).ToList();
            }
            int totalRecords = todoListsResults2.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults2
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UserlogExportToExcel()
        {
            try
            {
                var list = todoListsResults2.Select(o => new { UserRegisterId = o.UserRegId, UserType = o.UserType, IpAddress = o.IpAddress, LoginTime = o.LoginTime, LogoutTime = o.LogoutTime, LoginSource = o.LoginSource }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("UserLogDetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult UserlogExportToPDF()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = eAcademy.Models.ResourceCache.Localize("UserLogDetails");
                GeneratePDF.ExportPDF_Portrait(todoListsResults2, new string[] { "UserRegId", "UserType", "IpAddress", "LoginTime", "LogoutTime", "LoginSource" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", "UserLog.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult UserActivity(string id)
        {
            UserLogServices obj_userLog = new UserLogServices();
            string[] c_id = id.Split('/');
            int userid = Convert.ToInt16(c_id[0]);
            string type = c_id[1];
            var list = obj_userLog.getUserlogActivity(userid, type);
            ViewBag.count = list.Count;
            return View(list);
        }
        public ActionResult MapRole()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        static IList<MapFeatureRole> todoListsResults3 = new List<MapFeatureRole>();
        public JsonResult MapRoleResults(string sidx, string sord, int page, int rows, int? txtRoleType)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults3 = obj_mapRole.getMapRole();
            if (txtRoleType.HasValue)
            {
                todoListsResults3 = todoListsResults3.Where(p => p.RoleId == txtRoleType).ToList();
            }
            int totalRecords = todoListsResults3.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults3 = todoListsResults3.OrderByDescending(s => s.FeatureType).ToList();
                    todoListsResults3 = todoListsResults3.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults3 = todoListsResults3.OrderBy(s => s.FeatureType).ToList();
                    todoListsResults3 = todoListsResults3.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults3
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MapRoleExportToExcel()
        {
            try
            {
                var list = todoListsResults3.Select(o => new { Feature = o.FeatureType, Role = o.RoleType, Status = Convert.ToString(o.Status) }).ToList();//, FeatureDetails = o.FeatureDetails
                string heading = eAcademy.Models.ResourceCache.Localize("MapRoleDetails");
                GenerateExcel.ExportExcel(list, heading);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult MapRoleExportToPDF()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = eAcademy.Models.ResourceCache.Localize("MapRoleDetails");
                GeneratePDF.ExportPDF_Portrait(todoListsResults3, new string[] { "FeatureType", "RoleType", "Status" }, xfilePath, heading);//, "FeatureDetails"
                return File(xfilePath, "application/pdf", "MapRolewithFeature.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult MapRole(SaveMapFeatureRole model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int rid = model.RoleId;
                    string[] f_id = model.FeatureId.Split(',');
                    string Message = "";
                    for (int a = 0; a < f_id.Length; a++)
                    {
                        int fid = Convert.ToInt32(f_id[a]);
                        bool cc = obj_mapRole.checkMapRoleFeatures(rid, fid);
                        if (cc == true)
                        {
                            string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Feature_is_already_Exist_for_this_Role");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            obj_mapRole.addMapRoleFeatures(rid, fid);
                            useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_features"));                           
                        }
                    }
                     Message = eAcademy.Models.ResourceCache.Localize("Feature_Added_Successfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditMapRole(string mapId)
        {
            try
            {
                int Mid = Convert.ToInt32(QSCrypt.Decrypt(mapId));
                var ans = obj_mapRole.getMapFeature(Mid);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditMapRole(EditMapFeatureRole model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int mapId = Convert.ToInt32(QSCrypt.Decrypt(model.MRid));
                    int rid = model.RoleId;
                    int fid = model.FeatureId;
                    string status = model.status;
                    bool cc = obj_mapRole.checkMapRoleFeatures(rid, fid, status);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Feature_is_already_Exist_for_this_Role");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_mapRole.updateMapRole(mapId, status);
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_features"));
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult MapUser()
        {
            try
            {
                TechEmployeeServices obj_employee = new TechEmployeeServices();
                ViewBag.UserId = obj_employee.getTechEmployee();
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        static IList<MapUserRole> todoListsResults4 = new List<MapUserRole>();
        public JsonResult MapUserResults(string sidx, string sord, int page, int rows, int? txtRoleType)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults4 = obj_mapUser.getMapUser();
            if (txtRoleType.HasValue)
            {
                todoListsResults4 = todoListsResults4.Where(p => p.RoleId == txtRoleType).ToList();
            }
            int totalRecords = todoListsResults4.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults4 = todoListsResults4.OrderByDescending(s => s.UserName).ToList();
                    todoListsResults4 = todoListsResults4.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults4 = todoListsResults4.OrderBy(s => s.UserName).ToList();
                    todoListsResults4 = todoListsResults4.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults4
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MapRoleUserExportToExcel()
        {
            try
            {
                var list = todoListsResults4.Select(o => new { Role = o.RoleType, User = o.UserName, Status = Convert.ToString(o.Status) }).ToList();
                string heading = eAcademy.Models.ResourceCache.Localize("MapRoleUserDetails");
                GenerateExcel.ExportExcel(list, heading);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult MapRoleUserExportToPDF()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = eAcademy.Models.ResourceCache.Localize("MapRoleUserDetails");
                GeneratePDF.ExportPDF_Portrait(todoListsResults4, new string[] { "RoleType", "UserName", "Status" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", "MapRolewithUser.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult MapUser(MapUserRole model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int rid = model.RoleId;
                    int uid = model.UserId;
                    bool cc = obj_mapUser.checkMapRoleUser(rid, uid);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Role_is_already_Exist_for_this_User");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_mapUser.addMapRoleUser(rid, uid);
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_role_to_user"));
                        string Message = eAcademy.Models.ResourceCache.Localize("Role_Added_Successfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditMapUser(string mapId)
        {
            try
            {
                int mId = Convert.ToInt32(QSCrypt.Decrypt(mapId));
                var ans = obj_mapUser.getMapFeature(mId);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditMapUser(MapUserRole model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int mapId = Convert.ToInt32(QSCrypt.Decrypt(model.MapRoleId));
                    int rid = model.RoleId;
                    int uid = model.UserId;
                    string status = model.Status1;
                    bool cc = obj_mapUser.checkMapRoleUser(rid, uid, status);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Role_is_already_Exist_for_this_User");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        
                        obj_mapUser.updateMapRoleUser(mapId, status);                        
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_role_to_user"));
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}