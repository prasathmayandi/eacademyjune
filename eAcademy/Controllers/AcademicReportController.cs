﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using iTextSharp.text;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace eAcademy.Controllers
{

    [CheckSessionOutAttribute]

    public class AcademicReportController : Controller
    {
        // GET: /AcademicReport/
        //BaseController
        Data db = new Data();
        EacademyEntities ee = new EacademyEntities();
        public static IList<EventList> funtionlist = new List<EventList>();
        public static IList<extracurricular> facilitieslist = new List<extracurricular>();
        public static IList<extracurricular> Extracuricularlist = new List<extracurricular>();
        public static IList<HostalList> Hostellist = new List<HostalList>();
        public static IList<TransportList> Transportlist = new List<TransportList>();
        public int y;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {

            ReportHelper RptHelp = new ReportHelper();
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            ExamServices Es = new ExamServices();
            MonthsServices Ms = new MonthsServices();
            FeeCategoryServices FCS = new FeeCategoryServices();
            FeeCollectionServices FeeCollectionService = new FeeCollectionServices();
            AssignClassToStudentServices Acs = new AssignClassToStudentServices();
            TermServices Ts = new TermServices();
            List<SelectListItem> Sort = new List<SelectListItem>();
            Sort.Add(new SelectListItem { Text = "Select Status", Value = "" });
            Sort.Add(new SelectListItem { Text = "Both", Value = "All" });
            Sort.Add(new SelectListItem { Text = "Pass", Value = "Pass" });
            Sort.Add(new SelectListItem { Text = "Fail", Value = "fail" });
            ViewBag.SortList = Sort;
            ViewBag.academicyear = As.GetAcademicYear();
            ViewBag.Classes = Cs.GetClass();
            ViewBag.reporttype = getrtype();
            ViewBag.month = Ms.GetMonths();
            ViewBag.FeePaticular = getFeeParticulars();
            ViewBag.ExamType = Es.GetExamName();
            ViewBag.exam = Es.GetExamName();
            ViewBag.paytype = getPaymentType();
            ViewBag.date = DateTimeByZone.getCurrentDateTime();
            ViewBag.ClassList = Cs.Classes();
            ViewBag.feeCategory = FCS.getFeeCategory();
        }
        public IEnumerable getPaymentType()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select", Value = "" });
            li.Add(new SelectListItem { Text = "Single", Value = "1" });
            li.Add(new SelectListItem { Text = "Term", Value = "2" });
            return li;
        }
        public IEnumerable getrtype()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select", Value = "" });
            li.Add(new SelectListItem { Text = "Daily", Value = "1" });
            li.Add(new SelectListItem { Text = "From/To", Value = "2" });
            li.Add(new SelectListItem { Text = "Monthly", Value = "3" });
            return li;
        }
        public IEnumerable getFeeParticulars()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select", Value = "" });
            li.Add(new SelectListItem { Text = "Paid", Value = "1" });
            li.Add(new SelectListItem { Text = "Due", Value = "2" });
            return li;
        }
        public JsonResult getDate(int acid)
        {
            try
            {
                AcademicyearServices ays = new AcademicyearServices();
                var ans = ays.AcademicYears(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                return Json(new { sd = sdate, ed = edate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ChangeCurrentCulture(int id)
        {
            CultureHelper.CurrentCulture = id;
            Session["CurrentCulture"] = id;
            return Redirect(Request.UrlReferrer.ToString());
        }
        public ActionResult FunctionTimeTable()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult FunctionTimeTable(Mandatory m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AcademicyearServices As = new AcademicyearServices();
                    EventServices Es = new EventServices();
                    ViewBag.academicyear = As.GetAcademicYear();
                    var id = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = id;
                    ViewBag.yy = As.GetParticularYear(id);
                    var ans1 = Es.GetEvents(id);
                    TempData["ans"] = ans1;
                    ViewBag.view = ans1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        //jqrid for funtiontable
        public JsonResult JQgridFuntionTimeTable(string sidx, string sord, int page, int rows, int AcademicYear)
        {
            EventServices Es = new EventServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            //  List<EventList> todoListsResults = null; //Cs.GetAllClassandSection();
            funtionlist = Es.GetEvents(1);
            y = AcademicYear;
            if (AcademicYear != null)
            {
                funtionlist = Es.GetEvents(AcademicYear);
            }
            int totalRecords = funtionlist.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    funtionlist = funtionlist.OrderByDescending(s => s.Date).ToList();
                    funtionlist = funtionlist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    funtionlist = funtionlist.OrderBy(s => s.Date).ToList();
                    funtionlist = funtionlist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = funtionlist
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        //json for Function TimeTable
        [HttpPost]
        public JsonResult GetFunctionTimeTable(Rpt_FunctionTimeTable d)
        {
            try
            {
                var rr = new Rpt_FunctionTimeTable();
                List<EventList> aa = null;
                var valid = TryUpdateModel(rr);
                if (valid)
                {
                    EventServices Es = new EventServices();
                    var id = Convert.ToInt32(d.AcademicYear);
                    var ans1 = Es.GetEvents(d.AcademicYear);
                    aa = ans1;
                    return Json(aa, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Valid = valid, Errors = GetErrorsFromModelState(), });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult FunctionListReport_ExportToExcel()
        {
            try
            {
                var list = funtionlist.Select(o => new { EventDates = o.EventDates, EventName = o.EventName, EventDecribtion = o.EventDecribtion }).ToList();
                string fileName =  eAcademy.Models.ResourceCache.Localize("FunctionList");
                GenerateExcel.ExportExcel(list, fileName);
                return View("FunctionTimeTable");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult FunctionListReport_ExportToPdf()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = eAcademy.Models.ResourceCache.Localize("FunctionList");
                GeneratePDF.ExportPDF_Portrait(funtionlist, new string[] { "EventDates", "EventName", "EventDecribtion" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", "FunctionList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        //Fees 
        public ActionResult Fee()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Fee(Mandatory m, FormCollection fm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ReportHelper RptHelp = new ReportHelper();
                    AcademicyearServices As = new AcademicyearServices();
                    ClassServices Cs = new ClassServices();
                    ExamServices Es = new ExamServices();
                    ArrayList s1 = new ArrayList();
                    SectionServices Ss = new SectionServices();
                    FeeCategoryServices FCS = new FeeCategoryServices();
                    FeeCollectionServices FeeCollectionService = new FeeCollectionServices();
                    AssignClassToStudentServices Acs = new AssignClassToStudentServices();
                    StudentServices Stu = new StudentServices();
                    TermServices Ts = new TermServices();
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.Classes = Cs.GetClass();
                    ViewBag.ExamType = Es.GetExamName();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    var cid = Convert.ToInt32(m.Class);
                    ViewBag.cls = cid;
                    var sid = Convert.ToInt32(m.Rpt_Section);
                    ViewBag.section = Ss.GetSection(cid);
                    ViewBag.sec = sid;
                    var payid = Convert.ToInt32(m.PaymentType);
                    ViewBag.p = payid;
                    ViewBag.paytype = getPaymentType();
                    var fid = Convert.ToInt32(m.Feecategory);
                    ViewBag.FeePaticular = getFeeParticulars();
                    if (payid == 1)
                    {
                        var fe = FCS.GetAnnualFeeCategory(yid, cid, payid);
                        ViewBag.fee = fe;
                        ViewBag.f = fid;
                    }
                    else if (payid == 2)
                    {
                        var fe = FCS.GetTermFeeCategory(yid, cid, payid);
                        ViewBag.fee = fe;
                        ViewBag.f = fid;
                    }
                    var Listid = Convert.ToInt32(m.FeeParticular);
                    if (payid == 1 && Listid == 1)
                    {
                        TempData["type"] = "paid";
                        ViewBag.pp = "Annual";
                        var ans1 = FeeCollectionService.GetAnnualPaidCollection(yid, cid, sid, fid);
                        TempData["ans"] = ans1;
                        ViewBag.view = ans1.Count;
                    }
                    if (payid == 1 && Listid == 2)
                    {
                        TempData["type"] = "due";
                        ViewBag.pp = "Annual";
                        var ans1 = FeeCollectionService.GetAnnualDueCollection(yid, cid, sid, fid);
                        foreach (var v in ans1)
                        {
                            s1.Add(v.StudentRegisterId);
                        }
                        var a1 = Acs.GetClassStudent(yid, cid, sid);
                        for (int i = 0; i < s1.Count; i++)
                        {
                            string a = s1[i].ToString();
                            int s = Convert.ToInt32(a.ToString());
                            var ab = Stu.GetAnnualDuestudentName(s, a1);
                            a1 = ab;
                        }
                        var an = Stu.GetAnnualStudentNameRollNumber(yid, cid, sid, a1);
                        TempData["ans"] = an;
                        ViewBag.view = an.Count;
                    }
                    if (payid == 2 && Listid == 1)
                    {
                        TempData["type"] = "paid";
                        ViewBag.pp = "Term";
                        var tid = Convert.ToInt32(fm["Termtype"]);
                        ViewBag.termid = Ts.GetParticularTerm(tid);
                        ViewBag.term = Ts.GetTermList(yid);
                        ViewBag.t = tid;
                        var ans1 = FeeCollectionService.GetTermPaidCollection(yid, cid, sid, tid, fid);
                        TempData["ans"] = ans1;
                        ViewBag.view = ans1.Count;
                    }
                    if (payid == 2 && Listid == 2)
                    {
                        TempData["type"] = "due";
                        ViewBag.pp = "Term";
                        var tid = Convert.ToInt32(fm["Termtype"]);
                        ViewBag.termid = Ts.GetParticularTerm(tid);
                        ViewBag.term = Ts.GetTermList(yid);
                        ViewBag.t = tid;
                        var ans1 = FeeCollectionService.GetTermDueCollection(yid, cid, sid, fid, tid);
                        foreach (var v in ans1)
                        {
                            s1.Add(v.StudentRegisterId);
                        }
                        var a1 = Acs.GetClassStudent(yid, cid, sid);
                        for (int i = 0; i < s1.Count; i++)
                        {
                            string a = s1[i].ToString();
                            int s = Convert.ToInt32(a.ToString());
                            var ab = Stu.GetTermDuestudentName(s, a1);
                            a1 = ab;
                        }
                        var an = Stu.GetTermStudentNameRollNumber(yid, cid, sid, a1);
                        TempData["ans"] = an;
                        ViewBag.view = an.Count;
                    }
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(cid);
                    ViewBag.ss = Ss.GetParticularSection(sid);
                    ViewBag.ff = FCS.GetParticularFee(fid);
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        public JsonResult GetStudentFeelist(int aid, int cid, int sid, int payid, int feeid, int req)
        {
            try
            {
                FeeCollectionCategoryServices fee = new FeeCollectionCategoryServices();
                TermServices tt = new TermServices();
                FeeServices fs=new FeeServices();
                int temp = payid;
                var clssfeedetails = fs.getFeeByID(aid,feeid,cid);
                if (clssfeedetails == null)
                {
                    string Message = eAcademy.Models.ResourceCache.Localize("NoRecords");
                    return Json(new { Success = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    if (req == 1)
                    {
                        var result = fee.GetParticularClassSectionPeesPaidList(aid, cid, sid, payid, feeid, temp);
                        if (result.Count == 0)
                        {
                            string Message = eAcademy.Models.ResourceCache.Localize("NoRecords");
                            return Json(new { Success = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { result = result }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        if (payid == 0)
                        {
                            var dd = tt.GetAcademicterm(aid);
                            payid = dd.TermId;
                        }
                        var result = fee.GetParticularClassSectionPeesDueList(aid, cid, sid, payid, feeid, temp);
                        if (result.Count == 0)
                        {
                            string Message = eAcademy.Models.ResourceCache.Localize("NoRecords");
                            return Json(new { Success = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { result = result }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult JsonFeesDetails(VM_FeesDetails s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    FeeCollectionCategoryServices feeCollect = new FeeCollectionCategoryServices();
                    AssignClassToStudentServices Acs = new AssignClassToStudentServices();
                    StudentServices Stu = new StudentServices();
                    var Acyear = s.AcademicYearId;
                    var classid = s.Class;
                    var section = s.Section;
                    var Feecategotyid = s.FeeCategoryId;
                    var feeParticular = s.FeesParticular;
                    var PaymentType = s.PaymentType;
                    if (PaymentType == 1 && feeParticular == 1)
                    {
                        var list = feeCollect.GetAnnualFeePaidList(Acyear, classid, section, PaymentType, Feecategotyid);
                        return Json(list, JsonRequestBehavior.AllowGet);
                    }
                    else if (PaymentType == 1 && feeParticular == 2)
                    {
                        var list = feeCollect.GetAnnualFeePaidList(Acyear, classid, section, PaymentType, Feecategotyid);
                        int?[] s1 = new int?[list.Count];
                        int k = 0;
                        foreach (var v in list)
                        {
                            s1[k] = v.StudentRegId;
                            k++;
                        }
                        var a1 = Acs.GetClassStudent(Acyear, classid, section);
                        for (int i = 0; i < k; i++)
                        {
                            int? sw = s1[i];
                            var ab = Stu.GetAnnualDuestudentName(sw, a1);
                            a1 = ab;
                        }
                        var an = Stu.GetAnnualStudentNameRollNumber(Acyear, classid, section, a1);
                        return Json(an, JsonRequestBehavior.AllowGet);
                    }
                    else if (PaymentType == 2 && feeParticular == 1)
                    {
                        var TermId = Convert.ToInt16(s.TermType);
                        var list = feeCollect.GetTermFeePaidList(Acyear, classid, section, PaymentType, Feecategotyid, TermId);
                        return Json(list, JsonRequestBehavior.AllowGet);
                    }
                    else if (PaymentType == 2 && feeParticular == 2)
                    {
                        var TermId = Convert.ToInt16(s.TermType);
                        var list = feeCollect.GetTermFeePaidList(Acyear, classid, section, PaymentType, Feecategotyid, TermId);
                        int?[] s1 = new int?[list.Count];
                        int k = 0;
                        foreach (var v in list)
                        {
                            s1[k] = v.StudentRegId;
                            k++;
                        }
                        var a1 = Acs.GetClassStudent(Acyear, classid, section);
                        for (int i = 0; i < k; i++)
                        {
                            int? sw = s1[i];
                            var ab = Stu.GetAnnualDuestudentName(sw, a1);
                            a1 = ab;
                        }
                        var an = Stu.GetAnnualStudentNameRollNumber(Acyear, classid, section, a1);
                        return Json(an, JsonRequestBehavior.AllowGet);
                    }
                    string success = "";
                    return Json(new { Success = success }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        //json for fees
        [HttpPost]
        public ActionResult GetAnnualFees(Rpt_AnnualFees d)
        {
            try
            {
                var rr = new Rpt_AnnualFees();
                var valid = TryUpdateModel(rr);
                List<Ranklist> aa = null;
                if (valid)
                {
                    ArrayList s1 = new ArrayList();
                    FeeCollectionServices FeeCollectionService = new FeeCollectionServices();
                    AssignClassToStudentServices Acs = new AssignClassToStudentServices();
                    StudentServices Stu = new StudentServices();
                    var yid = Convert.ToInt32(d.AcademicYear);
                    var cid = Convert.ToInt32(d.Class);
                    var sid = Convert.ToInt32(d.Rpt_Section);
                    var payid = Convert.ToInt32(d.PaymentType);
                    var fid = Convert.ToInt32(d.Feecategory);
                    var Listid = Convert.ToInt32(d.FeeParticular);
                    if (payid == 1 && Listid == 1)
                    {
                        var ans1 = FeeCollectionService.GetAnnualPaidCollection(yid, cid, sid, fid);
                        aa = ans1;
                    }
                    if (payid == 1 && Listid == 2)
                    {
                        var ans1 = FeeCollectionService.GetAnnualDueCollection(yid, cid, sid, fid);
                        foreach (var v in ans1)
                        {
                            s1.Add(v.StudentRegisterId);
                        }
                        var a1 = Acs.GetClassStudent(yid, cid, sid);
                        for (int i = 0; i < s1.Count; i++)
                        {
                            string a = s1[i].ToString();
                            int s = Convert.ToInt32(a.ToString());
                            var ab = Stu.GetAnnualDuestudentName(s, a1);
                            a1 = ab;
                        }
                        var an = Stu.GetAnnualStudentNameRollNumber(yid, cid, sid, a1);
                        aa = an;
                    }
                    return Json(aa, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Valid = valid, Errors = GetErrorsFromModelState(), });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetTermFees(Rpt_TermFees d)
        {
            try
            {
                var rr = new Rpt_TermFees();
                List<Ranklist> aa = null;
                var valid = TryUpdateModel(rr);
                if (valid)
                {
                    StudentServices Stu = new StudentServices();
                    ArrayList s1 = new ArrayList();
                    FeeCollectionServices FeeCollectionService = new FeeCollectionServices();
                    AssignClassToStudentServices Acs = new AssignClassToStudentServices();
                    var yid = Convert.ToInt32(d.AcademicYear);
                    var cid = Convert.ToInt32(d.Class);
                    var sid = Convert.ToInt32(d.Rpt_Section);
                    var payid = Convert.ToInt32(d.PaymentType);
                    var fid = Convert.ToInt32(d.Feecategory);
                    var id = Convert.ToInt32(d.AcademicYear);
                    var Listid = Convert.ToInt32(d.FeeParticular);
                    if (payid == 2 && Listid == 1)
                    {
                        var tid = Convert.ToInt32(d.Termtype);
                        var ans1 = FeeCollectionService.GetTermPaidCollection(yid, cid, sid, tid, fid);
                        aa = ans1;
                    }
                    if (payid == 2 && Listid == 2)
                    {
                        var tid = Convert.ToInt32(d.Termtype);
                        var ans1 = FeeCollectionService.GetTermDueCollection(yid, cid, sid, fid, tid);
                        foreach (var v in ans1)
                        {
                            s1.Add(v.StudentRegisterId);
                        }
                        var a1 = Acs.GetClassStudent(yid, cid, sid);
                        for (int i = 0; i < s1.Count; i++)
                        {
                            string a = s1[i].ToString();
                            int s = Convert.ToInt32(a.ToString());
                            var ab = Stu.GetTermDuestudentName(s, a1);
                            a1 = ab;
                        }
                        var an = Stu.GetTermStudentNameRollNumber(yid, cid, sid, a1);
                        aa = an;
                    }
                    return Json(aa, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Valid = valid, Errors = GetErrorsFromModelState(), });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        //hostal student 
        public ActionResult HostelStudent(bool? pdf)
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult HostelStudent(Mandatory m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AcademicyearServices As = new AcademicyearServices();
                    HostelStudentServices Hs = new HostelStudentServices();
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    var id = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = id;
                    ViewBag.yy = As.GetParticularYear(id);
                    var a1 = Hs.GetHostelStudent(id);
                    TempData["ans"] = a1;
                    ViewBag.view = a1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        //json for Hostal Student
        [HttpPost]
        public JsonResult GetHostalStudent(Rpt_FunctionTimeTable d)
        {
            try
            {
                var rr = new Rpt_FunctionTimeTable();
                var valid = TryUpdateModel(rr);
                if (valid)
                {
                    HostelStudentServices Hs = new HostelStudentServices();
                    var id = Convert.ToInt32(d.AcademicYear);
                    var a1 = Hs.GetHostelStudent(id);
                    return Json(a1, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Valid = valid, Errors = GetErrorsFromModelState(), });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        //JQgridTable for Hostel
        public JsonResult JQgridhostel(string sidx, string sord, int page, int rows, int AcademicYear)
        {
            HostelStudentServices hs = new HostelStudentServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            y = AcademicYear;
            if (AcademicYear != null)
            {
                Hostellist = hs.GetHostelStudent(AcademicYear);
            }
            int totalRecords = Hostellist.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    Hostellist = Hostellist.OrderByDescending(s => s.RollNumber).ToList();
                    Hostellist = Hostellist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    Hostellist = Hostellist.OrderBy(s => s.RollNumber).ToList();
                    Hostellist = Hostellist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = Hostellist
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult HostalReport_ExportToExcel()
        {
            try
            {
                var list = Hostellist.Select(o => new { StudentId = o.StudentId, RollNumber = o.RollNumber, StudentName = o.StudentName, Class = o.Class, Section = o.Section, BlockName = o.BlockName, RoomNumber = o.RoomNumber, Address = o.Address, StudentContact = o.StudentContact, EmergencyContactPerson = o.EmergencyContactPerson, EmergencyContactPersonNo = o.EmergencyContactPersonNo }).ToList();
                string heading =  eAcademy.Models.ResourceCache.Localize("HostelStudentReport");
                GenerateExcel.ExportExcel(list, heading);
                return View("HostalStudent");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult HostalReport_ExportToPdf()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = eAcademy.Models.ResourceCache.Localize("HostelStudentReport");
                GeneratePDF.ExportPDF_Landscape(Hostellist, new string[] { "StudentId", "RollNumber", "StudentName", "Class", "Section", "BlockName", "RoomNumber", "Address", "StudentContact", "EmergencyContactPerson", "EmergencyContactPersonNo" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", "HostalStudantList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        //Transport Student Details
        public ActionResult TransportStudent(bool? pdf)
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult TransportStudent(Mandatory m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AcademicyearServices As = new AcademicyearServices();
                    TransportDetailServices Ts = new TransportDetailServices();
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    var id = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = id;
                    ViewBag.yy = As.GetParticularYear(id);
                    var a1 = Ts.GetTransportStudent(id);
                    TempData["ans"] = a1;
                    ViewBag.view = a1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        [HttpPost]
        public JsonResult GetTransportStudent(Rpt_FunctionTimeTable d)
        {
            try
            {
                var rr = new Rpt_FunctionTimeTable();
                var valid = TryUpdateModel(rr);
                if (valid)
                {
                    TransportDetailServices Ts = new TransportDetailServices();
                    var id = Convert.ToInt32(d.AcademicYear);
                    var a1 = Ts.GetTransportStudent(id);
                    return Json(a1, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Valid = valid, Errors = GetErrorsFromModelState(), });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        //JQgridTable for Transport
        public JsonResult JQgridTransport(string sidx, string sord, int page, int rows, int AcademicYear)
        {
            TransportDetailServices Ts = new TransportDetailServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            y = AcademicYear;
            if (AcademicYear != null)
            {
                Transportlist = Ts.GetTransportStudent(AcademicYear);
            }
            int totalRecords = Transportlist.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    Transportlist = Transportlist.OrderByDescending(s => s.BusNo).ToList();
                    Transportlist = Transportlist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    Transportlist = Transportlist.OrderBy(s => s.BusNo).ToList();
                    Transportlist = Transportlist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = Transportlist
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TransportReport_ExportToExcel()
        {
            try
            {
                var list = Transportlist.Select(o => new { StudentId = o.StudentId, RollNumber = o.RollNumber, StudentName = o.StudentName, Class = o.Class, Section = o.Section, BusNo = o.BusNo, Address = o.Address, StudentContact = o.StudentContact, EmergencyContactPerson = o.EmergencyContactPerson, EmergencyContactPersonNo = o.EmergencyContactPersonNo }).ToList();
                string heading = eAcademy.Models.ResourceCache.Localize("TransportStudentReport");
                GenerateExcel.ExportExcel(list, heading);
                return View("TransportStudent");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult TransportReport_ExportToPdf()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = eAcademy.Models.ResourceCache.Localize("TransportStudentReport");
                GeneratePDF.ExportPDF_Landscape(Transportlist, new string[] { "StudentId", "RollNumber", "StudentName", "Class", "Section", "BusNo", "Address", "StudentContact", "EmergencyContactPerson", "EmergencyContactPersonNo" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", "TransportStudantList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        //ExtraCuricular Activites
        public ActionResult ExtraCurricular(bool? pdf)
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult ExtraCurricular(Mandatory m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AcademicyearServices As = new AcademicyearServices();
                    ExtraActivitiesServices Es = new ExtraActivitiesServices();
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    var id = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = id;
                    ViewBag.yy = As.GetParticularYear(id);
                    var a1 = Es.GetExtraCurricularIncharge(id);
                    TempData["ans"] = a1;
                    ViewBag.view = a1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }

        //Json for ExtraCurricularIncharge
        [HttpPost]
        public JsonResult GetExtraCurricular(Rpt_FunctionTimeTable d)
        {
            try
            {
                var rr = new Rpt_FunctionTimeTable();
                var valid = TryUpdateModel(rr);
                if (valid)
                {
                    ExtraActivitiesServices Es = new ExtraActivitiesServices();
                    var id = Convert.ToInt32(d.AcademicYear);
                    var a1 = Es.GetExtraCurricularIncharge(id);
                    return Json(a1, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        Valid = valid,
                        Errors = GetErrorsFromModelState(),

                    });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        //JQgridTable for Extra Curicular
        public JsonResult JQgridExtraCuricular(string sidx, string sord, int page, int rows, int AcademicYear)
        {
            ExtraActivitiesServices Es = new ExtraActivitiesServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            y = AcademicYear;
            if (AcademicYear != null)
            {
                Extracuricularlist = Es.GetExtraCurricularIncharge(AcademicYear);
            }
            int totalRecords = facilitieslist.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    Extracuricularlist = Extracuricularlist.OrderByDescending(s => s.ActivityId).ToList();
                    Extracuricularlist = Extracuricularlist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    Extracuricularlist = Extracuricularlist.OrderBy(s => s.ActivityId).ToList();
                    Extracuricularlist = Extracuricularlist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = Extracuricularlist
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExtraCuricularReport_ExportToExcel()
        {
            try
            {
                var list = Extracuricularlist.Select(o => new { ActivityName = o.ActivityName, InchargeId = o.InchargeId, ActivityInchargeName = o.ActivityInchargeName, ContactNumber = o.ContactNumber }).ToList();
                string heading =  eAcademy.Models.ResourceCache.Localize("ExtraCurricularDetailsReport");
                GenerateExcel.ExportExcel(list, heading);
                return View("ExtraCurricular");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ExtraCuricularReport_ExportToPdf()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = eAcademy.Models.ResourceCache.Localize("ExtraCurricularDetailsReport");
                GeneratePDF.ExportPDF_Portrait(Extracuricularlist, new string[] { "ActivityName", "InchargeId", "ActivityInchargeName", "ContactNumber" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", "ExtraCurricularList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        //Facilities Details
        public ActionResult FacilitiesReport(bool? pdf)
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult FacilitiesReport(Mandatory m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AcademicyearServices As = new AcademicyearServices();
                    FacilityInchargeServices FS = new FacilityInchargeServices();
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    var id = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = id;
                    ViewBag.yy = As.GetParticularYear(id);
                    var a1 = FS.GetFacilitiesIncharge(id);
                    TempData["ans"] = a1;
                    ViewBag.view = a1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        //Json For Facilities
        public JsonResult GetFacilitiesReport(Rpt_FunctionTimeTable d)
        {
            try
            {
                var rr = new Rpt_FunctionTimeTable();
                var valid = TryUpdateModel(rr);
                if (valid)
                {
                    FacilityInchargeServices FS = new FacilityInchargeServices();
                    var id = Convert.ToInt32(d.AcademicYear);
                    var a1 = FS.GetFacilitiesIncharge(id);
                    return Json(a1, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        Valid = valid,
                        Errors = GetErrorsFromModelState(),

                    });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        //jQgridtable for facilities
        public JsonResult JQgridfacilities(string sidx, string sord, int page, int rows, int AcademicYear)
        {
            FacilityInchargeServices FS = new FacilityInchargeServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            y = AcademicYear;
            if (AcademicYear != null)
            {
                facilitieslist = FS.GetFacilitiesIncharge(AcademicYear);
            }
            int totalRecords = facilitieslist.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    facilitieslist = facilitieslist.OrderByDescending(s => s.FacilityName).ToList();
                    facilitieslist = facilitieslist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    facilitieslist = facilitieslist.OrderBy(s => s.FacilityName).ToList();
                    facilitieslist = facilitieslist.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = facilitieslist
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FacilityReport_ExportToExcel()
        {
            try
            {
                var list = facilitieslist.Select(o => new { FacilityName = o.FacilityName, FacultyInchargeId = o.FacultyInchargeId, FacultyInchargeName = o.FacultyInchargeName, ContactNumber = o.ContactNumber }).ToList();
                string heading =  eAcademy.Models.ResourceCache.Localize("FunctionDetailsReport");
                GenerateExcel.ExportExcel(list, heading);
                return View("FacilitiesReport");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult FacilityReport_ExportToPdf()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = eAcademy.Models.ResourceCache.Localize("FunctionDetailsReport");
                GeneratePDF.ExportPDF_Portrait(facilitieslist, new string[] { "FacilityName", "FacultyInchargeName", "FacultyInchargeId", "ContactNumber" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", "FacilityList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        //circular Details
        public ActionResult Circular()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Circular(FormCollection fm, Mandatory m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ReportHelper RptHelp = new ReportHelper();
                    AcademicyearServices As = new AcademicyearServices();
                    MonthsServices Ms = new MonthsServices();
                    CircularServices Cs = new CircularServices();
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.month = Ms.GetMonths();
                    var id = Convert.ToInt32(m.AcademicYear);
                    var rid = Convert.ToInt32(m.ReportType);
                    ViewBag.rid = rid;
                    ViewBag.yy = As.GetParticularYear(id);
                    ViewBag.reporttype = getrtype();
                    ViewBag.y = id;
                    if (rid == 1)
                    {
                        ViewBag.type = "daily";
                        DateTime da = Convert.ToDateTime(fm["Dates"]);
                        var ans1 = Cs.GetDailyCircular(id, da);
                        ViewBag.total = ans1.Count;
                        ViewBag.da = da.ToShortDateString();
                        TempData["ans"] = ans1;
                        ViewBag.view = ans1.Count;
                    }
                    else if (rid == 2)
                    {
                        ViewBag.type = "from/to";
                        DateTime from1 = Convert.ToDateTime(fm["Fromdate"]);
                        DateTime to = Convert.ToDateTime(fm["Todate"]);
                        var ans1 = Cs.GetFromToCircular(id, from1, to);
                        ViewBag.from1 = from1.ToShortDateString();
                        ViewBag.to = to.ToShortDateString();
                        TempData["ans"] = ans1;
                        ViewBag.view = ans1.Count;
                    }
                    else if (rid == 3)
                    {
                        ViewBag.type = "month";
                        var mid = Convert.ToInt32(fm["month"]);
                        var ans1 = Cs.GetMonthCircular(id, mid);
                        ViewBag.id = mid;
                        ViewBag.mid = Ms.GetParticularMonth(mid);
                        TempData["ans"] = ans1;
                        ViewBag.view = ans1.Count;
                    }
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        //json for circular report
        [HttpPost]
        public JsonResult getCircular(Rpt_Circular a)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string success = "Success";
                    return Json(new { Success = success }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult getdailycircular(Rpt_DailyCircular d)
        {
            try
            {
                JsonResponse res = new JsonResponse();
                var rr = new Rpt_DailyCircular();
                if (ModelState.IsValid)
                {
                    CircularServices Cs = new CircularServices();
                    var id = Convert.ToInt32(d.AcademicYear);
                    DateTime da = Convert.ToDateTime(d.Dates);
                    var ans1 = Cs.GetDailyCircular(id, da);
                    var count = ans1.Count;
                    return Json(ans1, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public JsonResult getfromtocircular(Rpt_FromToCircular d)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    CircularServices Cs = new CircularServices();
                    var id = Convert.ToInt32(d.AcademicYear);
                    var ans1 = Cs.GetFromToCircular(id, d.Fromdate, d.Todate);
                    var count = ans1.Count;
                    return Json(ans1, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult getmonthcircular(Rpt_MonthCircular d)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    CircularServices Cs = new CircularServices();
                    var ans1 = Cs.GetMonthCircular(d.AcademicYear, d.month);
                    var count = ans1.Count;
                    return Json(ans1, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public string RenderPartialViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult =
                    ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext,
                    viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
        private Dictionary<string, Object> GetErrorsFromModelState()
        {
            var errors = new Dictionary<string, Object>();
            foreach (var key in ModelState.Keys)
            {
                if (ModelState[key].Errors.Count > 0)
                {
                    errors[key] = ModelState[key].Errors;
                }
            }
            return errors;
        }
    }
}
