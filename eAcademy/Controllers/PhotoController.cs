﻿using eAcademy.HelperClass;
using System;
using System.IO;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    public class PhotoController : Controller
    {
        public ActionResult TakePhoto()
        {
            return View();
        }
        public ActionResult Capture()
        {
            var stream = Request.InputStream;
            string dump;
            using (var reader = new StreamReader(stream))
            {
                dump = reader.ReadToEnd();
                DateTime nm = DateTimeByZone.getCurrentDateTime();
                string date = nm.ToString("yyyymmddMMss");
                var path = Server.MapPath("~/WebImages/" + date + "test.jpg");
                System.IO.File.WriteAllBytes(path, String_To_Bytes2(dump));
                ViewData["path"] = date + "test.jpg";
                Session["val"] = date + "test.jpg";
            }
            return View("TakePhoto");
        }
        private byte[] String_To_Bytes2(string strInput)
        {
            int numBytes = (strInput.Length) / 2;
            byte[] bytes = new byte[numBytes];
            for (int x = 0; x < numBytes; ++x)
            {
                bytes[x] = Convert.ToByte(strInput.Substring(x * 2, 2), 16);
            }
            return bytes;
        }
        public JsonResult Rebind()
        {
            string path = "/WebImages/" + Session["val"].ToString();
            return Json(path, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Rebind1()
        {
            string path = "http://localhost:55654/WebImages/" + Session["val"].ToString();
            return Json(path, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BillReceipt(string cid)
        {

            return View();
        }
    }
}