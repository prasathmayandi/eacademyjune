﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class GradeInfoController : Controller
    {
        AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
        AssignClassToStudentServices stu = new AssignClassToStudentServices();
        StudentGradeServices StuGrade = new StudentGradeServices();
        UserActivityHelper useractivity = new UserActivityHelper();
       
        public int FacultyId;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            FacultyId = Convert.ToInt32(Session["empRegId"]);
            AcademicyearServices AcYear = new AcademicyearServices();
            ClassServices clas = new ClassServices();
            ExamServices exam = new ExamServices();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.allClasses = clas.ShowClasses();
            ViewBag.allExams = exam.ShowExams();
        }
        public JsonResult showSection(int passClassId)
        {
            try
            {
                var SecList = facultySub.ShowSections(FacultyId);
                return Json(SecList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult showSubjects(int passSecId)
        {
            try
            {
                var SubjectList = facultySub.ShowSubjects(passSecId, FacultyId);
                return Json(SubjectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult showStudents(int passSecId)
        {
            try
            {
                var StudentList = stu.getStudentsList(passSecId);
                return Json(StudentList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getClassWithSection(int passYearId)
        {
            try
            {
            var list = facultySub.getClassWithSection(FacultyId, passYearId);
            return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetStudents(int passYearId, int passClassId, int passSecId)
        {
            try
            {
                AssignClassToStudentServices stu = new AssignClassToStudentServices();
                var StudentList = stu.getStudents1(passYearId, passClassId, passSecId);
                return Json(StudentList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public IEnumerable<Object> ShowClassWithSection(int yearId)
        {
            var list = facultySub.getClassWithSection(FacultyId, yearId);
            return list;
        }
        public IEnumerable<Object> ShowStudents1(int yearId, int classId, int secId)
        {
            AssignClassToStudentServices stu = new AssignClassToStudentServices();
            var StudentList = stu.getStudents1(yearId, classId, secId);
            return StudentList;
        }
        public JsonResult showSubjects1(int passYearId, int passClassId, int passSecId)
        {
            try
            {
                var SubjectList = facultySub.ShowSubjects(passYearId, passClassId, passSecId, FacultyId);
                return Json(SubjectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Grade()
        {
            return View();
        }
        [HttpPost]
        public JsonResult getStudentListForGrade(int passYearId, int passClassId, int passSecId, int passSubId, int passExamId)
        {
            try
            {
                MarkServices mark = new MarkServices();
                var getMaxMark = mark.getMaxMark(passYearId, passClassId, passSecId, passSubId);
                if (getMaxMark == null)
                {
                    string msg1 = "Error";
                    return Json(new { msg1 }, JsonRequestBehavior.AllowGet);
                }
                ExamAttendanceServices examAtt = new ExamAttendanceServices();
                var checkAttendanceTaken = examAtt.CheckExamAttendanceExist(passYearId, passClassId, passSecId, passSubId, passExamId);
                if (checkAttendanceTaken == null)
                {
                    string ErrorMessage = ResourceCache.Localize("Exam_attendance_not_taken");
                    return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var checkMarkExist = StuGrade.checkGradeExist(passYearId, passClassId, passSecId, passSubId, passExamId);
                    if (checkMarkExist != null)
                    {
                        var getMarkExist = StuGrade.getExistGrade(passYearId, passClassId, passSecId, passSubId, passExamId);
                        string msg = "old";
                        var MaxMark = getMaxMark.MaxMark;
                        return Json(new { msg, getMarkExist, MaxMark }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var studentList = stu.getStudentsToGiveMark(passYearId, passClassId, passSecId, passSubId, passExamId);
                        string msg = "new";
                        var MaxMark = getMaxMark.MaxMark;
                        return Json(new { msg, studentList, MaxMark }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult Grade(Mark1 m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int YearId = Convert.ToInt32(m.Mark_allAcademicYears);
                    int currentAcYearId = Convert.ToInt32(Session["CurrentAcYearId"]);
                    if (YearId == currentAcYearId)
                    {
                        int yearId = m.Mark_allAcademicYears;
                        int classId = m.Mark_allClass;
                        int secId = m.Mark_Section;
                        int subId = m.Mark_allSubjects;
                        int examId = m.Mark_Exam;
                        GradeCommentServices grCmtSer = new GradeCommentServices();
                        var IsTotalCalculated = grCmtSer.IsReportCardGenerated(yearId, classId, secId, examId);
                        if (IsTotalCalculated != null)
                        {
                            string ErrorMessage = ResourceCache.Localize("You_cannot_change_the_mark_total_had_been_calculated");
                            return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            int TotalCount = Convert.ToInt32(m.TotalCount);
                            int size = TotalCount + 1;
                            string[] Id = new string[size];
                            string[] mark = new string[size];
                            string[] cmt = new string[size];
                            string[] grade_Id = new string[size];
                            GradeServices grade = new GradeServices();
                            var getGrade = grade.getGradeDetail(yearId, classId);
                            int gradeCount = getGrade.ToList().Count;
                            if (gradeCount != 0)
                            {
                                var checkMarkExist = StuGrade.checkGradeExist(yearId, classId, secId, subId, examId);
                                if (checkMarkExist != null)
                                {
                                    for (int i = 0; i <= TotalCount; i++)
                                    {
                                        Id[i] = QSCrypt.Decrypt(m.Id[i]);
                                        int StudentRegisterId = Convert.ToInt32(Id[i]);
                                        int Mark = Convert.ToInt32(m.Mark[i]);
                                        string comment = m.Comment[i];
                                        // int gradeId = Convert.ToInt32(QSCrypt.Decrypt(m.grade_Id[i]));
                                        string Grade = "";
                                        for (int k = 0; k < getGrade.Count; k++)
                                        {
                                            int? min = getGrade[k].minVal;
                                            int? max = getGrade[k].maxVal;
                                            if (Mark >= min && Mark <= max)
                                            {
                                                Grade = getGrade[k].grade;
                                            }
                                        }
                                        StuGrade.updateStudentGrade(yearId, classId, secId, examId, subId, StudentRegisterId, Mark, Grade, FacultyId, comment);
                                    }
                                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Update_grade_to_student"));
                                    string Message = ResourceCache.Localize("Marks_updated_successfully");
                                    return Json(new { Message }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    for (int i = 0; i <= TotalCount; i++)
                                    {
                                        Id[i] = QSCrypt.Decrypt(m.Id[i]);
                                        int StudentRegisterId = Convert.ToInt32(Id[i]);
                                        int Mark = Convert.ToInt32(m.Mark[i]);
                                        string comment = m.Comment[i];
                                        string Grade = "";
                                        for (int k = 0; k < getGrade.Count; k++)
                                        {
                                            int? min = getGrade[k].minVal;
                                            int? max = getGrade[k].maxVal;
                                            if (Mark >= min && Mark <= max)
                                            {
                                                Grade = getGrade[k].grade;
                                            }
                                        }
                                        StuGrade.addStudentGrade(StudentRegisterId, yearId, classId, secId, examId, subId, Mark, Grade, FacultyId, comment);
                                    }
                                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Add_grade_to_student"));
                                    string Message = ResourceCache.Localize("Grade_saved_successfully");
                                    return Json(new { Message }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                string ErrorMessage = ResourceCache.Localize("Please_ask_admin_to_set_grade_level");
                                return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        string InfoMessage = ResourceCache.Localize("Records_can_save_for_current_academic_year_only");
                        return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ProgressCard()
        {
            return View();
        }
        [HttpPost]
        public JsonResult ProgressCard(FacultyProgressCard pc)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int yearId = pc.PG_allAcademicYears;
                    var CI_ClassWithSection = pc.CI_ClassWithSection;
                    int stuId = pc.PG_allStudents;
                    int examId = pc.PG_Exam;
                    string clasSec = CI_ClassWithSection;
                    string[] words = clasSec.Split(' ');
                    int classId = Convert.ToInt32(words[0]);
                    int secId = Convert.ToInt32(words[1]);
                    var list = StuGrade.getGrade(yearId, classId, secId, examId, stuId);
                    int count = list.Count;
                    return Json(new { list, count }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}