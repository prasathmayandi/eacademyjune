﻿using eAcademy.DataModel;
using eAcademy.ViewModel;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Collections;
using eAcademy.HelperClass;
using eAcademy.Models;
using System.Net;
using eAcademy.Helpers;

namespace eAcademy.Controllers
{
    [Student_CheckSessionAttribute]
    public class StudentController : Controller
    {
        //
        // GET: /Student/

        ExamServices Exam = new ExamServices();
        CountryServices country = new CountryServices();
        StudentServices stu = new StudentServices();
        AssignClassToStudentServices clas = new AssignClassToStudentServices();
        FeeServices Annualfee = new FeeServices();
        TermFeeServices TermFee = new TermFeeServices();
        FeeCollectionServices FeeCollection = new FeeCollectionServices();
        StudentDailyAttendanceServices Attendance = new StudentDailyAttendanceServices();
        StudentMarkServices mark = new StudentMarkServices();
        MarkTotalServices markTotal = new MarkTotalServices();
        StudentGradeServices grade = new StudentGradeServices();

        public int StudentRegId;
        public int ParentRegId;

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AcademicyearServices AcYear = new AcademicyearServices();

            StudentRegId = Convert.ToInt32(Session["StudentRegId"]);
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.allExams = Exam.ShowExams();
            ParentRegId = Convert.ToInt32(Session["ParentRegId"]);
            ViewBag.allCountries = country.ShowCountries();
        }

        public ActionResult StudentProfile()
        {
            try
            {
                AdmissionTransactionServices admissionTransact = new AdmissionTransactionServices();
                var getStudentParentDetails = admissionTransact.checkStudentParentProfileExist(ParentRegId, StudentRegId);
                if (getStudentParentDetails != null)
                {
                    int? FatherRegId = getStudentParentDetails.FatherRegisterId;
                    int? MotherRegId = getStudentParentDetails.MotherRegisterId;
                    int? GuardianRegId = getStudentParentDetails.GuardianRegisterId;
                    var list = admissionTransact.getStudentParentDetails(ParentRegId, StudentRegId);

                    StudentServices stuser = new StudentServices();
                    var row = stuser.getStudentRow(StudentRegId);
                    if (row != null)
                    {
                        int? bloodGroupid = row.StudentBloodGroup;
                        if (bloodGroupid != null)
                        {
                            var blood = admissionTransact.getBloodGroup(bloodGroupid);
                            if (blood != null)
                            {
                                var BloodGroup = blood.BloodGroupName;
                                ViewBag.StudentBloodGroup = BloodGroup;
                            }
                        }
                        string localCountryId = row.LocalCountry;
                        ViewBag.StudentCountryname = localCountryId;
                    }
                    if (FatherRegId != null)
                    {
                        var getFatherDetail = admissionTransact.getFatherDetail(ParentRegId, StudentRegId, FatherRegId);
                        ViewBag.FatherName = getFatherDetail.FatherName;
                        ViewBag.FatherDOB = getFatherDetail.FatherDob;
                        ViewBag.FatherEmail = getFatherDetail.FatherEmail;
                        ViewBag.FatherMobile = getFatherDetail.FatherMobile;
                        ViewBag.FatherOccupation = getFatherDetail.FatherOccupation;
                        ViewBag.FatherQualification = getFatherDetail.FatherQualification;
                    }
                    if (MotherRegId != null)
                    {
                        var getMotherDetail = admissionTransact.getMotherDetail(ParentRegId, StudentRegId, MotherRegId);
                        ViewBag.MotherName = getMotherDetail.MotherName;
                        ViewBag.MotherDOB = getMotherDetail.MotherDob;
                        ViewBag.MotherEmail = getMotherDetail.MotherEmail;
                        ViewBag.MotherMobile = getMotherDetail.MotherMobile;
                        ViewBag.MotherOccupation = getMotherDetail.MotherOccupation;
                        ViewBag.MotherQualification = getMotherDetail.MotherQualification;
                    }
                    if (GuardianRegId != null)
                    {
                        var getGuardianDetail = admissionTransact.getGuardianDetail(ParentRegId, StudentRegId, GuardianRegId);
                        ViewBag.GuardianName = getGuardianDetail.GuardianName;
                        ViewBag.GuardianGender = getGuardianDetail.GuardianGender;
                        ViewBag.GuardianRelationship = getGuardianDetail.GuardianRelationship;
                        ViewBag.GuardianDob = getGuardianDetail.GuardianDob;
                        ViewBag.GuardianEmail = getGuardianDetail.GuardianEmail;
                        ViewBag.GuardianMobile = getGuardianDetail.GuardianMobile;
                        ViewBag.GuardianQualification = getGuardianDetail.GuardianQualification;
                        ViewBag.GuardianIncome = getGuardianDetail.GuardianIncome;
                    }
                    return View(list);
                }
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult EditProfile()
        {
            TempData["EditProfile"] = "Edit";
            return RedirectToAction("StudentProfile");
        }

        [HttpPost]
        public ActionResult StudentProfile(StudentProfile sp)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    stu.updateStudentProfile(StudentRegId, sp.BloodGroup, sp.Email, sp.Religion, sp.Community, sp.Nationality, sp.Contact, sp.EmergencyContactPerson, sp.EmergencyContactNumber, sp.Address, sp.City, sp.State, sp.Student_allCountries);
                    TempData["Message"] = "Profile updated successfully";
                    return RedirectToAction("StudentProfile");
                }
                else
                {
                    ViewBag.Edit = "Edit";
                    ViewBag.ErrorMessage = ResourceCache.Localize("Please_select_all_the_mantratory_fields");
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult FeeManagement()
        {
            try
            {
                var getRow = clas.getAssigendClass(StudentRegId);
                if (getRow != null)
                {
                    int yearId = Convert.ToInt32(getRow.AcademicYearId);
                    int classId = Convert.ToInt32(getRow.ClassId);
                    int secId = Convert.ToInt32(getRow.SectionId);

                    // To getAnnualFeeStructure
                    var getAnnualFeeStructure = Annualfee.getAnnualFee(yearId, classId);

                    ArrayList AnnualFeeName = new ArrayList();
                    ArrayList AnnualFeeAmount = new ArrayList();
                    ArrayList AnnualFeeLastDate = new ArrayList();
                    foreach (var d in getAnnualFeeStructure)
                    {
                        AnnualFeeName.Add(d.FeeName);
                        AnnualFeeAmount.Add(Math.Round(d.Amount.Value, 2, MidpointRounding.AwayFromZero));
                        AnnualFeeLastDate.Add(d.Last_Date);
                    }

                    ViewBag.AnnualfeeName = AnnualFeeName;
                    ViewBag.AnnualfeeAmount = AnnualFeeAmount;
                    ViewBag.AnnualfeeLastDate = AnnualFeeLastDate;

                    // To getTermFeeStructure

                    var getTermFeeStructure = TermFee.getTermFee(yearId, classId);

                    ArrayList termName = new ArrayList();
                    ArrayList termFeeName = new ArrayList();
                    ArrayList termFeeAmount = new ArrayList();
                    ArrayList termFeeLastDate = new ArrayList();
                    ArrayList termCount = new ArrayList();
                    foreach (var t in getTermFeeStructure)
                    {
                        termName.Add(t.TermName);
                        termCount.Add(t.TermName);
                        termFeeName.Add(t.TermFeeName);
                        termFeeAmount.Add(Math.Round(t.TermFeeAmount.Value, 2, MidpointRounding.AwayFromZero));
                        termFeeLastDate.Add(t.TermFeeLast_Date);
                    }
                    ViewBag.TermName = termName;
                    ViewBag.TermCount = termName.Count;
                    ViewBag.TermFeeName = termFeeName;
                    ViewBag.TermFeeAmount = termFeeAmount;
                    ViewBag.TermFeeLastDate = termFeeLastDate;

                    ViewBag.yearId = yearId;

                    var list = FeeCollection.getAnnualPaidFees(yearId, classId, secId, StudentRegId);
                    ViewBag.count = list.Count;

                    return View(list);
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public JsonResult FeeManagementList(int PassYearId)
        {
            try
            {
                var list = FeeCollection.getAnnualPaidFees1(PassYearId, StudentRegId);
                int count = list.Count;
                return Json(new { list, count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AttendanceReport()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AttendanceReport(StudentAttendanceReport r)
        {
            try
            {
                AcademicyearServices acYear = new AcademicyearServices();
                if (ModelState.IsValid)
                {
                    int yearId = r.SM_AcademicYear;
                    var getRow = clas.getAssigendClass(StudentRegId, yearId);
                    if (getRow != null)
                    {
                        int classId = Convert.ToInt32(getRow.ClassId);
                        int secId = Convert.ToInt32(getRow.SectionId);

                        var list = Attendance.getStudentDailyAttendance(r.SM_AcademicYear, classId, secId, StudentRegId, r.SM_txt_FromDate, r.SM_txt_ToDate);

                        string PresentStatus = "P";
                        string AbsentStatus = "A";

                        var NumberOfWorkingDays = Attendance.getWorkingDaysCountNew(yearId, classId, secId, r.SM_txt_FromDate, r.SM_txt_ToDate);
                        ViewBag.NumberOfWorkingDays = NumberOfWorkingDays;

                        var getPresentlist = Attendance.getPresentStudent(yearId, classId, secId, StudentRegId, r.SM_txt_FromDate, r.SM_txt_ToDate, PresentStatus).ToList();
                        var PresentDays = getPresentlist.Count;
                        ViewBag.PresentCount = getPresentlist.ToList().Count;

                        var getAbsentlist = Attendance.getPresentStudent(yearId, classId, secId, StudentRegId, r.SM_txt_FromDate, r.SM_txt_ToDate, AbsentStatus);
                        ViewBag.AbsentCount = getAbsentlist.ToList().Count;

                        if(NumberOfWorkingDays == 0)
                        {
                            ViewBag.attendancePercentage = 0;
                        }
                        else
                        {
                            var attendancePercent = (PresentDays / NumberOfWorkingDays) * 100.0;
                            ViewBag.attendancePercentage = attendancePercent;
                        }
                        ViewBag.prtAcYear = acYear.AcademicYear(r.SM_AcademicYear).AcademicYear1;
                        ViewBag.fdate = r.SM_txt_FromDate.ToString("dd/MM/yyyy");
                        ViewBag.todate = r.SM_txt_ToDate.ToString("dd/MM/yyyy");
                        return View(list);
                    }
                }
                ViewBag.prtAcYear = acYear.AcademicYear(r.SM_AcademicYear).AcademicYear1;
                ViewBag.fdate = r.SM_txt_FromDate.ToString("dd/MM/yyyy");
                ViewBag.todate = r.SM_txt_ToDate.ToString("dd/MM/yyyy");
                ViewBag.Records = "Empty";
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View("Error");
            }
        }


        public ActionResult ProgressCard()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ProgressCard(Stu_ProgressCard cp)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int yearId = cp.stu_PC_allAcademicYears;

                    var getRow = clas.getAssigendClass(StudentRegId, yearId);
                    if (getRow != null)
                    {
                        int classId = Convert.ToInt32(getRow.ClassId);
                        int secId = Convert.ToInt32(getRow.SectionId);

                        int examId = cp.stu_PC_allExams;

                        var list = mark.getMark(yearId, classId, secId, examId, StudentRegId);
                        ViewBag.listConut = list.Count;

                        var MarkTotal = markTotal.getMarkTotal(yearId, classId, secId, examId, StudentRegId);

                        if (MarkTotal != null)
                        {
                            ViewBag.Total = MarkTotal.TotalMark;
                            ViewBag.Result = MarkTotal.Result;
                            ViewBag.Rank = MarkTotal.StudentRank;
                            ViewBag.comment = MarkTotal.Comment;
                        }

                        return View(list);
                    }
                    else
                    {
                        ViewBag.records = "Empty";
                        return View();
                    }
                }
                else
                {
                    //ViewBag.ErrorMessage = ResourceCache.Localize("Please_select_all_the_mantratory_fields");
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public JsonResult CustomeActiveDate(int acid)
         {
            AcademicyearServices AcYear = new AcademicyearServices();
            var ans = AcYear.date(acid);
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            EventServices holidays = new EventServices();
            var HolidaysList = holidays.getHolidaysList(acid);
            DateTime CurrentDate = DateTimeByZone.getCurrentDate();
            DateTime sdate = ans.StartDate;
            DateTime edate = ans.EndDate;
            if (sdate <= CurrentDate && CurrentDate <= edate)
            {
                StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                EndingEnableDate = edate.ToString("dd/MM/yyyy");
                return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GradeProgressCard()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GradeProgressCard(Stu_ProgressCard cp)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int yearId = cp.stu_PC_allAcademicYears;
                    var getRow = clas.getAssigendClass(StudentRegId, yearId);
                    if (getRow != null)
                    {
                        int classId = Convert.ToInt32(getRow.ClassId);
                        int secId = Convert.ToInt32(getRow.SectionId);
                        int examId = cp.stu_PC_allExams;
                        var list = grade.getGrade(yearId, classId, secId, examId, StudentRegId);
                        ViewBag.ListCount = list.Count;

                        return View(list);
                    }
                    ViewBag.record = "empty";
                    return View();
                }
                else
                {
                    ViewBag.ErrorMessage = ResourceCache.Localize("Please_select_all_the_mantratory_fields");
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public JsonResult CurrentActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");

            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    int acid = CurrentAcYear[i].AcademicYearId;
                    return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Communication()
        {
            return View();
        }
        public static IList<CommunicationList> todoListsResults2 = new List<CommunicationList>();
        public JsonResult CommunicationResults(string sidx, string sord, int page, int rows, DateTime? SM_txt_FromDate_List, DateTime? SM_txt_ToDate_List)
        {
            AssignClassToStudentServices Assclas = new AssignClassToStudentServices();
            CommunicationServices cs = new CommunicationServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            var getRow = clas.getAssigendClass(StudentRegId);
           
            int yearid = Convert.ToInt32(getRow.AcademicYearId);
            int classId = Convert.ToInt32(getRow.ClassId);
            int secId = Convert.ToInt32(getRow.SectionId);


            if (SM_txt_FromDate_List != null && SM_txt_ToDate_List != null)
            {
                todoListsResults2 = cs.GetParentCommunicationDetaions(yearid, classId, SM_txt_FromDate_List.Value, SM_txt_ToDate_List.Value, StudentRegId);
            }
            else
            {
                todoListsResults2 = cs.GetParentCommunicationDetaions(yearid, classId, StudentRegId);
            }

            int totalRecords = todoListsResults2.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults2
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
            
        }

        public void OpenCommunicationFile(string id, string name)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                CommunicationServices cs = new CommunicationServices();
                var getSubjectNotesFile = "";
                var r1 = cs.GetClassFile(fid);
                var r2 = cs.GetStudentFile(fid);
                if (name == "class")
                {
                    if (r1 != null)
                    {
                        getSubjectNotesFile = r1.FileName;
                    }
                }
                if (name == "student")
                {
                    if (r2 != null)
                    {
                        getSubjectNotesFile = r2.FileName;
                    }
                }

                string file = getSubjectNotesFile;
                if (file != null)
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Areas/Communication/Documents/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {

                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + file);
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }
        public void OpenCommunicationFile1(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                CommunicationServices cs = new CommunicationServices();
                var getSubjectNotesFile = "";
                var r1 = cs.GetClassFile(fid);
                var r2 = cs.GetStudentFile(fid);

                if (r1 != null)
                {
                    getSubjectNotesFile = r1.FileName;
                }

                if (r2 != null)
                {
                    getSubjectNotesFile = r2.FileName;
                }

                string file = getSubjectNotesFile;
                if (file != null)
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Areas/Communication/Documents/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {

                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + file);
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }
        public ActionResult CommunicationExportToExcel()
        {
            try
            {
                var list = todoListsResults2.Select(o => new { EventDate = o.EventDate, Title = o.Title, Description = o.Description, PostedDate = o.PostedDate }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("CommunicationDetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult CommunicationExportToPDF()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("CommunicationDetails");
                GeneratePDF.ExportPDF_Portrait(todoListsResults2, new string[] { "EventDate", "Title", "Description", "PostedDate" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "CommunicationDetails.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
    }
}
