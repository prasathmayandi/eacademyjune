﻿using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class FeeManagementController : Controller
    {
        AcademicyearServices obj_academicYear = new AcademicyearServices();
        ClassServices obj_class = new ClassServices();
        TermServices obj_term = new TermServices();
        SectionServices obj_section = new SectionServices();
        FeeCategoryServices obj_FeeCategory = new FeeCategoryServices();
        FeeCollectionServices obj_feeCollection = new FeeCollectionServices();
        StudentServices obj_student = new StudentServices();
        TermFeeServices obj_termFee = new TermFeeServices();
        FeeServices obj_fee = new FeeServices();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ViewBag.acYear = obj_academicYear.AllList_AcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
            ViewBag.ClassList = obj_class.Classes();
            ViewBag.feeCategory = obj_FeeCategory.getFeeCategory();
        }
        public ActionResult FeeDetails()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
       
        public JsonResult FeeConfigResults(string sidx, string sord, int page, int rows, int? ddAcademicYear, int? ddClass)
        {
              IList<ConfigFee> todoListsResults2 = new List<ConfigFee>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults2 = obj_fee.getFeeConfig();
            if (ddAcademicYear.HasValue)
            {
                todoListsResults2 = todoListsResults2.Where(p => p.AcYearId == ddAcademicYear).ToList();
            }
            if (ddAcademicYear.HasValue && ddClass.HasValue)
            {
                todoListsResults2 = todoListsResults2.Where(p => p.AcYearId == ddAcademicYear && p.ClassId == ddClass).ToList();
            }
            int totalRecords = todoListsResults2.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults2 = todoListsResults2.OrderByDescending(s => s.AcYearId).ToList();
                    todoListsResults2 = todoListsResults2.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults2 = todoListsResults2.OrderBy(s => s.AcYearId).ToList();
                    todoListsResults2 = todoListsResults2.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults2;
           
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults2
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FeeConfigExportToExcel()
        {
            try
            {
                List<ConfigFee> todoListsResults2 = (List<ConfigFee>)Session["JQGridList"];
                var list = todoListsResults2.Select(o => new { AcademicYear = o.AcYear, Class = o.Class, FeeCategory = o.FeeCategory, Amount = o.Amount, Tax = o.Tax, Total = o.Total, LastDate = o.Date }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("FeesDetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult FeeConfigExportToPDF()
        {
            try
            {

                List<ConfigFee> todoListsResults2 = (List<ConfigFee>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("FeesDetails");
                GeneratePDF.ExportPDF_Landscape(todoListsResults2, new string[] { "AcademicYear", "Class", "FeeCategory", "Amount", "Tax", "Total", "LastDate" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "FeeDetails.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public JsonResult getClass()
        {
            try
            {
                var ans = obj_class.Classes();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getSection(int cid)
        {
            try
            {
                var ans = obj_section.getSectionByClassId(cid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getStudent(int acid, int sec_id, int cid)
        {
            try
            {
                var ans = obj_student.getStudentByIDs(sec_id, cid, acid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getStudentFees(int acid, int cid)
        {
            try
            {
                var ans1 = obj_termFee.getTermFeeByID(acid, cid);
                var ans2 = obj_fee.getFeeByID(acid, cid);
                return Json(new { ans1, ans2 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult FeePaidList()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult getTermCount(int acid)
        {
            try
            {
                var ans = obj_term.getTerm(acid);
                var count = ans.Count();
                return Json(count, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getTerms(int acid)
        {
            try
            {
                var ans = obj_term.TermOnYear(acid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult StudentsFeePaidList(int acid, int cid, int secid, int fid)
        {
            try
            {
                var ans = obj_feeCollection.getFeePaidStudentList(acid, cid, secid, fid);
                var result = obj_feeCollection.getFeePaidList(acid, cid, secid, fid);
                return Json(new { ans = ans, ans_count = result.ToList().Count, result = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getFeeAmount(int acid, int fid, int cid, int sec_id, int stu_id)
        {
            try
            {
                var ans = obj_feeCollection.getFeeCollection(acid, cid, sec_id, fid, stu_id);
                int tid = ans.SingleOrDefault().PaymentTypeId;
                var term = "";
                if (tid != 0)
                {
                    term = obj_term.TermList(acid, tid);
                }
                else
                {
                    term = "Annual Fee";
                }
                if (ans.Count() == 0)
                {
                    return Json(new { msg = "nodata" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { ans = ans, term = term }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult FeeDueList()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult GetStudentFeelist(int aid, int cid, int sid, int payid, int feeid)
        {
            try
            {
                FeeCollectionCategoryServices fee = new FeeCollectionCategoryServices();
                TermServices tt = new TermServices();
                int temp = payid;
                if (payid == 0)
                {
                    var dd = tt.GetAcademicterm(aid);
                    payid = dd.TermId;
                }
                var result = fee.GetParticularClassSectionPeesDueList(aid, cid, sid, payid, feeid, temp);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}