﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using iTextSharp.text;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class ClassReportController : Controller
    {
        // GET: /ClassReport/
        Data db = new Data();
        EacademyEntities ee = new EacademyEntities();
        //public static IList<getclassandsection> ClassSection = new List<getclassandsection>();
        //public static IList<getclassandsection> ClassIncharges = new List<getclassandsection>();
        //public static IList<getclassandsection> ClassFacultys = new List<getclassandsection>();
        //public static IList<getclassandsection> ClassSubjects = new List<getclassandsection>();
        //public static IList<getclassandsection> ClassStudentNameLists = new List<getclassandsection>();
        //public static IList<Ranklist> StudentrankLists = new List<Ranklist>();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ReportHelper RptHelp = new ReportHelper();
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            ExamServices Es = new ExamServices();
            MonthsServices Ms = new MonthsServices();
            FeeCategoryServices FCS = new FeeCategoryServices();
            FeeCollectionServices FeeCollectionService = new FeeCollectionServices();
            List<SelectListItem> Sort = new List<SelectListItem>();
            Sort.Add(new SelectListItem { Text = "Select Status", Value = "" });
            Sort.Add(new SelectListItem { Text = "Both", Value = "All" });
            Sort.Add(new SelectListItem { Text = "Pass", Value = "Pass" });
            Sort.Add(new SelectListItem { Text = "Fail", Value = "fail" });
            ViewBag.SortList = Sort;
            ViewBag.academicyear = As.GetAcademicYear();
            ViewBag.Classes = Cs.GetClass();
            ViewBag.reporttype = getrtype();
            ViewBag.month = Ms.GetMonths();
            ViewBag.FeePaticular = getFeeParticulars();
            ViewBag.exam = Es.GetExamName();
            ViewBag.paytype = RptHelp.getPaymentType();
            ViewBag.date = DateTimeByZone.getCurrentDateTime();
        }
        public IEnumerable getrtype()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select", Value = "" });
            li.Add(new SelectListItem { Text = "Daily", Value = "1" });
            //li.Add(new SelectListItem { Text = "From/To", Value = "2" });
            li.Add(new SelectListItem { Text = "Monthly", Value = "3" });
            return li;
        }
        public IEnumerable getmonth()
        {
            List<SelectListItem> lli = new List<SelectListItem>();
            lli.Add(new SelectListItem { Text = "Select", Value = "" });
            lli.Add(new SelectListItem { Text = "Jan", Value = "1" });
            lli.Add(new SelectListItem { Text = "Feb", Value = "2" });
            lli.Add(new SelectListItem { Text = "Mar", Value = "3" });
            lli.Add(new SelectListItem { Text = "Apr", Value = "4" });
            lli.Add(new SelectListItem { Text = "May", Value = "5" });
            lli.Add(new SelectListItem { Text = "Jun", Value = "6" });
            lli.Add(new SelectListItem { Text = "Jul", Value = "7" });
            lli.Add(new SelectListItem { Text = "Aug", Value = "8" });
            lli.Add(new SelectListItem { Text = "Sep", Value = "9" });
            lli.Add(new SelectListItem { Text = "Oct", Value = "10" });
            lli.Add(new SelectListItem { Text = "Nov", Value = "11" });
            lli.Add(new SelectListItem { Text = "De", Value = "12" });
            return lli;
        }
        public IEnumerable getFeeParticulars()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select", Value = "" });
            li.Add(new SelectListItem { Text = "Paid", Value = "1" });
            li.Add(new SelectListItem { Text = "Due", Value = "2" });
            return li;
        }
        public ActionResult ClassList()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
        }
        //jQgridtable for ClassSection
        public JsonResult JQgridClassSection(string sidx, string sord, int page, int rows)
        {
             IList<getclassandsection> ClassSection = new List<getclassandsection>();
            ClassServices cs = new ClassServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            ClassSection = cs.GetAllClassandSection();
            int totalRecords = ClassSection.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    ClassSection = ClassSection.OrderByDescending(s => s.ClassId).ToList();
                    ClassSection = ClassSection.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ClassSection = ClassSection.OrderBy(s => s.ClassId).ToList();
                    ClassSection = ClassSection.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = ClassSection;

            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = ClassSection
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ClassSectionReport_ExportToExcel()
        {
            try
            {
                IList<getclassandsection> ClassSection = (List<getclassandsection>)Session["JQGridList"];
                var list = ClassSection.Select(o => new { ClassName = o.ClassName, SectionName = o.SectionName }).ToList();
                string fileName =eAcademy.Models.ResourceCache.Localize("ClassSectionReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("ClassList");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ClassSectionReport_ExportToPdf()
        {
            try
            {
                IList<getclassandsection> ClassSection = (List<getclassandsection>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassSectionReport");
                GeneratePDF.ExportPDF_Portrait(ClassSection, new string[] { "ClassName", "SectionName" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "ClassSectionList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ClassIncharge()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ClassIncharge(Mandatory m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    TempData["show"] = "show";
                    ViewBag.academicyear = ee.AcademicYears.Select(q => q);
                    var id = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = id;
                    ViewBag.yy = ee.AcademicYears.Where(q => q.AcademicYearId.Equals(id)).FirstOrDefault().AcademicYear1;
                    var ans1 = db.SearchClassIncharge(id);
                    TempData["ans"] = ans1;
                    ViewBag.view = ans1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        //jQgridtable for ClassSIncharge
        public JsonResult JQgridClassIncharge(string sidx, string sord, int page, int rows, int AcademicYear)
        {
            IList<getclassandsection> ClassIncharges = new List<getclassandsection>();
            AssignClassTeacherServices cs = new AssignClassTeacherServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (AcademicYear != null)
            {
                ClassIncharges = cs.SearchClassIncharge(AcademicYear);
            }
            int totalRecords = ClassIncharges.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    ClassIncharges = ClassIncharges.OrderByDescending(s => s.ClassName).ToList();
                    ClassIncharges = ClassIncharges.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ClassIncharges = ClassIncharges.OrderBy(s => s.ClassName).ToList();
                    ClassIncharges = ClassIncharges.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = ClassIncharges;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = ClassIncharges
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ClassInchargeReport_ExportToExcel()
        {
            try
            {
                IList<getclassandsection> ClassIncharges = (List<getclassandsection>)Session["JQGridList"];
                var list = ClassIncharges.Select(o => new { ClassName = o.ClassName, SectionName = o.SectionName, ClassTeacher = o.ClassTeacher }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassInchargeReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("ClassIncharge");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ClassInchargeReport_ExportToPdf()
        {
            try
            {
                IList<getclassandsection> ClassIncharges = (List<getclassandsection>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassInchargeReport");
                GeneratePDF.ExportPDF_Portrait(ClassIncharges, new string[] { "ClassName", "SectionName", "ClassTeacher" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "ClassInchargesList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ClassSubject()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ClassSubject(Mandatory m)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            SectionServices SS = new SectionServices();
            AssignSubjectToSectionServices astss = new AssignSubjectToSectionServices();
            try
            {
                if (ModelState.IsValid)
                {
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.Classes = Cs.GetClass();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    var cid = Convert.ToInt32(m.Class);
                    ViewBag.cls = cid;
                    var sid = Convert.ToInt32(m.Rpt_Section);
                    ViewBag.section = SS.GetSection(cid);
                    ViewBag.sec = sid;
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(cid);
                    ViewBag.ss = SS.GetParticularSection(sid);
                    var ans1 = astss.SearchClassSubject(yid, cid, sid);
                    TempData["ans"] = ans1;
                    ViewBag.view = ans1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        //jQgridtable for ClassSIncharge
        public JsonResult JQgridClassSubject(string sidx, string sord, int page, int rows, int AcademicYear, int Class, int Rpt_Section)//
        {
            IList<getclassandsection> ClassSubjects = new List<getclassandsection>();
            AssignSubjectToSectionServices astss = new AssignSubjectToSectionServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (AcademicYear != null && Class != null && Rpt_Section != null)
            {
                ClassSubjects = astss.SearchClassSubject(AcademicYear, Class, Rpt_Section);
            }
            int totalRecords = ClassSubjects.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    ClassSubjects = ClassSubjects.OrderByDescending(s => s.ClassName).ToList();
                    ClassSubjects = ClassSubjects.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ClassSubjects = ClassSubjects.OrderBy(s => s.ClassName).ToList();
                    ClassSubjects = ClassSubjects.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = ClassSubjects;

            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = ClassSubjects
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ClassSubjectReport_ExportToExcel()
        {
            try
            {
                List<getclassandsection> ClassSubjects = (List<getclassandsection>)Session["JQGridList"];
                var list = ClassSubjects.Select(o => new { ClassName = o.ClassName, SectionName = o.SectionName, subjectName = o.subjectName }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassSubjectReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("ClassSubject");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ClassSubjectReport_ExportToPdf()
        {
            try
            {
                List<getclassandsection> ClassSubjects = (List<getclassandsection>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassSubjectReport");
                GeneratePDF.ExportPDF_Portrait(ClassSubjects, new string[] { "ClassName", "SectionName", "subjectName" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "ClassSubjectList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ClassFaculty()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ClassFaculty(Mandatory m)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            SectionServices SS = new SectionServices();
            AssignFacultyToSubjectServices afts = new AssignFacultyToSubjectServices();
            try
            {
                if (ModelState.IsValid)
                {
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.Classes = Cs.GetClass();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    var cid = Convert.ToInt32(m.Class);
                    ViewBag.cls = cid;
                    var sid = Convert.ToInt32(m.Rpt_Section);
                    ViewBag.section = SS.GetSection(cid);
                    ViewBag.sec = sid;
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(cid);
                    ViewBag.ss = SS.GetParticularSection(sid);
                    var ans1 = afts.SearchSubjectTeacher(yid, cid, sid);
                    TempData["ans"] = ans1;
                    ViewBag.view = ans1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        //jQgridtable for ClassSIncharge
        public JsonResult JQgridClassFaculty(string sidx, string sord, int page, int rows, int AcademicYear, int Class, int Rpt_Section)
        {
            AssignFacultyToSubjectServices afts = new AssignFacultyToSubjectServices();
            IList<getclassandsection> ClassFacultys = new List<getclassandsection>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (AcademicYear != null && Class != null && Rpt_Section != null)
            {
                ClassFacultys = afts.SearchSubjectTeacher(AcademicYear, Class, Rpt_Section);
            }
            int totalRecords = ClassFacultys.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    ClassFacultys = ClassFacultys.OrderByDescending(s => s.ClassName).ToList();
                    ClassFacultys = ClassFacultys.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ClassFacultys = ClassFacultys.OrderBy(s => s.ClassName).ToList();
                    ClassFacultys = ClassFacultys.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = ClassFacultys;

            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = ClassFacultys
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ClassFacultyReport_ExportToExcel()
        {
            try
            {
                List<getclassandsection> ClassFacultys = (List<getclassandsection>)Session["JQGridList"];
                var list = ClassFacultys.Select(o => new { ClassName = o.ClassName, SectionName = o.SectionName, subjectName = o.subjectName, ClassTeacher = o.ClassTeacher }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassFacultyReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("ClassFaculty");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ClassFacultyReport_ExportToPdf()
        {
            try
            {
                List<getclassandsection> ClassFacultys = (List<getclassandsection>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassFacultyReport");
                GeneratePDF.ExportPDF_Portrait(ClassFacultys, new string[] { "ClassName", "SectionName", "subjectName", "ClassTeacher" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "ClassFacultyList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult StudentNameList()
        {
            return View();
        }
        [HttpPost]
        public ActionResult StudentNameList(Mandatory m)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            SectionServices SS = new SectionServices();
            AssignClassToStudentServices actss = new AssignClassToStudentServices();
            try
            {
                if (ModelState.IsValid)
                {
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.Classes = Cs.GetClass();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    var cid = Convert.ToInt32(m.Class);
                    ViewBag.cls = cid;
                    var sid = Convert.ToInt32(m.Rpt_Section);
                    ViewBag.section = SS.GetSection(cid);
                    ViewBag.sec = sid;
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(cid);
                    ViewBag.ss = SS.GetParticularSection(sid);
                    var ans1 = actss.SearchStudentNameList(yid, cid, sid);
                    TempData["ans"] = ans1;
                    ViewBag.view = ans1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        //jQgridtable for ClassSIncharge
        public JsonResult JQgridStudentNameList(string sidx, string sord, int page, int rows, int AcademicYear, int Class, int Rpt_Section)//
        {
           IList<getclassandsection> ClassStudentNameLists = new List<getclassandsection>();
            AssignClassToStudentServices actss = new AssignClassToStudentServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (AcademicYear != null && Class != null && Rpt_Section != null)
            {
                ClassStudentNameLists = actss.SearchStudentNameList(AcademicYear, Class, Rpt_Section);
            }
            int totalRecords = ClassStudentNameLists.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    ClassStudentNameLists = ClassStudentNameLists.OrderByDescending(s => s.rollnumber).ToList();
                    ClassStudentNameLists = ClassStudentNameLists.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ClassStudentNameLists = ClassStudentNameLists.OrderBy(s => s.rollnumber).ToList();
                    ClassStudentNameLists = ClassStudentNameLists.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = ClassStudentNameLists;
           
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = ClassStudentNameLists
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult StudentNameListReport_ExportToExcel()
        {
            try
            {
                List<getclassandsection> ClassStudentNameLists = (List<getclassandsection>)Session["JQGridList"];
                var list = ClassStudentNameLists.Select(o => new { StudentId = o.StudentId, Rollnumber = o.rollnumber, StudentName = o.StudentName }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassStudentNameReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("StudentNameList");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult StudentNameListReport_ExportToPdf()
        {
            try
            {
                List<getclassandsection> ClassStudentNameLists = (List<getclassandsection>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassStudentNameReport");
                GeneratePDF.ExportPDF_Portrait(ClassStudentNameLists, new string[] { "StudentId", "Rollnumber", "StudentName" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "ClassStudentNameList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ClassRankList()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ClassRankList(Mandatory m)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            SectionServices SS = new SectionServices();
            ExamServices Es = new ExamServices();
            MarkTotalServices mts = new MarkTotalServices();
            try
            {
                if (ModelState.IsValid)
                {
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.exam = Es.GetExamName();
                    ViewBag.Classes = Cs.GetClass();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    var cid = Convert.ToInt32(m.Class);
                    ViewBag.cls = cid;
                    var sid = Convert.ToInt32(m.Rpt_Section);
                    ViewBag.section = SS.GetSection(cid);
                    ViewBag.sec = sid;
                    var eid = Convert.ToInt32(m.ExamTypeId);
                    ViewBag.e = eid;
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(cid);
                    ViewBag.ss = SS.GetParticularSection(sid);
                    ViewBag.exam1 = Es.GetParticularExamName(eid);
                    var ans1 = mts.GetStudentRankList(yid, cid, sid, eid);
                    ViewBag.view = ans1.Count;
                    TempData["res"] = ans1;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["res"];
            return View(ans);
        }
        //jQgridtable for ClassSIncharge
        public JsonResult JQgridClassRankList(string sidx, string sord, int page, int rows, int AcademicYear, int Class, int Rpt_Section, int ExamTypeId)//
        {
          IList<Ranklist> StudentrankLists = new List<Ranklist>();
            MarkTotalServices mts = new MarkTotalServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (AcademicYear != null && Class != null && Rpt_Section != null)
            {
                StudentrankLists = mts.GetStudentRankList(AcademicYear, Class, Rpt_Section, ExamTypeId);
            }
            int totalRecords = StudentrankLists.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    StudentrankLists = StudentrankLists.OrderByDescending(s => s.rollnumber).ToList();
                    StudentrankLists = StudentrankLists.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    StudentrankLists = StudentrankLists.OrderBy(s => s.rollnumber).ToList();
                    StudentrankLists = StudentrankLists.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = StudentrankLists;
           
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = StudentrankLists
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ClassRankListReport_ExportToExcel()
        {
            try
            {
                List<Ranklist> StudentrankLists = (List<Ranklist>)Session["JQGridList"];
                var list = StudentrankLists.Select(o => new { StudentId = o.StudentId, rollnumber = o.rollnumber, StudentName = o.StudentName, total = o.total, Rank = o.Rank, Result = o.Result }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassStudentRankReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("ClassRankList");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ClassRankListReport_ExportToPdf()
        {
            try
            {
                List<Ranklist> StudentrankLists = (List<Ranklist>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassStudentRankReport");
                GeneratePDF.ExportPDF_Portrait(StudentrankLists, new string[] { "StudentId", "rollnumber", "StudentName", "total", "Rank", "Result" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "StudentRankList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult StudentAttendance()
        {
            return View();
        }
        [HttpPost]
        public ActionResult StudentAttendance(Mandatory m, FormCollection fm)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            SectionServices SS = new SectionServices();
            ReportHelper RptHelp = new ReportHelper();
            StudentDailyAttendanceServices sdas = new StudentDailyAttendanceServices();
            AssignClassToStudentServices acts = new AssignClassToStudentServices();
            MonthsServices Ms = new MonthsServices();
            try
            {
                if (ModelState.IsValid)
                {
                    TempData["show"] = "show";
                    ArrayList s1 = new ArrayList();
                    ArrayList roll = new ArrayList();
                    ArrayList r1 = new ArrayList();
                    ArrayList name = new ArrayList();
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.Classes = Cs.GetClass();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    var cid = Convert.ToInt32(m.Class);
                    ViewBag.cls = cid;
                    var sid = Convert.ToInt32(m.Rpt_Section);
                    ViewBag.section = SS.GetSection(cid);
                    ViewBag.sec = sid;
                    var rid = Convert.ToInt32(m.ReportType);
                    ViewBag.rid = rid;
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(cid);
                    ViewBag.ss = SS.GetParticularSection(sid);
                    ViewBag.reporttype = getrtype();
                    ViewBag.month = Ms.GetMonths();
                    var type = Convert.ToInt32(m.ReportType);
                    if (type == 1)
                    {
                        ViewBag.type = "daily";
                        DateTime da = Convert.ToDateTime(fm["Dates"]);
                        var ans1 = sdas.GetStudentDailyAttendance(yid, cid, sid, da);
                        var tt = acts.GetClassStudent(yid, cid, sid);
                        ViewBag.total = tt.Count;
                        TempData["ans"] = ans1;
                        ViewBag.view = ans1.Count;
                        var p = sdas.GetStudentDailyPresentStatus(yid, cid, sid, da);
                        ViewBag.NOP = p.Count;
                        var a1 = sdas.GetStudentDailyAbsentStatus(yid, cid, sid, da);
                        ViewBag.NOA = a1.Count;
                        ViewBag.da = da.ToShortDateString();
                    }
                    else if (type == 2)
                    {
                        ViewBag.type = "from/to";
                        DateTime from1 = Convert.ToDateTime(fm["Fromdate"]);
                        DateTime to = Convert.ToDateTime(fm["Todate"]);
                        var ans1 = sdas.GetStudentFromToDateAttendance(yid, cid, sid, from1, to);
                        foreach (var v in ans1)
                        {
                            string r = v.rollnumber;
                            string d = v.date.Day.ToString();
                            string na = v.stuname;
                            if (!roll.Contains(r))
                            { roll.Add(r); }
                            if (!s1.Contains(d))
                            {
                                s1.Add(d);
                            }
                            if (!name.Contains(na))
                            {
                                name.Add(na);
                            }
                        }
                        for (int i = 0; i < roll.Count; i++)
                        {
                            string r = roll[i].ToString();
                            var count = sdas.GetStudentFromToDatePresentStatus(yid, cid, sid, from1, to, r);
                            r1.Add(count.Count);
                        }
                        ViewBag.NOP = r1;
                        ViewBag.count = name.Count;
                        ViewBag.date = s1;
                        ViewBag.from1 = from1.ToShortDateString();
                        ViewBag.to = to.ToShortDateString();
                        TempData["ans"] = ans1;
                        ViewBag.view = ans1.Count;
                    }
                    else if (type == 3)
                    {
                        ViewBag.type = "month";
                        var id = Convert.ToInt32(fm["month"]);
                        var ans1 = sdas.GetStudentMonthAttendance(yid, cid, sid, id);
                        ViewBag.id = id;
                        foreach (var v in ans1)
                        {
                            string r = v.rollnumber;
                            string d = v.date.Day.ToString();
                            string na = v.stuname;
                            if (!roll.Contains(r))
                            { roll.Add(r); }
                            if (!s1.Contains(d))
                            {
                                s1.Add(d);
                            }
                            if (!name.Contains(na))
                            {
                                name.Add(na);
                            }
                        }
                        for (int i = 0; i < roll.Count; i++)
                        {
                            string r = roll[i].ToString();
                            var count = sdas.GetStudentMonthPresentStatus(yid, cid, sid, id, r);
                            r1.Add(count.Count);
                        }
                        ViewBag.NOP = r1;
                        ViewBag.count = name.Count;
                        ViewBag.date1 = s1;
                        ViewBag.mid = Ms.GetParticularMonth(id);
                        TempData["ans"] = ans1;
                        ViewBag.view = ans1.Count;
                    }
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        [HttpPost]
        public JsonResult JsonStudentAttendance(VM_IndividualStudentAttendance s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    StudentDailyAttendanceServices sdas = new StudentDailyAttendanceServices();
                    AcademicyearServices acyear = new AcademicyearServices();
                    var rid = Convert.ToInt32(s.ReportType);
                    if (rid == 1)
                    {
                        var ans1 = sdas.GetStudentDailyAttendance(s.AcademicYear, s.Class, s.Rpt_Section, s.Dates);
                        return Json(new { AttendanceList = ans1 }, JsonRequestBehavior.AllowGet);
                    }
                    else if (rid == 2)
                    {
                        var ans1 = sdas.GetStudentFromToDateAttendance(s.AcademicYear, s.Class, s.Rpt_Section, s.FromDates, s.Todates);
                        string[] roll = new string[100];
                        string[] s1 = new string[100];
                        int Roll_Count = 0;
                        int date_Count = 0;
                        foreach (var v in ans1)
                        {
                            string r = v.rollnumber;
                            string d = v.date.Day.ToString();
                            string na = v.stuname;
                            if (!roll.Contains(r))
                            {
                                roll[Roll_Count] = r;
                                Roll_Count++;
                            }
                            if (!s1.Contains(d))
                            {
                                s1[date_Count] = d;
                                date_Count++;
                            }
                        }
                        return Json(new { AttendanceList = ans1, rollNumber = roll, Attendancedates = s1, Roll_Count = Roll_Count, date_Count = date_Count }, JsonRequestBehavior.AllowGet);
                    }
                    else if (rid == 3)
                    {
                        var ans1 = sdas.GetStudentMonthAttendance(s.AcademicYear, s.Class, s.Rpt_Section, s.month);
                        string[] roll = new string[100];
                        string[] s1 = new string[100];
                        int Roll_Count = 0;
                        
                        foreach (var v in ans1)
                        {
                            string r = v.rollnumber;
                            string d = v.date.Day.ToString();
                            string na = v.stuname;
                            if (!roll.Contains(r))
                            {
                                roll[Roll_Count] = r;
                                Roll_Count++;
                            }                           
                        }
                        int y = acyear.GetmonthDates(Convert.ToInt16(s.month), s.AcademicYear);
                        int date_Count = DateTime.DaysInMonth(y, Convert.ToInt16(s.month));

                        for (int i = 0; i < date_Count; i++)
                        {
                            string date = s.month + "/" + (i + 1) + "/" + y;
                            DateTime dateValue = DateTime.Parse(date, CultureInfo.InvariantCulture);
                            DateTimeOffset dateOffsetValue = new DateTimeOffset(dateValue,
                                     TimeZoneInfo.Local.GetUtcOffset(dateValue));
                            string ss;
                            ss = dateValue.ToString("ddd");
                            s1[i] = ss.Substring(0, 2);
                        }                                                
                        return Json(new { AttendanceList = ans1, rollNumber = roll, Attendancedates = s1, Roll_Count = Roll_Count, date_Count = date_Count }, JsonRequestBehavior.AllowGet);
                    }
                    string Success = "";
                    return Json(new { Success = Success }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ClassTimeTable()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ClassTimeTable(Mandatory m)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            SectionServices SS = new SectionServices();
            AssignSubjectToPeriodServices astps = new AssignSubjectToPeriodServices();
            SchoolSettingsServices sss = new SchoolSettingsServices();
            try
            {
                if (ModelState.IsValid)
                {
                    ArrayList Day = new ArrayList();
                    ArrayList Period = new ArrayList();
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.Classes = Cs.GetClass();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    var cid = Convert.ToInt32(m.Class);
                    ViewBag.cls = cid;
                    var sid = Convert.ToInt32(m.Rpt_Section);
                    ViewBag.section = SS.GetSection(cid);
                    ViewBag.sec = sid;
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(cid);
                    ViewBag.ss = SS.GetParticularSection(sid);
                    var ans1 = astps.GetClassTimeTable(yid, cid, sid);
                    TempData["res"] = ans1;
                    ViewBag.view = ans1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["res"];
            return View(ans);
        }
        [HttpPost]
        public JsonResult JsonClassTimeTable(VM_ClassTimeTableValidation s)
        {
            try
            {
                if (ModelState.IsValid)
                
                {
                    AssignSubjectToPeriodServices astps = new AssignSubjectToPeriodServices();
                    TimeScheduleServices tss = new TimeScheduleServices();
                    SchoolSettingsServices sss = new SchoolSettingsServices();
                    int year = s.AcademicYear;
                    int cid = s.Class;
                    int sec = s.Rpt_Section;
                    var dayorder = sss.GetSchoolDayOrder();
                    var DayOrder_Count = dayorder.DayOrder;
                    var PeriodsList = tss.GetClassPeriodTimeTable(cid);
                    int Periodcount = tss.GetClassPeriodCount(cid);
                    var StudentPeriodSubject = astps.GetClassTimeTable(year, cid, sec);
                    return Json(new { StudentPeriodSubject = StudentPeriodSubject, DayOrder = DayOrder_Count, PeriodsList = PeriodsList, Periodcount = Periodcount }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult MarkList()
        {
            return View();
        }
        [HttpPost]
        public ActionResult MarkList(Mandatory m1)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            SectionServices SS = new SectionServices();
            ExamServices Es = new ExamServices();
            AssignSubjectToSectionServices astss = new AssignSubjectToSectionServices();
            StudentMarkServices sms = new StudentMarkServices();
            MarkTotalServices mts = new MarkTotalServices();
            try
            {
                if (ModelState.IsValid)
                {
                    ArrayList sub = new ArrayList();
                    ArrayList rgid = new ArrayList();
                    ArrayList roll = new ArrayList();
                    ArrayList stuname = new ArrayList();
                    ArrayList total = new ArrayList();
                    ArrayList rank = new ArrayList();
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    ViewBag.exam = Es.GetExamName();
                    ViewBag.Classes = Cs.GetClass();
                    var yid = Convert.ToInt32(m1.AcademicYear);
                    ViewBag.y = yid;
                    var cid = Convert.ToInt32(m1.Class);
                    ViewBag.cls = cid;
                    var sid = Convert.ToInt32(m1.Rpt_Section);
                    ViewBag.section = SS.GetSection(cid);
                    ViewBag.sec = sid;
                    ViewBag.yy = As.GetParticularYear(yid);
                    ViewBag.cc = Cs.GetParticularClass(cid);
                    ViewBag.ss = SS.GetParticularSection(sid);
                    var eid = Convert.ToInt32(m1.ExamTypeId);
                    ViewBag.e = eid;
                    ViewBag.exam11 = Es.GetParticularExamName(eid);
                    var s = astss.SearchClassSubject(yid, cid, sid);
                    foreach (var v in s)
                    {
                        sub.Add(v.subjectName);
                    }
                    ViewBag.subject = sub;
                    var ans1 = sms.GetStudentmarksList(yid, cid, sid, eid);
                    foreach (var b in ans1)
                    {
                        if (!rgid.Contains(b.StudentRegId))
                        {
                            rgid.Add(b.StudentRegId);
                        }
                    }
                    ViewBag.rid = rgid;
                    var a1 = mts.GetAllStudentMarkList(yid, cid, sid, eid);
                    foreach (var b in a1)
                    {
                        roll.Add(b.rollnumber);
                        stuname.Add(b.stuname);
                        total.Add(b.total);
                        rank.Add(b.rank);
                    }
                    ViewBag.roll = roll;
                    ViewBag.stuname = stuname;
                    ViewBag.total = total;
                    ViewBag.rank = rank;
                    TempData["ans"] = ans1;
                    ViewBag.view = ans1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        [HttpPost]
        public JsonResult JsonClassMarkList(VM_ClassMarkListValidation s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    MarkTotalServices mts = new MarkTotalServices();
                    StudentMarkServices sms = new StudentMarkServices();
                    AssignSubjectToSectionServices astss = new AssignSubjectToSectionServices();
                    int year = s.AcademicYear;
                    int cid = s.Class;
                    int sec = s.Rpt_Section;
                    int examtypeid = s.ExamType;
                    var ClassTotalMarkList = mts.GetTotalMarkList(year, cid, sec, examtypeid);
                    var ClassSubjectMark = sms.GetStudentmarksList(year, cid, sec, examtypeid);
                    var ss = astss.SearchClassSubject(year, cid, sec);
                    return Json(new { TotalMark = ClassTotalMarkList, ClassSubjectMark = ClassSubjectMark, Subject = ss }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}