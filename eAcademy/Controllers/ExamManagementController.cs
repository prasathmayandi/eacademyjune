﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class ExamManagementController : Controller
    {
        //
        // GET: /ExamManagement/

        UserActivityHelper useractivity = new UserActivityHelper();

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            ClassServices clas = new ClassServices();
            ExamServices exam = new ExamServices();
            ViewBag.InvAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.allAcademicYears = AcYear.AddFormAcademicYear();
            ViewBag.allClasses = clas.ShowClasses();
            ViewBag.allExams = exam.ShowExams();
            ViewBag.allClassRooms = ShowClassRooms();
        }

        public IEnumerable<Object> ShowClassRooms()
        {
            ClassRoomServices classRoom = new ClassRoomServices();
            var ClassRoomList = classRoom.ShowClassRooms();
            return ClassRoomList;
        }
        public JsonResult GetSelectedClassSection(int passClassId)
        {
            try
            {
                SectionServices section = new SectionServices();
                var List = section.getSection(passClassId);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSelectedClassSectionSubject(int passYearId, int passClassId, int passSecId)
        {
            try
            {
                AssignSubjectToSectionServices secSub = new AssignSubjectToSectionServices();
                var SubjectList = secSub.ShowSubjects(passYearId, passClassId, passSecId);
                return Json(SubjectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getDate(int acid)
        {
            try
            {
                AcademicyearServices AcYear = new AcademicyearServices();
                var ans = AcYear.date(acid);
                string sdate = ans.StartDate.ToString("yyyy/MM/yyyy");
                string edate = ans.EndDate.ToString("yyyy/MM/yyyy");
                return Json(new { sd = sdate, ed = edate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetRollNumber(int passYearId, int passClassId, int passSectionId)
        {
            try
            {
                StudentRollNumberServices rollNum = new StudentRollNumberServices();
                var RollNumberList = rollNum.GetRollNumber(passYearId, passClassId, passSectionId);
                return Json(RollNumberList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetEndRollNumber(int yId, int cId, int sId, int passStartRollNumberId)
        {
            try
            {
                StudentRollNumberServices rollNum = new StudentRollNumberServices();
                var RollNumberList = rollNum.GetRollNumber(yId, cId, sId, passStartRollNumberId);
                return Json(RollNumberList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        // Exam Class Room (New)
        public JsonResult GetRoomCapacity(int? passYearId, int passClassId, int passSecId, int passExamId, int passSubjectId, int passRoomNo)  
        {
            try
            {
                ClassRoomServices classRoom = new ClassRoomServices();
                var CheckRoomCapacity = classRoom.getClassRoom(passRoomNo);
                string PleaseAssignCapacity = ""; string showCountDetails = ""; string ClassRoomFull = "";
                if (CheckRoomCapacity == null)
                {
                    PleaseAssignCapacity = ResourceCache.Localize("Please_assign_capacity_for_class_room");
                    return Json(new { PleaseAssignCapacity }, JsonRequestBehavior.AllowGet);
                }
                int? RoomCapacity = CheckRoomCapacity.Capacity;
                int? AssignedStudentCount = 0;

                ExamTimetableServices examTt = new ExamTimetableServices();
                var getExamDate = examTt.getExamDate(passYearId, passClassId, passExamId, passSubjectId);
                DateTime ExamDate = DateTimeByZone.getCurrentDate();
                int schedule;
                TimeSpan stime; TimeSpan etime;
                ExamTimeScheduleServices examTimeSchdule = new ExamTimeScheduleServices();
                if (getExamDate != null)
                {
                    ExamDate = getExamDate.ExamDate.Value;
                    schedule = getExamDate.TimeScheduleId.Value;

                    var scheduleDetail = examTimeSchdule.getRow(schedule);
                    stime = scheduleDetail.StartTime.Value;
                    etime = scheduleDetail.EndTime.Value;

                    var SchduleListBetweenTime = examTimeSchdule.getSchduleListBetweenTime(stime, etime);
                    for (int i = 0; i < SchduleListBetweenTime.Count; i++)
                    {
                        int ScId = Convert.ToInt32(SchduleListBetweenTime[i].ScheduleId);
                        AssignExamClassRoomServices examClassRoom = new AssignExamClassRoomServices();
                        var StudentCountList = examClassRoom.GetStudentCount(passYearId, passExamId, ExamDate, ScId, passRoomNo);
                        int StudentCountList1 = StudentCountList.Count;
                        for (int j = 0; j < StudentCountList1; j++)
                        {
                            int? capacity = StudentCountList[j].NumberOfStudents;
                            AssignedStudentCount = AssignedStudentCount + capacity;
                        }
                    }
                }

                int? availableCapacity = RoomCapacity - AssignedStudentCount;

                if (availableCapacity == 0)
                {
                    ClassRoomFull = ResourceCache.Localize("Class_room_full_please_choose_any_other_room");
                    return Json(new { ClassRoomFull, PleaseAssignCapacity }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    showCountDetails = "Yes";
                    return Json(new { showCountDetails, RoomCapacity, AssignedStudentCount, availableCapacity, PleaseAssignCapacity, ClassRoomFull }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string Exception = e.Message;
                return Json(new { Exception }, JsonRequestBehavior.AllowGet);
            }
         
        }

        public ActionResult ExamClassRoom()
        {
            return View();
        }

        public ActionResult AddExamClassRoom(EM_AllotClassRoom cr)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AcademicyearServices acyearServ = new AcademicyearServices();
                    bool existacademicyear = acyearServ.PastAcademicyear(cr.allAcademicYears.Value);
                    if (existacademicyear == true)
                    {
                        AssignExamClassRoomServices examClassRoom = new AssignExamClassRoomServices();
                        AssignClassToStudentServices aces = new AssignClassToStudentServices();
                        var YearEndResult = aces.checkAcademicYearResult(cr.allAcademicYears, cr.allClass, Convert.ToInt32(cr.Section));
                        if (YearEndResult == null)
                        { 
                        ExamTimetableServices examTt = new ExamTimetableServices();
                        var getExamDate = examTt.getExamDate(cr.allAcademicYears, cr.allClass, cr.allExams, cr.allSubjects);
                        DateTime ExamDate = DateTimeByZone.getCurrentDate();
                        int secId = Convert.ToInt32(cr.Section);

                        var checkExamExist = examClassRoom.checkExamStatus(cr.allAcademicYears, cr.allClass, secId, cr.allSubjects, cr.allExams);
                        if (checkExamExist.Count() == 0)
                        {
                            int startRollNumberId = cr.start_RollNumber;
                            int endRollNumberId = cr.end_RollNumber;
                            StudentRollNumberServices stuRollNum = new StudentRollNumberServices();
                            var getStartRollNumber = stuRollNum.getRollNumberDetail(startRollNumberId);

                            string StartRollNumber = "", EndRollNumber = "";
                            int? StartRegisterId = 0, EndRegisterId = 0;
                            int StartRollNumberId = 0, EndRollNumberId = 0;
                            if (getStartRollNumber != null)
                            {
                                StartRollNumber = getStartRollNumber.RollNumber;
                                StartRegisterId = getStartRollNumber.StudentRegisterId;
                                StartRollNumberId = startRollNumberId;
                            }

                            var getEndRollNumber = stuRollNum.getRollNumberDetail(endRollNumberId);
                            if (getEndRollNumber != null)
                            {
                                EndRollNumber = getEndRollNumber.RollNumber;
                                EndRegisterId = getEndRollNumber.StudentRegisterId;
                                EndRollNumberId = endRollNumberId;
                            }
                            // add 
                            var checkRollNumberExist = examClassRoom.checkRollNumberExist(cr.allAcademicYears, cr.allClass, secId, cr.allSubjects, cr.allExams);
                            if (checkRollNumberExist != null)
                            {
                                for (int i = 0; i < checkRollNumberExist.Count; i++)
                                {
                                    int ExistStartRollNumberId = checkRollNumberExist[i].StartRollNumberId;
                                    int ExistEndRollNumberId = checkRollNumberExist[i].EndRollNumberId;

                                    int f;
                                    int fCount = (ExistEndRollNumberId - ExistStartRollNumberId) + 1;
                                    int[] ExistIds = new int[fCount];
                                    for (f = 0; f < fCount; f++)
                                    {
                                        ExistIds[f] = ExistStartRollNumberId;
                                        ExistStartRollNumberId++;
                                    }
                                    if (ExistIds.Contains(StartRollNumberId) || ExistIds.Contains(EndRollNumberId))
                                    {
                                        string ErrorMessage = ResourceCache.Localize("Exam_room_already_assigned_for_selected_Roll_Number");
                                        return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }

                            int x = 0;
                            for (int i = startRollNumberId; i <= endRollNumberId; i++)
                            {
                                x++;
                            }
                            //ExamTimetableServices examTt = new ExamTimetableServices();
                            //var getExamDate = examTt.getExamDate(cr.allAcademicYears, cr.allClass, cr.allExams, cr.allSubjects);
                            //DateTime ExamDate = DateTime.Today;
                            int schedule = 0;
                            if (getExamDate == null)
                            {
                                string ErrorMessage = ResourceCache.Localize("Please_assign_exam_date_in_Timetable_Management_before_assigning_exam_room");
                                return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                            if (getExamDate != null)
                            {
                                ExamDate = getExamDate.ExamDate.Value;
                                schedule = getExamDate.TimeScheduleId.Value;
                            }
                            int NumberOfStudents = x;

                            // check space available 
                            ClassRoomServices classRoom = new ClassRoomServices();
                            var CheckRoomCapacity = classRoom.getClassRoom(cr.RoomNumberId);
                            if (CheckRoomCapacity == null)
                            {
                                string ErrorMessage = ResourceCache.Localize("Please_assign_capacity_for_class_room");
                                return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                            int? RoomCapacity = CheckRoomCapacity.Capacity;
                            int? AssignedStudentCount = 0;


                            int schedule1;
                            TimeSpan stime; TimeSpan etime;
                            ExamTimeScheduleServices examTimeSchdule = new ExamTimeScheduleServices();

                            if (getExamDate != null)
                            {
                                ExamDate = getExamDate.ExamDate.Value;
                                schedule1 = getExamDate.TimeScheduleId.Value;

                                var scheduleDetail = examTimeSchdule.getRow(schedule1);
                                stime = scheduleDetail.StartTime.Value;
                                etime = scheduleDetail.EndTime.Value;

                                var SchduleListBetweenTime = examTimeSchdule.getSchduleListBetweenTime(stime, etime);
                                for (int i = 0; i < SchduleListBetweenTime.Count; i++)
                                {
                                    int ScId = Convert.ToInt32(SchduleListBetweenTime[i].ScheduleId);
                                    var StudentCountList = examClassRoom.GetStudentCount(cr.allAcademicYears, cr.allExams, ExamDate, ScId, cr.RoomNumberId);
                                    int StudentCountList1 = StudentCountList.Count;
                                    for (int j = 0; j < StudentCountList1; j++)
                                    {
                                        int? capacity = StudentCountList[j].NumberOfStudents;
                                        AssignedStudentCount = AssignedStudentCount + capacity;
                                    }
                                }
                            }

                            int? availableCapacity = RoomCapacity - AssignedStudentCount;

                            if (availableCapacity == 0)
                            {
                                string ErrorMessage = ResourceCache.Localize("Class_room_full_please_choose_any_other_room");
                                return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                examClassRoom.addAllotExamClassRoom(cr.allAcademicYears, cr.allClass, secId, cr.allSubjects, cr.allExams, ExamDate, StartRollNumber, EndRollNumber, StartRegisterId, EndRegisterId, startRollNumberId, endRollNumberId, NumberOfStudents, cr.RoomNumberId, schedule);
                                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Add_exam_room"));
                                string Message = ResourceCache.Localize("Exam_room_assigned_successfully");
                                return Json(new { Message }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            string ErrorMessage = ResourceCache.Localize("Exam_Was_Finished_So_Cannot_assign_the_Student");
                            return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        }
                        else
                        {
                            string ErrorMessage = ResourceCache.Localize("Academicyear_result_is_Created_So_Unable_to_asssign_Exam_Room");
                            return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                        return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExamRoomListResults(string sidx, string sord, int page, int rows, int? allAcademicYears_List, int? allClass_List, string Section_List, int? allExams_List, string Subjects_List)
        {
              IList<EM_ExamClassRoomList> ExamRoomList = new List<EM_ExamClassRoomList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            AssignExamClassRoomServices examClassRoom = new AssignExamClassRoomServices();

            if (allAcademicYears_List != null || allClass_List != null || Section_List != null || allExams_List != null || Subjects_List != null)
            {
                int sectionId = Convert.ToInt32(Section_List);
                int subjectId = Convert.ToInt32(Subjects_List);
                int examId = allExams_List.Value;
                ExamRoomList = examClassRoom.getExamClassRoomList(allAcademicYears_List, allClass_List, sectionId, examId, subjectId);
            }

            int totalRecords = ExamRoomList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    ExamRoomList = ExamRoomList.OrderByDescending(s => s.RoomNumber).ToList();
                    ExamRoomList = ExamRoomList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ExamRoomList = ExamRoomList.OrderBy(s => s.RoomNumber).ToList();
                    ExamRoomList = ExamRoomList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = ExamRoomList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = ExamRoomList
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ExamRoom_ExportToExcel()
        {
            try
            {
                List<EM_ExamClassRoomList> ExamRoomList = (List<EM_ExamClassRoomList>)Session["JQGridList"];
                var list = ExamRoomList.Select(o => new { StartRollNumber = o.StartRollNumber, EndRollNumber = o.EndRollNumber, RoomNumber = o.RoomNumber }).ToList();
                string fileName = ResourceCache.Localize("ExamRoom");
                GenerateExcel.ExportExcel(list, fileName);
                return View("ExamClassRoom");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult ExamRoom_ExportToPdf()
        {
            try
            {
                List<EM_ExamClassRoomList> ExamRoomList = (List<EM_ExamClassRoomList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("Exam_Room");
                GeneratePDF.ExportPDF_Portrait(ExamRoomList, new string[] { "StartRollNumber", "EndRollNumber", "RoomNumber" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("ExamRoom.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult DeleteExamClassRoom(string passId)
        {
            try
            {
                int Did = Convert.ToInt32(QSCrypt.Decrypt(passId));
                AssignExamClassRoomServices examClassRoom = new AssignExamClassRoomServices();

                var getRow = examClassRoom.getRow(Did);
                if (getRow != null)
                {
                    int yearId = getRow.AcademicYearId.Value;
                    int classId = getRow.ClassId.Value;
                    int SecId = getRow.SectionId.Value;
                    int subId = getRow.SubjectId.Value;
                    int examId = getRow.ExamId.Value;
                    DateTime date = getRow.ExamDate.Value;
                    int schedule = getRow.TimeScheduleId.Value;
                    int startRollNumId = getRow.EndRollNumberId.Value;
                    int StartRegId = getRow.StartRegisterId.Value;

                    ExamAttendanceServices examAt = new ExamAttendanceServices();
                    var isAttendanceExist = examAt.CheckAttendanceExist(yearId, classId, SecId, subId, examId, date, schedule, startRollNumId, StartRegId);
                    if (isAttendanceExist != null)
                    {
                        string ErrorMessage = ResourceCache.Localize("Cannot_delete_exam_room_it_is_used_by_some_other_functionality");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }

                examClassRoom.DeleteExamClassRoom(Did);
                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Delete_exam_room"));
                string Message = ResourceCache.Localize("Exam_room_deleted_Successfully");
                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        // Assign Faculty to exam room
        public JsonResult GetScheduleByDate(int PassYearId, int PassExamId, DateTime PassDate)
        {
            ExamTimetableServices examTt = new ExamTimetableServices();
            var getSchedule = examTt.getScheduleBydate(PassYearId, PassExamId, PassDate);
            return Json(getSchedule, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AssignInvigilator()
        {
            ExamTimeScheduleServices tSchedule = new ExamTimeScheduleServices();
            ViewBag.TimeSchedule = tSchedule.ShowSchedule();
            return View();
        }

        public JsonResult GetExamDates(int PassYearId, int PassExamId)
        {
            ExamTimetableServices examTt = new ExamTimetableServices();
            var getDate = examTt.getExamDates(PassYearId, PassExamId).ToList();
            var count = getDate.Count;
            return Json(new { getDate = getDate, count = count }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExamAssignedRoom(int PassYearId, int PassExamId, DateTime PassDate, int PassSchedule)
        {
            AssignExamClassRoomServices examClassRoom = new AssignExamClassRoomServices();
            var checkExamRoomAllocated = examClassRoom.checkExamRoomAssigned(PassYearId, PassExamId, PassDate, PassSchedule);
            if(checkExamRoomAllocated == null)
            {
                string msg = "ExamRoomIsNotAssigned";
                return Json(new { msg }, JsonRequestBehavior.AllowGet);
            }

            TechemployeesServices techEmployee = new TechemployeesServices();
            var FacultyList = techEmployee.ShowTechFaculties();
            AssignFacultyToExamRoomServices FacultyExamRoom = new AssignFacultyToExamRoomServices();
            var checkFacultyAssigned = FacultyExamRoom.IsFacultyAssignedToExamRoom(PassYearId, PassExamId, PassDate, PassSchedule);
            if (checkFacultyAssigned != null)
            {
                string msg = "old";
                var ExistList = FacultyExamRoom.GetExistList(PassYearId, PassExamId, PassDate, PassSchedule);
                return Json(new { msg, ExistList, FacultyList }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string msg = "new";
                AssignExamClassRoomServices ExamRoom = new AssignExamClassRoomServices();
                var getAssignedExamRoomList = ExamRoom.getExamRoomList(PassYearId, PassExamId, PassDate, PassSchedule);
                int count = getAssignedExamRoomList.Count;

                return Json(new { msg, getAssignedExamRoomList, count, FacultyList }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveInvigilator(SaveInvigilator m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AcademicyearServices acyearServ = new AcademicyearServices();
                    bool existacademicyear = acyearServ.PastAcademicyear(m.AcademicYears);
                    if (existacademicyear == true)
                    {
                        var TotalCount = m.TotalCount;
                        int year = m.AcademicYears;
                        int ExamId = m.Exam;
                        DateTime date = m.Date;
                        int schedule = m.Schedule;
                        int size = TotalCount + 1;
                        string[] RoomId = new string[size];
                        int[] FacultyId = new int[size];
                        string[] Rid = new string[size];

                        ExamAttendanceServices examAtten = new ExamAttendanceServices();
                        var checkAttendanceTaken = examAtten.CheckAttendanceExistForSchedule(year, ExamId, date, schedule);
                        if (checkAttendanceTaken != null)
                        {
                            string ErrorMessage = ResourceCache.Localize("Cannot_change_invigilator_if_attendance_taken");
                            return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }

                        AssignFacultyToExamRoomServices FacultyExamRoom = new AssignFacultyToExamRoomServices();
                        var checkFacultyAssigned = FacultyExamRoom.IsFacultyAssignedToExamRoom(year, ExamId, date, schedule);

                        if (checkFacultyAssigned != null)
                        {
                            for (int i = 0; i <= TotalCount; i++)
                            {
                                RoomId[i] = m.ClassRoomId[i];
                                int roomid = Convert.ToInt32(QSCrypt.Decrypt(m.ClassRoomId[i]));
                                FacultyId[i] = m.EmployeeRegId[i];
                                Rid[i] = m.ID[i];
                                string rid = Rid[i];
                                int id = Convert.ToInt32(QSCrypt.Decrypt(rid));
                                FacultyExamRoom.UpdateInvigilator(id, year, ExamId, date, schedule, roomid, FacultyId[i]);
                            }
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Update_exam_invigilator"));
                            string Message = ResourceCache.Localize("Invigilator_updated_successfully");
                            return Json(new { Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            for (int i = 0; i <= TotalCount; i++)
                            {
                                RoomId[i] = m.ClassRoomId[i];
                                int roomid = Convert.ToInt32(QSCrypt.Decrypt(m.ClassRoomId[i]));
                                FacultyId[i] = m.EmployeeRegId[i];
                                FacultyExamRoom.addInvigilator(year, ExamId, date, schedule, roomid, FacultyId[i]);
                            }
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Add_exam_invigilator"));
                            string Message = ResourceCache.Localize("Invigilator_saved_successfully");
                            return Json(new { Message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                        return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckInvigilatorIsFree(int PassYearId, int PassExamId, DateTime passExamDate, int? passSchedule, int passEmpRegId)
        {
            AssignFacultyToExamRoomServices Faculty_ExamRoom = new AssignFacultyToExamRoomServices();
            var checkInvigilatorFree = Faculty_ExamRoom.checkInvigilatorIsFree(PassYearId, PassExamId, passExamDate, passEmpRegId);
            string Invigilator = "Free";
            TechemployeesServices techEmployee = new TechemployeesServices();
            var FacultyList = techEmployee.ShowTechFaculties();
            if (checkInvigilatorFree != null)
            {
                var InvigilatorScheduleList = Faculty_ExamRoom.getInvigilatorScheduleList(PassYearId, PassExamId, passExamDate, passEmpRegId);
                for (int i = 0; i < InvigilatorScheduleList.Count; i++)
                {
                    int? ExistScheduleId = InvigilatorScheduleList[i].TimeScheduleId;

                    if (ExistScheduleId == passSchedule)
                    {
                    }
                    else
                    {
                        ExamTimeScheduleServices ExamSchedule = new ExamTimeScheduleServices();
                        var ExistScheduleTime = ExamSchedule.getScheduleTiming(ExistScheduleId);
                        var Exist_stime = ExistScheduleTime.StartTime;
                        var Exist_etime = ExistScheduleTime.EndTime;
                        var CurrentScheduleTime = ExamSchedule.getScheduleTiming(passSchedule);
                        var Current_stime = CurrentScheduleTime.StartTime;
                        var Current_etime = CurrentScheduleTime.EndTime;
                        if (Exist_stime <= Current_stime && Current_stime <= Exist_etime)
                        {
                            Invigilator = "NotFree";
                            string ErrorMessage = ResourceCache.Localize("Selected_invigilator_is_not_free");
                            return Json(new { Invigilator, ErrorMessage, FacultyList }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else
            {
                return Json(new { Invigilator }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Invigilator }, JsonRequestBehavior.AllowGet);
        }

    }
}
