﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class RollNumberManagementController : Controller
    {
        AcademicyearServices obj_academicYear = new AcademicyearServices();
        RollNumberFormatServices obj_rollNumber = new RollNumberFormatServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ViewBag.acYear = obj_academicYear.AllList_AcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
        }
        public ActionResult FormatRollNumber()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult getClass()
        {
            ClassServices obj_class = new ClassServices();
            try
            {
                var ans = obj_class.Classes();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getSection(int cid)
        {
            SectionServices obj_section = new SectionServices();
            try
            {
                var ans = obj_section.getSectionByClassId(cid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
       
        public JsonResult FormatRollNumberList(string sidx, string sord, int page, int rows, int? yr_search)
        {
            IList<RollNumberList> todoListsResults1 = new List<RollNumberList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults1 = obj_rollNumber.getRollNumberFormatList();
            if (yr_search.HasValue)
            {
                todoListsResults1 = todoListsResults1.Where(p => p.AcademicYearId.Equals(yr_search)).ToList();
            }
            int totalRecords = todoListsResults1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults1 = todoListsResults1.OrderByDescending(s => s.ClassId).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults1 = todoListsResults1.OrderBy(s => s.ClassId).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults1;
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FormatExportToExcel()
        {
            try
            {
                List<RollNumberList> todoListsResults1 = (List<RollNumberList>)Session["JQGridList"];
                var list = todoListsResults1.Select(o => new { AcademicYear = o.AcademicYear, ClassSection = o.ClassSection, RollnumberFormat = o.RollNumberFormat, Status = Convert.ToString(o.Status) }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassRollNumberFormat");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult FormatExportToPDF()
        {
            try
            {
                List<RollNumberList> todoListsResults1 = (List<RollNumberList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassRollNumberFormat");
                GeneratePDF.ExportPDF_Portrait(todoListsResults1, new string[] { "AcademicYear", "ClassSection", "RollNumberFormat", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "ClassRollNumberFormat.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult AddRollNumber(RollNumber model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = Convert.ToInt32(model.acYear_Roll);
                    int cid = Convert.ToInt32(model.class_Roll);
                    int sec_id = Convert.ToInt32(model.Section_Roll);
                    string roll = (model.txt_rollNumber).ToString();
                    bool cc = obj_rollNumber.checkRollNumberFormat(acid, cid, sec_id);
                    bool cr = obj_rollNumber.checkRollNumberFormatExit(roll);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("FormatIsExist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else if (cr == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("FormatIsExist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_rollNumber.addRollNumberFormat(acid, cid, sec_id, roll);
                        string Message =eAcademy.Models.ResourceCache.Localize("FormatAddedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("AddRollNumber"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditRollNumber(string id)
        {
            try
            {
                int formatid = Convert.ToInt32(QSCrypt.Decrypt(id));
                var ans = obj_rollNumber.getRollNumberFormatById(formatid);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveEditRollNumber(EditRollNumber model)  //EditTerm
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int formatId = Convert.ToInt32(QSCrypt.Decrypt(model.FormatId));
                    string format = model.txt_rollNumber;
                    int acid = model.AcademicYearId;
                    int cid = model.ClassId;
                    int secid = model.SectionId;
                    string status = model.Status;
                    bool cc = obj_rollNumber.checkFormatAssigned(acid, cid, secid);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("Format_is_assingned._Should_not_edit");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    bool cc1 = obj_rollNumber.checkRollNumberFormat(acid, cid, secid, format, status);
                    if (cc1 == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("FormatIsExist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_rollNumber.updateRollNumberFormat(formatId, format, status);
                        string Message = eAcademy.Models.ResourceCache.Localize("UpdatedSuccessfully");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("UpdateRollNumber"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}