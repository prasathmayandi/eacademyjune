﻿using eAcademy.ViewModel;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using eAcademy.HelperClass;
using eAcademy.Models;

namespace eAcademy.Controllers
{
    [Student_Parent_Admin_SessionAttribute]
    public class AchievementsController : Controller
    {
        // GET: /Achievements/

        public int StudentRegId;
        public int yearId;
        public int classId;
        public int secId;

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            AssignClassToStudentServices clas = new AssignClassToStudentServices();

            StudentRegId = Convert.ToInt32(Session["StudentRegId"]);
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();

            var getRow = clas.getAssigendClass(StudentRegId);
            if (getRow != null)
            {
                yearId = Convert.ToInt32(getRow.AcademicYearId);
                classId = Convert.ToInt32(getRow.ClassId);
                secId = Convert.ToInt32(getRow.SectionId);
            }
            var s = Session["empRegId"];
            if (s != null)
            {
                ViewBag.empRegId = Convert.ToInt32(Session["empRegId"]);
            }
        }


        public ActionResult StudentAchievement()
        {

            return View();
        }

        public JsonResult StudentAchievementResults(string sidx, string sord, int page, int rows, string Achievement, DateTime? txt_doa_search, int? AcademicYear)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            StudentAchievementServices stuAchievement = new StudentAchievementServices();
            IList<AchievementList> Stu_AchievementList = new List<AchievementList>();
            Stu_AchievementList = stuAchievement.getStudentAchievement1();
            if (txt_doa_search.HasValue)
            {
                Stu_AchievementList = Stu_AchievementList.Where(p => p.txt_doa_search >= txt_doa_search).ToList();
            }

            if (!string.IsNullOrEmpty(Achievement))
            {
                Stu_AchievementList = Stu_AchievementList.Where(p => p.Achievement.Contains(Achievement)).ToList();
            }

            if (AcademicYear.HasValue)
            {
                Stu_AchievementList = Stu_AchievementList.Where(p => p.Ach_allAcademicYearsA1 == AcademicYear).ToList();
            }

            int totalRecords = Stu_AchievementList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    Stu_AchievementList = Stu_AchievementList.OrderByDescending(s => s.Achievement).ToList();
                    Stu_AchievementList = Stu_AchievementList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    Stu_AchievementList = Stu_AchievementList.OrderBy(s => s.Achievement).ToList();
                    Stu_AchievementList = Stu_AchievementList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = Stu_AchievementList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = Stu_AchievementList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult StuAchievement_ExportToExcel()
        {
            try
            {
                List<AchievementList> Stu_AchievementList = (List<AchievementList>)Session["JQGridList"];
                var list = Stu_AchievementList.Select(o => new { Date = o.Date, Achievement = o.Achievement, Description = o.Descriptions, Name = o.Name, Class = o.Class, Section = o.Section }).ToList();
                string fileName = ResourceCache.Localize("Student_Achievement");
                GenerateExcel.ExportExcel(list, fileName);
                return View("StudentAchievement");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult StuAchievement_ExportToPdf()
        {
            try
            {
                List<AchievementList> Stu_AchievementList = (List<AchievementList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("StudentAchievement");
                GeneratePDF.ExportPDF_Portrait(Stu_AchievementList, new string[] { "Date", "Achievement", "Descriptions", "Name", "Class", "Section" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("StudentAchievement.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult FacultyAchievement()
        {
            return View();
        }

        public JsonResult FacultyAchievementResults(string sidx, string sord, int page, int rows, string Achievement, DateTime? txt_doa_search, int? AcademicYear)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            EmployeeAchievementServices empAchievement = new EmployeeAchievementServices();
            IList<AchievementList> Faculty_AchievementList = new List<AchievementList>();
            Faculty_AchievementList = empAchievement.getEmployeeAchievement();
            if (txt_doa_search.HasValue)
            {
                Faculty_AchievementList = Faculty_AchievementList.Where(p => p.txt_doa_search >= txt_doa_search).ToList();
            }

            if (!string.IsNullOrEmpty(Achievement))
            {
                Faculty_AchievementList = Faculty_AchievementList.Where(p => p.Achievement.Contains(Achievement)).ToList();
            }

            if (AcademicYear.HasValue)
            {
                Faculty_AchievementList = Faculty_AchievementList.Where(p => p.Ach_allAcademicYearsA1 == AcademicYear).ToList();
            }

            int totalRecords = Faculty_AchievementList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    Faculty_AchievementList = Faculty_AchievementList.OrderByDescending(s => s.Achievement).ToList();
                    Faculty_AchievementList = Faculty_AchievementList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    Faculty_AchievementList = Faculty_AchievementList.OrderBy(s => s.Achievement).ToList();
                    Faculty_AchievementList = Faculty_AchievementList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = Faculty_AchievementList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = Faculty_AchievementList
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FacultyAchievement_ExportToExcel()
        {
            try
            {
                List<AchievementList> Faculty_AchievementList = (List<AchievementList>)Session["JQGridList"];
                var list = Faculty_AchievementList.Select(o => new { Date = o.Date, Achievement = o.Achievement, Description = o.Descriptions, Name = o.Name }).ToList();
                string fileName = ResourceCache.Localize("Faculty_Achievement");
                GenerateExcel.ExportExcel(list, fileName);
                return View("FacultyAchievement");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult FacultyAchievement_ExportToPdf()
        {
            try
            {
                List<AchievementList> Faculty_AchievementList = (List<AchievementList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("FacultyAchievement");
                GeneratePDF.ExportPDF_Portrait(Faculty_AchievementList, new string[] { "Date", "Achievement", "Descriptions", "Name" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("FacultyAchievement.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult SchoolAchievement()
        {
            return View();
        }

        public JsonResult SchoolAchievementResults(string sidx, string sord, int page, int rows, string Achievement, DateTime? txt_doa_search, int? AcademicYear)
        {
            IList<AchievementList> School_AchievementList = new List<AchievementList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            SchoolAchievementServices schoolAchievement = new SchoolAchievementServices();
            School_AchievementList = schoolAchievement.getSchoolAchievement1();
            if (txt_doa_search.HasValue)
            {
                School_AchievementList = School_AchievementList.Where(p => p.txt_doa_search >= txt_doa_search).ToList();
            }

            if (!string.IsNullOrEmpty(Achievement))
            {
                School_AchievementList = School_AchievementList.Where(p => p.Achievement.Contains(Achievement)).ToList();
            }

            if (AcademicYear.HasValue)
            {
                School_AchievementList = School_AchievementList.Where(p => p.Ach_allAcademicYearsA1 == AcademicYear).ToList();
            }

            int totalRecords = School_AchievementList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    School_AchievementList = School_AchievementList.OrderByDescending(s => s.Achievement).ToList();
                    School_AchievementList = School_AchievementList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    School_AchievementList = School_AchievementList.OrderBy(s => s.Achievement).ToList();
                    School_AchievementList = School_AchievementList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = School_AchievementList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = School_AchievementList
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SchoolAchievement_ExportToExcel()
        {
            try
            {
                List<AchievementList> School_AchievementList = (List<AchievementList>)Session["JQGridList"];
                var list = School_AchievementList.Select(o => new { Date = o.Date, Achievement = o.Achievement, Description = o.Descriptions, Name = o.Name }).ToList();
                string fileName = ResourceCache.Localize("School_Achievement");
                GenerateExcel.ExportExcel(list, fileName);
                return View("SchoolAchievement");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult SchoolAchievement_ExportToPdf()
        {
            try
            {
                List<AchievementList> School_AchievementList = (List<AchievementList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("SchoolAchievement");
                GeneratePDF.ExportPDF_Portrait(School_AchievementList, new string[] { "Date", "Achievement", "Descriptions", "Name" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("SchoolAchievement.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }


    }
}