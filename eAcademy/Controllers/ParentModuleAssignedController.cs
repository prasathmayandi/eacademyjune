﻿using eAcademy.DataModel;
using eAcademy.ViewModel;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eAcademy.Models;
using eAcademy.Helpers;
using eAcademy.HelperClass;

namespace eAcademy.Controllers
{
    [Parent_CheckSessionAttribute]
    public class ParentModuleAssignedController : Controller
    {
        //
        // GET: /ParentHomework/

        public int parentPrimaryUserRegId;
        public int StudentRegId;
        public int yearId;
        public int classId;
        public int secId;

        AssignClassToStudentServices assignStu = new AssignClassToStudentServices();
        HomeworkServices HW = new HomeworkServices();
        AssignmentServices Assgn = new AssignmentServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            parentPrimaryUserRegId = Convert.ToInt32(Session["ParentRegId"]);
            StudentRegId = Convert.ToInt32(Session["StudentRegId"]);
            AssignSubjectToSectionServices sub = new AssignSubjectToSectionServices();
            var getRow = assignStu.getAssigendClass(StudentRegId);
            if (getRow != null)
            {
                yearId = Convert.ToInt32(getRow.AcademicYearId);
                classId = Convert.ToInt32(getRow.ClassId);
                secId = Convert.ToInt32(getRow.SectionId);
                ViewBag.AllSubjects = sub.ShowSubjects(yearId, classId, secId);
                ViewBag.AllSubjects1 = sub.ShowSubjects(yearId, classId, secId);
            }
        }

        public ActionResult ELearning()
        {
            return View();
        }

        public JsonResult ELearnListResults(string sidx, string sord, int page, int rows, DateTime? ELearn_FromDate, DateTime? ELearn_ToDate)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            ELearningServices ELearn = new ELearningServices();
            IList<ELearningList> ELearnList = new List<ELearningList>();
            if (ELearn_FromDate != null && ELearn_ToDate != null)
            {
                ELearnList = ELearn.getStudentElearn(yearId, classId, secId, ELearn_FromDate.Value, ELearn_ToDate.Value);
            }
            int totalRecords = ELearnList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    ELearnList = ELearnList.OrderByDescending(s => s.Title).ToList();
                    ELearnList = ELearnList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    ELearnList = ELearnList.OrderBy(s => s.Title).ToList();
                    ELearnList = ELearnList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = ELearnList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = ELearnList
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ELearn_ExportToExcel()
        {
            try
            {
                List<ELearningList> ELearnList = (List<ELearningList>)Session["JQGridList"];
                var list = ELearnList.Select(o => new { DateOf_WorkPosted = o.PostedtDate, Title = o.Title, Description = o.Description, DateTo_CompleteWork = o.LastDate }).ToList();
                string fileName = ResourceCache.Localize("ELearningExcelFileName");
                GenerateExcel.ExportExcel(list, fileName);
                return View("ELearning");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult ELearn_ExportToPdf()
        {
            try
            {
                List<ELearningList> ELearnList = (List<ELearningList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("ELearningList");
                GeneratePDF.ExportPDF_Portrait(ELearnList, new string[] { "PostedtDate", "Title", "Descriptions", "LastDate" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("E-Learning.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public void OpenELearnFile(string id)
        {
            try
            {
                ELearningServices ELearn = new ELearningServices();
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                var getHomeworkFile = ELearn.getStudentELearnFile(fid).FileName;
                string file = getHomeworkFile;
                if (file != "No file")
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Documents/ELearning/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {
                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("content-length", FileBuffer.Length.ToString());
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
            }
        }

        public ActionResult Homework()
        {
            return View();
        }

        public JsonResult HomeworkListResults(string sidx, string sord, int page, int rows, int? SM_AllSubjects, DateTime? SM_Homework_FromDate, DateTime? SM_Homework_ToDate)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            IList<StudentHomework> HomeworkList = new List<StudentHomework>();
            if (SM_AllSubjects == 0 && SM_Homework_FromDate != null && SM_Homework_ToDate != null)
            {
                HomeworkList = HW.getStudentHomework1(yearId, classId, secId, SM_Homework_FromDate.Value, SM_Homework_ToDate.Value);
            }
            if (SM_AllSubjects != 0 && SM_Homework_FromDate != null && SM_Homework_ToDate != null)
            {
                HomeworkList = HW.getStudentHomework1(yearId, classId, secId, SM_AllSubjects.Value, SM_Homework_FromDate.Value, SM_Homework_ToDate.Value);
            }
            int totalRecords = HomeworkList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    HomeworkList = HomeworkList.OrderByDescending(s => s.Subject).ToList();
                    HomeworkList = HomeworkList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    HomeworkList = HomeworkList.OrderBy(s => s.Subject).ToList();
                    HomeworkList = HomeworkList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = HomeworkList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = HomeworkList
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Homework_ExportToExcel()
        {
            try
            {
                List<StudentHomework> HomeworkList = (List<StudentHomework>)Session["JQGridList"];
                var list = HomeworkList.Select(o => new { Subject = o.Subject, PostedDate = o.PostedDate, HomeWork = o.Homework, Description = o.Description, DateOfSubmission = o.DateOfSubmission }).ToList();
                string fileName = ResourceCache.Localize("HomeworkExcelFileName");
                GenerateExcel.ExportExcel(list, fileName);
                return View("Homework");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Homework_ExportToPdf()
        {
            try
            {
                List<StudentHomework> HomeworkList1 = (List<StudentHomework>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("HomeworkList");
                GeneratePDF.ExportPDF_Portrait(HomeworkList1, new string[] { "Subject", "DateOf_WorkPosted", "HomeWork1", "Descriptions", "DateTo_CompleteWork" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("HomeworkList.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public void OpenHomeworkFile(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                var getHomeworkFile = HW.getStudentHomeworkFile(fid).HomeWorkFileName;
                string file = getHomeworkFile;
                if (file != "No file")
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Documents/Homework/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {
                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("content-length", FileBuffer.Length.ToString());
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
            }
        }

        public ActionResult Assignment()
        {
            return View();
        }

        public JsonResult AssignmentListResults(string sidx, string sord, int page, int rows, int? SM_AllSubjects, DateTime? SM_Assignment_FromDate, DateTime? SM_Assignment_ToDate)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            IList<StudentAssignment> AssignmentList = new List<StudentAssignment>();
            if (SM_AllSubjects == 0 && SM_Assignment_FromDate != null && SM_Assignment_ToDate != null)
            {
                AssignmentList = Assgn.getStudentAssignment(yearId, classId, secId, SM_Assignment_FromDate.Value, SM_Assignment_ToDate.Value);

            }
            if (SM_AllSubjects != 0 && SM_Assignment_FromDate != null && SM_Assignment_ToDate != null)
            {
                AssignmentList = Assgn.getStudentAssignment(yearId, classId, secId, SM_AllSubjects.Value, SM_Assignment_FromDate.Value, SM_Assignment_ToDate.Value);
            }
            int totalRecords = AssignmentList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    AssignmentList = AssignmentList.OrderByDescending(s => s.Subject).ToList();
                    AssignmentList = AssignmentList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    AssignmentList = AssignmentList.OrderBy(s => s.Subject).ToList();
                    AssignmentList = AssignmentList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = AssignmentList;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = AssignmentList
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Assignment_ExportToExcel()
        {
            try
            {
                List<StudentAssignment> AssignmentList1 = (List<StudentAssignment>)Session["JQGridList"];
                var list = AssignmentList1.Select(o => new { Subject = o.Subject, PostedDate = o.PostedDate, Assignment = o.Assignment, Description = o.Description, SubmissionDate = o.SubmissionDate }).ToList();
                string fileName = ResourceCache.Localize("AssignmentExcelFileName");
                GenerateExcel.ExportExcel(list, fileName);
                return View("Assignment");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Assignment_ExportToPdf()
        {
            try
            {
                List<StudentAssignment> AssignmentList1 = (List<StudentAssignment>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("AssignmentList");
                GeneratePDF.ExportPDF_Portrait(AssignmentList1, new string[] { "Subject", "PostedDate", "Assignment", "Description", "SubmissionDate" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("AssignmentList.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public void OpenAssignmentFile(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                var getAssignmentFile = Assgn.getStudentAssignmentFile(fid).AssignmentFileName;
                string file = getAssignmentFile;
                if (file != "No file")
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Documents/Assignment/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {
                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("content-length", FileBuffer.Length.ToString());
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
            }
        }

        [EncryptedActionParameter]
        public ActionResult FacultyDetails(int id)
        {
            try
            {
                TechemployeesServices techEmp = new TechemployeesServices();
                var faculty = techEmp.getFacultyDetails(id);
                return View(faculty);
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult ModulesAssigned()
        {
            try
            {
                AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
                var list = facultySub.SubjectList(yearId, classId, secId);
                return View(list);
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public JsonResult feedback(string passId)
        {
            AssignFacultyToSubjectServices faculSub = new AssignFacultyToSubjectServices();
            int rowId = Convert.ToInt32(QSCrypt.Decrypt(passId));
            var SubName = faculSub.getSubName(rowId);
            var sub = SubName.Subject;
            var faculty = SubName.Faculty;
            return Json(new { subjectName = sub, FacultyName = faculty }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FacultyFeedback(ModuleAssignedFeedback m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AssignFacultyToSubjectServices faculSub = new AssignFacultyToSubjectServices();
                    int RowId = Convert.ToInt32(QSCrypt.Decrypt(m.rowId));
                    var FacultyId = faculSub.getFacultyId(RowId).EmployeeRegisterId.Value;
                    string Feedback = m.Feedback;

                    AssignFacultyToSubjectServices facultySub = new AssignFacultyToSubjectServices();
                    var getSubRow = facultySub.getFaculySubject(yearId, classId, secId, FacultyId);
                    int subId = Convert.ToInt32(getSubRow.SubjectId);
                    ParentFacultyFeedbackServices feedback = new ParentFacultyFeedbackServices();
                    feedback.addParentFacultyFeedback(parentPrimaryUserRegId, StudentRegId, yearId, classId, secId, subId, FacultyId, Feedback);
                    useractivity.AddParentActivityDetails(ResourceCache.Localize("Send_feedback_to_faculty"));
                    string Message = ResourceCache.Localize("Feedback_sent_to_Faculty");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("Please_fill_all_the_mandatory_fields");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

       
        public ActionResult Query()
        {
            return View();
        }

        public JsonResult QueryListResults(string sidx, string sord, int page, int rows, string MI_Query_List_Subject, DateTime? MI_Query_List_txt_fromDate, DateTime? MI_Query_List_txt_toDate)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            StudentfacultyquriesServices fq = new StudentfacultyquriesServices();
            IList<MI_Queries> QueryList1 = new List<MI_Queries>();
            if (MI_Query_List_Subject != "" && MI_Query_List_Subject != "all" && MI_Query_List_txt_fromDate != null && MI_Query_List_txt_toDate != null)
            {
                int subId = Convert.ToInt32(MI_Query_List_Subject);
                QueryList1 = fq.getParentQueryList(yearId, classId, secId, StudentRegId, subId, MI_Query_List_txt_fromDate, MI_Query_List_txt_toDate).ToList();
            }

            if (MI_Query_List_Subject != "" && MI_Query_List_Subject == "all" && MI_Query_List_txt_fromDate != null && MI_Query_List_txt_toDate != null)
            {
                QueryList1 = fq.getParentQueryListAll(yearId, classId, secId, StudentRegId, MI_Query_List_txt_fromDate, MI_Query_List_txt_toDate).ToList();
            }

            int totalRecords = QueryList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    QueryList1 = QueryList1.OrderByDescending(s => s.R_Query_FromDate).ToList();
                    QueryList1 = QueryList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    QueryList1 = QueryList1.OrderBy(s => s.R_Query_FromDate).ToList();
                    QueryList1 = QueryList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = QueryList1;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = QueryList1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Query_ExportToExcel()
        {
            try
            {
                List<MI_Queries> QueryList1 = (List<MI_Queries>)Session["JQGridList"];
                var list = QueryList1.Select(o => new { Date = o.Date, Subject = o.Subject, Faculty = o.Faculty, QueryFrom = o.QueryFrom, Query = o.Query, Reply = o.Reply }).ToList();
                string fileName = ResourceCache.Localize("QueryListExcelFileName");
                GenerateExcel.ExportExcel(list, fileName);
                return View("Query");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Query_ExportToPdf()
        {
            try
            {
                List<MI_Queries> QueryList1 = (List<MI_Queries>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("QueryList");
                GeneratePDF.ExportPDF_Portrait(QueryList1, new string[] { "Date", "Subject", "Faculty", "QueryFrom", "Query", "Reply" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("QueryList.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public JsonResult getFacultyName(int passSubId)
        {
            AssignFacultyToSubjectServices subFaculty = new AssignFacultyToSubjectServices();
            var GetFacultyName = subFaculty.GetSubFacultyName(yearId, classId, secId, passSubId);
            var FirstName = GetFacultyName.FacultyName;
            var LastName = GetFacultyName.LastName;
            var FacultyName = FirstName + " " + LastName;
            return Json(FacultyName, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddQuery()
        {
            try
            {
                string QueryFileName = null;
                var subId = Request["subId"];
                string query = Request["query"];

                if (subId != null && subId != "" && query != null)
                {
                    int subjectId = Convert.ToInt32(subId);

                    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                    {
                        var file1 = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
                        string filenamewithoutextention;
                        Random random = new Random();
                        string fname = random.Next(00000, 99999).ToString();
                        string extention = System.IO.Path.GetExtension(file1.FileName);

                        var fileSize = file1.ContentLength;
                        fileSize = file1.ContentLength / 1048576;

                        if (extention == ".pdf" || extention == ".docx" || extention == ".doc" || extention == "txt")
                        {
                            if (file1 != null && file1.ContentLength > 0 && fileSize <= 3)
                            {
                                filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file1.FileName);
                                QueryFileName = "Query" + fname + extention;
                                string path = System.IO.Path.Combine(Server.MapPath("~/Documents/Query"), QueryFileName);
                                file1.SaveAs(path);
                            }
                            else
                            {
                                string ErrorMessage = ResourceCache.Localize("FileSizeExceeds");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            string ErrorMessage = ResourceCache.Localize("FileFormatDoesNotSupport");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    AssignFacultyToSubjectServices subFaculty = new AssignFacultyToSubjectServices();
                    var FacultyId = subFaculty.GetSubFacultyName(yearId, classId, secId, subjectId).FacultyId;
                    StudentfacultyquriesServices FQ = new StudentfacultyquriesServices();
                    string QueryFrom = "Parent";
                    FQ.addQuery(yearId, classId, secId, subjectId, FacultyId, query, QueryFileName, QueryFrom, StudentRegId, parentPrimaryUserRegId);
                    useractivity.AddParentActivityDetails(ResourceCache.Localize("AddQuery"));
                    string Message = ResourceCache.Localize("Query_sent_successfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("PleaseFillAllMandatoryFields");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public void OpenQueryFile(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                StudentfacultyquriesServices FQ = new StudentfacultyquriesServices();
                var getQueryFile = FQ.getQueryFile(fid).QueryFile;
                string file = getQueryFile;
                if (file != null)
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Documents/Query/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {
                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("content-length", FileBuffer.Length.ToString());
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }

        public void OpenReplyFile(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                StudentfacultyquriesServices FQ = new StudentfacultyquriesServices();
                var getReplyFile = FQ.getQueryFile(fid).ReplyFile;
                string file = getReplyFile;
                if (file != null)
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Documents/QueryReply/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {
                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("content-length", FileBuffer.Length.ToString());
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }

        public JsonResult CurrentActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");

            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    int acid = CurrentAcYear[i].AcademicYearId;
                    return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }


    }
}
