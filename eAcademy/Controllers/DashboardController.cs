﻿using eAcademy.DataModel;
using eAcademy.Services;
using System;
using System.Linq;
using System.Web.Mvc;
using eAcademy.Models;
using eAcademy.HelperClass;
using eAcademy.ViewModel;

namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class DashboardController : Controller
    {
        public int EmployeeRegId;
      //  public String EmpUSerid;
        UserActivityHelper useractivity = new UserActivityHelper();

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            EmployeeRegId = Convert.ToInt32(Session["empRegId"]);

           // EmpUSerid = Convert.ToString(Session["username"]);
        }

        public JsonResult totalstudent()
        {
            try
            {
                StudentServices se = new StudentServices();
                var total = se.totalstudentstrength();
                var serverTime = DateTimeByZone.getCurrentDateTimeDashboard();
                return Json(new { total, serverTime }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Dashboard()
        {
            try
            {
                ViewBag.LastLogin = Session["LastLogin"];

                var currentDate = DateTimeByZone.getCurrentDateTime();   //DateTime.Today;  it gets server system date
                AnnouncementServices announce = new AnnouncementServices();
                var announcementList = announce.getAnnouncementList(currentDate);
                ViewBag.count = announcementList.Count; 

                // for Last Request
                FacultyleaverequestServices leaveReq = new FacultyleaverequestServices();
                var LeaveRequestStatus1 = leaveReq.getLastLeavReq(EmployeeRegId);
                if (LeaveRequestStatus1 != null)
                {
                    ViewBag.fdate = LeaveRequestStatus1.FromDate.Value.ToString("dd MMM, yyyy");
                    ViewBag.todate = LeaveRequestStatus1.ToDate.Value.ToString("dd MMM, yyyy"); 
                    ViewBag.status = LeaveRequestStatus1.Status;
                }

                //AssignClassTeacherServices classTeacher = new AssignClassTeacherServices();
                //var IsClassTeacher = classTeacher.isClassTeacher(EmployeeRegId);
                //if (IsClassTeacher != null)
                //{
                //    ViewBag.classTeacher = "Yes";
                //}
                return View(announcementList);
            }
            catch(Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public string OpenPopup(int id)
        {
            AnnouncementServices announce = new AnnouncementServices();
            var desc = announce.getAnnoucementDesc(id).AnnouncementDescription;
            return desc;
        }

        //[Authorize(Role="SuperAdmin")]
        public ActionResult UserProfile()
        {
            try
            {
                TechEmployeeServices obj_techEmp = new TechEmployeeServices();

                int empRegId=int.Parse(Session["empRegId"].ToString());
                var ans = obj_techEmp.getEmployeeByEmployeeRegisterId(empRegId);
                    
                    
                   
                return View(ans);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public ActionResult ChangePassword(EmployeeChangePassword model)
        {
            TechEmployeeServices obj_techEmp = new TechEmployeeServices();
            try
            {
                int empRegId = int.Parse(Session["empRegId"].ToString());
                if (ModelState.IsValid)
                {
                    string psw = model.Emp_NewPsw;
                    string cpsw = model.Emp_RePsw;
                    if (psw.Equals(cpsw))
                    {
                        obj_techEmp.UpdatePassword(empRegId,psw);
                        useractivity.AddEmployeeActivityDetails("Update password");
                        TempData["Message"] = "Password Changed Successfully";
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "New Password & Re-Enter Password is not Matched";
                    }


                }
                else
                {
                    TempData["modelState"] = "Enter all the Mandatory Fields";
                }
                return RedirectToAction("UserProfile");
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public ActionResult Error()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        //public JsonResult date()
        //{
        //    var ans = ee.Events.Select(q => new { dat = q.EventDate, name = q.EventName, id = q.EventId, des = q.EventDecribtion }).ToList();

        //    return Json(ans, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult QuickMail(QuickE_Mail model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string displayName = Session["EmployeeName"].ToString();
                    string from_mail = Session["mail"].ToString();
                    string to_mail = model.emailto;
                    string subject = model.subject;
                    string body = model.message;
                    string PlaceName = "Dashboard-Quick mail";
                    string UserName;
                    string UserId;

                    string User_Id = Convert.ToString(Session["username"]);
                    if (User_Id == "Admin")
                    {
                        UserName = "Admin";
                        UserId = Convert.ToString(Session["username"]);
                        
                    }
                    else
                    {
                        UserName = "Employee";
                        UserId = Convert.ToString(Session["username"]);

                    }

                    Mail.SendMultipleMail1(displayName, from_mail, to_mail, subject, body, PlaceName, UserName, UserId);  


                    useractivity.AddEmployeeActivityDetails("Send quick mail");
                    string Message = "Mail sent successfully";
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAttendanceStatusList()
        {
            try
            {
                tblAttendanceStatusServices attStatus = new tblAttendanceStatusServices();
                var attStatusList = attStatus.AttendanceStatusList();
                return Json(attStatusList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getDate(int acid)
        {
            try
            {
                
                AcademicyearServices AcYear = new AcademicyearServices();
                var ans = AcYear.date(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                EventServices holidays = new EventServices();
                var HolidaysList = holidays.getHolidaysList(acid);
                return Json(new { sd = sdate, ed = edate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult FutureActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    EventServices holidays = new EventServices();
                    int acid = CurrentAcYear[i].AcademicYearId;
                    var HolidaysList = holidays.getHolidaysList(acid);
                    return Json(new { StartingEnableDate, EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { StartingEnableDate, EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult PastActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    EventServices holidays = new EventServices();
                    int acid = CurrentAcYear[i].AcademicYearId;
                    var HolidaysList = holidays.getHolidaysList(acid);
                    return Json(new { StartingEnableDate, EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { StartingEnableDate, EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CurrentActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");

            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    int acid = CurrentAcYear[i].AcademicYearId;
                    return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CustomeActiveDate(int acid)
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var ans = AcYear.date(acid);
            //string sdate1 = ans.StartDate.ToString("dd/MM/yyyy");
            //string edate1 = ans.EndDate.ToString("dd/MM/yyyy");
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            EventServices holidays = new EventServices();
            var HolidaysList = holidays.getHolidaysList(acid);
            DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = ans.StartDate;
                DateTime edate = ans.EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");

                    return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
                else {
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
        }
 

    }
}
