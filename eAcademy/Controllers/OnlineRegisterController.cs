﻿using eAcademy.Areas.HostelManagement.Services;
using eAcademy.Areas.Transport.Services;
using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using Rotativa;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
namespace eAcademy.Controllers
{
    public class OnlineRegisterController : Controller
    {
        int onlineregid = 0;
        UserActivityHelper useractivity = new UserActivityHelper();
        ClassServices clas = new ClassServices();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            SchoolSettingServices schoolsetting = new SchoolSettingServices();
            AcademicyearServices AcYear = new AcademicyearServices();
            CountriesServices country = new CountriesServices();
            BloodGroupServices bg = new BloodGroupServices();
            CommunityServices com = new CommunityServices();
            BoardofSchoolServices bos = new BoardofSchoolServices();
            EmployeeDesinationServices EmployeeDesignation = new EmployeeDesinationServices();
            var school = schoolsetting.getSchoolSettings();
            Session["SSchoolName"] = school.SchoolName;
            Session["SSchoolLogo"] = school.SchoolLogo;
            Session["SchoolAddress1"] = school.AddressLine1;
            Session["SchoolAddress2"] = school.AddressLine2;
            Session["SchoolCity"] = school.City;
            Session["SchoolCountry"] = school.Country;
            Session["SchoolPincode"] = school.ZIP;
            Session["SchoolPhone"] = school.Phone1;
            Session["SchoolFax"] = school.Fax;
            ViewBag.academicyear = AcYear.getvacancyAcademicYearList();
            ViewBag.country = country.GetCountry_Admission();
            ViewBag.bloodgroups = bg.GetBloodGroup();
            ViewBag.communities = com.GetCommunity();
            ViewBag.Classes = clas.GetClassvacauncy();
            ViewBag.Distances = GetDistance();
            ViewBag.PreviousBoard = bos.Showpreviousmedium();
            //ViewBag.Pre_Board = GetPre_Board();
            //ViewBag.Previousclasses = GetPre_Classes();
            ViewBag.EmployeeDesignation = EmployeeDesignation.EmployeeDesignation();
        }
        public IEnumerable GetDistance()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select Distance", Value = "" });
            li.Add(new SelectListItem { Text = "1km", Value = "1" });
            li.Add(new SelectListItem { Text = "2km", Value = "2" });
            li.Add(new SelectListItem { Text = "3km", Value = "3" });
            li.Add(new SelectListItem { Text = "4km", Value = "4" });
            li.Add(new SelectListItem { Text = "5km", Value = "5" });
            li.Add(new SelectListItem { Text = "6km", Value = "6" });
            li.Add(new SelectListItem { Text = "7km", Value = "7" });
            li.Add(new SelectListItem { Text = "8km", Value = "8" });
            li.Add(new SelectListItem { Text = "9km", Value = "9" });
            li.Add(new SelectListItem { Text = "10km", Value = "10" });
            li.Add(new SelectListItem { Text = "11km", Value = "11" });
            li.Add(new SelectListItem { Text = "12km", Value = "12" });
            li.Add(new SelectListItem { Text = "13km", Value = "13" });
            li.Add(new SelectListItem { Text = "14km", Value = "14" });
            li.Add(new SelectListItem { Text = "15km", Value = "15" });
            li.Add(new SelectListItem { Text = "16km", Value = "16" });
            li.Add(new SelectListItem { Text = "17km", Value = "17" });
            li.Add(new SelectListItem { Text = "18km", Value = "18" });
            li.Add(new SelectListItem { Text = "19km", Value = "19" });
            li.Add(new SelectListItem { Text = "20km", Value = "20" });
            return li;
        }
        //public IEnumerable GetPre_Board()
        //{
        //    List<SelectListItem> board = new List<SelectListItem>();
        //    board.Add(new SelectListItem { Text = "Board Studying in", Value = "" });
        //    board.Add(new SelectListItem { Text = "CBSE", Value = "CBSE" });
        //    board.Add(new SelectListItem { Text = "ICSE", Value = "ICSE" });
        //    board.Add(new SelectListItem { Text = "CIE", Value = "CIE" });
        //    board.Add(new SelectListItem { Text = "IB", Value = "IB" });
        //    board.Add(new SelectListItem { Text = "StateBoard", Value = "StateBoard" });
        //    board.Add(new SelectListItem { Text = "Others", Value = "Others" });
        //    return board;
        //}
        public IEnumerable getpreviousclass(int classid)
        {
            AsignClassVacancyServices Acs = new AsignClassVacancyServices();
            var ans = Acs.Showpreclass(classid);
            return ans;
        }
        public JsonResult getpreclass(int classid)
        {
            try
            {
                AsignClassVacancyServices Acs = new AsignClassVacancyServices();
                var ans = Acs.Showpreclass(classid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Login()
        {
            try
            {
                SchoolSettingsServices ss = new SchoolSettingsServices();
                var ans = ss.getSchoolSettings();
                if (ans.Count > 0)
                {
                    ViewBag.Email = ans.FirstOrDefault().EmailRequired;
                    ViewBag.Sms = ans.FirstOrDefault().SmsRequired;
                    ViewBag.App = ans.FirstOrDefault().AppRequired;
                    if (Session["OnlineRegisterId"] == null)
                    {
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("HomePage", new { Id = Session["OnlineRegisterId"] });
                    }
                }
                else
                {
                    ViewBag.error = ResourceCache.Localize("School_Settings_not_completed");
                    return View("Error");
                }

            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Login(VM_Login qd)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var s444 = Session["Captcha"];
                    if (Session["Captcha"].ToString() == qd.Captcha) //  isCaptchaCodeValid
                    {
                        AcademicyearServices AcYear = new AcademicyearServices();
                        AsignClassVacancyServices acs = new AsignClassVacancyServices();
                        PreAdmissionOnlineRegisterServices PAORS = new PreAdmissionOnlineRegisterServices();
                        PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                        SchoolSettingServices sss = new SchoolSettingServices();
                        PreAdmissionTransactionServices pars = new PreAdmissionTransactionServices();
                        PreAdmissionPrimaryUserRegisterServices papurs = new PreAdmissionPrimaryUserRegisterServices();
                        PrimaryUserRegisterServices purs = new PrimaryUserRegisterServices();
                        SchoolSettingServices ss = new SchoolSettingServices();
                        Admission1Services As1 = new Admission1Services();
                        SchoolSettingsServices ss1 = new SchoolSettingsServices();
                        var ans = ss1.getSchoolSettings();
                        ViewBag.Email = ans.FirstOrDefault().EmailRequired;
                        ViewBag.Sms = ans.FirstOrDefault().SmsRequired;
                        ViewBag.App = ans.FirstOrDefault().AppRequired;
                        var formtype = ss.getSchoolSettings();
                        if (formtype != null)
                        {
                            var commingAcademicyear = AcYear.GetAcademicYear();
                            if (commingAcademicyear != null)
                            {
                                var vacuncyAvailable = acs.GetAnyOneYearvacancy();
                                if (vacuncyAvailable != null)
                                {
                                    string uname = "";
                                    if (qd.UsernameType == "Email")
                                    {
                                        uname = qd.Username;
                                        // string pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(qd.passwords, "SHA1");
                                        var NewUser = PAORS.CheckEmailUserAvaliable(uname, qd.passwords);
                                        var ExitUser = purs.CheckEmailUserAvaliable(uname, qd.passwords);
                                        //string covertpwd = FormsAuthentication.HashPasswordForStoringInConfigFile(qd.passwords, "SHA1");
                                        int s = 0;
                                        if (NewUser != null)
                                        {
                                            Session["RegForm_SessionAttribute"] = NewUser.Username;
                                            Session["OnlineRegisterId"] = QSCrypt.Encrypt(NewUser.OnlineRegisterId.ToString());
                                            var User = papurs.FindEmailPrimaryUser(uname);
                                            if (User == 0)
                                            {
                                                Session["Home"] = "0";
                                                if (formtype.FormFormat == "Single Form")
                                                {
                                                    return RedirectToAction("AdmissionForm1", new { Id = QSCrypt.Encrypt(NewUser.OnlineRegisterId.ToString()) }); //, Sid = QSCrypt.Encrypt(s.ToString())
                                                }
                                                else
                                                {
                                                    return RedirectToAction("AdmissionForm4", new { Id = QSCrypt.Encrypt(NewUser.OnlineRegisterId.ToString()) });//, Sid = QSCrypt.Encrypt(s.ToString())
                                                }
                                            }
                                            else
                                            {
                                                Session["Home"] = QSCrypt.Encrypt(NewUser.OnlineRegisterId.ToString());
                                                return RedirectToAction("HomePage", new { Id = QSCrypt.Encrypt(NewUser.OnlineRegisterId.ToString()) });
                                            }
                                        }
                                        else if (ExitUser != null)
                                        {
                                            Session["RegForm_SessionAttribute"] = ExitUser.PrimaryUserEmail;
                                            int? OnlineRegisterId = pars.AddEmailExistDetailsToNewtable(ExitUser.PrimaryUserEmail);
                                            Session["OnlineRegisterId"] = QSCrypt.Encrypt(OnlineRegisterId.ToString());
                                            if (formtype.FormFormat == "Single Form")
                                            {
                                                return RedirectToAction("AdmissionForm1", new { Id = QSCrypt.Encrypt(OnlineRegisterId.ToString()) });//, Sid = QSCrypt.Encrypt(s.ToString())
                                            }
                                            else
                                            {
                                                return RedirectToAction("AdmissionForm4", new { Id = QSCrypt.Encrypt(OnlineRegisterId.ToString()) });//, Sid = QSCrypt.Encrypt(s.ToString())
                                            }
                                        }
                                        else
                                        {
                                            ViewBag.ErrorMessage = ResourceCache.Localize("Enter_Correct_UserName_and_Password");
                                        }
                                    }
                                    else
                                    {
                                        uname = qd.Username;
                                        // string pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(qd.passwords, "SHA1");
                                        var NewUser = PAORS.CheckMobileUserAvaliable(uname, qd.passwords);
                                        var ExitUser = purs.CheckMobileUserAvaliable(uname, qd.passwords);
                                        //string covertpwd = FormsAuthentication.HashPasswordForStoringInConfigFile(qd.passwords, "SHA1");
                                        int s = 0;
                                        if (NewUser != null)
                                        {
                                            Session["RegForm_SessionAttribute"] = NewUser.Username;
                                            Session["OnlineRegisterId"] = QSCrypt.Encrypt(NewUser.OnlineRegisterId.ToString());
                                            var User = papurs.FindMobilePrimaryUser(uname);
                                            if (User == 0)
                                            {
                                                Session["Home"] = "0";
                                                if (formtype.FormFormat == "Single Form")
                                                {
                                                    return RedirectToAction("AdmissionForm1", new { Id = QSCrypt.Encrypt(NewUser.OnlineRegisterId.ToString()) }); //, Sid = QSCrypt.Encrypt(s.ToString())
                                                }
                                                else
                                                {
                                                    return RedirectToAction("AdmissionForm4", new { Id = QSCrypt.Encrypt(NewUser.OnlineRegisterId.ToString()) });//, Sid = QSCrypt.Encrypt(s.ToString())
                                                }
                                            }
                                            else
                                            {
                                                Session["Home"] = QSCrypt.Encrypt(NewUser.OnlineRegisterId.ToString());
                                                return RedirectToAction("HomePage", new { Id = QSCrypt.Encrypt(NewUser.OnlineRegisterId.ToString()) });
                                            }
                                        }
                                        else if (ExitUser != null)
                                        {
                                            Session["RegForm_SessionAttribute"] = ExitUser.PrimaryUserContactNo;
                                            int? OnlineRegisterId = pars.AddMobileExistDetailsToNewtable(ExitUser.PrimaryUserContactNo);
                                            Session["OnlineRegisterId"] = QSCrypt.Encrypt(OnlineRegisterId.ToString());
                                            if (formtype.FormFormat == "Single Form")
                                            {
                                                return RedirectToAction("AdmissionForm1", new { Id = QSCrypt.Encrypt(OnlineRegisterId.ToString()) });//, Sid = QSCrypt.Encrypt(s.ToString())
                                            }
                                            else
                                            {
                                                return RedirectToAction("AdmissionForm4", new { Id = QSCrypt.Encrypt(OnlineRegisterId.ToString()) });//, Sid = QSCrypt.Encrypt(s.ToString())
                                            }
                                        }
                                        else
                                        {
                                            ViewBag.ErrorMessage = ResourceCache.Localize("Enter_Correct_UserName_and_Password");
                                        }
                                    }
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = ResourceCache.Localize("School_Settings_not_completed");
                                }
                            }
                            else
                            {
                                ViewBag.ErrorMessage = ResourceCache.Localize("School_Settings_not_completed");
                            }
                        }
                        else
                        {
                            ViewBag.ErrorMessage = ResourceCache.Localize("School_Settings_not_completed");
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = ResourceCache.Localize("Captcha_is_mismatch");
                    }
                    ModelState.Clear();
                    return View();
                }
                else
                {
                    ViewBag.modelState = ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        public ActionResult Register()
        {
            try
            {
                SchoolSettingsServices ss = new SchoolSettingsServices();
                var ans = ss.getSchoolSettings();
                if (ans.Count > 0)
                {
                    ViewBag.Email = ans.FirstOrDefault().EmailRequired;
                    ViewBag.Sms = ans.FirstOrDefault().SmsRequired;
                    ViewBag.App = ans.FirstOrDefault().AppRequired;
                    return View();
                }
                else
                {
                    ViewBag.error = ResourceCache.Localize("School_Settings_not_completed");
                    return View("Error");
                }

            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult Register(VM_Register qd, string command)
        {
            try
            {
                SchoolSettingsServices ss = new SchoolSettingsServices();
                var ans = ss.getSchoolSettings();
                ViewBag.Email = ans.FirstOrDefault().EmailRequired;
                ViewBag.Sms = ans.FirstOrDefault().SmsRequired;
                ViewBag.App = ans.FirstOrDefault().AppRequired;
                if (command == "Register")
                {
                    if (ModelState.IsValid)
                    {
                        var s444 = Session["Captcha"];
                        if (Session["Captcha"].ToString() == qd.Captcha) //  isCaptchaCodeValid
                        {
                            PreAdmissionOnlineRegisterServices PAORS = new PreAdmissionOnlineRegisterServices();
                            var uname = qd.Username;
                            string pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(qd.passwords, "SHA1");
                            var rpwd = FormsAuthentication.HashPasswordForStoringInConfigFile(qd.repasswords, "SHA1");
                            if (pwd == rpwd)
                            {
                                if (qd.UsernameType == "Email")
                                {
                                    var res = PAORS.Emailregister(uname, pwd, rpwd);
                                    if (res == true)
                                    {
                                        var SchoolName = ans.FirstOrDefault().SchoolName;
                                        var SchoolLogo = ans.FirstOrDefault().SchoolLogo;
                                        StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/UserRegisterEmail.html"));
                                        string readFile2 = reader2.ReadToEnd();
                                        string myString2 = "";
                                        myString2 = readFile2;
                                        myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
                                        myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
                                        string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                                        string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                                        string PlaceName = "OnlineRegister-Register-User registered successfully";
                                        string UserName="Parent";
                                        string UserId = Convert.ToString(Session["OnlineUserName"]);

                                        Mail.SendMail(EmailDisplay, supportEmail, uname, "Account Credentials", myString2.ToString(), true, PlaceName, UserName, UserId); 
                                        ViewBag.RegisterMessage = ResourceCache.Localize("User_Registered_Successfully");
                                        return View("Login");
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("User_email_already_exist,so_use_another_email");
                                    }
                                }
                                else if (qd.UsernameType == "Mobile")
                                {
                                    var res = PAORS.Mobileregister(uname, pwd, rpwd);
                                    if (res == true)
                                    {
                                        ViewBag.RegisterMessage = ResourceCache.Localize("User_Registered_Successfully");
                                        PreAdmissionRegisterSMS(uname);
                                        return View("Login");
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("User_mobile_number_already_exist,so_use_another_mobile_number");
                                    }
                                }
                            }
                            else
                            {
                                ViewBag.ErrorMessage = ResourceCache.Localize("Password_Mismatch");
                            }
                        }
                        else
                        {
                            ViewBag.ErrorMessage = ResourceCache.Localize("Captcha_is_mismatch");
                        }
                        ModelState.Clear();
                        return View();
                    }
                    else
                    {
                        ViewBag.modelState = ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                        return View();
                    }
                }
                else if (command == "GetUser")
                {
                    var s444 = Session["Captcha"];
                    if (Session["Captcha"].ToString() == qd.Captcha) //  isCaptchaCodeValid
                    {
                        OnLineRegisterServices ors = new OnLineRegisterServices();
                        PrimaryUserRegisterServices pur = new PrimaryUserRegisterServices();
                        string PrimaryuserEmail = "";
                        if (qd.StudentId != null)
                        {
                            var findstudent = pur.FindStudent(qd.StudentId);
                            if (findstudent != null)
                            {
                                var user = pur.GetStudentPrimarUserDetails(qd.StudentId);
                                if (qd.UsernameType == "Email")
                                {
                                    if (user.PrimaryUserEmail != null)
                                    {
                                        PrimaryuserEmail = user.PrimaryUserEmail;
                                    }
                                }
                                else
                                {
                                    if (user.PrimaryUserContactNo != null)
                                    {
                                        PrimaryuserEmail = user.PrimaryUserContactNo.ToString();
                                    }
                                }

                            }
                            else
                            {
                                var findParent = pur.GetParentPrimarUserEmail(qd.StudentId);
                                if (findParent != null)
                                {
                                    if (qd.UsernameType == "Email")
                                    {
                                        if (findParent.PrimaryUserEmail != null)
                                        {
                                            PrimaryuserEmail = findParent.PrimaryUserEmail;
                                        }
                                    }
                                    else
                                    {
                                        if (findParent.PrimaryUserContactNo != null)
                                        {
                                            PrimaryuserEmail = findParent.PrimaryUserContactNo.ToString();
                                        }
                                    }
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = ResourceCache.Localize("Please_enter_correct_id");
                                    return View();
                                }
                            }
                        }
                        if (PrimaryuserEmail == "")
                        {
                            ViewBag.ErrorMessage = ResourceCache.Localize("Please_enter_correct_id");
                        }
                        else
                        {
                            ViewBag.Message = ResourceCache.Localize("Use") + " " + PrimaryuserEmail + ResourceCache.Localize(",and_school_login_password_to_enroled_another_student");
                        }
                        return View();
                    }
                    else
                    {
                        ViewBag.ErrorMessage = ResourceCache.Localize("Captcha_is_mismatch");
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        public void PreAdmissionRegisterSMS(string number)
        {
            string ToNumber = number;
            string sub = "User Registration";
            string Description = "Hi, you have successfully registered with us";
            string fromDisplayName = ConfigurationManager.AppSettings["EmailFromDisplayName"];
            SMS.Main(fromDisplayName, ToNumber, sub, Description);
        }
        public ActionResult ForgetPassword()
        {
            try
            {
                SchoolSettingsServices ss = new SchoolSettingsServices();
                var ans = ss.getSchoolSettings();
                if (ans.Count > 0)
                {
                    ViewBag.Email = ans.FirstOrDefault().EmailRequired;
                    ViewBag.Sms = ans.FirstOrDefault().SmsRequired;
                    ViewBag.App = ans.FirstOrDefault().AppRequired;
                    return View();
                }
                else
                {
                    ViewBag.error = ResourceCache.Localize("School_Settings_not_completed");
                    return View("Error");
                }
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult ForgetPassword(VM_ForgetPassword f)
        {
            try
            {
                SchoolSettingsServices ss1 = new SchoolSettingsServices();
                var ans1 = ss1.getSchoolSettings();
                ViewBag.Email = ans1.FirstOrDefault().EmailRequired;
                ViewBag.Sms = ans1.FirstOrDefault().SmsRequired;
                ViewBag.App = ans1.FirstOrDefault().AppRequired;
                if (ModelState.IsValid)
                {
                    var s444 = Session["Captcha"];
                    if (Session["Captcha"].ToString() == f.Captcha) //  isCaptchaCodeValid
                    {
                        PrimaryUserResetPasswordServices PRS = new PrimaryUserResetPasswordServices();
                        PreAdmissionOnlineRegisterServices Pors = new PreAdmissionOnlineRegisterServices();
                        PreAdmissionPrimaryUserRegisterServices ppurs = new PreAdmissionPrimaryUserRegisterServices();
                        PrimaryUserRegisterServices purs = new PrimaryUserRegisterServices();
                        SchoolSettingServices ss = new SchoolSettingServices();
                        //SchoolSettingsServices ss1 = new SchoolSettingsServices();
                        string MatchEmailPattern =
         @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
  + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
  + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
  + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
                        string uname = "";


                        if (f.Username != null)
                        {
                            if (Regex.IsMatch(f.Username, MatchEmailPattern))
                            {
                                uname = f.Username;
                            }
                            else
                            {
                                uname = f.Username;
                            }
                        }
                        if (uname != "")
                        {
                            if (f.UsernameType == "Email")
                            {
                                var uid = Guid.NewGuid();
                                var ans = Pors.CheckOnlineUserName(uname);
                                var offlineuser = Pors.CheckExitUserName(uname);
                                var Mastertableuser = Pors.CheckMasterUserName(uname);
                                if (ans != null)
                                {
                                    int RegId = Convert.ToInt16(ans.PrimaryUserAdmissionId);
                                    DateTime date = DateTimeByZone.getCurrentDate();
                                    var Name = ans.PrimaryUserName;
                                    PRS.AddPrimaryUserResetPwd(uid, RegId, date, uname);
                                    var CheckRowExist = Pors.CheckPrimaryUserId(RegId);
                                    if (CheckRowExist != null)
                                    {
                                        var school = ss.getSchoolSettings();
                                        string schoolname = school.SchoolName;
                                        string logo = school.SchoolLogo;
                                        string Url = ConfigurationManager.AppSettings["Url"];
                                        var link = Url + "OnlineRegister/UserResetPassword?uid=" + uid;
                                        StreamReader reader = new StreamReader(Server.MapPath("~/Template/SuperUserResetPassword.html"));
                                        string readFile = reader.ReadToEnd();
                                        string myString = "";
                                        myString = readFile;
                                        myString = myString.Replace("$$name$$", Name.ToString());
                                        myString = myString.Replace("$$link$$", link.ToString());
                                        myString = myString.Replace("$$SchoolName$$", schoolname.ToString());
                                        myString = myString.Replace("$$SchoolLogo$$", logo.ToString());
                                        var HaveEamil = ans.PrimaryUserEmail;
                                        if (HaveEamil != null)
                                        {
                                            var ToMail = HaveEamil;
                                            string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                                            string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];

                                            string PlaceName = "OnlineRegister-ForgetPassword-Parent reset password link sent";
                                            string UserName = "Parent";
                                            string UserId = Convert.ToString(Session["OnlineUserName"]);


                                            Mail.SendMail(EmailDisplay, supportEmail, ToMail, "Reset Password", myString.ToString(), true, PlaceName, UserName, UserId);  
                                            ViewBag.Message = ResourceCache.Localize("Link_has_been_sent_to_your_mail_address._Please__using_that_Link_to_change_Password");
                                        }
                                        else
                                        {
                                            PRS.DeleteRequestPrimary(uid);
                                            ViewBag.ErrorMessage = ResourceCache.Localize("Primary_User_Email_is_not_found_,So_create_new_user_to_Login");
                                        }
                                    }
                                    return View();
                                }
                                else if (offlineuser != null)
                                {
                                    int RegId = Convert.ToInt16(offlineuser.PrimaryUserAdmissionId);
                                    DateTime date = DateTimeByZone.getCurrentDate();
                                    var Name = offlineuser.PrimaryUserName;
                                    PRS.AddPrimaryUserResetPwd(uid, RegId, date, uname);
                                    var CheckRowExist = Pors.CheckPrimaryUserId(RegId);
                                    if (CheckRowExist != null)
                                    {
                                        var school = ss.getSchoolSettings();
                                        string schoolname = school.SchoolName;
                                        string logo = school.SchoolLogo;
                                        string Url = ConfigurationManager.AppSettings["Url"];
                                        var link = Url + "OnlineRegister/UserResetPassword?uid=" + uid;
                                        StreamReader reader = new StreamReader(Server.MapPath("~/Template/SuperUserResetPassword.html"));
                                        string readFile = reader.ReadToEnd();
                                        string myString = "";
                                        myString = readFile;
                                        myString = myString.Replace("$$name$$", Name.ToString());
                                        myString = myString.Replace("$$link$$", link.ToString());
                                        myString = myString.Replace("$$SchoolName$$", schoolname.ToString());
                                        myString = myString.Replace("$$SchoolLogo$$", logo.ToString());
                                        var HaveEamil = offlineuser.PrimaryUserEmail;
                                        if (HaveEamil != null)
                                        {
                                            var ToMail = HaveEamil;
                                            string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                                            string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                                            string PlaceName = "OnlineRegisterController-Parent Reset Password Mail Sent";
                                            string UserName = "Parent";
                                            string UserId = Convert.ToString(Session["OnlineUserName"]);
                                            Mail.SendMail(EmailDisplay, supportEmail, ToMail, "Reset Password", myString.ToString(), true,PlaceName, UserName, UserId);  
                                            ViewBag.Message = ResourceCache.Localize("Link_has_been_sent_to_your_mail_address._Please__using_that_Link_to_change_Password");
                                        }
                                        else
                                        {
                                            PRS.DeleteRequestPrimary(uid);
                                            ViewBag.ErrorMessage = ResourceCache.Localize("Primary_User_Email_is_not_found_,So_create_new_user_to_Login");
                                        }
                                    }
                                    return View();
                                }
                                else if (Mastertableuser != null)
                                {
                                    int RegId = Convert.ToInt16(Mastertableuser.PrimaryUserAdmissionId);
                                    DateTime date = DateTimeByZone.getCurrentDate();
                                    var Name = Mastertableuser.PrimaryUserName;
                                    PRS.AddPrimaryUserResetPwd(uid, RegId, date, uname);
                                    var CheckRowExist = Pors.CheckPrimaryUserId(RegId);
                                    if (CheckRowExist != null)
                                    {
                                        var school = ss.getSchoolSettings();
                                        string schoolname = school.SchoolName;
                                        string logo = school.SchoolLogo;
                                        string Url = ConfigurationManager.AppSettings["Url"];
                                        var link = Url + "OnlineRegister/UserResetPassword?uid=" + uid;
                                        StreamReader reader = new StreamReader(Server.MapPath("~/Template/SuperUserResetPassword.html"));
                                        string readFile = reader.ReadToEnd();
                                        string myString = "";
                                        myString = readFile;
                                        myString = myString.Replace("$$name$$", Name.ToString());
                                        myString = myString.Replace("$$link$$", link.ToString());
                                        myString = myString.Replace("$$SchoolName$$", schoolname.ToString());
                                        myString = myString.Replace("$$SchoolLogo$$", logo.ToString());
                                        var HaveEamil = Mastertableuser.PrimaryUserEmail;
                                        if (HaveEamil != null)
                                        {
                                            var ToMail = HaveEamil;
                                            string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                                            string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                                            string PlaceName = "OnlineRegisterController-Parent Reset Password Mail Sent";
                                            string UserName = "Parent";
                                            string UserId = Convert.ToString(Session["OnlineUserName"]);
                                            Mail.SendMail(EmailDisplay, supportEmail, ToMail, "Reset Password", myString.ToString(), true, PlaceName, UserName, UserId);  
                                            ViewBag.Message = ResourceCache.Localize("Link_has_been_sent_to_your_mail_address._Please__using_that_Link_to_change_Password");
                                        }
                                        else
                                        {
                                            PRS.DeleteRequestPrimary(uid);
                                            ViewBag.ErrorMessage = ResourceCache.Localize("Primary_User_Email_is_not_found_,So_create_new_user_to_Login");
                                        }
                                    }
                                    return View();
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = ResourceCache.Localize("Enter_Correct_UserName");
                                    return View();
                                }
                            }
                            else if (f.UsernameType == "Mobile")
                            {
                                long contact = Convert.ToInt64(uname.Trim());
                                var uid = Guid.NewGuid();
                                var ans = Pors.CheckOnlineUserName(uname);
                                var offlineuser = ppurs.GetMobilePrimaryuserdetails(contact);
                                var Mastertableuser = purs.GetMobilePrimaryuserdetails(contact);
                                Random S_random = new Random();
                                string S_randomNumber = S_random.Next(00000, 99999).ToString();
                                if (ans != null)
                                {
                                    if (ans.PrimaryUserName != null)
                                    {
                                        int RegId = Convert.ToInt16(ans.PrimaryUserAdmissionId);
                                        DateTime date = DateTimeByZone.getCurrentDate();
                                        var Name = ans.PrimaryUserName;
                                        long? otp = Convert.ToInt64(S_randomNumber.Trim());
                                        PRS.AddPrimaryUserMobileResetPwd(uid, RegId, date, otp, uname);
                                        string tonumber = ans.PrimaryUserName;
                                        ForgetOtp(tonumber, S_randomNumber);
                                        return RedirectToAction("UserMobileResetPassword", new { Id = uid });
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Primary_User_Email_is_not_found_,So_create_new_user_to_Login");
                                    }
                                    return View();
                                }
                                else if (offlineuser != null)
                                {
                                    if (offlineuser.PrimaryUserContactNo != null)
                                    {
                                        int RegId = Convert.ToInt16(offlineuser.PrimaryUserAdmissionId);
                                        DateTime date = DateTimeByZone.getCurrentDate();
                                        var Name = offlineuser.PrimaryUserName;
                                        long? otp = Convert.ToInt64(S_randomNumber.Trim());
                                        PRS.AddPrimaryUserMobileResetPwd(uid, RegId, date, otp, uname);
                                        string tonumber = offlineuser.PrimaryUserContactNo.ToString();
                                        ForgetOtp(tonumber, S_randomNumber);
                                        return RedirectToAction("UserMobileResetPassword", new { Id = uid });
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Primary_User_Email_is_not_found_,So_create_new_user_to_Login");
                                    }
                                    return View();
                                }
                                else if (Mastertableuser != null)
                                {
                                    if (Mastertableuser.PrimaryUserContactNo != null)
                                    {
                                        int RegId = Convert.ToInt16(Mastertableuser.PrimaryUserRegisterId);
                                        DateTime date = DateTimeByZone.getCurrentDate();
                                        var Name = Mastertableuser.PrimaryUserName;
                                        long? otp = Convert.ToInt64(S_randomNumber.Trim());
                                        PRS.AddPrimaryUserMobileResetPwd(uid, RegId, date, otp, uname);
                                        string tonumber = Mastertableuser.PrimaryUserContactNo.ToString();
                                        ForgetOtp(tonumber, S_randomNumber);
                                        return RedirectToAction("UserMobileResetPassword", new { Id = uid });
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Primary_User_Email_is_not_found_,So_create_new_user_to_Login");
                                    }
                                    return View();
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = ResourceCache.Localize("Enter_Correct_UserName");
                                    return View();
                                }
                            }
                            else
                            {
                                ViewBag.ErrorMessage = ResourceCache.Localize("Enter_Correct_UserName");
                                ViewBag.ErrorMessage = ResourceCache.Localize("Captcha_is_mismatch");
                                return View();
                            }
                        }
                        else
                        {
                            ViewBag.ErrorMessage = ResourceCache.Localize("Enter_Correct_UserName");
                            return View();
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = ResourceCache.Localize("Enter_Correct_UserName");
                        ViewBag.ErrorMessage = ResourceCache.Localize("Captcha_is_mismatch");
                        return View();
                    }
                }
                else
                {
                    ViewBag.modelState = ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }

        public void ForgetOtp(string Tonumber, string Otp)
        {
            string ToNumber = Tonumber;
            string sub = "Application OTP";
            string Description = "Hi, don't share this OTP=" + Otp;
            string fromDisplayName = ConfigurationManager.AppSettings["EmailFromDisplayName"];
            SMS.Main(fromDisplayName, ToNumber, sub, Description);
        }
        public ActionResult UserMobileResetPassword(string id)
        {
            try
            {
                PrimaryUserResetPasswordServices PRS = new PrimaryUserResetPasswordServices();
                var uid = id;
                ViewBag.uid = uid;
                var gd = new Guid(uid);
                var IsUidVaild = PRS.CheckPrimaryId(gd);
                if (IsUidVaild == null)
                {
                    ViewBag.ErrorMessage = ResourceCache.Localize("Link_is_Expired_or_Invalid_,So_you_one_more_request_to_ForgetPassword");
                    return View("~/Views/OnlineRegister/Error_RegResetPassword.cshtml");
                }
                else
                {
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult UserMobileResetPassword(VM_ResetPassword qd)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionPrimaryUserRegisterServices ors = new PreAdmissionPrimaryUserRegisterServices();
                    PrimaryUserResetPasswordServices PRS = new PrimaryUserResetPasswordServices();
                    var s444 = Session["Captcha"];
                    if (Session["Captcha"].ToString() == qd.Captcha) //  isCaptchaCodeValid
                    {
                        var id = new Guid(qd.uid);
                        var IsUidVaild = PRS.CheckOtpPrimaryId(id, qd.otp);
                        if (IsUidVaild == null)
                        {
                            ViewBag.ErrorMessage = ResourceCache.Localize("Invalid_Otp");
                            return View();
                        }
                        else
                        {
                            var uname = Convert.ToInt16(IsUidVaild.UserId);
                            var pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(qd.passwords, "SHA1");
                            var rpwd = FormsAuthentication.HashPasswordForStoringInConfigFile(qd.repasswords, "SHA1");
                            if (pwd == rpwd)
                            {
                                string type = "Mobile";
                                var res = ors.Updateregister(uname, pwd, type, IsUidVaild.Username);
                                if (res == true)
                                {
                                    PRS.DeleteRequestPrimary(id);
                                    ViewBag.Message = ResourceCache.Localize("Reset_Password_Updated");
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = ResourceCache.Localize("User_name_not_available");
                                }
                            }
                            else
                            {
                                ViewBag.ErrorMessage = ResourceCache.Localize("Password_Mismatch");
                            }
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = ResourceCache.Localize("Captcha_is_mismatch");
                    }
                    return View();
                }
                else
                {
                    ViewBag.modelState = ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }

        public ActionResult UserResetPassword()
        {
            try
            {
                PrimaryUserResetPasswordServices PRS = new PrimaryUserResetPasswordServices();
                var uid = Request.QueryString["uid"];
                ViewBag.uid = uid;
                var id = new Guid(uid);
                var IsUidVaild = PRS.CheckPrimaryId(id);
                if (IsUidVaild == null)
                {
                    ViewBag.ErrorMessage = ResourceCache.Localize("Link_is_Expired_or_Invalid_,So_you_one_more_request_to_ForgetPassword");
                    return View("~/Views/OnlineRegister/Error_RegResetPassword.cshtml");
                }
                else
                {
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult UserResetPassword(VM_ResetPassword qd)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionPrimaryUserRegisterServices ors = new PreAdmissionPrimaryUserRegisterServices();
                    PrimaryUserResetPasswordServices PRS = new PrimaryUserResetPasswordServices();
                    var s444 = Session["Captcha"];
                    if (Session["Captcha"].ToString() == qd.Captcha) //  isCaptchaCodeValid
                    {
                        var id = new Guid(qd.uid);
                        var IsUidVaild = PRS.CheckPrimaryId(id);
                        if (IsUidVaild == null)
                        {
                            ViewBag.ErrorMessage = ResourceCache.Localize("Link_is_Expired_or_Invalid_,So_you_one_more_request_to_ForgetPassword");
                            return View();
                        }
                        else
                        {
                            var uname = Convert.ToInt16(IsUidVaild.UserId);
                            var pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(qd.passwords, "SHA1");
                            var rpwd = FormsAuthentication.HashPasswordForStoringInConfigFile(qd.repasswords, "SHA1");
                            if (pwd == rpwd)
                            {
                                string type = "Email";
                                var res = ors.Updateregister(uname, pwd, type, IsUidVaild.Username);
                                if (res == true)
                                {
                                    PRS.DeleteRequestPrimary(id);
                                    ViewBag.Message = ResourceCache.Localize("Reset_Password_Updated");
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = ResourceCache.Localize("User_name_not_available");
                                }
                            }
                            else
                            {
                                ViewBag.ErrorMessage = ResourceCache.Localize("Password_Mismatch");
                            }
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = ResourceCache.Localize("Captcha_is_mismatch");
                    }
                    return View();
                }
                else
                {
                    ViewBag.modelState = ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        public JsonResult GetAdmissionClass(int year)
        {
            try
            {
                AsignClassVacancyServices avs = new AsignClassVacancyServices();
                var ans = avs.ShowVacancyClassList(year);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetFeeSubcategory(int AcademicyearId, int feecategoryid)
        {
            try
            {
                HostelFeesCategoryServices avs = new HostelFeesCategoryServices();
                var ans = avs.GetFeeSubcategory(AcademicyearId, feecategoryid);
                var FeeAmount = avs.getFeeAmount(AcademicyearId, feecategoryid);
                return Json(new { ans, FeeAmount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetParticularFeeAmount(int AcademicyearId, int FeeCategoryId, int subcategoryId)
        {
            try
            {
                HostelFeesCategoryServices avs = new HostelFeesCategoryServices();
                var ans = avs.getFeeAmount(AcademicyearId, FeeCategoryId, subcategoryId);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetHostelFeeStructure(int AcademicyearId)
        {
            try
            {
                HostelFeesCategoryServices avs = new HostelFeesCategoryServices();
                var ans = avs.GetFeeStructure(AcademicyearId);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        [RegForm_CheckSessionOut]
        public ActionResult HomePage(string id)
        {
            try
            {
                PreAdmissionOnlineRegisterServices PAORS = new PreAdmissionOnlineRegisterServices();
                PreAdmissionStudentRegisterServices pasrs = new PreAdmissionStudentRegisterServices();
                TempData["detailsallsaved"] = "No";
                var oid = QSCrypt.Decrypt(id);
                int onlineRegisterId = Convert.ToInt16(oid);
                var result = pasrs.GetPrevivousApplication(onlineRegisterId);
                int sss = 0;
                SchoolSettingServices ss = new SchoolSettingServices();
                var user = PAORS.findUser(onlineRegisterId);
                Session["OnlineUserName"] = user.Username;
                var formtype = ss.getSchoolSettings();
                string formformat = "";
                if (formtype != null)
                {
                    if (formtype.FormFormat == "Single Form")
                    {
                        formformat = "AdmissionForm1";
                    }
                    else
                    {
                        formformat = "AdmissionForm4";
                    }
                    Session["Home"] = id;
                    ViewBag.Sid = QSCrypt.Encrypt(sss.ToString());
                    ViewBag.formformat = formformat;
                    ViewBag.Onlineid = id;
                    if (result.Count != 0)
                    {
                        return View(result);
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = ResourceCache.Localize("Settings_not_completed");
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
//        [RegForm_CheckSessionOut]
//        public ActionResult AdmissionForm1(string Id)
//        {
//            try
//            {
//                if (Convert.ToString(TempData["detailsallsaved"]) == "Yes")
//                {
//                    return RedirectToAction("HomePage", "OnlineRegister", new { id = Id });
//                }
//                else
//                {
//                    TempData["onlineregid"] = Convert.ToInt16(QSCrypt.Decrypt(Id));
//                    Admission1Services As1 = new Admission1Services();
//                    PreAdmissionOnlineRegisterServices PAORS = new PreAdmissionOnlineRegisterServices();
//                    PreAdmissionStudentRegisterServices PASRS = new PreAdmissionStudentRegisterServices();
//                    EmployeeDesinationServices EmployeeDesignation = new EmployeeDesinationServices();
//                    PreAdmissionTransactionServices Admintrans = new PreAdmissionTransactionServices();
//                    PrimaryUserRegisterServices pu = new PrimaryUserRegisterServices();
//                    SchoolSettingsServices ss = new SchoolSettingsServices();
//                    TransportDestinationServices TrDestnServ = new TransportDestinationServices();
//                    int OnlineRegId = Convert.ToInt16(QSCrypt.Decrypt(Id));
//                    int StudentRegId = PAORS.GetStudentId(OnlineRegId);
//                    ViewBag.Regid = OnlineRegId;
//                    var user = PAORS.findUser(OnlineRegId);
//                    Session["OnlineUserName"] = user.Username;
//                    string MatchEmailPattern =
//            @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
//     + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
//				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
//     + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
//				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
//     + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
//                    if (user.Username != null)
//                    {
//                        if (Regex.IsMatch(user.Username, MatchEmailPattern))
//                        {
//                            ViewBag.PrimaryUserEmail = user.Username;
//                        }
//                        else
//                        {
//                            ViewBag.PrimaryuserContact = user.Username.Trim();
//                        }
//                    }
//                    ViewBag.allDestination = TrDestnServ.DestinationList();
//                    ViewBag.EmployeeDesignation = EmployeeDesignation.EmployeeDesignation();
//                    var ans = PASRS.findParticularStudentDetails(OnlineRegId, StudentRegId);
//                    var result = PASRS.GetPrevivousApplication(OnlineRegId);
//                    var exist = pu.FindPrimaryUser(PAORS.getRow(OnlineRegId).Username);
//                    var Settings = ss.getSchoolSettings();
//                    ViewBag.Email = Settings.FirstOrDefault().EmailRequired;
//                    ViewBag.Sms = Settings.FirstOrDefault().SmsRequired;
//                    ViewBag.App = Settings.FirstOrDefault().AppRequired;
//                    ViewBag.Transport = Settings.FirstOrDefault().TransportRequired;
//                    ViewBag.Hostel = Settings.FirstOrDefault().HostelRequired;
//                    if (result.Count > 0)
//                    {
//                        if (result.FirstOrDefault().PreApplicationStatus != "InComplete" && result.FirstOrDefault().PreApplicationStatus != "")
//                        {
//                            ViewBag.oldstatus = "yes";
//                        }
//                        else
//                        {
//                            ViewBag.oldstatus = "no";
//                        }
//                    }
//                    else
//                    {
//                        ViewBag.oldstatus = "no";
//                        if (exist > 0)
//                        {
//                            ViewBag.oldstatus = "yes";
//                        }
//                    }
//                    if (ans.Count != 0)
//                    {
//                        ViewBag.Distance = ans.FirstOrDefault().Distance;
//                        var PreviousClass = ans.FirstOrDefault().AdmissionClass;
//                        ViewBag.Previousclass = null;
//                        if (PreviousClass != null)
//                        {
//                            int classid = Convert.ToInt32(PreviousClass);
//                            ViewBag.Previousclass = getpreviousclass(classid);
//                        }
//                        return View(ans);
//                    }
//                    else
//                    {
//                        return View();
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                ViewBag.admission = "PreAdmission";
//                ViewBag.error = e.Message.ToString();
//                return View("Error");
//            }
//        }

      
        [RegForm_CheckSessionOut]
        public ActionResult AdmissionForm1(string Id, string StudentId)

        {
            try
            {
                if (Convert.ToString(TempData["detailsallsaved"]) == "Yes")
                {
                    return RedirectToAction("HomePage", "OnlineRegister", new { id = Id });
                }
                else
                {
                    TempData["onlineregid"] = Convert.ToInt16(QSCrypt.Decrypt(Id));
                Admission1Services As1 = new Admission1Services();
                PreAdmissionOnlineRegisterServices PAORS = new PreAdmissionOnlineRegisterServices();
                PreAdmissionStudentRegisterServices PASRS = new PreAdmissionStudentRegisterServices();
                EmployeeDesinationServices EmployeeDesignation = new EmployeeDesinationServices();
                PreAdmissionTransactionServices Admintrans = new PreAdmissionTransactionServices();
                PrimaryUserRegisterServices pu = new PrimaryUserRegisterServices();
                SchoolSettingsServices ss = new SchoolSettingsServices();
                TransportDestinationServices TrDestnServ = new TransportDestinationServices();
                int OnlineRegId = Convert.ToInt16(QSCrypt.Decrypt(Id));
               
                int StudentRegId = 0;
                if (StudentId == null)
                {
                  StudentRegId = PAORS.GetStudentId(OnlineRegId);
                }
                else
                {
                    int studentadmissionid = Convert.ToInt32(QSCrypt.Decrypt(StudentId));
                   StudentRegId = PAORS.Get_Addmoredetails_by_StudentId(OnlineRegId, studentadmissionid);
                }
                ViewBag.Regid = OnlineRegId;
                var user = PAORS.findUser(OnlineRegId);
                Session["OnlineUserName"] = user.Username;
                string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
 + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
 + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
 + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
                if (user.Username != null)
                {
                    if (Regex.IsMatch(user.Username, MatchEmailPattern))
                    {
                        ViewBag.PrimaryUserEmail = user.Username;
                    }
                    else
                    {
                        ViewBag.PrimaryuserContact = user.Username.Trim();
                    }
                }
                ViewBag.allDestination = TrDestnServ.DestinationList();
                ViewBag.EmployeeDesignation = EmployeeDesignation.EmployeeDesignation();
                var ans = PASRS.findParticularStudentDetails(OnlineRegId, StudentRegId);
                var result = PASRS.GetPrevivousApplication(OnlineRegId);
                var exist = pu.FindPrimaryUser(PAORS.getRow(OnlineRegId).Username);
                var Settings = ss.getSchoolSettings();
                ViewBag.Email = Settings.FirstOrDefault().EmailRequired;
                ViewBag.Sms = Settings.FirstOrDefault().SmsRequired;
                ViewBag.App = Settings.FirstOrDefault().AppRequired;
                ViewBag.Transport = Settings.FirstOrDefault().TransportRequired;
                ViewBag.Hostel = Settings.FirstOrDefault().HostelRequired;
                if (result.Count > 0)
                {
                    if (result.FirstOrDefault().PreApplicationStatus != "InComplete" && result.FirstOrDefault().PreApplicationStatus != "")
                    {
                        ViewBag.oldstatus = "yes";
                    }
                    else
                    {
                        ViewBag.oldstatus = "no";
                    }
                }
                else
                {
                    ViewBag.oldstatus = "no";
                    if (exist > 0)
                    {
                        ViewBag.oldstatus = "yes";
                    }
                }
                if (ans.Count != 0)
                {
                    ViewBag.Distance = ans.FirstOrDefault().Distance;
                    var PreviousClass = ans.FirstOrDefault().AdmissionClass;
                    ViewBag.Previousclass = null;
                    if (PreviousClass != null)
                    {
                        int classid = Convert.ToInt32(PreviousClass);
                        ViewBag.Previousclass = getpreviousclass(classid);
                    }
                    return View(ans);
                }
                else
                {
                    return View();
                }
            }
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }

        [HttpPost]
        [RegForm_CheckSessionOut]
        public ActionResult AdmissionForm1(VM_Pre_AdmissionValidation a)
        {
            try
            {
                onlineregid = Convert.ToInt16(a.OnlineRegid);
                ViewBag.Regid = onlineregid;
                Admission1Services As1 = new Admission1Services();
                PreAdmissionPrimaryUserRegisterServices Pprimaryuser = new PreAdmissionPrimaryUserRegisterServices();
                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                PreAdmissionOnlineRegisterServices PonlineRegister = new PreAdmissionOnlineRegisterServices();
                PreAdmissionTransactionServices Admintrans = new PreAdmissionTransactionServices();
                SchoolSettingServices schoolsetting = new SchoolSettingServices();
                SchoolSettingsServices ss1 = new SchoolSettingsServices();
                PrimaryUserRegisterServices pu = new PrimaryUserRegisterServices();
                var stuadmissionid = PstudentRegister.getStudentid(onlineregid);
                //var stuid = stuadmissionid.StudentAdmissionId;
                var stuid = a.Stuadmissionid;
                var ans = PstudentRegister.findParticularStudentDetails(onlineregid, Convert.ToInt16(stuid));
                var result = PstudentRegister.GetPrevivousApplication(onlineregid);
                var user = PonlineRegister.getRow(onlineregid);
                var exist = pu.FindPrimaryUser(PonlineRegister.getRow(onlineregid).Username);
                string MatchEmailPattern =
      @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
+ @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
                if (user.Username != null)
                {
                    if (Regex.IsMatch(user.Username, MatchEmailPattern))
                    {
                        ViewBag.PrimaryUserEmail = user.Username;
                    }
                    else
                    {
                        ViewBag.PrimaryuserContact = user.Username.Trim();
                    }

                }
                var Settings = ss1.getSchoolSettings();
                ViewBag.Email = Settings.FirstOrDefault().EmailRequired;
                ViewBag.Sms = Settings.FirstOrDefault().SmsRequired;
                ViewBag.App = Settings.FirstOrDefault().AppRequired;
                if (result.Count > 0)
                {
                    if (result.FirstOrDefault().PreApplicationStatus != "InComplete" && result.FirstOrDefault().PreApplicationStatus != "")
                    {
                        ViewBag.oldstatus = "yes";
                    }
                    else
                    {
                        ViewBag.oldstatus = "no";
                    }
                }
                else
                {
                    ViewBag.oldstatus = "no";
                    if (exist > 0)
                    {
                        ViewBag.oldstatus = "yes";
                    }
                }

                var PreviousClass = ans.FirstOrDefault().AdmissionClass;
                ViewBag.Previousclass = null;
                if (PreviousClass != null)
                {
                    int classid = Convert.ToInt32(PreviousClass);
                    ViewBag.Previousclass = getpreviousclass(classid);
                }
                ViewBag.Noofsudent = Admintrans.GetNoOfStudent(PonlineRegister.getRow(onlineregid).Username);
                string PrimaryEmail = "";
                if (ans.Count != 0)
                {
                    if (ModelState.IsValid)
                    {
                        int count = 0;
                        //ViewBag.PrimaryUserEmail = ans.FirstOrDefault().primaryUserEmail;
                        int applyclass = Convert.ToInt16(a.AdmissionClass);
                        if (a.Edit != "Edit")
                        {
                            if (a.StudentPhoto != null && a.CommunityCertificate != null && a.BirthCertificate != null)
                            {
                                count++;
                            }
                            else
                            {
                                ViewBag.ErrorMessage = ResourceCache.Localize("Please_load_Student_Photo_,Community_Certificate_and_Birth_Certificate");
                                return View(ans);
                            }
                            if (applyclass == 1)
                            {
                                count++;
                            }
                            else if (applyclass != 1)
                            {
                                if (a.PreSchool != null && a.PreBoardOfSchool != null && a.PreClass != null) //&& a.PreMarks != null && a.PreFromDate != null && a.PreToDate != null
                                {
                                    count++;
                                    if (a.TransferCertificate != null)
                                    {
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_load_Transfer_Certificate");
                                        return View(ans);
                                    }
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_all_previous_school_information");
                                    return View(ans);
                                }
                            }
                            if (ViewBag.PrimaryUserEmail != "")
                            {
                                if (a.PrimaryUser == "Father")
                                {
                                    if (a.FatherFirstName != null && a.FatherLastName != null && a.FatherDOB != null && a.FatherEmail != null) // a.FatherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.FatherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Father_All_Information");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Mother")
                                {
                                    if (a.MotherFirstname != null && a.MotherLastName != null && a.MotherDOB != null && a.MotherEmail != null) // a.MotherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.MotherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Mother_All_Information");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Guardian")
                                {
                                    if (a.GuardianFirstname != null && a.GuardianLastname != null && a.GuardianEmail != null && a.GuardianDOB != null)
                                    {
                                        PrimaryEmail = a.GuardianEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Guardian_All__Information");
                                        return View(ans);
                                    }
                                }
                            }
                            else if (ViewBag.PrimaryuserContact != "")
                            {
                                if (a.PrimaryUser == "Father")
                                {
                                    if (a.FatherFirstName != null && a.FatherLastName != null && a.FatherDOB != null && a.FatherMobileNo != null) // a.FatherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.FatherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Father_All_Information");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Mother")
                                {
                                    if (a.MotherFirstname != null && a.MotherLastName != null && a.MotherDOB != null && a.MotherMobileNo != null) // a.MotherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.MotherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Mother_All_Information");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Guardian")
                                {
                                    if (a.GuardianFirstname != null && a.GuardianLastname != null && a.GuardianDOB != null && a.GuardianMobileNo != null)
                                    {
                                        PrimaryEmail = a.GuardianEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Guardian_All__Information");
                                        return View(ans);
                                    }
                                }
                            }

                        }
                        else if (a.Edit == "Edit")
                        {
                            if (a.StudentPhoto1 != null && a.CommunityCertificate1 != null && a.BirthCertificate1 != null)
                            {
                                count++;
                            }
                            else
                            {
                                ViewBag.ErrorMessage = ResourceCache.Localize("Please_load_Student_Photo_,Community_Certificate_and_Birth_Certificate");
                                return View(ans);
                            }
                            if (applyclass == 1)
                            {
                                count++;
                            }
                            else if (applyclass != 1)
                            {
                                if (a.PreSchool != null && a.PreBoardOfSchool != null && a.PreClass != null)//&& a.PreMarks != null && a.PreFromDate != null && a.PreToDate != null
                                {
                                    count++;
                                    if (a.TransferCertificate1 != null)
                                    {
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_load_Transfer_Certificate");
                                        return View(ans);
                                    }
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_all_previous_school_information ");
                                    return View(ans);
                                }
                            }
                            if (ViewBag.PrimaryUserEmail != "" && ViewBag.PrimaryUserEmail != null)
                            {
                                if (a.PrimaryUser == "Father")
                                {
                                    if (a.FatherFirstName != null && a.FatherLastName != null && a.txt_Fatherdob != null && a.FatherEmail != null) // a.FatherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.FatherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Father_All_Information ");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Mother")
                                {
                                    if (a.MotherFirstname != null && a.MotherLastName != null && a.txt_Motherdob != null && a.MotherEmail != null) // a.MotherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.MotherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Mother_All_Information");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Guardian")
                                {
                                    if (a.GuardianFirstname != null && a.GuardianLastname != null && a.GuardianEmail != null && a.txt_Guardiandob != null)
                                    {
                                        PrimaryEmail = a.GuardianEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Guardian_All__Information");
                                        return View(ans);
                                    }
                                }
                            }
                            else if (ViewBag.PrimaryuserContact != "" && ViewBag.PrimaryuserContact != null)
                            {
                                if (a.PrimaryUser == "Father")
                                {
                                    if (a.FatherFirstName != null && a.FatherLastName != null && a.txt_Fatherdob != null && a.FatherMobileNo != null) // a.FatherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.FatherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Father_All_Information ");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Mother")
                                {
                                    if (a.MotherFirstname != null && a.MotherLastName != null && a.txt_Motherdob != null && a.MotherMobileNo != null) // a.MotherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.MotherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Mother_All_Information");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Guardian")
                                {
                                    if (a.GuardianFirstname != null && a.GuardianLastname != null && a.txt_Guardiandob != null && a.GuardianMobileNo != null)
                                    {
                                        PrimaryEmail = a.GuardianEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Guardian_All__Information");
                                        return View(ans);
                                    }
                                }
                            }

                        }
                        if (count == 3)
                        {
                            var ss = PstudentRegister.GetStudentApplicationdetails(Convert.ToInt16(stuid));
                            var Primary = Pprimaryuser.FindPrimary(onlineregid);
                            ViewBag.Email = Settings.FirstOrDefault().EmailRequired;
                            ViewBag.Sms = Settings.FirstOrDefault().SmsRequired;
                            ViewBag.App = Settings.FirstOrDefault().AppRequired;
                            var school = schoolsetting.getSchoolSettings();
                            var SchoolName = school.SchoolName;
                            var SchoolLogo = school.SchoolLogo;
                            var Primaryuser = Primary.PrimaryUserName.ToString();
                            var Studentname = a.StuFirstName + " " + a.StuLastname;
                            var AppNo = stuid.ToString();
                            var year = ss.Academicyear.ToString();
                            var classs = ss.classs.ToString();
                            if ((ViewBag.Email == true && ViewBag.Sms == true && ViewBag.App == true) || (ViewBag.Email == true && ViewBag.Sms == true) || (ViewBag.Sms == true && ViewBag.App == true))
                            {
                                if (Primary.EmailRequired == true)
                                {
                                    var Femail = PrimaryEmail.ToString();
                                    var Username = Primary.PrimaryUserEmail.ToString();
                                    PreAdmissionEmail(Primaryuser, AppNo, Username, year, classs, Studentname, SchoolName, SchoolLogo);
                                }
                                if (Primary.SmsRequired == true)
                                {
                                    var contact = Primary.PrimaryUserContactNo.Value.ToString().Trim();
                                    PreAdmissionSMS(contact, Primaryuser, AppNo, Studentname, year, classs);
                                }
                            }
                            else if (ViewBag.Email == true || ViewBag.App == true)
                            {
                                // For sending Email to Father
                                if (PrimaryEmail != "")
                                {
                                    var Femail = PrimaryEmail.ToString();
                                    var Username = Primary.PrimaryUserEmail.ToString();
                                    PreAdmissionEmail(Primaryuser, AppNo, Username, year, classs, Studentname, SchoolName, SchoolLogo);

                                }
                            }
                            else if (ViewBag.Sms == true)
                            {
                                var contact = Primary.PrimaryUserContactNo.Value.ToString().Trim();
                                PreAdmissionSMS(contact, Primaryuser, AppNo, Studentname, year, classs);
                            }
                            string statusflag = "Enrolled";
                            bool result1 = PstudentRegister.UpdateStudentRegisterStatus(Convert.ToInt32(stuid), statusflag);
                            TempData["detailsallsaved"] = "Yes";
                            return RedirectToAction("SingleReport", new { Id = QSCrypt.Encrypt(Primary.PrimaryUserAdmissionId.ToString()), Sid = QSCrypt.Encrypt(stuid.ToString()) });
                        }
                        return View(ans);
                    }
                    else
                    {
                        ViewBag.modelState = ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                        return View(ans);
                    }
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        int count = 0;
                        int applyclass = Convert.ToInt16(a.AdmissionClass);
                        if (applyclass == 1)
                        {
                            count++;
                        }
                        else if (applyclass != 1)
                        {
                            if (a.PreSchool != "" || a.PreBoardOfSchool != null || a.PreClass != null)
                            {
                                count++;
                            }
                            else
                            {
                                ViewBag.modelState = ResourceCache.Localize("Please_fill_All_Previous_School_Information_And_load_transfercertificate");
                                return View(ans);
                            }
                        }
                        return View();
                    }
                    else
                    {
                        ViewBag.modelState = ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                        return View();
                    }
                }
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        public void PreAdmissionEmail(string PrimaryUser, string ApplicationNumber, string UserName, string AcademicYear, string Class, string Studentname, string SchoolName, string SchoolLogo)
        {
            StreamReader reader2 = new StreamReader(Server.MapPath("~/Template/SuperUserEmailPage.html"));
            string readFile2 = reader2.ReadToEnd();
            string myString2 = "";
            myString2 = readFile2;
            myString2 = myString2.Replace("$$PrimaryUser$$", PrimaryUser.ToString());
            myString2 = myString2.Replace("$$appno$$", ApplicationNumber.ToString());
            myString2 = myString2.Replace("$$username$$", UserName.ToString());
            myString2 = myString2.Replace("$$AcdemicYear$$", AcademicYear.ToString());
            myString2 = myString2.Replace("$$ApplyClass$$", Class.ToString());
            myString2 = myString2.Replace("$$StudentName$$", Studentname.ToString());
            myString2 = myString2.Replace("$$SchoolName$$", SchoolName.ToString());
            myString2 = myString2.Replace("$$SchoolLogo$$", SchoolLogo.ToString());
            string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
            string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
            string PlaceName = "OnlineRegister-PreAdmissionEmail-Application enrolled successfully";
            string User_Name = "Parent";
            string UserId = Convert.ToString(Session["OnlineUserName"]);



            Mail.SendMail(EmailDisplay, supportEmail, UserName, "Account Credentials", myString2.ToString(), true, PlaceName, User_Name, UserId);  

        }
        public void PreAdmissionSMS(string number, string PrimaryUser, string ApplicationNumber, string Studentname, string AcademicYear, string Class)
        {
            string ToNumber = number;
            string sub = "Application Status";
            string Description = "Hi " + PrimaryUser.ToString() + ", Your application Number=" + ApplicationNumber.ToString() + ", Student name=" + Studentname.ToString() + ", Academic Year=" + AcademicYear.ToString() + ", Apply Class=" + Class.ToString() + ", Successfully registered";
            string fromDisplayName = ConfigurationManager.AppSettings["EmailFromDisplayName"];
            SMS.Main(fromDisplayName, ToNumber, sub, Description);
        }
        public JsonResult CheckEmailExist(string Email, long? contact)
        {
            try
            {
                PreAdmissionPrimaryUserRegisterServices papurs = new PreAdmissionPrimaryUserRegisterServices();
                PrimaryUserRegisterServices purs = new PrimaryUserRegisterServices();

                long MobileNo = Convert.ToInt64(contact.Value);
                bool status = papurs.CheckPrimaryUserEmailExistusingMobile(MobileNo, Email);
                return Json(status, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage, }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CheckMobileExist(string Email, long? contact)
        {
            try
            {
                PreAdmissionPrimaryUserRegisterServices papurs = new PreAdmissionPrimaryUserRegisterServices();
                PrimaryUserRegisterServices purs = new PrimaryUserRegisterServices();

                long MobileNo = Convert.ToInt64(contact.Value);
                bool status = papurs.CheckPrimaryUserMobileExistusingMobile(MobileNo, Email);
                return Json(status, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage, }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SingleReport(string Id, string Sid)
        {
            try
            {
                SchoolSettingServices ss1 = new SchoolSettingServices();

                var primaryuserid = Convert.ToInt16(QSCrypt.Decrypt(Id));
                var studentid = Convert.ToInt16(QSCrypt.Decrypt(Sid));
                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                EmployeeDesinationServices employee_Design = new EmployeeDesinationServices();
                SchoolSettingsServices ss2 = new SchoolSettingsServices();
                TransportDestinationServices destination = new TransportDestinationServices();
                TransportPickPointServices pickpoint = new TransportPickPointServices();
                HostelConfigSubCategoriesServices hostelsubcategory = new HostelConfigSubCategoriesServices();
                BoardofSchoolServices board = new BoardofSchoolServices();
                var stu = 0;
                var ss = PstudentRegister.getOnlineregid(studentid);
                var formtype = ss1.getSchoolSettings();
                var ans = ss2.getSchoolSettings();
                ViewBag.Email = ans.FirstOrDefault().EmailRequired;
                ViewBag.Sms = ans.FirstOrDefault().SmsRequired;
                ViewBag.App = ans.FirstOrDefault().AppRequired;

                string formformat = "";
                if (formtype != null)
                {
                    if (formtype.FormFormat == "Single Form")
                    {
                        formformat = "AdmissionForm1";
                    }
                    else
                    {
                        formformat = "AdmissionForm4";
                    }
                    ViewBag.formformat = formformat;
                }
                ViewBag.Regid = QSCrypt.Encrypt(ss.OnlineRegisterId.ToString());
                ViewBag.sid = QSCrypt.Encrypt(stu.ToString());
                var result = PstudentRegister.GetStudentAlldetails(primaryuserid, studentid);
                if (result.FirstOrDefault().PreClass != null)
                {
                    ViewBag.previousclass_text = clas.GetParticularClass(Convert.ToInt32(result.FirstOrDefault().PreClass));
                }
                if (result.FirstOrDefault().Pre_BoardofSchool != null)
                {
                    ViewBag.previousBoardtxt = board.Showpreviousmedium(Convert.ToInt32(result.FirstOrDefault().Pre_BoardofSchool));
                }
                if (result.FirstOrDefault().TransportDestination != null && result.FirstOrDefault().TransportPickpoint != null)
                {
                    int destinationid = Convert.ToInt16(result.FirstOrDefault().TransportDestination);
                    int PickPointId = Convert.ToInt16(result.FirstOrDefault().TransportPickpoint);
                    ViewBag.TransportDestination = destination.getDestination(destinationid).DestinationName;
                    ViewBag.TransportPickpoint = pickpoint.GetPickPoint(destinationid, PickPointId);
                }
                if (result.FirstOrDefault().FoodFeeCategoryId != null)
                {
                    int foodFeesId = Convert.ToInt16(result.FirstOrDefault().FoodFeeCategoryId);
                    int AccomFeesid = Convert.ToInt16(result.FirstOrDefault().AccommodationFeeCategoryId);
                    if (result.FirstOrDefault().FoodSubFeeCategoryId != null)
                    {
                        int foodsubFeesId = Convert.ToInt16(result.FirstOrDefault().FoodSubFeeCategoryId);
                        ViewBag.FoodSubCategory = hostelsubcategory.GetSubCategory(foodFeesId, foodsubFeesId);
                    }
                    if (result.FirstOrDefault().AccommodationSubFeeCategoryId != null)
                    {
                        int AccomsubFeesId = Convert.ToInt16(result.FirstOrDefault().AccommodationSubFeeCategoryId);
                        ViewBag.AccomSubCategory = hostelsubcategory.GetSubCategory(AccomFeesid, AccomsubFeesId);
                    }
                }
                if (result.FirstOrDefault().EmployeeDesignationId != null)
                {
                    int id = Convert.ToInt16(result.FirstOrDefault().EmployeeDesignationId);
                    ViewBag.EmployeeDesignation = employee_Design.GetparticularEmployeeDesignation(id);
                }
                return View(result);
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult PDF1(M_StudentRegisteration r)
        {
            try
            {
                onlineregid = Convert.ToInt16(r.OnlineRegid);
                var studentid = Convert.ToInt16(r.Stuadmissionid);
                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                PreAdmissionTransactionServices Ptrans = new PreAdmissionTransactionServices();
                EmployeeDesinationServices employee_Design = new EmployeeDesinationServices();
                SchoolSettingsServices ss2 = new SchoolSettingsServices();
                TransportDestinationServices destination = new TransportDestinationServices();
                TransportPickPointServices pickpoint = new TransportPickPointServices();
                HostelConfigSubCategoriesServices hostelsubcategory = new HostelConfigSubCategoriesServices();
                BoardofSchoolServices board = new BoardofSchoolServices();
                var ans = ss2.getSchoolSettings();
                ViewBag.Email = ans.FirstOrDefault().EmailRequired;
                ViewBag.Sms = ans.FirstOrDefault().SmsRequired;
                ViewBag.App = ans.FirstOrDefault().AppRequired;
                var primaryuserid = Convert.ToInt16(Ptrans.FindPrimaryUserId(studentid).PrimaryUserAdmissionId);
                var result = PstudentRegister.GetStudentAlldetails(primaryuserid, studentid);
                if (result.FirstOrDefault().PreClass != null)
                {
                    ViewBag.previousclass_text = clas.GetParticularClass(Convert.ToInt32(result.FirstOrDefault().PreClass));
                }
                if (result.FirstOrDefault().Pre_BoardofSchool != null)
                {
                    ViewBag.previousBoardtxt = board.Showpreviousmedium(Convert.ToInt32(result.FirstOrDefault().Pre_BoardofSchool));
                }
                if (result.FirstOrDefault().TransportDestination != null && result.FirstOrDefault().TransportPickpoint != null)
                {
                    int destinationid = Convert.ToInt16(result.FirstOrDefault().TransportDestination);
                    int PickPointId = Convert.ToInt16(result.FirstOrDefault().TransportPickpoint);
                    ViewBag.TransportDestination = destination.getDestination(destinationid).DestinationName;
                    ViewBag.TransportPickpoint = pickpoint.GetPickPoint(destinationid, PickPointId);
                }
                if (result.FirstOrDefault().FoodFeeCategoryId != null)
                {
                    int foodFeesId = Convert.ToInt16(result.FirstOrDefault().FoodFeeCategoryId);
                    int AccomFeesid = Convert.ToInt16(result.FirstOrDefault().AccommodationFeeCategoryId);
                    if (result.FirstOrDefault().FoodSubFeeCategoryId != null)
                    {
                        int foodsubFeesId = Convert.ToInt16(result.FirstOrDefault().FoodSubFeeCategoryId);
                        ViewBag.FoodSubCategory = hostelsubcategory.GetSubCategory(foodFeesId, foodsubFeesId);
                    }
                    if (result.FirstOrDefault().AccommodationSubFeeCategoryId != null)
                    {
                        int AccomsubFeesId = Convert.ToInt16(result.FirstOrDefault().AccommodationSubFeeCategoryId);
                        ViewBag.AccomSubCategory = hostelsubcategory.GetSubCategory(AccomFeesid, AccomsubFeesId);
                    }
                }
                if (result.FirstOrDefault().EmployeeDesignationId != null)
                {
                    int id = Convert.ToInt16(result.FirstOrDefault().EmployeeDesignationId);
                    ViewBag.EmployeeDesignation = employee_Design.GetparticularEmployeeDesignation(id);
                }
                string cusomtSwitches = string.Format("--print-media-type  --header-html {0}  ",
                 Url.Action("Header", "OnlineRegister", new { area = "" }, "http"));
                if (result.FirstOrDefault().EmployeeDesignationId != null)
                {
                    int id = Convert.ToInt16(result.FirstOrDefault().EmployeeDesignationId);
                    ViewBag.EmployeeDesignation = employee_Design.GetparticularEmployeeDesignation(id);
                }
                return new ViewAsPdf("~/Views/OnlineRegister/Single_GeneratePDF.cshtml", result)
                {
                    FileName = "ApplicationNumber" + studentid + ".pdf",
                    // CustomSwitches = cusomtSwitches
                };
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        [AllowAnonymous]
        public ActionResult Header()
        {
            try
            {
                SchoolSettingServices ss = new SchoolSettingServices();
                var school = ss.getSchoolSettings();
                ViewBag.schoolName = school.SchoolName;
                return View();
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        public ActionResult StepReport(string Id, string Sid)
        {
            try
            {
                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                SchoolSettingServices ss1 = new SchoolSettingServices();
                EmployeeDesinationServices employee_Design = new EmployeeDesinationServices();
                SchoolSettingsServices ss2 = new SchoolSettingsServices();
                TransportDestinationServices destination = new TransportDestinationServices();
                TransportPickPointServices pickpoint = new TransportPickPointServices();
                HostelConfigSubCategoriesServices hostelsubcategory = new HostelConfigSubCategoriesServices();
                BoardofSchoolServices board = new BoardofSchoolServices();
                var primaryuserid = Convert.ToInt16(QSCrypt.Decrypt(Id));
                var studentid = Convert.ToInt16(QSCrypt.Decrypt(Sid));

                var stu = 0;
                var formtype = ss1.getSchoolSettings();
                var ans = ss2.getSchoolSettings();
                ViewBag.Email = ans.FirstOrDefault().EmailRequired;
                ViewBag.Sms = ans.FirstOrDefault().SmsRequired;
                ViewBag.App = ans.FirstOrDefault().AppRequired;
                string formformat = "";
                if (formtype != null)
                {
                    if (formtype.FormFormat == "Single Form")
                    {
                        formformat = "AdmissionForm1";
                    }
                    else
                    {
                        formformat = "AdmissionForm4";
                    }


                    ViewBag.formformat = formformat;
                }
                var ss = PstudentRegister.getOnlineregid(studentid);
                ViewBag.Regid = QSCrypt.Encrypt(ss.OnlineRegisterId.ToString());
                ViewBag.sid = QSCrypt.Encrypt(stu.ToString());

                var result = PstudentRegister.GetStudentAlldetails(primaryuserid, studentid);
                if (result.FirstOrDefault().PreClass != null)
                {
                    ViewBag.previousclass_text = clas.GetParticularClass(Convert.ToInt32(result.FirstOrDefault().PreClass));
                }
                if (result.FirstOrDefault().Pre_BoardofSchool != null)
                {
                    ViewBag.previousBoardtxt = board.Showpreviousmedium(Convert.ToInt32(result.FirstOrDefault().Pre_BoardofSchool));
                }
                if (result.FirstOrDefault().TransportDestination != null && result.FirstOrDefault().TransportPickpoint != null)
                {
                    int destinationid = Convert.ToInt16(result.FirstOrDefault().TransportDestination);
                    int PickPointId = Convert.ToInt16(result.FirstOrDefault().TransportPickpoint);
                    ViewBag.TransportDestination = destination.getDestination(destinationid).DestinationName;
                    ViewBag.TransportPickpoint = pickpoint.GetPickPoint(destinationid, PickPointId);
                }
                if (result.FirstOrDefault().FoodFeeCategoryId != null)
                {
                    int foodFeesId = Convert.ToInt16(result.FirstOrDefault().FoodFeeCategoryId);
                    int AccomFeesid = Convert.ToInt16(result.FirstOrDefault().AccommodationFeeCategoryId);
                    if (result.FirstOrDefault().FoodSubFeeCategoryId != null)
                    {
                        int foodsubFeesId = Convert.ToInt16(result.FirstOrDefault().FoodSubFeeCategoryId);
                        ViewBag.FoodSubCategory = hostelsubcategory.GetSubCategory(foodFeesId, foodsubFeesId);
                    }
                    if (result.FirstOrDefault().AccommodationSubFeeCategoryId != null)
                    {
                        int AccomsubFeesId = Convert.ToInt16(result.FirstOrDefault().AccommodationSubFeeCategoryId);
                        ViewBag.AccomSubCategory = hostelsubcategory.GetSubCategory(AccomFeesid, AccomsubFeesId);
                    }
                }
                if (result.FirstOrDefault().EmployeeDesignationId != null)
                {
                    int id = Convert.ToInt16(result.FirstOrDefault().EmployeeDesignationId);
                    ViewBag.EmployeeDesignation = employee_Design.GetparticularEmployeeDesignation(id);
                }
                return View(result);
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult PDF2(M_StudentRegisteration r)
        {
            try
            {
                onlineregid = Convert.ToInt16(r.OnlineRegid);
                var studentid = Convert.ToInt16(r.Stuadmissionid);
                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                PreAdmissionTransactionServices Ptrans = new PreAdmissionTransactionServices();
                EmployeeDesinationServices employee_Design = new EmployeeDesinationServices();
                SchoolSettingsServices ss2 = new SchoolSettingsServices();
                TransportDestinationServices destination = new TransportDestinationServices();
                TransportPickPointServices pickpoint = new TransportPickPointServices();
                HostelConfigSubCategoriesServices hostelsubcategory = new HostelConfigSubCategoriesServices();
                BoardofSchoolServices board = new BoardofSchoolServices();
                var ans = ss2.getSchoolSettings();
                ViewBag.Email = ans.FirstOrDefault().EmailRequired;
                ViewBag.Sms = ans.FirstOrDefault().SmsRequired;
                ViewBag.App = ans.FirstOrDefault().AppRequired;
                var primaryuserid = Convert.ToInt16(Ptrans.FindPrimaryUserId(studentid).PrimaryUserAdmissionId);
                var result = PstudentRegister.GetStudentAlldetails(primaryuserid, studentid);
                if (result.FirstOrDefault().PreClass != null)
                {
                    ViewBag.previousclass_text = clas.GetParticularClass(Convert.ToInt32(result.FirstOrDefault().PreClass));
                }
                if (result.FirstOrDefault().Pre_BoardofSchool != null)
                {
                    ViewBag.previousBoardtxt = board.Showpreviousmedium(Convert.ToInt32(result.FirstOrDefault().Pre_BoardofSchool));
                }
                if (result.FirstOrDefault().TransportDestination != null && result.FirstOrDefault().TransportPickpoint != null)
                {
                    int destinationid = Convert.ToInt16(result.FirstOrDefault().TransportDestination);
                    int PickPointId = Convert.ToInt16(result.FirstOrDefault().TransportPickpoint);
                    ViewBag.TransportDestination = destination.getDestination(destinationid).DestinationName;
                    ViewBag.TransportPickpoint = pickpoint.GetPickPoint(destinationid, PickPointId);
                }
                if (result.FirstOrDefault().FoodFeeCategoryId != null)
                {
                    int foodFeesId = Convert.ToInt16(result.FirstOrDefault().FoodFeeCategoryId);
                    int AccomFeesid = Convert.ToInt16(result.FirstOrDefault().AccommodationFeeCategoryId);
                    if (result.FirstOrDefault().FoodSubFeeCategoryId != null)
                    {
                        int foodsubFeesId = Convert.ToInt16(result.FirstOrDefault().FoodSubFeeCategoryId);
                        ViewBag.FoodSubCategory = hostelsubcategory.GetSubCategory(foodFeesId, foodsubFeesId);
                    }
                    if (result.FirstOrDefault().AccommodationSubFeeCategoryId != null)
                    {
                        int AccomsubFeesId = Convert.ToInt16(result.FirstOrDefault().AccommodationSubFeeCategoryId);
                        ViewBag.AccomSubCategory = hostelsubcategory.GetSubCategory(AccomFeesid, AccomsubFeesId);
                    }
                }
                string cusomtSwitches = string.Format("--print-media-type  --header-html {0}  ",
               Url.Action("Header", "OnlineRegister", new { area = "" }, "http"));
                if (result.FirstOrDefault().EmployeeDesignationId != null)
                {
                    int id = Convert.ToInt16(result.FirstOrDefault().EmployeeDesignationId);
                    ViewBag.EmployeeDesignation = employee_Design.GetparticularEmployeeDesignation(id);
                }
                return new ViewAsPdf("~/Views/OnlineRegister/Step_GeneratePDF.cshtml", result)
                {
                    FileName = "ApplicationNumber" + studentid + ".pdf",
                    // CustomSwitches = cusomtSwitches
                };
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        public JsonResult getAgelimitdate(int? AdminssionClassId, int? AcademicyearId)
        {
            try
            {
                AsignClassVacancyServices acs = new AsignClassVacancyServices();
                var Getdate = acs.getdate(AdminssionClassId, AcademicyearId);
                string date = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
                if (Getdate != null)
                {
                    date = Getdate.DateofAgeLimit.Value.ToString("dd/MM/yyyy");
                }
                return Json(new { date }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetSelectedDestinationPickPoints(int passDestinationId)
        {
            TransportPickPointServices pickPtServ = new TransportPickPointServices();
            var List = pickPtServ.getPickPoints(passDestinationId);
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetPickPointAmount(int passPickPointId)
        {
            TransportPickPointServices pickPtServ = new TransportPickPointServices();
            var getFeeAmount = pickPtServ.getPickPointAmount(passPickPointId);
            Decimal FeeAmount = Math.Round(getFeeAmount.Amount.Value, 2, MidpointRounding.AwayFromZero);
            SchoolSettingsServices obj_settings = new SchoolSettingsServices();
            var ans = obj_settings.SchoolSettings();
            string ServiceTax = ans.ServicesTax.ToString();
            string VAT = ans.VAT.ToString();
            var Total = FeeAmount;
            return Json(new { FeeAmount, ServiceTax, VAT, Total }, JsonRequestBehavior.AllowGet);
        }
        //json for Save button click
        [HttpPost]
        public ActionResult form1(M_StudentRegisteration r)
        {
            try
            {
                PreAdmissionOnlineRegisterServices PonlineRegister = new PreAdmissionOnlineRegisterServices();
                PreAdmissionPrimaryUserRegisterServices Pprimaryuser = new PreAdmissionPrimaryUserRegisterServices();
                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                PreAdmissionTransactionServices PAdmissionTransaction = new PreAdmissionTransactionServices();
                PreAdmissionFatherRegisterServices PFatherRegister = new PreAdmissionFatherRegisterServices();
                PreAdmissionMotherRegisterServices Pmotherregister = new PreAdmissionMotherRegisterServices();
                PreAdmissionGuardianRegisterServices PguardianRegister = new PreAdmissionGuardianRegisterServices();
                PreAdmissionParentsSblingServices Pparentssibling = new PreAdmissionParentsSblingServices();
                SchoolSettingServices schoolsetting = new SchoolSettingServices();
                onlineregid = Convert.ToInt16(r.OnlineRegid);
                int count = Convert.ToInt16(r.NoOfSbling);
                int studentid = Convert.ToInt16(r.Stuadmissionid);
                string[] years = new string[count];
                string[] rolls = new string[count];
                string[] classes = new string[count];
                string[] sections = new string[count];
                string SiblingStatus = "";
                if (r.NoOfSbling != 0 && r.NoOfSbling != null)
                {
                    SiblingStatus = "Yes";
                }
                else if (r.NoOfSbling == 0)
                {
                    SiblingStatus = "No";
                }
                int PrimaryAdmissionId = 0; int StudentAdmissionId = 0; int FatherAdmissionId = 0; int MotherAdmissionId = 0; int GuardianAdmissionId = 0;
                var CheckPrimaryUser = Pprimaryuser.FindPrimary(onlineregid);
                if (CheckPrimaryUser == null)
                {
                    PrimaryAdmissionId = Pprimaryuser.AddPrimaryUser(onlineregid, r.PrimaryUser, r.primaryUserEmail, r.primaryUserContact, r.CompanyAddress1, r.CompanyAddress2, r.CompanyCity, r.CompanyState, r.CompanyCountry, r.CompanyPostelcode, r.CompanyContact, r.UserCompanyname, r.WorkSameSchool, r.EmployeeDesignationId, r.EmailRequired, r.SmsRequired, r.EmployeeId);
                }
                else
                {
                    PrimaryAdmissionId = Pprimaryuser.UpdatePrimaryUser(onlineregid, r.PrimaryUser, r.primaryUserEmail, r.primaryUserContact, r.CompanyAddress1, r.CompanyAddress2, r.CompanyCity, r.CompanyState, r.CompanyCountry, r.CompanyPostelcode, r.CompanyContact, r.UserCompanyname, r.WorkSameSchool, r.EmployeeDesignationId, r.EmailRequired, r.SmsRequired, r.EmployeeId);
                }
                if (studentid == 0)
                {
                    bool exist = PstudentRegister.GetStudentDetailsExist(PrimaryAdmissionId, r.StuFirstName, r.StuLastname, r.DOB);
                    if (exist == false)
                    {
                        StudentAdmissionId = PstudentRegister.AddStudentdetailsForm1(onlineregid, r.StuFirstName, r.StuMiddlename, r.StuLastname, r.Gender, r.DOB, r.PlaceOfBirth, r.Community, r.Religion, r.Nationality, r.BloodGroup, r.AdmissionClass, r.LocAddress1, r.LocAddress2, r.LocCity, r.LocState, r.LocCountry,
                            r.LocPostelcode, r.NoOfSbling, r.AcademicyearId, r.Distance, SiblingStatus, r.GuardianRequried, r.EmergencyContactPersonName, r.EmergencyContactNumber, r.ContactPersonRelationship,r.Email,r.Contact);
                        int name = StudentAdmissionId;
                        var path = Server.MapPath("~/Documents/" + name);
                        System.IO.Directory.CreateDirectory(path);
                        //Directory.CreateDirectory(path);
                        var checkprimaryuserintransaction = PAdmissionTransaction.FindPrimaryuser(PrimaryAdmissionId);
                        if (checkprimaryuserintransaction != null)
                        {
                            if (checkprimaryuserintransaction.PrimaryUserAdmissionId == PrimaryAdmissionId && checkprimaryuserintransaction.StudentAdmissionId == null)
                            {
                                PAdmissionTransaction.UpdateStudentId(PrimaryAdmissionId, StudentAdmissionId);
                            }
                            else if (StudentAdmissionId != 0)
                            {
                                PAdmissionTransaction.AddPrimaryUserandStudentId(PrimaryAdmissionId, StudentAdmissionId);
                            }
                        }
                        else
                        {
                            PAdmissionTransaction.AddPrimaryUserandStudentId(PrimaryAdmissionId, StudentAdmissionId);
                        }
                    }
                    else
                    {
                        String ErrorMessage = ResourceCache.Localize("Student_details_already_exist_Dont_repeat_same_student_details");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    bool exist = PstudentRegister.GetStudentDetailsExist(PrimaryAdmissionId, studentid, r.StuFirstName, r.StuLastname, r.DOB);
                    if (exist == false)
                    {
                        StudentAdmissionId = PstudentRegister.UpdateStudentdetailsForm1(onlineregid, r.StuFirstName, r.StuMiddlename, r.StuLastname, r.Gender, r.DOB, r.PlaceOfBirth, r.Community, r.Religion, r.Nationality, r.BloodGroup, r.AdmissionClass, r.LocAddress1, r.LocAddress2, r.LocCity, r.LocState, r.LocCountry,
                            r.LocPostelcode, r.NoOfSbling, r.AcademicyearId, r.Distance, SiblingStatus, r.Stuadmissionid, r.GuardianRequried, r.EmergencyContactPersonName, r.EmergencyContactNumber, r.ContactPersonRelationship,r.Email,r.Contact);
                    }
                    else
                    {
                        String ErrorMessage = ResourceCache.Localize("Student_details_already_exist_Dont_repeat_same_student_details");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                PstudentRegister.UpdateStudentTransportDetails(StudentAdmissionId, r.TransportRequired, r.TransportDestination, r.TransportPickpoint, r.TransportFeeAmount);
                PstudentRegister.UpdateStudentHostelDetails(StudentAdmissionId, r.HostelRequired, r.AccommodationFeeCategoryId, r.AccommodationSubFeeCategoryId, r.FoodFeeCategoryId, r.FoodSubFeeCategoryId);
                //ADD Father Details
                var NoOfStudents = PAdmissionTransaction.GetRemainingStudent(PrimaryAdmissionId, StudentAdmissionId);
                if (NoOfStudents.Count == 0)
                {
                    var checkuser = PAdmissionTransaction.GetPrimaryUser(PrimaryAdmissionId, StudentAdmissionId);
                    if (r.PrimaryUser == "Father")
                    {
                        if (checkuser.FatherAdmissionId == null)
                        {
                            if (r.FatherFirstName != null && r.FatherLastName != null && r.FatherDOB != null)//&& r.FatherEmail != null && r.FatherMobileNo != null
                            {
                                FatherAdmissionId = PFatherRegister.AddFatherdetails(r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                                PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                            }
                        }
                        else
                        {
                            PFatherRegister.UpdatefatherDetails(checkuser.FatherAdmissionId, r.FatherQualification, r.FatherOccupation, r.FatherEmail, r.FatherMobileNo);
                            Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                        }
                        if (checkuser.MotherAdmissionId == null)
                        {
                            if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                            {
                                MotherAdmissionId = Pmotherregister.AddMotherdetails(r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                                PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                            }
                        }
                        else
                        {
                            Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                        }
                        if (checkuser.GuardianAdmissionId == null)
                        {
                            if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                            {
                                GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                            }
                        }
                        else
                        {
                            PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                        }
                    }
                    else if (r.PrimaryUser == "Mother")
                    {
                        if (checkuser.MotherAdmissionId == null)
                        {
                            if (r.MotherFirstname != null && r.MotherLastName != null && r.MotherDOB != null)//&& r.MotherEmail != null && r.MotherMobileNo != null
                            {
                                MotherAdmissionId = Pmotherregister.AddMotherdetails(r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                                PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                            }
                        }
                        else
                        {
                            Pmotherregister.UpdatemotherDetails(checkuser.MotherAdmissionId, r.MotherQualification, r.MotherOccupation, r.MotherEmail, r.MotherMobileNo);
                            Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                        }
                        if (checkuser.FatherAdmissionId == null)
                        {
                            if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                            {
                                FatherAdmissionId = PFatherRegister.AddFatherdetails(r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                                PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                            }
                        }
                        else
                        {
                            PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                        }
                        if (checkuser.GuardianAdmissionId == null)
                        {
                            if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                            {
                                GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                            }
                        }
                        else
                        {
                            PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.FatherLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                        }
                    }
                    else if (r.PrimaryUser == "Guardian")
                    {
                        if (checkuser.GuardianAdmissionId == null)
                        {
                            if (r.GuardianFirstname != null && r.GuardianLastName != null && r.GuardianDOB != null)//&& r.GuardianEmail != null && r.GuardianMobileNo != null
                            {
                                GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                            }
                        }
                        else
                        {
                            PguardianRegister.UpdateGuardianDetails(checkuser.GuardianAdmissionId, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                            Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                        }
                        if (checkuser.MotherAdmissionId == null)
                        {
                            if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null || r.TotalIncome != null)
                            {
                                MotherAdmissionId = Pmotherregister.AddMotherdetails(r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                                PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                            }
                        }
                        else
                        {
                            Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                        }
                        if (checkuser.FatherAdmissionId == null)
                        {
                            if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null | r.TotalIncome != null)
                            {
                                FatherAdmissionId = PFatherRegister.AddFatherdetails(r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                                PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                            }
                        }
                        else
                        {
                            PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                        }
                    }
                }
                else
                {
                    var checkuser = PAdmissionTransaction.GetPrimaryUser(PrimaryAdmissionId, StudentAdmissionId);
                    if (r.PrimaryUser == "Father")
                    {
                        if (checkuser.FatherAdmissionId == null)
                        {
                            if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null || r.TotalIncome!= null)
                            {
                                FatherAdmissionId = PFatherRegister.CheckFatherExit(PrimaryAdmissionId, StudentAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                                PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                            }
                        }
                        else
                        {
                            PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                            Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                        }
                        if (checkuser.MotherAdmissionId == null)
                        {
                            if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null || r.TotalIncome != null)
                            {
                                MotherAdmissionId = Pmotherregister.CheckMotherExit(PrimaryAdmissionId, StudentAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                                PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                            }
                        }
                        else
                        {
                            Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                        }
                        if (checkuser.GuardianAdmissionId == null)
                        {
                            if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null ||  r.TotalIncome != null)
                            {
                                GuardianAdmissionId = PguardianRegister.CheckGuardianExit(PrimaryAdmissionId, StudentAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                            }
                        }
                        else
                        {
                            PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                        }
                    }
                    else if (r.PrimaryUser == "Mother")
                    {
                        if (checkuser.MotherAdmissionId == null)
                        {
                            if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null || r.TotalIncome != null)
                            {
                                MotherAdmissionId = Pmotherregister.CheckMotherExit(PrimaryAdmissionId, StudentAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                                PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                            }
                        }
                        else
                        {
                            Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                            Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                        }
                        if (checkuser.FatherAdmissionId == null)
                        {
                            if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null || r.TotalIncome != null)
                            {
                                FatherAdmissionId = PFatherRegister.CheckFatherExit(PrimaryAdmissionId, StudentAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                                PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                            }
                        }
                        else
                        {
                            PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                        }
                        if (checkuser.GuardianAdmissionId == null)
                        {
                            if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                            {
                                GuardianAdmissionId = PguardianRegister.CheckGuardianExit(PrimaryAdmissionId, StudentAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                            }
                        }
                        else
                        {
                            PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                        }
                    }
                    else if (r.PrimaryUser == "Guardian")
                    {
                        if (checkuser.GuardianAdmissionId == null)
                        {
                            if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                            {
                                GuardianAdmissionId = PguardianRegister.CheckGuardianExit(PrimaryAdmissionId, StudentAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                            }
                        }
                        else
                        {
                            PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                            Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                        }
                        if (checkuser.FatherAdmissionId == null)
                        {
                            if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                            {
                                FatherAdmissionId = PFatherRegister.CheckFatherExit(PrimaryAdmissionId, StudentAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                                PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                            }
                        }
                        else
                        {
                            PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                        }
                        if (checkuser.MotherAdmissionId == null)
                        {
                            if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                            {
                                MotherAdmissionId = Pmotherregister.CheckMotherExit(PrimaryAdmissionId, StudentAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                                PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                            }
                        }
                        else
                        {
                            Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                        }
                    }
                }

                //Sibling details
                if (count != 0)//NoOfSibling Count
                {
                    Pparentssibling.UpdateSblingdetails(onlineregid, StudentAdmissionId);
                }
                int AdmissionClass = Convert.ToInt16(r.AdmissionClass);
                if (AdmissionClass != 1 && AdmissionClass != 0)
                {
                    PstudentRegister.UpdatePreviousSchool(StudentAdmissionId, r.PreSchool, r.PreClass, r.Pre_BoardofSchool, r.PreMarks, r.PreFromDate1, r.PreToDate);
                }
                string Message = ResourceCache.Localize("Sucessfully_records_are_added");
                return Json(new { Message = Message, StudentAdmissionId = StudentAdmissionId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult UploadFile()
        {
            try
            {
                onlineregid = Convert.ToInt16(Request["OnlineRegId"]);
                PreAdmissionStudentRegisterServices Pstudentregister = new PreAdmissionStudentRegisterServices();
                var stuadmissionid = Pstudentregister.getStudentid(onlineregid);
                var stuid = stuadmissionid.StudentAdmissionId;
                int pathname = stuadmissionid.StudentAdmissionId;
                string name = ""; long FileSzie = 0;
                string BirthCertificate = "";
                string IncomeCertificate = ""; string TransferCertificate = ""; string CommunityCertificate = "";
                string studentphoto = "";
                FileHelper fh = new FileHelper();
                if (stuadmissionid != null)
                {
                    var Birth = HttpContext.Request.Files["UploadBirth"];
                    if (Birth != null)
                    {
                        name = "BC";
                        FileSzie = 512000;
                        var BirthCertificate1 = fh.CheckFile(Birth, pathname, name, FileSzie);
                        if (BirthCertificate1.FileSizeId == 98)
                        {
                            string ErrorMessage = ResourceCache.Localize("Birth_certificate_file_size_exceeds,whithin_512kb");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (BirthCertificate1.FileTypeId == 99)
                        {
                            string ErrorMessage = ResourceCache.Localize("Birth_certificate_file_format_not_supported");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (BirthCertificate1.FileSizeId != 98 && BirthCertificate1.FileTypeId != 99 && BirthCertificate1.filename != "")
                        {
                            BirthCertificate = BirthCertificate1.filename;
                            Pstudentregister.updateBirthcertificate(onlineregid, stuid, BirthCertificate);
                        }
                    }
                    var community = HttpContext.Request.Files["UploadCommunity"];
                    if (community != null)
                    {
                        name = "CC";
                        FileSzie = 512000;
                        var CommunityCertificate1 = fh.CheckFile(community, pathname, name, FileSzie);
                        if (CommunityCertificate1.FileSizeId == 98)
                        {
                            string ErrorMessage = ResourceCache.Localize("Community_certificate_file_size_exceeds,whithin_512kb");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (CommunityCertificate1.FileTypeId == 99)
                        {
                            string ErrorMessage = ResourceCache.Localize("Community_certificate_file_format_not_supported");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (CommunityCertificate1.FileSizeId != 98 && CommunityCertificate1.FileTypeId != 99 && CommunityCertificate1.filename != "")
                        {
                            CommunityCertificate = CommunityCertificate1.filename;
                            Pstudentregister.updateCommunitycertificate(onlineregid, stuid, CommunityCertificate);
                        }
                    }
                    var Income = HttpContext.Request.Files["UploadIncome"];
                    if (Income != null)
                    {
                        name = "IC";
                        FileSzie = 512000;
                        var IncomeCertificate1 = fh.CheckFile(Income, pathname, name, FileSzie);
                        if (IncomeCertificate1.FileSizeId == 98)
                        {
                            string ErrorMessage = ResourceCache.Localize("Income_certificate_file_size_exceeds,whithin_512kb");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (IncomeCertificate1.FileTypeId == 99)
                        {
                            string ErrorMessage = ResourceCache.Localize("Income_certificate_file_format_not_supported");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (IncomeCertificate1.FileSizeId != 98 && IncomeCertificate1.FileTypeId != 99 && IncomeCertificate1.filename != "")
                        {
                            IncomeCertificate = IncomeCertificate1.filename;
                            Pstudentregister.updateIncomecertificate(onlineregid, stuid, IncomeCertificate);
                        }
                    }
                    var Transfer = HttpContext.Request.Files["UploadTransfer"];
                    if (Transfer != null)
                    {
                        name = "TC";
                        FileSzie = 512000;
                        var TransferCertificate1 = fh.CheckFile(Transfer, pathname, name, FileSzie);
                        if (TransferCertificate1.FileSizeId == 98)
                        {
                            string ErrorMessage = ResourceCache.Localize("Transfer_certificate_file_size_exceeds,whithin_512kb");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (TransferCertificate1.FileTypeId == 99)
                        {
                            string ErrorMessage = ResourceCache.Localize("Transfer_certificate_file_format_not_supported");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (TransferCertificate1.FileSizeId != 98 && TransferCertificate1.FileTypeId != 99 && TransferCertificate1.filename != "")
                        {
                            TransferCertificate = TransferCertificate1.filename;
                            Pstudentregister.updateTransfercertificate(onlineregid, stuid, TransferCertificate);
                        }
                    }
                    var Image = HttpContext.Request.Files["UploadImage"];
                    if (Image != null)
                    {
                        name = "Photo";
                        FileSzie = 512000;
                        var Photo1 = fh.CheckImageFile(Image, pathname, name, FileSzie);
                        if (Photo1.FileSizeId == 98)
                        {
                            string ErrorMessage = ResourceCache.Localize("Student_photo_file_size_exceeds,whithin_512kb");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (Photo1.FileTypeId == 99)
                        {
                            string ErrorMessage = ResourceCache.Localize("Student_photo_file_format_not_supported");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        else if (Photo1.FileSizeId != 98 && Photo1.FileTypeId != 99 && Photo1.filename != "")
                        {
                            studentphoto = Photo1.filename;
                            Pstudentregister.updateStudentPhoto(onlineregid, stuid, studentphoto);
                        }
                    }
                    string Message = ResourceCache.Localize("Records_Added_sucessfully ") + ResourceCache.Localize("Please_Btn") + " <button class='btn btn-md btn_search btn-success nextBtn btn-md' type='submit'>" + ResourceCache.Localize("Clickhere") + "</button> " + ResourceCache.Localize("toconfirmyourdetails");
                    return Json(new { Message = Message, BirthCertificate = BirthCertificate, CommunityCertificate = CommunityCertificate, TransferCertificate = TransferCertificate, studentphoto = studentphoto, IncomeCertificate = IncomeCertificate }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("Please_Required_Files");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage, }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult sibling(M_StudentRegisteration r)
        {
            try
            {
                PreAdmissionParentsSblingServices pss = new PreAdmissionParentsSblingServices();
                int regid = Convert.ToInt16(r.OnlineRegid);
                var ans = pss.GetJsonSiblingonline(regid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetSibling(M_StudentRegisteration r)
        {
            try
            {
                PreAdmissionParentsSblingServices pss = new PreAdmissionParentsSblingServices();
                int regid = Convert.ToInt16(r.Stuadmissionid);
                var ans = pss.GetJsonSiblingStudentId(regid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [RegForm_CheckSessionOut]
        public ActionResult FeesStructure(string id)
        {
            try
            {
                PreAdmissionStudentRegisterServices pstudent = new PreAdmissionStudentRegisterServices();
                ViewBag.Regid = QSCrypt.Decrypt(id);
                int regid = Convert.ToInt16(QSCrypt.Decrypt(id));
                var ans = pstudent.GetYearAndClass(regid);
                ViewBag.Regid = QSCrypt.Encrypt(ans.FirstOrDefault().OnlineRegid.ToString());
                ViewBag.sid = QSCrypt.Encrypt(ans.FirstOrDefault().StudentAdmissionId.ToString());
                return View(ans);
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        [HttpGet]
        public JsonResult getFeesStructure(VM_GetFeeStructure s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdminServices ps = new PreAdminServices();
                    int yearid = Convert.ToInt16(s.AcademicYearId);
                    int classid = Convert.ToInt16(s.ClassId);
                    var result = ps.getallFeesByID(yearid, classid);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult GetPayFeesStructure(VM_GetFeeStructure s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdminServices ps = new PreAdminServices();
                    PreAdmissionStudentRegisterServices pstudent = new PreAdmissionStudentRegisterServices();
                    int yearid = Convert.ToInt16(s.AcademicYearId);
                    int classid = Convert.ToInt16(s.ClassId);
                    var result = ps.getallFeesByID(yearid, classid);
                    var tid = ps.GetAcademicfirsttermId(yearid);
                    int termid = Convert.ToInt16(tid);
                    int appid = Convert.ToInt32(s.ApplicationNumber);
                    var getstudentdetails = pstudent.StudentDetails(appid);
                    HostelFeesCategoryServices HP = new HostelFeesCategoryServices();
                    TransportPickPointServices Tp = new TransportPickPointServices();
                    var lastdate = getstudentdetails.FeePayLastDate.Value.ToString("dd/mm/yyyy");
                    if (getstudentdetails.TransportRequired == true && getstudentdetails.HostelRequired == false)
                    {
                        var getTransportfeeid = Tp.gettransfortfeestructure(getstudentdetails.TransportDesignation, getstudentdetails.TransportPickpoint);
                        return Json(new { result, getTransportfeeid, lastdate }, JsonRequestBehavior.AllowGet);
                    }
                    else if (getstudentdetails.HostelRequired == true && getstudentdetails.TransportRequired == false)
                    {
                        var gethostelfee = HP.getHostelfee(getstudentdetails.AcademicyearId, getstudentdetails.AccommodationFeeCategoryId, getstudentdetails.AccommodationSubFeeCategoryId, getstudentdetails.FoodFeeCategoryId, getstudentdetails.FoodSubFeeCategoryId);
                        return Json(new { result, gethostelfee, lastdate }, JsonRequestBehavior.AllowGet);
                    }
                    else if (getstudentdetails.HostelRequired == true && getstudentdetails.TransportRequired == true)
                    {
                        var gethostelfee = HP.getHostelfee(getstudentdetails.AcademicyearId, getstudentdetails.AccommodationFeeCategoryId, getstudentdetails.AccommodationSubFeeCategoryId, getstudentdetails.FoodFeeCategoryId, getstudentdetails.FoodSubFeeCategoryId);
                        var getTransportfeeid = Tp.gettransfortfeestructure(getstudentdetails.TransportDesignation, getstudentdetails.TransportPickpoint);
                        return Json(new { result, gethostelfee, getTransportfeeid, lastdate }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var gethostelfee = HP.getHostelfee(getstudentdetails.AcademicyearId, getstudentdetails.AccommodationFeeCategoryId, getstudentdetails.AccommodationSubFeeCategoryId, getstudentdetails.FoodFeeCategoryId, getstudentdetails.FoodSubFeeCategoryId);
                        return Json(new { result, lastdate }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult PayFees(VM_PayFees s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Admission1Services As1 = new Admission1Services();
                    PreAdminServices ps = new PreAdminServices();
                    PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
                    var count = s.NoOfFees;
                    string[] FeeCategoryId = new string[count];
                    string[] PaymentTypeId = new string[count];
                    string[] Amount = new string[count];
                    string[] ServiceTax = new string[count];
                    string[] Total = new string[count];
                    var Total_Amount = s.TotalAmount;
                    string statusflag = "";
                    var feecollectionId = As1.AddFeeCollection(DateTimeByZone.getCurrentDateTime(), s.AcademicYearId, s.ClassId, s.TotalAmount, s.ApplicationId);
                    int FeeCollectid = Convert.ToInt16(feecollectionId);
                    if (count != 0)//NoOfSibling Count
                    {
                        for (int i = 0; i < count; i++)
                        {
                            FeeCategoryId[i] = String.Join(" ", s.feecategoryid[i]);
                            PaymentTypeId[i] = String.Join(" ", s.paymenttypeid[i]);
                            Amount[i] = String.Join(" ", s.amount[i]);
                            ServiceTax[i] = String.Join(" ", s.servicetax[i]);
                            Total[i] = String.Join(" ", s.total[i]);
                            int feeCatId = Convert.ToInt16(FeeCategoryId[i]);
                            int PaymentId = Convert.ToInt16(PaymentTypeId[i]);
                            decimal amt = Convert.ToDecimal(Amount[i]);
                            decimal sertax = Convert.ToDecimal(ServiceTax[i]);
                            decimal tot = Convert.ToDecimal(Total[i]);
                            As1.AddFeeCollectionCategoryFees(FeeCollectid, feeCatId, PaymentId, amt, sertax, tot, s.AcademicYearId, s.ClassId, Convert.ToInt16(s.ApplicationId));
                        }
                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Collect_PreAdmission_Fees"));
                        statusflag = "FeesPaid";
                    }
                    bool result = Pstudent.UpdateStudentRegisterStatusFeesCollection(s.ApplicationId, statusflag);
                    string encrptycollectionid = QSCrypt.Encrypt(feecollectionId.ToString());
                    if (result == true)
                    {
                        string Message = ResourceCache.Localize("Sucessfully_Fees_Paid_You_Are_Waiting_to_Mail_For_further_process");
                        return Json(new { Message = Message, collectionid = feecollectionId, encrptycollectionid = encrptycollectionid }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string Message = ResourceCache.Localize("Records_Not_Found");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        //[HttpPost]
        //public JsonResult PayFees(VM_PayFees s)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            Admission1Services As1 = new Admission1Services();
        //            PreAdminServices ps = new PreAdminServices();
        //            PreAdmissionStudentRegisterServices Pstudent = new PreAdmissionStudentRegisterServices();
        //            var count = s.NoOfFees;
        //            string[] FeeCategoryId = new string[count];
        //            string[] PaymentTypeId = new string[count];
        //            string[] Amount = new string[count];
        //            string[] ServiceTax = new string[count];
        //            string[] Total = new string[count];
        //            var Total_Amount = s.TotalAmount;
        //            string statusflag = "";
        //            var feecollectionId = As1.AddFeeCollection(DateTimeByZone.getCurrentDateTime(), s.AcademicYearId, s.ClassId, s.TotalAmount, s.ApplicationId);
        //            int FeeCollectid = Convert.ToInt16(feecollectionId);
        //            if (count != 0)//NoOfSibling Count
        //            {
        //                for (int i = 0; i < count; i++)
        //                {
        //                    FeeCategoryId[i] = String.Join(" ", s.feecategoryid[i]);
        //                    PaymentTypeId[i] = String.Join(" ", s.paymenttypeid[i]);
        //                    Amount[i] = String.Join(" ", s.amount[i]);
        //                    ServiceTax[i] = String.Join(" ", s.servicetax[i]);
        //                    Total[i] = String.Join(" ", s.total[i]);
        //                    int feeCatId = Convert.ToInt16(FeeCategoryId[i]);
        //                    int PaymentId = Convert.ToInt16(PaymentTypeId[i]);
        //                    decimal amt = Convert.ToDecimal(Amount[i]);
        //                    decimal sertax = Convert.ToDecimal(ServiceTax[i]);
        //                    decimal tot = Convert.ToDecimal(Total[i]);
        //                    As1.AddFeeCollectionCategoryFees(FeeCollectid, feeCatId, PaymentId, amt, sertax, tot, s.AcademicYearId, s.ClassId, Convert.ToInt16(s.ApplicationId));
        //                }
        //                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Collect_PreAdmission_Fees"));
        //                statusflag = "FeesPaid";
        //            }
        //            bool result = Pstudent.UpdateStudentRegisterStatusFeesCollection(s.ApplicationId, statusflag);
        //            string encrptycollectionid = QSCrypt.Encrypt(feecollectionId.ToString());
        //            if (result == true)
        //            {
        //                string Message = ResourceCache.Localize("Sucessfully_Fees_Paid_You_Are_Waiting_to_Mail_For_further_process");
        //                return Json(new { Message = Message, collectionid = feecollectionId, encrptycollectionid = encrptycollectionid }, JsonRequestBehavior.AllowGet);
        //            }
        //            else
        //            {
        //                string Message = ResourceCache.Localize("Records_Not_Found");
        //                return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        else
        //        {
        //            string totalError = "";
        //            string[] t = new string[ModelState.Values.Count];
        //            int i = 0;
        //            foreach (var obj in ModelState.Values)
        //            {
        //                foreach (var error in obj.Errors)
        //                {
        //                    if (!string.IsNullOrEmpty(error.ErrorMessage))
        //                    {
        //                        totalError = totalError + error.ErrorMessage + Environment.NewLine;
        //                        t[i] = error.ErrorMessage;
        //                        i++;
        //                    }
        //                }
        //            }
        //            return Json(new { Success = 0, ex = t });
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        String ErrorMessage = e.Message;
        //        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //4 step process
        [RegForm_CheckSessionOut]
        public ActionResult AdmissionForm4(string Id, string Sid)
        {
            try
            {
                if (Convert.ToString(TempData["detailsallsaved"]) == "Yes")
                {
                    return RedirectToAction("HomePage", "OnlineRegister", new { id = Id });
                }
                else
                {
                    TempData["onlineregid"] = Convert.ToInt16(QSCrypt.Decrypt(Id));
                    Admission1Services As1 = new Admission1Services();
                    PreAdmissionOnlineRegisterServices PAORS = new PreAdmissionOnlineRegisterServices();
                    PreAdmissionStudentRegisterServices PASRS = new PreAdmissionStudentRegisterServices();
                    PreAdmissionTransactionServices Admintrans = new PreAdmissionTransactionServices();
                    EmployeeDesinationServices EmployeeDesignation = new EmployeeDesinationServices();
                    PrimaryUserRegisterServices pu = new PrimaryUserRegisterServices();
                    SchoolSettingsServices ss = new SchoolSettingsServices();
                    TransportDestinationServices TrDestnServ = new TransportDestinationServices();
                    int OnlineRegId = Convert.ToInt16(QSCrypt.Decrypt(Id));
                    int StudentRegId = 0;
                    if (Sid == null)
                    {
                        StudentRegId = PAORS.GetStudentId(OnlineRegId);
                    }
                    else
                    {
                        int studentadmissionid = Convert.ToInt32(QSCrypt.Decrypt(Sid));
                        StudentRegId = PAORS.Get_Addmoredetails_by_StudentId(OnlineRegId, studentadmissionid);
                    }
                    //int StudentRegId = PAORS.GetStudentId(OnlineRegId);
                    ViewBag.Regid = OnlineRegId;
                    var user = PAORS.findUser(OnlineRegId);
                    Session["OnlineUserName"] = user.Username;
                  //  ViewBag.PrimaryUserEmail = PAORS.getRow(OnlineRegId).Username;    //check its use....
                    var ans = PASRS.findParticularStudentDetails(OnlineRegId, StudentRegId);
                    ViewBag.Noofsudent = Admintrans.GetNoOfStudent(PAORS.getRow(OnlineRegId).Username);
                    string MatchEmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"+ @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
                    if (user.Username != null)
                    {
                        if (Regex.IsMatch(user.Username, MatchEmailPattern))
                        {
                            ViewBag.PrimaryUserEmail = user.Username;
                        }                      
                        else
                        {
                            ViewBag.PrimaryuserContact = user.Username.Trim();
                        }
                    }
                    ViewBag.allDestination = TrDestnServ.DestinationList();
                    ViewBag.EmployeeDesignation = EmployeeDesignation.EmployeeDesignation();
                    var result = PASRS.GetPrevivousApplication(OnlineRegId);
                    var exist = pu.FindPrimaryUser(PAORS.getRow(OnlineRegId).Username);
                    var Settings = ss.getSchoolSettings();
                    ViewBag.Email = Settings.FirstOrDefault().EmailRequired;
                    ViewBag.Sms = Settings.FirstOrDefault().SmsRequired;
                    ViewBag.App = Settings.FirstOrDefault().AppRequired;
                    ViewBag.Transport = Settings.FirstOrDefault().TransportRequired;
                    ViewBag.Hostel = Settings.FirstOrDefault().HostelRequired;
                    if (result.Count > 0)
                    {
                        if (result.FirstOrDefault().PreApplicationStatus != "InComplete" && result.FirstOrDefault().PreApplicationStatus != "")
                        {
                            ViewBag.oldstatus = "yes";
                        }
                        else
                        {
                            ViewBag.oldstatus = "no";
                        }
                    }
                    else
                    {
                        ViewBag.oldstatus = "no";
                        if (exist > 0)
                        {
                            ViewBag.oldstatus = "yes";
                        }
                    }
                    if (ans.Count != 0)
                    {
                        ViewBag.Distance = ans.FirstOrDefault().Distance;
                        var PreviousClass = ans.FirstOrDefault().AdmissionClass;
                        ViewBag.Previousclass = null;
                        if (PreviousClass != null)
                        {
                            int classid = Convert.ToInt32(PreviousClass);
                            ViewBag.Previousclass = getpreviousclass(classid);
                        }
                        return View(ans);
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        [RegForm_CheckSessionOut]
        public ActionResult AdmissionForm4(VM_Pre_AdmissionValidation a)
        {
            try
            {

                onlineregid = Convert.ToInt16(a.OnlineRegid);
                ViewBag.Regid = onlineregid;
                Admission1Services As1 = new Admission1Services();
                PreAdmissionPrimaryUserRegisterServices Pprimaryuser = new PreAdmissionPrimaryUserRegisterServices();
                PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                PreAdmissionOnlineRegisterServices PonlineRegister = new PreAdmissionOnlineRegisterServices();
                PreAdmissionTransactionServices Admintrans = new PreAdmissionTransactionServices();
                SchoolSettingServices schoolsetting = new SchoolSettingServices();
                SchoolSettingsServices ss1 = new SchoolSettingsServices();
                PrimaryUserRegisterServices pu = new PrimaryUserRegisterServices();
                DateTime currentdate = Convert.ToDateTime(DateTimeByZone.getCurrentDate());
                var stuadmissionid = PstudentRegister.getStudentid(onlineregid);
                //var stuid = stuadmissionid.StudentAdmissionId;
                var stuid = a.Stuadmissionid;
                var ans = PstudentRegister.findParticularStudentDetails(onlineregid, Convert.ToInt16(stuid));
                var result = PstudentRegister.GetPrevivousApplication(onlineregid);
                var user = PonlineRegister.getRow(onlineregid);
                var exist = pu.FindPrimaryUser(PonlineRegister.getRow(onlineregid).Username);
                string MatchEmailPattern =
      @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
+ @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
                if (user.Username != null)
                {
                    if (Regex.IsMatch(user.Username, MatchEmailPattern))
                    {
                        ViewBag.PrimaryUserEmail = user.Username;
                    }
                    else
                    {
                        ViewBag.PrimaryuserContact = user.Username.Trim();
                    }
                }
                var Settings = ss1.getSchoolSettings();
                ViewBag.Email = Settings.FirstOrDefault().EmailRequired;
                ViewBag.Sms = Settings.FirstOrDefault().SmsRequired;
                ViewBag.App = Settings.FirstOrDefault().AppRequired;
                if (result.Count > 0)
                {
                    if (result.FirstOrDefault().PreApplicationStatus != "InComplete" && result.FirstOrDefault().PreApplicationStatus != "")
                    {
                        ViewBag.oldstatus = "yes";
                    }
                    else
                    {
                        ViewBag.oldstatus = "no";
                    }
                }
                else
                {
                    ViewBag.oldstatus = "no";
                    if (exist > 0)
                    {
                        ViewBag.oldstatus = "yes";
                    }
                }
                ViewBag.Noofsudent = Admintrans.GetNoOfStudent(PonlineRegister.getRow(onlineregid).Username);
                string PrimaryEmail = "";
                if (ans.Count != 0)
                {
                    if (ModelState.IsValid)
                    {
                        //ViewBag.PrimaryUserEmail = ans.FirstOrDefault().primaryUserEmail;
                        int count = 0;
                        int applyclass = Convert.ToInt16(a.AdmissionClass);
                        if (a.Edit != "Edit")
                        {
                            if (a.StudentPhoto != null && a.CommunityCertificate != null && a.BirthCertificate != null)
                            {
                                count++;
                            }
                            else
                            {
                                ViewBag.ErrorMessage = ResourceCache.Localize("Please_load_Student_Photo_,Community_Certificate_and_Birth_Certificate");
                                return View(ans);
                            }
                            if (applyclass == 1)
                            {
                                count++;
                            }
                            else if (applyclass != 1)
                            {
                                if (a.PreSchool != null && a.PreBoardOfSchool != null && a.PreClass != null)//&& a.PreMarks != null && a.PreFromDate != null && a.PreToDate != null
                                {
                                    count++;
                                    if (a.TransferCertificate1 != null)
                                    {
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_load_Transfer_Certificate");
                                        return View(ans);
                                    }
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_all_previous_school_information ");
                                    return View(ans);
                                }
                            }
                            if (ViewBag.PrimaryUserEmail != "")
                            {
                                if (a.PrimaryUser == "Father")
                                {
                                    if (a.FatherFirstName != null && a.FatherLastName != null && a.FatherDOB != null && a.FatherEmail != null) // a.FatherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.FatherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Father_All_Information");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Mother")
                                {
                                    if (a.MotherFirstname != null && a.MotherLastName != null && a.MotherDOB != null && a.MotherEmail != null) // a.MotherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.MotherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Mother_All_Information");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Guardian")
                                {
                                    if (a.GuardianFirstname != null && a.GuardianLastname != null && a.GuardianEmail != null && a.GuardianDOB != null)
                                    {
                                        PrimaryEmail = a.GuardianEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Guardian_All__Information");
                                        return View(ans);
                                    }
                                }
                            }
                            else if (ViewBag.PrimaryuserContact != "")
                            {
                                if (a.PrimaryUser == "Father")
                                {
                                    if (a.FatherFirstName != null && a.FatherLastName != null && a.FatherDOB != null && a.FatherMobileNo != null) // a.FatherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.FatherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Father_All_Information");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Mother")
                                {
                                    if (a.MotherFirstname != null && a.MotherLastName != null && a.MotherDOB != null && a.MotherMobileNo != null) // a.MotherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.MotherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Mother_All_Information");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Guardian")
                                {
                                    if (a.GuardianFirstname != null && a.GuardianLastname != null && a.GuardianDOB != null && a.GuardianMobileNo != null)
                                    {
                                        PrimaryEmail = a.GuardianEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Guardian_All__Information");
                                        return View(ans);
                                    }
                                }
                            }

                        }
                        else if (a.Edit == "Edit")
                        {
                            if (a.StudentPhoto1 != null && a.CommunityCertificate1 != null && a.BirthCertificate1 != null)
                            {
                                count++;
                            }
                            else
                            {
                                ViewBag.ErrorMessage = ResourceCache.Localize("Please_load_Student_Photo_,Community_Certificate_and_Birth_Certificate");
                                return View(ans);
                            }
                            if (applyclass == 1)
                            {
                                count++;
                            }
                            else if (applyclass != 1)
                            {
                                if (a.PreSchool != null && a.PreBoardOfSchool != null && a.PreClass != null)//&& a.PreMarks != null && a.PreFromDate != null && a.PreToDate != null
                                {
                                    count++;
                                    if (a.TransferCertificate1 != null)
                                    {
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_load_Transfer_Certificate");
                                        return View(ans);
                                    }
                                }
                                else
                                {
                                    ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_all_previous_school_information ");
                                    return View(ans);
                                }
                            }
                            if (ViewBag.PrimaryUserEmail != "" && ViewBag.PrimaryUserEmail != null)
                            {
                                if (a.PrimaryUser == "Father")
                                {
                                    if (a.FatherFirstName != null && a.FatherLastName != null && a.txt_Fatherdob != null && a.FatherEmail != null) // a.FatherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.FatherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Father_All_Information ");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Mother")
                                {
                                    if (a.MotherFirstname != null && a.MotherLastName != null && a.txt_Motherdob != null && a.MotherEmail != null) // a.MotherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.MotherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Mother_All_Information");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Guardian")
                                {
                                    if (a.GuardianFirstname != null && a.GuardianLastname != null && a.GuardianEmail != null && a.txt_Guardiandob != null)
                                    {
                                        PrimaryEmail = a.GuardianEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Guardian_All__Information");
                                        return View(ans);
                                    }
                                }
                            }
                            else if (ViewBag.PrimaryuserContact != "" && ViewBag.PrimaryuserContact != null)
                            {
                                if (a.PrimaryUser == "Father")
                                {
                                    if (a.FatherFirstName != null && a.FatherLastName != null && a.txt_Fatherdob != null && a.FatherMobileNo != null) // a.FatherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.FatherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Father_All_Information ");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Mother")
                                {
                                    if (a.MotherFirstname != null && a.MotherLastName != null && a.txt_Motherdob != null && a.MotherMobileNo != null) // a.MotherMobileNo!=null &&
                                    {
                                        PrimaryEmail = a.MotherEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Mother_All_Information");
                                        return View(ans);
                                    }
                                }
                                else if (a.PrimaryUser == "Guardian")
                                {
                                    if (a.GuardianFirstname != null && a.GuardianLastname != null && a.txt_Guardiandob != null && a.GuardianMobileNo != null)
                                    {
                                        PrimaryEmail = a.GuardianEmail;
                                        count++;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ResourceCache.Localize("Please_fill_Guardian_All__Information");
                                        return View(ans);
                                    }
                                }
                            }
                        }
                        if (count == 3)
                        {
                            var ss = PstudentRegister.GetStudentApplicationdetails(Convert.ToInt16(stuid));
                            var Primary = Pprimaryuser.FindPrimary(onlineregid);
                            ViewBag.Email = Settings.FirstOrDefault().EmailRequired;
                            ViewBag.Sms = Settings.FirstOrDefault().SmsRequired;
                            ViewBag.App = Settings.FirstOrDefault().AppRequired;
                            var school = schoolsetting.getSchoolSettings();
                            var SchoolName = school.SchoolName;
                            var SchoolLogo = school.SchoolLogo;
                            var Primaryuser = Primary.PrimaryUserName.ToString();
                            var Studentname = a.StuFirstName + " " + a.StuLastname;
                            var AppNo = stuid.ToString();
                            var year = ss.Academicyear.ToString();
                            var classs = ss.classs.ToString();
                            var PreviousClass = ans.FirstOrDefault().AdmissionClass;
                            ViewBag.Previousclass = null;
                            if (PreviousClass != null)
                            {
                                int classid = Convert.ToInt32(PreviousClass);
                                ViewBag.Previousclass = getpreviousclass(classid);
                            }
                            if ((ViewBag.Email == true && ViewBag.Sms == true && ViewBag.App == true) || (ViewBag.Email == true && ViewBag.Sms == true) || (ViewBag.Sms == true && ViewBag.App == true))
                            {
                                if (Primary.EmailRequired == true)
                                {
                                    var Femail = PrimaryEmail.ToString();
                                    var Username = Primary.PrimaryUserEmail.ToString();
                                    PreAdmissionEmail(Primaryuser, AppNo, Username, year, classs, Studentname, SchoolName, SchoolLogo);
                                }
                                if (Primary.SmsRequired == true)
                                {
                                    var contact = Primary.PrimaryUserContactNo.Value.ToString().Trim();
                                    PreAdmissionSMS(contact, Primaryuser, AppNo, Studentname, year, classs);
                                }
                            }
                            else if (ViewBag.Email == true || ViewBag.App == true)
                            {
                                // For sending Email to Father
                                if (PrimaryEmail != "")
                                {
                                    var Femail = PrimaryEmail.ToString();
                                    var Username = Primary.PrimaryUserEmail.ToString();
                                    PreAdmissionEmail(Primaryuser, AppNo, Username, year, classs, Studentname, SchoolName, SchoolLogo);

                                }
                            }
                            else if (ViewBag.Sms == true)
                            {
                                var contact = Primary.PrimaryUserContactNo.Value.ToString().Trim();
                                PreAdmissionSMS(contact, Primaryuser, AppNo, Studentname, year, classs);
                            }
                            string statusflag = "Enrolled";
                            TempData["detailsallsaved"] = "Yes";
                            bool result1 = PstudentRegister.UpdateStudentRegisterStatus(Convert.ToInt32(stuid), statusflag);
                            return RedirectToAction("StepReport", new { Id = QSCrypt.Encrypt(Primary.PrimaryUserAdmissionId.ToString()), Sid = QSCrypt.Encrypt(stuid.ToString()) });
                        }
                        return View(ans);
                    }
                    else
                    {
                        ViewBag.modelState = ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                        ViewBag.PrimaryUserEmail = ans.FirstOrDefault().primaryUserEmail;

                        return View(ans);
                    }
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        int count = 0;
                        int applyclass = Convert.ToInt16(a.AdmissionClass);
                        if (applyclass == 1)
                        {
                            count++;
                        }
                        else if (applyclass != 1)
                        {
                            if (a.PreSchool != "" || a.PreBoardOfSchool != null || a.PreClass != null || a.PreMarks != "" || a.PreFromDate != null || a.PreToDate != null)
                            {
                                count++;
                            }
                            else
                            {
                                ViewBag.modelState = ResourceCache.Localize("Please_fill_All_Previous_School_Information_And_load_transfercertificate");
                                return View(ans);
                            }
                        }
                        return View();
                    }
                    else
                    {
                        ViewBag.modelState = ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                        return View();
                    }
                }
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        [HttpPost]
        public JsonResult SaveContinue1(VM_StepStudent r)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionOnlineRegisterServices PonlineRegister = new PreAdmissionOnlineRegisterServices();
                    PreAdmissionPrimaryUserRegisterServices Pprimaryuser = new PreAdmissionPrimaryUserRegisterServices();
                    PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                    PreAdmissionTransactionServices PAdmissionTransaction = new PreAdmissionTransactionServices();
                    PreAdmissionFatherRegisterServices PFatherRegister = new PreAdmissionFatherRegisterServices();
                    PreAdmissionMotherRegisterServices Pmotherregister = new PreAdmissionMotherRegisterServices();
                    PreAdmissionGuardianRegisterServices PguardianRegister = new PreAdmissionGuardianRegisterServices();
                    PreAdmissionParentsSblingServices Pparentssibling = new PreAdmissionParentsSblingServices();
                    onlineregid = Convert.ToInt16(r.OnlineRegid);
                    int studentid = Convert.ToInt16(r.Stuadmissionid);
                    int AdmissionClass = Convert.ToInt16(r.AdmissionClass);
                    int PrimaryAdmissionId = 0; int StudentAdmissionId = 0; int FatherAdmissionId = 0; int MotherAdmissionId = 0; int GuardianAdmissionId = 0;
                    //PrimaryUser Details
                    var CheckPrimaryUser = Pprimaryuser.FindPrimary(onlineregid);
                    if (CheckPrimaryUser == null)
                    {
                        PrimaryAdmissionId = Pprimaryuser.StepAddPrimaryUser(onlineregid, r.PrimaryUser, r.primaryUserEmail, r.TotalIncome, r.EmployeeDesignationId, r.EmployeeId, r.WorkSameSchool, r.primaryUserContact, r.EmailRequired, r.SmsRequired);
                    }
                    else
                    {
                        PrimaryAdmissionId = Pprimaryuser.StepUpdatePrimaryUser(onlineregid, r.PrimaryUser, r.primaryUserEmail, r.TotalIncome, r.EmployeeDesignationId, r.EmployeeId, r.WorkSameSchool, r.primaryUserContact, r.EmailRequired, r.SmsRequired);
                    }
                    //student details
                    if (studentid == 0)
                    {
                        bool exist = PstudentRegister.GetStudentDetailsExist(PrimaryAdmissionId, r.StuFirstName, r.StuLastname, r.DOB);
                        if (exist == false)
                        {
                            StudentAdmissionId = PstudentRegister.AddStudentdetailsForm4(onlineregid, r.StuFirstName, r.StuMiddlename, r.StuLastname, r.Gender, r.DOB, r.PlaceOfBirth, r.Community, r.Religion, r.Nationality, r.AdmissionClass,
                                  r.AcademicyearId, r.GuardianRequried);
                            int name = StudentAdmissionId;
                            var path = Server.MapPath("~/Documents/" + name);
                            System.IO.Directory.CreateDirectory(path);
                            //Directory.CreateDirectory(path);
                            var checkprimaryuserintransaction = PAdmissionTransaction.FindPrimaryuser(PrimaryAdmissionId);
                            if (checkprimaryuserintransaction != null)
                            {
                                if (checkprimaryuserintransaction.PrimaryUserAdmissionId == PrimaryAdmissionId && checkprimaryuserintransaction.StudentAdmissionId == null)
                                {
                                    PAdmissionTransaction.UpdateStudentId(PrimaryAdmissionId, StudentAdmissionId);
                                }
                                else if (StudentAdmissionId != 0)
                                {
                                    PAdmissionTransaction.AddPrimaryUserandStudentId(PrimaryAdmissionId, StudentAdmissionId);
                                }
                            }
                            else
                            {
                                PAdmissionTransaction.AddPrimaryUserandStudentId(PrimaryAdmissionId, StudentAdmissionId);
                            }
                        }
                        else
                        {
                            String ErrorMessage = ResourceCache.Localize("Student_details_already_exist_Dont_repeat_same_student_details");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        bool exist = PstudentRegister.GetStudentDetailsExist(PrimaryAdmissionId, studentid, r.StuFirstName, r.StuLastname, r.DOB);
                        if (exist == false)
                        {
                            StudentAdmissionId = PstudentRegister.UpdateStudentdetailsForm4(onlineregid, studentid, r.StuFirstName, r.StuMiddlename, r.StuLastname, r.Gender, r.DOB, r.PlaceOfBirth, r.Community, r.Religion, r.Nationality, r.AdmissionClass, r.AcademicyearId, r.GuardianRequried);
                        }
                        else
                        {
                            String ErrorMessage = ResourceCache.Localize("Student_details_already_exist_Dont_repeat_same_student_details");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }

                    }
                    if (AdmissionClass != 1 && AdmissionClass != 0)
                    {
                        PstudentRegister.UpdatePreviousSchool(StudentAdmissionId, r.PreSchool, r.PreClass, r.PreBoardOfSchool, r.PreMarks, r.PreFromDate1, r.PreToDate);
                    }
                    var NoOfStudents = PAdmissionTransaction.GetRemainingStudent(PrimaryAdmissionId, StudentAdmissionId);
                    if (NoOfStudents.Count == 0)
                    {
                        var checkuser = PAdmissionTransaction.GetPrimaryUser(PrimaryAdmissionId, StudentAdmissionId);
                        if (r.PrimaryUser == "Father")
                        {
                            if (checkuser.FatherAdmissionId == null)
                            {
                                if (r.FatherFirstName != null && r.FatherLastName != null && r.FatherDOB != null)//&& r.FatherEmail != null && r.FatherMobileNo != null
                                {
                                    FatherAdmissionId = PFatherRegister.AddFatherdetails(r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                                    PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                    Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                                }
                            }
                            else
                            {
                                PFatherRegister.UpdatefatherDetails(checkuser.FatherAdmissionId, r.FatherQualification, r.FatherOccupation, r.FatherEmail, r.FatherMobileNo);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                            }
                            if (checkuser.MotherAdmissionId == null)
                            {
                                if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                                {
                                    MotherAdmissionId = Pmotherregister.AddMotherdetails(r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                                    PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                }
                            }
                            else
                            {
                                Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                            }
                            if (checkuser.GuardianAdmissionId == null)
                            {
                                if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                                {
                                    GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                    PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                }
                            }
                            else
                            {
                                PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                            }
                        }
                        else if (r.PrimaryUser == "Mother")
                        {
                            if (checkuser.MotherAdmissionId == null)
                            {
                                if (r.MotherFirstname != null && r.MotherLastName != null && r.MotherDOB != null)//&& r.MotherEmail != null && r.MotherMobileNo != null
                                {
                                    MotherAdmissionId = Pmotherregister.AddMotherdetails(r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                                    PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                    Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                                }
                            }
                            else
                            {
                                Pmotherregister.UpdatemotherDetails(checkuser.MotherAdmissionId, r.MotherQualification, r.MotherOccupation, r.MotherEmail, r.MotherMobileNo);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                            }
                            if (checkuser.FatherAdmissionId == null)
                            {
                                if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                                {
                                    FatherAdmissionId = PFatherRegister.AddFatherdetails(r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                                    PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                }
                            }
                            else
                            {
                                PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                            }
                            if (checkuser.GuardianAdmissionId == null)
                            {
                                if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                                {
                                    GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                    PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                }
                            }
                            else
                            {
                                PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                            }
                        }
                        else if (r.PrimaryUser == "Guardian")
                        {
                            if (checkuser.GuardianAdmissionId == null)
                            {
                                if (r.GuardianFirstname != null && r.GuardianLastName != null && r.GuardianDOB != null)//&& r.GuardianEmail != null && r.GuardianMobileNo != null
                                {
                                    GuardianAdmissionId = PguardianRegister.AddGuardiandetails(r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                    PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                    Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                                }
                            }
                            else
                            {
                                PguardianRegister.UpdateGuardianDetails(checkuser.GuardianAdmissionId, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                            }
                            if (checkuser.MotherAdmissionId == null)
                            {
                                if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                                {
                                    MotherAdmissionId = Pmotherregister.AddMotherdetails(r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                                    PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                }
                            }
                            else
                            {
                                Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                            }
                            if (checkuser.FatherAdmissionId == null)
                            {
                                if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                                {
                                    FatherAdmissionId = PFatherRegister.AddFatherdetails(r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                                    PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                }
                            }
                            else
                            {
                                PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                            }
                        }
                    }
                    else
                    {
                        var checkuser = PAdmissionTransaction.GetPrimaryUser(PrimaryAdmissionId, StudentAdmissionId);
                        if (r.PrimaryUser == "Father")
                        {
                            if (checkuser.FatherAdmissionId == null)
                            {
                                if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                                {
                                    FatherAdmissionId = PFatherRegister.CheckFatherExit(PrimaryAdmissionId, StudentAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                                    PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                    Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                                }
                            }
                            else
                            {
                                PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherEmail, r.FatherMobileNo, r.TotalIncome);
                            }
                            if (checkuser.MotherAdmissionId == null)
                            {
                                if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                                {
                                    MotherAdmissionId = Pmotherregister.CheckMotherExit(PrimaryAdmissionId, StudentAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                                    PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                }
                            }
                            else
                            {
                                Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                            }
                            if (checkuser.GuardianAdmissionId == null)
                            {
                                if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                                {
                                    GuardianAdmissionId = PguardianRegister.CheckGuardianExit(PrimaryAdmissionId, StudentAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                    PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                }
                            }
                            else
                            {
                                PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                            }
                        }
                        else if (r.PrimaryUser == "Mother")
                        {
                            if (checkuser.MotherAdmissionId == null)
                            {
                                if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                                {
                                    MotherAdmissionId = Pmotherregister.CheckMotherExit(PrimaryAdmissionId, StudentAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                                    PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                    Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                                }
                            }
                            else
                            {
                                Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherEmail, r.MotherMobileNo, r.TotalIncome);
                            }
                            if (checkuser.FatherAdmissionId == null)
                            {
                                if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                                {
                                    FatherAdmissionId = PFatherRegister.CheckFatherExit(PrimaryAdmissionId, StudentAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                                    PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                }
                            }
                            else
                            {
                                PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail,r.TotalIncome);
                            }
                            if (checkuser.GuardianAdmissionId == null)
                            {
                                if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                                {
                                    GuardianAdmissionId = PguardianRegister.CheckGuardianExit(PrimaryAdmissionId, StudentAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                    PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                }
                            }
                            else
                            {
                                PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                            }
                        }
                        else if (r.PrimaryUser == "Guardian")
                        {
                            if (checkuser.GuardianAdmissionId == null)
                            {
                                if (r.GuardianFirstname != null || r.GuardianLastName != null || r.GuardianDOB != null || r.GuardianQualification != null || r.GuardianOccupation != null || r.GuardianMobileNo != null || r.GuardianEmail != null || r.GuardianIncome != null || r.GuRelationshipToChild != null)
                                {
                                    GuardianAdmissionId = PguardianRegister.CheckGuardianExit(PrimaryAdmissionId, StudentAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                    PAdmissionTransaction.UpdateGuardiantId(PrimaryAdmissionId, StudentAdmissionId, GuardianAdmissionId);
                                    Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                                }
                            }
                            else
                            {
                                PguardianRegister.UpdateGuardianDetails1(checkuser.GuardianAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianDOB, r.GuardianQualification, r.GuardianOccupation, r.GuardianMobileNo, r.GuardianEmail, r.GuardianIncome, r.GuRelationshipToChild, r.GuardianGender);
                                Pprimaryuser.UpdatePrimaryUsername(PrimaryAdmissionId, r.GuardianFirstname, r.GuardianLastName, r.GuardianEmail, r.GuardianMobileNo, r.GuardianIncome);
                            }
                            if (checkuser.FatherAdmissionId == null)
                            {
                                if (r.FatherFirstName != null || r.FatherLastName != null || r.FatherDOB != null || r.FatherQualification != null || r.FatherOccupation != null || r.FatherMobileNo != null || r.FatherEmail != null)
                                {
                                    FatherAdmissionId = PFatherRegister.CheckFatherExit(PrimaryAdmissionId, StudentAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                                    PAdmissionTransaction.UpdateFathertId(PrimaryAdmissionId, StudentAdmissionId, FatherAdmissionId);
                                }
                            }
                            else
                            {
                                PFatherRegister.UpdatefatherDetails1(checkuser.FatherAdmissionId, r.FatherFirstName, r.FatherLastName, r.FatherDOB, r.FatherQualification, r.FatherOccupation, r.FatherMobileNo, r.FatherEmail, r.TotalIncome);
                            }
                            if (checkuser.MotherAdmissionId == null)
                            {
                                if (r.MotherFirstname != null || r.MotherLastName != null || r.MotherDOB != null || r.MotherQualification != null || r.MotherOccupation != null || r.MotherMobileNo != null || r.MotherEmail != null)
                                {
                                    MotherAdmissionId = Pmotherregister.CheckMotherExit(PrimaryAdmissionId, StudentAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail,r.TotalIncome);
                                    PAdmissionTransaction.UpdateMothertId(PrimaryAdmissionId, StudentAdmissionId, MotherAdmissionId);
                                }
                            }
                            else
                            {
                                Pmotherregister.UpdatemotherDetails1(checkuser.MotherAdmissionId, r.MotherFirstname, r.MotherLastName, r.MotherDOB, r.MotherQualification, r.MotherOccupation, r.MotherMobileNo, r.MotherEmail, r.TotalIncome);
                            }
                        }
                    }
                    string Message = ResourceCache.Localize("Sucessfully_student_details_added");
                    return Json(new { Message = Message, StudentAdmissionId = StudentAdmissionId }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult SaveContinue2(VM_StepParents r)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PreAdmissionPrimaryUserRegisterServices Pprimaryuser = new PreAdmissionPrimaryUserRegisterServices();
                    PreAdmissionStudentRegisterServices PstudentRegister = new PreAdmissionStudentRegisterServices();
                    PreAdmissionParentsSblingServices Pparentssibling = new PreAdmissionParentsSblingServices();
                    int stid = Convert.ToInt16(r.Stuadmissionid);
                    onlineregid = Convert.ToInt16(r.OnlineRegid);
                    int count = Convert.ToInt16(r.NoOfSbling);
                    string SiblingStatus = "";
                    int PrimaryAdmissionId = 0; int StudentAdmissionId = 0;
                    if (r.NoOfSbling != 0 && r.NoOfSbling != null)
                    {
                        SiblingStatus = "Yes";
                    }
                    else if (r.NoOfSbling == 0)
                    {
                        SiblingStatus = "No";
                    }
                    var Student = PstudentRegister.findStudent(stid);
                    if (Student != null)
                    {
                        StudentAdmissionId = PstudentRegister.UpdatestudentAddressothersDetailsForm4(r.LocAddress1, r.LocAddress2, r.LocCity, r.LocState, r.LocCountry, r.LocPostelcode, r.NoOfSbling, r.Height, r.Weights,
                            r.IdentificationMark, r.Email, r.Contact, r.PerAddress1, r.PerAddress2, r.PerCity, r.PerState, r.PerCountry, r.PerPostelcode, r.Distance, SiblingStatus, Student.StudentAdmissionId, r.BloodGroup, r.EmergencyContactPersonName,
                                                                              r.EmergencyContactNumber, r.ContactPersonRelationship);
                    }
                    var Primary = Pprimaryuser.FindPrimary(onlineregid);
                    if (Primary != null)
                    {
                        PrimaryAdmissionId = Pprimaryuser.UpdatePrimaryUserStepForm(onlineregid, Primary.PrimaryUser, Primary.PrimaryUserEmail, r.CompanyAddress1, r.CompanyAddress2, r.CompanyCity, r.CompanyState, r.CompanyCountry, r.CompanyPostelcode, r.CompanyContact,
                                                                           r.UserCompanyname);
                    }
                    PstudentRegister.UpdateStudentTransportDetails(StudentAdmissionId, r.TransportRequired, r.TransportDestination, r.TransportPickpoint, r.TransportFeeAmount);
                    PstudentRegister.UpdateStudentHostelDetails(StudentAdmissionId, r.HostelRequired, r.AccommodationFeeCategoryId, r.AccommodationSubFeeCategoryId, r.FoodFeeCategoryId, r.FoodSubFeeCategoryId);
                    //Sibling details
                    if (count != 0)//NoOfSibling Count
                    {
                        Pparentssibling.UpdateSblingdetails(onlineregid, StudentAdmissionId);
                    }

                    string Message = ResourceCache.Localize("Sucessfully_Parents_details_added");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [RegForm_CheckSessionOut]
        public ActionResult FeesPaid(string Id)
        {
            try
            {
                PreAdmissionStudentRegisterServices pstudent = new PreAdmissionStudentRegisterServices();
                int regid = Convert.ToInt16(QSCrypt.Decrypt(Id));
                var result = pstudent.GetStatusFlagViewPageDetails(regid);
                ViewBag.Regid = QSCrypt.Encrypt(result.FirstOrDefault().OnlineregisterId.ToString());
                ViewBag.sid = QSCrypt.Encrypt(result.FirstOrDefault().ApplicationNo.ToString());
                return View(result);
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        [RegForm_CheckSessionOut]
        public ActionResult Reject(string Id)
        {
            try
            {
                PreAdmissionStudentRegisterServices pstudent = new PreAdmissionStudentRegisterServices();
                int regid = Convert.ToInt16(QSCrypt.Decrypt(Id));
                var result = pstudent.GetStatusFlagViewPageDetails(regid);
                ViewBag.Regid = QSCrypt.Encrypt(result.FirstOrDefault().OnlineregisterId.ToString());
                ViewBag.sid = QSCrypt.Encrypt(result.FirstOrDefault().ApplicationNo.ToString());
                return View(result);
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        [RegForm_CheckSessionOut]
        public ActionResult Pending(string Id)
        {
            try
            {
                PreAdmissionStudentRegisterServices pstudent = new PreAdmissionStudentRegisterServices();
                int regid = Convert.ToInt16(QSCrypt.Decrypt(Id));
                var result = pstudent.GetStatusFlagViewPageDetails(regid);
                ViewBag.Regid = QSCrypt.Encrypt(result.FirstOrDefault().OnlineregisterId.ToString());
                ViewBag.sid = QSCrypt.Encrypt(result.FirstOrDefault().ApplicationNo.ToString());
                return View(result);
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        [RegForm_CheckSessionOut]
        public ActionResult Enrolled(string Id)
        {
            try
            {
                PreAdmissionStudentRegisterServices pstudent = new PreAdmissionStudentRegisterServices();
                int regid = Convert.ToInt16(QSCrypt.Decrypt(Id));
                var result = pstudent.GetStatusFlagViewPageDetails(regid);
                ViewBag.Regid = QSCrypt.Encrypt(result.FirstOrDefault().OnlineregisterId.ToString());
                ViewBag.sid = QSCrypt.Encrypt(result.FirstOrDefault().ApplicationNo.ToString());
                return View(result);
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        [RegForm_CheckSessionOut]
        public ActionResult Accept(string Id)
        {
            try
            {
                PreAdmissionStudentRegisterServices pstudent = new PreAdmissionStudentRegisterServices();
                int regid = Convert.ToInt16(QSCrypt.Decrypt(Id));
                var result = pstudent.GetStatusFlagViewPageDetails(regid);
                ViewBag.Regid = QSCrypt.Encrypt(result.FirstOrDefault().OnlineregisterId.ToString());
                ViewBag.sid = QSCrypt.Encrypt(result.FirstOrDefault().ApplicationNo.ToString());
                return View(result);
            }
            catch (Exception e)
            {
                ViewBag.admission = "PreAdmission";
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }
        public ActionResult CaptchaImage(string prefix, bool noisy = true)
        {
            var chars = "0123456789";
            var rand = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 5)
                          .Select(s => s[rand.Next(s.Length)])
                          .ToArray());
            var captcha = result;
            Session["Captcha" + prefix] = result;
            FileContentResult img = null;
            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(130, 30))
            using (var gfx = Graphics.FromImage((Image)bmp))
            {
                gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));
                if (noisy)
                {
                    int i, r, x, y;
                    var pen = new Pen(Color.Yellow);
                    for (i = 1; i < 10; i++)
                    {
                        pen.Color = Color.FromArgb(
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)));
                        r = rand.Next(0, (130 / 3));
                        x = rand.Next(0, 130);
                        y = rand.Next(0, 30);
                        int x1 = x - r;
                        int y1 = y - r;
                        gfx.DrawEllipse(pen, x1, y1, r, r);
                    }
                }
                gfx.DrawString(captcha, new Font("Tahoma", 15), Brushes.Gray, 2, 3);
                bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
                img = this.File(mem.GetBuffer(), "image/Jpeg");
            }
            return img;
        }
        [HttpPost]
        public ActionResult CaptchaImage1(string prefix, bool noisy = true)
        {
            var chars = "0123456789";
            var rand = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 5)
                          .Select(s => s[rand.Next(s.Length)])
                          .ToArray());
            var captcha = result;
            Session["Captcha" + prefix] = result;
            FileContentResult img = null;
            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(130, 30))
            using (var gfx = Graphics.FromImage((Image)bmp))
            {
                gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));
                if (noisy)
                {
                    int i, r, x, y;
                    var pen = new Pen(Color.Yellow);
                    for (i = 1; i < 10; i++)
                    {
                        pen.Color = Color.FromArgb(
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)));
                        r = rand.Next(0, (130 / 3));
                        x = rand.Next(0, 130);
                        y = rand.Next(0, 30);
                        int x1 = x - r;
                        int y1 = y - r;
                        gfx.DrawEllipse(pen, x1, y1, r, r);
                    }
                }
                gfx.DrawString(captcha, new Font("Tahoma", 15), Brushes.Gray, 2, 3);
                bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
                img = this.File(mem.GetBuffer(), "image/Jpeg");
            }
            return Json(img, JsonRequestBehavior.AllowGet);
        }
        private bool GetCaptchaResponse(out string message)
        {
            bool flag = false;
            string[] result;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://www.google.com/recaptcha/api/verify");
            request.ProtocolVersion = HttpVersion.Version10;
            request.Timeout = 0x7530;
            request.Method = "POST";
            request.UserAgent = "reCAPTCHA/ASP.NET";
            request.ContentType = "application/x-www-form-urlencoded";
            string formData = string.Format(
                "privatekey={0}&remoteip={1}&challenge={2}&response={3}",
                new object[]{
                        HttpUtility.UrlEncode("6LdcNwQTAAAAAKK7micYU-fbPMYGUsF5KAMvOA7f"),
                        HttpUtility.UrlEncode(Dns.GetHostEntry(Dns.GetHostName()).AddressList[1].ToString()),
                        HttpUtility.UrlEncode(Request.Form["recaptcha_challenge_field"]),
                        HttpUtility.UrlEncode(Request.Form["recaptcha_response_field"])
                    });
            byte[] formbytes = Encoding.ASCII.GetBytes(formData);
            using (System.IO.Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(formbytes, 0, formbytes.Length);
            }
            try
            {
                using (WebResponse httpResponse = request.GetResponse())
                {
                    using (System.IO.TextReader readStream = new System.IO.StreamReader(httpResponse.GetResponseStream(), Encoding.UTF8))
                    {
                        result = readStream.ReadToEnd().Split(new string[] { "\n", @"\n" }, StringSplitOptions.RemoveEmptyEntries);
                        message = result[1];
                        flag = Convert.ToBoolean(result[0]);
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
            return flag;
        }
        public ActionResult Photo()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message.ToString();
                return View("Error");
            }
        }

        public JsonResult GetVacuncyClass(int passAcyearId)
        {
            try
            {
                AsignClassVacancyServices vacuncy = new AsignClassVacancyServices();
                var vacuncyClass = vacuncy.ShowVacancyClassList(passAcyearId);
                return Json(vacuncyClass, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();
            return RedirectToAction("Login", "OnlineRegister");
        }
    }
}