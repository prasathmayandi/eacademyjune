﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eAcademy.Controllers
{

    [CheckSessionOutAttribute]
    public class AttendanceManagementController : Controller
    {
        // GET: /AttendanceManagement/

        AcademicyearServices AcYear = new AcademicyearServices();
        //AcademicyearServices acservice = new AcademicyearServices();
        TermServices term = new TermServices();
        AssignClassToStudentServices stu = new AssignClassToStudentServices();
        TechemployeesServices techEmp = new TechemployeesServices();
        OtherEmployeeServices otherEmp = new OtherEmployeeServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        public int facultyId;

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            facultyId = Convert.ToInt32(Session["empRegId"]);
            ClassServices clas = new ClassServices();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.allClasses = clas.ShowClasses();
            ViewBag.allFaculties = techEmp.ShowTechFaculties();
            ViewBag.allEmployees = otherEmp.ShowEmployees();
        }

        public JsonResult GetSelectedClassSection(int passClassId)
        {
            try
            {
                SectionServices sec = new SectionServices();
                var List = sec.getSection(passClassId);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSelectedYearTerms(int passYearId)
        {
            try
            {
                var List = term.selectedYearTerms(passYearId);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSectionStudents(int passYearId, int passClassId, int passSectionId)
        {
            try
            {
                var StudentList = stu.getStudents(passYearId, passClassId, passSectionId);
                return Json(StudentList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getDate(int acid)
        {
            try
            {
                var ans = AcYear.date(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                EventServices holidays = new EventServices();
                var HolidaysList = holidays.getHolidaysList(acid);
                return Json(new { sd = sdate, ed = edate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }


        tblAttendanceStatusServices attStatus = new tblAttendanceStatusServices();

        public JsonResult GetAttendanceStatusList()
        {
            try
            {
                var attStatusList = attStatus.AttendanceStatusList();
                return Json(attStatusList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        // Faculty Attendance 
        public ActionResult FacultyAttendance()
        {
            return View();
        }

        public JsonResult GetFacultyListToMarkAttendance(int passYearId, DateTime passDate)
        {
            try
            {
                FacultyDailyAttendanceServices faculytAttendance = new FacultyDailyAttendanceServices();
                var checkDataExist = faculytAttendance.AttendanceExist(passYearId, passDate);
                if (checkDataExist != null)
                {
                    var ExistList = faculytAttendance.getAttendanceExistList(passYearId, passDate);
                    string status = "P";
                    for (int i = 0; i < ExistList.Count; i++)
                    {
                        string AttStatus = ExistList[i].status;
                        if (AttStatus != "P")
                        {
                            status = "not_P";
                        }
                    }
                    return Json(new { msg = "old", ExistList = ExistList, status }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var list = techEmp.FacultyListToMarkAttendance(passDate); 
                    int Count = list.ToList().Count;
                    if (Count == 0)
                    {
                        string StudentCount = "empty";
                        return Json(new { StudentCount }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { msg = "new", list = list }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddFacultyAttendance(AM_AddFacultyAttendance am)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int YearId = Convert.ToInt32(am.Att_allAcademicYears);
                    var Cyear = AcYear.getCurrentAcademicYearId(DateTimeByZone.getCurrentDate());
                    if (Cyear != null)
                    {
                        Session["CurrentAcYearId"] = Cyear.AcademicYearId;
                    }
                    else
                    {
                        Session["CurrentAcYearId"] = "";
                    }
                    int CurrentYearId;
                    if (Session["CurrentAcYearId"] != "")
                    {
                        CurrentYearId = Convert.ToInt16(Session["CurrentAcYearId"]);
                    }
                    else
                    {
                        CurrentYearId = 0;
                    }
                    //int currentAcYearId = Convert.ToInt32(Session["CurrentAcYearId"]);
                    if (YearId == CurrentYearId)
                    {
                        var TotalCount = am.TotalCount;
                        int year = am.Att_allAcademicYears;
                        DateTime date = am.txt_AttendanceDate;
                        int size = TotalCount + 1;
                        string[] Id = new string[size];
                        string[] atn_comment = new string[size];
                        string[] atn_status = new string[size];

                        FacultyDailyAttendanceServices faculytAttendance = new FacultyDailyAttendanceServices();
                        var checkDataExist = faculytAttendance.AttendanceExist(year, date);
                        if (checkDataExist != null)
                        {
                            for (int i = 0; i <= TotalCount; i++)
                            {
                                Id[i] = QSCrypt.Decrypt(am.Id[i]);
                                int RegId = Convert.ToInt32(Id[i]);
                                atn_status[i] = am.atn_status[i];
                                atn_comment[i] = am.atn_comment[i];
                                DateTime updatedDate = DateTimeByZone.getCurrentDate();
                                faculytAttendance.updateFacultyDailyAttendance(RegId, year, date, atn_status[i], atn_comment[i], updatedDate);
                            }
                            string Message = ResourceCache.Localize("AttendanceUpdatedSuccessfully");
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdateFacultyAttendance"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            for (int i = 0; i <= TotalCount; i++)
                            {
                                Id[i] = QSCrypt.Decrypt(am.Id[i]);
                                int FacultyId = Convert.ToInt32(Id[i]);
                                atn_status[i] = am.atn_status[i];
                                atn_comment[i] = am.atn_comment[i];
                                DateTime markedDate = DateTimeByZone.getCurrentDate();

                                faculytAttendance.addFacultyDailyAttendance(year, FacultyId, date, atn_status[i], atn_comment[i], markedDate);
                            }
                            string Message = ResourceCache.Localize("Attendance_saved _Successfully");
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddFacultyAttendance"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string InfoMessage = ResourceCache.Localize("Records_can_save_for_current_academic_year_only");
                        return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                    }   
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }


        // Employee Attendance
        public ActionResult EmployeeAttendance()
        {
            return View();
        }
        public JsonResult GetEmployeeListToMarkAttendance(int passYearId, DateTime passDate)
        {
            try
            {
                EmployeeDailyAttendanceServices empAttendance = new EmployeeDailyAttendanceServices();
                var checkDataExist = empAttendance.AttendanceExist(passYearId, passDate);
                if (checkDataExist != null)
                {
                    var ExistList = empAttendance.getAttendanceExistList(passYearId, passDate);
                    string status = "P";
                    for (int i = 0; i < ExistList.Count; i++)
                    {
                        string AttStatus = ExistList[i].status;
                        if (AttStatus != "P")
                        {
                            status = "not_P";
                        }
                    }
                    return Json(new { msg = "old", ExistList = ExistList, status }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var list = otherEmp.EmployeeListToMarkAttendance(passDate);
                    int Count = list.ToList().Count;
                    if (Count == 0)
                    {
                        string StudentCount = "empty";
                        return Json(new { StudentCount }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { msg = "new", list = list }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddEmployeeAttendance(AM_AddFacultyAttendance am)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int YearId = Convert.ToInt32(am.Att_allAcademicYears);
                    int currentAcYearId = Convert.ToInt32(Session["CurrentAcYearId"]);
                    if (YearId == currentAcYearId)
                    {
                        var TotalCount = am.TotalCount;
                        int year = am.Att_allAcademicYears;
                        DateTime date = am.txt_AttendanceDate;
                        int size = TotalCount + 1;
                        string[] Id = new string[size];
                        string[] atn_comment = new string[size];
                        string[] atn_status = new string[size];

                        EmployeeDailyAttendanceServices empAttendance = new EmployeeDailyAttendanceServices();
                        var checkDataExist = empAttendance.AttendanceExist(year, date);
                        if (checkDataExist != null)
                        {
                            for (int i = 0; i <= TotalCount; i++)
                            {
                                Id[i] = QSCrypt.Decrypt(am.Id[i]);
                                int RegId = Convert.ToInt32(Id[i]);
                                atn_status[i] = am.atn_status[i];
                                atn_comment[i] = am.atn_comment[i];
                                DateTime updatedDate = DateTimeByZone.getCurrentDate();
                                empAttendance.updateEmployeeDailyAttendance(RegId, year, date, atn_status[i], atn_comment[i], updatedDate);
                            }
                            string Message = ResourceCache.Localize("AttendanceUpdatedSuccessfully");
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdateEmployeeAttendance"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            for (int i = 0; i <= TotalCount; i++)
                            {
                                Id[i] = QSCrypt.Decrypt(am.Id[i]);
                                int FacultyId = Convert.ToInt32(Id[i]);
                                atn_status[i] = am.atn_status[i];
                                atn_comment[i] = am.atn_comment[i];
                                DateTime markedDate = DateTimeByZone.getCurrentDate();

                                empAttendance.addEmployeeDailyAttendance(year, FacultyId, date, atn_status[i], atn_comment[i], markedDate);
                            }
                            string Message = ResourceCache.Localize("Attendance_saved _Successfully");
                            useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddEmployeeAttendance"));
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string InfoMessage = ResourceCache.Localize("Records_can_save_for_current_academic_year_only");
                        return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                    }  
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        // Reports
        public ActionResult FacultyAttendanceReport()
        {
            return View();
        }

        //Faculty Attendance Report
        private IList<FacultyAttendanceReport> todoListFacultyAttendance = new List<FacultyAttendanceReport>();

        public JsonResult FacultyAttendanceReportResults(string sidx, string sord, int page, int rows, int? allFaculties, DateTime? faculty_txt_FromDate, DateTime? faculty_txt_ToDate)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            if (allFaculties != 0 && faculty_txt_FromDate != null && faculty_txt_ToDate != null)
            {
                FacultyDailyAttendanceServices faculytAttendance = new FacultyDailyAttendanceServices();
                todoListFacultyAttendance = faculytAttendance.FacultyAttendanceList(allFaculties, faculty_txt_FromDate, faculty_txt_ToDate);
                todoListFacultyAttendance = todoListFacultyAttendance.Where(p => p.txt_FromDate >= faculty_txt_FromDate && p.txt_FromDate <= faculty_txt_ToDate).ToList();
            }

            int totalRecords = todoListFacultyAttendance.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListFacultyAttendance = todoListFacultyAttendance.OrderByDescending(s => s.Date).ToList();
                    todoListFacultyAttendance = todoListFacultyAttendance.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListFacultyAttendance = todoListFacultyAttendance.OrderBy(s => s.Date).ToList();
                    todoListFacultyAttendance = todoListFacultyAttendance.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListFacultyAttendance;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListFacultyAttendance
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FacultyAttendanceReport_ExportToExcel()
        {
            try
            {
                List<FacultyAttendanceReport> todoListFacultyAttendance = (List<FacultyAttendanceReport>)Session["JQGridList"];
                var list = todoListFacultyAttendance.Select(o => new { Date = o.Date1, Status = o.Status, Comment = o.Reason }).ToList();
                string fileName = ResourceCache.Localize("FacultyAttendanceReportXlsx");
                GenerateExcel.ExportExcel(list, fileName);
                return View("FacultyAttendanceReport");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult FacultyAttendanceReport_ExportToPdf()
        {
            try
            {
                List<FacultyAttendanceReport> todoListFacultyAttendance = (List<FacultyAttendanceReport>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("FacultyAttendanceReport");
                GeneratePDF.ExportPDF_Portrait(todoListFacultyAttendance, new string[] { "Date1", "Status", "Reason" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("FacultyAttendanceReport.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        //Employee Attendance Report
        private IList<FacultyAttendanceReport> todoListEmployeeAttendance = new List<FacultyAttendanceReport>();

        public JsonResult EmployeeAttendanceReportResults(string sidx, string sord, int page, int rows, int? allEmployees, DateTime? emp_txt_FromDate, DateTime? emp_txt_ToDate)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            if (allEmployees != 0 && emp_txt_FromDate != null && emp_txt_ToDate != null)
            {
                EmployeeDailyAttendanceServices empAttendance = new EmployeeDailyAttendanceServices();
                todoListEmployeeAttendance = empAttendance.EmployeeAttendanceList(allEmployees, emp_txt_FromDate, emp_txt_ToDate);
                todoListEmployeeAttendance = todoListEmployeeAttendance.Where(p => p.txt_FromDate >= emp_txt_FromDate && p.txt_FromDate <= emp_txt_ToDate).ToList();
            }

            int totalRecords = todoListEmployeeAttendance.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListEmployeeAttendance = todoListEmployeeAttendance.OrderByDescending(s => s.Date).ToList();
                    todoListEmployeeAttendance = todoListEmployeeAttendance.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListEmployeeAttendance = todoListEmployeeAttendance.OrderBy(s => s.Date).ToList();
                    todoListEmployeeAttendance = todoListEmployeeAttendance.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListEmployeeAttendance;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListEmployeeAttendance
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmployeeAttendanceReport_ExportToExcel()
        {
            try
            {
                List<FacultyAttendanceReport> todoListEmployeeAttendance = (List<FacultyAttendanceReport>)Session["JQGridList"];
                var list = todoListEmployeeAttendance.Select(o => new { Date = o.Date1, Status = o.Status, Comment = o.Reason }).ToList();
                string fileName = ResourceCache.Localize("EmployeeAttendanceReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("FacultyAttendanceReport");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult EmployeeAttendanceReport_ExportToPdf()
        {
            try
            {
                List<FacultyAttendanceReport> todoListEmployeeAttendance = (List<FacultyAttendanceReport>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("Employee_Attendance_Report");
                GeneratePDF.ExportPDF_Portrait(todoListEmployeeAttendance, new string[] { "Date1", "Status", "Reason" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("EmployeeAttendanceReport.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        //student Attendance Report


        public JsonResult StudentAttendanceReportResults(string sidx, string sord, int page, int rows, int? Att_SectionStudent1, DateTime? txt_FromDate, DateTime? txt_ToDate)//int stuId, DateTime fromDate, DateTime toDate
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            IList<FacultyAttendanceReport> todoListStudentAttendance = new List<FacultyAttendanceReport>();
            if (Att_SectionStudent1 != 0 && txt_FromDate != null && txt_ToDate != null)
            {
                StudentDailyAttendanceServices stuAttendance = new StudentDailyAttendanceServices();
                todoListStudentAttendance = stuAttendance.StudentAttendanceList(Att_SectionStudent1, txt_FromDate, txt_ToDate);
                todoListStudentAttendance = todoListStudentAttendance.Where(p => p.txt_FromDate >= txt_FromDate && p.txt_FromDate <= txt_ToDate).ToList();
            }

            int totalRecords = todoListStudentAttendance.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListStudentAttendance = todoListStudentAttendance.OrderByDescending(s => s.Date).ToList();
                    todoListStudentAttendance = todoListStudentAttendance.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListStudentAttendance = todoListStudentAttendance.OrderBy(s => s.Date).ToList();
                    todoListStudentAttendance = todoListStudentAttendance.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListStudentAttendance;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListStudentAttendance
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StudentAttendanceReport_ExportToExcel()
        {
            try
            {
                List<FacultyAttendanceReport> todoListStudentAttendance = (List<FacultyAttendanceReport>)Session["JQGridList"];
                var list = todoListStudentAttendance.Select(o => new { Date = o.Date1, Status = o.Status, Comment = o.Reason }).ToList();
                string fileName = ResourceCache.Localize("StudentAttendanceReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("FacultyAttendanceReport");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult StudentAttendanceReport_ExportToPdf()
        {
            try
            {
                List<FacultyAttendanceReport> todoListStudentAttendance = (List<FacultyAttendanceReport>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("Student_Attendance_Report");
                GeneratePDF.ExportPDF_Portrait(todoListStudentAttendance, new string[] { "Date1", "Status", "Reason" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("StudentAttendanceReport.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }


    }
}
