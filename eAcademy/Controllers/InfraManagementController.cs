﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class InfraManagementController : Controller
    {
        ClassRoomServices obi_classRoom = new ClassRoomServices();
        AcademicyearServices obj_academicYear = new AcademicyearServices();
        ClassServices obj_class = new ClassServices();
        SectionServices obj_section = new SectionServices();
        SectionStrengthServices obj_strength = new SectionStrengthServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ViewBag.acYear = obj_academicYear.AllList_AcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
            ViewBag.Classes = obj_class.GetClass();
        }
        public ActionResult ClassRooms()
        {
            return View();
        }
        
        public JsonResult ClassRoomList(string sidx, string sord, int page, int rows, int? capacity)
        {
         IList<ClassRoomsList> todoListsResults1 = new List<ClassRoomsList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults1 = obi_classRoom.getClassRooms();
            if (capacity.HasValue)
            {
                todoListsResults1 = (from t1 in todoListsResults1
                                     where t1.Capacity.Value >= capacity
                                     select t1).ToList();
            }
            int totalRecords = todoListsResults1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults1 = todoListsResults1.OrderByDescending(s => s.ClassRoomId).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults1 = todoListsResults1.OrderBy(s => s.ClassRoomId).ToList();
                    todoListsResults1 = todoListsResults1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults1;
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults1
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ClassRoomExportToExcel()
        {
            try
            {
                List<ClassRoomsList> todoListsResults1 = (List<ClassRoomsList>)Session["JQGridList"];
                var list = todoListsResults1.Select(o => new { RoomNumber = o.RoomNumber, Capacity = o.Capacity, Status = Convert.ToString(o.Status.Value) }).ToList();
                string fileName = ResourceCache.Localize("ClassRoomDetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ClassRoomExportToPDF()
        {
            try
            {
                List<ClassRoomsList> todoListsResults1 = (List<ClassRoomsList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = ResourceCache.Localize("Class_Room_Details  ");
                GeneratePDF.ExportPDF_Portrait(todoListsResults1, new string[] { "RoomNumber", "Capacity", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "ClassRoom.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public JsonResult ClassRooms(ClassRoom model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string RoomNo = model.RoomNumber;
                    int Capacity = model.RoomCapacity;
                    bool cc = obi_classRoom.checkClassRoom(RoomNo);
                    if (cc == true)
                    {
                        string ErrorMessage = ResourceCache.Localize("Class_Room_is_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obi_classRoom.addClassRoom(RoomNo, Capacity);
                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddClassRooms"));
                        string Message = ResourceCache.Localize("Class_Room_Added_Successfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditClassRooms(string CRid)
        {
            try
            {
                int cr_Id = Convert.ToInt32(QSCrypt.Decrypt(CRid));
                var ans = obi_classRoom.getClassRooms(cr_Id);
                return Json(new { ans = ans }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveEditClassRooms(ClassRoom model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string status = model.Status;
                    int capacity = model.RoomCapacity;
                    string RoomNo = model.RoomNumber;
                    int CRid = Convert.ToInt32(QSCrypt.Decrypt(model.ClassRoomId));
                    bool cc = obi_classRoom.checkClassRoom(CRid, RoomNo, capacity, status);
                    if (cc == true)
                    {
                        string ErrorMessage = ResourceCache.Localize("Class_Room_is_Exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        SectionStrengthServices secStrengthServ = new SectionStrengthServices();
                        int AcYearId = Convert.ToInt32(Session["CurrentAcYearId"]);



                        var checkClassRoomAssigned = secStrengthServ.IsClassRoomAssigned(CRid, AcYearId);
                        if (checkClassRoomAssigned != null)
                        {
                            if (status == "Inactive")
                            {
                                string ErrorMessage = ResourceCache.Localize("Cannot_change_status_to_false_because_already_assigned_classroom_strength");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                            int existCapacity = checkClassRoomAssigned.Strength.Value;
                            if(existCapacity > capacity)
                            {
                                string ErrorMessage = ResourceCache.Localize("Current_room_capacity_strength_cannot_lessthan_already_assigned_classroom_strength");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        AssignExamClassRoomServices examClassRoomServ = new AssignExamClassRoomServices();
                        var checkExamClassRoomAssigned = examClassRoomServ.IsExamClassRoomAssigned(CRid, AcYearId);
                        if (checkExamClassRoomAssigned != null)
                        {
                            string ErrorMessage = ResourceCache.Localize("Exam_room_already_assigned_So_unable_to_change_room_capacity");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                        obi_classRoom.updatClassRooms(CRid, RoomNo, capacity, status);
                        string Message = ResourceCache.Localize("UpdatedSuccessfully");
                        useractivity.AddEmployeeActivityDetails("Update_class_room");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult AllocateClassRooms()
        {
            return View();
        }
        [HttpPost]
        public JsonResult AllocateClassRooms(CRSection model)
        {
            try
            {

                bool existacademicyear = obj_academicYear.PastAcademicyear(model.Acyear);
                if (existacademicyear == true)
                {
                    int count = model.Count;
                    int tempcount = 0;
                    int[] SectionStrength = new int[count];
                    string ErrorMessage = "";
                    for (int i = 0; i < count; i++)
                    {
                        if (model.CRid[i] != 0)
                        {
                            bool cc = obj_strength.checkCRtoSection(model.SSId[i], model.CRid[i]);
                            if (cc == true)
                            {
                                var ans = obi_classRoom.getClassRooms(model.CRid[i]);
                                ErrorMessage = ans.RoomNumber + ResourceCache.Localize("allocated_already_Use_Some_Other_Class_Room");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                obj_strength.allocateCRtoSection(model.SSId[i], model.CRid[i]);
                            }
                        }
                        else
                        {
                            tempcount++;
                        }
                    }
                    string Message = "";
                    if (tempcount == count)
                    {
                        ErrorMessage = ResourceCache.Localize("SelectClassRoom"); ;
                    }
                    else
                    {
                        Message = ResourceCache.Localize("Class_Room_Allocated_Successfully");
                    }
                    return Json(new { ErrorMessage = ErrorMessage, Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getClass()
        {
            try
            {
                var ans = obj_class.Classes();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getSectionList(int acid, int cid)
        {
            try
            {
                var ans = obj_section.getSectionwithStrength(acid, cid);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getClassRooms()
        {
            try
            {
                var ans = obi_classRoom.getClassRooms();
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getClassRoomsstrength(int strength)
        {
            try
            {
                var ans = obi_classRoom.getClassRooms1(strength);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}