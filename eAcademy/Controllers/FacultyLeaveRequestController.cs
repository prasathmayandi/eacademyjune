﻿using eAcademy.ViewModel;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using eAcademy.Helpers;
using eAcademy.HelperClass;
using eAcademy.Models;

namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class FacultyLeaveRequestController : Controller
    {
        //
        // GET: /FacultyLeaveRequest/

        FacultyleaverequestServices leave = new FacultyleaverequestServices();
        public int FacultyId ;
        UserActivityHelper useractivity = new UserActivityHelper();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            FacultyId = Convert.ToInt32(Session["empRegId"]);
        }

        public JsonResult getAllDate()
         {
            try
            {
                EventServices holidays = new EventServices();
                var HolidaysList = holidays.getAllHolidaysList();
                return Json(new { HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult FutureActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    EventServices holidays = new EventServices();
                    int acid = CurrentAcYear[i].AcademicYearId;
                    var HolidaysList = holidays.getHolidaysList(acid);
                    return Json(new { StartingEnableDate, EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { StartingEnableDate, EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CurrentActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    EventServices holidays = new EventServices();
                    int acid = CurrentAcYear[i].AcademicYearId;
                    var HolidaysList = holidays.getHolidaysList(acid);
                    return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LeaveRequest()
        {
            return View();
        }

        

        public JsonResult LeaveRequestListResults(string sidx, string sord, int page, int rows, DateTime? SM_txt_FromDate_List, DateTime? SM_txt_ToDate_List)
        {
            IList<LeaveRequestStatus> LeaveRequestList1 = new List<LeaveRequestStatus>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            if (SM_txt_FromDate_List != null && SM_txt_ToDate_List != null)
            {
                LeaveRequestList1 = leave.getFacultyLeaveRequest(FacultyId, SM_txt_FromDate_List, SM_txt_ToDate_List);
            }

            int totalRecords = LeaveRequestList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    LeaveRequestList1 = LeaveRequestList1.OrderByDescending(s => s.FromDate).ToList();
                    LeaveRequestList1 = LeaveRequestList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    LeaveRequestList1 = LeaveRequestList1.OrderBy(s => s.FromDate).ToList();
                    LeaveRequestList1 = LeaveRequestList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = LeaveRequestList1;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = LeaveRequestList1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditLeaveRequest(string passId)
        {
            try
            {
                int eid = Convert.ToInt32(QSCrypt.Decrypt(passId));
                var getRow = leave.FacultyLeaveReq(eid);
                var fDate = getRow.FromDate.Value.ToString("dd/MM/yyyy");
                var toDate = getRow.ToDate.Value.ToString("dd/MM/yyyy");
                var reason = getRow.LeaveReason;
                return Json(new { fDate, toDate, reason }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult EditLeaveRequest(EditLeaveRequest e)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int eid = Convert.ToInt32(QSCrypt.Decrypt(e.RequestId));
                    DateTime fdate = e.FL_Edit_txt_FromDate;
                    DateTime toDate = e.FL_Edit_txt_ToDate;
                    Double NoOfDays = (toDate - fdate).TotalDays;
                    int NoOfDays1 = Convert.ToInt32(NoOfDays) + 1;

                    var isApproved = leave.FacultyLeaveReq(eid);
                    var status = isApproved.Status;
                    if(status == "Pending")
                    {
                        var checkLeavReqExist = leave.IsLeaveReqExist(fdate, toDate, FacultyId, eid);
                        if (checkLeavReqExist == "dateExist")
                         {
                             string ErrorMessage = ResourceCache.Localize("Leave_request_already_exist_for_same_date");
                             return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                         }
                         else
                         {
                             leave.EditLeaveReq(eid, FacultyId, fdate, toDate, NoOfDays1, e.FL_Edit_txt_ParentLeaveReason);
                             string Message = ResourceCache.Localize("LeaveRequestUpdatedSuccessfully");
                             useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("UpdateLeaveRequest"));
                             return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                         }                     
                    }
                    else
                    {
                        string ErrorMessage = ResourceCache.Localize("You_cannot_edit");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }              
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LeaveRequest_ExportToExcel()
        {
            try
            {
                List<LeaveRequestStatus> LeaveRequestList1 = (List<LeaveRequestStatus>)Session["JQGridList"];
                var list = LeaveRequestList1.Select(o => new { From_Date = o.From_Date, To_Date = o.To_Date, NumberOfDays = o.NumberOfDays, LeaveReason = o.LeaveReason, Status = o.Status }).ToList();
                string fileName = ResourceCache.Localize("LeaveRequestXlsx");
                GenerateExcel.ExportExcel(list, fileName);
                return View("LeaveRequest");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult LeaveRequest_ExportToPdf()
        {
            try
            {
                List<LeaveRequestStatus> LeaveRequestList1 = (List<LeaveRequestStatus>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("LeaveRequestList");
                GeneratePDF.ExportPDF_Portrait(LeaveRequestList1, new string[] { "From_Date", "To_Date", "NumberOfDays", "LeaveReason", "Status"}, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("LeaveRequest.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult LeaveRequest(LeaveRequest l)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DateTime fdate = l.SM_txt_FromDate;
                    DateTime toDate = l.SM_txt_ToDate;
                    string reason = l.txt_ParentLeaveReason;

                    Double NoOfDays = (toDate - fdate).TotalDays;
                    int NoOfDays1 = Convert.ToInt32(NoOfDays) + 1;

                    var checkLeavReqExist = leave.IsLeaveReqExist(fdate, toDate, FacultyId);
                    if (checkLeavReqExist == "dateExist")
                    {
                        string ErrorMessage = ResourceCache.Localize("Leave_request_already_exist_for_same_date");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    leave.AddFacultyLeaveRequest(FacultyId, fdate, toDate, NoOfDays1, reason);
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("AddLeaveRequest"));
                    string Message = ResourceCache.Localize("Leave_request_has_been_sent");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteLeaveRequest(string passId)
        {
            try
            {
                int Did = Convert.ToInt32(QSCrypt.Decrypt(passId));
                var status = leave.FacultyLeaveReq(Did);
                if (status.Status == "Pending")
                {
                    leave.delLeaveReq(Did);
                    string Message = ResourceCache.Localize("Leave_request_deleted_Successfully");
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("DeleteLeaveRequest"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                string ErrorMessage = "You_cannot_delete";
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
