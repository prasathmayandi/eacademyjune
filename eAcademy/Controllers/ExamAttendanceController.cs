﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class ExamAttendanceController : Controller
    {
        //
        // GET: /ExamAttendance/
        AcademicyearServices acservice = new AcademicyearServices();
        public int FacultyId;
        UserActivityHelper useractivity = new UserActivityHelper();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            FacultyId = Convert.ToInt32(Session["empRegId"]);
            AcademicyearServices AcYear = new AcademicyearServices();
            ExamServices exam = new ExamServices();
            ViewBag.ExamAtn_AcademicYears = AcYear.ShowAcademicYears();
            ViewBag.allExams = exam.ShowExams();     
        }

        public JsonResult getDate(int acid)
        {
            try
            {
                AcademicyearServices AcYear = new AcademicyearServices();
                var ans = AcYear.date(acid);
                string sdate = ans.StartDate.ToString("yyyy/MM/yyyy");
                string edate = ans.EndDate.ToString("yyyy/MM/yyyy");
                return Json(new { sd = sdate, ed = edate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetExamDates(int PassYearId, int PassExamId)
        {
            ExamTimetableServices examTt = new ExamTimetableServices();
            var getDate = examTt.getExamDates(PassYearId, PassExamId).ToList();
            var count = getDate.Count;
            return Json(new { getDate = getDate, count = count }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetScheduleByDate(int PassYearId, int PassExamId, DateTime PassDate)
        {
            ExamTimetableServices examTt = new ExamTimetableServices();
            var getSchedule = examTt.getScheduleBydate(PassYearId, PassExamId, PassDate);
            return Json(getSchedule, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRooomBySchedule(int PassYearId, int PassExamId, DateTime PassDate,int PassSchedule)
        {
            AssignFacultyToExamRoomServices FacultyExamRoom = new AssignFacultyToExamRoomServices();
            var Room = FacultyExamRoom.GetRoom(PassYearId,PassExamId,PassDate,PassSchedule,FacultyId);
            return Json(Room, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetClassWithSection(int PassYearId, int PassExamId, DateTime PassDate,int PassSchedule,int PassRoomId)
        {
             AssignExamClassRoomServices ExamRoom = new AssignExamClassRoomServices();
             var ClassList = ExamRoom.GetClassList(PassYearId, PassExamId, PassDate, PassSchedule, PassRoomId);
             return Json(ClassList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Attendance()
        {
            return View();
        }

        public JsonResult GetStudentList(int PassYearId, int PassExamId, DateTime PassDate, int PassSchedule, int PassRoomId, string PassClassWithSection)
        {
            string clasSec = PassClassWithSection;
            string[] words = clasSec.Split(' ');
            int classId = Convert.ToInt32(words[0]);
            int secId = Convert.ToInt32(words[1]);

            tblAttendanceStatusServices attStatus = new tblAttendanceStatusServices();
            var attStatusList = attStatus.AttendanceStatusList();

            AssignExamClassRoomServices ExamRoom = new AssignExamClassRoomServices();
            var Row = ExamRoom.GetRollNumber(PassYearId, PassExamId, PassDate, PassSchedule, PassRoomId, classId, secId);           
            if (Row != null)
            {
                int StartRollNumberId = Row.StartRollNumberId.Value;
                int EndRollNumberId = Row.EndRollNumberId.Value;
                int subId = Row.SubjectId.Value;
                int StuRegId = Row.StartRegisterId.Value;
                
                ExamAttendanceServices examAtt = new ExamAttendanceServices();
                var checkRecordExist = examAtt.isRecordExist(PassYearId, classId, secId, PassExamId, PassDate, PassSchedule);                

                if (checkRecordExist != null)
                {
                    string msg = "old";
                    var ExistList = examAtt.getExistList(PassYearId, classId, secId, PassExamId, PassDate, PassSchedule);                  
                    int count = ExistList.Count;
                    var n = count;
                    var j = 0;
                    int size = count + 1;
                    for (var i = 0; i < n; i++)
                    {
                        var selVal = ExistList[i].AttStatus;
                        var defVal = ExistList[0].AttStatus;
                        if (selVal == defVal)
                        {
                            j++;
                        }
                        else
                        {
                        }
                    }
                    string schedule = "single";
                    if (j == n)
                    {
                        schedule = "All";
                    }
                    else
                    {
                        schedule = "single";
                    }
                    return Json(new { msg, ExistList, attStatusList, schedule }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    StudentRollNumberServices stuRollnum = new StudentRollNumberServices();
                    string msg = "new";
                    var RollNumberlist = ExamRoom.GetRollNumber1(PassYearId, PassExamId, PassDate, PassSchedule, PassRoomId, classId, secId);
                    int listcount = RollNumberlist.Count();
                    int StartRollNumId;
                    int EndRollNumId;
                    
                    List<ExamAttendanceStudentList> studentList = new List<ExamAttendanceStudentList>();
                    for (int k = 0; k < listcount; k++) 
                    {
                        StartRollNumId = RollNumberlist[k].StartRoolNo.Value;
                        EndRollNumId = RollNumberlist[k].EndRoolNo.Value;
                        var List = stuRollnum.GetStudentList(PassYearId, classId, secId, StartRollNumId, EndRollNumId);
                        studentList.AddRange(List);
                        
                    }
                    
                    var count = studentList.Count;
                    return Json(new { msg, studentList, count, attStatusList }, JsonRequestBehavior.AllowGet);
                }            
            }
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }
       
        [HttpPost]
        public JsonResult SaveAttendance(SaveExamAttendance m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int YearId = Convert.ToInt32(m.AcademicYears);
                    int currentAcYearId;
                    var Cyear = acservice.getCurrentAcademicYearId(DateTimeByZone.getCurrentDate());
                    if (Cyear != null)
                    {
                        Session["CurrentAcYearId"] = Cyear.AcademicYearId;
                    }
                    else
                    {
                        Session["CurrentAcYearId"] = "";
                    }                  
                    if (Session["CurrentAcYearId"] != "")
                    {
                        currentAcYearId = Convert.ToInt16(Session["CurrentAcYearId"]);
                    }
                    else
                    {
                        currentAcYearId = 0;
                    }

                    if (YearId == currentAcYearId)
                    {
                        var TotalCount = m.TotalCount;
                        int year = m.AcademicYears;
                        int ExamId = m.Exam;
                        DateTime date = Convert.ToDateTime(m.Date);
                        int schedule = m.Schedule;
                        int Roomid = m.ClassRoomId;

                        string clasSec = m.ClassWithSection;
                        string[] words = clasSec.Split(' ');
                        int classId = Convert.ToInt32(words[0]);
                        int secId = Convert.ToInt32(words[1]);

                        SchoolSettingServices schSett = new SchoolSettingServices();
                        var MarkType = schSett.getSchoolSettings().MarkFormat;
                        if (MarkType == "Grade")
                        {
                            GradeCommentServices grServ = new GradeCommentServices();
                            var IsProgressCardGenerated = grServ.IsReportCardGenerated(year, classId, secId, ExamId);
                            if (IsProgressCardGenerated != null)
                            {
                                string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_because_progresscard_already_generated");
                                return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            MarkTotalServices mrServ = new MarkTotalServices();
                            var IsProgressCardGenerated = mrServ.ExistRankList(year, classId, secId, ExamId);
                            if (IsProgressCardGenerated != null)
                            {
                                string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_because_progresscard_already_generated");
                                return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        int size = TotalCount + 1;
                        string[] stuRegid = new string[size];
                        string[] RollNumberId = new string[size];
                        string[] AttendanceStatus = new string[size];
                        string[] Reason = new string[size];
                        string[] Rid = new string[size];

                        AssignExamClassRoomServices ExamRoom = new AssignExamClassRoomServices();
                        var Row = ExamRoom.GetRollNumber(year, ExamId, date, schedule, Roomid, classId, secId);
                        if (Row != null)
                        {
                            int StartRollNumberId = Row.StartRollNumberId.Value;
                            int subId = Row.SubjectId.Value;

                            ExamAttendanceServices examAtt = new ExamAttendanceServices();
                            var checkRecordExist = examAtt.isRecordExist(year, classId, secId, ExamId, date, schedule, StartRollNumberId);

                            if (checkRecordExist != null)
                            {
                                for (int i = 0; i <= TotalCount; i++)
                                {
                                    Rid[i] = m.rid[i];
                                    //  int Rowid = Convert.ToInt32(QSCrypt.Decrypt(Rid[i]));
                                    AttendanceStatus[i] = m.AttendanceStatus[i];
                                    Reason[i] = m.Comment[i];
                                    examAtt.UpdateExamAttendance(year, ExamId, date, schedule, classId, secId, AttendanceStatus[i], Reason[i], FacultyId);
                                }
                                string Message = ResourceCache.Localize("AttendanceUpdatedSuccessfully");
                                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Update_student_exam_attendance"));
                                return Json(new { Message }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                for (int i = 0; i <= TotalCount; i++)
                                {
                                    RollNumberId[i] = m.Rollnumberid[i];
                                    int rollNumid = Convert.ToInt32(QSCrypt.Decrypt(RollNumberId[i]));
                                    stuRegid[i] = m.StudentRegId[i];
                                    int sRegId = Convert.ToInt32(QSCrypt.Decrypt(stuRegid[i]));
                                    AttendanceStatus[i] = m.AttendanceStatus[i];
                                    Reason[i] = m.Comment[i];
                                    examAtt.addExamAttendance(year, ExamId, date, schedule, classId, secId, rollNumid, sRegId, AttendanceStatus[i], Reason[i], FacultyId, subId);
                                }
                                string Message = ResourceCache.Localize("Attendance_saved _Successfully");
                                useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Add_student_exam_attendance"));
                                return Json(new { Message }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            string ErrorMessage = ResourceCache.Localize("Please_assign_exam_room_to_the_selected_student");
                            return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
                        } 
                    }
                    else
                    {
                        string InfoMessage = ResourceCache.Localize("Records_can_save_for_current_academic_year_only");
                        return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }


    }
}
