﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eAcademy.Controllers
{
    [Student_CheckSessionAttribute]
    public class DashboardStudentController : Controller
    {
        //
        // GET: /StudentDashboard/

        public int StudentRegId;

        UserActivityHelper useractivity = new UserActivityHelper();

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            StudentRegId = Convert.ToInt32(Session["StudentRegId"]);
        }

        public JsonResult totalstudent()
        {
            try
            {
                var serverTime = DateTimeByZone.getCurrentDateTimeDashboard();
                return Json(new { serverTime }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { ErrorMessage = e.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Dashboard()
        {
            try
            {
                ViewBag.LastLogin = Session["LastLogin"];

                var currentDate = DateTimeByZone.getCurrentDate();
                AnnouncementServices announce = new AnnouncementServices();
                var announcementList = announce.getAnnouncementList(currentDate);
                ViewBag.count = announcementList.Count;

                AssignClassToStudentServices Assclas = new AssignClassToStudentServices();
                var getRow = Assclas.getCurrentActiveClass(StudentRegId);
                int classId = getRow.ClassId.Value;
                int secId = getRow.SectionId.Value;
                // get academicYear, class, section
                //     AcademicyearServices AcYear = new AcademicyearServices();
                ClassServices clas = new ClassServices();
                SectionServices sec = new SectionServices();
                //  var getYear = AcYear.AcademicYear(year).AcademicYear1;
                var getClass = clas.getClassNameByClassID(classId).ClassType;
                var getSection = sec.getSectionName(classId, secId).SectionName;

                Session["studentclass"] = getClass + " " + getSection;

                // for Last Request
                ParentLeaveRequestServices leavReq = new ParentLeaveRequestServices();
                var LeaveRequestStatus1 = leavReq.getLeaveReqDetail(StudentRegId);
                if (LeaveRequestStatus1 != null)
                {
                    ViewBag.fdate = LeaveRequestStatus1.FromDate.Value.ToString("dd MMM, yyyy");
                    ViewBag.todate = LeaveRequestStatus1.ToDate.Value.ToString("dd MMM, yyyy");
                    ViewBag.status = LeaveRequestStatus1.Status;
                }

                HomeworkServices hw = new HomeworkServices();
                var Homework = hw.getHomeworkDetail(currentDate);
                if (Homework != null)
                {
                    ViewBag.Homework = Homework.HomeWork1;
                }

                return View(announcementList);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public string OpenPopup(int id)
        {
            AnnouncementServices announce = new AnnouncementServices();
            var desc = announce.getAnnoucementDesc(id).AnnouncementDescription;
            return desc;
        }
        public JsonResult getDate(int acid)
        {
            try
            {
                AcademicyearServices AcYear = new AcademicyearServices();
                var ans = AcYear.date(acid);
                string sdate = ans.StartDate.ToString("dd/MM/yyyy");
                string edate = ans.EndDate.ToString("dd/MM/yyyy");
                EventServices holidays = new EventServices();
                var HolidaysList = holidays.getHolidaysList(acid);
                return Json(new { sd = sdate, ed = edate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult FutureActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    EventServices holidays = new EventServices();
                    int acid = CurrentAcYear[i].AcademicYearId;
                    var HolidaysList = holidays.getHolidaysList(acid);
                    return Json(new { StartingEnableDate, EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { StartingEnableDate, EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult PastActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    EventServices holidays = new EventServices();
                    int acid = CurrentAcYear[i].AcademicYearId;
                    var HolidaysList = holidays.getHolidaysList(acid);
                    return Json(new { StartingEnableDate, EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { StartingEnableDate, EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CurrentActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");

            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    int acid = CurrentAcYear[i].AcademicYearId;
                    return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CustomeActiveDate(int acid)
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var ans = AcYear.date(acid);
            //string sdate1 = ans.StartDate.ToString("dd/MM/yyyy");
            //string edate1 = ans.EndDate.ToString("dd/MM/yyyy");
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            EventServices holidays = new EventServices();
            var HolidaysList = holidays.getHolidaysList(acid);
            DateTime CurrentDate = DateTimeByZone.getCurrentDate();
            DateTime sdate = ans.StartDate;
            DateTime edate = ans.EndDate;
            if (sdate <= CurrentDate && CurrentDate <= edate)
            {
                StartingEnableDate = sdate.ToString("dd/MM/yyyy");

                return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                EndingEnableDate = edate.ToString("dd/MM/yyyy");
                return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
        }
     
    }
}
