﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.IO;
using System.Drawing;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using eAcademy.DataModel;
using eAcademy.Services;
using System.Collections;
using System.Web.Security;
using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.ViewModel;
using System.Configuration;
using System.Threading;
using System.Globalization;
using System.Web.SessionState;


namespace eAcademy.Controllers
{
    public class HomeController : Controller
    {

        Data db = new Data();
        EacademyEntities ee = new EacademyEntities();
        UserLogServices user_log = new UserLogServices();

        public ActionResult Login()
        {
            try
            {
                if (Session["StudentRegId_SessionAttribute"] != null)
                {
                    return RedirectToAction("Dashboard", "DashboardStudent");
                }
                else if (Session["ParentRegId_SessionAttribute"] != null)
                {
                    return RedirectToAction("Dashboard", "DashboardParent");
                }
                else if (Session["username"] != null)
                {
                    return RedirectToAction("Dashboard", "Dashboard");
                }
                else
                {
                    return View("Login");
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public ActionResult StudentLogin(StudentLogin s)
        {
            try
            {
                string StudentUserName = s.std_userName;
                string StudentPassword = s.std_password;
                string StudentCaptcha = s.Captcha;
                if (StudentUserName == null || StudentUserName == " " || StudentPassword == null || StudentPassword == " " || StudentCaptcha == null || StudentCaptcha == " ")
                {
                    TempData["StdNoValues"] = ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                    return RedirectToAction("Login", "Home");
                }
                if (ModelState.IsValid)
                {
                    if (Session["Captcha"].ToString() == s.Captcha)
                    {
                        string UserName = s.std_userName;
                        string Password = s.std_password;

                        var GetUserSaltStudent = ee.Students.Where(a => a.StudentId.Equals(UserName)).FirstOrDefault();
                        if (GetUserSaltStudent != null)
                        {
                            var salt = GetUserSaltStudent.Salt;
                            var PasswordWithSalt = salt + Password;
                            string ConvertedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(PasswordWithSalt, "SHA1");
                            var CheckValidUserStudent = (from a in ee.Students where a.StudentId == UserName && a.Password == ConvertedPassword select a).FirstOrDefault();

                            if (CheckValidUserStudent != null)
                            {
                                int StudentRegId = CheckValidUserStudent.StudentRegisterId;
                                Session["StudentRegId"] = CheckValidUserStudent.StudentRegisterId;
                                Session["StudentRegId_SessionAttribute"] = CheckValidUserStudent.StudentRegisterId;
                                AdmissionTransactionServices admissionTrans = new AdmissionTransactionServices();
                                AcademicyearServices acservice = new AcademicyearServices();
                                var primaryUserId = admissionTrans.getPrimaryUserId(CheckValidUserStudent.StudentRegisterId);
                                if (primaryUserId != null)
                                {
                                    Session["ParentRegId"] = primaryUserId.PrimaryUserRegisterId;
                                }

                                var school = ee.SchoolSettings.Select(q => q).FirstOrDefault();
                                if (school != null)
                                {
                                    Session["schoolName"] = school.SchoolName;
                                    var year = acservice.getCurrentAcademicYearId(DateTimeByZone.getCurrentDate());
                                    if (year != null)
                                    {
                                        Session["CurrentAcYearId"] = year.AcademicYearId;
                                        Session["List_ChosenAcademicYearId"] = year.AcademicYearId;
                                        Session["Add_ChosenAcademicYearId"] = year.AcademicYearId;

                                    }
                                    else
                                    {
                                        Session["CurrentAcYearId"] = "";
                                    }
                                }
                                Session["Image"] = "/Documents/" + CheckValidUserStudent.StudentId + "/" + CheckValidUserStudent.Photo.ToString();
                                Session["StudentName"] = CheckValidUserStudent.FirstName;

                                var GetLastLogin = (from a in ee.UserLogs where a.UserId == CheckValidUserStudent.StudentId select a).OrderByDescending(q => q.UserLogId).FirstOrDefault();
                                if (GetLastLogin != null)
                                {
                                    Session["LastLogin"] = GetLastLogin.LoginTime.Value.ToString("dd MMM, yyyy HH:mm");
                                }
                                Session["WebsiteUrl"] = ConfigurationManager.AppSettings["Url"];
                                Session["StudentUsername"] = CheckValidUserStudent.StudentId;
                                SessionIDManager manager = new SessionIDManager();
                                string newSessionId = manager.CreateSessionID(System.Web.HttpContext.Current);
                                string ClientIP = IpAddress.GetIP4Address();
                                DateTime loginTime = DateTimeByZone.getCurrentDateTime();
                                var time = loginTime.ToString("yyyy-MM-dd HH:mm");
                                DateTime login = Convert.ToDateTime(time);
                                Session["StudentLoginTime"] = time;
                                Session["StudentSessionId"] = newSessionId;
                                user_log.checkPreviousloginclose(StudentRegId);
                                db.addUserLog(newSessionId, Session["StudentUsername"].ToString(), "Student", ClientIP, login, StudentRegId);
                                return RedirectToAction("Dashboard", "DashboardStudent");
                            }
                            else
                            {
                                TempData["StdErrorMessage"] = ResourceCache.Localize("PasswordIncorrect");
                                return RedirectToAction("Login", "Home");
                            }
                        }
                        else
                        {
                            TempData["StdErrorMessage"] = ResourceCache.Localize("Please_check_Username_and_Password");
                            return RedirectToAction("Login", "Home");
                        }
                    }
                    else
                    {
                        TempData["StdUserId"] = s.std_userName;
                        TempData["StdErrorMessage"] = ResourceCache.Localize("Security_code_is_wrong");
                        return RedirectToAction("Login", "Home");
                    }
                }
                else
                {
                    TempData["StdmodelState"] = "Password Must Contains 3 Characters";
                    return RedirectToAction("Login", "Home");
                }

            }
            catch (Exception e)
            {
                TempData["StdErrorMessage"] = e.Message.ToString();
                return RedirectToAction("Login", "Home");
            }
        }


        public ActionResult ParentLogin(ParentLogin p)
        {
            try
            {
                string ParentUserName = p.parent_userName;
                string ParentPassword = p.parent_password;
                string ParentCaptcha = p.Captcha;
                if (ParentUserName == null || ParentUserName == " " || ParentPassword == null || ParentPassword == " " || ParentCaptcha == null || ParentCaptcha == " ")
                {
                    TempData["ParentNoValues"] = ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                    return RedirectToAction("Login", "Home");
                }
                if (ModelState.IsValid)
                {
                    if (Session["Captcha"].ToString() == p.Captcha)
                    {
                        string UserName = p.parent_userName;
                        string Password = p.parent_password;

                        var GetPrimaryUser = ee.PrimaryUserRegisters.Where(a => a.PrimaryUserId == UserName).FirstOrDefault();

                        if (GetPrimaryUser != null)
                        {
                            var salt = GetPrimaryUser.Salt;
                            var PasswordWithSalt = salt + Password;
                            string ConvertedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(PasswordWithSalt, "SHA1");
                            var CheckValidPrimaryUser = (from a in ee.PrimaryUserRegisters where a.PrimaryUserId == UserName && a.PrimaryUserPwd == ConvertedPassword select a).FirstOrDefault();
                            if (CheckValidPrimaryUser != null)
                            {
                                AcademicyearServices acservice = new AcademicyearServices();
                                int Parentid = CheckValidPrimaryUser.PrimaryUserRegisterId;
                                Session["ParentRegId"] = CheckValidPrimaryUser.PrimaryUserRegisterId;
                                Session["ParentRegId_SessionAttribute"] = CheckValidPrimaryUser.PrimaryUserRegisterId;
                                Session["ParentUserName"] = CheckValidPrimaryUser.PrimaryUserName;
                                Session["PrimaryUSerEmail"] = CheckValidPrimaryUser.PrimaryUserEmail;

                                // Session["StudentRegId"] = CheckValidPrimaryUser.StudentRegisterId;
                                // Session["FatherName"] = CheckValidUserFather.FatherFirstName;
                                var school = ee.SchoolSettings.Select(q => q).FirstOrDefault();
                                if (school != null)
                                {
                                    Session["schoolName"] = school.SchoolName;
                                    var year = acservice.getCurrentAcademicYearId(DateTimeByZone.getCurrentDate());
                                    if (year != null)
                                    {
                                        Session["CurrentAcYearId"] = year.AcademicYearId;
                                        Session["List_ChosenAcademicYearId"] = year.AcademicYearId;
                                        Session["Add_ChosenAcademicYearId"] = year.AcademicYearId;
                                    }
                                    else
                                    {
                                        Session["CurrentAcYearId"] = "";
                                    }
                                }
                                // Session["Image"] = "/Documents/FatherPhoto/" + CheckValidUserFather.FatherPhoto.ToString();

                                var GetLastLogin = (from a in ee.UserLogs where a.UserId == CheckValidPrimaryUser.PrimaryUserId select a).OrderByDescending(q => q.UserLogId).FirstOrDefault();
                                if (GetLastLogin != null)
                                {
                                    Session["LastLogin"] = GetLastLogin.LoginTime.Value.ToString("dd MMM, yyyy HH:mm");
                                }

                                Session["WebsiteUrl"] = ConfigurationManager.AppSettings["Url"];
                                Session["ParentUsername"] = CheckValidPrimaryUser.PrimaryUserId;
                                SessionIDManager manager = new SessionIDManager();
                                string newSessionId = manager.CreateSessionID(System.Web.HttpContext.Current);
                                string ClientIP = IpAddress.GetIP4Address();
                                DateTime loginTime = DateTimeByZone.getCurrentDateTime();
                                var time = loginTime.ToString("yyyy-MM-dd HH:mm");
                                DateTime login = Convert.ToDateTime(time);
                                Session["ParentLoginTime"] = time;
                                Session["ParentSessionId"] = newSessionId;
                                user_log.checkPreviousloginclose(Parentid);
                                db.addUserLog(newSessionId, Session["ParentUsername"].ToString(), "Parent", ClientIP, login, Parentid);

                                var primaryUser = CheckValidPrimaryUser.PrimaryUser;

                                if (primaryUser == "Guardian")
                                {
                                    Session["PrimaryUser"] = "Guardian";
                                    Session["GuardianRegId_SessionAttribute"] = CheckValidPrimaryUser.PrimaryUserRegisterId;
                                    return RedirectToAction("StudentCredential", "Guardian");
                                }
                                else
                                {
                                    Session["PrimaryUser"] = primaryUser;
                                    return RedirectToAction("Dashboard", "DashboardParent");
                                }
                            }
                            else
                            {
                                TempData["ParentErrorMessage"] = ResourceCache.Localize("PasswordIncorrect");
                                return RedirectToAction("Login", "Home");
                            }
                        }
                        else
                        {
                            TempData["ParentErrorMessage"] = ResourceCache.Localize("UsernameIncorrect");
                            return RedirectToAction("Login", "Home");
                        }
                    }
                    else
                    {
                        TempData["ParentUserId"] = p.parent_userName;
                        TempData["ParentErrorMessage"] = ResourceCache.Localize("Security_code_is_wrong");
                        return RedirectToAction("Login", "Home");
                    }
                }
                else
                {
                    TempData["ParentmodelState"] = "Password Must Contains 3 Characters";
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (Exception e)
            {
                TempData["ParentErrorMessage"] = e.Message.ToString();
                return RedirectToAction("Login", "Home");
            }
        }


        public JsonResult CurrentActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");

            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    int acid = CurrentAcYear[i].AcademicYearId;
                    return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }
      
        [HttpPost]
        public ActionResult AdminLogin(FormCollection frm, EmployeeLogin model)
        {
            try
            {
                TechEmployeeServices obj_tech = new TechEmployeeServices();
                RoleServices obj_role = new RoleServices();

                string AdminUserName = model.admin_userid;
                string AdminPassword = model.admin_password;
                string AdminiCaptcha = model.Captcha;

                if (AdminUserName == null || AdminPassword == null || AdminiCaptcha == null || AdminUserName == " " || AdminPassword == " " || AdminiCaptcha == " ")
                {
                    TempData["NoValues"] = ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                    return RedirectToAction("Login", "Home");
                }
                if (ModelState.IsValid)
                {
                    string captcha = frm["Captcha"].ToString();

                    if (Session["Captcha"] == null || Session["Captcha"].ToString() != captcha)
                    {
                        TempData["AdminUserId"] = model.admin_userid;
                        TempData["ErrorMessage"] = ResourceCache.Localize("Entered_Wrong_Code");
                        return RedirectToAction("Login", "Home");
                    }
                    else
                    {
                        string username = frm["admin_userid"];
                        string password = frm["admin_password"];

                        string ConvertedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");

                        var ans = obj_tech.LoginCheck(username, ConvertedPassword);

                        if (ans.Count == 1)
                        {
                            AcademicyearServices acservice = new AcademicyearServices();
                            Session["username"] = username;
                            int empid = ans.SingleOrDefault().EmployeeId;
                            Session["empRegId"] = empid;
                            Session["mail"] = ans.SingleOrDefault().Email;
                            var school = ee.SchoolSettings.Select(q => q).ToList();
                            if (school.Count != 0)
                            {
                                var s = ee.SchoolSettings.Select(q => q).FirstOrDefault();
                                Session["schoolName"] = s.SchoolName;
                                Session["SchoolAddress1"] = s.AddressLine1;
                                Session["SchoolAddress2"] = s.AddressLine2;
                                Session["SchoolCity"] = s.City;
                                Session["SchoolCountry"] = s.Country;
                                Session["SchoolPincode"] = s.ZIP;
                                Session["SchoolPhone"] = s.Phone1;
                                Session["SchoolFax"] = s.Fax;
                                var year = acservice.getCurrentAcademicYearId(DateTimeByZone.getCurrentDate());
                                if (year != null)
                                {
                                    Session["CurrentAcYearId"] = year.AcademicYearId;
                                    Session["List_ChosenAcademicYearId"] = year.AcademicYearId;
                                    Session["Add_ChosenAcademicYearId"] = year.AcademicYearId;
                                }
                                else
                                {
                                    Session["CurrentAcYearId"] = "";
                                }
                            }

                            var GetLastLogin = (from a in ee.UserLogs where a.UserId == username select a).OrderByDescending(q => q.UserLogId).FirstOrDefault();
                            if (GetLastLogin != null)
                            {
                                Session["LastLogin"] = GetLastLogin.LoginTime.Value.ToString("dd MMM, yyyy HH:mm");
                            }
                            ArrayList FeatureLink = new ArrayList();
                            ArrayList FeatureType = new ArrayList();
                            ArrayList RoleType = new ArrayList();
                            ArrayList ControllerName = new ArrayList();
                            ArrayList GroupName = new ArrayList();
                            ArrayList GName = new ArrayList();
                            ArrayList FeatureId = new ArrayList();
                            ArrayList SubMenus = new ArrayList();
                            ArrayList SubMenusLink = new ArrayList();
                            ArrayList SubMenusFeatureId = new ArrayList();

                            var getFeatureLink = obj_role.getFeatureLink(empid);

                            var getRoleType = obj_role.getRoleType(empid);

                            var getControllerName = obj_role.getControllerName(empid);

                            var getGName = obj_role.getGName(empid);

                            foreach (var d in getFeatureLink)
                            {
                                FeatureLink.Add(d.link);
                                FeatureType.Add(d.feature);
                                GroupName.Add(d.gname);
                                FeatureId.Add(d.fid);
                            }

                            foreach (var s in getRoleType)
                            {
                                RoleType.Add(s.role);
                            }
                            foreach (var c in getControllerName)
                            {
                                ControllerName.Add(c.cname);
                            }
                            foreach (var c in getGName)
                            {
                                GName.Add(c.gname);
                            }


                            foreach (int f in FeatureId)
                            {

                                var SubMenu1 = (from t1 in ee.Features
                                                from t2 in ee.Menus
                                                where t2.MenuStatus == true && t1.FeatureStatus == true && t1.FeatureId == t2.FeatureId && t2.FeatureId == f
                                                select new MenuList { SubMenu = t2.MenuType, SubMenu_Link = t2.MenuLink, FeatureId = t2.FeatureId.Value }).ToList().Distinct();
                                foreach (var sub in SubMenu1)
                                {
                                    SubMenus.Add(sub.SubMenu);
                                    SubMenusLink.Add(sub.SubMenu_Link);
                                    SubMenusFeatureId.Add(sub.FeatureId);
                                }
                            }

                            Session["Sub_Menu"] = SubMenus;
                            Session["Sub_MenuLink"] = SubMenusLink;
                            Session["Sub_MenuFeatureId"] = SubMenusFeatureId;
                            Session["menu"] = FeatureLink;
                            Session["feature"] = FeatureType;
                            Session["gname"] = GroupName;
                            Session["groupName"] = GName;
                            Session["EmployeeName"] = ans.SingleOrDefault().EmployeeName;
                            Session["Role"] = RoleType;
                            Session["ControllerName"] = ControllerName;
                            Session["Image"] = "/img/EmployeePhotos/" + ans.SingleOrDefault().EmpRegId + "/" + ans.SingleOrDefault().File.ToString();
                            Session["FeatureId"] = FeatureId;

                            Uri myReferrer = Request.UrlReferrer;
                            string actual = myReferrer.ToString();
                            SessionIDManager manager = new SessionIDManager();
                            string newSessionId = manager.CreateSessionID(System.Web.HttpContext.Current);

                            Session["EmployeeSessionId"] = newSessionId;
                            Session["WebsiteUrl"] = ConfigurationManager.AppSettings["Url"];
                            string WebUrl = Convert.ToString(Session["WebsiteUrl"]);
                            string ClientIP = IpAddress.GetIP4Address();
                            DateTime loginTime = DateTimeByZone.getCurrentDateTime();
                            var t = loginTime;
                            var time = loginTime.ToString("yyyy-MM-dd HH:mm");
                            DateTime login = Convert.ToDateTime(time);
                            Session["LoginTime"] = time;
                            Session["sessionid"] = newSessionId;
                            user_log.checkPreviousloginclose(empid);
                            db.addUserLog(newSessionId, Session["username"].ToString(), "Employee", ClientIP, login, empid);
                            return RedirectToAction("Dashboard", "Dashboard");
                        }

                        else
                        {
                            TempData["ErrorMessage"] = ResourceCache.Localize("UserName_or_Password_is_Incorrect");
                            return RedirectToAction("Login", "Home");
                        }
                    }
                }
                else
                {
                    TempData["modelState"] = "Password Must Contains 3 Characters";
                    //  TempData["modelState"] = ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                    // return RedirectToAction("Login", "Home");
                    return RedirectToAction("Login", "Home");
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("Login", "Home");
            }
        }

        public ActionResult StaffLogin()
        {
            try
            {
                return RedirectToAction("Home", "SuperAdmin");
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
                return RedirectToAction("Login", "Home");
            }
        }

        public ActionResult Student_forgetpassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Student_forgetpassword(StudentForgotPassword f)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (Session["Captcha"].ToString() == f.Captcha)
                    {
                        var checkValidUser = (from a in ee.Students where a.StudentId == f.student_frpsw_UserName select a).FirstOrDefault();
                        if (checkValidUser != null)
                        {
                            var uid = Guid.NewGuid();
                            int RegId = checkValidUser.StudentRegisterId;
                            DateTime date = DateTimeByZone.getCurrentDate();

                            var Name = checkValidUser.FirstName + " " + checkValidUser.LastName;


                            tblStudentResetPassword RP = new tblStudentResetPassword();
                            RP.Id = uid;
                            RP.UserId = RegId;
                            RP.ResetRequestDateTime = date;
                            ee.tblStudentResetPasswords.Add(RP);
                            ee.SaveChanges();

                            var CheckRowExist = (from a in ee.tblStudentResetPasswords where a.UserId == RegId select a).FirstOrDefault();
                            if (CheckRowExist != null)
                            {
                                string serverUrl = ConfigurationManager.AppSettings["Url"];
                                var link = serverUrl + "/Home/Student_resetpassword?uid=" + uid;
                                StreamReader reader = new StreamReader(Server.MapPath("~/Views/Home/StudentResetPassword.html"));
                                string readFile = reader.ReadToEnd();
                                string myString = "";
                                myString = readFile;
                                myString = myString.Replace("$$name$$", Name.ToString());
                                myString = myString.Replace("$$link$$", link.ToString());

                                string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                                string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                                var HaveEamil = checkValidUser.Email;

                                string PlaceName = "Home-Student_forgetpassword-Student reset password link sent";
                                string UserName = "Student";
                                string UserId = f.student_frpsw_UserName;

                                if (HaveEamil != null)
                                {
                                    var ToMail = HaveEamil;
                                    Mail.SendMail(EmailDisplay, supportEmail, ToMail, ResourceCache.Localize("ResetPassword"), myString.ToString(), true, PlaceName, UserName, UserId);  
                                }
                                else
                                {
                                    var ToMail = ConfigurationManager.AppSettings["stdEmail"];
                                    Mail.SendMail(EmailDisplay, supportEmail, ToMail, ResourceCache.Localize("ResetPassword"), myString.ToString(), true, PlaceName, UserName, UserId);  
                                }
                                return View("ResetPasswordMail");
                            }


                        }
                        else
                        {
                            ViewBag.ErrorMessage = ResourceCache.Localize("Username_doesnot_match");
                        }
                    }
                    else
                    {
                        ViewBag.StdUserId = f.student_frpsw_UserName;
                        ViewBag.ErrorMessage = ResourceCache.Localize("Captcha_wrong");
                    }
                }
                else
                {
                    // ViewBag.ErrorMessage = ResourceCache.Localize("Please_enter_all_mandatory_fields");
                    return View();
                }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public ActionResult ResetPasswordMail()
        {
            return View();
        }

        public ActionResult Student_resetpassword()
        {
            try
            {
                var uid = Request.QueryString["uid"];
                ViewBag.uid = uid;
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        [HttpPost]
        public ActionResult Student_resetpassword(StudentResetPassword r)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (Session["Captcha"].ToString() == r.Captcha)
                    {
                        var id = new Guid(r.uid);
                        var IsUidVaild = (from a in ee.tblStudentResetPasswords where a.Id == id select a).FirstOrDefault();
                        if (IsUidVaild == null)
                        {
                            ViewBag.LinkExpiredMsg = ResourceCache.Localize("Link_is_Expired_ or_Invalid");
                            return View();
                        }
                        else
                        {
                            var RegId = IsUidVaild.UserId;
                            var allsalt = (from a in ee.Students where a.StudentRegisterId == RegId select a).FirstOrDefault();
                            var salt = allsalt.Salt;
                            var UPassWordSalt = salt + r.student_reset_newpsw;
                            string EncryptedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(UPassWordSalt, "SHA1");
                            var update = (from a in ee.Students where a.StudentRegisterId == RegId select a).First();
                            update.Password = EncryptedPassword;
                            ee.SaveChanges();
                            Delete(id);
                            return View("ResetPasswordMsg");
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = ResourceCache.Localize("Captcha_wrong");
                        return View();
                    }
                }
                else
                {
                    //ViewBag.ErrorMessage = ResourceCache.Localize("Please_enter_all_mandatory_fields");       
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        private void Delete(Guid uid)
        {
            try
            {
                var del = (from a in ee.tblStudentResetPasswords where a.Id == uid select a).First();
                ee.tblStudentResetPasswords.Remove(del);
                ee.SaveChanges();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }


        public ActionResult ResetPasswordMsg()
        {
            return View();
        }

        public ActionResult Parent_forgetpassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Parent_forgetpassword(ParentForgotPassword f)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (Session["Captcha"].ToString() == f.Captcha)
                    {
                        var checkValidParent = (from a in ee.PrimaryUserRegisters where a.PrimaryUserId == f.parent_frpsw_username select a).FirstOrDefault();

                        if (checkValidParent != null)
                        {
                            var checkFatherHaveEmail = checkValidParent.PrimaryUserEmail;
                            if (checkFatherHaveEmail != null)
                            {
                                var uid = Guid.NewGuid();
                                int RegId = checkValidParent.PrimaryUserRegisterId;
                                string userName = checkValidParent.PrimaryUserName;
                                DateTime date = DateTimeByZone.getCurrentDate();

                                var Name = checkValidParent.PrimaryUserName;
                                var ToMail = checkValidParent.PrimaryUserEmail;

                                tblFatherResetPassword RP = new tblFatherResetPassword();
                                RP.Id = uid;
                                RP.UserId = RegId;
                                RP.UserName = userName;
                                RP.ResetRequestDateTime = date;
                                ee.tblFatherResetPasswords.Add(RP);
                                ee.SaveChanges();

                                var CheckRowExist = (from a in ee.tblFatherResetPasswords where a.UserName == userName select a).FirstOrDefault();
                                if (CheckRowExist != null)
                                {
                                    string serverUrl = ConfigurationManager.AppSettings["Url"];
                                    var link = serverUrl + "Home/Parent_resetpassword?uid=" + uid;
                                    StreamReader reader = new StreamReader(Server.MapPath("~/Views/Home/ParentResetPassword.html"));
                                    string readFile = reader.ReadToEnd();
                                    string myString = "";
                                    myString = readFile;
                                    myString = myString.Replace("$$name$$", Name.ToString());
                                    myString = myString.Replace("$$link$$", link.ToString());
                                    string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                                    string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];

                                    string PlaceName = "Home-Parent_forgetpassword-Parent reset password link sent";
                                    string UserName = "Parent";
                                    string UserId = f.parent_frpsw_username;

                                    Mail.SendMail(EmailDisplay, supportEmail, ToMail, ResourceCache.Localize("ResetPassword"), myString.ToString(), true, PlaceName, UserName, UserId);  

                                    return View("ResetPasswordMail");
                                }
                            }
                            else
                            {
                                ViewBag.ErrorMessage = ResourceCache.Localize("Your_Email_address_does_not_found_Please_provide_your_Email_address_to_Admin");
                                return View();
                            }
                        }
                        else
                        {
                            ViewBag.ErrorMessage = ResourceCache.Localize("Username_doesnot_match");
                            return View();
                        }
                    }
                    else
                    {
                        ViewBag.ParentUserId = f.parent_frpsw_username;
                        ViewBag.ErrorMessage = ResourceCache.Localize("Captcha_wrong");
                        return View();
                    }
                }
                else
                {
                    // ViewBag.ErrorMessage = ResourceCache.Localize("Please_enter_all_mandatory_fields");
                    return View();
                }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public ActionResult Parent_resetpassword()
        {
            try
            {
                var uid = Request.QueryString["uid"];
                ViewBag.uid = uid;
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        [HttpPost]
        public ActionResult Parent_resetpassword(ParentResetPassword r)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (Session["Captcha"].ToString() == r.Captcha)
                    {
                        var id = new Guid(r.uid);
                        var IsUidVaildParent = (from a in ee.tblFatherResetPasswords where a.Id == id select a).FirstOrDefault();
                        if (IsUidVaildParent != null)
                        {
                            var RegId = IsUidVaildParent.UserId;
                            var userName = IsUidVaildParent.UserName;
                            var allsalt = (from a in ee.PrimaryUserRegisters where a.PrimaryUserRegisterId == RegId && a.PrimaryUserName == userName select a).FirstOrDefault();
                            var salt = allsalt.Salt;
                            var UPassWordSalt = salt + r.parent_reset_newpsw;
                            string EncryptedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(UPassWordSalt, "SHA1");
                            var update = (from a in ee.PrimaryUserRegisters where a.PrimaryUserRegisterId == RegId select a).First();
                            update.PrimaryUserPwd = EncryptedPassword;
                            ee.SaveChanges();
                            ParentDelete(id);
                            return View("ResetPasswordMsg");
                        }
                        else
                        {
                            ViewBag.LinkExpiredMsg = ResourceCache.Localize("Link_is_Expired_ or_Invalid");
                            return View();
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = ResourceCache.Localize("Captcha_wrong");
                        return View();
                    }
                }
                else
                {
                    // ViewBag.ErrorMessage = ResourceCache.Localize("Please_enter_all_mandatory_fields");
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        private void ParentDelete(Guid uid)
        {
            try
            {
                var delFather = (from a in ee.tblFatherResetPasswords where a.Id == uid select a).FirstOrDefault();
                var delMother = (from a in ee.tblMotherResetPasswords where a.Id == uid select a).FirstOrDefault();
                var delGuardian = (from a in ee.tblGuardianResetPasswords where a.Id == uid select a).FirstOrDefault();
                if (delFather != null)
                {
                    ee.tblFatherResetPasswords.Remove(delFather);
                    ee.SaveChanges();
                }
                else if (delMother != null)
                {
                    ee.tblMotherResetPasswords.Remove(delMother);
                    ee.SaveChanges();
                }
                else if (delGuardian != null)
                {
                    ee.tblGuardianResetPasswords.Remove(delGuardian);
                    ee.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        public ActionResult Logout()
        {

            if (Session["sessionid"] != null)
            {
                Uri myReferrer = Request.UrlReferrer;
                string actual = myReferrer.ToString();

                string ClientIP = IpAddress.GetIP4Address();
                DateTime loginTime = DateTimeByZone.getCurrentDateTime();
                var t = loginTime;
                var time = loginTime.ToString("yyyy-MM-dd HH:mm");
                DateTime login = Convert.ToDateTime(Session["LoginTime"]);
                DateTime logout = Convert.ToDateTime(time);
                string sessionid = Convert.ToString(Session["EmployeeSessionId"]);
                string uname = Convert.ToString(Session["username"]);
                db.UpdateUserLog(sessionid, uname, "Employee", ClientIP, login, logout);
            }

            //FormsAuthentication.SignOut();


            //this.Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            //this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //this.Response.Cache.SetNoStore();

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult StudentLogout()
        {
            if (Session["StudentSessionId"] != null)
            {
                string ClientIP = IpAddress.GetIP4Address();
                DateTime logoutTime = DateTimeByZone.getCurrentDateTime();
                var time = logoutTime.ToString("yyyy-MM-dd HH:mm");
                DateTime login = Convert.ToDateTime(Session["StudentLoginTime"]);
                DateTime logout = Convert.ToDateTime(time);
                var sessionid = Session["StudentSessionId"];
                db.UpdateUserLog(Convert.ToString(sessionid), Convert.ToString(Session["StudentUsername"]), "Student", ClientIP, login, logout);
            }


            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult ParentLogout()
        {
            if (Session["ParentSessionId"] != null)
            {
                string ClientIP = IpAddress.GetIP4Address();
                DateTime logoutTime = DateTimeByZone.getCurrentDateTime();
                var time = logoutTime.ToString("yyyy-MM-dd HH:mm");
                DateTime login = Convert.ToDateTime(Session["ParentLoginTime"]);
                DateTime logout = Convert.ToDateTime(time);
                var sessionid = Session["ParentSessionId"];
                db.UpdateUserLog(Convert.ToString(sessionid), Convert.ToString(Session["ParentUsername"]), "Parent", ClientIP, login, logout);
            }


            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult CaptchaImage(string prefix, bool noisy = true)
        {
            var chars = "0123456789";
            var rand = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 5)
                          .Select(s => s[rand.Next(s.Length)])
                          .ToArray());

            var captcha = result;
            //store answer
            Session["Captcha" + prefix] = result;

            //image stream
            FileContentResult img = null;

            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(130, 30))
            using (var gfx = Graphics.FromImage((Image)bmp))
            {
                gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));
                //add noise
                if (noisy)
                {
                    int i, r, x, y;
                    var pen = new Pen(Color.Yellow);
                    for (i = 1; i < 10; i++)
                    {
                        pen.Color = Color.FromArgb(
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)));
                        r = rand.Next(0, (130 / 3));
                        x = rand.Next(0, 130);
                        y = rand.Next(0, 30);
                        int x1 = x - r;
                        int y1 = y - r;
                        //gfx.DrawEllipse(pen, x1, y1, r, r);
                    }
                }
                //add question
                gfx.DrawString(captcha, new Font("Tahoma", 15), Brushes.Gray, 2, 3);
                //render as Jpeg
                bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
                img = this.File(mem.GetBuffer(), "image/Jpeg");
            }

            return img;
        }

        //Staff forgetpassword
        public ActionResult Employee_forgetpassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Employee_forgetpassword(EmployeeForgotPassword f)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (Session["Captcha"].ToString() == f.Captcha)
                    {
                        var checkValidUser = (from a in ee.TechEmployees where a.Email == f.Employee_frpsw_UserName select a).FirstOrDefault();
                        if (checkValidUser != null)
                        {
                            var uid = Guid.NewGuid();
                            int RegId = checkValidUser.EmployeeRegisterId;
                            DateTime date = DateTimeByZone.getCurrentDate();

                            var Name = checkValidUser.EmployeeName + " " + checkValidUser.LastName;


                            tblTechEmployeeResetPassword RP = new tblTechEmployeeResetPassword();
                            RP.Id = uid;
                            RP.UserId = RegId;
                            RP.ResetRequestDateTime = date;
                            ee.tblTechEmployeeResetPasswords.Add(RP);
                            ee.SaveChanges();

                            var CheckRowExist = (from a in ee.tblTechEmployeeResetPasswords where a.UserId == RegId select a).FirstOrDefault();
                            if (CheckRowExist != null)
                            {
                                string serverUrl = ConfigurationManager.AppSettings["Url"];
                                var link = serverUrl + "Home/Employee_resetpassword?uid=" + uid;
                                StreamReader reader = new StreamReader(Server.MapPath("~/Views/Home/StudentResetPassword.html"));
                                string readFile = reader.ReadToEnd();
                                string myString = "";
                                myString = readFile;
                                myString = myString.Replace("$$name$$", Name.ToString());
                                myString = myString.Replace("$$link$$", link.ToString());

                                var HaveEamil = f.Employee_frpsw_UserName;
                                var ToMail = HaveEamil;
                                string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                                string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                                string PlaceName = "Home-Employee_forgetpassword-Employee reset password link sent";
                                string UserName = "Employee";
                                string UserId = f.Employee_frpsw_UserName;

                                Mail.SendMail(EmailDisplay, supportEmail, ToMail, ResourceCache.Localize("ResetPassword"), myString.ToString(), true, PlaceName, UserName, UserId);  
                                return View("ResetPasswordMail");
                            }


                        }
                        else
                        {
                            ViewBag.ErrorMessage = ResourceCache.Localize("User_Email_Id_doesnot_match");
                        }
                    }
                    else
                    {
                        ViewBag.EmailAddress = f.Employee_frpsw_UserName;
                        ViewBag.ErrorMessage = ResourceCache.Localize("Captcha_wrong");
                    }
                }
                else
                {
                    //ViewBag.ErrorMessage = ResourceCache.Localize("Please_enter_all_mandatory_fields");
                    return View();
                }
                return View();

            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        public ActionResult Employee_resetpassword()
        {
            try
            {
                var uid = Request.QueryString["uid"];
                ViewBag.uid = uid;
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        [HttpPost]
        public ActionResult Employee_resetpassword(TechEmployeeResetPassword r)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (Session["Captcha"].ToString() == r.Captcha)
                    {
                        var id = new Guid(r.uid);
                        var IsUidVaild = (from a in ee.tblTechEmployeeResetPasswords where a.Id == id select a).FirstOrDefault();
                        if (IsUidVaild == null)
                        {
                            ViewBag.LinkExpiredMsg = ResourceCache.Localize("Link_is_Expired_ or_Invalid");
                            return View();
                        }
                        else
                        {
                            var RegId = IsUidVaild.UserId;
                            var UPassWord = r.Employee_reset_newpsw;
                            string EncryptedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(UPassWord, "SHA1");
                            var update = (from a in ee.TechEmployees where a.EmployeeRegisterId == RegId select a).First();
                            update.Password = EncryptedPassword;
                            ee.SaveChanges();
                            EmployeeDelete(id);
                            return View("ResetPasswordMsg");
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = ResourceCache.Localize("Captcha_wrong");
                        return View();
                    }
                }
                else
                {
                    // ViewBag.ErrorMessage = ResourceCache.Localize("Please_enter_all_mandatory_fields");
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }

        private void EmployeeDelete(Guid uid)
        {
            try
            {
                var del = (from a in ee.tblTechEmployeeResetPasswords where a.Id == uid select a).First();
                ee.tblTechEmployeeResetPasswords.Remove(del);
                ee.SaveChanges();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }




        //Language change
        public ActionResult ChangeCulture(string PassCulture)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo(PassCulture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(PassCulture);

            Session["CurrentCulture"] = PassCulture;
            //string currenturl = Request.Url.AbsoluteUri;
            string Message = ResourceCache.Localize("Language_changed");
            string selVal = PassCulture;
            return Json(new { Message, selVal }, JsonRequestBehavior.AllowGet);
        }

        //Skin change

        public JsonResult Changeskin(string id)
        {
            Session["skin"] = id;
            var x = Session["StrippedBlack_Skin"];
            string Message = ResourceCache.Localize("skin_changed");
            string selVal = id;
            return Json(new { Message, selVal }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SkinChange()
        {
            string skin = "skin1";
            Session["skin"] = skin;
            var x = Session["StrippedBlack_Skin"];
            string Message = ResourceCache.Localize("skin_changed");
            string selVal = skin;
            return Json(new { Message, selVal }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCulture()
        {
            var cul = Session["CurrentCulture"];
            if (cul != "" && cul != null)
            {
                return Json(new { cul }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                cul = "empty";
                return Json(new { cul }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UserLogin(LoginData model)
        {
            var user = ee.TechEmployees.Where(q => q.EmployeeId.Equals(model.Username) && q.Password.Equals(model.Password)).FirstOrDefault(); ;
            return new JsonResult { Data = user, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        public ActionResult part3()
        {
            return View();
        }
        public ActionResult PageNotFound()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.Error = e.Message.ToString();
                return View("Error");
            }
        }

        protected override void Dispose(bool disposing)
        {
            ee.Dispose();
            base.Dispose(disposing);
        }
    }
}
