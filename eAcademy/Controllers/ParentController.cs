﻿using eAcademy.DataModel;
using eAcademy.ViewModel;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using eAcademy.Helpers;
using eAcademy.HelperClass;
using eAcademy.Models;
using System.Net;

namespace eAcademy.Controllers
{
    [Parent_CheckSessionAttribute]
    public class ParentController : Controller
    {
        //
        // GET: /Parent/
        Data ee = new Data();
        EacademyEntities db = new EacademyEntities();

        public int ParentRegId;
        public int StudentRegId;
        public string ParentUserName;

        ParentDetailServices parentDetail = new ParentDetailServices();
        AssignClassToStudentServices clas = new AssignClassToStudentServices();
        FeeServices Annualfee = new FeeServices();
        StudentGradeServices grade = new StudentGradeServices();
        UserActivityHelper useractivity = new UserActivityHelper();

        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ParentRegId = Convert.ToInt32(Session["ParentRegId"]);
            ParentUserName = Convert.ToString(Session["ParentUserName"]);
            StudentRegId = Convert.ToInt32(Session["StudentRegId"]);
            AcademicyearServices AcYear = new AcademicyearServices();
            ExamServices Exam = new ExamServices();
            CountryServices country = new CountryServices();
            ViewBag.allAcademicYears = AcYear.ShowAcademicYears();
            ViewBag.allExams = Exam.ShowExams();
            ViewBag.allCountries = country.ShowCountries();
        }

        public JsonResult getAllDate()
        {
            try
            {
                EventServices holidays = new EventServices();
                var HolidaysList = holidays.getAllHolidaysList();
                return Json(new { HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult FutureActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    EventServices holidays = new EventServices();
                    int acid = CurrentAcYear[i].AcademicYearId;
                    var HolidaysList = holidays.getHolidaysList(acid);
                    return Json(new { StartingEnableDate, EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { StartingEnableDate, EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CurrentActiveDate()
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var CurrentAcYear = AcYear.getCurrentAcYear();
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");

            for (int i = 0; i < CurrentAcYear.Count; i++)
            {
                DateTime CurrentDate = DateTimeByZone.getCurrentDate();
                DateTime sdate = CurrentAcYear[i].StartDate;
                DateTime edate = CurrentAcYear[i].EndDate;
                if (sdate <= CurrentDate && CurrentDate <= edate)
                {
                    EndingEnableDate = edate.ToString("dd/MM/yyyy");
                    StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                    int acid = CurrentAcYear[i].AcademicYearId;
                    return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { sd = StartingEnableDate, ed = EndingEnableDate }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CustomeActiveDate(int acid)
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var ans = AcYear.date(acid);
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            EventServices holidays = new EventServices();
            var HolidaysList = holidays.getHolidaysList(acid);
            DateTime CurrentDate = DateTimeByZone.getCurrentDate();
            DateTime sdate = ans.StartDate;
            DateTime edate = ans.EndDate;
            if (sdate <= CurrentDate && CurrentDate <= edate)
            {
                StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                EndingEnableDate = edate.ToString("dd/MM/yyyy");
                return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Profile1()
        {
            try
            {
                AdmissionTransactionServices admissionTransact = new AdmissionTransactionServices();
                var getStudentParentDetails = admissionTransact.checkStudentParentProfileExist(ParentRegId, StudentRegId);
                if (getStudentParentDetails != null)
                {
                    int? FatherRegId = getStudentParentDetails.FatherRegisterId;
                    int? MotherRegId = getStudentParentDetails.MotherRegisterId;
                    int? GuardianRegId = getStudentParentDetails.GuardianRegisterId;
                    var list = admissionTransact.getStudentParentDetails(ParentRegId, StudentRegId);

                    StudentServices stuser = new StudentServices();
                    var row = stuser.getStudentRow(StudentRegId);
                    if (row != null)
                    {
                        int? bloodGroupid = row.StudentBloodGroup;
                        if (bloodGroupid != null)
                        {
                            var blood = admissionTransact.getBloodGroup(bloodGroupid);
                            if (blood != null)
                            {
                                var BloodGroup = blood.BloodGroupName;
                                ViewBag.StudentBloodGroup = BloodGroup;
                            }
                        }
                        string localCountryId = row.LocalCountry;
                        ViewBag.StudentCountryname = localCountryId;
                    }
                    if (FatherRegId != null)
                    {
                        var getFatherDetail = admissionTransact.getFatherDetail(ParentRegId, StudentRegId, FatherRegId);
                        ViewBag.FatherName = getFatherDetail.FatherName;
                        ViewBag.FatherDOB = getFatherDetail.FatherDob;
                        ViewBag.FatherEmail = getFatherDetail.FatherEmail;
                        ViewBag.FatherMobile = getFatherDetail.FatherMobile;
                        ViewBag.FatherOccupation = getFatherDetail.FatherOccupation;
                        ViewBag.FatherQualification = getFatherDetail.FatherQualification;
                    }
                    if (MotherRegId != null)
                    {
                        var getMotherDetail = admissionTransact.getMotherDetail(ParentRegId, StudentRegId, MotherRegId);
                        ViewBag.MotherName = getMotherDetail.MotherName;
                        ViewBag.MotherDOB = getMotherDetail.MotherDob;
                        ViewBag.MotherEmail = getMotherDetail.MotherEmail;
                        ViewBag.MotherMobile = getMotherDetail.MotherMobile;
                        ViewBag.MotherOccupation = getMotherDetail.MotherOccupation;
                        ViewBag.MotherQualification = getMotherDetail.MotherQualification;
                    }
                    if (GuardianRegId != null)
                    {
                        var getGuardianDetail = admissionTransact.getGuardianDetail(ParentRegId, StudentRegId, GuardianRegId);
                        ViewBag.GuardianName = getGuardianDetail.GuardianName;
                        ViewBag.GuardianGender = getGuardianDetail.GuardianGender;
                        ViewBag.GuardianRelationship = getGuardianDetail.GuardianRelationship;
                        ViewBag.GuardianDob = getGuardianDetail.GuardianDob;
                        ViewBag.GuardianEmail = getGuardianDetail.GuardianEmail;
                        ViewBag.GuardianMobile = getGuardianDetail.GuardianMobile;
                        ViewBag.GuardianQualification = getGuardianDetail.GuardianQualification;
                        ViewBag.GuardianIncome = getGuardianDetail.GuardianIncome;
                    }
                    return View(list);
                }
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult FeeManagement()
        {
            try
            {
                var getRow = clas.getAssigendClass(StudentRegId);
                if (getRow != null)
                {
                    int yearId = Convert.ToInt32(getRow.AcademicYearId);
                    int classId = Convert.ToInt32(getRow.ClassId);
                    int secId = Convert.ToInt32(getRow.SectionId);

                    // To getAnnualFeeStructure
                    var getAnnualFeeStructure = Annualfee.getAnnualFee(yearId, classId);

                    ArrayList AnnualFeeName = new ArrayList();
                    ArrayList AnnualFeeAmount = new ArrayList();
                    ArrayList AnnualFeeLastDate = new ArrayList();
                    foreach (var d in getAnnualFeeStructure)
                    {
                        AnnualFeeName.Add(d.FeeName);
                        AnnualFeeAmount.Add(Math.Round(d.Amount.Value, 2, MidpointRounding.AwayFromZero));
                        AnnualFeeLastDate.Add(d.Last_Date);
                    }

                    ViewBag.AnnualfeeName = AnnualFeeName;
                    ViewBag.AnnualfeeAmount = AnnualFeeAmount;
                    ViewBag.AnnualfeeLastDate = AnnualFeeLastDate;

                    // To getTermFeeStructure
                    TermFeeServices TermFee = new TermFeeServices();
                    var getTermFeeStructure = TermFee.getTermFee(yearId, classId);

                    ArrayList termName = new ArrayList();
                    ArrayList termFeeName = new ArrayList();
                    ArrayList termFeeAmount = new ArrayList();
                    ArrayList termFeeLastDate = new ArrayList();
                    foreach (var t in getTermFeeStructure)
                    {
                        termName.Add(t.TermName);
                        termFeeName.Add(t.TermFeeName);
                        termFeeAmount.Add(t.TermFeeAmount);
                        termFeeLastDate.Add(t.TermFeeLast_Date);
                    }
                    ViewBag.TermName = termName;
                    ViewBag.TermFeeName = termFeeName;
                    ViewBag.TermFeeAmount = termFeeAmount;
                    ViewBag.TermFeeLastDate = termFeeLastDate;

                    ViewBag.yearId = yearId;
                    FeeCollectionServices FeeCollection = new FeeCollectionServices();
                    var list = FeeCollection.getAnnualPaidFees(yearId, classId, secId, StudentRegId);
                    ViewBag.count = list.Count;

                    return View(list);
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public JsonResult FeeManagementList(int PassYearId)
        {
            try
            {
                FeeCollectionServices FeeCollection = new FeeCollectionServices();
                var list = FeeCollection.getAnnualPaidFees1(PassYearId, StudentRegId);
                int count = list.Count;
                return Json(new { list, count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

         public ActionResult AttendanceReport()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AttendanceReport(StudentAttendanceReport r)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int yearId = r.SM_AcademicYear;
                    var getRow = clas.getAssigendClass(StudentRegId, yearId);
                    if (getRow != null)
                    {
                        int classId = Convert.ToInt32(getRow.ClassId);
                        int secId = Convert.ToInt32(getRow.SectionId);


                        StudentDailyAttendanceServices Attendance = new StudentDailyAttendanceServices();
                        var list = Attendance.getStudentDailyAttendance(r.SM_AcademicYear, classId, secId, StudentRegId, r.SM_txt_FromDate, r.SM_txt_ToDate);

                        string PresentStatus = "P";
                        string AbsentStatus = "A";
                        string Halfday = "H";

                        var NumberOfWorkingDays = Attendance.getWorkingDaysCountNew(yearId, classId, secId, r.SM_txt_FromDate, r.SM_txt_ToDate);
                        ViewBag.NumberOfWorkingDays = NumberOfWorkingDays;

                        var getPresentlist = Attendance.getPresentStudent(yearId, classId, secId, StudentRegId, r.SM_txt_FromDate, r.SM_txt_ToDate, PresentStatus).ToList();
                        var PresentDays = getPresentlist.Count;
                        ViewBag.PresentCount = getPresentlist.ToList().Count;

                        var getAbsentlist = Attendance.getPresentStudent(yearId, classId, secId, StudentRegId, r.SM_txt_FromDate, r.SM_txt_ToDate, AbsentStatus);
                        ViewBag.AbsentCount = getAbsentlist.ToList().Count;

                        int HalfDaysCount = Attendance.getPresentStudent(yearId, classId, secId, StudentRegId, r.SM_txt_FromDate, r.SM_txt_ToDate, Halfday).ToList().Count;
                        ViewBag.HalfDaysCount = HalfDaysCount;
                        double totalPresentDays; double Half_FulldayCount = 0.0; double remainingHalfDay = 0.0; double HalfDayCount;

                        if (HalfDaysCount > 1)
                        {
                            Half_FulldayCount = (HalfDaysCount / 2);
                            HalfDayCount = (HalfDaysCount % 2);
                            if (HalfDayCount == 1)
                            {
                                remainingHalfDay = 0.5;
                            }
                        }
                        else if (HalfDaysCount == 1)
                        {
                            remainingHalfDay = 0.5;
                        }
                        totalPresentDays = PresentDays + Half_FulldayCount + remainingHalfDay;
                        double attendancePercent = ((double)totalPresentDays / (double)NumberOfWorkingDays) * 100;
                        ViewBag.attendancePercentage = attendancePercent.ToString("##.##");

                        var f = attendancePercent.ToString("##.##");

                        ViewBag.fdate = r.SM_txt_FromDate.ToString("dd/MM/yyyy");
                        ViewBag.todate = r.SM_txt_ToDate.ToString("dd/MM/yyyy");
                        return View(list);
                    }
                    else
                    {
                        ViewBag.fdate = r.SM_txt_FromDate.ToString("dd/MM/yyyy");
                        ViewBag.todate = r.SM_txt_ToDate.ToString("dd/MM/yyyy");
                        ViewBag.Record = "No student found";
                    }
                }
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult LeaveRequest()
        {
            return View();
        }

        public JsonResult LeaveRequestListResults(string sidx, string sord, int page, int rows, DateTime? SM_txt_FromDate_List, DateTime? SM_txt_ToDate_List)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            IList<LeaveRequestStatus> LeaveRequestList1 = new List<LeaveRequestStatus>();
            if (SM_txt_FromDate_List != null && SM_txt_ToDate_List != null)
            {
                ParentLeaveRequestServices parentLeaveReq = new ParentLeaveRequestServices();
                LeaveRequestList1 = parentLeaveReq.geLeaveRequest(StudentRegId, SM_txt_FromDate_List.Value, SM_txt_ToDate_List.Value);
            }

            int totalRecords = LeaveRequestList1.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    LeaveRequestList1 = LeaveRequestList1.OrderByDescending(s => s.FromDate).ToList();
                    LeaveRequestList1 = LeaveRequestList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    LeaveRequestList1 = LeaveRequestList1.OrderBy(s => s.FromDate).ToList();
                    LeaveRequestList1 = LeaveRequestList1.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = LeaveRequestList1;
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = LeaveRequestList1
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditLeaveRequest(string passId)
        {
            try
            {
                int eid = Convert.ToInt32(QSCrypt.Decrypt(passId));
                ParentLeaveRequestServices parentLeaveReq = new ParentLeaveRequestServices();
                var getRow = parentLeaveReq.ParentLeaveReq(eid);
                var fDate = getRow.FromDate.Value.ToString("dd/MM/yyyy");
                var toDate = getRow.ToDate.Value.ToString("dd/MM/yyyy");
                var reason = getRow.LeaveReason;
                return Json(new { fDate, toDate, reason }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult EditLeaveRequest(EditLeaveRequest e)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int eid = Convert.ToInt32(QSCrypt.Decrypt(e.RequestId));
                    DateTime fdate = e.FL_Edit_txt_FromDate;
                    DateTime toDate = e.FL_Edit_txt_ToDate;
                    Double NoOfDays = (toDate - fdate).TotalDays;
                    int NoOfDays1 = Convert.ToInt32(NoOfDays) + 1;

                    ParentLeaveRequestServices parentLeaveReq = new ParentLeaveRequestServices();
                    var isApproved = parentLeaveReq.ParentLeaveReq(eid);

                    var status = isApproved.Status;
                    if (status == "Pending")
                    {
                        var checkLeavReqExist = parentLeaveReq.IsLeaveReqExist(fdate, toDate, StudentRegId, eid);
                        if (checkLeavReqExist == "dateFree")
                        {

                            parentLeaveReq.EditLeaveReq(eid, StudentRegId, fdate, toDate, NoOfDays1, e.FL_Edit_txt_ParentLeaveReason);
                            useractivity.AddParentActivityDetails(ResourceCache.Localize("UpdateLeaveRequest"));
                            string Message = ResourceCache.Localize("LeaveRequestUpdatedSuccessfully");
                            return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string ErrorMessage = ResourceCache.Localize("Leave_request_already_exist_for_same_date");
                            return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string ErrorMessage = ResourceCache.Localize("You_cannot_edit");
                        return Json(new { Message = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LeaveRequest(LeaveRequest l)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DateTime fdate = l.SM_txt_FromDate;
                    DateTime toDate = l.SM_txt_ToDate;
                    string reason = l.txt_ParentLeaveReason;

                    AssignClassToStudentServices clas = new AssignClassToStudentServices();
                    var getRow = clas.getAssigendClass(StudentRegId);

                    int yearId = Convert.ToInt32(getRow.AcademicYearId);
                    int classId = Convert.ToInt32(getRow.ClassId);
                    int secId = Convert.ToInt32(getRow.SectionId);


                    Double NoOfDays = (toDate - fdate).TotalDays;
                    int NoOfDays1 = Convert.ToInt32(NoOfDays) + 1;

                    AssignClassTeacherServices ClasTeacher = new AssignClassTeacherServices();
                    var getClassIncharge = ClasTeacher.getClassTeacher(yearId, classId, secId);
                    int? EmployeeRegId = getClassIncharge.EmployeeRegisterId;

                    ParentLeaveRequestServices parentLeaveReq = new ParentLeaveRequestServices();

                    var checkLeavReqExist = parentLeaveReq.IsLeaveReqExist(fdate, toDate, StudentRegId);
                    if (checkLeavReqExist == "dateExist")
                    {
                        string ErrorMessage = ResourceCache.Localize("Leave_request_already_exist_for_same_date");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    parentLeaveReq.AddParentLeaveRequest(ParentRegId, StudentRegId, yearId, classId, secId, fdate, toDate, NoOfDays1, reason, EmployeeRegId);
                    useractivity.AddParentActivityDetails(ResourceCache.Localize("Send_leave_request"));
                    string Message = ResourceCache.Localize("Leave_request_has_been_sent");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {

                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception ex)
            {
                string ExceptionError = ex.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteLeaveRequest(string passId)
        {
            try
            {
                int Did = Convert.ToInt32(QSCrypt.Decrypt(passId));
                ParentLeaveRequestServices parentLeaveReq = new ParentLeaveRequestServices();
                var status = parentLeaveReq.ParentLeaveReq(Did);
                if (status.Status == "Pending")
                {
                    parentLeaveReq.delLeaveReq(Did);
                    useractivity.AddParentActivityDetails(ResourceCache.Localize("DeleteLeaveRequest"));
                    string Message = ResourceCache.Localize("Leave_request_deleted_Successfully");
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                string ErrorMessage = ResourceCache.Localize("You_cannot_delete");
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LeaveRequest_ExportToExcel()
        {
            try
            {
                List<LeaveRequestStatus> LeaveRequestList1 = (List<LeaveRequestStatus>)Session["JQGridList"];
                var list = LeaveRequestList1.Select(o => new { From_Date = o.From_Date, To_Date = o.To_Date, NumberOfDays = o.NumberOfDays, LeaveReason = o.LeaveReason, Status = o.Status }).ToList();
                string fileName = ResourceCache.Localize("LeaveRequest");
                GenerateExcel.ExportExcel(list, fileName);
                return View("LeaveRequest");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult LeaveRequest_ExportToPdf()
        {
            try
            {
                List<LeaveRequestStatus> LeaveRequestList1 = (List<LeaveRequestStatus>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string heading = ResourceCache.Localize("LeaveRequestList");
                GeneratePDF.ExportPDF_Portrait(LeaveRequestList1, new string[] { "From_Date", "To_Date", "NumberOfDays", "LeaveReason", "Status" }, xfilePath, heading);
                return File(xfilePath, "application/pdf", ResourceCache.Localize("LeaveRequest.pdf"));
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult ProgressCard()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ProgressCard(CreateProgressCard1 cp)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int yearId = cp.PC_allAcademicYears;
                    var getRow = clas.getAssigendClass(StudentRegId, yearId);
                    if (getRow != null)
                    {
                        int classId = Convert.ToInt32(getRow.ClassId);
                        int secId = Convert.ToInt32(getRow.SectionId);
                        int examId = cp.PC_allExams;

                        StudentMarkServices mark = new StudentMarkServices();
                        var list = mark.getMark(yearId, classId, secId, examId, StudentRegId);

                        ViewBag.list = list.Count;

                        MarkTotalServices markTotal = new MarkTotalServices();
                        var MarkTotal = markTotal.getMarkTotal(yearId, classId, secId, examId, StudentRegId);

                        if (MarkTotal != null)
                        {
                            ViewBag.Total = MarkTotal.TotalMark;
                            ViewBag.Result = MarkTotal.Result;
                            ViewBag.Rank = MarkTotal.StudentRank;
                            ViewBag.comment = MarkTotal.Comment;
                        }

                        return View(list);
                    }
                    ViewBag.Record = "No student found";

                }
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult GradeProgressCard()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GradeProgressCard(CreateProgressCard1 cp)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int yearId = cp.PC_allAcademicYears;
                    var getRow = clas.getAssigendClass(StudentRegId, yearId);
                    int classId = Convert.ToInt32(getRow.ClassId);
                    int secId = Convert.ToInt32(getRow.SectionId);
                    int examId = cp.PC_allExams;

                    var list = grade.getGrade(yearId, classId, secId, examId, StudentRegId);
                    if (list.Count != 0)
                    {
                        ViewBag.list = list.Count;
                        return View(list);
                    }
                    else
                    {
                        ViewBag.record = "empty";
                        return View();
                    }
                }
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Communication()
        {
            return View();
        }
        public static IList<CommunicationList> todoListsResults2 = new List<CommunicationList>();
        public JsonResult CommunicationResults(string sidx, string sord, int page, int rows, DateTime? SM_txt_FromDate_List, DateTime? SM_txt_ToDate_List)
        {
            AssignClassToStudentServices Assclas = new AssignClassToStudentServices();
            CommunicationServices cs = new CommunicationServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            int DefaultStudentRegId = Convert.ToInt16(Session["StudentRegId"]);
            var getRow = Assclas.getCurrentActiveClass(DefaultStudentRegId);
            int? classId = getRow.ClassId;
            int yearid = Convert.ToInt16(Session["CurrentAcYearId"]);


            if (SM_txt_FromDate_List != null && SM_txt_ToDate_List != null)
            {
                todoListsResults2 = cs.GetParentCommunicationDetaions(yearid, classId, SM_txt_FromDate_List.Value, SM_txt_ToDate_List.Value, DefaultStudentRegId);
            }
            else
            {
                todoListsResults2 = cs.GetParentCommunicationDetaions(yearid, classId, DefaultStudentRegId);
            }

            int totalRecords = todoListsResults2.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults2
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
      
        public void OpenCommunicationFile(string id, string name)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                CommunicationServices cs = new CommunicationServices();
                var getSubjectNotesFile = "";
                var r1 = cs.GetClassFile(fid);
                var r2 = cs.GetStudentFile(fid);
                if (name == "class")
                {
                    if (r1 != null)
                    {
                        getSubjectNotesFile = r1.FileName;
                    }
                }
                if (name == "student")
                {
                    if (r2 != null)
                    {
                        getSubjectNotesFile = r2.FileName;
                    }
                }

                string file = getSubjectNotesFile;
                if (file != null)
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Areas/Communication/Documents/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {

                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + file);
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }
        public void OpenCommunicationFile1(string id)
        {
            try
            {
                int fid = Convert.ToInt32(QSCrypt.Decrypt(id));
                CommunicationServices cs = new CommunicationServices();
                var getSubjectNotesFile = "";
                var r1 = cs.GetClassFile(fid);
                var r2 = cs.GetStudentFile(fid);

                if (r1 != null)
                {
                    getSubjectNotesFile = r1.FileName;
                }

                if (r2 != null)
                {
                    getSubjectNotesFile = r2.FileName;
                }

                string file = getSubjectNotesFile;
                if (file != null)
                {
                    string extention = System.IO.Path.GetExtension(file);
                    string FilePath = Server.MapPath("~/Areas/Communication/Documents/" + file);
                    WebClient User = new WebClient();
                    Byte[] FileBuffer = User.DownloadData(FilePath);
                    if (FileBuffer != null)
                    {
                        if (extention == ".pdf")
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else if (extention == ".doc" || extention == ".docx")
                        {

                            Response.ContentType = "application/vnd.ms-word";
                        }
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + file);
                        Response.BinaryWrite(FileBuffer);
                        Response.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message.ToString();
            }
        }
        public ActionResult CommunicationExportToExcel()
        {
            try
            {
                var list = todoListsResults2.Select(o => new { EventDate = o.EventDate, Title = o.Title, Description = o.Description, PostedDate = o.PostedDate }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("CommunicationDetails");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult CommunicationExportToPDF()
        {
            try
            {
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("CommunicationDetails");
                GeneratePDF.ExportPDF_Portrait(todoListsResults2, new string[] { "EventDate", "Title", "Description", "PostedDate" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "CommunicationDetails.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
    }
}
