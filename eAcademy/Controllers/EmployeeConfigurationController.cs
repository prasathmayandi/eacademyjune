﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class EmployeeConfigurationController : Controller
    {
        ClassServices obj_class = new ClassServices();
        SubjectServices obj_subject = new SubjectServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        public int Currentyearid;
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ViewBag.ClassList = obj_class.Classes();
            ViewBag.SubjectList = obj_subject.getSubjectList();
            if (Session["CurrentAcYearId"] == "")
            {
                Currentyearid = 0;
            }
            else
            {
                Currentyearid = Convert.ToInt32(Session["CurrentAcYearId"]);
            }
        }
        public JsonResult GetEmployeeDesination(int EmployeeType)
        {
            try
            {
                EmployeeDesinationServices Empdesignationservice = new EmployeeDesinationServices();
                var Designation = Empdesignationservice.GetEmployeeDesinationList(EmployeeType);
                return Json(Designation, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetEmployeeSubject(string EmpId, int SubjectId)
        {
            try
            {
                int empId = Convert.ToInt32(QSCrypt.Decrypt(EmpId));
                TechEmployeeServices obj_employee = new TechEmployeeServices();
                var checkempalreadyexist = obj_employee.checkemproll(empId, Currentyearid, SubjectId);
                var checkclassteacher = obj_employee.checkempclass(empId, Currentyearid, SubjectId);
                if (checkempalreadyexist != null || checkclassteacher != null)
                {
                    string ErrorMessage =ResourceCache.Localize("In_this_Subject_handled_by_this_staff_in_this_academic_year_So_cannot_remove_this_subject");
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = "";
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetEmployeeclass(string EmpId, int classId)
        {
            try
            {
                int empId = Convert.ToInt32(QSCrypt.Decrypt(EmpId));
                TechEmployeeServices obj_employee = new TechEmployeeServices();
                var checkempalreadyexist = obj_employee.checkempsubjectclass(empId, Currentyearid, classId);
                var checkclassteacher = obj_employee.checkemphandledclass(empId, Currentyearid, classId);
                if (checkempalreadyexist != null || checkclassteacher != null)
                {
                    string ErrorMessage = ResourceCache.Localize("In_this_class_handled_by_this_staff_in_this_academic_year_So_cannot_remove_this_class"); 
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMessage = "";
                    return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EmployeeEnrollment()
        {
            tblCountryServices obj_country = new tblCountryServices();
            CommunityServices obj_community = new CommunityServices();
            BloodGroupServices obj_bloodgroup = new BloodGroupServices();
            EmployeeTypeServices Emptype = new EmployeeTypeServices();
            ViewBag.Country = obj_country.CountryList();
            ViewBag.Community = obj_community.GetCommunity();
            ViewBag.BloodGroup = obj_bloodgroup.GetBloodGroup();
            ViewBag.EmployeeTypes = Emptype.GetEmployeeType();
            try
            {
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        [HttpPost]
        public ActionResult EmployeeEnrollment(SaveOtherEmployee model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult StaffEnrollment1(SaveTechEmployee1 model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    TechEmployeeServices tech = new TechEmployeeServices();
                    OtherEmployeeServices otech = new OtherEmployeeServices();
                    string Fname = model.txt_empname;
                    string Mname = model.txt_empMname;
                    string Lname = model.txt_emplastname;
                    DateTime DOB = model.txt_dob;
                    string Gender = model.txt_gender;
                    string Bloodgrp = model.txt_bgrp;
                    string Religion = model.txt_religion;
                    string Nationality = model.txt_nationality;
                    string Community = model.txt_community;
                    string Marital = model.txt_MaritalStatus;
                    int employeetype = model.txt_EmployeeType;
                    int empdesignation = model.txt_EmployeeDesignation;
                    string txt_ClassList1 = "";
                    string txt_SubjectList1 = "";
                    if (model.txt_ClassList1 != null && model.txt_SubjectList1 != null)
                    {
                        txt_ClassList1 = String.Join(",", model.txt_ClassList1);
                        txt_SubjectList1 = String.Join(",", model.txt_SubjectList1);
                    }
                    string licenseno = model.txt_LicenseNo;
                    DateTime? Expirdate = null;
                    if (model.txt_ExpireDate != null)
                    {
                        Expirdate = Convert.ToDateTime(model.txt_ExpireDate);
                    }
                    string Contact = model.txt_contact;
                    long Mobile = model.txt_Mobile;
                    string Email = model.txt_email;
                    string Address1 = model.txt_addressLine1;
                    string Address2 = model.txt_addressLine2;
                    string City = model.txt_city;
                    string State = model.txt_state;
                    string Country = model.Country;
                    string PIN = model.txt_pin;
                    string PermanentAddress1 = model.txt_Permanentaddress;
                    string PermanentAddress2 = model.txt_PermanentaddressLine;
                    string PermanentCity = model.txt_Permanentcity;
                    string PermanentState = model.txt_Permanentstate;
                    string PermanentCountry = model.PermanentCountry;
                    string PermanentPIN = model.txt_Permanentpin;
                    string EmgPerson = model.txt_emergencyconper;
                    long EmgContact = model.txt_econno;
                    string Relationship = model.txt_emergencyconperrelationship;
                    string Empid;
                    bool checkemailexit = tech.Add_checkEmail(Email);
                    if (checkemailexit == true)
                    {
                        string ErrorMessage = ResourceCache.Localize("Employee_email_already_exist,so_use_another_email ");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    if (employeetype == 4 || employeetype == 5)
                    {
                        Empid = otech.addOtherEmployee(Fname, Mname, Lname, DOB, Gender, Bloodgrp, Religion, Nationality, Community, Marital, employeetype, empdesignation, licenseno, Expirdate, Contact, Mobile, Email, Address1, Address2, City, State, Country, PIN, PermanentAddress1, PermanentAddress2, PermanentCity, PermanentState, PermanentCountry, PermanentPIN, EmgPerson, EmgContact, Relationship);
                        string Message = ResourceCache.Localize("Employee_Updated_Successfully_fill_step2");
                        useractivity.AddEmployeeActivityDetails("Addemployee");
                        return Json(new { Message = Message, Empid }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        Empid = tech.addTechEmployee1(Fname, Mname, Lname, DOB, Gender, Bloodgrp, Religion, Nationality, Community, Marital, employeetype, empdesignation, txt_ClassList1, txt_SubjectList1, Contact, Mobile, Email, Address1, Address2, City, State, Country, PIN, PermanentAddress1, PermanentAddress2, PermanentCity, PermanentState, PermanentCountry, PermanentPIN, EmgPerson, EmgContact, Relationship);
                        string Message = ResourceCache.Localize("Employee_Updated_Successfully_fill_step2");
                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Addemployee"));
                        return Json(new { Message = Message, Empid }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ErrorMessage = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult StaffEnrollment2(SaveTechEmployee2 model)
        {
            TechEmployeeServices obj_employee = new TechEmployeeServices();
            try
            {
                if (ModelState.IsValid)
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ErrorMessage = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SaveStaffEnrollment2()
        {
            TechEmployeeServices obj_employee = new TechEmployeeServices();
            OtherEmployeeServices otech = new OtherEmployeeServices();
            try
            {
                string EmployeeId = Request.Form["Empid"];
                HttpPostedFileBase Certificate = Request.Files["txt_Certificate"];
                int count1 = int.Parse(Request.Form["Education_count"]);
                string Course = Request.Form["Course"];
                string Insitute = Request.Form["Insitute"];
                string University = Request.Form["University"];
                string Year = Request.Form["Year"];
                string Mark = Request.Form["Mark"];
                string Email = Request.Form["txt_email"];
                HttpPostedFileBase Resume = Request.Files["txt_Resume"];
                string txt_Experience = Request.Form["txt_Experience"];
                int count3 = int.Parse(Request.Form["Employeement_count"]);
                string EmpInst = Request.Form["EmpInst"];
                string Designation = Request.Form["Designation"];
                string Expertise = Request.Form["Expertise"];
                string EmpSdate = Request.Form["EmpSdate"];
                string EmpEdate = Request.Form["EmpEdate"];
                string empTotal = Request.Form["empTotal"];
                int count2 = int.Parse(Request.Form["Professional_count"]);
                string CourseName = Request.Form["CourseName"];
                string CertificateName = Request.Form["CertificateName"];
                HttpPostedFileBase[] CertificationFile = new HttpPostedFileBase[count2];
                for (int i = 0; i < count2; i++)
                {
                    CertificationFile[i] = Request.Files["CertificationFile[" + i + "]"];
                }
                HttpPostedFileBase txt_empphoto = Request.Files["txt_empphoto"];
                HttpPostedFileBase RationCard = Request.Files["RationCard"];
                HttpPostedFileBase VoterId = Request.Files["VoterId"];
                DateTime txt_doj = Convert.ToDateTime(Request.Form["txt_doj"]);
                long? txt_Salary = Convert.ToInt64(Request.Form["txt_Salary"]);
                string txt_AcNo = Request.Form["txt_AcNo"];
                string txt_AcName = Request.Form["txt_AcName"];
                string txt_BName = Request.Form["txt_BName"];
                string txt_Pan_Number = Request.Form["txt_Pan_Number"];
                int employeetype = Convert.ToInt16(Request.Form["EmployeeType"]);
                Random r = new Random();
                var psw = r.Next(99999);
                if (employeetype == 4 || employeetype == 5)
                {
                    string msg = otech.addOtherEmployee2(EmployeeId, txt_Experience, Resume, Certificate, count1, Course, Insitute, University, Year, Mark, count3, EmpInst, Designation, Expertise, EmpSdate, EmpEdate, empTotal, Email, txt_empphoto, RationCard, VoterId, txt_doj, txt_Salary, txt_AcNo, txt_AcName, txt_BName, txt_Pan_Number, psw);
                    if (msg == "True")
                    {
                        string Message = ResourceCache.Localize("Employee_Details_Added_Successfully");
                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Addemployee"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { ErrorMessage = msg }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string msg1 = obj_employee.addTechEmployee2(EmployeeId, txt_Experience, Resume, Certificate, count1, Course, Insitute, University, Year, Mark, count2, CourseName, CertificateName, CertificationFile, count3, EmpInst, Designation, Expertise, EmpSdate, EmpEdate, empTotal, Email, txt_empphoto, RationCard, VoterId, txt_doj, txt_Salary, txt_AcNo, txt_AcName, txt_BName, txt_Pan_Number, psw);
                    if (msg1 == "True")
                    {
                        var userdetails = obj_employee.GetTechEmployeeDetails(EmployeeId);
                        var empname = userdetails.EmployeeName + " " + userdetails.LastName;
                        var password = psw;
                        var empid = userdetails.EmployeeId;
                        var empemail = userdetails.Email;
                        var Dateofjoin = userdetails.DOJ;
                        StreamReader reader = new StreamReader(Server.MapPath("~/Views/EmployeeConfiguration/EmailPage.html"));
                        string readFile = reader.ReadToEnd();
                        string myString = "";
                        var link = "http://localhost:55654/#admin";
                        myString = readFile;
                        myString = myString.Replace("$$empname$$", empname.ToString());
                        myString = myString.Replace("$$empid$$", empid.ToString());
                        myString = myString.Replace("$$link$$", link.ToString());
                        myString = myString.Replace("$$username$$", empid.ToString());
                        myString = myString.Replace("$$password$$", password.ToString());
                        myString = myString.Replace("$$Dateofjoin$$", Dateofjoin.ToString());
                        string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                        string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];

                        string PlaceName = "EmployeeConfiguration-EmployeeEnrollment-Tech employee username password details sent";
                        string UserName = "Employee";
                        string UserId = Convert.ToString(Session["username"]);
                        Mail.SendMail(EmailDisplay, supportEmail, empemail, "Account Credentials", myString.ToString(), true, PlaceName, UserName, UserId);  
                        string Message = ResourceCache.Localize("Employee_Details_Added_Successfully,Username_Passowrd_Has_been_Sent_To_Employee_Mail_ID");
                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Addemployee"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { ErrorMessage = msg1 }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ErrorMessage = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult TechEmployeeList()
        {
            TechEmployeeServices obj_employee = new TechEmployeeServices();
            try
            {
                ViewBag.Employee = obj_employee.getEmp();
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult EmpResults(string sidx, string sord, int page, int rows, int? EmployeeRegisterId)
        {
            IList<Employee> TechEmployeeResult = new List<Employee>();
            TechEmployeeServices obj_employee = new TechEmployeeServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            TechEmployeeResult = obj_employee.getEmployee();
            if (EmployeeRegisterId.HasValue)
            {
                TechEmployeeResult = TechEmployeeResult.Where(p => p.EmployeeId == EmployeeRegisterId).ToList();
            }
            int totalRecords = TechEmployeeResult.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    TechEmployeeResult = TechEmployeeResult.OrderByDescending(s => s.DOJ).ToList();
                    TechEmployeeResult = TechEmployeeResult.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    TechEmployeeResult = TechEmployeeResult.OrderBy(s => s.DOJ).ToList();
                    TechEmployeeResult = TechEmployeeResult.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = TechEmployeeResult;
            
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = TechEmployeeResult
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TechEmployeeListExportToExcel()
        {
            try
            {
                List<Employee> TechEmployeeResult = (List<Employee>)Session["JQGridList"];
                var list = TechEmployeeResult.Select(o => new { EmployeeId = o.EmployeeId, EmployeeName = o.EmployeeName, EmployeeTypes = o.EmployeeTypes, EmployeeDesianations = o.EmployeeDesianations, Address = o.Address }).ToList();
                string fileName = "TechEmployeeList";
                GenerateExcel.ExportExcel(list, fileName);
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult TechEmployeeListExportToPDF()
        {
            try
            {
                List<Employee> TechEmployeeResult = (List<Employee>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = "TechEmployeeList";
                GeneratePDF.ExportPDF_Landscape(TechEmployeeResult, new string[] { "EmployeeId", "EmployeeName", "EmployeeTypes", "EmployeeDesianations", "Address" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "TechEmployeeList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult EditTechEmployee(string id)
        {
            tblCountryServices obj_country = new tblCountryServices();
            CommunityServices obj_community = new CommunityServices();
            BloodGroupServices obj_bloodgroup = new BloodGroupServices();
            EmployeeTypeServices Emptype = new EmployeeTypeServices();
            EmployeeDesinationServices design = new EmployeeDesinationServices();
            ViewBag.Country = obj_country.CountryList();
            ViewBag.Community = obj_community.GetCommunity();
            ViewBag.BloodGroup = obj_bloodgroup.GetBloodGroup();
            ViewBag.EmployeeTypes = Emptype.GetEmployeeType();
            ViewBag.EmployeeDesignaion = design.GetEmployeeDesignation();
            
            try
            {
                TechEmployeeServices obj_employee = new TechEmployeeServices();
                TechEmployeeEducationalServices obj_edu = new TechEmployeeEducationalServices();
                TechEmployeeEmploymentServices obj_emp = new TechEmployeeEmploymentServices();
                TechEmployeeProfessionalServices obj_services = new TechEmployeeProfessionalServices();
                TechEmployeeClassSubjectServices obj_cls_sub = new TechEmployeeClassSubjectServices();
                int empId = Convert.ToInt32(QSCrypt.Decrypt(id));
                ViewBag.Empid = id;
                var ans = obj_employee.getEmployeeListById(empId);
                var ans1 = obj_edu.GetEmployeeEducationalDetails(empId);
                ViewBag.ans1 = ans1;
                var ans2 = obj_services.GetEmployeeProfessionalDetails(empId);
                ViewBag.ans2 = ans2;
                var ans3 = obj_emp.GetEmployeeExperience(empId);
                ViewBag.ans3 = ans3;
                var classes = obj_cls_sub.GetEmployeeClass(empId);
                ViewBag.clsCount = classes.Count;
                ViewBag.EmpClasses =JsonConvert.SerializeObject(classes);
                var subjects = obj_cls_sub.GetEmployeeSubject(empId);
                ViewBag.subCount = subjects.Count;
                ViewBag.EmpSubjects =JsonConvert.SerializeObject(subjects);
                return View(ans);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult EditStaffEnrollment1(SaveTechEmployee1 model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    TechEmployeeServices obj_employee = new TechEmployeeServices();
                    int empRegId = Convert.ToInt32(QSCrypt.Decrypt(model.EmpId));
                    string Fname = model.txt_empname;
                    string Mname = model.txt_empMname;
                    string Lname = model.txt_emplastname;
                    DateTime DOB = model.txt_dob;
                    string Gender = model.txt_gender;
                    string Bloodgrp = model.txt_bgrp;
                    string Religion = model.txt_religion;
                    string Nationality = model.txt_nationality;
                    string Community = model.txt_community;
                    string Marital = model.txt_MaritalStatus;
                    int employeetype = model.txt_EmployeeType;
                    int empdesignation = model.txt_EmployeeDesignation;
                    string txt_ClassList1 = "";
                    string txt_SubjectList1 = "";
                    if (model.txt_ClassList1 != null && model.txt_SubjectList1 != null)
                    {
                        txt_ClassList1 = String.Join(",", model.txt_ClassList1);
                        txt_SubjectList1 = String.Join(",", model.txt_SubjectList1);
                    }
                    string licenseno = model.txt_LicenseNo;
                    DateTime? Expirdate = null;
                    if (model.txt_ExpireDate != null)
                    {
                        Expirdate = Convert.ToDateTime(model.txt_ExpireDate);
                    }
                    string Contact = model.txt_contact;
                    long Mobile = model.txt_Mobile;
                    string Email = model.txt_email;
                    string Address1 = model.txt_addressLine1;
                    string Address2 = model.txt_addressLine2;
                    string City = model.txt_city;
                    string State = model.txt_state;
                    string Country = model.Country;
                    string PIN = model.txt_pin;
                    string PermanentAddress1 = model.txt_Permanentaddress;
                    string PermanentAddress2 = model.txt_PermanentaddressLine;
                    string PermanentCity = model.txt_Permanentcity;
                    string PermanentState = model.txt_Permanentstate;
                    string PermanentCountry = model.PermanentCountry;
                    string PermanentPIN = model.txt_Permanentpin;
                    string EmgPerson = model.txt_emergencyconper;
                    long EmgContact = model.txt_econno;
                    string Relationship = model.txt_emergencyconperrelationship;
                    bool cc = obj_employee.checkEmail(empRegId, Email);
                    if (cc == true)
                    {
                        string ErrorMessage = ResourceCache.Localize("Email_is_exist ");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_employee.updateTechEmployee1(empRegId, Fname, Mname, Lname, DOB, Gender, Bloodgrp, Religion, Nationality, Community, Marital, employeetype, empdesignation, txt_ClassList1, txt_SubjectList1, Contact, Mobile, Email, Address1, Address2, City, State, Country, PIN, PermanentAddress1, PermanentAddress2, PermanentCity, PermanentState, PermanentCountry, PermanentPIN, EmgPerson, EmgContact, Relationship);
                        string Message = ResourceCache.Localize("Employee_Updated_Successfully");
                        useractivity.AddEmployeeActivityDetails("Update_employee");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ErrorMessage = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult EditStaffEnrollment(VM_EditTechEmployee model)
        {
            TechEmployeeServices obj_employee = new TechEmployeeServices();
            try
            {
                if (ModelState.IsValid)
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ErrorMessage = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult EditStaffEnrollment2()
        {
            TechEmployeeServices obj_employee = new TechEmployeeServices();
            try
            {
                if (ModelState.IsValid)
                {
                    int empRegId = Convert.ToInt32(QSCrypt.Decrypt(Request.Form["EmpId"]));
                    HttpPostedFileBase Certificate = Request.Files["txt_Certificate"];
                    int count1 = int.Parse(Request.Form["Education_count"]);
                    string Course = Request.Form["Course"];
                    string Insitute = Request.Form["Insitute"];
                    string University = Request.Form["University"];
                    string Year = Request.Form["Year"];
                    string Mark = Request.Form["Mark"];
                    string EducationalDetailsId = Request.Form["EducationalDetailsId"];
                    string Email = Request.Form["txt_email"];
                    HttpPostedFileBase Resume = Request.Files["txt_Resume"];
                    string txt_Experience = Request.Form["txt_Experience"];
                    int count3 = int.Parse(Request.Form["Employeement_count"]);
                    string EmpInst = Request.Form["EmpInst"];
                    string Designation = Request.Form["Designation"];
                    string Expertise = Request.Form["Expertise"];
                    string EmpSdate = Request.Form["EmpSdate"];
                    string EmpEdate = Request.Form["EmpEdate"];
                    string empTotal = Request.Form["empTotal"];
                    string EmployeeExperienceId = Request.Form["EmployeeExperienceId"];
                    int count2 = int.Parse(Request.Form["Professional_count"]);
                    string CourseName = Request.Form["CourseName"];
                    string CertificateName = Request.Form["CertificateName"];
                    string ProfessionalId = Request.Form["ProfessionalId"];
                    string Error= "";
                    DateTime? txt_dor = null;
                    string ss = Request.Form["txt_dor"];
                    if (ss != "")
                    {
                        
                        var checkempalreadyexist = obj_employee.checkemproll(empRegId, Currentyearid);
                        var checkclassteacher = obj_employee.checkempclass(empRegId, Currentyearid);
                        var roll = obj_employee.checkemployeeerror(empRegId);
                        if (checkempalreadyexist != null || checkclassteacher != null )
                        {
                            Error = "This employee assigned subject in this academic year.So can't give releiving";
                        }
                        if (roll != null)
                        {
                            Error = "This employee assigned to admin staff. so can't update status was false";
                        }
                       
                    }
                  if (Error!="")
                    {
                        return Json(new { ErrorMessage = Error }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                    HttpPostedFileBase[] CertificationFile = new HttpPostedFileBase[count2];
                    for (int i = 0; i < count2; i++)
                    {
                        CertificationFile[i] = Request.Files["CertificationFile[" + i + "]"];
                    }
                    HttpPostedFileBase txt_empphoto = Request.Files["txt_empphoto"];
                    HttpPostedFileBase RationCard = Request.Files["RationCard"];
                    HttpPostedFileBase VoterId = Request.Files["VoterId"];
                    DateTime txt_doj = Convert.ToDateTime(Request.Form["txt_doj"]);
                   
                    if (ss != "")
                    {
                        txt_dor = Convert.ToDateTime(Request.Form["txt_dor"]);
                    }
                    long? txt_Salary = Convert.ToInt64(Request.Form["txt_Salary"]);
                    string txt_AcNo = Request.Form["txt_AcNo"];
                    string txt_AcName = Request.Form["txt_AcName"];
                    string txt_BName = Request.Form["txt_BName"];
                    string txt_Pan_Number = Request.Form["txt_Pan_Number"];
                    int employeetype = Convert.ToInt16(Request.Form["EmployeeType"]);
                    string txt_empphoto1 = Request.Form["txt_empphoto1"];
                    string RationCard1 = Request.Form["RationCard1"];
                    string VoterId1 = Request.Form["VoterId1"];
                    string txt_Resume1 = Request.Form["txt_Resume1"];
                    string txt_Certificate1 = Request.Form["txt_Certificate1"];
                    Random r = new Random();
                    var psw = r.Next(99999);
                    string Message = "";
                    string msg = obj_employee.updateTechEmployee2(empRegId, txt_Experience, Resume, Certificate, count1, Course, Insitute, University, Year, Mark, count2, CourseName, CertificateName, CertificationFile, count3, EmpInst, Designation, Expertise, EmpSdate, EmpEdate, empTotal,
                             Email, txt_empphoto, RationCard, VoterId, txt_doj, txt_Salary, txt_AcNo, txt_AcName, txt_BName, txt_Pan_Number, EducationalDetailsId, EmployeeExperienceId, ProfessionalId, psw, txt_dor);
                    if (msg == "True")
                    {
                        var userdetails = obj_employee.EmployeeListByRegId(empRegId);
                        var empname = userdetails.EmployeeName + " " + userdetails.LastName;
                        var password = psw;
                        var empid = userdetails.EmployeeId;
                        var empemail = userdetails.Email;
                        var Dateofjoin = userdetails.DOJ;
                        StreamReader reader = new StreamReader(Server.MapPath("~/Views/EmployeeConfiguration/EmailPage.html"));
                        string readFile = reader.ReadToEnd();
                        string myString = "";
                        var link = "http://localhost:55654/#admin";
                        myString = readFile;
                        myString = myString.Replace("$$empname$$", empname.ToString());
                        myString = myString.Replace("$$empid$$", empid.ToString());
                        myString = myString.Replace("$$link$$", link.ToString());
                        myString = myString.Replace("$$username$$", empid.ToString());
                        myString = myString.Replace("$$password$$", password.ToString());
                        myString = myString.Replace("$$Dateofjoin$$", Dateofjoin.ToString());
                        string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                        string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                        string PlaceName = "EmployeeConfiguration-EditStaffEnrollment2-Edit tech employee username password details sent";
                        string UserName = "Employee";
                        string UserId = Convert.ToString(Session["username"]);


                        Mail.SendMail(EmailDisplay, supportEmail, empemail, "Account Credentials", myString.ToString(), true, PlaceName, UserName, UserId);
                        Message = ResourceCache.Localize("Employee_Details_Added_Successfully,Username_Passowrd_Has_been_Sent_To_Employee_Mail_ID");
                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Update_employee"));
                    }
                    else
                    {
                        Message = ResourceCache.Localize("Employee_Details_Successfully_update");
                    }
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ErrorMessage = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Non_TechEmployeeList()
        {
            OtherEmployeeServices obj_otherEmployee = new OtherEmployeeServices();
            try
            {
                ViewBag.OtherEmployee = obj_otherEmployee.ShowEmployees();
                var ans = obj_otherEmployee.getEmployee();
                ViewBag.count = ans.Count;
                return View(ans);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
       
        public JsonResult OtherEmpResults(string sidx, string sord, int page, int rows, int? EmployeeRegisterId)
        {
             IList<Employee> todoListsResults = new List<Employee>();
            OtherEmployeeServices obj_otheremployee = new OtherEmployeeServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults = obj_otheremployee.getEmployee();
            if (EmployeeRegisterId.HasValue)
            {
                todoListsResults = todoListsResults.Where(p => p.EmployeeId == EmployeeRegisterId).ToList();
            }
            int totalRecords = todoListsResults.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults = todoListsResults.OrderByDescending(s => s.DOJ).ToList();
                    todoListsResults = todoListsResults.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults = todoListsResults.OrderBy(s => s.DOJ).ToList();
                    todoListsResults = todoListsResults.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults;
           
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OtherEmployeeListExportToExcel()
        {
            try
            {
                List<Employee> todoListsResults = (List<Employee>)Session["JQGridList"];
                var list = todoListsResults.Select(o => new { EmployeeId = o.EmployeeId, EmployeeName = o.EmployeeName, EmployeeTypes = o.EmployeeTypes, EmployeeDesianations = o.EmployeeDesianations, Address = o.Address }).ToList();
                string fileName = "OtherEmployeeList";
                GenerateExcel.ExportExcel(list, fileName);
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult OtherEmployeeListExportToPDF()
        {
            try
            {
                List<Employee> todoListsResults = (List<Employee>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = "OtherEmployeeList";
                GeneratePDF.ExportPDF_Landscape(todoListsResults, new string[] { "EmployeeId", "EmployeeName", "EmployeeTypes", "EmployeeDesianations", "Address" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "OtherEmployeeList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult EditOtherEmployee(string id)
        {
            tblCountryServices obj_country = new tblCountryServices();
            CommunityServices obj_community = new CommunityServices();
            BloodGroupServices obj_bloodgroup = new BloodGroupServices();
            EmployeeTypeServices Emptype = new EmployeeTypeServices();
            EmployeeDesinationServices design = new EmployeeDesinationServices();
            ViewBag.Country = obj_country.CountryList();
            ViewBag.Community = obj_community.GetCommunity();
            ViewBag.BloodGroup = obj_bloodgroup.GetBloodGroup();
            ViewBag.EmployeeTypes = Emptype.GetEmployeeType();
            ViewBag.EmployeeDesignaion = design.GetEmployeeDesignation();
            try
            {
                OtherEmployeeServices obj_employee = new OtherEmployeeServices();
                TechEmployeeEducationalServices obj_edu = new TechEmployeeEducationalServices();
                TechEmployeeEmploymentServices obj_emp = new TechEmployeeEmploymentServices();
                int empId = Convert.ToInt32(QSCrypt.Decrypt(id));
                ViewBag.Empid = id;
                var ans = obj_employee.getEmployeeListById(empId);
                var ans1 = obj_edu.GetOtherEmployeeEducationalDetails(empId);
                ViewBag.ans1 = ans1;
                var ans3 = obj_emp.GetOtherEmployeeExperience(empId);
                ViewBag.ans3 = ans3;
                return View(ans);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message.ToString();
                return View();
            }
        }
        public JsonResult EditOtherStaffEnrollment1(SaveTechEmployee1 model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    TechEmployeeServices obj_employee = new TechEmployeeServices();
                    OtherEmployeeServices obj_otheremployee = new OtherEmployeeServices();
                    int empRegId = Convert.ToInt32(QSCrypt.Decrypt(model.EmpId));
                    string Fname = model.txt_empname;
                    string Mname = model.txt_empMname;
                    string Lname = model.txt_emplastname;
                    DateTime DOB = model.txt_dob;
                    string Gender = model.txt_gender;
                    string Bloodgrp = model.txt_bgrp;
                    string Religion = model.txt_religion;
                    string Nationality = model.txt_nationality;
                    string Community = model.txt_community;
                    string Marital = model.txt_MaritalStatus;
                    int employeetype = model.txt_EmployeeType;
                    int empdesignation = model.txt_EmployeeDesignation;
                    string licenseno = model.txt_LicenseNo;
                    //DateTime Expirdate = Convert.ToDateTime(model.txt_ExpireDate);
                    DateTime? Expirdate = null;
                    if (model.txt_ExpireDate != null)
                    {
                        Expirdate = Convert.ToDateTime(model.txt_ExpireDate);
                    }
                    string Contact = model.txt_contact;
                    long Mobile = model.txt_Mobile;
                    string Email = model.txt_email;
                    string Address1 = model.txt_addressLine1;
                    string Address2 = model.txt_addressLine2;
                    string City = model.txt_city;
                    string State = model.txt_state;
                    string Country = model.Country;
                    string PIN = model.txt_pin;
                    string PermanentAddress1 = model.txt_Permanentaddress;
                    string PermanentAddress2 = model.txt_PermanentaddressLine;
                    string PermanentCity = model.txt_Permanentcity;
                    string PermanentState = model.txt_Permanentstate;
                    string PermanentCountry = model.PermanentCountry;
                    string PermanentPIN = model.txt_Permanentpin;
                    string EmgPerson = model.txt_emergencyconper;
                    long EmgContact = model.txt_econno;
                    string Relationship = model.txt_emergencyconperrelationship;
                    bool cc = obj_otheremployee.checkEmail(empRegId, Email);
                    if (cc == true)
                    {
                        string ErrorMessage = ResourceCache.Localize("Email_is_exist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_otheremployee.updateOtherEmployee1(empRegId, Fname, Mname, Lname, DOB, Gender, Bloodgrp, Religion, Nationality, Community, Marital, employeetype, empdesignation, Contact, Mobile, Email, Address1, Address2, City, State, Country, PIN, PermanentAddress1, PermanentAddress2, PermanentCity, PermanentState, PermanentCountry, PermanentPIN, EmgPerson, EmgContact, Relationship, licenseno, Expirdate);
                        string Message = ResourceCache.Localize("Employee_Updated_Successfully");
                        useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Update_employee"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ErrorMessage = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult EditOtherEnrollment(VM_EditTechEmployee model)
        {
            TechEmployeeServices obj_employee = new TechEmployeeServices();
            try
            {
                if (ModelState.IsValid)
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ErrorMessage = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult EditOtherStaffEnrollment2()
        {
            TechEmployeeServices obj_employee = new TechEmployeeServices();
            OtherEmployeeServices obj_otheremployee = new OtherEmployeeServices();
            try
            {
                if (ModelState.IsValid)
                {
                    int empRegId = Convert.ToInt32(QSCrypt.Decrypt(Request.Form["EmpId"]));
                    HttpPostedFileBase Certificate = Request.Files["txt_Certificate"];
                    int count1 = int.Parse(Request.Form["Education_count"]);
                    string Course = Request.Form["Course"];
                    string Insitute = Request.Form["Insitute"];
                    string University = Request.Form["University"];
                    string Year = Request.Form["Year"];
                    string Mark = Request.Form["Mark"];
                    string EducationalDetailsId = Request.Form["EducationalDetailsId"];
                    string Email = Request.Form["txt_email"];
                    HttpPostedFileBase Resume = Request.Files["txt_Resume"];
                    string txt_Experience = Request.Form["txt_Experience"];
                    int count3 = int.Parse(Request.Form["Employeement_count"]);
                    string EmpInst = Request.Form["EmpInst"];
                    string Designation = Request.Form["Designation"];
                    string Expertise = Request.Form["Expertise"];
                    string EmpSdate = Request.Form["EmpSdate"];
                    string EmpEdate = Request.Form["EmpEdate"];
                    string empTotal = Request.Form["empTotal"];
                    string EmployeeExperienceId = Request.Form["EmployeeExperienceId"];
                    HttpPostedFileBase txt_empphoto = Request.Files["txt_empphoto"];
                    HttpPostedFileBase RationCard = Request.Files["RationCard"];
                    HttpPostedFileBase VoterId = Request.Files["VoterId"];
                    DateTime txt_doj = Convert.ToDateTime(Request.Form["txt_doj"]);
                    DateTime? txt_dor = null;
                    if (Request.Form["txt_dor"] != "")
                    {
                        txt_dor = Convert.ToDateTime(Request.Form["txt_dor"]);
                    }
                    long? txt_Salary = Convert.ToInt64(Request.Form["txt_Salary"]);
                    string txt_AcNo = Request.Form["txt_AcNo"];
                    string txt_AcName = Request.Form["txt_AcName"];
                    string txt_BName = Request.Form["txt_BName"];
                    string txt_Pan_Number = Request.Form["txt_Pan_Number"];
                    int employeetype = Convert.ToInt16(Request.Form["EmployeeType"]);
                    string txt_empphoto1 = Request.Form["txt_empphoto1"];
                    string RationCard1 = Request.Form["RationCard1"];
                    string VoterId1 = Request.Form["VoterId1"];
                    string txt_Resume1 = Request.Form["txt_Resume1"];
                    string txt_Certificate1 = Request.Form["txt_Certificate1"];
                    Random r = new Random();
                    var psw = r.Next(99999);
                    string Message = "";
                    string msg = obj_otheremployee.updateotherEmployee2(empRegId, txt_Experience, Resume, Certificate, count1, Course, Insitute, University, Year, Mark, count3, EmpInst, Designation, Expertise, EmpSdate, EmpEdate, empTotal,
                             Email, txt_empphoto, RationCard, VoterId, txt_doj, txt_Salary, txt_AcNo, txt_AcName, txt_BName, txt_Pan_Number, EducationalDetailsId, EmployeeExperienceId, psw, txt_dor);
                    Message = ResourceCache.Localize("Employee_Details_Successfully_update");
                    useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Update_employee"));
                    return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ErrorMessage = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}