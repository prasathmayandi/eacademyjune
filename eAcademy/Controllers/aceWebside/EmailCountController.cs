﻿using eAcademy.Controllers.aceWebside.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eAcademy.Controllers.aceWebside
{
    public class EmailCountController : Controller
    {
        // GET: EmailCount
        public JsonResult DailyEmail(DateTime FromDate, DateTime ToDate, string placeName)        
        {
            EmailCountServices Obj_Count = new EmailCountServices();
            
                      
             var month = FromDate.ToString("MMM");
            
            var currentMonth = FromDate.Month;
            var currentYear = FromDate.Year;


            var getDate = Obj_Count.EmailSentDate(currentYear, currentMonth);
            int dateCount = getDate.Count; 
            if (dateCount != 0)
            {
                DateTime? date1;

                ArrayList Day = new ArrayList();
                foreach (var d in getDate)
                {
                    var day = d.Date.Value;
                    var day1 = day.ToString("dd");
                    Day.Add(day1);
                }
                ArrayList Placelist = new ArrayList();
                Placelist.Add("AceConnect-Forget-App reset password mail Sent");
                Placelist.Add("EmployeeConfiguration-EmployeeEnrollment-Tech employee username password details sent");
                Placelist.Add("EmployeeConfiguration-EditStaffEnrollment2-Edit tech employee username password details sent");
                Placelist.Add("OnlineRegister-Register-User registered successfully");
                Placelist.Add("OnlineRegister-ForgetPassword-Parent reset password link sent");
                Placelist.Add("OnlineRegister-PreAdmissionEmail-Application enrolled successfully");
                Placelist.Add("StudentEntrollment-SendEmail-Primary user login detail sent");
                Placelist.Add("StudentEntrollment-Add_AssignClass-Login details sent");
                Placelist.Add("StudentEntrollment-Edit_ChangeSection-Section details sent");
                Placelist.Add("StudentEntrollment-SendEmail1-Asking confirmation to add new student");
                Placelist.Add("StudentEnrollment-PreAdmissionEmail-Application enrolled successfully");
                Placelist.Add("Home-Student_forgetpassword-Student reset password link sent");
                Placelist.Add("Home-Parent_forgetpassword-Parent reset password link sent");
                Placelist.Add("Home-Employee_forgetpassword-Employee reset password link sent");
                Placelist.Add("StudentManagement-Resend primary user login detail");
                Placelist.Add("Dashboard-Quick mail");
                Placelist.Add("DashboardParent-Quick mail");
                Placelist.Add("Communication-Message-Announcement");
                Placelist.Add("ModulesInfo-AddSubjectNotes-Subject notes ");
                Placelist.Add("ModulesInfo-AddHomework-Homework ");
                Placelist.Add("ModulesInfo-AddELearn-Online test");
                Placelist.Add("ModulesInfo-AddAssignment-Assignment");
                Placelist.Add("FeeCollection-BillReceipt-Bill receipt sent to parent");
                Placelist.Add("Admission-ViaEmail-Sent application status");
                Placelist.Add("Admission-ViaEmail-Sent fess structure");

                int PlaceCount = Placelist.Count;
                ArrayList EmailCount = new ArrayList();               
                ArrayList DayPlaceCount = new ArrayList();
                ArrayList MonthPlaceCount = new ArrayList();
                int EmailTotalCount;
                string place;
                if(placeName=="")
                {
                    EmailTotalCount = Obj_Count.TotalCount(currentYear, currentMonth);
                    for (int i = 0; i < getDate.Count; i++)
                    {
                    date1 = getDate[i].Date;
                    var EmailSentCount = Obj_Count.EmailSentSingleDate(date1);
                    EmailCount.Add(EmailSentCount.Count);                       
                    }
                }
                else
                {
                    EmailTotalCount = EmailTotalCount = Obj_Count.TotalByPlaceCount(currentYear, currentMonth, placeName);
                    for (int i = 0; i < getDate.Count; i++)
                    {
                        date1 = getDate[i].Date;
                        var EmailSentCount = Obj_Count.EmailSentSingleDateWithPlace(date1, placeName);
                        EmailCount.Add(EmailSentCount.Count);
                    }
                }
                
                for (int j = 0; j < Placelist.Count; j++)
                {
                    place = Convert.ToString(Placelist[j]);
                    var Place_Count = Obj_Count.PlaceWiseCount(currentYear, currentMonth, place);
                    DayPlaceCount.Add(Place_Count);

                }                                               
                var ErrorMessage = "";
                return Json(new { dateCount, Day, EmailCount, month, ErrorMessage, Placelist, DayPlaceCount, EmailTotalCount, PlaceCount }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var ErrorMessage = "NoDetailsFound";
                return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}