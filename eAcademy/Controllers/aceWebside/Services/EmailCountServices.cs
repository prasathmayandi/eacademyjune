﻿using eAcademy.Controllers.aceWebside.Models;
using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.Controllers.aceWebside.Services
{
    public class EmailCountServices
    {
        EacademyEntities dc = new EacademyEntities();
        public List<EmailSentDate> EmailSentDate(int currentYear, int currentMonth)
        {
           
            var List = (from a in dc.EmailNotifications
                             where a.MailSentDate.Value.Year == currentYear &&
                             a.MailSentDate.Value.Month == currentMonth
                                    
                             select new EmailSentDate
                             {                                  
                                 Date= a.SentDate
                                 

                             }).OrderBy(a => a.Date).Distinct().ToList();
            return List;
        }
        public int  TotalCount(int currentYear, int currentMonth)
        {
            int TotalmailSent;
            var List = (from a in dc.EmailNotifications
                        where a.MailSentDate.Value.Year == currentYear &&
                        a.MailSentDate.Value.Month == currentMonth

                        select new 
                        {
                            Place = a.Place

                        }).ToList();
            TotalmailSent= List.Count();
            return TotalmailSent;
        }
        public int TotalByPlaceCount(int currentYear, int currentMonth, string placekey)
        {
            int TotalmailSent;
            var List = (from a in dc.EmailNotifications
                        where a.MailSentDate.Value.Year == currentYear &&
                        a.MailSentDate.Value.Month == currentMonth && a.Place == placekey

                        select new
                        {
                            Place = a.Place

                        }).ToList();
            TotalmailSent = List.Count();
            return TotalmailSent;
        }
        public List<EmailNotification> EmailSentSingleDate(DateTime? date1)
        {
            var getPresentStudents = (from a in dc.EmailNotifications where a.SentDate == date1 select a).ToList();
            return getPresentStudents;
        }
        public List<EmailNotification> EmailSentSingleDateWithPlace(DateTime? date1,string placekey)
        {
            var getPresentStudents = (from a in dc.EmailNotifications where a.SentDate == date1 && a.Place==placekey select a).ToList();
            return getPresentStudents;
        }
        public int  PlaceWiseCount(int currentYear, int currentMonth, string PlaceKey)
        {
            int placecount;
            var PlaceList = (from a in dc.EmailNotifications
                             where a.MailSentDate.Value.Year == currentYear &&
                             a.MailSentDate.Value.Month == currentMonth &&
                             a.Place == PlaceKey
                             
                             select new EmailSentDate
                             {
                                 Date = a.SentDate


                             }).ToList();
            placecount = PlaceList.Count;
            return placecount;
            
        }


        
    }
}