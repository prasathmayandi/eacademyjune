﻿using eAcademy.DataModel;
using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class StudentAdmissionController : Controller
    {
        //
        // GET: /StudentAdmission/
        UserActivityHelper useractivity = new UserActivityHelper();
        public ActionResult UploadStudentDetails()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadStudentDetails(HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                // ExcelDataReader works with the binary Excel file, so it needs a FileStream
                // to get started. This is how we avoid dependencies on ACE or Interop:
                Stream stream = upload.InputStream;

                // We return the interface, so that
                IExcelDataReader reader = null;

                if (upload.FileName.EndsWith(".xls") || upload.FileName.EndsWith(".xlsx"))
                {
                    tblCountryServices cty = new tblCountryServices();
                    PrimaryUserRegisterServices primary = new PrimaryUserRegisterServices();
                    reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    reader.IsFirstRowAsColumnNames = true;
                    //DataTable schematable=reader.GetSchemaTable();
                    DataSet result = reader.AsDataSet();
                    CommunityServices cs = new CommunityServices();
                    ClassServices cls = new ClassServices();
                    BloodGroupServices bg = new BloodGroupServices();
                    AcademicyearServices AcServices = new AcademicyearServices();
                    AdmissionTransactionServices Admintrans = new AdmissionTransactionServices();
                    StudentServices s_student = new StudentServices();
                    FatherRegisterServices fatherservice = new FatherRegisterServices();
                    MotherRegisterServices motherservices = new MotherRegisterServices();
                    GuardianRegisterServices guardianservices = new GuardianRegisterServices();
                    tblSiblingServices s_sbling = new tblSiblingServices();
                    foreach (DataRow myField in result.Tables[0].Rows)
                    {
                        //For each property of the field...
                        foreach (DataColumn myProperty in result.Tables[0].Columns)
                        {
                            //Display the field name and value.
                            string columnname = myProperty.ColumnName + " = " + myField[myProperty].ToString();
                            
                        }                      
                    }
                    int rowcount = result.Tables[0].Rows.Count;
                    string columncount = result.Tables[0].Columns[0].ColumnName.ToString();
                    string CellValue = result.Tables[0].Rows[0][1].ToString();
                    for (int i = 1; i < rowcount;i++ )
                    {
                        string studentFirstname=result.Tables[0].Rows[i][0].ToString();
                        string StudentLastName=result.Tables[0].Rows[i][2].ToString();
                        string gender = result.Tables[0].Rows[i][3].ToString();
                        string sDOB = result.Tables[0].Rows[i][4].ToString();
                        DateTime StudentDOB = Convert.ToDateTime("01/01/1990");
                        if(sDOB!="")
                        {
                             StudentDOB = Convert.ToDateTime(sDOB);
                        }                        
                        string Academicyear = result.Tables[0].Rows[i][9].ToString();
                        string Class = result.Tables[0].Rows[i][10].ToString();
                        string primaryemail = result.Tables[0].Rows[i][18].ToString();
                        string Primaryuser = result.Tables[0].Rows[i][17].ToString();
                        int NewprimaryUserId = 0; int NewStudentRegisterId = 0; int NewFatherRegisterId = 0; int NewMotherRegisterId = 0; int NewGuardianRegisterId = 0;
                        if (studentFirstname != "" && gender != null && StudentDOB != null && primaryemail != "" && Primaryuser != "" && Academicyear != "" && Class!="")
                        {
                            /*collect primary user details */
                            PreAdmissionPrimaryUserRegister ss = new PreAdmissionPrimaryUserRegister();
                            ss.PrimaryUser=result.Tables[0].Rows[i][17].ToString();
                            if(ss.PrimaryUser == "Father")
                            {
                                ss.PrimaryUserName = result.Tables[0].Rows[i][19].ToString();
                                string mno =result.Tables[0].Rows[i][22].ToString();
                                if(mno =="")
                                {
                                    ss.PrimaryUserContactNo =null;
                                }
                                else
                                {
                                    ss.PrimaryUserContactNo = Convert.ToInt64(result.Tables[0].Rows[i][22].ToString());
                                }
                            }
                            else if(ss.PrimaryUser == "Mother")
                            {
                                ss.PrimaryUserName = result.Tables[0].Rows[i][27].ToString();
                                ss.PrimaryUserContactNo = Convert.ToInt64(result.Tables[0].Rows[i][30].ToString());
                            }
                            else
                            {
                                ss.PrimaryUserName = result.Tables[0].Rows[i][38].ToString();
                                ss.PrimaryUserContactNo = Convert.ToInt64(result.Tables[0].Rows[i][42].ToString());
                            }                            
                            ss.PrimaryUserEmail = result.Tables[0].Rows[i][18].ToString();
                            ss.CompanyAddress1 = result.Tables[0].Rows[i][50].ToString();
                            ss.CompanyAddress2 = result.Tables[0].Rows[i][51].ToString();
                            ss.CompanyCity = result.Tables[0].Rows[i][52].ToString();
                            ss.CompanyState = result.Tables[0].Rows[i][53].ToString();
                            string country = result.Tables[0].Rows[i][54].ToString();
                            int countryid = cty.getCountryId(country);
                            if (country == null)
                            {
                                ss.CompanyCountry = null;
                            }
                            else
                            {
                                ss.CompanyCountry = country;
                            }
                            string companypCode = result.Tables[0].Rows[i][55].ToString();
                                if(companypCode == "")
                                {
                                    ss.CompanyPostelcode = null;
                                }
                                else
                                {
                                    ss.CompanyPostelcode = Convert.ToInt64(companypCode);
                                }
                            string companyContact = result.Tables[0].Rows[i][49].ToString();
                            if(companyContact=="")
                            {
                                ss.CompanyContact = null;
                            }
                            else
                            {
                                ss.CompanyContact = Convert.ToInt64(companyContact);
                            }                            
                            ss.EmergencyContactPersonName = result.Tables[0].Rows[i][70].ToString();
                            ss.ContactPersonRelationship= result.Tables[0].Rows[i][71].ToString();
                            string emergencycontact = result.Tables[0].Rows[i][72].ToString();
                            if(emergencycontact =="")
                            {
                                ss.EmergencyContactNumber = null;
                            }
                            else
                            {
                                ss.EmergencyContactNumber = Convert.ToInt64(emergencycontact);
                            }
                            ss.UserCompanyname = result.Tables[0].Rows[i][48].ToString();
                            ss.WorkSameSchool = result.Tables[0].Rows[i][34].ToString();
                                string income = result.Tables[0].Rows[i][26].ToString();
                            if(income =="")
                            {
                                ss.TotalIncome = null;
                            }
                            else
                            {
                                ss.TotalIncome = Convert.ToInt64(income);
                            }
                            var emailrequired = result.Tables[0].Rows[i][85].ToString();
                            var mobilerequired = result.Tables[0].Rows[i][84].ToString();
                            if(emailrequired=="Yes"){
                                ss.EmailRequired = true;
                            }
                            else{
                                ss.EmailRequired = false;
                            }
                            if (mobilerequired=="Yes")
                            {
                                ss.SmsRequired = true;
                            }
                            else{

                                ss.SmsRequired = false;
                            }
                            //store primary user details
                             NewprimaryUserId = primary.PrimaryUserCopyfromPreAdmissiontoMaster(ss);
                            var record = cls.getClassIdByClassName(result.Tables[0].Rows[i][10].ToString());
                            var Communityid = cs.getCommunityId(result.Tables[0].Rows[i][6].ToString());
                            var bloodid = bg.GetBloodGroupId(result.Tables[0].Rows[i][11].ToString());
                            var LocalCountryId = cty.getCountryId(result.Tables[0].Rows[i][60].ToString());
                            var PercountryId = cty.getCountryId(result.Tables[0].Rows[i][68].ToString());
                            int AcademicyearId = AcServices.getAcademicyearId(result.Tables[0].Rows[i][9].ToString());
                            //get Student Details
                            PreAdmissionStudentRegister user = new PreAdmissionStudentRegister();
                            user.StuFirstName = studentFirstname;
                            user.StuMiddlename = result.Tables[0].Rows[i][1].ToString();
                            user.StuLastname = result.Tables[0].Rows[i][2].ToString();
                            user.Gender = result.Tables[0].Rows[i][3].ToString();
                            string stuDOB = result.Tables[0].Rows[i][4].ToString();
                            if(stuDOB =="")
                            {
                                user.DOB = null;
                            }
                            else
                            {
                                user.DOB = Convert.ToDateTime(stuDOB);
                            }                            
                            user.PlaceOfBirth = result.Tables[0].Rows[i][5].ToString();
                            if (Communityid == 0)
                            {
                                user.Community = null;
                            }
                            else
                            {
                                user.Community = Communityid;
                            }
                            user.Religion = result.Tables[0].Rows[i][7].ToString();
                            user.Nationality = result.Tables[0].Rows[i][8].ToString();
                            user.AdmissionClass = record.ClassId;
                            if(bloodid == 0)
                            {
                                user.BloodGroup =null;
                            }
                            else
                            {
                                user.BloodGroup = bloodid;
                            }                            
                            user.Height = result.Tables[0].Rows[i][12].ToString();
                            user.Weights = result.Tables[0].Rows[i][13].ToString();
                            user.IdentificationMark = result.Tables[0].Rows[i][14].ToString();
                            user.LocAddress1 = result.Tables[0].Rows[i][56].ToString();
                            user.LocAddress2 = result.Tables[0].Rows[i][57].ToString();
                            user.LocCity = result.Tables[0].Rows[i][58].ToString();
                            user.LocState = result.Tables[0].Rows[i][59].ToString();
                            if(countryid == 0)
                            {
                                user.LocCountry = null;
                            }
                            else
                            {
                                user.LocCountry =Convert.ToString(LocalCountryId);
                            }
                            string localPcode = result.Tables[0].Rows[i][61].ToString();
                            if(localPcode == "")
                            {
                                user.LocPostelcode = null;
                            }
                            else
                            {
                                user.LocPostelcode = Convert.ToInt64(localPcode);
                            }
                            user.Email = result.Tables[0].Rows[i][15].ToString();
                            string contact = result.Tables[0].Rows[i][16].ToString();
                            if(contact == "")
                            {
                                user.Contact =null;
                            }
                            else
                            {
                                user.Contact = Convert.ToInt64(contact);
                            }                            
                            var sameaspresent = result.Tables[0].Rows[i][63].ToString();
                            if(sameaspresent == "Yes")
                            {
                                user.PerAddress1 = result.Tables[0].Rows[i][56].ToString();
                                user.PerAddress2 = result.Tables[0].Rows[i][57].ToString();
                                user.PerCity = result.Tables[0].Rows[i][58].ToString();
                                user.PerState = result.Tables[0].Rows[i][59].ToString();
                                user.PerCountry = user.LocCountry;
                                string perpCode = result.Tables[0].Rows[i][69].ToString();
                                if (perpCode == "")
                                {
                                    user.PerPostelcode = null;
                                }
                                else
                                {
                                    user.PerPostelcode = Convert.ToInt64(perpCode);
                                }
                             
                            }
                            else if (sameaspresent == "No")
                            {
                                user.PerAddress1 = result.Tables[0].Rows[i][64].ToString();
                                user.PerAddress2 = result.Tables[0].Rows[i][65].ToString();
                                user.PerCity = result.Tables[0].Rows[i][66].ToString();
                                user.PerState = result.Tables[0].Rows[i][67].ToString();
                                if(PercountryId == null)
                                {
                                    user.PerCountry =null;
                                }
                                else
                                {
                                    user.PerCountry =Convert.ToString(PercountryId);
                                }
                                string perpCode = result.Tables[0].Rows[i][69].ToString();
                                if(perpCode == "")
                                {
                                    user.PerPostelcode = null;
                                }
                                else
                                {
                                    user.PerPostelcode = Convert.ToInt64(perpCode);
                                }
                            }
                            user.StudentPhoto = result.Tables[0].Rows[i][73].ToString();
                            user.PreSchholName = result.Tables[0].Rows[i][78].ToString();
                            var medium = result.Tables[0].Rows[i][79].ToString();
                            if (medium == "")
                            {
                                user.PreSchoolMedium = null;
                            }
                            else
                            {
                                user.PreSchoolMedium = Convert.ToInt32(medium);
                            }
                            //user.PreSchoolMedium = result.Tables[0].Rows[i][79].ToString();
                            user.PreSchoolClass = result.Tables[0].Rows[i][80].ToString();
                            user.IncomeCertificate = result.Tables[0].Rows[i][74].ToString();
                            user.BirthCertificate = result.Tables[0].Rows[i][75].ToString();
                            user.CommunityCertificate = result.Tables[0].Rows[i][76].ToString();
                            user.TransferCertificate = result.Tables[0].Rows[i][77].ToString();
                            var HostelRequired=result.Tables[0].Rows[i][82].ToString();
                            var TransportRequired= result.Tables[0].Rows[i][83].ToString();
                            if (HostelRequired == "Yes")
                            {
                                user.HostelRequired = true;
                            }
                            else
                            {
                                user.HostelRequired = false;
                            }
                            if (TransportRequired == "Yes")
                            {
                                user.TransportRequired = true;
                            }
                            else
                            {
                                user.TransportRequired = false;
                            }
                            user.StudentAdmissionId = i;
                            user.AcademicyearId = AcademicyearId;

                            string dista = result.Tables[0].Rows[i][62].ToString();
                            if (dista != "")
                            {
                                user.Distance = Convert.ToInt32(dista);
                            }
                            
                            user.Statusflag = "Accepted";
                            user.ApplicationSource ="Offline";
                            string Dateofapply = result.Tables[0].Rows[i][81].ToString();
                            if(Dateofapply =="")
                            {
                                user.DateOfApply = null;
                            }
                            else
                            {
                                user.DateOfApply = Convert.ToDateTime(Dateofapply);
                            }                            
                            user.GuardianRequried = result.Tables[0].Rows[i][37].ToString();
                            var getstudentcount = Admintrans.CheckStudentDetailsExit(NewprimaryUserId, user.StuFirstName, user.StuLastname, user.DOB);

                            //Get Father details
                            PreAdmissionFatherRegister father = new PreAdmissionFatherRegister();
                            father.FatherFirstName = result.Tables[0].Rows[i][19].ToString();
                            father.FatherLastName = result.Tables[0].Rows[i][20].ToString();
                            string faDOB = result.Tables[0].Rows[i][21].ToString();
                            if(faDOB == "")
                            {
                                father.FatherDOB = null;
                            }
                            else
                            {
                                father.FatherDOB = Convert.ToDateTime(faDOB);
                            }
                            father.FatherQualification = result.Tables[0].Rows[i][24].ToString();
                            father.FatherOccupation = result.Tables[0].Rows[i][25].ToString();
                            string fathermno = result.Tables[0].Rows[i][22].ToString();
                            if(fathermno == "")
                            {
                                father.FatherMobileNo = null;
                            }
                            else
                            {
                                father.FatherMobileNo = Convert.ToInt64(fathermno);
                            }                            
                            father.FatherEmail = result.Tables[0].Rows[i][23].ToString();
                            PreAdmissionMotherRegister mother = new PreAdmissionMotherRegister();
                            mother.MotherFirstname = result.Tables[0].Rows[i][27].ToString();
                            mother.MotherLastName = result.Tables[0].Rows[i][28].ToString();
                            string MoDOB = result.Tables[0].Rows[i][29].ToString();
                            if(MoDOB =="")
                            {
                                mother.MotherDOB =null;
                            }
                            else
                            {
                                mother.MotherDOB = Convert.ToDateTime(MoDOB);
                            }
                            mother.MotherQualification = result.Tables[0].Rows[i][32].ToString();
                            mother.MotherOccupation = result.Tables[0].Rows[i][33].ToString();
                            mother.MotherEmail = result.Tables[0].Rows[i][31].ToString();
                            string mothermno = result.Tables[0].Rows[i][30].ToString();
                            if(mothermno == "")
                            {
                                mother.MotherMobileNo = null;
                            }
                            else
                            {
                                mother.MotherMobileNo = Convert.ToInt64(mothermno);
                            }
                            
                            

                            PreAdmissionGuardianRegister guardian = new PreAdmissionGuardianRegister();
                            guardian.GuardianFirstname = result.Tables[0].Rows[i][38].ToString();
                            guardian.GuardianLastName = result.Tables[0].Rows[i][39].ToString();
                            string guDOB = result.Tables[0].Rows[i][41].ToString();
                            if(guDOB =="")
                            {
                                guardian.GuardianDOB = null;
                            }
                            else
                            {
                                guardian.GuardianDOB = Convert.ToDateTime(guDOB);
                            }
                            guardian.GuardianQualification = result.Tables[0].Rows[i][44].ToString();
                            guardian.GuardianOccupation = result.Tables[0].Rows[i][45].ToString();
                            string guardianmno = result.Tables[0].Rows[i][42].ToString();
                            if(guardianmno == "")
                            {
                                guardian.GuardianMobileNo =null;
                            }
                            else
                            {
                                guardian.GuardianMobileNo = Convert.ToInt64(guardianmno);
                            }                            
                            guardian.GuardianEmail = result.Tables[0].Rows[i][43].ToString();
                            guardian.GuardianGender = result.Tables[0].Rows[i][40].ToString();
                            guardian.GuRelationshipToChild = result.Tables[0].Rows[i][46].ToString();
                            string guincome = result.Tables[0].Rows[i][47].ToString();
                            if(guincome == "")
                            {
                                guardian.GuardianIncome = null;
                            }
                            else
                            {
                                guardian.GuardianIncome = Convert.ToInt64(guincome);
                            }                           
                            if(getstudentcount.Count == 0)
                            {
                                NewStudentRegisterId = s_student.CopyFromStudentRegToStudent(user);
                                s_student.updatestudentenrollmenttype(NewStudentRegisterId);
                                int name = user.StudentAdmissionId;
                                var path = Server.MapPath("~/Documents/" + name);
                                System.IO.Directory.CreateDirectory(path);
                              Admintrans.AddStudentPrimaryuser(NewprimaryUserId, NewStudentRegisterId);
                              if (father.FatherFirstName != "" )
                              {
                                   NewFatherRegisterId = fatherservice.FatherdetailsCopyfromPreAdmissiontoFather(father, NewprimaryUserId, NewStudentRegisterId);
                                  Admintrans.EditUpdateFatherPrimaryUserId(NewprimaryUserId, NewStudentRegisterId, NewFatherRegisterId);
                              }
                              if (mother.MotherFirstname !="" )
                              {
                                   NewMotherRegisterId = motherservices.MotherdetailsCopyfromPreAdmissiontomother(mother, NewprimaryUserId, NewStudentRegisterId);
                                 Admintrans.EditUpdateMotherPrimaryUserId(NewprimaryUserId, NewStudentRegisterId, NewMotherRegisterId);
                              }
                              if (guardian.GuardianFirstname != "" )
                              {
                                   NewGuardianRegisterId = guardianservices.GuardiandetailsCopyfromPreAdmissiontoguardian(guardian, NewprimaryUserId, NewStudentRegisterId);
                                  Admintrans.EditUpdateGuardianPrimaryUserId(NewprimaryUserId, NewStudentRegisterId, NewGuardianRegisterId);
                              }
                              s_sbling.CopysiblingfromPreAdmissiontoMaster(NewprimaryUserId, NewStudentRegisterId, NewFatherRegisterId, NewMotherRegisterId, NewGuardianRegisterId);
                            }
                            else
                            {
                                int fatherid = 0;
                                int motherid = 0; int guardianid = 0;
                                int stuid = Convert.ToInt16(getstudentcount.FirstOrDefault().StudentRegId);
                                if (getstudentcount.FirstOrDefault().StudentRegId != 0)
                                {
                                  
                                    s_student.UpdateStudentinformation(stuid, user.StuFirstName, user.StuMiddlename, user.StuLastname, user.Gender, user.DOB, user.PlaceOfBirth, user.Community, user.Religion,
                                                                       user.Nationality, user.BloodGroup, user.LocAddress1, user.LocAddress2, user.LocCity, user.LocState, user.LocCountry, user.LocPostelcode, user.Distance, user.PerAddress1,
                                                                       user.PerAddress2, user.PerCity, user.PerState, user.PerCountry, user.PerPostelcode, user.GuardianRequried, user.Height, user.Weights, user.IdentificationMark, user.Email,
                                                                       user.Contact, user.EmergencyContactPersonName, user.EmergencyContactNumber, user.ContactPersonRelationship,user.HostelRequired,user.TransportRequired);
                                   
                                }
                                var checkuser = Admintrans.GetPrimaryUser(NewprimaryUserId, stuid);
                                if (checkuser.FatherRegisterId != null)
                                {                                    
                                    fatherid = Convert.ToInt16(checkuser.FatherRegisterId);
                                    fatherservice.EditFatherDetails(fatherid, father.FatherFirstName, father.FatherLastName, father.FatherDOB, father.FatherQualification, father.FatherOccupation, father.FatherMobileNo, father.FatherEmail); //update mother details in same motherregisterid
                                }
                                else
                                {
                                    if (father.FatherFirstName != "" )
                                    {
                                        fatherid = fatherservice.EditAddFatherdetails(father.FatherFirstName, father.FatherLastName, father.FatherDOB, father.FatherQualification, father.FatherOccupation, father.FatherMobileNo, father.FatherEmail);
                                        Admintrans.UpdateFathertId(NewprimaryUserId, stuid, fatherid);
                                    }
                                }
                                if (checkuser.MotherRegisterId != null)
                                {
                                    
                                     motherid = Convert.ToInt16(checkuser.MotherRegisterId);
                                    motherservices.EditmotherDetails1(motherid, mother.MotherFirstname, mother.MotherLastName, mother.MotherDOB, mother.MotherQualification, mother.MotherOccupation, mother.MotherMobileNo, mother.MotherEmail); //update mother details in same motherregisterid
                                }
                                else
                                {
                                    if (mother.MotherFirstname != "" )
                                    {
                                        motherid = motherservices.EditAddMotherdetails(mother.MotherFirstname, mother.MotherLastName, mother.MotherDOB, mother.MotherQualification, mother.MotherOccupation, mother.MotherMobileNo, mother.MotherEmail);
                                        Admintrans.UpdateMothertId(NewprimaryUserId, stuid, motherid);
                                    }
                                }
                                if (checkuser.GuardianRegisterId != null)
                                {
                                     guardianid = Convert.ToInt16(checkuser.GuardianRegisterId);
                                    guardianservices.EditGuardianDetails(guardianid, guardian.GuardianFirstname, guardian.GuardianLastName, guardian.GuardianDOB, guardian.GuardianQualification, guardian.GuardianOccupation, guardian.GuardianMobileNo, guardian.GuardianEmail, guardian.GuardianIncome, guardian.GuRelationshipToChild, guardian.GuardianGender); //update Guardian details in same motherregisterid
                                }
                                else
                                {
                                    if (guardian.GuardianFirstname != "" )
                                    {
                                        guardianid = guardianservices.EditAddGuardiandetails(guardian.GuardianFirstname, guardian.GuardianLastName, guardian.GuardianDOB, guardian.GuardianQualification, guardian.GuardianOccupation, guardian.GuardianMobileNo, guardian.GuardianEmail, guardian.GuardianIncome, guardian.GuRelationshipToChild, guardian.GuardianGender);
                                        Admintrans.UpdateGuardiantId(NewprimaryUserId, stuid, guardianid);
                                    }
                                }

                            }
                        }
                        else{
                            ViewBag.message = ResourceCache.Localize("Upto") + (i - 1) + ResourceCache.Localize("RecordsUpdated");
                                        return View();
                        }
                    }
                    ViewBag.message = ResourceCache.Localize("Upload all student details");
                        reader.Close();
                    return View(result.Tables[0]);
                }
                else
                {
                    ModelState.AddModelError("File", ResourceCache.Localize("ThisFileFormatIsNotSupported"));
                    return View();
                }
            }
            else
            {
                ModelState.AddModelError("File", ResourceCache.Localize("PleaseUploadYourFile"));
            }
            return View();
        }

        public ActionResult UploadEmployeeDetails()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadEmployeeDetails(HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                // ExcelDataReader works with the binary Excel file, so it needs a FileStream
                // to get started. This is how we avoid dependencies on ACE or Interop:
                Stream stream = upload.InputStream;
                // We return the interface, so that
                IExcelDataReader reader = null;

                if (upload.FileName.EndsWith(".xls") || upload.FileName.EndsWith(".xlsx"))
                {
                     reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    reader.IsFirstRowAsColumnNames = true;
                    //DataTable schematable=reader.GetSchemaTable();
                    DataSet result = reader.AsDataSet();
                    CommunityServices cs = new CommunityServices();
                    ClassServices cls = new ClassServices();
                    BloodGroupServices bg = new BloodGroupServices();
                    tblCountryServices cty = new tblCountryServices();
                    EmployeeTypeServices ets = new EmployeeTypeServices();
                    EmployeeDesinationServices eds = new EmployeeDesinationServices();
                    SubjectServices subser = new SubjectServices();
                    TechEmployeeServices tech = new TechEmployeeServices();
                    OtherEmployeeServices otech = new OtherEmployeeServices();
                    foreach (DataRow myField in result.Tables[0].Rows)
                    {
                        //For each property of the field...
                        foreach (DataColumn myProperty in result.Tables[0].Columns)
                        {
                            //Display the field name and value.
                            string columnname = myProperty.ColumnName + " = " + myField[myProperty].ToString();
                            
                        }                      
                    }
                    int rowcount = result.Tables[0].Rows.Count;
                    string columncount = result.Tables[0].Columns[0].ColumnName.ToString();
                    string CellValue = result.Tables[0].Rows[0][1].ToString();

                    for (int i = 1; i < rowcount; i++)
                    {
                         string EmployeeFname=result.Tables[0].Rows[i][0].ToString();
                         string EmployeeLname = result.Tables[0].Rows[i][2].ToString();
                         string EmpDOB = result.Tables[0].Rows[i][4].ToString();
                         string EmpCategory = result.Tables[0].Rows[i][10].ToString();
                         string EmpDesignation = result.Tables[0].Rows[i][11].ToString();
                         if (EmployeeFname != "" && EmployeeLname!="" && EmpDOB !="")
                        {
                            DateTime DOB = Convert.ToDateTime(EmpDOB);
                            string Mname = result.Tables[0].Rows[i][1].ToString();
                            string Gender = result.Tables[0].Rows[i][3].ToString();
                            string bloodgrp = result.Tables[0].Rows[i][5].ToString();
                            var bloodid = bg.GetBloodGroupId(result.Tables[0].Rows[i][5].ToString());
                            string community = result.Tables[0].Rows[i][6].ToString();
                            var Communityid = cs.getCommunityId(result.Tables[0].Rows[i][6].ToString());                             
                            string Religion = result.Tables[0].Rows[i][7].ToString();
                            string nationality = result.Tables[0].Rows[i][8].ToString();
                            string MaritialStatus = result.Tables[0].Rows[i][9].ToString();
                            string EmpType = result.Tables[0].Rows[i][10].ToString();
                            int EmpTypeId = ets.GetTypeId(EmpType);
                            string EmpDesign = result.Tables[0].Rows[i][11].ToString();
                            var EmpDesignid = eds.GetEmployeeDesignationId(EmpDesign);
                            string ClassList = result.Tables[0].Rows[i][12].ToString();
                            string[] Class = ClassList.Split(',');
                              string Classid="";
                             string SubjectId="";
                            for (int k = 0; k < Class.Length; k++)
                            {
                                if (Class[k] != "")
                                {
                                    var record=cls.getClassIdByClassName(Class[k]);
                                    if (record != null)
                                    {
                                        Classid = record.ClassId.ToString() + ",";
                                    }
                                }
                            }
                            string SubjectList = result.Tables[0].Rows[i][13].ToString();
                            string[] Subject = SubjectList.Split(',');
                            for (int k = 0; k < Subject.Length; k++)
                            {
                                if (Subject[k] != "")
                                {
                                    var record = subser.getSubjectIdBySubjet(Subject[k]);
                                    if (record != null)
                                    {
                                        SubjectId = record.SubjectId.ToString() + ",";
                                    }
                                }
                            }
                            string Contact = result.Tables[0].Rows[i][14].ToString();
                            long MobileNo =Convert.ToInt64(result.Tables[0].Rows[i][15].ToString());
                            string email = result.Tables[0].Rows[i][16].ToString();
                            string PresentAddr1 = result.Tables[0].Rows[i][17].ToString(); 
                            string PreSentAddr2 = result.Tables[0].Rows[i][18].ToString();
                            string PresentCity = result.Tables[0].Rows[i][19].ToString();
                            string PresentState = result.Tables[0].Rows[i][20].ToString();
                            string PresentCountry = result.Tables[0].Rows[i][21].ToString();
                            string PresentPostelCode = result.Tables[0].Rows[i][22].ToString();
                            string Sameas = result.Tables[0].Rows[i][23].ToString();
                            string PerAddr1, PerAddr2, PerCity,PerState,PerCountry,PerPin = "";
                             if(Sameas == "yes")
                             {
                                 PerAddr1 = PresentAddr1;
                                 PerAddr2 = PreSentAddr2;
                                 PerCity =PresentCity ;
                                 PerState= PresentState ;
                                 PerCountry= PresentCountry ;
                                 PerPin= PresentPostelCode ;
                             }
                             else
                             {
                                  PerAddr1 = result.Tables[0].Rows[i][24].ToString();
                                  PerAddr2 = result.Tables[0].Rows[i][25].ToString();
                                  PerCity = result.Tables[0].Rows[i][26].ToString();
                                  PerState = result.Tables[0].Rows[i][27].ToString();
                                  PerCountry = result.Tables[0].Rows[i][28].ToString();
                                  PerPin = result.Tables[0].Rows[i][29].ToString();
                             }
                             string EmgPerson = result.Tables[0].Rows[i][30].ToString();
                             long EmgContact = Convert.ToInt64(result.Tables[0].Rows[i][32].ToString());
                             string Relationship = result.Tables[0].Rows[i][31].ToString();
                             string Certificate = result.Tables[0].Rows[i][33].ToString();
                             string Course = result.Tables[0].Rows[i][34].ToString();
                             string Insitute = result.Tables[0].Rows[i][35].ToString();
                             string University = result.Tables[0].Rows[i][36].ToString();
                             string Year = result.Tables[0].Rows[i][37].ToString();
                             string Mark = result.Tables[0].Rows[i][38].ToString();
                             string Resume = result.Tables[0].Rows[i][40].ToString();
                             string txt_Experience = result.Tables[0].Rows[i][39].ToString();                                                        
                             string CourseName = result.Tables[0].Rows[i][41].ToString();
                             string CertificateName = result.Tables[0].Rows[i][42].ToString();
                             string CertificationFile = result.Tables[0].Rows[i][43].ToString();
                             string txt_empphoto = result.Tables[0].Rows[i][46].ToString();
                             string RationCard = result.Tables[0].Rows[i][44].ToString();
                             string VoterId = result.Tables[0].Rows[i][45].ToString();
                             string doj = result.Tables[0].Rows[i][48].ToString();
                             DateTime txt_doj =Convert.ToDateTime("01/01/1990");

                             if(doj != "")
                             {
                                 txt_doj = Convert.ToDateTime(doj);
                             }
                             string EmpInst = result.Tables[0].Rows[i][55].ToString();
                             string Designation = result.Tables[0].Rows[i][56].ToString();
                             string Expertise = result.Tables[0].Rows[i][57].ToString();
                             string EmpSdate = result.Tables[0].Rows[i][58].ToString();
                             string EmpEdate = result.Tables[0].Rows[i][59].ToString();
                             string empTotal = result.Tables[0].Rows[i][60].ToString();
                             string sal = result.Tables[0].Rows[i][47].ToString();
                             long? txt_Salary=null;
                             if(sal!="")
                             {
                                 txt_Salary = Convert.ToInt64(sal);
                             }
                             
                             string txt_AcNo = result.Tables[0].Rows[i][50].ToString();
                             string txt_AcName = result.Tables[0].Rows[i][51].ToString();
                             string txt_BName = result.Tables[0].Rows[i][52].ToString();
                             string txt_Pan_Number = result.Tables[0].Rows[i][49].ToString();
                             //int employeetype = Convert.ToInt16(Request.Form["EmployeeType"]);
                             Random r = new Random();
                             var psw = r.Next(99999);
                             bool checkemailexit = tech.Add_checkEmail(email);
                             string Empid;
                             string LicenceNo = result.Tables[0].Rows[i][53].ToString();
                             string licExpdate = result.Tables[0].Rows[i][54].ToString();
                             DateTime? Licnceexpiredate = null;
                             DateTime? txt_dor = null;
                             if(licExpdate!="")
                             {
                                 Licnceexpiredate = Convert.ToDateTime(licExpdate);
                             }
                             if (checkemailexit == true)
                             {
                                 string ErrorMessage = eAcademy.Models.ResourceCache.Localize("EmployeeEmailAlreadyExist,SoUseAnotherEmail");                         
                             }
                             int count1 = 1; int count2 = 1;int  count3 = 1;
                             if (EmpTypeId == 4 || EmpTypeId == 5)
                             {
                                 int resultvalue = otech.GetEditEmployeeId(EmployeeFname, EmployeeLname, DOB);
                                 if (resultvalue == 0)
                                 {
                                     Empid = otech.addOtherEmployee(EmployeeFname, Mname, EmployeeLname, DOB, Gender, bloodgrp, Religion, nationality, community, MaritialStatus, EmpTypeId, EmpDesignid, LicenceNo, Licnceexpiredate, Contact, MobileNo, email, PresentAddr1, PreSentAddr2, PresentCity, PresentState, PresentCountry, PresentPostelCode, PerAddr1, PerAddr2, PerCity, PerState, PerCountry, PerPin, EmgPerson, EmgContact, Relationship);
                                     string msg = otech.addExcelOtherEmployee2(Empid, txt_Experience, Resume, Certificate, count1, Course, Insitute, University, Year, Mark, count3, EmpInst, Designation, Expertise, EmpSdate, EmpEdate, empTotal, email, txt_empphoto, RationCard, VoterId, txt_doj, txt_Salary, txt_AcNo, txt_AcName, txt_BName, txt_Pan_Number, psw);
                                 }
                                 else
                                 {
                                     otech.updateOtherEmployee1(resultvalue, EmployeeFname, Mname, EmployeeLname, DOB, Gender, bloodgrp, Religion, nationality, community, MaritialStatus, EmpTypeId, EmpDesignid, Contact, MobileNo, email, PresentAddr1, PreSentAddr2, PresentCity, PresentState, PresentCountry, PresentPostelCode, PerAddr1, PerAddr2, PerCity, PerState, PerCountry, PerPin, EmgPerson, EmgContact, Relationship, LicenceNo, Licnceexpiredate);                                     
                                 }
                             }
                             else
                             {
                                 int restultId = tech.GetTechEmployeeId(email);
                                 if (restultId == 0)
                                 {                                 
                                 Empid = tech.addTechEmployee1(EmployeeFname, Mname, EmployeeLname, DOB, Gender, bloodgrp, Religion, nationality, community, MaritialStatus, EmpTypeId, EmpDesignid, Classid,SubjectId, Contact, MobileNo, email, PresentAddr1,PreSentAddr2,PresentCity, PresentState,PresentCountry,PresentPostelCode,PerAddr1,PerAddr2,PerCity,PerState,PerCountry,PerPin, EmgPerson, EmgContact, Relationship);                                
                                 string msg1 = tech.addExcelTechEmployee2(Empid, txt_Experience, Resume, Certificate, count1, Course, Insitute, University, Year, Mark, count2, CourseName, CertificateName, CertificationFile, count3, EmpInst, Designation, Expertise, EmpSdate, EmpEdate, empTotal, email, txt_empphoto, RationCard, VoterId, txt_doj, txt_Salary, txt_AcNo, txt_AcName, txt_BName, txt_Pan_Number, psw);                                                                  
                                 useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Addemployee"));
                                 if (msg1 == "True")
                                 {
                                     var userdetails = tech.GetTechEmployeeDetails(Empid);
                                     var empname = userdetails.EmployeeName + " " + userdetails.LastName;
                                     var password = psw;
                                     var empid = userdetails.EmployeeId;
                                     var empemail = userdetails.Email;
                                     var Dateofjoin = userdetails.DOJ;
                                     StreamReader reader1 = new StreamReader(Server.MapPath("~/Views/EmployeeConfiguration/EmailPage.html"));
                                     string readFile = reader1.ReadToEnd();
                                     string myString = "";
                                     var link = "http://localhost:55654/#admin";
                                     myString = readFile;
                                     myString = myString.Replace("$$empname$$", empname.ToString());
                                     myString = myString.Replace("$$empid$$", empid.ToString());
                                     myString = myString.Replace("$$link$$", link.ToString());
                                     myString = myString.Replace("$$username$$", empid.ToString());
                                     myString = myString.Replace("$$password$$", password.ToString());
                                     myString = myString.Replace("$$Dateofjoin$$", Dateofjoin.ToString());
                                     string supportEmail = ConfigurationManager.AppSettings["supportEmail"];
                                     string EmailDisplay = ConfigurationManager.AppSettings["EmailFromDisplayName"];
                                     string PlaceName = "StudentAdmissionController-Excel Upload-Employee Username Password Sent Mail";
                                     string UserName = "Employee";
                                     string UserId = Convert.ToString(Session["username"]);

                                     Mail.SendMail(EmailDisplay, supportEmail, empemail, "Account Credentials", myString.ToString(), true, PlaceName, UserName, UserId);  

                                     string Message1 = ResourceCache.Localize("Employee_Details_Added_Successfully,Username_Passowrd_Has_been_Sent_To_Employee_Mail_ID");
                                     useractivity.AddEmployeeActivityDetails(ResourceCache.Localize("Addemployee"));
                                     
                                 }
                             }
                                 else
                                 {
                                     tech.updateTechEmployee1(restultId, EmployeeFname, Mname, EmployeeLname, DOB, Gender, bloodgrp, Religion, nationality, community, MaritialStatus, EmpTypeId, EmpDesignid, Classid, SubjectId, Contact, MobileNo, email, PresentAddr1, PreSentAddr2, PresentCity, PresentState, PresentCountry, PresentPostelCode, PerAddr1, PerAddr2, PerCity, PerState, PerCountry, PerPin, EmgPerson, EmgContact, Relationship);
                                 }
                                
                             }
                        }
                        else{
                            ViewBag.message = ResourceCache.Localize("Upto") + (i - 1) + ResourceCache.Localize("RecordsUpdated");
                           // "Upto" + (i-1) + "records updated ";
                                        return View();
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("File", ResourceCache.Localize("ThisFileFormatIsNotSupported"));
                    return View();
                }

            }
            else
            {
                ModelState.AddModelError("File", ResourceCache.Localize("PleaseUploadYourFile"));
                return View();
            }
            ViewBag.message = ResourceCache.Localize("UploadAllEmployeeDetails");
            return View();
        }
    }
}
