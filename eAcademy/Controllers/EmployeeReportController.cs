﻿using eAcademy.HelperClass;
using eAcademy.Models;
using eAcademy.Services;
using eAcademy.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class EmployeeReportController : Controller
    {
        // GET: /EmployeeReport/
        
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            AcademicyearServices As = new AcademicyearServices();
            ClassServices Cs = new ClassServices();
            ExamServices Es = new ExamServices();
            MonthsServices Ms = new MonthsServices();
            TechEmployeeServices tes = new TechEmployeeServices();
            EmployeeTypeServices emptype = new EmployeeTypeServices();
            ViewBag.allTechFaculties = tes.ShowTechFaculties();
            List<SelectListItem> Sort = new List<SelectListItem>();
            Sort.Add(new SelectListItem { Text = "Select Status", Value = "" });
            Sort.Add(new SelectListItem { Text = "Both", Value = "All" });
            Sort.Add(new SelectListItem { Text = "Pass", Value = "Pass" });
            Sort.Add(new SelectListItem { Text = "Fail", Value = "fail" });
            ViewBag.SortList = Sort;
            ViewBag.academicyear = As.GetAcademicYear();
            ViewBag.Classes = Cs.GetClass();
            ViewBag.emptype = getEmployeetype();
            ViewBag.reporttype = getrtype();
            ViewBag.EmployeeTypes = emptype.GetEmployeeType();
            ViewBag.month = Ms.GetMonths();
            ViewBag.exam = Es.GetExamName();
            ViewBag.date = DateTimeByZone.getCurrentDateTime();
        }
        public IEnumerable getrtype()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = eAcademy.Models.ResourceCache.Localize("Select_Report_Type"), Value = "" });
            li.Add(new SelectListItem { Text = eAcademy.Models.ResourceCache.Localize("Daily"), Value = "1" });
            //li.Add(new SelectListItem { Text = "From/To", Value = "2" });
            li.Add(new SelectListItem { Text = eAcademy.Models.ResourceCache.Localize("Monthly"), Value = "3" });
            return li;
        }
        public IEnumerable getEmployeetype()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select EmployeeType", Value = "" });
            li.Add(new SelectListItem { Text = "Technical", Value = "1" });
            li.Add(new SelectListItem { Text = "Others", Value = "2" });
            return li;
        }
        public ActionResult EmployeeProfile()
        {
            return View();
        }
        [HttpPost]
        public ActionResult EmployeeProfile(Mandatory m)
        {
            TechEmployeeServices tes = new TechEmployeeServices();
            OtherEmployeeServices oes = new OtherEmployeeServices();
            try
            {
                if (ModelState.IsValid)
                {
                    TempData["show"] = "show";
                    ViewBag.emptype = getEmployeetype();
                    var epmtype = Convert.ToInt32(m.EmployeeType);
                    ViewBag.eid = epmtype;
                    var id = Convert.ToInt32(m.EmployeeName);
                    ViewBag.id = id;
                    if (epmtype == 1)
                    {
                        ViewBag.empname = tes.GetTechEmployee();
                        ViewBag.name = tes.GetParticularTechEmployeeName(id);
                    }
                    else if (epmtype == 2)
                    {
                        ViewBag.empname = oes.GetOtherEmployee();
                        ViewBag.name = oes.GetParticularOtherEmployeeName(id);
                    }
                    if (epmtype == 1)
                    {
                        ViewBag.profession = "Technical-Staff";
                        ViewBag.empid = tes.GetTechEmployee();
                        var ans1 = tes.SearchTechEmployeeProfile(id);
                        TempData["ans"] = ans1;
                        ViewBag.view = ans1.Count;
                    }
                    else if (epmtype == 2)
                    {
                        ViewBag.profession = "Non-Technical-Staff";
                        ViewBag.empid = oes.GetOtherEmployee();
                        var ans1 = oes.SearchOtherEmployeeProfile(id);
                        TempData["ans"] = ans1;
                        ViewBag.view = ans1.Count;
                    }
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        [HttpPost]
        public JsonResult jsonCheckEmployee(VM_EmployeeProfileValidation s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string success = "Success";
                    return Json(new { Success = success }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult JsonTechnicalEmployeeProfile(VM_EmployeeProfileValidation s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    TechEmployeeServices tes = new TechEmployeeServices();
                    TechEmployeeEducationalServices TechEdcational = new TechEmployeeEducationalServices();
                    TechEmployeeEmploymentServices TechEmployment = new TechEmployeeEmploymentServices();
                    TechEmployeeClassSubjectServices TechClassSubject = new TechEmployeeClassSubjectServices();
                    int Employeeid = s.EmployeeId;
                    var Profile = tes.SearchTechEmployeeProfile(Employeeid);
                    var educational = TechEdcational.GetEmployeeEducationalDetails(Employeeid);
                    var experience = TechEmployment.GetEmployeeExperience(Employeeid);
                    var ClassSubject = TechClassSubject.GetEmployeeClassSubject(Employeeid);
                    return Json(new { Profile = Profile, educational = educational, experience = experience, ClassSubject = ClassSubject }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult JsonOtherEmployeeProfile(VM_EmployeeProfileValidation s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    OtherEmployeeServices oes = new OtherEmployeeServices();
                    TechEmployeeEducationalServices TechEdcational = new TechEmployeeEducationalServices();
                    TechEmployeeEmploymentServices TechEmployment = new TechEmployeeEmploymentServices();
                    int Employeeid = s.EmployeeId;
                    var Profile = oes.SearchOtherEmployeeProfile(Employeeid);
                    var educational = TechEdcational.GetOtherEmployeeEducationalDetails(Employeeid);
                    var experience = TechEmployment.GetOtherEmployeeExperience(Employeeid);
                    return Json(new { Profile = Profile, educational = educational, experience = experience }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CustomeActiveDate(int acid)
        {
            AcademicyearServices AcYear = new AcademicyearServices();
            var ans = AcYear.date(acid);
            string StartingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            string EndingEnableDate = DateTimeByZone.getCurrentDate().ToString("dd/MM/yyyy");
            EventServices holidays = new EventServices();
            var HolidaysList = holidays.getHolidaysList(acid);
            DateTime CurrentDate = DateTimeByZone.getCurrentDate();
            DateTime sdate = ans.StartDate;
            DateTime edate = ans.EndDate;
            if (sdate <= CurrentDate && CurrentDate <= edate)
            {
                StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                StartingEnableDate = sdate.ToString("dd/MM/yyyy");
                EndingEnableDate = edate.ToString("dd/MM/yyyy");
                return Json(new { sd = StartingEnableDate, ed = EndingEnableDate, HolidaysList }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult TeacherTimeTable()
        {
            return View();
        }
        [HttpPost]
        public ActionResult TeacherTimeTable(Mandatory m)
        {
            AcademicyearServices As = new AcademicyearServices();
            TechEmployeeServices tes = new TechEmployeeServices();
            AssignFacultyToSubjectServices afs = new AssignFacultyToSubjectServices();
            SchoolSettingsServices sss = new SchoolSettingsServices();
            try
            {
                if (ModelState.IsValid)
                {
                    ArrayList Dayorder = new ArrayList();
                    ArrayList day = new ArrayList();
                    ArrayList period = new ArrayList();
                    ArrayList subject = new ArrayList();
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    var yid = Convert.ToInt32(m.Academicyear1);
                    ViewBag.y = yid;
                    var id = Convert.ToInt32(m.EmpName1);
                    ViewBag.id = id;
                    var emp = tes.GetTechEmployeename(yid);
                    ViewBag.empname = emp;
                    ViewBag.empid = tes.GetTechEmployeenameId(id);
                    ViewBag.name = tes.GetParticularTechEmployeeName(id);
                    ViewBag.yy = As.GetParticularYear(yid);
                    var ans1 = afs.GetTimeTable(yid, id);
                    foreach (var v in ans1)
                    {
                        int d = Convert.ToInt32(v.day);
                        int p = Convert.ToInt32(v.period);
                        string s = v.subject;
                        if (!Dayorder.Contains(d))
                        { Dayorder.Add(d); }
                        day.Add(d);
                        period.Add(p);
                        subject.Add(s);
                    }
                    ViewBag.period = period;
                    ViewBag.day = day;
                    ViewBag.subject = subject;
                    ViewBag.profession = "Technical-Staff";
                    TempData["ans"] = ans1;
                    ViewBag.view = ans1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        [HttpPost]
        public JsonResult JsonTeacherTimeTable(VM_TeacherTimetable s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AssignFacultyToSubjectServices afs = new AssignFacultyToSubjectServices();
                    TimeScheduleServices tss = new TimeScheduleServices();
                    SchoolSettingsServices sss = new SchoolSettingsServices();
                    ArrayList classid = new ArrayList();
                    int year = s.AcademicYear;
                    int eid = s.EmployeeId;
                    AssignSubjectToPeriodServices SubjectPeriod = new AssignSubjectToPeriodServices();
                    var ans1 = SubjectPeriod.getFacultyPeriodSubject(year, eid);
                    //var ans1 = afs.GetTimeTable(year, eid);
                    var dayorder = sss.GetSchoolDayOrder();
                    var DayOrder_Count = dayorder.DayOrder;
                    var getGreaterClass = afs.GetgreaterClass(year, eid);
                    var periods = tss.ClassPeriodCount(getGreaterClass);
                    return Json(new { Profile = ans1, DayOrder = DayOrder_Count, Periods = periods }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EmployeeAttendance()
        {
            return View();
        }
        [HttpPost]
        public JsonResult JsonEmployeeAttendance(VM_EmployeeAttendanceValidation s)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    FacultyDailyAttendanceServices fdas = new FacultyDailyAttendanceServices();
                    EmployeeDailyAttendanceServices edas = new EmployeeDailyAttendanceServices();
                    var rid = Convert.ToInt32(s.ReportType);
                    var emptype = Convert.ToInt16(s.EmployeeType);
                    AcademicyearServices acyear = new AcademicyearServices();
                    if (rid == 1)
                    {
                        if (emptype >= 1 && emptype <= 3)
                        {
                            var ans1 = fdas.GetTechEmployeeDailyAttendance(s.AcademiYear, s.Dates, s.EmployeeDesignantion);
                            return Json(new { AttendanceList = ans1 }, JsonRequestBehavior.AllowGet);
                        }
                        else if (emptype >= 4 && emptype <= 5)
                        {
                            var ans1 = edas.GetOtherEmployeeDailyAttendance(s.AcademiYear, s.Dates, s.EmployeeDesignantion);
                            return Json(new { AttendanceList = ans1 }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else if (rid == 2)
                    {
                        if (emptype >= 1 && emptype <= 3)
                        {
                            var ans1 = fdas.GetTechEmployeeFromToAttendance(s.AcademiYear, s.FromDates, s.Todates, s.EmployeeDesignantion);
                            string[] roll = new string[100];
                            string[] s1 = new string[100];
                            int Roll_Count = 0;
                            int date_Count = 0;
                            foreach (var v in ans1)
                            {
                                string r = v.rollnumber;
                                string d = v.date.Day.ToString();
                                string na = v.stuname;
                                if (!roll.Contains(r))
                                {
                                    roll[Roll_Count] = r;
                                    Roll_Count++;
                                }
                                if (!s1.Contains(d))
                                {
                                    s1[date_Count] = d;
                                    date_Count++;
                                }
                            }
                            return Json(new { AttendanceList = ans1, rollNumber = roll, Attendancedates = s1, Roll_Count = Roll_Count, date_Count = date_Count }, JsonRequestBehavior.AllowGet);
                        }
                        else if (emptype >= 4 && emptype <= 5)
                        {
                            var ans1 = edas.GetOtherEmployeeFromToAttendance(s.AcademiYear, s.FromDates, s.Todates, s.EmployeeDesignantion);
                            string[] roll = new string[100];
                            string[] s1 = new string[100];
                            int Roll_Count = 0;
                            int date_Count = 0;
                            foreach (var v in ans1)
                            {
                                string r = v.rollnumber;
                                string d = v.date.Day.ToString();
                                string na = v.stuname;
                                if (!roll.Contains(r))
                                {
                                    roll[Roll_Count] = r;
                                    Roll_Count++;
                                }
                                if (!s1.Contains(d))
                                {
                                    s1[date_Count] = d;
                                    date_Count++;
                                }
                            }
                            return Json(new { AttendanceList = ans1, rollNumber = roll, Attendancedates = s1, Roll_Count = Roll_Count, date_Count = date_Count }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else if (rid == 3)
                    {
                        if (emptype >= 1 && emptype <= 3)
                        {
                            var ans1 = fdas.GetTechEmployeeMonthAttendance(s.AcademiYear, s.month, s.EmployeeDesignantion);
                            string[] roll = new string[100];
                            string[] s1 = new string[100];
                            int Roll_Count = 0;                            
                            foreach (var v in ans1)
                            {
                                string r = v.rollnumber;
                                string d = v.date.Day.ToString();
                                string na = v.stuname;
                                if (!roll.Contains(r))
                                {
                                    roll[Roll_Count] = r;
                                    Roll_Count++;
                                }                                
                            }
                            int y = acyear.GetmonthDates(Convert.ToInt16(s.month), s.AcademiYear);
                            int date_Count = DateTime.DaysInMonth(y, Convert.ToInt16(s.month));

                            for (int i = 0; i < date_Count; i++)
                            {
                                string date = s.month + "/" + (i + 1) + "/" + y;
                                DateTime dateValue = DateTime.Parse(date, CultureInfo.InvariantCulture);
                                DateTimeOffset dateOffsetValue = new DateTimeOffset(dateValue,
                                         TimeZoneInfo.Local.GetUtcOffset(dateValue));
                                string ss;
                                ss = dateValue.ToString("ddd");
                                s1[i] = ss.Substring(0, 2);
                            }            
                            return Json(new { AttendanceList = ans1, rollNumber = roll, Attendancedates = s1, Roll_Count = Roll_Count, date_Count = date_Count }, JsonRequestBehavior.AllowGet);
                        }
                        else if (emptype >= 4 && emptype <= 5)
                        {
                            var ans1 = edas.GetOtherEmployeeMonthAttendance(s.AcademiYear, s.month, s.EmployeeDesignantion);
                            string[] roll = new string[100];
                            string[] s1 = new string[100];
                            int Roll_Count = 0;                            
                            foreach (var v in ans1)
                            {
                                string r = v.rollnumber;
                                string d = v.date.Day.ToString();
                                string na = v.stuname;
                                if (!roll.Contains(r))
                                {
                                    roll[Roll_Count] = r;
                                    Roll_Count++;
                                }
                                                         }
                            int y = acyear.GetmonthDates(Convert.ToInt16(s.month), s.AcademiYear);
                            int date_Count = DateTime.DaysInMonth(y, Convert.ToInt16(s.month));

                            for (int i = 0; i < date_Count; i++)
                            {
                                string date = s.month + "/" + (i + 1) + "/" + y;
                                DateTime dateValue = DateTime.Parse(date, CultureInfo.InvariantCulture);
                                DateTimeOffset dateOffsetValue = new DateTimeOffset(dateValue,
                                         TimeZoneInfo.Local.GetUtcOffset(dateValue));
                                string ss;
                                ss = dateValue.ToString("ddd");
                                s1[i] = ss.Substring(0, 2);
                            }       
                            return Json(new { AttendanceList = ans1, rollNumber = roll, Attendancedates = s1, Roll_Count = Roll_Count, date_Count = date_Count }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    string Success = "";
                    return Json(new { Success = Success }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                String ErrorMessage = e.Message;
                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EmployeeLeaves()
        {
            return View();
        }
        [HttpPost]
        public ActionResult EmployeeLeaves(Mandatory m, FormCollection fm)
        {
            AcademicyearServices As = new AcademicyearServices();
            FacultyleaverequestServices flrs = new FacultyleaverequestServices();
            try
            {
                if (ModelState.IsValid)
                {
                    TempData["show"] = "show";
                    ViewBag.academicyear = As.GetAcademicYear();
                    var yid = Convert.ToInt32(m.AcademicYear);
                    ViewBag.y = yid;
                    DateTime from1 = Convert.ToDateTime(fm["Fromdate"]);
                    DateTime to = Convert.ToDateTime(fm["Todate"]);
                    ViewBag.from1 = from1.ToShortDateString();
                    ViewBag.to1 = to.ToShortDateString();
                    ViewBag.yy = As.GetParticularYear(yid);
                    var ans1 = flrs.GetEmployeeLeave(yid, from1, to);
                    TempData["ans"] = ans1;
                    ViewBag.view = ans1.Count;
                }
                else
                {
                    ViewBag.modelState = "EacademyEntities";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message.ToString();
                return View("Error");
            }
            var ans = TempData["ans"];
            return View(ans);
        }
        public JsonResult JQgridEmployeeLeaveRequest(string sidx, string sord, int page, int rows, DateTime? Fromdate, DateTime? Todate)//, DateTime Fromdate, DateTime Todate
        {
            IList<M_EmployeeLeaveRequest> EmployeeLeaveRequestList = new List<M_EmployeeLeaveRequest>();
            FacultyleaverequestServices flrs = new FacultyleaverequestServices();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (Fromdate != null && Todate != null)
            {
                EmployeeLeaveRequestList = flrs.GetEmployeeLeaveRequest(Fromdate, Todate);
            }
            int totalRecords = EmployeeLeaveRequestList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    EmployeeLeaveRequestList = EmployeeLeaveRequestList.OrderByDescending(s => s.EmployeeId).ToList();
                    EmployeeLeaveRequestList = EmployeeLeaveRequestList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    EmployeeLeaveRequestList = EmployeeLeaveRequestList.OrderBy(s => s.EmployeeId).ToList();
                    EmployeeLeaveRequestList = EmployeeLeaveRequestList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = EmployeeLeaveRequestList;
           
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = EmployeeLeaveRequestList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EmployeeLeaveRequestReport_ExportToExcel()
        {
            try
            {
                List<M_EmployeeLeaveRequest> EmployeeLeaveRequestList = (List<M_EmployeeLeaveRequest>)Session["JQGridList"];
                var list = EmployeeLeaveRequestList.Select(o => new { EmployeeId = o.EmployeeId, EmployeeName = o.EmployeeName, FromDate = o.FromDate, ToDate = o.ToDate, TotalLeave = o.TotalLeave, status = o.status }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("EmployeeLeaveRequestReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("EmployeeLeaves");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult EmployeeLeaveRequestReport_ExportToPdf()
        {
            try
            {
                List<M_EmployeeLeaveRequest> EmployeeLeaveRequestList = (List<M_EmployeeLeaveRequest>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("EmployeeLeaveRequestReport");
                GeneratePDF.ExportPDF_Portrait(EmployeeLeaveRequestList, new string[] { "EmployeeId", "EmployeeName", "FromDate", "ToDate", "TotalLeave", "status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "EmployeeLeaveRequestList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult FacultyFeedback()
        {
            return View();
        }
        public JsonResult JQgridFacultyFeedback1(string sidx, string sord, int page, int rows, int? R_Feedback_FacultyName, DateTime? R_Feedback_FromDate, DateTime? R_Feedback_ToDate)//, DateTime Fromdate, DateTime Todate , DateTime? R_Feedback_FromDate, DateTime? R_Feedback_ToDate , int R_Feedback_FacultyName
        {
             IList<R_FacultyFeedback> FacultyFeedbackList = new List<R_FacultyFeedback>();
            StudentFacultyFeedback_service ffbs = new StudentFacultyFeedback_service();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            if (R_Feedback_FromDate != null && R_Feedback_ToDate != null && R_Feedback_FacultyName != 0)
            {
                FacultyFeedbackList = ffbs.getFaculyFeedbackList(R_Feedback_FacultyName, R_Feedback_FromDate, R_Feedback_ToDate);
            }
            int totalRecords = FacultyFeedbackList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    FacultyFeedbackList = FacultyFeedbackList.OrderByDescending(s => s.FacultyId).ToList();
                    FacultyFeedbackList = FacultyFeedbackList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    FacultyFeedbackList = FacultyFeedbackList.OrderBy(s => s.FacultyId).ToList();
                    FacultyFeedbackList = FacultyFeedbackList.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = FacultyFeedbackList;
           
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = FacultyFeedbackList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FacultyFeedbackReport_ExportToExcel()
        {
            try
            {
                List<R_FacultyFeedback> FacultyFeedbackList = (List<R_FacultyFeedback>)Session["JQGridList"];
                var list = FacultyFeedbackList.Select(o => new { Date = o.Date, Feedback = o.Feedback, UserName = o.UserName, UserType = o.UserType, SubjectName = o.SubjectName }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("FacultyFeedbackReport");
                GenerateExcel.ExportExcel(list, fileName);
                return View("EmployeeLeaves");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult FacultyFeedbackReport_ExportToPdf()
        {
            try
            {
                List<R_FacultyFeedback> FacultyFeedbackList = (List<R_FacultyFeedback>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("FacultyFeedbackReport");
                GeneratePDF.ExportPDF_Portrait(FacultyFeedbackList, new string[] { "Date", "Feedback", "UserName", "UserType", "SubjectName" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "FacultyFeedBackList.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
    }
}