﻿using eAcademy.Services;
using System.Web.Mvc;

namespace eAcademy.Controllers
{
    [StaffCheckSessionAttribute]
    public class ReportCenterController : Controller
    {
        public ActionResult Reports()
        {
            return View();
        }
    }
}
