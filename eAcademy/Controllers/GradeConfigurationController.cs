﻿using eAcademy.HelperClass;
using eAcademy.Helpers;
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
namespace eAcademy.Controllers
{
    [CheckSessionOutAttribute]
    public class GradeConfigurationController : Controller
    {
        AcademicyearServices obj_academicYear = new AcademicyearServices();
        ClassServices obj_class = new ClassServices();
        GradeTypeServices obj_gradeType = new GradeTypeServices();
        GradeServices obj_grade = new GradeServices();
        AssignGradeTypeToClassServices obj_gradeTypetoClass = new AssignGradeTypeToClassServices();
        UserActivityHelper useractivity = new UserActivityHelper();
        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            ViewBag.acYear = obj_academicYear.AllList_AcademicYear();
            ViewBag.AddacYear = obj_academicYear.Add_formAcademicYear();
            ViewBag.GradeType = obj_gradeType.getGradeType();
            ViewBag.ClassList = obj_class.Classes();
            ViewBag.class_Fees = obj_class.Classes();
        }
        public ActionResult GradeType()
        {
            try
            {
                ViewBag.EditList = TempData["EditList"];
                if (ViewBag.EditList != null)
                {
                    var ans = TempData["EditList"];
                    return View(ans);
                }
                else
                {
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
                return View();
            }
        }
        
        public JsonResult GradeTypeResult(string sidx, string sord, int page, int rows, int? dd_GradeType)
        {
           IList<GradeTypeList> todoListsResults4 = new List<GradeTypeList>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults4 = obj_gradeType.getGradeTypeList();
            if (dd_GradeType.HasValue)
            {
                todoListsResults4 = todoListsResults4.Where(p => p.GradeTypeId == dd_GradeType).ToList();
            }
            int totalRecords = todoListsResults4.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults4 = todoListsResults4.OrderByDescending(s => s.GradeType).ToList();
                    todoListsResults4 = todoListsResults4.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults4 = todoListsResults4.OrderBy(s => s.GradeType).ToList();
                    todoListsResults4 = todoListsResults4.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults4;
           
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults4
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GradetypeExportToExcel()
        {
            try
            {
                List<GradeTypeList> todoListsResults4 = (List<GradeTypeList>)Session["JQGridList"];
                var list = todoListsResults4.Select(o => new { GradeType = o.GradeType, Status = o.Status.Value.ToString() }).ToList();
                string fileName = "GradeType";
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult GradetypeExportToPDF()
        {
            try
            {
                List<GradeTypeList> todoListsResults4 = (List<GradeTypeList>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("GradeType");
                GeneratePDF.ExportPDF_Portrait(todoListsResults4, new string[] { "GradeType", "Status" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "GradeType.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        [HttpPost]
        public JsonResult addGradeDetails(GradeClass frm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string gradeType = frm.txt_GradeType;
                    int count = frm.count;
                    string[] grade = new string[count];
                    int[] min = new int[count];
                    int[] max = new int[count];
                    string[] desc = new string[count];
                    string[] gradename = frm.gradename;
                    for (int i = 0; i < count; i++)
                    {
                        for (int j = i + 1; j < count; j++)
                        {
                            if (gradename[i].Equals(gradename[j]))
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("PleasedonotrepeatsameGradeName");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    bool cc = obj_gradeType.checkGradeType(gradeType);
                    if (cc == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("GradeAlreadyExist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var gtypeid = obj_gradeType.addGradeType(gradeType);
                        var ans = obj_gradeType.getGradeType();
                        for (int i = 0; i < count; i++)
                        {
                            grade[i] = String.Join(" ", frm.gradename[i]);
                            desc[i] = String.Join(" ", frm.des[i]);
                            min[i] = Convert.ToInt32(String.Join(" ", frm.min[i]));
                            max[i] = Convert.ToInt32(String.Join(" ", frm.max[i]));
                            obj_grade.addgrade(gtypeid, grade[i], min[i], max[i], desc[i]);
                        }
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_grade_type_and_mark"));
                        string Message = eAcademy.Models.ResourceCache.Localize("Grade_type_and_mark_added_successfully");
                        return Json(new { ans = ans, Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                         {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }

            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GradeDetails(string id)
        {
            try
            {
                int gradeTypeId = Convert.ToInt32(QSCrypt.Decrypt(id));
                var ans = obj_grade.getGradeDetails(gradeTypeId);
                var ans1 = obj_grade.getGrade(gradeTypeId);
                int count = ans1.Count;
                string GradeTypeName = obj_grade.Getgradesystemdetails(gradeTypeId).GradeTypeName;
                bool? Status = obj_grade.Getgradesystemdetails(gradeTypeId).GradeTypeStatus;
                return Json(new { count, ans, GradeTypeName, gradeTypeId, Status }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getGradeDetails(int gradeTypeId)
        {
            try
            {
                var ans = obj_grade.getGrade(gradeTypeId);
                int count = ans.Count;
                return Json(count, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getGradelist1(string gradeTypeId)
        {
            try
            {
                int id = Convert.ToInt32(QSCrypt.Decrypt(gradeTypeId));
                var ans = obj_grade.getGradeDetails(id);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getGradelist(int gradeTypeId)
        {
            try
            {
                var ans = obj_grade.getGradeDetails(gradeTypeId);
                return Json(ans, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EditGrade(int gid)
        {
            try
            {
                TempData["EditId"] = gid;
                var ans = obj_grade.getGrade(gid);
                TempData["EditList"] = ans;
                TempData["EditListCount"] = ans.Count;
                return RedirectToAction("GradeType");
            }
            catch (Exception e)
            {
                TempData["ErrorMessage"] = e.Message;
                return RedirectToAction("GradeType");
            }
        }
        public JsonResult SaveEditGrade(EditGradeClass frm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int count = frm.count;
                    string[] g = new string[count];
                    string[] desc = new string[count];
                    int[] m1 = new int[count];
                    int[] m2 = new int[count];
                    int?[] gid = new int?[count];
                    int gradetypeid = frm.grdesystemid;
                    string Gradetypename = frm.gradesystemname;
                    string[] updategradeType = frm.gradename;
                    bool Status = frm.Status;

                    for (int i = 0; i < count; i++)
                    {
                        for (int j = i + 1; j < count; j++)
                        {
                            if (frm.gradename[i].Equals(frm.gradename[j]))
                            {
                                string ErrorMessage = eAcademy.Models.ResourceCache.Localize("PleasedonotrepeatsameGradeName");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    bool check = obj_gradeType.checkGradeTypeexist(gradetypeid, Gradetypename);

                    if (check == true)
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("GradeAlreadyExist");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        obj_gradeType.UpdateGradeType(gradetypeid, Gradetypename, Status);
                        for (int i = 0; i < count; i++)
                        {
                            string g1 = frm.gradename[i];
                            string desc1 = frm.des[i];
                            int min1 = Convert.ToInt32(frm.min[i]);
                            int max1 = Convert.ToInt32(frm.max[i]);
                            int Gid = Convert.ToInt32(frm.gid[i]);

                            if (frm.gid[i] == null)
                            {

                                obj_grade.addgrade(gradetypeid, g1, min1, max1, desc1);
                            }
                            else
                            {
                                obj_grade.updateGrade(Gid, g1, min1, max1, desc1);
                            }
                        }
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Update_grade_type_and_mark"));
                        string Message = eAcademy.Models.ResourceCache.Localize("Grade_type_and_mark_updated_successfully");
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }

            }
            catch (Exception e)
            {
                var ErrorMessage = e.Message.ToString();
                return Json(ErrorMessage, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GradePlanner()
        {
            try
            {
                ViewBag.count = obj_gradeTypetoClass.getAssignGrade().Count;
                return View(obj_gradeTypetoClass.getAssignGrade());
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
                return View();
            }
        }
        [HttpPost]
        public ActionResult GradePlanner(FormCollection frm, SaveGrade model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = Convert.ToInt32(frm["GradeacYear"]);
                    int gid = Convert.ToInt32(frm["GradeType"]);
                    string checkclass = frm["txt-checkclass"];
                    int count = Convert.ToInt32(frm["hidden-count"]);
                    if (checkclass == "on")
                    {
                        var ans = obj_class.getAllClassId();
                        for (int k = 0; k < ans.Count; k++)
                        {
                            int id = ans[k];
                            bool cc = obj_gradeTypetoClass.checkAssignGradeClass(acid, id, gid);
                            if (cc == true)
                            {
                                ViewBag.ErrorMessage = eAcademy.Models.ResourceCache.Localize("Grade_Already_assigned_this_class");
                            }
                            else
                            {
                                obj_gradeTypetoClass.addAssignGradeClass(acid, id, gid);
                            }
                        }
                    }
                    else
                    {
                        string classid = (frm["class_Fees"]).ToString();
                        string[] c_id = classid.Split(',');
                        for (int a = 0; a < c_id.Length; a++)
                        {
                            int cid = Convert.ToInt32(c_id[a]);
                            bool cc = obj_gradeTypetoClass.checkAssignGradeClass(acid, cid, gid);
                            if (cc == true)
                            {
                                ViewBag.ErrorMessage = eAcademy.Models.ResourceCache.Localize("GradeAlreadySuccessfully");
                            }
                            else
                            {
                                obj_gradeTypetoClass.addAssignGradeClass(acid, cid, gid);
                            }
                        }
                    }
                    ViewBag.Message = eAcademy.Models.ResourceCache.Localize("Grade_Type_Added_Successfully");
                }
                else
                {
                    ViewBag.modelState = eAcademy.Models.ResourceCache.Localize("Enter_all_the_Mandatory_Fields");
                }
                ViewBag.count = obj_gradeTypetoClass.getAssignGrade().Count;
                return View(obj_gradeTypetoClass.getAssignGrade());
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;
                return View();
            }
        }
        [HttpPost]
        public JsonResult AddGradeplanner(SaveGrade model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int acid = Convert.ToInt32(model.GradeacYear);
                    int gid = Convert.ToInt32(model.GradeType);
                    string checkclass = model.CheckClass;
                    string Success = "", ErrorMessage = "";
                    if (checkclass == "on")
                    {
                        var ans = obj_class.getAllClassId();
                        for (int k = 0; k < ans.Count; k++)
                        {
                            int id = ans[k];
                            bool cc = obj_gradeTypetoClass.checkAssignGradeClass(acid, id, gid);
                            if (cc == true)
                            {
                                ErrorMessage = eAcademy.Models.ResourceCache.Localize("Grade_Already_assigned_to_class_Successfully");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                obj_gradeTypetoClass.addAssignGradeClass(acid, id, gid);
                                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_grade_to_class"));
                            }
                        }
                    }
                    else
                    {
                        string classid = (model.class_Fees).ToString();
                        string[] c_id = classid.Split(',');
                        for (int a = 0; a < c_id.Length; a++)
                        {
                            int cid = Convert.ToInt32(c_id[a]);
                            bool cc = obj_gradeTypetoClass.checkAssignGradeClass(acid, cid, gid);
                            if (cc == true)
                            {
                                ErrorMessage = eAcademy.Models.ResourceCache.Localize("Grade_Already_assigned_this_class");
                                return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                obj_gradeTypetoClass.addAssignGradeClass(acid, cid, gid);
                                useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Add_grade_to_class"));
                            }
                        }
                    }
                    Success = eAcademy.Models.ResourceCache.Localize("Grade_Type_Added_Successfully");
                    return Json(new { Success = Success }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string totalError = "";
                    string[] t = new string[ModelState.Values.Count];
                    int i = 0;
                    foreach (var obj in ModelState.Values)
                    {
                        foreach (var error in obj.Errors)
                        {
                            if (!string.IsNullOrEmpty(error.ErrorMessage))
                            {
                                totalError = totalError + error.ErrorMessage + Environment.NewLine;
                                t[i] = error.ErrorMessage;
                                i++;
                            }
                        }
                    }
                    return Json(new { Success = 0, ex = t });
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DeleteGradeSystem(string passId)
        {
            try
            {
                AssignGradeTypeToClassServices ags = new AssignGradeTypeToClassServices();
                var gradeclass = ags.getgradetypeclass(Convert.ToInt32(QSCrypt.Decrypt(passId)));
                bool existacademicyear = obj_academicYear.PastAcademicyear(gradeclass.AcademicYearId);
                if (existacademicyear == true)
                {
                    int Did = Convert.ToInt32(QSCrypt.Decrypt(passId));
                    bool ss = ags.DeleteGrade(Did);
                    if (ss == true)
                    {
                        string Message = eAcademy.Models.ResourceCache.Localize("Grade_system_deleted_current_class");
                        useractivity.AddEmployeeActivityDetails(eAcademy.Models.ResourceCache.Localize("Delete_grade_system_to_class"));
                        return Json(new { Message = Message }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMessage = eAcademy.Models.ResourceCache.Localize("This_grade_system_already_used,so_never_deleted");
                        return Json(new { ErrorMessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string InfoMessage = ResourceCache.Localize("Unable_to_do_changes_for_past_academic_year");
                    return Json(new { InfoMessage = InfoMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                string ExceptionError = e.Message.ToString();
                return Json(new { ExceptionError = ExceptionError }, JsonRequestBehavior.AllowGet);
            }
        }
        
        public JsonResult ClassGradeSystem(string sidx, string sord, int page, int rows, int? GradeacYear1, int? Gradeclass1)
        {
        IList<GradeClass> todoListsResults2 = new List<GradeClass>();
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            todoListsResults2 = obj_gradeTypetoClass.getAssignGrade();
            if (GradeacYear1.HasValue && Gradeclass1.HasValue)
            {
                todoListsResults2 = todoListsResults2.Where(p => p.AcYaerId == GradeacYear1 && p.ClassId == Gradeclass1).ToList();
            }
            int totalRecords = todoListsResults2.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord != null)
            {
                if (sord.ToUpper() == "DESC")
                {
                    todoListsResults2 = todoListsResults2.OrderByDescending(s => s.AcYaerId).ToList();
                    todoListsResults2 = todoListsResults2.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    todoListsResults2 = todoListsResults2.OrderBy(s => s.AcYaerId).ToList();
                    todoListsResults2 = todoListsResults2.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
            }
            Session["JQGridList"] = todoListsResults2;
           
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = todoListsResults2
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ClassGradeSystemExportToExcel()
        {
            try
            {
                List<GradeClass> todoListsResults2 = (List<GradeClass>)Session["JQGridList"];
                var list = todoListsResults2.Select(o => new { AcademicYear = o.AcYear, Class = o.Class, GradeType = o.GradeType }).ToList();
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassGradeSystem");
                GenerateExcel.ExportExcel(list, fileName);
                return View("CreateRole");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
        public ActionResult ClassGradeSystemExportToPDF()
        {
            try
            {
                List<GradeClass> todoListsResults2 = (List<GradeClass>)Session["JQGridList"];
                string xfilePath = Server.MapPath("~/Views/") + "Sample1.pdf";
                string fileName = eAcademy.Models.ResourceCache.Localize("ClassGradeSystem");
                GeneratePDF.ExportPDF_Portrait(todoListsResults2, new string[] { "AcademicYear", "Class", "GradeType" }, xfilePath, fileName);
                return File(xfilePath, "application/pdf", "ClassGradeSystem.pdf");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Error");
            }
        }
    }
}