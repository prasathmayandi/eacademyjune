﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace eAcademy.HelperClass
{
    public static class DateTimeByZone
    {
        static string zone = ConfigurationManager.AppSettings["DateTimeZone"];
        private static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById(zone);

        public static DateTime getCurrentDateTime()
        {
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            return indianTime;
        }
        public static DateTime getCurrentDate()
        {
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            return indianTime.Date;
        }
        public static string getCurrentDateTimeDashboard()
        {
            var indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            return indianTime.ToString("yyyy,MM,dd,HH,mm,ss");

            //var currentDate = DateTime.Today.ToString("yyyy,MM,dd,HH,mm,ss");
            //return currentDate;
        }
    }
}