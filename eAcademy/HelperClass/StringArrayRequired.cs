﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eAcademy.HelperClass
{


    public class StringArrayRequiredAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string[] array = value as string[];

            if (array == null || array.Any(item => string.IsNullOrEmpty(item)))
            {
                return new ValidationResult(this.ErrorMessage);
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }
    public class TimeSpanValidationAttribute : ValidationAttribute
    {
        int TimeNull;
        TimeSpan span;
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            TimeSpan[] array = value as TimeSpan[];

            span = TimeSpan.Zero;
            int count = array.Length;

            for (int i = 0; i < count; i++)
            {
                if (array[i] == span)
                {
                    // TimeNull = 0;
                    return new ValidationResult(this.ErrorMessage);
                }
                else
                {
                    //TimeNull = 1;
                }
            }
            return ValidationResult.Success;
            //if (TimeNull == 0)
            //{
            //    return new ValidationResult(this.ErrorMessage);
            //}
            //else
            //{
            //    return ValidationResult.Success;
            //}
        }
    }
    public class DateTimeValidationAttribute : ValidationAttribute
    {
        int TimeNull;
        // DateTime span;
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            DateTime[] array = value as DateTime[];

            //DateTime span = {01/01/0001 12:00:00 AM};
            int count = array.Length;

            for (int i = 0; i < count; i++)
            {
                if (array[i] == null)
                {
                    TimeNull = 0;
                }
                else
                {
                    TimeNull = 1;
                }
            }

            if (TimeNull == 0)
            {
                return new ValidationResult(this.ErrorMessage);
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }
  


}