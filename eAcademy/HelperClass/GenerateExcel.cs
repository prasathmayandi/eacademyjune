﻿using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eAcademy.HelperClass
{
    public class GenerateExcel
    {
        public static void ExportExcel<TSource>(IList<TSource> todoListsResults)
        {
            var grid = new GridView();
            grid.DataSource = todoListsResults;
            grid.DataBind();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=StudentAchievement.xls");
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            grid.RenderControl(htw);
            HttpContext.Current.Response.Write(sw.ToString());
            HttpContext.Current.Response.End();
        }
        public static void ExportExcel<TSource>(IList<TSource> todoListsResults, string fileName)
        {
            var grid = new GridView();
            grid.DataSource = todoListsResults;
            if (todoListsResults.Count == 0)
            {
               List<string> SelectionList = new List<string>();
               SelectionList.Add("No record found");
               grid.DataSource = SelectionList;
            }
            grid.DataBind();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename="+fileName+".xls");
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            grid.RenderControl(htw);
            HttpContext.Current.Response.Write(sw.ToString());
            HttpContext.Current.Response.End();
        }

        public static string statusValue(bool? status)
        {
            if (status == true)
            {
                return "True";
            }
            else
            {
                return "False";
            }
        }
    }
}