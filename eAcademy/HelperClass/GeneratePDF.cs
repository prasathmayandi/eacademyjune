﻿using System.Collections.Generic;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
namespace eAcademy.HelperClass
{
    public class GeneratePDF
    {
        public static void ExportPDF<TSource>(IList<TSource> todoListsResults, string[] columns, string filePath)
        {
            string FilePath = System.Web.HttpContext.Current.Server.MapPath("~/img/logo.png");
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(FilePath);
            logo.ScaleAbsolute(100, 50);
            iTextSharp.text.Font headerFont = FontFactory.GetFont("Verdana", 10, iTextSharp.text.BaseColor.WHITE);
            iTextSharp.text.Font rowfont = FontFactory.GetFont("Verdana", 10, iTextSharp.text.BaseColor.BLUE);
            iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4);
            Document doc = new Document(PageSize.A4, 36, 72, 108, 180);
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.OpenOrCreate));
            document.Open();
            PdfPTable table = new PdfPTable(columns.Length);
        //    PdfPTable table = new PdfPTable(1);
            table.HeaderRows = 1;
            foreach (var column in columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column, headerFont));
                cell.BackgroundColor = BaseColor.BLACK;
                table.AddCell(cell);
            }
            foreach (var item in todoListsResults)
            {
                foreach (var column in columns)
                {
                    var value1 =  item.GetType().GetProperty(column).GetValue(item);
                    if (value1 == null)
                    {
                        // your cell value is null, do something in null value case
                        string value = "null";
                        PdfPCell cell5 = new PdfPCell(new Phrase(value, rowfont));
                        table.AddCell(cell5);
                    }
                    else
                    {
                        string value = item.GetType().GetProperty(column).GetValue(item).ToString();
                        PdfPCell cell5 = new PdfPCell(new Phrase(value, rowfont));
                        table.AddCell(cell5);
                    }
                    
                }
            }        
            logo.Border = iTextSharp.text.Rectangle.BOX;
            logo.BorderColor = iTextSharp.text.BaseColor.BLACK;
            logo.BorderWidth = 3f;
            document.Add(logo);
            document.Add(new Paragraph("Acestra Networks"));   
            // Setting paragraph's text alignment using iTextSharp.text.Element class
            // Adding this 'para' to the Document object
            document.Add(table);
            document.Close();
        }
        public static void ExportPDF_Landscape<TSource>(IList<TSource> todoListsResults, string[] columns, string filePath, string heading)
        {
            //string FilePath = System.Web.HttpContext.Current.Server.MapPath("~/img/logo.png");
            //iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(FilePath);
            //logo.ScaleAbsolute(100, 50);
            iTextSharp.text.Font headerFont = FontFactory.GetFont("Verdana", 10,iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font rowfont = FontFactory.GetFont("Verdana", 10, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate(), 36, 36, 108, 180);
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.OpenOrCreate));
            document.Open();
            PdfPTable table = new PdfPTable(columns.Length);
            table.HeaderRows = 1;
            foreach (var column in columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column, headerFont));
             //   cell.BackgroundColor = BaseColor.BLACK;             // To set column head background color              
                table.AddCell(cell);
            }
            foreach (var item in todoListsResults)
            {
                foreach (var column in columns)
                {
                    var value1 = item.GetType().GetProperty(column).GetValue(item);
                    if (value1 == null)
                    {
                        // your cell value is null, do something in null value case
                        string value = "";
                        PdfPCell cell5 = new PdfPCell(new Phrase(value, rowfont));
                        table.AddCell(cell5);
                    }
                    else
                    {
                        string value = item.GetType().GetProperty(column).GetValue(item).ToString();
                        PdfPCell cell5 = new PdfPCell(new Phrase(value, rowfont));
                        table.AddCell(cell5);
                    }
                }
            }
            //logo.Border = iTextSharp.text.Rectangle.BOX;
            //logo.BorderColor = iTextSharp.text.BaseColor.BLACK;
            //logo.BorderWidth = 3f;
            //document.Add(logo);
            Paragraph tableHeading = new Paragraph(Font.BOLD, heading);
            tableHeading.Alignment = Element.ALIGN_CENTER;
            // Adding this 'para' to the Document object
            //document.AddTitle("Hello World example");
            //document.AddSubject("This is an Example 4 of Chapter 1 of Book 'iText in Action'");
            //document.AddKeywords("Metadata, iTextSharp 5.4.4, Chapter 1, Tutorial");
            //document.AddCreator("iTextSharp 5.4.4");
            //document.AddAuthor("Debopam Pal");
            //document.AddHeader("Nothing", "No Header");

            tableHeading.Font.Size = 14;
            tableHeading.SpacingAfter = 20;
            document.Add(tableHeading);           
            document.Add(table);
            document.Close();
        }
        public static void ExportPDF_Portrait<TSource>(IList<TSource> todoListsResults, string[] columns, string filePath, string heading)
        {
            //string FilePath = System.Web.HttpContext.Current.Server.MapPath("~/img/logo.png");
            //iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(FilePath);
            //logo.ScaleAbsolute(100, 50);
            iTextSharp.text.Font headerFont = FontFactory.GetFont("Verdana", 10, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Font rowfont = FontFactory.GetFont("Verdana", 10, iTextSharp.text.BaseColor.BLACK);
            iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 36, 36, 108, 180);
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.OpenOrCreate));
            document.Open();
            PdfPTable table = new PdfPTable(columns.Length);
            table.HeaderRows = 1;
            foreach (var column in columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column, headerFont));
                //   cell.BackgroundColor = BaseColor.BLACK;             // To set column head background color              
                table.AddCell(cell);
            }
            foreach (var item in todoListsResults)
            {
                foreach (var column in columns)
                {
                    var value1 = item.GetType().GetProperty(column).GetValue(item);
                    if (value1 == null)
                    {
                        // your cell value is null, do something in null value case
                        string value = "";
                        PdfPCell cell5 = new PdfPCell(new Phrase(value, rowfont));
                        table.AddCell(cell5);
                    }
                    else
                    {
                        string value = item.GetType().GetProperty(column).GetValue(item).ToString();
                        PdfPCell cell5 = new PdfPCell(new Phrase(value, rowfont));
                        table.AddCell(cell5);
                    }
                }
            }
            //logo.Border = iTextSharp.text.Rectangle.BOX;
            //logo.BorderColor = iTextSharp.text.BaseColor.BLACK;
            //logo.BorderWidth = 3f;
            //document.Add(logo);
            Paragraph tableHeading = new Paragraph(Font.BOLD, heading);
            tableHeading.Alignment = Element.ALIGN_CENTER;
            // Adding this 'para' to the Document object
            //document.AddTitle("Hello World example");
            //document.AddSubject("This is an Example 4 of Chapter 1 of Book 'iText in Action'");
            //document.AddKeywords("Metadata, iTextSharp 5.4.4, Chapter 1, Tutorial");
            //document.AddCreator("iTextSharp 5.4.4");
            //document.AddAuthor("Debopam Pal");
            //document.AddHeader("Nothing", "No Header");
            tableHeading.Font.Size = 14;
            tableHeading.SpacingAfter = 20;
            document.Add(tableHeading);
            document.Add(table);
            document.Close();
        }
    }
}