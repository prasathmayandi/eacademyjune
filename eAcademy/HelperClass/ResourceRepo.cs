﻿using eAcademy.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.HelperClass
{
    public class ResourceRepo
    {
        public Dictionary<string, string> GetResourceValues(ResourceType type, string cultureName)
        {
            var list = new Dictionary<string, string>();
            string typeName = type.ToString();

            using (var ctx = new EacademyEntities())
            {
                var query =
                    from resource in ctx.DefaultResources
                    join rt in ctx.ResourceTypes on resource.ResourceTypeId equals rt.Id into rt1
                    from resourceType in rt1.DefaultIfEmpty()
                    join rv in ctx.ResourceValues on resource.Id equals rv.ResourceId into rv1
                    from resourceValue in rv1.Where(rv => rv.CultureName == cultureName).DefaultIfEmpty()
                    where resourceType.Name == typeName && resourceValue.CultureName == cultureName && !resource.Deleted
                    select new
                    {
                        Name = resource.Name,
                        Value =  resourceValue.Value,
                    };

                query.ToList().ForEach(x => { if (!list.ContainsKey(x.Name)) list.Add(x.Name, x.Value); });
            }

            return list;
        }

        public static string GetResourceValue(string resourceName, string cultureName)
        {
            using (var ctx = new EacademyEntities())
            {
                var query =
                    from resource in ctx.DefaultResources
                    join rv in ctx.ResourceValues on resource.Id equals rv.ResourceId into rv1
                    from resourceValue in rv1.Where(rv => rv.CultureName == cultureName).DefaultIfEmpty()
                    where resource.Name == resourceName
                    select new
                    {
                        Value = resourceValue.Value
                    };

                var queryResult = query.FirstOrDefault();

                return queryResult != null ? queryResult.Value : string.Empty;
            }
        }
    }
}