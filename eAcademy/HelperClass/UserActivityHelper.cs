﻿using eAcademy.DataModel;
using System;
using System.Linq;
using System.Web;
namespace eAcademy.HelperClass
{
    public class UserActivityHelper
    {
        EacademyEntities dc = new EacademyEntities();
        string actionName = HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString();
        string controllerName = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString();
        public void AddEmployeeActivityDetails(string Activity)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var id =HttpContext.Current.Session["EmployeeSessionId"];
            string link = "/" + controllerName + "/" + actionName;    
                var getrow = dc.UserLogs.Where(q => q.SessionId == id).FirstOrDefault();
                if (getrow != null)
                {
                    EmployeeUserActivity a = new DataModel.EmployeeUserActivity()
                    {
                        UserLogId = getrow.UserLogId,
                        Activity = Activity,
                        Features = link,
                       
                        Time = date
                    };
                    dc.EmployeeUserActivities.Add(a);
                    dc.SaveChanges();
                }            
        }
        public void AddStudentActivityDetails(string Activity)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var id = HttpContext.Current.Session["StudentSessionId"];
            string link = "/" + controllerName + "/" + actionName;            
            var getrow = dc.UserLogs.Where(q => q.SessionId == id).FirstOrDefault();
            if (getrow != null)
            {
                StudentUserActivity a = new DataModel.StudentUserActivity()
                {
                    UserLogId = getrow.UserLogId,
                    Activity = Activity,
                    Features = link,

                    Time =date
                };
                dc.StudentUserActivities.Add(a);
                dc.SaveChanges();
            }
        }
        public void AddParentActivityDetails(string Activity)
        {
            var date = DateTimeByZone.getCurrentDateTime();
            var id = HttpContext.Current.Session["ParentSessionId"];
            string link = "/" + controllerName + "/" + actionName;           
            var getrow = dc.UserLogs.Where(q => q.SessionId == id).FirstOrDefault();
            if (getrow != null)
            {
                ParentUserActivity a = new DataModel.ParentUserActivity()
                {
                    UserLogId = getrow.UserLogId,
                    Activity = Activity,
                    Features = link,

                    Time = date
                };
                dc.ParentUserActivities.Add(a);
                dc.SaveChanges();
            }
        }
    }
}