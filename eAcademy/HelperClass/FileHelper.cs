﻿using eAcademy.Models;
using System.IO;
using System.Web;
namespace eAcademy.HelperClass
{
    public class FileHelper
    {
        public M_File CheckFile(HttpPostedFileBase file, int pathname,string name,long filesize)
        {
                           // string filenamewithoutextention;
                            string extention3 = System.IO.Path.GetExtension(file.FileName);
                            string filename = "";
                          int FileSizeId=0,FileTypeId=0;
                          long fileSize3 = file.ContentLength;
                          fileSize3 = file.ContentLength / filesize;
                          if (extention3 == ".pdf" || extention3 == ".PDF" || extention3 == ".docx" || extention3 == ".DOCX"  ||  extention3 == ".doc" || extention3 == ".DOC" )
                            {
                                if (file != null && file.ContentLength > 0 && fileSize3 <= 3)
                                {
                                   // filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file.FileName);
                                    //string path = System.IO.Path.Combine(Microsoft.SqlServer.Server.MapPath("D:/25-03-2015 Pre-Admin/eAcademy/eAcademy/Documents/TC/"), fname3 + extention3);
                                     filename=name+pathname+ extention3;
                                     string path = Path.Combine(HttpContext.Current.Server.MapPath("~/Documents/" + pathname + ""), filename);// HttpContext.Current.Server.MapPath("~/Documents/" + pathname + "" + filename);
                                     if (File.Exists(path))
                                     {
                                         File.Delete(path);
                                         
                                         file.SaveAs(path);
                                     }
                                     else
                                     {
                                         file.SaveAs(path);
                                     }
                                }
                                else
                                {
                                     //ErrorMessage1 = "BirthCertificate file size exceeds";
                                    FileSizeId=98;                                    
                                }
                            }
                            else
                            {
                                 //ErrorMessage2 = "BirthCertificate file format not supported";
                               FileTypeId=99;
                            }
                            return new M_File { filename = filename, FileSizeId = FileSizeId, FileTypeId = FileTypeId };
        }
        public M_File CheckImageFile(HttpPostedFileBase file, int pathname, string name, long filesize)
        {
            // string filenamewithoutextention;
            string extention3 = System.IO.Path.GetExtension(file.FileName);
            string filename = "";
            int FileSizeId = 0, FileTypeId = 0;
            long fileSize3 = file.ContentLength;
            fileSize3 = file.ContentLength / filesize;
            if ( extention3 == ".png" || extention3 == ".PNG" || extention3 == ".jpeg" || extention3 == ".JPEG" || extention3 == ".jpg" || extention3 == ".JPG")
            {
                if (file != null && file.ContentLength > 0 && fileSize3 <= 3)
                {
                    // filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file.FileName);
                    //string path = System.IO.Path.Combine(Microsoft.SqlServer.Server.MapPath("D:/25-03-2015 Pre-Admin/eAcademy/eAcademy/Documents/TC/"), fname3 + extention3);
                    filename = name + pathname + extention3;
                    string path = Path.Combine(HttpContext.Current.Server.MapPath("~/Documents/" + pathname + ""), filename);// HttpContext.Current.Server.MapPath("~/Documents/" + pathname + "" + filename);
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                        file.SaveAs(path);
                    }
                    else
                    {
                        file.SaveAs(path);
                    }
                }
                else
                {
                    //ErrorMessage1 = "BirthCertificate file size exceeds";
                    FileSizeId = 98;
                }
            }
            else
            {
                //ErrorMessage2 = "BirthCertificate file format not supported";
                FileTypeId = 99;
            }
            return new M_File { filename = filename, FileSizeId = FileSizeId, FileTypeId = FileTypeId };
        }
        public M_File Edit_CheckFile(HttpPostedFileBase file, string pathname, string name, long filesize)
        {
            // string filenamewithoutextention;
            string extention3 = System.IO.Path.GetExtension(file.FileName);
            string filename = "";
            int FileSizeId = 0, FileTypeId = 0;
            long fileSize3 = file.ContentLength;
            fileSize3 = file.ContentLength / filesize;
            if (extention3 == ".pdf" || extention3 == ".PDF" || extention3 == ".docx" || extention3 == ".DOCX" || extention3 == ".doc" || extention3 == ".DOC")
            {
                if (file != null && file.ContentLength > 0 && fileSize3 <= 3)
                {
                    // filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file.FileName);
                    //string path = System.IO.Path.Combine(Microsoft.SqlServer.Server.MapPath("D:/25-03-2015 Pre-Admin/eAcademy/eAcademy/Documents/TC/"), fname3 + extention3);
                    filename = name + pathname + extention3;
                    string path = Path.Combine(HttpContext.Current.Server.MapPath("~/Documents/" + pathname + ""), filename);// HttpContext.Current.Server.MapPath("~/Documents/" + pathname + "" + filename);
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                        file.SaveAs(path);
                    }
                    else
                    {
                        file.SaveAs(path);
                    }
                }
                else
                {
                    //ErrorMessage1 = "BirthCertificate file size exceeds";
                    FileSizeId = 98;
                }
            }
            else
            {
                //ErrorMessage2 = "BirthCertificate file format not supported";
                FileTypeId = 99;
            }
            return new M_File { filename = filename, FileSizeId = FileSizeId, FileTypeId = FileTypeId };
        }
        public M_File Edit_CheckImageFile(HttpPostedFileBase file, string pathname, string name, long filesize)
        {
            // string filenamewithoutextention;
            string extention3 = System.IO.Path.GetExtension(file.FileName);
            string filename = "";
            int FileSizeId = 0, FileTypeId = 0;
            long fileSize3 = file.ContentLength;
            fileSize3 = file.ContentLength / filesize;
            if (extention3 == ".png" || extention3 == ".PNG" || extention3 == ".jpeg" || extention3 == ".JPEG" || extention3 == ".jpg" || extention3 == ".JPG")
            {
                if (file != null && file.ContentLength > 0 && fileSize3 <= 3)
                {
                    // filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file.FileName);
                    //string path = System.IO.Path.Combine(Microsoft.SqlServer.Server.MapPath("D:/25-03-2015 Pre-Admin/eAcademy/eAcademy/Documents/TC/"), fname3 + extention3);
                    filename = name + pathname + extention3;
                    string path = Path.Combine(HttpContext.Current.Server.MapPath("~/Documents/" + pathname + ""), filename);// HttpContext.Current.Server.MapPath("~/Documents/" + pathname + "" + filename);
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                        file.SaveAs(path);
                    }
                    else
                    {
                        file.SaveAs(path);
                    }
                }
                else
                {
                    //ErrorMessage1 = "BirthCertificate file size exceeds";
                    FileSizeId = 98;
                }
            }
            else
            {
                //ErrorMessage2 = "BirthCertificate file format not supported";
                FileTypeId = 99;
            }
            return new M_File { filename = filename, FileSizeId = FileSizeId, FileTypeId = FileTypeId };
        }
        public M_File CheckEmployeeFile(HttpPostedFileBase file, string pathname, string name, long filesize, string foldername)
        {
            // string filenamewithoutextention;
            string extention3 = System.IO.Path.GetExtension(file.FileName);
            string filename = "";
            int FileSizeId = 0, FileTypeId = 0;
            long fileSize3 = file.ContentLength;
            fileSize3 = file.ContentLength / filesize;
            if (extention3 == ".pdf" || extention3 == ".PDF" || extention3 == ".docx" || extention3 == ".DOCX" || extention3 == ".doc" || extention3 == ".DOC")
            {
                if (file != null && file.ContentLength > 0 && fileSize3 <= 3)
                {
                    // filenamewithoutextention = System.IO.Path.GetFileNameWithoutExtension(file.FileName);
                    //string path = System.IO.Path.Combine(Microsoft.SqlServer.Server.MapPath("D:/25-03-2015 Pre-Admin/eAcademy/eAcademy/Documents/TC/"), fname3 + extention3);
                    filename = name + pathname + extention3;
                    string path = Path.Combine(HttpContext.Current.Server.MapPath("~/img/" + foldername + "/" + pathname + ""), filename);// HttpContext.Current.Server.MapPath("~/Documents/" + pathname + "" + filename);
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                        file.SaveAs(path);
                    }
                    else
                    {
                        file.SaveAs(path);
                    }
                }
                else
                {
                    //ErrorMessage1 = "BirthCertificate file size exceeds";
                    FileSizeId = 98;
                }
            }
            else
            {
                //ErrorMessage2 = "BirthCertificate file format not supported";
                FileTypeId = 99;
            }
            return new M_File { filename = filename, FileSizeId = FileSizeId, FileTypeId = FileTypeId };
        }
        public M_File CheckEmployeeImageFile(HttpPostedFileBase file, string pathname, string name, long filesize, string foldername)
        {  
            string extention3 = System.IO.Path.GetExtension(file.FileName);
            string filename = "";
            int FileSizeId = 0, FileTypeId = 0;
            long fileSize3 = file.ContentLength;
            fileSize3 = file.ContentLength / filesize;
            if (extention3 == ".png" || extention3 == ".PNG" || extention3 == ".jpeg" || extention3 == ".JPEG" || extention3 == ".jpg" || extention3 == ".JPG")
            {
                if (file != null && file.ContentLength > 0 && fileSize3 <= 3)
                {                  
                    filename = name + pathname + extention3;
                    string path = Path.Combine(HttpContext.Current.Server.MapPath("~/img/" + foldername + "/" + pathname + ""), filename);
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                        file.SaveAs(path);
                    }
                    else
                    {
                        file.SaveAs(path);
                    }
                }
                else
                {                  
                    FileSizeId = 98;
                }
            }
            else
            {                
                FileTypeId = 99;
            }
            return new M_File { filename = filename, FileSizeId = FileSizeId, FileTypeId = FileTypeId };
        }
    }
}