﻿
using eAcademy.Models;
using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;

namespace eAcademy.Globalisation
{
    public class AdminResources
    {
        public static string School_name_is_required
        {
            get
            {
                return ResourceCache.Localize("School_name_is_required");
            }
        }
        public static string School_name_should_contain_3_characters_can_not_exceed_200_characters
        {
            get
            {
                return ResourceCache.Localize("School_name_should_contain_3_characters_can_not_exceed_200_characters");
            }
        }
        public static string School_code_is_required
        {
            get
            {
                return ResourceCache.Localize("School_code_is_required");
            }
        }
        public static string School_code_should_contain_3_characters_can_not_exceed_10_characters
        {
            get
            {
                return ResourceCache.Localize("School_code_should_contain_3_characters_can_not_exceed_10_characters");
            }
        }
        public static string AddressLine1_name_is_required
        {
            get
            {
                return ResourceCache.Localize("AddressLine1_name_is_required");
            }
        }
        public static string Adress_line1_can_not_exceed_100_characters
        {
            get
            {
                return ResourceCache.Localize("Adress_line1_can_not_exceed_100_characters");
            }
        }
        public static string Adress_line2_can_not_exceed_100_characters
        {
            get
            {
                return ResourceCache.Localize("Adress_line2_can_not_exceed_100_characters");
            }
        }
        public static string City_is_required
        {
            get
            {
                return ResourceCache.Localize("City_is_required");
            }
        }
        public static string City_should_contain_3_characters_can_not_exceed_50_characters
        {
            get
            {
                return ResourceCache.Localize("City_should_contain_3_characters_can_not_exceed_50_characters");
            }
        }
        public static string State_is_required
        {
            get
            {
                return ResourceCache.Localize("State_is_required");
            }
        }
        public static string State_should_contain_2_characters_can_not_exceed_50_characters
        {
            get
            {
                return ResourceCache.Localize("State_should_contain_2_characters_can_not_exceed_50_characters");
            }
        }
        public static string Zip_code_is_required
        {
            get
            {
                return ResourceCache.Localize("Zip_code_is_required");
            }
        }
        public static string Zip_code_should_contain_6_digit_can_not_exceed_6_digit
        {
            get
            {
                return ResourceCache.Localize("Zip_code_should_contain_6_digit_can_not_exceed_6_digit");
            }
        }
        public static string Country_is_required
        {
            get
            {
                return ResourceCache.Localize("Country_is_required");
            }
        }
        public static string Phone_Number1_is_required
        {
            get
            {
                return ResourceCache.Localize("Phone_Number1_is_required");
            }
        }
        public static string Phone_number1_can_not_exceed_14_digit
        {
            get
            {
                return ResourceCache.Localize("Phone_number1_can_not_exceed_14_digit");
            }
        }
        public static string Phone_number2_can_not_exceed_14_digit
        {
            get
            {
                return ResourceCache.Localize("Phone_number2_can_not_exceed_14_digit");
            }
        }
        public static string Mobile_Number1_is_required
        {
            get
            {
                return ResourceCache.Localize("Mobile_Number1_is_required");
            }
        }
        public static string Mobile_Number2_is_required
        {
            get
            {
                return ResourceCache.Localize("Mobile_Number2_is_required");
            }
        }
        public static string Fax_is_required
        {
            get
            {
                return ResourceCache.Localize("Fax_is_required");
            }
        }
        public static string FAX_can_not_exceed_14_digit
        {
            get
            {
                return ResourceCache.Localize("FAX_can_not_exceed_14_digit");
            }
        }
        public static string Email_is_required
        {
            get
            {
                return ResourceCache.Localize("Email_is_required");
            }
        }
        public static string Email_can_not_exceed_70_characters
        {
            get
            {
                return ResourceCache.Localize("Email_can_not_exceed_70_characters");
            }
        }
        public static string Invalid_email_address
        {
            get
            {
                return ResourceCache.Localize("Invalid_email_address");
            }
        }
        public static string Website_URL_is_required
        {
            get
            {
                return ResourceCache.Localize("Website_URL_is_required");
            }
        }
        public static string Website_URL_can_not_exceed_50_characters
        {
            get
            {
                return ResourceCache.Localize("Website_URL_can_not_exceed_50_characters");
            }
        }
        public static string Student_ID_format_can_not_exceed_10_digit
        {
            get
            {
                return ResourceCache.Localize("Student_ID_format_can_not_exceed_10_digit");
            }
        }
        public static string Student_ID_format_is_required
        {
            get
            {
                return ResourceCache.Localize("Student_ID_format_is_required");
            }
        }
        public static string Parent_ID_format_is_required
        {
            get
            {
                return ResourceCache.Localize("Parent_ID_format_is_required");
            }
        }
        public static string Parent_ID_format_can_not_exceed_10_digit
        {
            get
            {
                return ResourceCache.Localize("Parent_ID_format_can_not_exceed_10_digit");
            }
        }
        public static string Staff_ID_format_is_required
        {
            get
            {
                return ResourceCache.Localize("Staff_ID_format_is_required");
            }
        }
        public static string Staff_ID_format_can_not_exceed_10_digit
        {
            get
            {
                return ResourceCache.Localize("Staff_ID_format_can_not_exceed_10_digit");
            }
        }
        public static string Non_Staff_ID_format_is_required
        {
            get
            {
                return ResourceCache.Localize("Non_Staff_ID_format_is_required");
            }
        }
        public static string Currency_type_is_required
        {
            get
            {
                return ResourceCache.Localize("Currency_type_is_required");
            }
        }
        public static string Day_order_is_required
        {
            get
            {
                return ResourceCache.Localize("Day_order_is_required");
            }
        }
        public static string Marking_scheme_format_is_required
        {
            get
            {
                return ResourceCache.Localize("Marking_scheme_format_is_required");
            }
        }
        public static string Services_tax_is_required
        {
            get
            {
                return ResourceCache.Localize("Services_tax_is_required");
            }
        }
        public static string Please_Enter_Schedule_Name
        {
            get
            {
                return ResourceCache.Localize("Please_Enter_Schedule_Name");
            }
        }
        public static string Select_StartTime_in_all_Added_Rows
        {
            get
            {
                return ResourceCache.Localize("Select_StartTime_in_all_Added_Rows");
            }
        }
        public static string Select_EndTime_in_all_Added_Rows
        {
            get
            {
                return ResourceCache.Localize("Select_EndTime_in_all_Added_Rows");
            }
        }
        public static string Fill_Periods_in_all_Added_Rows
        {
            get
            {
                return ResourceCache.Localize("Fill_Periods_in_all_Added_Rows");
            }
        }
        public static string Select_Type_in_all_Added_Rows
        {
            get
            {
                return ResourceCache.Localize("Select_Type_in_all_Added_Rows");
            }
        }
        public static string Please_enter_employee_first_name
        {
            get
            {
                return ResourceCache.Localize("Please_enter_employee_first_name");
            }
        }
        public static string employee_first_name_should_contain_3_characters_can_not_exceed_200_characters
        {
            get
            {
                return ResourceCache.Localize("employee_first_name_should_contain_3_characters_can_not_exceed_200_characters");
            }
        }
        public static string employee_Middle_name_should_contain_3_characters_can_not_exceed_200_characters
        {
            get
            {
                return ResourceCache.Localize("employee_Middle_name_should_contain_3_characters_can_not_exceed_200_characters");
            }
        }
        public static string Please_enter_employee_last_name
        {
            get
            {
                return ResourceCache.Localize("Please_enter_employee_last_name");
            }
        }
        public static string employee_last_name_name_should_contain_3_characters_can_not_exceed_200_characters
        {
            get
            {
                return ResourceCache.Localize("employee_last_name_name_should_contain_3_characters_can_not_exceed_200_characters");
            }
        }
        public static string Please_choose_dateofbirth
        {
            get
            {
                return ResourceCache.Localize("Please_choose_dateofbirth");
            }
        }
        public static string Please_select_gender
        {
            get
            {
                return ResourceCache.Localize("Please_select_gender");
            }
        }
        public static string Please_enter_religion
        {
            get
            {
                return ResourceCache.Localize("Please_enter_religion");
            }
        }
        public static string Please_enter_nationality
        {
            get
            {
                return ResourceCache.Localize("Please_enter_nationality");
            }
        }
        public static string Please_select_community
        {
            get
            {
                return ResourceCache.Localize("Please_select_community");
            }
        }
        public static string Please_select_martial_status
        {
            get
            {
                return ResourceCache.Localize("Please_select_martial_status");
            }
        }
        public static string Please_select_employee_type
        {
            get
            {
                return ResourceCache.Localize("Please_select_employee_type");
            }
        }
        public static string Please_enter_mobile_number
        {
            get
            {
                return ResourceCache.Localize("Please_enter_mobile_number");
            }
        }
        public static string PleaseSelectEmployeeDesignation
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectEmployeeDesignation");
            }
        }
        public static string Please_enter_local_addressline1
        {
            get
            {
                return ResourceCache.Localize("Please_enter_local_addressline1");
            }
        }
        public static string Please_enter_local_city
        {
            get
            {
                return ResourceCache.Localize("Please_enter_local_city");
            }
        }
        public static string Please_enter_local_state
        {
            get
            {
                return ResourceCache.Localize("Please_enter_local_state");
            }
        }
        public static string Please_select_local_country
        {
            get
            {
                return ResourceCache.Localize("Please_select_local_country");
            }
        }
        public static string Please_enter_local_zipcode
        {
            get
            {
                return ResourceCache.Localize("Please_enter_local_zipcode");
            }
        }
        public static string Please_enter_permanent_addressline1
        {
            get
            {
                return ResourceCache.Localize("Please_enter_permanent_addressline1");
            }
        }
        public static string Please_enter_permanent_city
        {
            get
            {
                return ResourceCache.Localize("Please_enter_permanent_city");
            }
        }
        public static string Please_enter_permanent_state
        {
            get
            {
                return ResourceCache.Localize("Please_enter_permanent_state");
            }
        }
        public static string Please_select_permanent_country
        {
            get
            {
                return ResourceCache.Localize("Please_select_permanent_country");
            }
        }
        public static string Please_enter_permanent_zipcode
        {
            get
            {
                return ResourceCache.Localize("Please_enter_permanent_zipcode");
            }
        }
        public static string Please_enter_emergency_contact_person_name
        {
            get
            {
                return ResourceCache.Localize("Please_enter_emergency_contact_person_name");
            }
        }
        public static string Please_enter_emergency_contact_person_contact
        {
            get
            {
                return ResourceCache.Localize("Please_enter_emergency_contact_person_contact");
            }
        }
        public static string Please_enter_emergency_contact_person_relationship
        {
            get
            {
                return ResourceCache.Localize("Please_enter_emergency_contact_person_relationship");
            }
        }
        public static string Please_upload_employee_photo
        {
            get
            {
                return ResourceCache.Localize("Please_upload_employee_photo");
            }
        }
        public static string Please_enter_employee_salary
        {
            get
            {
                return ResourceCache.Localize("Please_enter_employee_salary");
            }
        }
        public static string Please_select_date_of_join
        {
            get
            {
                return ResourceCache.Localize("Please_select_date_of_join");
            }
        }
        public static string Please_select_blood_group
        {
            get
            {
                return ResourceCache.Localize("Please_select_blood_group");
            }
        }
        public static string Role_name_is_required
        {
            get
            {
                return ResourceCache.Localize("Role_name_is_required");
            }
        }
        public static string Role_name_should_contain_3_characters_can_not_exceed_35_characters
        {
            get
            {
                return ResourceCache.Localize("Role_name_should_contain_3_characters_can_not_exceed_35_characters");
            }
        }
        public static string Select_Role
        {
            get
            {
                return ResourceCache.Localize("Select_Role");
            }
        }
        public static string Select_Feature
        {
            get
            {
                return ResourceCache.Localize("Select_Feature");
            }
        }
        public static string Select_User
        {
            get
            {
                return ResourceCache.Localize("Select_User");
            }
        }

        public static string Start_date_is_required { get { return ResourceCache.Localize("Start_date_is_required"); } }

        public static string End_date_is_required
        {
            get
            {
                return ResourceCache.Localize("End_date_is_required");
            }
        }
        public static string SelectAcademicYear
        {
            get
            {
                return ResourceCache.Localize("SelectAcademicYear");
            }
        }
        public static string Term_is_required
        {
            get
            {
                return ResourceCache.Localize("Term_is_required");
            }
        }
        public static string Term_should_contain_2_characters_can_not_exceed_20_characters
        {
            get
            {
                return ResourceCache.Localize("Term_should_contain_2_characters_can_not_exceed_20_characters");
            }
        }
        public static string Section_is_required
        {
            get
            {
                return ResourceCache.Localize("Section_is_required");
            }
        }
        public static string Section_should_not_exceed_5_characters
        {
            get
            {
                return ResourceCache.Localize("Section_should_not_exceed_5_characters");
            }
        }
        public static string Subject_is_required
        {
            get
            {
                return ResourceCache.Localize("Subject_is_required");
            }
        }
        public static string Subject_should_contain_2_characters_can_not_exceed_30_characters
        {
            get
            {
                return ResourceCache.Localize("Subject_should_contain_2_characters_can_not_exceed_30_characters");
            }
        }
        public static string GroupSubjectName_is_required
        {
            get
            {
                return ResourceCache.Localize("GroupSubjectName_is_required");
            }
        }
        public static string GroupSubjectName_should_contain_2_characters_can_not_exceed_30_characters
        {
            get
            {
                return ResourceCache.Localize("GroupSubjectName_should_contain_2_characters_can_not_exceed_30_characters");
            }
        }
        public static string Holiday_name_is_required
        {
            get
            {
                return ResourceCache.Localize("Holiday_name_is_required");
            }
        }
        public static string Event_should_contain_3_characters_can_not_exceed_50_characters
        {
            get
            {
                return ResourceCache.Localize("Event_should_contain_3_characters_can_not_exceed_50_characters");
            }
        }
        public static string Event_description_should_not_exceed_250_characters
        {
            get
            {
                return ResourceCache.Localize("Event_description_should_not_exceed_250_characters");
            }
        }
        public static string EventDate_is_required
        {
            get
            {
                return ResourceCache.Localize("EventDate_is_required");
            }
        }
        public static string Announcement_name_is_required
        {
            get
            {
                return ResourceCache.Localize("Announcement_name_is_required");
            }
        }
        public static string Announcement_should_contain_3_characters_can_not_exceed_50_characters
        {
            get
            {
                return ResourceCache.Localize("EventDate_is_required");
            }
        }
        public static string Announcement_description_should_not_exceed_300_characters
        {
            get
            {
                return ResourceCache.Localize("Announcement_description_should_not_exceed_300_characters");
            }
        }
        public static string Announcement_date_is_required
        {
            get
            {
                return ResourceCache.Localize("Announcement_date_is_required");
            }
        }
        public static string Facility_is_required
        {
            get
            {
                return ResourceCache.Localize("Facility_is_required");
            }
        }
        public static string Facility_should_contain_3_characters_can_not_exceed_30_characters
        {
            get
            {
                return ResourceCache.Localize("Facility_should_contain_3_characters_can_not_exceed_30_characters");
            }
        }
        public static string Facility_description_should_not_exceed_250_characters
        {
            get
            {
                return ResourceCache.Localize("Facility_description_should_not_exceed_250_characters");
            }
        }
        public static string Activity_is_required
        {
            get
            {
                return ResourceCache.Localize("Activity_is_required");
            }
        }
        public static string Activity_should_contain_3_characters_can_not_exceed_20_characters
        {
            get
            {
                return ResourceCache.Localize("Activity_should_contain_3_characters_can_not_exceed_20_characters");
            }
        }
        public static string Activity_description_should_not_exceed_300_characters
        {
            get
            {
                return ResourceCache.Localize("Activity_description_should_not_exceed_300_characters");
            }
        }
        public static string PleaseSelectAcademicYear
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectAcademicYear");
            }
        }
        public static string PleaseSelectClass
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectClass");
            }
        }
        public static string Please_enter_class_strength_to_all_section_of_selected_class
        {
            get
            {
                return ResourceCache.Localize("Please_enter_class_strength_to_all_section_of_selected_class");
            }
        }
        public static string Please_enter_class_strength
        {
            get
            {
                return ResourceCache.Localize("Please_enter_class_strength");
            }
        }
        public static string Fee_Category_is_required
        {
            get
            {
                return ResourceCache.Localize("Fee_Category_is_required");
            }
        }
        public static string Fee_type_should_not_exceed_30_characters﻿
        {
            get
            {
                return ResourceCache.Localize("Fee_type_should_not_exceed_30_characters﻿");
            }
        }
        public static string Feeisrequired
        {
            get
            {
                return ResourceCache.Localize("Feeisrequired");
            }
        }
        public static string SelectFeeCategory
        {
            get
            {
                return ResourceCache.Localize("SelectFeeCategory");
            }
        }
        public static string Last_date_is_required
        {
            get
            {
                return ResourceCache.Localize("Last_date_is_required");
            }
        }
        public static string Refund_amount_is_required
        {
            get
            {
                return ResourceCache.Localize("Refund_amount_is_required");
            }
        }
        public static string Net_amount_is_required
        {
            get
            {
                return ResourceCache.Localize("Net_amount_is_required");
            }
        }
        public static string Descriptionisrequired
        {
            get
            {
                return ResourceCache.Localize("Descriptionisrequired");
            }
        }
        public static string Room_number_is_required
        {
            get
            {
                return ResourceCache.Localize("Room_number_is_required");
            }
        }
        public static string Room_number_should_not_exceed_20_characters
        {
            get
            {
                return ResourceCache.Localize("Room_number_should_not_exceed_20_characters");
            }
        }
        public static string Room_capacity_is_required
        {
            get
            {
                return ResourceCache.Localize("Room_capacity_is_required");
            }
        }
        public static string Exam_name_is_required
        {
            get
            {
                return ResourceCache.Localize("Exam_name_is_required");
            }
        }
        public static string Exam_name_should_contain_3_characters_can_not_exceed_30_characters
        {
            get
            {
                return ResourceCache.Localize("Exam_name_should_contain_3_characters_can_not_exceed_30_characters");
            }
        }
        public static string PleaseSelectSection
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectSection");
            }
        }
        public static string PleaseSelectSubjects
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectSubjects");
            }
        }
        public static string Maximum_mark_is_required
        {
            get
            {
                return ResourceCache.Localize("Maximum_mark_is_required");
            }
        }
        public static string Minimum_mark_is_required
        {
            get
            {
                return ResourceCache.Localize("Minimum_mark_is_required");
            }
        }
        public static string PleaseSelectFaculty
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectFaculty");
            }
        }
        public static string Language_Display_Name_is_Required
        {
            get
            {
                return ResourceCache.Localize("Language_Display_Name_is_Required");
            }
        }
        public static string Culture_Name_is_Required
        {
            get
            {
                return ResourceCache.Localize("Culture_Name_is_Required");
            }
        }
        public static string Culture_Code_is_Required
        {
            get
            {
                return ResourceCache.Localize("Culture_Code_is_Required");
            }
        }
        public static string Country_Code_is_Required
        {
            get
            {
                return ResourceCache.Localize("Country_Code_is_Required");
            }
        }
        public static string Status_is_Required
        {
            get
            {
                return ResourceCache.Localize("Status_is_Required");
            }
        }
        public static string Resource_Type_is_Required
        {
            get
            {
                return ResourceCache.Localize("Resource_Type_is_Required");
            }
        }
        public static string Resource_Value_is_Required
        {
            get
            {
                return ResourceCache.Localize("Resource_Value_is_Required");
            }
        }
        public static string Resource_Name_is_Required
        {
            get
            {
                return ResourceCache.Localize("Resource_Name_is_Required");
            }
        }
        public static string PleaseSelectExam
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectExam");
            }
        }
        public static string Please_enter_schedule_name
        {
            get
            {
                return ResourceCache.Localize("Please_enter_schedule_name");
            }
        }
        public static string Schedule_name_should_not_exceed_20_characters
        {
            get
            {
                return ResourceCache.Localize("Schedule_name_should_not_exceed_20_characters");
            }
        }
        public static string Please_select_subject_to_all_fields
        {
            get
            {
                return ResourceCache.Localize("Please_select_subject_to_all_fields");
            }
        }
        public static string Please_select_date_to_all_fields
        {
            get
            {
                return ResourceCache.Localize("Please_select_date_to_all_fields");
            }
        }
        public static string Please_select_schedule_to_all_fields
        {
            get
            {
                return ResourceCache.Localize("Please_select_schedule_to_all_fields");
            }
        }
        public static string Roll_number_format_is_required
        {
            get
            {
                return ResourceCache.Localize("Roll_number_format_is_required");
            }
        }
        public static string Roll_number_should_not_exceed_10_characters
        {
            get
            {
                return ResourceCache.Localize("Roll_number_should_not_exceed_10_characters");
            }
        }
        public static string PleaseSelectDate
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectDate");

            }
        }
        public static string Please_select_attenance_status_to_all_faculty
        {
            get
            {
                return ResourceCache.Localize("Please_select_attenance_status_to_all_faculty");
            }
        }
        public static string Please_select_start_roll_number
        {
            get
            {
                return ResourceCache.Localize("Please_select_start_roll_number");
            }
        }
        public static string Please_select_end_roll_number
        {
            get
            {
                return ResourceCache.Localize("Please_select_end_roll_number");
            }
        }
        public static string Please_select_room_number
        {
            get
            {
                return ResourceCache.Localize("Please_select_room_number");
            }
        }
        public static string PleaseSelectSchedule
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectSchedule");
            }
        }
        public static string PleaseSelectStudent
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectStudent");
            }
        }
        public static string PleaseEnterAchievement
        {
            get
            {
                return ResourceCache.Localize("PleaseEnterAchievement");
            }
        }
        public static string PleaseEnterDescription
        {
            get
            {
                return ResourceCache.Localize("PleaseEnterDescription");
            }
        }
        public static string PleaseSelectEmployee
        {
            get
            {
                return ResourceCache.Localize("PleaseSelectEmployee");
            }
        }
        public static string PleaseEnterPeopleInvolved
        {
            get
            {
                return ResourceCache.Localize("PleaseEnterPeopleInvolved");
            }
        }
        public static string Grade_Name_is_required
        {
            get
            {
                return ResourceCache.Localize("Grade_Name_is_required");
            }
        }
        public static string Grade_type_is_required { get { return ResourceCache.Localize("Grade_type_is_required"); } }
        public static string PleaseSelectRoom { get { return ResourceCache.Localize("PleaseSelectRoom"); } }
        public static string PleaseSelectFromdate { get { return ResourceCache.Localize("PleaseSelectFromdate"); } }
        public static string PleaseSelectTodate { get { return ResourceCache.Localize("PleaseSelectTodate"); } }
        public static string PleaseSelectStatus { get { return ResourceCache.Localize("PleaseSelectStatus"); } }
        public static string PleaseEnterReason { get { return ResourceCache.Localize("PleaseEnterReason"); } }
        public static string PleaseSelectStudentName { get { return ResourceCache.Localize("PleaseSelectStudentName"); } }
        public static string PleaseSelectReportType { get { return ResourceCache.Localize("PleaseSelectReportType"); } }
        public static string PleaseSelectmonth { get { return ResourceCache.Localize("PleaseSelectmonth"); } }
        public static string PleaseSelectEmployeeName { get { return ResourceCache.Localize("PleaseSelectEmployeeName"); } }
        public static string PleaseSelectEmployeeCategory { get { return ResourceCache.Localize("PleaseSelectEmployeeCategory"); } }
        public static string PleaseSelectExamType { get { return ResourceCache.Localize("PleaseSelectExamType"); } }
        public static string PleaseSelectFeeCategory { get { return ResourceCache.Localize("PleaseSelectFeeCategory"); } }
        public static string PleaseSelectPaymentType { get { return ResourceCache.Localize("PleaseSelectPaymentType"); } }
        public static string Please_select_fee_particular { get { return ResourceCache.Localize("Please_select_fee_particular"); } }
        public static string PleaseEnterFeedback { get { return ResourceCache.Localize("PleaseEnterFeedback"); } }
        public static string Please_enter_to_mail { get { return ResourceCache.Localize("Please_enter_to_mail"); } }
        public static string Please_enter_subject { get { return ResourceCache.Localize("Please_enter_subject"); } }
        public static string Please_enter_message { get { return ResourceCache.Localize("Please_enter_message"); } }

    }
}

