﻿using System;
using System.Configuration;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
class SMS
{
    public static void Main()
    {
        // Find your Account Sid and Auth Token at twilio.com/user/account
        //string AccountSid = "AC54dd74e9867b6bc44fd3b120243ffddb";
        //string AuthToken = "9d2d3b31af5e9cae6972dde4ea855c48";
        //TwilioClient.Init(AccountSid, AuthToken);
        ////var message = twilio.SendMessage("+1321-558-7571", "+919003206099", "Tomorrow holiday from Ace Class Room", "");
        //Console.WriteLine(message.Sid);
        //if (message.RestException != null)
        //{
        //    var error = message.RestException.Message;
        //    // handle the error ...
        //}
    }
    public static void Main(string module,string ToNumber,string sub, string Description)
    {
        
        string AccountSid = ConfigurationManager.AppSettings["ACCOUNT_SID"];
        string AuthToken = ConfigurationManager.AppSettings["AUTH_TOKEN"];
        string FromPhoneNum = ConfigurationManager.AppSettings["FromTwilioNumber"];

        TwilioClient.Init(AccountSid, AuthToken);
        var from = new PhoneNumber(FromPhoneNum);
        string PhCode = ConfigurationManager.AppSettings["PhoneNumberCode"];
       
        if (!string.IsNullOrEmpty(ToNumber))
        {
            if (ToNumber.Contains(","))
            {
                string[] bccs = ToNumber.Split(',');
                for (int i = 0; i < bccs.Length; i++)
                {
                    string TomobNum = bccs[i];
                    
                    string Tomob = PhCode + TomobNum;
                    var to = new PhoneNumber(Tomob);
                    var message = MessageResource.Create(
                            to: to,
                            from: from,
                            body: sub + " " + Description);
                    Console.WriteLine(message.Sid);

                }
            }
            else
            {
                string TomobNum = ToNumber;
                string Tomob = PhCode + TomobNum;
                var to = new PhoneNumber(Tomob);
                var message = MessageResource.Create(
                        to: to,
                        from: from,
                        body: sub + " " + Description);
                Console.WriteLine(message.Sid);
            }       

        }
        
    }
}
