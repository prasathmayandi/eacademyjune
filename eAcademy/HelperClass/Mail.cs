﻿using eAcademy.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
namespace eAcademy.HelperClass
{
    public class Mail
    {
        
        public static void SendMail(string fromDisplayName, string fromEmailAddress, string toEmailAddress, string subject, string body, bool trf,string PlaceName,string UserName, string UserId)
        {

            EmailNotificationServices Object_EN = new EmailNotificationServices();
            MailAddress from = new MailAddress(fromEmailAddress, fromDisplayName);
            MailAddress to = new MailAddress(toEmailAddress);
            MailMessage mailMessage = new MailMessage(from, to);
            mailMessage.Body = body.ToString();
            mailMessage.IsBodyHtml = trf;
            mailMessage.Subject = subject;
            ////////////This method using send email from smtpgo account////////////////////////////
            //SmtpClient client = new SmtpClient();
            ////client.UseDefaultCredentials = false;
            //client.Send(mailMessage);
            //string SendingStatus ="Sent Successfully";
            //Object_EN.AddEmailNotification(PlaceName,fromEmailAddress,toEmailAddress,SendingStatus,UserName,UserId);

            //AddEmailNotification(string PlaceName, string FromEmailId, string ToEmailId, string UserName, string UserId, string SendingStatus)
            ////////////This method using send email from gmail account////////////////////////////

            SmtpClient smtp = new SmtpClient();
            smtp.Port = 587;
            smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential
                 ("gpriya@acestranetworks.com", "priyalusu");
            //Or your Smtp Email ID and Password
            smtp.EnableSsl = true;
            smtp.Send(mailMessage);
            string SendingStatus = "Sent Successfully";
            Object_EN.AddEmailNotification(PlaceName, fromEmailAddress, toEmailAddress, SendingStatus, UserName, UserId);

            //var mail = new MailMessage();
            //var smtpServer = new SmtpClient(ConfigurationManager.AppSettings["sendGridSmtpServer"]);
            //mail.From = new MailAddress(ConfigurationManager.AppSettings["sendGridFrom"], fromDisplayName);
            //mail.To.Add(toEmailAddress);
            //smtpServer.Port = Convert.ToInt32(ConfigurationManager.AppSettings["sendGridPort"]);
            //smtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["sendGridUser"], ConfigurationManager.AppSettings["sendGridPassword"]);
            //smtpServer.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
            //mail.Subject = subject;
            //mail.Body = body;
            //mail.IsBodyHtml = trf;
            //smtpServer.Send(mail);
        }

        public static void SendMultipleMail1(string fromDisplayName, string fromEmailAddress, string toEmailAddress, string subject, string body, string PlaceName, string UserName, string UserId)
        {
            EmailNotificationServices Object_EN = new EmailNotificationServices();
            //MailMessage mailMessage = new MailMessage();
            //mailMessage.From = new MailAddress(fromEmailAddress);
            //mailMessage.Body = body.ToString();
            //mailMessage.IsBodyHtml = true;
            //mailMessage.Subject = subject;
            //if (!string.IsNullOrEmpty(toEmailAddress))
            //{
            //    if (toEmailAddress.Contains(","))
            //    {
            //        string[] bccs = toEmailAddress.Split(',');
            //        for (int i = 0; i < bccs.Length; i++)
            //        {
            //            mailMessage.Bcc.Add(new MailAddress(bccs[i]));
            //        }
            //    }
            //    else
            //    {
            //        mailMessage.Bcc.Add(new MailAddress(toEmailAddress));
            //    }
            //}
            ////////////This method using send email from smtpgo account////////////////////////////
            //SmtpClient client = new SmtpClient();
            //client.Send(mailMessage);
            //string SendingStatus = "Sent Successfully";
            //Object_EN.AddEmailNotification(PlaceName, fromEmailAddress, toEmailAddress, SendingStatus, UserName, UserId);


            ////////////This method using send email from gmail account////////////////////////////

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(fromEmailAddress);
            mail.Body = body.ToString();
            mail.IsBodyHtml = true;
            mail.Subject = subject;
            if (!string.IsNullOrEmpty(toEmailAddress))
            {
                if (toEmailAddress.Contains(","))
                {
                    string[] bccs = toEmailAddress.Split(',');
                    for (int i = 0; i < bccs.Length; i++)
                    {
                        mail.Bcc.Add(new MailAddress(bccs[i]));
                    }
                }
                else
                {
                    mail.Bcc.Add(new MailAddress(toEmailAddress));
                }
            }
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 587;
            smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential
                 ("gpriya@acestranetworks.com", "priyalusu");
            //Or your Smtp Email ID and Password
            smtp.EnableSsl = true;
            smtp.Send(mail);
            string SendingStatus = "Sent Successfully";
            Object_EN.AddEmailNotification(PlaceName, fromEmailAddress, toEmailAddress, SendingStatus, UserName, UserId);

            //var mail = new MailMessage();
            //var smtpServer = new SmtpClient(ConfigurationManager.AppSettings["sendGridSmtpServer"]);
            //mail.From = new MailAddress(ConfigurationManager.AppSettings["sendGridFrom"], fromDisplayName);
            //mail.To.Add(toEmailAddress);
            //smtpServer.Port = Convert.ToInt32(ConfigurationManager.AppSettings["sendGridPort"]);
            //smtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["sendGridUser"], ConfigurationManager.AppSettings["sendGridPassword"]);
            //smtpServer.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
            //mail.Subject = subject;
            //mail.Body = body;
            ////mail.IsBodyHtml = trf;
            //smtpServer.Send(mail);
        }
        public static void SendMultipleMail(string fromDisplayName, string fromEmailAddress, string toEmailAddress, string subject, string body, bool trf, string fileName ,string PlaceName,string UserName, string UserId)
        {
            EmailNotificationServices Object_EN = new EmailNotificationServices();
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(fromEmailAddress);
            mailMessage.Body = body.ToString();
            mailMessage.IsBodyHtml = trf;
            mailMessage.Subject = subject;
            if (!string.IsNullOrEmpty(toEmailAddress))
            {
                if (toEmailAddress.Contains(","))
                {
                    string[] bccs = toEmailAddress.Split(',');
                    for (int i = 0; i < bccs.Length; i++)
                    {
                        mailMessage.Bcc.Add(new MailAddress(bccs[i]));
                    }
                }
                else
                {
                    mailMessage.Bcc.Add(new MailAddress(toEmailAddress));
                }

                if (fileName != null && fileName != "")
                {
                    mailMessage.Attachments.Add(new Attachment(fileName));
                }
            }
            ////////////This method using send email from smtpgo account////////////////////////////
            //SmtpClient client = new SmtpClient();
            //client.Send(mailMessage);
            //string SendingStatus = "Sent Successfully";
            //Object_EN.AddEmailNotification(PlaceName, fromEmailAddress, toEmailAddress, SendingStatus, UserName, UserId);


            ////////////This method using send email from gmail account////////////////////////////

            SmtpClient smtp = new SmtpClient();
            smtp.Port = 587;
            smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential
                 ("gpriya@acestranetworks.com", "priyalusu");
            //Or your Smtp Email ID and Password
            smtp.EnableSsl = true;
            smtp.Send(mailMessage);
            string SendingStatus = "Sent Successfully";
            Object_EN.AddEmailNotification(PlaceName, fromEmailAddress, toEmailAddress, SendingStatus, UserName, UserId);


            //var mail = new MailMessage();
            //var smtpServer = new SmtpClient(ConfigurationManager.AppSettings["sendGridSmtpServer"]);
            //mail.From = new MailAddress(ConfigurationManager.AppSettings["sendGridFrom"], fromDisplayName);

            //mail.To.Add(toEmailAddress);
            //smtpServer.Port = Convert.ToInt32(ConfigurationManager.AppSettings["sendGridPort"]);
            //smtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["sendGridUser"], ConfigurationManager.AppSettings["sendGridPassword"]);
            //smtpServer.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);

            //mail.Subject = subject;
            //mail.Body = body;
            //mail.IsBodyHtml = true;

            //if (!string.IsNullOrEmpty(toEmailAddress))
            //{
            //    if (toEmailAddress.Contains(","))
            //    {
            //        string[] bccs = toEmailAddress.Split(',');
            //        for (int i = 0; i < bccs.Length; i++)
            //        {
            //            if (bccs[i] == "")
            //            {
            //            }
            //            else
            //            {
            //                mail.Bcc.Add(new MailAddress(bccs[i]));
            //                //if (fileName != null && fileName != "")
            //                //{
            //                //    mail.Attachments.Add(new Attachment(fileName));
            //                //}
            //            }
            //        }

            //        if (fileName != null && fileName != "")
            //        {
            //            mail.Attachments.Add(new Attachment(fileName));
            //        }
            //        if (toEmailAddress.Contains("@"))
            //        {
            //            smtpServer.Send(mail);
            //        }
            //    }
            //    else
            //    {
            //        mail.Bcc.Add(new MailAddress(toEmailAddress));
            //        if (fileName != null && fileName != "")
            //        {
            //            mail.Attachments.Add(new Attachment(fileName));
            //        }
            //        if (toEmailAddress.Contains("@"))
            //        {
            //            smtpServer.Send(mail);
            //        }
            //    }
            //}               
        }
        public static void SendMailWithFile(string fromDisplayName, string fromEmailAddress, string toEmailAddress, string subject, string body, bool trf, string fileName, string PlaceName, string UserName, string UserId)
        {
            EmailNotificationServices Object_EN = new EmailNotificationServices();
            MailAddress from = new MailAddress(fromEmailAddress, fromDisplayName);
            MailAddress to = new MailAddress(toEmailAddress);
            MailMessage mailMessage = new MailMessage(from, to);
            mailMessage.Body = body.ToString();
            mailMessage.IsBodyHtml = trf;
            mailMessage.Subject = subject;
            if (fileName != null && fileName != "")
            {
                mailMessage.Attachments.Add(new Attachment(fileName));
            }

            ////////////This method using send email from smtpgo account////////////////////////////
            //SmtpClient client = new SmtpClient();
            //client.Send(mailMessage);
            //string SendingStatus = "Sent Successfully";
            //Object_EN.AddEmailNotification(PlaceName, fromEmailAddress, toEmailAddress, SendingStatus, UserName, UserId);

            ////////////This method using send email from gmail account////////////////////////////
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 587;
            smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential
                 ("gpriya@acestranetworks.com", "priyalusu");
            //Or your Smtp Email ID and Password
            smtp.EnableSsl = true;
            smtp.Send(mailMessage);
            string SendingStatus = "Sent Successfully";
            Object_EN.AddEmailNotification(PlaceName, fromEmailAddress, toEmailAddress, SendingStatus, UserName, UserId);


            //var mail = new MailMessage();
            //var smtpServer = new SmtpClient(ConfigurationManager.AppSettings["sendGridSmtpServer"]);
            //mail.From = new MailAddress(ConfigurationManager.AppSettings["sendGridFrom"], fromDisplayName);
            //mail.To.Add(toEmailAddress);
            //smtpServer.Port = Convert.ToInt32(ConfigurationManager.AppSettings["sendGridPort"]);
            //smtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["sendGridUser"], ConfigurationManager.AppSettings["sendGridPassword"]);
            //smtpServer.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
            //mail.Subject = subject;
            //mail.Body = body;
            //mail.IsBodyHtml = trf;
            //if (fileName != null && fileName != "")
            //{
            //    mail.Attachments.Add(new Attachment(fileName));
            //}
            //smtpServer.Send(mail);
        }
        public static void SendMultipleMail(string fromDisplayName, string fromEmailAddress, string toEmailAddress, string subject, string body, bool trf, string PlaceName, string UserName, string UserId)
        {
            try
            {
                EmailNotificationServices Object_EN = new EmailNotificationServices();
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(fromEmailAddress);
                mailMessage.Body = body.ToString();
                mailMessage.IsBodyHtml = true;
                mailMessage.Subject = subject;
                if (!string.IsNullOrEmpty(toEmailAddress))
                {
                    if (toEmailAddress.Contains(","))
                    {
                        string[] bccs = toEmailAddress.Split(',');
                        for (int i = 0; i < bccs.Length; i++)
                        {
                            mailMessage.Bcc.Add(new MailAddress(bccs[i]));
                        }
                    }
                    else
                    {
                        mailMessage.Bcc.Add(new MailAddress(toEmailAddress));
                    }
                }
                ////////////This method using send email from smtpgo account////////////////////////////

                //SmtpClient client = new SmtpClient();
                //client.Send(mailMessage);
                //string SendingStatus = "Sent Successfully";
                //Object_EN.AddEmailNotification(PlaceName, fromEmailAddress, toEmailAddress, SendingStatus, UserName, UserId);

                ////////////This method using send email from gmail account////////////////////////////
                SmtpClient smtp = new SmtpClient();
                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential
                     ("gpriya@acestranetworks.com", "priyalusu");
                //Or your Smtp Email ID and Password
                smtp.EnableSsl = true;
                smtp.Send(mailMessage);
                string SendingStatus = "Sent Successfully";
                Object_EN.AddEmailNotification(PlaceName, fromEmailAddress, toEmailAddress, SendingStatus, UserName, UserId);
                //var mail = new MailMessage();
                //var smtpServer = new SmtpClient(ConfigurationManager.AppSettings["sendGridSmtpServer"]);
                //mail.From = new MailAddress(ConfigurationManager.AppSettings["sendGridFrom"], fromDisplayName);
                //mail.To.Add(toEmailAddress);
                //smtpServer.Port = Convert.ToInt32(ConfigurationManager.AppSettings["sendGridPort"]);
                //smtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["sendGridUser"], ConfigurationManager.AppSettings["sendGridPassword"]);
                //smtpServer.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                //mail.Subject = subject;
                //mail.Body = body;
                //mail.IsBodyHtml = trf;
                //smtpServer.Send(mail);
            }
            catch (Exception e)
            {
                string msg = e.Message;
            }
        }


    }
}