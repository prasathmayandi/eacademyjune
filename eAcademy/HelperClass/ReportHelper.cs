﻿using System.Collections;
using System.Collections.Generic;
using System.Web.WebPages.Html;
namespace eAcademy.HelperClass
{
    public class ReportHelper
    {
        public IEnumerable getPaymentType()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select", Value = "" });
            li.Add(new SelectListItem { Text = "Single", Value = "1" });
            li.Add(new SelectListItem { Text = "Term", Value = "2" });
            return li;
        }
        public IEnumerable getrtype()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select", Value = "" });
            li.Add(new SelectListItem { Text = "Daily", Value = "1" });
            li.Add(new SelectListItem { Text = "From/To", Value = "2" });
            li.Add(new SelectListItem { Text = "Monthly", Value = "3" });
            return li;
        }
        public IEnumerable getFeeParticulars()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select", Value = "" });
            li.Add(new SelectListItem { Text = "Paid", Value = "1" });
            li.Add(new SelectListItem { Text = "Due", Value = "2" });

            return li;
        }
    }
}