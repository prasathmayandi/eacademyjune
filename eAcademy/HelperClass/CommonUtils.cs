﻿using System;
namespace eAcademy.HelperClass
{
    public static class CommonUtils
    {
        /// <summary>
        /// Calculates number of business days, taking into account:
        ///  - weekends (Saturdays and Sundays)
        ///  - bank holidays in the middle of the week
        /// </summary>
        /// <param name="firstDay">First day in the time interval</param>
        /// <param name="lastDay">Last day in the time interval</param>
        /// <param name="schoolHolidays">List of bank holidays excluding weekends</param>
        /// <returns>Number of school days during the 'span'</returns>
        //public static int SchoolDaysUntil(this DateTime firstDay, DateTime lastDay, params DateTime[] schoolHolidays) 
        public static int SchoolDaysUntil(this DateTime firstDay, DateTime lastDay, int schoolHolidays)
        {
            firstDay = firstDay.Date;
            lastDay = lastDay.Date;
            if (firstDay > lastDay)
                throw new ArgumentException("Incorrect last day " + lastDay);
            TimeSpan span = lastDay - firstDay;
            int schoolDays = span.Days + 1;
            int fullWeekCount = schoolDays / 7;
            // find out if there are weekends during the time exceedng the full weeks
            if (schoolDays > fullWeekCount * 7)
            {
                // we are here to find out if there is a 1-day or 2-days weekend
                // in the time interval remaining after subtracting the complete weeks
                ////int firstDayOfWeek = (int)firstDay.DayOfWeek;
                ////int lastDayOfWeek = (int)lastDay.DayOfWeek;
                int firstDayOfWeek = firstDay.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)firstDay.DayOfWeek;
                int lastDayOfWeek = lastDay.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)lastDay.DayOfWeek;
                if (lastDayOfWeek < firstDayOfWeek)
                    lastDayOfWeek += 7;
                if (firstDayOfWeek <= 6)
                {
                    if (lastDayOfWeek >= 7)// Both Saturday and Sunday are in the remaining time interval
                        schoolDays -= 2;
                    else if (lastDayOfWeek >= 6)// Only Saturday is in the remaining time interval
                        schoolDays -= 1;
                }
                else if (firstDayOfWeek <= 7 && lastDayOfWeek >= 7)// Only Sunday is in the remaining time interval
                    schoolDays -= 1;
            }
            // subtract the weekends during the full weeks in the interval
            schoolDays -= fullWeekCount + fullWeekCount - schoolHolidays;
            // subtract the number of bank holidays during the time interval
            //foreach (DateTime schoolHoliday in schoolHolidays)
            //{
            //    DateTime bh = schoolHoliday.Date;
            //    if (firstDay <= bh && bh <= lastDay)
            //        --schoolDays;
            //}
            return schoolDays;
        }
    }
}