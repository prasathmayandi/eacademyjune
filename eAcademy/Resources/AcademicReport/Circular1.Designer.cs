﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eAcademy.Resources.AcademicReport {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Circular {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Circular() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("eAcademy.Resources.AcademicReport.Circular", typeof(Circular).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AcademicYear.
        /// </summary>
        public static string AcademicYear {
            get {
                return ResourceManager.GetString("AcademicYear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select academic year.
        /// </summary>
        public static string AcademicYearRequired {
            get {
                return ResourceManager.GetString("AcademicYearRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select date.
        /// </summary>
        public static string DailyDateRequired {
            get {
                return ResourceManager.GetString("DailyDateRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SelectDate.
        /// </summary>
        public static string Dates {
            get {
                return ResourceManager.GetString("Dates", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to FromDate.
        /// </summary>
        public static string FromDate {
            get {
                return ResourceManager.GetString("FromDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select from date.
        /// </summary>
        public static string FromDateRequired {
            get {
                return ResourceManager.GetString("FromDateRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Month.
        /// </summary>
        public static string Month {
            get {
                return ResourceManager.GetString("Month", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select month.
        /// </summary>
        public static string MonthRequired {
            get {
                return ResourceManager.GetString("MonthRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ReportType.
        /// </summary>
        public static string ReportType {
            get {
                return ResourceManager.GetString("ReportType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select report type.
        /// </summary>
        public static string ReportTypeRequired {
            get {
                return ResourceManager.GetString("ReportTypeRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ToDate.
        /// </summary>
        public static string ToDate {
            get {
                return ResourceManager.GetString("ToDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please select to date.
        /// </summary>
        public static string ToDateRequired {
            get {
                return ResourceManager.GetString("ToDateRequired", resourceCulture);
            }
        }
    }
}
