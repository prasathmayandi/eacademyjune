﻿using System.Web;
using System.Web.Optimization;

namespace eAcademy
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            //bundles.Add(new ScriptBundle("~/bundles/myjs").Include(
            //   "~/js/vendor/bootstrap.min.js",
            //   "~/js/main.js",
            //   "~/js/jquery.filter_input.js",
            //   "~/js/chosen.jquery.js",
            //   "~/js/jQueryTimepicker.js",
            //   "~/js/jquery.dataTables.js"
            //   ));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new StyleBundle("~/AfterLogin/css").Include(
               "~/Content/jquery.jqGrid/ui.jqgrid.css",
               "~/css/plugins/plugin.css",
               "~/css/chosen.css",
               "~/css/print.css"
           ));
            bundles.Add(new StyleBundle("~/AfterLogin2/css").Include(
           "~/css/jquery.dataTables.css",
                 "~/css/fullcalendar.css"
                //"~/css/fullcalendar.print.css"
          ));

            bundles.Add(new StyleBundle("~/AfterLogin3/css").Include(

           "~/css/breadcrumb.css",
           "~/css/AdminProcess/admin-process-style.css",

           "~/css/OfflineForm/preadmissionOffline.css",
           "~/css/OfflineForm/step-formOffline.css",
           "~/css/OfflineForm/form-oneOffline.css",
           "~/css/OfflineForm/form-one2.css",
           "~/css/style123.css",
           "~/css/main1.css",

           "~/css/newmain.css",
           "~/css/adminSitemap.css",
           "~/css/HolidayDatepicker.css",
           "~/css/bootstrap-table/bootstrap-table.css"

           ));

            bundles.Add(new ScriptBundle("~/bundles/AfterLoginJs").Include(
              "~/js/jquery-1.10.2.js",
              "~/js/jquery-ui1.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/AfterLoginJs2").Include(
               "~/Scripts/i18n/grid.locale-en.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/AfterLoginJs3").Include(
              "~/js/jqGridExportToExcel.js",
              "~/js/TodoList.js",
               "~/js/print.js",
              "~/js/main.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/AfterLoginJs4").Include(
            "~/js/FormValidation.js",
             "~/js/jquery.filter_input.js",
            "~/js/chosen.jquery.js",
             "~/js/jQueryTimepicker.js",
            "~/js/jquery.dataTables.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/AfterLoginJs5").Include(
             "~/js/jquery.fileupload.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/AfterLoginJs6").Include(
            "~/js/funtiontimetable.js",
            "~/js/Report.js",
            "~/js/gistfile1.js",
            "~/js/marque.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/AfterLoginJs7").Include(
              "~/js/Enrollment/form.js",
              "~/js/AdminProcess/AdminProcess.js",
              "~/js/OnlineRegisterJs/Registerform.js",
              //"~/js/OfflineEnrolement/form.js",
               "~/js/OnlineRegisterJs/PrimaryUser.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/AfterLoginJs8").Include(
             "~/js/bootstrap-table/bootstrap-table-export.js",
             "~/js/bootstrap-table/tableExport.js"
             ));

            bundles.Add(new StyleBundle("~/StudentAfterLogin/css").Include(
              "~/css/breadcrumb.css"
            ));

            bundles.Add(new StyleBundle("~/StudentAfterLogin2/css").Include(
             "~/css/Dashboard.css",
             "~/css/style123.css",
             "~/css/main1.css",
             "~/css/newmain.css",
             "~/css/Nav_Header.css",
             "~/css/studentSitemap.css",
           "~/css/HolidayDatepicker.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/StudentAfterLoginJs2").Include(
             "~/js/main.js",
            "~/Scripts/i18n/grid.locale-en.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/StudentAfterLoginJs3").Include(
                "~/js/jquery.fileupload.js",
             "~/js/jquery.filter_input.js",
             "~/js/chosen.jquery.js",
             "~/js/jQueryTimepicker.js",
             "~/js/jquery.dataTables.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/StudentAfterLoginJs4").Include(
             "~/js/Report.js",
            "~/js/print.js",
            "~/js/gistfile1.js",
            "~/js/marque.js"
            ));  
        }
    }
}