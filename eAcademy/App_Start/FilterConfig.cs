﻿using eAcademy.Services;
using System.Web;
using System.Web.Mvc;

namespace eAcademy
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new CheckSessionOutAttribute());
        }
    }
}