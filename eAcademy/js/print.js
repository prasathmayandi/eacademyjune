﻿$(document).ready(function () {



    var schoolname;
    var schoollogo;
    $.getJSON('/Report/getschoolname',
      function (data) {
          schoolname = data.name;
          schoollogo = data.logo
      });
    // Create a jquery plugin that prints the given element.
    jQuery.fn.print = function () {
        // NOTE: We are trimming the jQuery collection down to the
        // first element in the collection.
        if (this.size() > 1) {
            this.eq(0).print();
            return;
        } else if (!this.size()) {
            return;
        }



        // ASSERT: At this point, we know that the current jQuery
        // collection (as defined by THIS), contains only one
        // printable element.

        // Create a random name for the print frame.
 
        var strFrameName = ("printer-" + (new Date()).getTime());
 
        // Create an iFrame with the new name.
        var jFrame = $("<iframe name='" + strFrameName + "'>");
        
        // Hide the frame (sort of) and attach to the body.
        jFrame
        .css("width", "1px")
        .css("height", "1px")
        .css("position", "absolute")
        .css("left", "-9999px")
        .appendTo($("body:first"))
        ;

        // Get a FRAMES reference to the new frame.
        var objFrame = window.open("");//window.frames[strFrameName];

        // Get a reference to the DOM in the new frame.
        var objDoc = objFrame.document;

        // Grab all the style tags and copy to the new
        // document so that we capture look and feel of
        // the current document.

        // Create a temp document DIV to hold the style tags.
        // This is the only way I could find to get the style
        // tags into IE.
        //var jStyleDiv = $("<div>").append(
        //$("style").clone()
        //);

        // Write the HTML for the document. In this, we will
        // write out the HTML of the current element.
        objDoc.open();
        objDoc.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
        objDoc.write("<html>");
        objDoc.write("<head>");


       // objDoc.write("<link rel=\"stylesheet\"  href=\"http://localhost:55654/css/print.css\" type=\"text/css\" media=\"print\" />");
        // objDoc.write("<link href=\"~/css/plugins/print.css\" type=\"text/css\" media=\"print\" rel=\"stylesheet\"/>");
        objDoc.write("<title>");
        //objDoc.write(document.title);
        objDoc.write("</title>");
        objDoc.write("<style>");
        objDoc.write("@media print{@import url('http://localhost:55654/css/bootstrap.min.css');::-webkit-input-placeholder {color: transparent;}:-moz-placeholder {color: transparent} ::-moz-placeholder {color: transparent;} :-ms-input-placeholder {color: transparent;}.noprint {display:none; } .Active_Print{text-align:center;} img{ width:50px;height:50px; } #theImg{float:right; height:98px !important;width:110px !important;margin-top:-80px !important; margin-bottom:10px !important;} table {      align-content:center; page-break-inside:auto; width:100%;height:auto; border-collapse:collapse; } tr{ page-break-inside:avoid; page-break-after:auto; } thead { align-content:center;display:table-header-group; }th { align-content:center; }tfoot { display:table-footer-group;} td { width:auto; font-size:12pt; padding:1px; } .empalin{margin-left:-250px !important;}}");
        objDoc.write("</style>");
        //objDoc.write('<link rel=\'stylesheet\'  href=\'http://localhost:55654/css/print.css\' type=\'text/css\' media=\'print\' />');
        objDoc.write("</head>");
        objDoc.write("<body>");
        //objDoc.write(jStyleDiv.html());
 
        // width=\"50\" height=\"50\"
        objDoc.write('<center><img  src=\'http://localhost:55654/img/' + schoollogo + '\'  /></img><h1>' + schoolname + '</h1></center>');
 
        //objDoc.write("");
        //objDoc.write("<hr>");
        objDoc.write(this.html());


        objDoc.write("</body>");
        objDoc.write("</html>");
        objDoc.close();

        // Print the document.
        objFrame.focus();
        objFrame.print();
        //objFrame.onload = function () {
        //    objFrame.focus();
        //    objFrame.print();
        //    jFrame.remove();
        //};

        // Have the frame remove itself in about a minute so that
        // we don't build up too many of these frames.
        setTimeout(
        function () {
            jFrame.remove();
        },
        (60 * 1000)
        );
        $('#Report_head').hide();
    }
});
