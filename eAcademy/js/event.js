﻿$(document).ready(function () {

    //Event Calendar for Student Dashboard
    $.getJSON("/DashboardStudent/date1",

             function (data) {
                 var disabledDay = [];
                 var events = [];
                 var des = [];
                 var d = [];
                 var m = [];
                 var y = [];
                 $.each(data, function (i, v) {
                     //console.log("table");

                     var singledate = v.dat;

                     var changeddate = singledate.match(/\d+/g).map(function (s) { return new Date(+s); });
                     var date = new Date(changeddate);

                     var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
                     m[i] = mnth;

                     var day = ("0" + date.getDate()).slice(-2);
                     d[i] = day;

                     y[i] = date.getFullYear();

                     //alert("month" + mnth + "day=" + day+"year="+date.getFullYear());
                     var FromateDate = [mnth, day, date.getFullYear()].join("/");

                     disabledDay[i] = FromateDate;
                     events[i] = v.name;
                     des[i] = v.des;

                 });

                 //["10/20/2014", "10/21/2014", "11/15/2014", "11/17/2014"];

                 var holydays = disabledDay;
                 var whosebday = events;
                 var description = des;

                 //alert(holydays);
                 function highlightDays(date) {
                     for (var i = 0; i < holydays.length; i++) {
                         if (new Date(holydays[i]).toString() == date.toString()) {
                             return [false, 'Highlighted', whosebday[i]];
                             alert(whosebday[i]);
                         }
                     }
                     var day = date.getDay();
                     return [(day > 0), ''];

                 }
                 var eventsonecalendar = [], k;
                 for (var k = 0; k < disabledDay.length; k += 1) {


                     eventsonecalendar.push({ title: whosebday[k], description: description[k], datetime: new Date(y[k], m[k], d[k]) });


                 }
                 alert("d");

                 $('#ecal1').eCalendar({

                     weekDays: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                     months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                              'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                     textArrows: { previous: '<', next: '>' },
                     eventTitle: 'Events',
                     url: '',
                     events: eventsonecalendar

                 });

             });
    //Event Calendar for Parent Dashboard
    $.getJSON("/DashboardParent/date3",
             function (data) {
                 var disabledDay = [];
                 var events = [];
                 var des = [];
                 var d = [];
                 var m = [];
                 var y = [];
                 $.each(data, function (i, v) {
                     //console.log("table");

                     var singledate = v.dat;

                     var changeddate = singledate.match(/\d+/g).map(function (s) { return new Date(+s); });
                     var date = new Date(changeddate);

                     var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
                     m[i] = mnth;

                     var day = ("0" + date.getDate()).slice(-2);
                     d[i] = day;

                     y[i] = date.getFullYear();

                     //alert("month" + mnth + "day=" + day+"year="+date.getFullYear());
                     var FromateDate = [mnth, day, date.getFullYear()].join("/");

                     disabledDay[i] = FromateDate;
                     events[i] = v.name;
                     des[i] = v.des;

                 });

                 //["10/20/2014", "10/21/2014", "11/15/2014", "11/17/2014"];

                 var holydays = disabledDay;
                 var whosebday = events;
                 var description = des;

                 //alert(holydays);
                 function highlightDays(date) {
                     for (var i = 0; i < holydays.length; i++) {
                         if (new Date(holydays[i]).toString() == date.toString()) {
                             return [false, 'Highlighted', whosebday[i]];
                             alert(whosebday[i]);
                         }
                     }
                     var day = date.getDay();
                     return [(day > 0), ''];

                 }
                 var eventsonecalendar = [], k;
                 for (var k = 0; k < disabledDay.length; k += 1) {


                     eventsonecalendar.push({ title: whosebday[k], description: description[k], datetime: new Date(y[k], m[k], d[k]) });


                 }

                 alert("parent");
                 $('#ecal2').eCalendar({

                     weekDays: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                     months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                              'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                     textArrows: { previous: '<', next: '>' },
                     eventTitle: 'Events',
                     url: '',
                     events: eventsonecalendar

                 });


             });






});