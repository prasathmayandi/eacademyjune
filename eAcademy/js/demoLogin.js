﻿$(document).ready(function () {
    $("#parent1").hide();
    $("#parent2").hide();

     $('.tabpage').hide().first().show();
     $('.tabs a').click(function () {
         //$(".pshow").removeClass("display:inline-block;");
         //$(".pshow").addClass("display:none;");

         $('.tabpage').hide();

         var selectedUser = $(this).attr('id');
         if (selectedUser == "header_student")
         {
             $("#student1").show();
         }
         if (selectedUser == "header_parent")
         {
             $("#student1").hide();
             $("#parent1").show();
             $("#parent2").show();
         }
         if (selectedUser == "header_admin") {
             $("#student1").hide();
             $("#admin1").show();
             $("#admin2").show();
         }


        var target = $(this).attr('id').replace('header_', '');
 
        //this will return like 'tabpage_1'

        
        $('#' + target).fadeIn(500);
        $('.tabs a').removeClass('selected');
        $(this).addClass('selected');
    });
});