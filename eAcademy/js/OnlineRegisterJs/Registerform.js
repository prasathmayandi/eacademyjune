﻿$(document).ready(function () {

    var pdfpage = $('#Pdf_page').val();

    if (pdfpage != "" && pdfpage != null && pdfpage != undefined) {
        $("#myLoadingElement").hide();
    }

    //$('#Admission_AcademicYear').change(function () {
    //    var yearid = $('#Admission_AcademicYear').val();
    //    $.getJSON('/Admission/JsonvacuncyClass', { yearid: yearid },
    //           function (data) {
    //               $("#Admission_Class").empty().append('<option value="">Select Class</option>');
    //               $.each(data, function (i, c) {
    //                   $("#Admission_Class").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //               });
    //           });
    //});

    //function getvacancyclass() {
    //    var yearid = $('#Admission_AcademicYear').val();
    //    $.getJSON('/Admission/JsonvacuncyClass', { yearid: yearid },
    //           function (data) {
    //               $("#Admission_Class").empty().append('<option value="">Select Class</option>');
    //               $.each(data, function (i, c) {
    //                   $("#Admission_Class").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //               });
    //           });
    //}
    //$('#AcademicyearId').change(function () {
    //    var yearid = $('#AcademicyearId').val();
    //    $.getJSON('/Admission/JsonvacuncyClass', { yearid: yearid },
    //           function (data) {
    //               $("#AdmissionClass").empty().append('<option value="">Select Class</option>');
    //               $.each(data, function (i, c) {
    //                   $("#AdmissionClass").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //               });
    //           });        
    //});
    //$('#AdmissionClass').change(function () {
    //    var classid = $('#AdmissionClass').val();
    //    $.getJSON('/OnlineRegister/getpreclass', { classid: classid },
    //           function (data) {
    //               $("#PreClass").empty().append('<option value="">Select Class</option>');
    //               $.each(data, function (i, c) {
    //                   $("#PreClass").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //               });
    //           });
    //});
    var OnlineRegid = $('#Onlineregid').val();
    if (OnlineRegid != 0 && OnlineRegid != "" && OnlineRegid != null) {
        var stuid = $('#stuAdmissionId').val();
        if (stuid != null) {

            var ss = {
                OnlineRegid: OnlineRegid,
            }
            $.ajax({
                type: "POST",
                url: '/OnlineRegister/sibling',
                data: ss,

                success: function (data) {
                    if (data.ErrorMessage != "" && data.ErrorMessage != null) {
                        $("#jsonError").hide();
                        $("#jsonExceptionMsg").hide();
                        $("#jsonSuccessMsg").hide();
                        $("#jsonErrorMsg").show();
                        $("#jErrMsg").html(data.ErrorMessage).val(data.ErrorMessage);
                    }
                    else {
                        if (data != 0) {
                            var count = 0;
                            $('#Div_Sibling').show();
                            $.each(data, function (i, c) {

                                var row = ('<tr id="rowCount' + i + '"><td><input style="margin-top: 15px;"  readonly  type="text" value="' + c.StudentRegId + '" /></td><td><input style="  margin-top: 15px;" readonly type="text"  value="' + c.StudentID + '"/></td></tr>')

                                $("#Siblingtable").append(row);
                                count++;
                            });

                            $('#Sbling').val(count);
                        }
                    }
                },
                error: function (xhr) {
                    alert(xhr.responseText);
                    alert("Critical Error!. Failed to call the server.");
                }
            });
        }

    }
    var ss = $('#Onlineregid').val();
    if (ss != 0 && ss != "" && ss != null) {
        var stuid = $('#stuAdmissionId').val();
        if (stuid != null) {

            var ss1 = {
                OnlineRegid: ss,// OnlineRegid: OnlineRegid, 
            }
            $.ajax({
                type: "POST",
                url: '/OnlineRegister/sibling',
                data: ss1,
                success: function (data) {
                    if (data.ErrorMessage != "" && data.ErrorMessage != null) {
                        $("#jsonError").hide();
                        $("#jsonExceptionMsg").hide();
                        $("#jsonSuccessMsg").hide();
                        $("#jsonErrorMsg").show();
                        $("#jErrMsg").html(data.ErrorMessage).val(data.ErrorMessage);
                    }
                    else {
                        if (data != 0) {
                            $('#Div_Sibling').show();
                            $.each(data, function (i, c) {
                                var row = ('<tr id="rowCount' + i + '"><td><input style="margin-top: 15px;"  readonly  type="text" value="' + c.StudentRegId + '" /></td><td><input style="  margin-top: 15px;" readonly type="text"  value="' + c.StudentID + '"/></td></tr>')

                                $("#Siblingtable_Pdf").append(row);
                            });
                        }
                    }



                },
                error: function (xhr) {
                    alert(xhr.responseText);
                    alert("Critical Error!. Failed to call the server.");

                }
            });
        }

    }

    var classs = $("#AdmissionClass option:selected").val();
    
    if (classs == "" || classs == 1 || classs == 0) {
        $('#PreviousSchool').hide();
        $('#tc').hide();
    }
    else if (classs != "" || classs != 1) {
        $('#PreviousSchool').show();
        $('#tc').show();
    }

    var classs1 = $("#ApplyClass").val();

    if (classs1 == null || classs1 == 'PreKG') {
        $('#PreviousSchool_Report').hide();

    }
    else if (classs1 != null || classs1 != 'PreKG') {
        $('#PreviousSchool_Report').show();

    }
    $('#AdmissionClass').change(function () {

        var classs = $("#AdmissionClass option:selected").val();
        if (classs == "" || classs == 1 || classs == 0) {
            $('#PreviousSchool').hide();
            $('#tc').hide();
        }
        else if (classs != "" || classs != 1) {
            $('#PreviousSchool').show();
            $('#tc').show();
        }
        var AcademicyearId = $('#AcademicyearId').val();
        var AdminssionClassId = $('#AdmissionClass').val();
        $.getJSON('/OnlineRegister/getAgelimitdate', { AdminssionClassId: AdminssionClassId, AcademicyearId: AcademicyearId },
                   function (data) {
                       if (data.date != "" && data.date != null) {                           
                           $("#DOB").datepicker("destroy");
                           $('#DOB').datepicker({
                               dateFormat: 'dd/mm/yy',
                               changeMonth: true,
                               changeYear: true,
                               yearRange: '1990:' + new Date().getFullYear(),
                               maxDate: data.date,
                           });
                       }
                       else
                       {
                           $("#DOB").datepicker("destroy");
                           $('#DOB').datepicker({
                               dateFormat: 'dd/mm/yy',
                               changeMonth: true,
                               changeYear: true,
                               yearRange: '1990:' + new Date().getFullYear(),
                               maxDate: '@maxDate'
                           });
                       }
                   });
    });




    //var AcademicyearId = $('#AcademicyearId').val();
    //if (AcademicyearId != "")
    //{
    //    var AdminssionClassId = $('#AdmissionClass').val();
    //    if(AdminssionClassId!="")
    //    {
    //        $.getJSON('/OnlineRegister/getAgelimitdate', { AdminssionClassId: AdminssionClassId, AcademicyearId: AcademicyearId },
    //           function (data) {
    //               if (data.date != "" && data.date != null) {
    //                   $("#DOB").datepicker("destroy");
    //                   $('#DOB').datepicker({
    //                       dateFormat: 'dd/mm/yy',
    //                       changeMonth: true,
    //                       changeYear: true,
    //                       yearRange: '1990:' + new Date().getFullYear(),
    //                       maxDate: data.date,
    //                   });
    //               }
    //               else {
    //                   $("#DOB").datepicker("destroy");
    //                   $('#DOB').datepicker({
    //                       dateFormat: 'dd/mm/yy',
    //                       changeMonth: true,
    //                       changeYear: true,
    //                       yearRange: '1990:' + new Date().getFullYear(),
    //                       maxDate: '@maxDate'
    //                   });
    //               }
    //           });
    //    }
    //}


    //$('#DOB').datepicker({
    //    dateFormat: 'dd/mm/yy',
    //    changeMonth: true,
    //    changeYear: true,
    //    yearRange: '1990:' + new Date().getFullYear(),
    //    maxDate: '@maxDate'
    //});


    $(function () {
        var dates = $("#PreFromDate, #PreToDate").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            maxDate: '@maxDate',
            onSelect: function (selectedDate) {
                var option = this.id == "PreFromDate" ? "minDate" : "maxDate",
                instance = $(this).data("datepicker"),
                date = $.datepicker.parseDate(
                instance.settings.dateFormat ||
                $.datepicker._defaults.dateFormat,
                selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
            }
        });
    });
    $('#FatherDOB').datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1950:' + new Date().getFullYear(),
        maxDate: '@maxDate'
    });
    $('#MotherDOB').datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1950:' + new Date().getFullYear(),
        maxDate: '@maxDate'
    });

    $('#GuardianDOB').datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1950:' + new Date().getFullYear(),
        maxDate: '@maxDate'
    });
    var employeedhere = $("#WorkSameSchool").val();
    if (employeedhere == "Yes") {
        $('#EmployeeDetails').show();
    }
    else {
        $('#EmployeeDetails').hide();
    }
    $('#WorkSameSchool').change(function () {
        var employeedhere = $(this).val();
        if (employeedhere == "Yes") {
            $('#EmployeeDetails').show();
        }
        else {
            $('#EmployeeDetails').hide();
        }
    });


    var pri = $('input[name=PrimaryUser]:checked').val();

    if (pri == "Guardian") {
        $('input[name=GuardianRequried][value=Yes]').prop('checked', true);
        $('#gg').show();
    }
    else {
        $('input[name=GuardianRequried][value=No]').prop('checked', true);
        $('#gg').hide();
    }

    $(".PP").change(function (event) {
        var pri = $('input[name=PrimaryUser]:checked').val();
        if (pri == "Guardian") {
            $('input[name=GuardianRequried][value=Yes]').prop('checked', true);
            $('#gg').show();

        }
        else {
            $('input[name=GuardianRequried][value=No]').prop('checked', true);
            $('#gg').hide();

        }
    });
    var G = $('input[name=GuardianRequried]:checked').val();
    if (G == "Yes") {
        $('#gg').show();
    }
    if (G == "No") {
        var pri = $('input[name=PrimaryUser]:checked').val();
        if (pri == "Guardian") {
            alert("You Must Enter Guardian Details Because Primary User is Guardian");
            $('#gg').show();
            $('input[name=GuardianRequried][value=Yes]').prop('checked', true);
        }
        else {
            $('input[name=GuardianRequried][value=No]').prop('checked', true);
            $('#gg').hide();
        }
    }
    $(".HaveGuardian").change(function (event) {

        var G = $('input[name=GuardianRequried]:checked').val();

        if (G == "Yes") {
            $('#gg').show();

        }
        if (G == "No") {
            var pri = $('input[name=PrimaryUser]:checked').val();

            if (pri == "Guardian") {
                alert("You Must Enter Guardian Details Because Primary User is Guardian");
                $('input[name=GuardianRequried][value=Yes]').prop('checked', true)
            }
            else {
                $('input[name=GuardianRequried][value=No]').prop('checked', true);
                $('#gg').hide();

            }
        }
    });



    //edit primary user

    $(".PP1").change(function (event) {
        var pri = $('input[name=PrimaryUser1]:checked').val();
        if (pri == "Guardian") {
            $('input[name=GuardianRequried][value=Yes]').prop('checked', true);
            $('#gg').show();

        }
        else {
            $('input[name=GuardianRequried][value=No]').prop('checked', true);
            $('#gg').hide();

        }
    });
    var G = $('input[name=GuardianRequried]:checked').val();

    if (G == "Yes") {

        $('#gg').show();
    }
    if (G == "No") {
        var pri = $('input[name=PrimaryUser1]:checked').val();

        if (pri == "Guardian") {
            alert("You Must Enter Guardian Details Because Primary User is Guardian");
            $('#gg').show();
            $('input[name=GuardianRequried][value=Yes]').prop('checked', true);
        }
        else {
            $('input[name=GuardianRequried][value=No]').prop('checked', true);
            $('#gg').hide();
        }
    }
    $(".HaveGuardian").change(function (event) {
        var G = $('input[name=GuardianRequried]:checked').val();
        if (G == "Yes") {
            $('#gg').show();
        }
        if (G == "No") {
            var pri = $('input[name=PrimaryUser1]:checked').val();

            if (pri == "Guardian") {
                alert("You Must Enter Guardian Details Because Primary User is Guardian");
                $('input[name=GuardianRequried][value=Yes]').prop('checked', true)
            }
            else {
                $('input[name=GuardianRequried][value=No]').prop('checked', true);
                $('#gg').hide();

            }
        }
    });

    //---------------------------------------edit end----------------------------------------------
    $("#Sbling").change(function () {

        var counter = $('#Sbling option:selected').val();


        for (i = 0; i < 5; i++) {
            jQuery('#rowCount' + i).remove();

        }
        for (i = 0; i < counter; i++) {

            var row = ('<tr id="rowCount' + i + '"><td><input style="margin-top: 15px;" placeholder="RollNumber" id="txt_roll' + i + '" name="txt_roll' + i + '" type="text" /></td><td><input style="margin-top: 15px;" placeholder="AcademicYear" id="txt_year' + i + '" name="txt_year' + i + '" type="text" /></td><td><input style="margin-top: 15px;" placeholder="Class" id="txt_class' + i + '" name="txt_class' + i + '" type="text" /></td><td><input style="margin-top: 15px;" placeholder="Section" id="txt_section' + i + '" name="txt_section' + i + '" type="text" /></td></tr>')
            $("#Siblingtable").append(row);

        }
    });

    //single step perocess code







    $('.SameAs').change(function () {
        if ($('.SameAs:checked').length == $('.SameAs').length) {
            $('#PerAddress1').val($('#LocAddress1').val());
            $('#PerAddress2').val($('#LocAddress2').val());
            $('#PerCity').val($('#LocCity').val());
            $('#PerState').val($('#LocState').val());
            $('#PerCountry').val($('#LocCountry').val());
            $('#PerPostelcode').val($('#LocPostelcode').val());
        }
        else {
            $('#PerAddress1').val("");
            $('#PerAddress2').val("");
            $('#PerCity').val("");
            $('#PerState').val("");
            $('#PerCountry').val(1);
            $('#PerPostelcode').val("");

        }
    });





    //Jquery Validation


    //student validation
    $('#StuFirstName').focusout(function () {
        var sname = $('#StuFirstName').val();
        var Mname_regexpr = /^[a-zA-Z ]*$/;
        $('#Err_StuFirstName').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {

                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');
                $('#Err_StuFirstName').show();
                return false;
            }
            else {

                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);
                $('#Err_StuFirstName').hide();
                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#StuMiddlename').focusout(function () {
        var sname = $('#StuMiddlename').val();
        var Mname_regexpr = /^[a-zA-Z ]*$/;
        $('#Err_StuMiddleName').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {

                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');
                $('#Err_StuMiddleName').show();
                return false;
            }
            else {

                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);
                $('#Err_StuMiddleName').hide();
                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#StuLastname').focusout(function () {
        var sname = $('#StuLastname').val();
        var Mname_regexpr = /^[a-zA-Z ]*$/;
        $('#Err_StuLastName').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_StuLastName').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {

                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);
                $('#Err_StuLastName').hide();
                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });

    $('.StuGender').focusout(function () {

        if ($('input[name=Gender]:checked').val() != null) {
            $('button').prop('disabled', false);

        }
        else {
            $('button').prop('disabled', true);
            alert("please select Student gender");

            return false;

        }
    });
    $('#PlaceOfBirth').focusout(function () {
        var sname = $('#PlaceOfBirth').val();
        var Mname_regexpr = /^[a-zA-Z ]*$/;
        $('#Err_pob').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {

                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');
                $('#Err_pob').show();
                return false;
            }
            else {

                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);
                $('#Err_pob').hide();
                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#Religion').focusout(function () {
        var sname = $('#Religion').val();
        var Mname_regexpr = /^[a-zA-Z ]*$/;
        $('#Err_Religion').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_Religion').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_Religion').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#Nationality').focusout(function () {
        var sname = $('#Nationality').val();
        var Mname_regexpr = /^[a-zA-Z ]*$/;
        $('#Err_nationality').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_nationality').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_nationality').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });


    //health Information Validation

    $('#Height').focusout(function () {
        var sname = $('#Height').val();
        var Mname_regexpr = /^\s*[a-zA-Z0-9  .,*&-_\s]+\s*$/;
        $('#Err_Height').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_Height').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_Height').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#Weights').focusout(function () {
        var sname = $('#Weights').val();
        var Mname_regexpr = /^\s*[a-zA-Z0-9  ,*&-_\s]+\s*$/;
        $('#Err_Weight').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_Weight').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_Weight').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#IdentificationMark').focusout(function () {
        var sname = $('#IdentificationMark').val();
        var Mname_regexpr = /^[a-zA-Z ]*$/;
        $('#Err_idmark').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_idmark').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_idmark').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#Contact').focusout(function () {
        var sname = $('#Contact').val();
        var Mname_regexpr = /^[789]\d{9}$/;
        $('#Err_Contact').hide();
        if (sname != null && sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_Contact').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_Contact').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });

    $('#Email').focusout(function () {
        var sname = $('#Email').val();
        var Mname_regexpr = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/;
        $('#Err_Email').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {

                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');
                $('#Err_Email').show();
                return false;
            }
            else {
                $('#Err_Email').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });


    //Parents validation
    $('#FatherFirstName').focusout(function () {
        var sname = $('#FatherFirstName').val();
        var Mname_regexpr = /^[a-zA-Z ]*$/;
        $('#Err_FatherFirstname').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_FatherFirstname').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');


                return false;
            }
            else {
                $('#Err_FatherFirstname').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#MotherFirstname').focusout(function () {
        var sname = $('#MotherFirstname').val();
        var Mname_regexpr = /^[a-zA-Z ]*$/;
        $('#Err_MotherFirstname').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_MotherFirstname').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');


                return false;
            }
            else {
                $('#Err_MotherFirstname').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#FatherQualification').focusout(function () {
        var sname = $('#FatherQualification').val();
        var Mname_regexpr = /^\s*[a-zA-Z0-9  ,*&-_\s]+\s*$/;//
        $('#Err_FatherQuali').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_FatherQuali').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_FatherQuali').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#MotherQualification').focusout(function () {
        var sname = $('#MotherQualification').val();
        var Mname_regexpr = /^\s*[a-zA-Z0-9  ,*&-_\s]+\s*$/;//
        $('#Err_MotherQuali').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_MotherQuali').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');


                return false;
            }
            else {
                $('#Err_MotherQuali').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#FatherOccupation').focusout(function () {
        var sname = $('#FatherOccupation').val();

        var Mname_regexpr = /^[a-zA-Z ]{3,20}$/;
        $('#Err_FatherOccu').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {

                $('#Err_FatherOccu').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');

                $(this).addClass('wrong');


                return false;
            }
            else {

                $('#Err_FatherOccu').hide();
                $(this).removeClass('wrong');

                $(this).addClass('success');

                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#MotherOccupation').focusout(function () {
        var sname = $('#MotherOccupation').val();
        var Mname_regexpr = /^[a-zA-Z ]{3,20}$/;
        $('#Err_MotherOccu').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_MotherOccu').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');


                return false;
            }
            else {
                $('#Err_MotherOccu').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#FatherMobileNo').focusout(function () {
        var sname = $('#FatherMobileNo').val();

        var Mname_regexpr = /^[789]\d{9}$/;
        $('#Err_FatherNum').hide();

        if (sname != null && sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_FatherNum').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');


                return false;
            }
            else {

                $('#Err_FatherNum').hide();
                $(this).removeClass('wrong');

                $(this).addClass('success');

                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#MotherMobileNo').focusout(function () {
        var sname = $('#MotherMobileNo').val();
        var Mname_regexpr = /^[789]\d{9}$/;
        $('#Err_MotherNum').hide();
        if (sname != null && sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_MotherNum').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');


                return false;
            }
            else {
                $('#Err_MotherNum').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#FatherEmail').focusout(function () {
        var sname = $('#FatherEmail').val();
        var Mname_regexpr = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/;
        $('#Err_FatherEmail').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_FatherEmail').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');


                return false;
            }
            else {
                $('#Err_FatherEmail').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#MotherEmail').focusout(function () {
        var sname = $('#MotherEmail').val();
        var Mname_regexpr = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/;
        $('#Err_MotherEmail').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_MotherEmail').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');


                return false;
            }
            else {
                $('#Err_MotherEmail').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#FatherLastname').focusout(function () {
        var sname = $('#FatherLastname').val();
        var Mname_regexpr = /^[a-zA-Z ]*$/;
        $('#Err_FatherLastname').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_FatherLastname').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');


                return false;
            }
            else {
                $('#Err_FatherLastname').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#MotherLastname').focusout(function () {
        var sname = $('#MotherLastname').val();
        var Mname_regexpr = /^[a-zA-Z ]*$/;
        $('#Err_MotherLastname').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_MotherLastname').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');


                return false;
            }
            else {
                $('#Err_MotherLastname').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });

    $('#TotalIncome').focusout(function () {
        var sname = $('#TotalIncome').val();
        var Mname_regexpr = /^(\d{1,20})(\.\d{1,10})?$/;
        $('#Err_totalIncome').hide();
        if (sname != null && sname != "") {

            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_totalIncome').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');


                return false;
            }
            else {
                $('#Err_totalIncome').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    //Primary User Work Details Validation
    $('#UserCompanyname').focusout(function () {
        var sname = $('#UserCompanyname').val();
        var Mname_regexpr = /^[a-zA-Z0-9 ,.'@%$#^&*()-_]+$/;
        $('#Err_usercompanyname').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_usercompanyname').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_usercompanyname').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#CompanyAddress1').focusout(function () {
        var sname = $('#CompanyAddress1').val();
        var Mname_regexpr = /^[a-zA-Z0-9 ,.'@%$#^&*()-_]+$/;
        $('#Err_CompanyAddress1').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_CompanyAddress1').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_CompanyAddress1').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#CompanyAddress2').focusout(function () {
        var sname = $('#CompanyAddress2').val();
        var Mname_regexpr = /^[a-zA-Z0-9 ,.'@%$#^&*()-_]+$/;
        $('#Err_CompanyAddress2').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_CompanyAddress2').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_CompanyAddress2').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#CompanyCity').focusout(function () {
        var sname = $('#CompanyCity').val();
        var Mname_regexpr = /^[a-zA-Z ]{1,50}$/;
        $('#Err_CompanyCity').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_CompanyCity').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_CompanyCity').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#CompanyState').focusout(function () {
        var sname = $('#CompanyState').val();
        var Mname_regexpr = /^[a-zA-Z ]{1,50}$/;

        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {

                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {

                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#CompanyContact').focusout(function () {
        var sname = $('#CompanyContact').val();
        var Mname_regexpr = /^[789]\d{9}$/;
        $('#Err_usercompanycontact').hide();
        if (sname != null && sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_usercompanycontact').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_usercompanycontact').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });

    //guardian validation
    $('#GuardianFirstname').focusout(function () {
        var sname = $('#GuardianFirstname').val();
        var Mname_regexpr = /^[a-zA-Z ]*$/;
        $('#Err_GuardianFirstname').hide();
        if (sname != "") {

            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_GuardianFirstname').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_GuardianFirstname').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#GuRelationshipToChild').focusout(function () {
        var sname = $('#GuRelationshipToChild').val();
        var Mname_regexpr = /^[a-zA-Z ]{2,20}$/;
        $('#Err_GuRelationShip').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_GuRelationShip').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_GuRelationShip').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });

    $('.GuGender').focusout(function () {

        if ($('input[name=GuardianGender]:checked').val() != null) {
            $('button').prop('disabled', false);

        }
        else {
            $('button').prop('disabled', true);
            alert("please select Guardian gender");

            return false;

        }
    });

    $('#GuardianQualification').focusout(function () {
        var sname = $('#GuardianQualification').val();
        var Mname_regexpr = /^\s*[a-zA-Z0-9  ,*&-_\s]+\s*$/;//
        $('#Err_GuQuali').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_GuQuali').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_GuQuali').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });

    $('#GuardianOccupation').focusout(function () {
        var sname = $('#GuardianOccupation').val();
        var Mname_regexpr = /^[a-zA-Z ]{2,20}$/;
        $('#Err_GuOccu').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_GuOccu').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_GuOccu').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });

    $('#GuardianMobileNo').focusout(function () {
        var sname = $('#GuardianMobileNo').val();
        var Mname_regexpr = /^[789]\d{9}$/;
        $('#Err_GuNumber').hide();
        if (sname != null && sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_GuNumber').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_GuNumber').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });

    $('#GuardianEmail').focusout(function () {
        var sname = $('#GuardianEmail').val();
        var Mname_regexpr = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/;
        $('#Err_GuEmail').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {

                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');
                $('#Err_GuEmail').show();
                return false;
            }
            else {
                $('#Err_GuEmail').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#GuardianIncome').focusout(function () {
        var sname = $('#GuardianIncome').val();
        var Mname_regexpr = /^(\d{1,20})(\.\d{1,10})?$/;
        $('#Err_GuIncome').hide();
        if (sname != null && sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_GuIncome').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_GuIncome').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });


    //Address validation
    $('#LocAddress1').focusout(function () {
        var sname = $('#LocAddress1').val();
        var Mname_regexpr = /^[a-zA-Z0-9 ,.'@%$#^&*()-_]+$/;
        $('#Err_locAddr1').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_locAddr1').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_locAddr1').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#LocAddress2').focusout(function () {
        var sname = $('#LocAddress2').val();
        var Mname_regexpr = /^[a-zA-Z0-9 ,.'@%$#^&*()-_]+$/;
        $('#Err_locAddr2').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_locAddr2').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_locAddr2').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#LocCity').focusout(function () {
        var sname = $('#LocCity').val();
        var Mname_regexpr = /^[a-zA-Z ]{1,50}$/;
        $('#Err_locCity').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_locCity').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_locCity').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#LocState').focusout(function () {
        var sname = $('#LocState').val();
        var Mname_regexpr = /^[a-zA-Z ]{1,50}$/;
        $('#Err_locState').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_locState').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_locState').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    //$('#Distance').focusout(function () {
    //    var sname = $('#Distance').val();
    //    var Mname_regexpr = /^[a-zA-Z0-9 .,-_]{2,50}$/
    //    $('#Err_Distance').hide();
    //    if (sname != "") {
    //        if (!sname.match(Mname_regexpr) || sname.length == 0) {
    //            $('#Err_Distance').show();
    //            //$('button').prop('disabled', true);
    //            $(this).removeClass('success');
    //            $(this).addClass('wrong');

    //            return false;
    //        }
    //        else {
    //            $('#Err_Distance').hide();
    //            $(this).removeClass('wrong');
    //            $(this).addClass('success');
    //            //$('button').prop('disabled', false);

    //            return true;
    //        }
    //    }
    //    else {
    //        $(this).removeClass('wrong');
    //        $(this).removeClass('success');
    //    }
    //});


    $('#PerAddress1').focusout(function () {
        var sname = $('#PerAddress1').val();
        var Mname_regexpr = /^[a-zA-Z0-9 ,.'@%$#^&*()-_]+$/;
        $('#Err_preAddr1').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_preAddr1').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_preAddr1').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#PerAddress2').focusout(function () {
        var sname = $('#PerAddress2').val();
        var Mname_regexpr = /^[a-zA-Z0-9 ,.'@%$#^&*()-_]+$/;
        $('#Err_preAddr2').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_preAddr2').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_preAddr2').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#PerCity').focusout(function () {
        var sname = $('#PerCity').val();
        var Mname_regexpr = /^[a-zA-Z ]{1,50}$/;
        $('#Err_PerCity').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_PerCity').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_PerCity').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#PerState').focusout(function () {
        var sname = $('#PerState').val();
        var Mname_regexpr = /^[a-zA-Z ]{1,50}$/;
        $('#Err_PerState').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_PerState').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_PerState').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    //Emergency contact Person Validation

    $('#EmergencyContactPersonName').focusout(function () {
        var sname = $('#EmergencyContactPersonName').val();
        var Mname_regexpr = /^[a-zA-Z ]*$/;
        $('#Err_EmergencyContactName').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_EmergencyContactName').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_EmergencyContactName').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#ContactPersonRelationship').focusout(function () {
        var sname = $('#ContactPersonRelationship').val();
        var Mname_regexpr = /^[a-zA-Z ]*$/;
        $('#Err_ContactPersonRelationship').hide();
        if (sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_ContactPersonRelationship').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_ContactPersonRelationship').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#EmergencyContactNumber').focusout(function () {
        var sname = $('#EmergencyContactNumber').val();
        var Mname_regexpr = /^[789]\d{9}$/;
        $('#Err_EmergencyContactNumber').hide();
        if (sname != null && sname != "") {
            if (!sname.match(Mname_regexpr) || sname.length == 0) {
                $('#Err_EmergencyContactNumber').show();
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');

                return false;
            }
            else {
                $('#Err_EmergencyContactNumber').hide();
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);

                return true;
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });


    //File Upload Validation
    $('#BirthCertificate').focusout(function () {

        var birth = $('#BirthCertificate').val();        
        if (birth != "") {
            var pdfdocx_regexpr = /^.*\.(doc|DOC|docx|DOCX|pdf|PDF)$/;
            if (!birth.match(pdfdocx_regexpr) || birth.length == 0) {
                $('#Err_Bc').addClass('wrong');;
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');
                $('#BirthCertificate').focusin();
                return false;
            }
            else {
                $('#Err_Bc').removeClass('wrong')
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });

    //community certifivate
    $('#CommunityCertificate').focusout(function () {

        var community = $('#CommunityCertificate').val();        
        if (community != "") {
            var pdfdocx_regexpr = /^.*\.(doc|DOC|docx|DOCX|pdf|PDF)$/;
            if (!community.match(pdfdocx_regexpr) || community.length == 0) {
                $('#Err_Cc').addClass('wrong');
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');
                $('#CommunityCertificate').focusin();
                return false;
            }
            else {
                $('#Err_Cc').removeClass('wrong');
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });


    $('#IncomeCertificate').focusout(function () {

        var address = $('#IncomeCertificate').val();        
        if (address != "") {
            var pdfdocx_regexpr = /^.*\.(doc|DOC|docx|DOCX|pdf|PDF)$/;
            if (!address.match(pdfdocx_regexpr) || address.length == 0) {
                $('#Err_Ic').addClass('wrong');
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');
                $('#IncomeCertificate').focusin();
                return false;
            }
            else {
                $('#Err_Ic').removeClass('wrong');
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $('#Transfercertificate').focusout(function () {

        var address = $('#Transfercertificate').val();
        if (address != "") {
            var pdfdocx_regexpr = /^.*\.(doc|DOC|docx|DOCX|pdf|PDF)$/;
            if (!address.match(pdfdocx_regexpr) || address.length == 0) {
                $('#Err_Tc').addClass('wrong');
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');
                $('#IncomeCertificate').focusin();
                return false;
            }
            else {
                $('#Err_Tc').removeClass('wrong');
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });

    //photo

    $('#StudentPhoto').focusout(function () {
        var photo = $('#StudentPhoto').val();        
        if (photo != "") {
            var jpgjpegpng_regexpr = /^.*\.(jpg|JPG|jpeg|JPEG|png|PNG)$/;
            if (!photo.match(jpgjpegpng_regexpr) || photo.length == 0) {
                $('#Err_Studentphoto').addClass('wrong');
                $('button').prop('disabled', true);
                $(this).removeClass('success');
                $(this).addClass('wrong');
                $('#StudentPhoto').focusin();
                return false;
            }
            else {
                $('#Err_Studentphoto').removeClass('wrong');
                $(this).removeClass('wrong');
                $(this).addClass('success');
                $('button').prop('disabled', false);
            }
        }
        else {
            $(this).removeClass('wrong');
            $(this).removeClass('success');
        }
    });
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
    jQuery(document).ready(function ($) {
        // browser window scroll (in pixels) after which the "back to top" link is shown
        var offset = 300,
            //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
            offset_opacity = 1200,
            //duration of the top scrolling animation (in ms)
            scroll_top_duration = 700,
            //grab the "back to top" link
            $back_to_top = $('.cd-top');

        //hide or show the "back to top" link
        $(window).scroll(function () {
            ($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
            if ($(this).scrollTop() > offset_opacity) {
                $back_to_top.addClass('cd-fade-out');
            }
        });

        //smooth scroll to top
        $back_to_top.on('click', function (event) {
            event.preventDefault();
            $('body,html').animate({
                scrollTop: 0,
            }, scroll_top_duration
            );
        });

    });



});