﻿$(document).ready(function () {
    $("input[name=PrimaryUser]:radio").change(function () {
        var prop_for = $("input[name=PrimaryUser]:checked").val();       
        if (prop_for == 'Father') {
            $(".father-input").css("border-color", "crimson");
            $(".mother-input").css("border-color", "#CCCCCC");
            $(".guardian-input").css("border-color", "#CCCCCC");
            $(".MotherEmail").css("border-color", "#CCCCCC");
            $(".MotherSms").css("border-color", "#CCCCCC");
            var email = $('#PrimaryEmail').val();         
            if (email != "") {
                $('#FatherEmail').val(email);
                $('#FatherEmail').addClass('father-input');
                $('#FatherMobileNo').removeClass('father-input');
                $('#FatherEmail').attr('readonly', true);                               
                $('#MotherEmail').attr('readonly', false);               
                $('#GuardianEmail').attr('readonly', false);
            }
            var contact = $('#PrimaryContact').val();          
            if(contact!="")
            {
                $('#FatherMobileNo').val(contact);
                $('#FatherMobileNo').addClass('father-input');
                $('#FatherEmail').removeClass('father-input');
                $('#FatherMobileNo').attr('readonly', true);
                //$('#MotherEmail').val("");
                $('#MotherMobileNo').attr('readonly', false);
                //$('#GuardianEmail').val("");
                $('#GuardianMobileNo').attr('readonly', false);
            }
            if ($('.selectone').prop('checked')) {            
                $(".FatherEmail").css("border-color", "crimson");
                
            } else {               
                $(".FatherEmail").css("border-color", "#CCCCCC");            
            }
            if ($('#Sms').prop('checked')) {               
                $(".FatherSms").css("border-color", "crimson");
            } else {              
                $(".FatherSms").css("border-color", "#CCCCCC");               
            }              
        } else if (prop_for == 'Mother') {
            $(".mother-input").css("border-color", "crimson");
            $(".father-input").css("border-color", "#CCCCCC");
            $(".guardian-input").css("border-color", "#CCCCCC");
            $(".FatherEmail").css("border-color", "#CCCCCC");
            $(".FatherSms").css("border-color", "#CCCCCC");
            var email = $('#PrimaryEmail').val();
            alert(email);
            if (email != "") {
                $('#MotherEmail').val(email);
                $('#MotherEmail').addClass('mother-input');
                $('#MotherMobileNo').removeClass('mother-input');
                $('#MotherEmail').attr('readonly', true);
                //$('#FatherEmail').val("");
                $('#FatherEmail').attr('readonly', false);
                //$('#GuardianEmail').val("");
                $('#GuardianEmail').attr('readonly', false);
            }
            var contact = $('#PrimaryContact').val();
            alert(contact);
            if (contact != "")
            {
                $('#MotherMobileNo').val(contact);
                $('#MotherMobileNo').addClass('mother-input');
                $('#MotherEmail').removeClass('mother-input');
                $('#MotherMobileNo').attr('readonly', true);
                 //$('#FatherEmail').val("");
                $('#FatherMobileNo').attr('readonly', false);
                //$('#GuardianEmail').val("");
                $('#GuardianMobileNo').attr('readonly', false);
            }

            if ($('.selectone').prop('checked')) {              
                $(".MotherEmail").css("border-color", "crimson");

            } else {              
                $(".MotherEmail").css("border-color", "#CCCCCC");
            }
            if ($('#Sms').prop('checked')) {               
                $(".MotherSms").css("border-color", "crimson");
            } else {               
                $(".MotherSms").css("border-color", "#CCCCCC");
            }
        }
        else if (prop_for == 'Guardian') {
            $(".mother-input").css("border-color", "#CCCCCC");
            $(".father-input").css("border-color", "#CCCCCC");
            $(".FatherEmail").css("border-color", "#CCCCCC");
            $(".FatherSms").css("border-color", "#CCCCCC");
            $(".MotherEmail").css("border-color", "#CCCCCC");
            $(".MotherSms").css("border-color", "#CCCCCC");
            $(".guardian-input").css("border-color", "crimson");

            $("input").attr("href", "#GuardianName");
            var email = $('#PrimaryEmail').val();
            if (email != "") {
                $('#GuardianEmail').val(email);
                $('#GuardianEmail').addClass('guardian-input');
                $('#GuardianMobileNo').removeClass('guardian-input');
                $('#GuardianEmail').attr('readonly', true);
                //$('#MotherEmail').val("");
                $('#MotherEmail').attr('readonly', false);
                //$('#FatherEmail').val("");
                $('#FatherEmail').attr('readonly', false);
            }
            var contact = $('#PrimaryContact').val();
            if (contact != "") {
                $('#GuardianMobileNo').val(contact);
                $('#GuardianMobileNo').addClass('guardian-input');
                $('#GuardianEmail').removeClass('guardian-input');
                $('#GuardianMobileNo').attr('readonly', true);
                //$('#MotherEmail').val("");
                $('#MotherMobileNo').attr('readonly', false);
                //$('#FatherEmail').val("");
                $('#FatherMobileNo').attr('readonly', false);
            }           
            if ($('.selectone').prop('checked')) {              
                $(".GuardianEmail").css("border-color", "crimson");

            } else {               
                $(".GuardianEmail").css("border-color", "#CCCCCC");
            }
            if ($('#Sms').prop('checked')) {                
                $(".GuardianSms").css("border-color", "crimson");
            } else {               
                $(".GuardianSms").css("border-color", "#CCCCCC");
            }
        }
    });

    $('input[type="checkbox"]').change(function () {
        var name = $(this).val();
        var check = $(this).prop('checked');       
        var null_id = $(this).attr('name');       
        var prop_for = $("input[name=PrimaryUser]:checked").val();       
        if (prop_for == 'Father') {
            if (null_id == 'Email') {
                  if (check == true) {
                      $(".FatherEmail").css("border-color", "crimson");
            } else {
                      $(".FatherEmail").css("border-color", "#CCCCCC");
            }
            }
            if (null_id == 'Sms') {
                if (check == true) {
                    $(".FatherSms").css("border-color", "crimson");
                } else {
                    $(".FatherSms").css("border-color", "#CCCCCC");
                    }
                }
        } else if (prop_for == 'Mother') {
            if (null_id == 'Email') {
                if (check == true) {
                    $(".MotherEmail").css("border-color", "crimson");
                } else {
                    $(".MotherEmail").css("border-color", "#CCCCCC");
                }
            }
            if (null_id == 'Sms') {
                if (check == true) {
                    $(".MotherSms").css("border-color", "crimson");
                } else {
                    $(".MotherSms").css("border-color", "#CCCCCC");
                }
            }

        }
        else if (prop_for == 'Guardian') {
            if (null_id == 'Email') {
                if (check == true) {
                    $(".GuardianEmail").css("border-color", "crimson");
                } else {
                    $(".GuardianEmail").css("border-color", "#CCCCCC");
                }
            }
            if (null_id == 'Sms') {
                if (check == true) {
                    $(".GuardianSms").css("border-color", "crimson");
                } else {
                    $(".GuardianSms").css("border-color", "#CCCCCC");
                }
            }
        }
             });
     


    $("input[name=PrimaryUser1]:radio").change(function () {
        var prop_for = $("input[name=PrimaryUser1]:checked").val();
        if (prop_for == 'Father') {
            $(".father-input").css("border-color", "crimson");
            $(".mother-input").css("border-color", "#CCCCCC");
            $(".guardian-input").css("border-color", "#CCCCCC");
            var email = $('#PrimaryEmail').val();
            if (email != "") {
                $('#FatherEmail').val(email);
            }
            var contact = $('#PrimaryContact').val();
            if (contact != "") {
                $('#FatherMobileNo').val(contact);
            }
        } else if (prop_for == 'Mother') {
            $(".mother-input").css("border-color", "crimson");
            $(".father-input").css("border-color", "#CCCCCC");
            $(".guardian-input").css("border-color", "#CCCCCC");
            var email = $('#PrimaryEmail').val();
            if (email != "") {
                $('#MotherEmail').val(email);
            }
            var contact = $('#PrimaryContact').val();
            if (contact != "") {
                $('#MotherMobileNo').val(contact);
            }
        }
        else if (prop_for == 'Guardian') {
            $(".mother-input").css("border-color", "#CCCCCC");
            $(".father-input").css("border-color", "#CCCCCC");
            $(".guardian-input").css("border-color", "crimson");
            $("input").attr("href", "#GuardianName");
            var email = $('#PrimaryEmail').val();
            if (email != "") {
                $('#GuardianEmail').val(email);
            }
            var contact = $('#PrimaryContact').val();
            if (contact != "") {
                $('#GuardianMobileNo').val(contact);
            }
        }
    });
   

});