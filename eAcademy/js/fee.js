﻿$(document).ready(function () {
    $.ajax({

        type: "POST",
        url: '/StudentReport/getFeesStructure',
        traditional: true,
        data: datavalue,
        success: function (data) {

            if (data == null) {
                $('#Err').show();
                $("#jsonError").hide();
                $("#jsonExceptionMsg").hide();
                $("#jsonSuccessMsg").hide();
                $("#jsonErrorMsg").show();
                $("#jErrMsg").html("Details Not Found").val("Details Not Found");

            }
            else {
                $('#Txt_Year').text($('#AcademicYear option:selected').text());
                $('#Txt_Class').text($('#Class option:selected').text());
                $('#Txt_Sec').text($('#Rpt_Section option:selected').text());
                $('#Txt_Name').text($('#StudentId option:selected').text());
                $('#show').show();
                $('#Err').hide();
                var td_body1 = 0;
                var feeid = 0;
                var total = 0;
                var head = "<tr><th class='tr-content' style='border: 1px solid rgb(150, 150, 150);'>Fee Category</th><th class='tr-content' style='border: 1px solid rgb(150, 150, 150);'> Amount</th><th class='tr-content' style=' border: 1px solid rgb(150, 150, 150);'>Tax</th><th class='tr-content' style='border: 1px solid rgb(150, 150, 150);'>Total</th><th class='tr-content' style='border: 1px solid rgb(150, 150, 150);'>Last Date</th></tr>";
                $("#feesstructure").empty().append(head);

                $.each(data, function (i, v) {

                    var row1, table1;


                    if (feeid == v.FeeId) {


                        feeid = v.FeeId;

                        table1 = "<tr class='tr-content-email'><td>" + v.PaymentType + "</td><td>" + v.Amount + "</td><td>" + v.ServicesTax + "</td><td>" + v.Total + "</abel></td><td>" + v.Ldate + "</td></tr>";

                        $("#" + td_body1).append(table1);




                    }
                    else {

                        feeid = v.FeeId;
                        row1 = "<tr class='tr-content-email'><td>" + v.FeeCategory
                            + "<table class='table table-bordered table-striped'><tbody id=" + td_body1 + "></tbody></table></td><td>" + v.Amount + "</td><td>" + v.ServicesTax + "</td><td>" + v.Total + " </td><td>" + v.Ldate
                            + "</td></tr>";
                        total = total + v.Total;
                        $("#feesstructure").append(row1);

                    }
                });
                var row2 = "<tr class='tr-content-email'><td style='border:transparent'></td><td style='border:transparent'></td><td >Total </td><td>" + total + "</abel></td><td style='border:transparent'></td></tr>";
                $("#feesstructure").append(row2);




            }

        }
    });
    $.ajax({

        type: "POST",
        url: '/StudentReport/getpaidFeesStructure',
        traditional: true,
        data: datavalue1,
        success: function (list) {

            if (list == null) {
                $('#Err').show();
                $("#jsonError").hide();
                $("#jsonExceptionMsg").hide();
                $("#jsonSuccessMsg").hide();
                $("#jsonErrorMsg").show();
                $("#jErrMsg").html("Details Not Found").val("Details Not Found");

            }
            else {
                $('#Txt_Year').text($('#AcademicYear option:selected').text());
                $('#Txt_Class').text($('#Class option:selected').text());
                $('#Txt_Sec').text($('#Rpt_Section option:selected').text());
                $('#Txt_Name').text($('#StudentId option:selected').text());
                $('#show').show();
                $('#Err').hide();
                var td_body2 = 0;
                var feeid2 = 0;
                var total1 = 0;
                var head = "<tr><th class='tr-content' style='border: 1px solid rgb(150, 150, 150);'>Fee Type</th><th class='tr-content' style='border: 1px solid rgb(150, 150, 150);'>Fee Category</th><th class='tr-content' style='border: 1px solid rgb(150, 150, 150);'> Amount</th><th class='tr-content' style=' border: 1px solid rgb(150, 150, 150);'>Tax</th><th class='tr-content' style='border: 1px solid rgb(150, 150, 150);'>Total</th><th class='tr-content' style='border: 1px solid rgb(150, 150, 150);'>Fees Collection date Date</th></tr>";
                $("#paidFees").empty().append(head);

                $.each(list, function (i, k) {

                    var row3, rowtable2;

                    //alert("k.FeeId=" + k.FeeId);
                    //if (feeid2 == k.FeeId) {


                    //    feeid2 = k.FeeId;

                    //    td_body2 =td_body2+ "<tr class='tr-content-email'><td>" + k.PaymentType + "</td><td>" + k.Amount + "</td><td>" + k.ServicesTax + "</td><td>" + k.Total + "</abel></td><td>" + k.FeespaidDate + "</td></tr>";





                    //}
                    //else {

                    //feeid2 = k.FeeId;  <table class='table table-bordered table-striped'><tbody>" + td_body2 + "</tbody></table>
                    //  alert("td_body2=" + td_body2);
                    row3 = "<tr class='tr-content-email'><td>" + k.PaymentType
                        + "</td><td>" + k.FeeCategory
                            + "</td><td>" + k.Amount + "</td><td>" + k.ServicesTax + "</td><td>" + k.Total + " </td><td>" + k.FeespaidDate
                            + "</td></tr>";

                    total1 = total1 + k.Total;
                    $("#paidFees").append(row3);


                    //}
                });
                var row4 = "<tr class='tr-content-email'><td style='border:transparent'></td><td style='border:transparent'></td><td style='border:transparent'></td><td >Total </td><td>" + total1 + "</abel></td><td style='border:transparent'></td></tr>";
                $("#paidFees").append(row4);
            }

        }
    });

    $.ajax({

        type: "POST",
        url: '/StudentReport/getDueFeesStructure',
        traditional: true,
        data: datavalue1,
        success: function (info) {

            if (list == null) {
                $('#Err').show();
                $("#jsonError").hide();
                $("#jsonExceptionMsg").hide();
                $("#jsonSuccessMsg").hide();
                $("#jsonErrorMsg").show();
                $("#jErrMsg").html("Details Not Found").val("Details Not Found");

            }
            else {
                $('#Txt_Year').text($('#AcademicYear option:selected').text());
                $('#Txt_Class').text($('#Class option:selected').text());
                $('#Txt_Sec').text($('#Rpt_Section option:selected').text());
                $('#Txt_Name').text($('#StudentId option:selected').text());
                $('#show').show();
                $('#Err').hide();
                var td_body2 = 0;
                var feeid2 = 0;
                var total1 = 0;
                var head = "<tr><th class='tr-content' style='border: 1px solid rgb(150, 150, 150);'>Fee Type</th><th class='tr-content' style='border: 1px solid rgb(150, 150, 150);'>Fee Category</th><th class='tr-content' style='border: 1px solid rgb(150, 150, 150);'> Amount</th><th class='tr-content' style=' border: 1px solid rgb(150, 150, 150);'>Tax</th><th class='tr-content' style='border: 1px solid rgb(150, 150, 150);'>Total</th><th class='tr-content' style='border: 1px solid rgb(150, 150, 150);'>Fees Collection date Date</th></tr>";
                $("#DueFees").empty().append(head);

                $.each(info, function (i, k) {

                    var row3, rowtable2;

                    //alert("k.FeeId=" + k.FeeId);
                    //if (feeid2 == k.FeeId) {


                    //    feeid2 = k.FeeId;

                    //    td_body2 =td_body2+ "<tr class='tr-content-email'><td>" + k.PaymentType + "</td><td>" + k.Amount + "</td><td>" + k.ServicesTax + "</td><td>" + k.Total + "</abel></td><td>" + k.FeespaidDate + "</td></tr>";





                    //}
                    //else {

                    //feeid2 = k.FeeId;  <table class='table table-bordered table-striped'><tbody>" + td_body2 + "</tbody></table>
                    //  alert("td_body2=" + td_body2);
                    row3 = "<tr class='tr-content-email'><td>" + k.PaymentType
                        + "</td><td>" + k.FeeCategory
                            + "</td><td>" + k.Amount + "</td><td>" + k.ServicesTax + "</td><td>" + k.Total + " </td><td>" + k.FeespaidDate
                            + "</td></tr>";

                    total1 = total1 + k.Total;
                    $("#DueFees").append(row3);


                    //}
                });
                var row4 = "<tr class='tr-content-email'><td style='border:transparent'></td><td style='border:transparent'></td><td style='border:transparent'></td><td >Total </td><td>" + total1 + "</abel></td><td style='border:transparent'></td></tr>";
                $("#DueFees").append(row4);
            }

        }
    });
});