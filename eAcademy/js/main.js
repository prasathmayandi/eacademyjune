﻿$(document).ready(function () {
    

    // Academic year change function

    


    $(window).resize(function() {
        if ($(window).width() < 770) {           
            $(".sidebar_wrap").hide();
        }
    });
    $(window).resize(checkSize1);
    function checkSize1() {
        if ($(window).width() < 768) { $("#wrapper").removeClass("active");}
        if ($(window).width() > 768) {$("#wrapper").addClass("active");}
        if ($(window).width() < 600) { $(".sname2").each(function (i) { len = $(this).text().length; if (len > 10) { $(this).text($(this).text().substr(0, 10) + '...'); } }); }
        else if ($(window).width() < 650) { $(".sname2").each(function (i) { len = $(this).text().length; if (len > 10) { $(this).text($(this).text().substr(0, 20) + '...'); } }); }
        else if ($(window).width() < 700) { $(".sname2").each(function (i) { len = $(this).text().length; if (len > 10) { $(this).text($(this).text().substr(0, 24) + '...'); } }); }
        else if ($(window).width() < 750) { $(".sname2").each(function (i) { len = $(this).text().length; if (len > 10) { $(this).text($(this).text().substr(0, 27) + '...'); } }); }
        else if ($(window).width() < 800) { $(".sname2").each(function (i) {len = $(this).text().length;if (len > 10) {$(this).text($(this).text().substr(0, 30) + '...');}});}
        else if ($(window).width() < 850) { $(".sname2").each(function (i) {len = $(this).text().length;if (len > 10) {$(this).text($(this).text().substr(0, 33) + '...');}});}
        else if ($(window).width() < 900) { $(".sname2").each(function (i) {len = $(this).text().length;if (len > 10) {$(this).text($(this).text().substr(0, 37) + '...');}});}
        else if ($(window).width() < 950) { $(".sname2").each(function (i) {len = $(this).text().length;if (len > 10) {$(this).text($(this).text().substr(0, 45) + '...');}});}
        else if ($(window).width() < 1000){ $(".sname2").each(function (i) {len = $(this).text().length;if (len > 10) {$(this).text($(this).text().substr(0, 50) + '...');}});}   
    }
    $("#parent1").hide();
    $("#parent2").hide();
    $("#admin1").hide();
    $("#admin2").hide();

    $(window).bind('resize', function () {
        var width = $('#div_Grid,#show,#gridSize').width();
        $('#grid,#grid1,#grid2,#FacultyAchievementGrid,#SchoolAchievementGrid,#grid,#Faculty_Achievement_grid,#Stu_Achievement_grid,#ConfirmationProcessFilterGrid,#SelectionProcessGrid,#FlagdatalistGrid,#FacultyAttendanceReportGrid,#MI_StudentListGrid,#StuRemarkGrid,#SubjectWithFacultyListGrid,#FacultyFeedbackgrid1,#EmployeeLeaveRequestgrid,#ExamRoomListGrid,#ExamAttendanceGrid,#StuRemarkGrid,#FacultyLeaveReqListGrid,#AssignmentListGrid,#HomeworkListGrid,#NotesListGrid,#MI_AssignmentListGrid,#MI_HomeworkListGrid,#MI_QueryListGrid,#MI_StudentListGrid,#MI_NoteListGrid,#ParentLeaveReqListGrid,#HomeworkListGrid,#NotesListGrid,#MI_QueryListGrid,#MI_QueryListGrid,#BookDetailsgrid ').setGridWidth(width);
        $('div').setGridWidth(width);
    });
   
    $(".btn_submit").addClass("btn-success");
    $(".btn_add").addClass("btn-primary");
    $(".btn_cancel").addClass("btn-danger");
    $(".btn_reset").addClass("btn-sample");
    $(".btn_update").addClass("btn-success");
    $(".btn_print").addClass("btn-success");
    $(".btn_save").addClass("btn-primary");
    $(".btn_search").addClass("btn-primary");


    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");
        $("#menu-toggle1").show();
    });

    $("#menu-toggle1").click(function (e) {
        e.preventDefault(); 
        $("#wrapper").toggleClass("active");
        $("#menu-toggle1").hide();
    });


    $("#btnprint").click(function () {
        window.print();
    });

    $("#tbl_dataTable").dataTable();
    $("#tbl_dataTable_length").addClass("no_print");
    $("#tbl_dataTable_filter").addClass("no_print");
    $("#tbl_dataTable_paginate").addClass("no_print");
    $("#tbl_dataTable_info").addClass("no_print");

    $(".navbar-toggle").click(function (event) {
        $(".sidebar_wrap").toggle(function (event) {
            $("#menu li a").click(function (event) {
                $(".sidebar_wrap").hide();
            });
        });
    });

   
    $(function () {
        var str = location.href;
        var pathname = window.location.pathname;
        var endURL = pathname.substring(pathname.lastIndexOf('/') + 1, pathname.length);
        var cc1 = str.substring(str.indexOf('/') + 2, str.lastIndexOf('/'));
      
        $("#menu li ul li a").each(function () {
            var currentHref = this.href;
            var controller1 = currentHref.substring(currentHref.indexOf('/') + 2, currentHref.lastIndexOf('/'));

                   if(cc1 == controller1){
                $("#menu li ul li").removeClass("in");

                $(this).parents('li').addClass("active");
                $(this).parents('li ul').removeClass("collapse").addClass("in");


                $(this).parents('li ul li').addClass("dynamicactive");
               
            }


        });

        $(".sidebar-nav1 li a").each(function () {
            var pathname1 = window.location.pathname;    // current url
            var currentHref = this.href;  // all url
            var controller1 = currentHref.substring(currentHref.indexOf('/') + 2, currentHref.lastIndexOf('/'));    // location:1122/controller name
            var endURL1 = controller1.substring(controller1.lastIndexOf('/') + 1, controller1.length);  // controller name
            var endURL2 = pathname1.substring(pathname1.indexOf('/') + 1, pathname1.lastIndexOf('/'));  // controller name
            var str = location.href.toLowerCase();
            var str1 = this.href.toLowerCase();
            if (endURL2.indexOf(endURL1) == 0) {
                $(".sidebar-nav1 li").removeClass("dynamicactive");
                $(this).parents('.sidebar-nav1 li').addClass("dynamicactive");
            }
            if(endURL2 == "StudentModuleAssigned")
            {
                $(".sidebar-nav1 li").removeClass("dynamicactive");
                $('.sidebar-nav1 #toAct').addClass("dynamicactive");
            }
            if (endURL2 == "Modules") {
                $(".sidebar-nav1 li").removeClass("dynamicactive");
                $('.sidebar-nav1 #toAct').addClass("dynamicactive");
            }
            if (endURL2 == "ParentModuleAssigned") {
                $(".sidebar-nav1 li").removeClass("dynamicactive");
                $('.sidebar-nav1 #toAct').addClass("dynamicactive");
            }
        });

        $(".nav-tabs li a").each(function () {
            var currentHref = this.href;
            var controller1 = currentHref.substring(currentHref.indexOf('/') + 2, currentHref.lastIndexOf('/'));
            var endURL1 = currentHref.substring(currentHref.lastIndexOf('/') + 1, currentHref.length);
            if (endURL.indexOf(endURL1) > -1) {
                $(".nav-tabs  li").removeClass("active");
                $(this).parents('.nav-tabs li').addClass("active");
            }
        });

        $(".spLink a").each(function () {
            var pathname1 = window.location.pathname;
            var currentHref = this.href;
            var controller1 = currentHref.substring(currentHref.indexOf('/') + 2, currentHref.lastIndexOf('/'));
            var endURL1 = currentHref.substring(currentHref.lastIndexOf('/') + 1, currentHref.length);
            if (endURL.indexOf(endURL1) > -1) {
                $(".spLink").removeClass("dynamicactive");
                $(this).parents('.spLink').addClass("dynamicactive");
            }
        });
    });

    $(function () {
        $('#menu').metisMenu();
    });

    //Home 
    $("#Country").addClass("col-lg-12");
    $("#Country1").addClass("col-lg-12");


    //Event calendar for Admin Dashboard
    $.getJSON("/Report/date",
             function (data) {
                 var disabledDay = [];
                 var events = [];
                 var des = [];
                 var d = [];
                 var m = [];
                 var y = [];
                 $.each(data, function (i, v) {
                     var singledate = v.dat;
                     var changeddate = singledate.match(/\d+/g).map(function (s) { return new Date(+s); });
                     var date = new Date(changeddate);
                     var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
                     m[i] = mnth;
                     var day = ("0" + date.getDate()).slice(-2);
                     d[i] = day;
                     y[i] = date.getFullYear();
                     var FromateDate = [mnth, day, date.getFullYear()].join("/");
                     disabledDay[i] = FromateDate;
                     events[i] = v.name;
                     des[i] = v.des;

                 });
                 var holydays = disabledDay;
                 var whosebday = events;
                 var description = des;
                 function highlightDays(date) {
                     for (var i = 0; i < holydays.length; i++) {
                         if (new Date(holydays[i]).toString() == date.toString()) {
                             return [false, 'Highlighted', whosebday[i]];
                          //   alert(whosebday[i]);
                         }
                     }
                     var day = date.getDay();
                     return [(day > 0), ''];

                 }
                 var eventsonecalendar = [], k;
                 for (var k = 0; k < disabledDay.length; k += 1) {
                     eventsonecalendar.push({
                         title: whosebday[k],
                         start: new Date(holydays[k])
                         //end: new Date(column_date.setHours(10, 40)),
                         //allDay: false
                     });


                 }


                 //$('#ecal').eCalendar({

                 //    weekDays: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                 //    months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                 //             'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                 //    textArrows: { previous: '<', next: '>' },
                 //    eventTitle: 'Events',
                 //    url: '',
                 //    events: eventsonecalendar

                 //});
                 $('#ecal').fullCalendar({
                     theme: true,
                     header: {
                         left: 'prev,next today',
                         center: 'title',
                         right: 'month,agendaWeek,agendaDay'
                     },
                     //defaultDate: '2014-11-12',
                     //editable: true,
                     //eventLimit: true,
                     events: eventsonecalendar

                 });

             });

    $("#div_admin").click(function (event) {

        $("#div_Admin_login").slideToggle(1000);
        $("#div_Staff_login").slideUp(1000);

    });

    $("#div_staff").click(function (event) {

        $("#div_Staff_login").slideToggle(1000);
        $("#div_Admin_login").slideUp(1000);

    });


    //Student & parent slidetoggle

    $("#div_student").click(function (event) {

        $("#div_Student_login").slideToggle(1000);
        $("#div_Parent_login").slideUp(1000);


    });

    $("#div_parent").click(function (event) {
        $("#div_Parent_login").slideToggle(1000);
        $("#div_Student_login").slideUp(1000);

    });


    /*********************************************************************************************************************School Configuration*************************************************************************************************************************************************************/

  

  

    //Academic configuration Starts



    /* Academic Year */

    $(function () {
        var dates = $("#acYSD, #acYED").datepicker({
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            minDate: '+1d',
            onSelect: function (selectedDate) {
                var option = this.id == "acYSD" ? "minDate" : "maxDate",
                instance = $(this).data("datepicker"),
                date = $.datepicker.parseDate(
                instance.settings.dateFormat ||
                $.datepicker._defaults.dateFormat,
                selectedDate , instance.settings);
                dates.not(this).datepicker("option", option, date);
            }
        });
    });


    $(function() {
        var dates = $("#EditacYSD, #EditacYED").datepicker({
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
         
            onSelect: function (selectedDate) {
                var option = this.id == "EditacYSD" ? "minDate" : "maxDate",
                instance = $(this).data("datepicker"),
                date = $.datepicker.parseDate(
                instance.settings.dateFormat ||
                $.datepicker._defaults.dateFormat,
                selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
            },
           
        });
    });
    /* Term */

    /*   Section */






    //$("#acYear").addClass("col-lg-12");
    $("#acYear1").addClass("col-lg-12");
    $("#acYear_Fees").addClass("col-lg-12");
    $("#class_Fees").addClass("col-lg-12");
    $("#feeCategory").addClass("col-lg-12");

    $(".acYear").change(function (event) {
        var acid = $(".acYear").val();
        $("#tacYSD").val("").html("");
        $("#tacYED").val("").html("");
        $.getJSON("/AcademicsConfiguration/getDate", { acid: acid },
            function (data) {
                var mdate = data.sd + "------" + data.ed;
                $("#tacYSD").val("").html("");
                $("#tacYED").val("").html("");
                $(function () {
                    var dates = $("#tacYSD, #tacYED").datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: data.sd,
                        maxDate: data.ed,
                        changeMonth: true,
                        changeYear: true,
                        onSelect: function (selectedDate) {
                            var option = this.id == "tacYSD" ? "minDate" : "maxDate",
                            instance = $(this).data("datepicker"),
                            date = $.datepicker.parseDate(
                            instance.settings.dateFormat ||
                            $.datepicker._defaults.dateFormat,
                            selectedDate, instance.settings);
                            dates.not(this).datepicker("option", option, date);
                        }
                    });
                });
            });
    });

    $("#acYear").change(function (event) {
        var acid = $("#acYear1").val();
        $("#tacYSD1").val("").html("");
        $("#tacYED1").val("").html("");
        $.getJSON("/AcademicsConfiguration/getDate", { acid: acid },
            function (data) {
                var mdate = data.sd + "------" + data.ed;
                $("#tacYSD1").val("").html("");
                $("#tacYED1").val("").html("");
                $(function () {
                    var dates = $("#tacYSD1, #tacYED1").datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: data.sd,
                        maxDate: data.ed,
                        changeMonth: true,
                        changeYear: true,
                        onSelect: function (selectedDate) {
                            var option = this.id == "tacYSD1" ? "minDate" : "maxDate",
                            instance = $(this).data("datepicker"),
                            date = $.datepicker.parseDate(
                            instance.settings.dateFormat ||
                            $.datepicker._defaults.dateFormat,
                            selectedDate, instance.settings);
                            dates.not(this).datepicker("option", option, date);
                        }
                    });
                });
            });
    });



    $("#acYear1").change(function (event) {
        var acid = $("#acYear1").val();
        if (acid == "") {
            acid = 0;
        }

        $.getJSON("/AcademicsConfiguration/getTermRecord", { acid: acid },
            function (data) {
                if (data.c == 0) {
                    $(".tbl_term").hide();
                    $(".alert_term").show();
                }
                else {
                    $(".tbl_term").show();
                    $(".alert_term").hide();
                    $('#tboby_term').append().empty();
                    $.each(data.ans, function (i, v) {
                        row = $('<tr><td>' + (i + 1) + '</td><td>' + v.TermName + '</td><td>' + v.StartDate + '</td><td>' + v.EndDate + '</td><td>' + '<a href="/AcademicsConfiguration/EditTerm/?tid=' + v.TermID + '"><i class="fa fa-edit"></i></a></td></tr>');
                        $('#tboby_term').append(row);

                    });
                }
            });
    });


    $(".btn_reset").click(function (event) {
        $(".jsonErrorMsg").hide();
        $(".jsonSuccessMsg").hide();
        $("#jsonErrorMsg").hide();
        $("#jsonSuccessMsg").hide();
        $("#MandatoryFieldErrorMessage_Add").hide();
        $("#MandatoryFieldErrorMessage_AddWithMsg").hide();
        $("#jErr").hide();
        $("#jsonError").hide();
        $(".jsonError").hide(); 
        $('#chars').text(250);
    });


    $("#Event_acY").change(function (event) {
        var acid = $("#Event_acY").val();
        if (acid == "") {
            acid = 0;
        }
        $.getJSON("/AcademicsConfiguration/getCalenderRecord", { acid: acid },
            function (data) {
                if (data.c == 0) {
                    $(".tbl_calender").hide();
                    $(".alert_calender").show();
                }
                else {
                    $(".tbl_calender").show();
                    $(".alert_calender").hide();
                    $('#tbody_Calender').append().empty();
                    $.each(data.ans, function (i, v) {
                        row = $('<tr><td>' + (i + 1) + '</td><td>' + v.EventDate + '</td><td>' + v.EventName + '</td><td>' + v.EventDecribtion + '</td><td>' + "<i class='fa fa-edit'></i>" + '</td></tr>');
                        $('#tbody_Calender').append(row);

                    });
                }

            });
    });



    $("#Announcement_acY").change(function (event) {
        var acid = $("#Announcement_acY").val();
        if (acid == "") {
            acid = 0;
        }

        $.getJSON("/AcademicsConfiguration/getAnnouncementRecord", { acid: acid },
            function (data) {
                if (data.c == 0) {
                    $(".tbl_announcement").hide();
                    $(".alert_announcement").show();
                }
                else {
                    $(".tbl_announcement").show();
                    $(".alert_announcement").hide();
                    $('#tbody_announcement').append().empty();
                    $.each(data.ans, function (i, v) {
                        row = $('<tr><td>' + (i + 1) + '</td><td>' + v.ed + '</td><td>' + v.e + '</td><td>' + v.edesc + '</td><td>' + "<i class='fa fa-edit'></i>" + '</td></tr>');
                        $('#tbody_announcement').append(row);

                    });
                }

            });


    });




    $("#addClassTab").click(function (event) {
        $("#addClassSection").show();
        $("#addSectionSection").hide();
        $("#editClass").hide();
    });


    //$("#addExSection").click(function () {
    //    $("#divAddExSec").append().empty();
    //    var txt = $('<input class="col-lg-12" type="text" id="txt_section" name="txt_section" placeholder="Section" required title="Section" />');
    //    $("#divAddExSec").append(txt);
    //});




    $("#linkSection").click(function (event) {
        $("#addSectionSection").show();
    });



    $("#btnAddSection").click(function (event) {

        $("#addSectionSection").hide();
    });
    $("#Event_acY").addClass("col-lg-12");
    $("#Event_acYear").addClass("col-lg-12");

    $("#Event_acYear").change(function (event) {
     $("#txt_event_date").datepicker("destroy");
        var acid = $("#Event_acYear").val();
        $.getJSON("/AcademicsConfiguration/getDate", { acid: acid },
            function (data) {
                var mdate = data.sd + "------" + data.ed;
                $(function () {
                    var dates = $("#txt_event_date").datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: data.sd,
                        maxDate: data.ed,
                        changeMonth: true,
                        changeYear: true
                    });
                });


            });

    });



    $("#Announcement_acY").addClass("col-lg-12");
    $("#Announcement_acYear").addClass("col-lg-12");

    $("#Announcement_acYear").change(function (event) {
        $("#txt_Announcement_date").datepicker("destroy");
        var acid = $("#Announcement_acYear").val();
        $.getJSON("/AcademicsConfiguration/getDate", { acid: acid },
            function (data) {
                var mdate = data.sd + "------" + data.ed;
                $(function () {
                    var dates = $("#txt_Announcement_date").datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: data.sd,
                        maxDate: data.ed,
                        changeMonth: true,
                        changeYear: true
                    });
                });
            });
    });


   

   




    $('#txt-checksub').click(function () {

        var isChecked = $('#txt-checksub:checked').val();
        if (isChecked == "on") {
            $("#Subjects").attr('disabled', true);

            $("#Subjects_chosen").hide();
        }

        else {
            $("#Subjects").prop('disabled', false);
            $("#Subjects_chosen").show();
        }
    });


    $('#txt_checkclass').click(function () {

        var isChecked = $('#txt_checkclass:checked').val();
        if (isChecked == "on") {


            $("#class_Fees").attr('disabled', true);

            $("#class_Fees_chosen").hide();
        }

        else {
            $("#class_Fees").prop('disabled', false);
            $("#class_Fees_chosen").show();
        }
    });


    //$('#txt_checkterm').click(function () {
    //    var isChecked = $('#txt_checkterm:checked').val();
    //    var acid = $("#acYear_Fees").val();
    //    var cid = $().val();
    //    var fid = $().val();

    //    if (isChecked == "on") {


    //        $.getJSON("/FeeConfiguration/getTermsCount", { acid: acid },
    //             function (data) {
                     
    //                 $('#hidden-count').val(data);
    //             });



    //        $.getJSON("/FeeConfiguration/getTerms", { acid: acid },
    //            function (data) {


    //                $('#termDetails').append().empty();
    //                $.each(data, function (i, v) {
    //                    row = $('<tr><td><input type="hidden" name="th' + i + '" id="th' + i + '" value="' + v.TermID
    //                        + '"</td><td><label class="col-lg-12">' + v.TermName + '</label></td><td><input class="col-lg-12  cls_fees " type="text" id="txt-fees' + i
    //                        + '" name="txt_fees' + i + '" placeholder="Enter Fees Amount" required title="Fees Amount" /></td><td><input class="col-lg-12" type="text" id="txt_ServicesTax' + i
    //                        + '" name="txt_ServicesTax' + i + '" placeholder="Services Tax" required title="Services Tax" readonly /></td><td><input class="checkcTax"  id="txt_checkcTax' + i + '"  name="txt_checkcTax' + i + '" type="checkbox" checked /></td><td><input class="col-lg-12" type="text" id="txt_Total' + i
    //                        + '"  name="txt_Total' + i + '" placeholder="Total Amount" required title="Total Amount" readonly /></td><td><input class="col-lg-12 ldate" type="text" id="ldate' + i
    //                        + '" name="ldate' + i + '" placeholder="Select Last Date to Pay" required title="Last Date" readonly /></td></tr>');
    //                    $('#termDetails').append(row);
    //                    var n = i;
    //                    $.getJSON("/FeeConfiguration/CurrentAcademicterm", { acid: acid, tid: v.TermID },
    //                        function (data) {
    //                            if (data.Message == null || data.Message == "") {
    //                                var HolidayDate1 = [];
    //                                var HolidayReason1 = [];
    //                                var sdate = data.StartingEnableDate;
    //                                var edate = data.EndingEnableDate;
    //                                $.getJSON("/Dashboard/FutureActiveDate",
    //                                     function (data) {
    //                                         $.each(data.HolidaysList, function (i, c) {
    //                                             HolidayDate1[i] = c.Date;
    //                                             HolidayReason1[i] = c.Holiday;
    //                                         });
    //                                         var holidays = HolidayDate1;      //["8/04/2015", "08/20/2015"];
    //                                         $(function () {
    //                                             var dates = $("#ldate" + n).datepicker({
    //                                                 dateFormat: 'dd/mm/yy',
    //                                                 minDate: sdate,
    //                                                 maxDate: edate,
    //                                                 changeMonth: true,
    //                                                 changeYear: true,
    //                                                 beforeShowDay: function (date) {
    //                                                     show = true;
    //                                                     for (var i = 0; i < holidays.length; i++) {
    //                                                         if (new Date(holidays[i]).toString() == date.toString()) { return [false, 'holiday green', HolidayReason1[i]]; }//No Holidays
    //                                                     }
    //                                                     if (date.getDay() == 0 || date.getDay() == 6) { return [false, 'holiday red', '']; }//No Weekends
    //                                                     var display = [show, '', (show) ? '' : 'Holidays'];//With Fancy hover tooltip!
    //                                                     return display;
    //                                                 },
    //                                                 onSelect: function (selectedDate) {
    //                                                     var option = this.id == "ldate" + n ? "minDate" : "maxDate",
    //                                                     instance = $(this).data("datepicker"),
    //                                                     date = $.datepicker.parseDate(
    //                                                     instance.settings.dateFormat ||
    //                                                     $.datepicker._defaults.dateFormat,
    //                                                     selectedDate, instance.settings);
    //                                                     dates.not(this).datepicker("option", option, date);
    //                                                 },

    //                                             });
    //                                         });
    //                                     });
    //                            }
    //                            else {
    //                                $("#ldate"+n).datepicker("destroy");
    //                            }

    //                        });
                       
    //                });


    //            });

    //        $.getJSON("/FeeConfiguration/getDate", { acid: acid },
    //            function (data) {
    //                $(".ldate").datepicker("destroy");
    //                var mdate = data.sd + "------" + data.ed;
    //                $(function () {
    //                    var dates = $(".ldate").datepicker({
    //                        dateFormat: 'dd/mm/yy',
    //                        minDate: data.sd,
    //                        maxDate: data.ed,
    //                        changeMonth: true,
    //                        changeYear: true
    //                    });
    //                });
    //            });

    //    }
    //    else {
    //        $('#termDetails').append().empty();
    //    }

    //});

    $('#acYear_Fees').change(function () {
        $("#txt_checkterm").prop("checked", false);
        $('#termDetails').append().empty();
        $("#txt_ldate").val("").html("");
        $("#div_Error").hide();
        var acid = $("#acYear_Fees").val();
        $.getJSON("/FeeConfiguration/getDate", { acid: acid },
            function (data) {
                $("#txt_ldate").datepicker("destroy");
                var mdate = data.sd + "------" + data.ed;
                $(function () {
                    var dates = $("#txt_ldate").datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: data.sd,
                        maxDate: data.ed,
                        changeMonth: true,
                        changeYear: true
                    });
                });

                $(function () {
                    $(".ldate").datepicker("destroy");
                    var dates = $(".ldate").datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: data.sd,
                        maxDate: data.ed,
                        changeMonth: true,
                        changeYear: true
                    });
                });
            });
    });


    $("#FeeacYear").addClass("col-lg-12");
    $("#FeeClass").addClass("col-lg-12");
    $("#FeeCat").addClass("col-lg-12");

    $("#FeeacYear").change(function () {



    });

    $("#FeeClass").change(function () {



    });
    $("#FeeCat").change(function () {



    });


    $(".chzn-select").chosen();




    // Academic Configuration Ends
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //Student Enrollment


    //$("#SE_Sibling_allAcademicYears").addClass("col-lg-12");
    //$("#SE_Sibling_allClass").addClass("col-lg-12");
    //$("#SE_Sibling_Section").addClass("col-lg-12");

    //$(".HaveSibling").change(function (event) {
    //    var S = $('input[name=Sibling]:checked').val();
    //    if (S == "Yes") {
    //        $("#div_Siblings_Yes").show();
    //        $("#div_Siblings_No").hide();
    //    }
    //    if (S == "No") {
    //        $("#div_Siblings_No").show();
    //        $("#div_Siblings_Yes").hide();
    //    }
    //});

    //$('#SE_Sibling_allAcademicYears').change(function (event) {
    //    event.preventDefault();
    //    Load_SE_Sibling_ClassDetails();
    //});

    //function Load_SE_Sibling_ClassDetails() {

    //    var YearId = $('#SE_Sibling_allAcademicYears').val();
    //    $.getJSON('/StudentEnrollment/GetSelectedClass', { passYearId: YearId },
    //        function (orderdetails) {
    //            $("#SE_Sibling_allClass").empty().append('<option value="">Select Class</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#SE_Sibling_allClass").append($('<option></option>').val(c.ClassId).html(c.ClassName));
    //            });
    //        });
    //}

    //$('#SE_Sibling_allClass').change(function (event) {
    //    event.preventDefault();
    //    Load_SE_Sibling_SectionDetails();
    //});

    //function Load_SE_Sibling_SectionDetails() {
    //    var YearId = $('#SE_Sibling_allAcademicYears').val();
    //    var ClassId = $('#SE_Sibling_allClass').val();
    //    $.getJSON('/StudentEnrollment/GetSelectedClassSection', { passClassId: ClassId },
    //        function (orderdetails) {
    //            $("#SE_Sibling_Section").empty().append('<option value="">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#SE_Sibling_Section").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //            });
    //        });
    //}

    //$('#SE_Sibling_Section').change(function (event) {
    //    event.preventDefault();
    //    Load_SE_Sibling_RollNumberDetails();
    //});


    //function Load_SE_Sibling_RollNumberDetails() {
    //    var YearId = $('#SE_Sibling_allAcademicYears').val();
    //    var ClassId = $('#SE_Sibling_allClass').val();
    //    var SecId = $('#SE_Sibling_Section').val();
    //    $.getJSON('/StudentEnrollment/getSelectedSectionRollNumber', { passYearId: YearId, passClassID: ClassId, passSecId: SecId },
    //        function (orderdetails) {
    //            $("#SE_Sibling_SectionStudentRollNumber").empty().append('<option value="">Select RollNumber</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#SE_Sibling_SectionStudentRollNumber").append($('<option></option>').val(c.RegId).html(c.RollNumber));
    //            });
    //        });
    //}

   // $(".HaveGuardian").change(function (event) {

    //    var S = $('input[name=Sibling]:checked').val();
    //    if (S == "Yes") {
    //        $("#div_Siblings_Yes").show();
    //        $("#div_Siblings_No").hide();
    //    }
    //    if (S == "No") {
    //        $("#div_Siblings_No").show();
    //        $("#div_Siblings_Yes").hide();
    //    }
    //});

    //$('#SE_Sibling_allAcademicYears').change(function (event) {
    //    event.preventDefault();
    //    Load_SE_Sibling_ClassDetails();
    //});

    //function Load_SE_Sibling_ClassDetails() {

    //    var YearId = $('#SE_Sibling_allAcademicYears').val();
    //    $.getJSON('/StudentEnrollment/GetSelectedClass', { passYearId: YearId },
    //        function (orderdetails) {
    //            $("#SE_Sibling_allClass").empty().append('<option value="">Select Class</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#SE_Sibling_allClass").append($('<option></option>').val(c.ClassId).html(c.ClassName));
    //            });
    //        });
    //}


    //$('#SE_Sibling_allClass').change(function (event) {
    //    event.preventDefault();
    //    Load_SE_Sibling_SectionDetails();
    //});


    //function Load_SE_Sibling_SectionDetails() {
    //    var YearId = $('#SE_Sibling_allAcademicYears').val();
    //    var ClassId = $('#SE_Sibling_allClass').val();
    //    $.getJSON('/StudentEnrollment/GetSelectedClassSection', { passClassId: ClassId },
    //        function (orderdetails) {
    //            $("#SE_Sibling_Section").empty().append('<option value="">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#SE_Sibling_Section").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //            });
    //        });
    //}

    //$('#SE_Sibling_Section').change(function (event) {
    //    event.preventDefault();
    //    Load_SE_Sibling_RollNumberDetails();
    //});


    //function Load_SE_Sibling_RollNumberDetails() {
    //    var YearId = $('#SE_Sibling_allAcademicYears').val();
    //    var ClassId = $('#SE_Sibling_allClass').val();
    //    var SecId = $('#SE_Sibling_Section').val();
    //    $.getJSON('/StudentEnrollment/getSelectedSectionRollNumber', { passYearId: YearId, passClassID: ClassId, passSecId: SecId },
    //        function (orderdetails) {
    //            $("#SE_Sibling_SectionStudentRollNumber").empty().append('<option value="">Select RollNumber</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#SE_Sibling_SectionStudentRollNumber").append($('<option></option>').val(c.RegId).html(c.RollNumber));
    //            });
    //        });
    //}





    //$(".HaveGuardian").change(function (event) {

    //    var G = $('input[name=Guardian]:checked').val();
    //    if (G == "Yes") {
    //        $("#div_parent").hide();
    //        $("#div_Guardian").show();
    //    }
    //    if (G == "No") {
    //        $("#div_parent").show();
    //        $("#div_Guardian").hide();
    //    }
    //});






    //$("#btnStudentInfo").click(function () {
    //    var sName = $("#txt_studentName").val();
    //    var sGender = $("#Gender").val();
    //    var sDOB = $("#txt_StudentDob").val();
    //    var sEmergencyContactPerson = $("#txt_EmergencycontactPerson").val();
    //    var sEmergencyContactNumber = $("#txt_EmergencycontactNumber").val();
    //    var sAddress = $("#txt_address").val();
    //    var sCity = $("#txt_city").val();
    //    var sState = $("#txt_state").val();
    //    var sCountry = $("#Student_allCountries").val();
    //    var sDOJ = $("#txt_StudentDoj").val();
    //    var sPhoto = $("#StudentPhoto").val();
    //    var Sibling = $('input[name=Sibling]:checked').val();
    //    var p = $('input[name=Guardian]:checked').val();

    //    if (sName == "" || sGender == "" || sDOB == "" || sEmergencyContactPerson == "" || sEmergencyContactNumber == "" || sAddress == "" || sCity == "" || sState == "" || sCountry == "" || sDOJ == "" || sPhoto == "" || Sibling == null) {
    //        $(".er-message").text("Please enter all mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }

    //    if (Sibling == "Yes") {
    //        var YearId = $("#SE_Sibling_allAcademicYears").val();
    //        var ClassId = $("#SE_Sibling_allClass").val();
    //        var SecId = $("#SE_Sibling_Section").val();
    //        var RegId = $("#SE_Sibling_SectionStudentRollNumber").val();

    //        if (YearId == "" || ClassId == "" || SecId == "" || RegId == "") {
    //            $(".er-message").text("Please enter all mantratory fields").css("color", "#ff0000");
    //            return false;
    //        }
    //    }

    //    if (Sibling == "No") {
    //        if (p == null) {
    //            $(".er-message").text("Please enter all mantratory fields").css("color", "#ff0000");
    //            return false;
    //        }
    //    }

    //    if (p == "No") {
    //        var FatherName = $("#txt_FatherName").val();
    //        var FatherEmail = $("#txt_FatherEmail").val();
    //        var FatherMobile = $("#txt_FatherMobile").val();
    //        var FatherQualification = $("#txt_FatherQualification").val();
    //        var FatherIncome = $("#txt_FatherIncome").val();
    //        var FatherPhoto = $("#FatherPhoto").val();

    //        var MotherName = $("#txt_MotherName").val();
    //        var MotherEmail = $("#txt_MotherEmail").val();
    //        var MotherMobile = $("#txt_MotherMobile").val();
    //        var MotherQualification = $("#txt_MotherQualification").val();
    //        var MotherPhoto = $("#MotherPhoto").val();

    //        if (FatherName == "" || FatherIncome == "" || MotherName == "") {
    //            $(".er-message").text("Please enter all mantratory fields").css("color", "#ff0000");
    //            return false;
    //        }
    //        if (FatherEmail == "" && MotherEmail == "") {
    //            $(".er-message").text("Please mention Father or Mother email address").css("color", "#ff0000");
    //            return false;
    //        }

    //        var regex = /^[a-zA-Z ]*$/;
    //        if (regex.test(FatherName)) {
    //        } else {
    //            $(".ValidName-message").text("Please enter valid Name").css("color", "#ff0000");
    //            return false;
    //        }

    //        var regex = /^[a-zA-Z ]*$/;
    //        if (regex.test(MotherName)) {
    //        } else {
    //            $(".ValidName-message").text("Please enter valid Name").css("color", "#ff0000");
    //            return false;
    //        }
    //        if (FatherEmail != "") {
    //            var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    //            var valid = emailReg.test(FatherEmail);

    //            if (!valid) {
    //                $(".ValidMail-message").text("Please enter valid Father Email Id").css("color", "#ff0000");
    //                return false;
    //            }
    //        }

    //        if (MotherEmail != "") {
    //            var emailReg1 = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    //            var valid = emailReg1.test(MotherEmail);

    //            if (!valid) {
    //                $(".ValidMail-message").text("Please enter valid Mother Email Id").css("color", "#ff0000");
    //                return false;
    //            }
    //        }

    //    }


    //    if (p == "Yes") {
    //        var GuardianName = $("#txt_GuardianName").val();
    //        var Relation = $("#txt_GuardianRelation").val();
    //        var GuardianGender = $("#Guardian_Gender").val();
    //        var GuardianEmail = $("#txt_GuardianEmail").val();
    //        var GuardianMobile = $("#txt_GuardianMobile").val();
    //        var GuardianQualification = $("#txt_GuardianQualification").val();
    //        var GuardianIncome = $("#txt_GuardianIncome").val();
    //        var GuardianPhoto = $("#GuardianPhoto").val();

    //        if (GuardianName == "" || Relation == "" || GuardianGender == "" || GuardianEmail == "" || GuardianMobile == "" || GuardianQualification == "" || GuardianIncome == "" || GuardianPhoto == "") {
    //            $(".er-message").text("Please enter all mantratory fields").css("color", "#ff0000");
    //            return false;
    //        }
    //        else {
    //            $.post('/StudentEnrollment/Enrollment', {},
    //            function (data) {
    //                $.each(data, function (i, v) {
    //                   // alert(data);
    //                });
    //            }, 'json');
    //        }

    //        var regex = /^[a-zA-Z ]*$/;
    //        if (regex.test(GuardianName)) {
    //        } else {
    //            $(".ValidName-message").text("Please enter valid Name").css("color", "#ff0000");
    //            return false;
    //        }

    //        if (regex.test(Relation)) {
    //        } else {
    //            $(".ValidName-message").text("Please enter valid Relation").css("color", "#ff0000");
    //            return false;
    //        }

    //        var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    //        var valid = emailReg.test(GuardianEmail);

    //        if (!valid) {
    //            $(".ValidMail-message").text("Please enter valid GuardianEmail Id").css("color", "#ff0000");
    //            return false;
    //        }

    //    }

    //});

    $(".num").filter_input({ regex: '[0-9]' });


    
    //$('#SE_AssignClassToStu_allClass').change(function (event) {
    //    event.preventDefault();
    //    Load_SE_AssignClassToStu_ClassDetails();
    //});


    //function Load_SE_AssignClassToStu_ClassDetails() {

    //    var ClassId = $('#SE_AssignClassToStu_allClass').val();
    //    $.getJSON('/StudentEnrollment/GetSelectedClassSection', { passClassId: ClassId },
    //        function (orderdetails) {
    //            $("#SE_AssignClassToStu_Section").empty().append('<option value="">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#SE_AssignClassToStu_Section").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //            });
    //        });
    //}

    //$('#btnAssignClass').click(function () {
    //    var year = $("#SE_AssignClassToStu_allAcademicYears").val();
    //    var classes = $("#SE_AssignClassToStu_allClass").val();
    //    var sec = $("#SE_AssignClassToStu_Section").val();
    //    var Transport = $("#Transport").val();
    //    var Hostel = $("#Hostel").val();

    //    if (year == "" || classes == "" || sec == "" || Transport == "" || Hostel == "") {
    //        $(".AssignClass-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});

    //$('#SE_AssignClassToStu_allAcademicYears').addClass("col-lg-12");
    //$('#SE_AssignClassToStu_allClass').addClass("col-lg-12");
    //$('#SE_AssignClassToStu_Section').addClass("col-lg-12");
    //$('#Hostel').addClass("col-lg-12");
    //$('#Transport').addClass("col-lg-12");

    //Separate Student
    //$('#Section').change(function (event) {
    //    event.preventDefault();
    //    LoadStudentDetails();
    //});


    //function LoadStudentDetails() {
    //    var SectionId = $('#Section').val();
    //    $.getJSON('/StudentEnrollment/GetSectionStudents', { passSectionId: SectionId },

    //        function (orderdetails) {
    //            $("#SectionStudent").empty().append('<option value="0">Select Student</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#SectionStudent").append($('<option></option>').val(c.StudentRegId).html(c.StudentName));
    //            });
    //        });
    //}

    //$('#btnSeparateValid').click(function () {
    //    var year = $("#allAcademicYears").val();
    //    var classes = $("#allClass").val();
    //    var sec = $("#Section").val();
    //    var student = $("#SectionStudent").val();

    //    if (year == "" || classes == "" || sec == "0" || student == "0") {
    //        $(".SeparateStudentValid-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //    else {
    //        $(".SeparateStudentValid-message").hide();
    //        $("#GetReason").show();
    //    }
    //});

    //$("#txt-DateOfIssue").datepicker();

    //$('#btnSeparateStudent').click(function () {
    //    var date = $("#txt-DateOfIssue").val();
    //    var Reason = $("#txt-Reason").val();
    //    var Description = $("#txt-Description").val();
    //    var tc = $("#Tc").val();
    //    var cc = $("#Cc").val();

    //    if (date == "" || Reason == "" || Description == "" || tc == "" || cc == "") {
    //        $(".SeparateStudent-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});



    //$('#btnOldStudent').click(function () {
    //    var year = $("#allAcademicYears").val();
    //    var classes = $("#allClass").val();
    //    var sec = $("#Section").val();

    //    $("#allAcademicYears").addClass("current_button");
    //    $("#allClass").addClass("current_button");
    //    $("#Section").addClass("current_button");

    //    var a = [];
    //    var field_val = [];
    //    $(".current_button").each(function () {
    //        var valu = $(this).val();
    //        if (valu == '') {
    //            var null_id = $(this).attr('id');
    //            if (null_id == 'allAcademicYears') {

    //                a.push('Please select academic year');
    //            }
    //            if (null_id == 'allClass') {

    //                a.push('Please select class');
    //            }
    //            if (null_id == 'Section') {

    //                a.push('Please select section');
    //            }
    //        }
    //        else {
    //        }
    //    });

    //    if (a.length != 0) {
    //        $("#MandatoryFieldErrorMessage_List").show();
    //        $(".List_jErr").append().empty();
    //        $.each(a, function (i, v) {
    //            if (a[i] != "" && a[i] != null) {
    //                li = $("<li></li>").text(a[i]);
    //                $(".List_jErr").append(li);
    //            }
    //        });
    //        a = [];
    //        return false;
    //    }
    //    else {
    //        $("#MandatoryFieldErrorMessage_List").hide();
    //        $("#OldStudentList").show();
    //    }
    //});


    //$('#btnApplySeparation').click(function () {
    //    var year = $("#allAcademicYears").val();
    //    if (year == "") {
    //        $("#MandatoryFieldErrorMessage_List").show();
    //        return false;
    //    }
    //    $("#MandatoryFieldErrorMessage_List").hide();
    //});




    //------------------------------------------------ Fee Collection Starts--------------------------------------------------------------//

    $("#Subjects").addClass("col-lg-12");
    $("#acYear_Sub").addClass("col-lg-12");
    $("#ClassList").addClass("col-lg-12");
    $("#SectionList").addClass("col-lg-12");
    $("#AcyearList").addClass("col-lg-12");
    $("#FeeCatList").addClass("col-lg-12");
    $("#class_Sub").addClass("col-lg-12");
    $("#Section_sub").addClass("col-lg-12");

    $("#Subjects1").addClass("col-lg-12");
    $("#acYear_Sub1").addClass("col-lg-12");
    $("#class_Sub1").addClass("col-lg-12");
    $("#Section_sub1").addClass("col-lg-12");

    //$("#acYear_Sub").change(function () {     
    //    $("#class_Sub").empty().append('<option value>Select Class</option>');
    //    $("#Section_sub").empty().append('<option value>Select Section</option>');
    //    $("#Subjects").empty().append('<option value>Select Subject</option>');

    //    $.getJSON('/MarkConfiguration/getClass',

    //   function (data) {         
    //       $("#class_Sub").empty().append('<option value>Select Class</option>');
    //       $.each(data, function (i, c) {
    //           $("#class_Sub").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //       });

    //   });
    //});

    //$("#class_Sub").change(function () {

    //    var cid = $("#class_Sub").val();

    //    $("#Section_sub").empty().append('<option value>Select Section</option>');
    //    $("#Subjects").empty().append('<option value>Select Subject</option>');

    //    $.getJSON('/MarkConfiguration/getSection', { cid: cid },

    //    function (data) {
    //        $("#Section_sub").empty().append('<option value>Select Section</option>');
    //        $.each(data, function (i, c) {
    //            $("#Section_sub").append($('<option></option>').val(c.SectionId).html(c.Section));
    //        });

    //    });
    //});


    //$("#acYear_Sub1").change(function () {
       
       
    //    $("#class_Sub1").empty().append('<option value>Select Class</option>');
    //    $("#Section_sub1").empty().append('<option value>Select Section</option>');
    //    $("#Subjects1").empty().append('<option value>Select Subject</option>');

    //    $.getJSON('/MarkConfiguration/getClass',

    //   function (data) {         
    //       $("#class_Sub1").empty().append('<option value>Select Class</option>');
    //       $.each(data, function (i, c) {
    //           $("#class_Sub1").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //       });

    //   });
    //});

    //$("#class_Sub1").change(function () {

    //    var cid = $("#class_Sub1").val();

    //    $("#Section_sub1").empty().append('<option value>Select Section</option>');
    //    $("#Subjects1").empty().append('<option value>Select Subject</option>');

    //    $.getJSON('/MarkConfiguration/getSection', { cid: cid },

    //    function (data) {
    //        $("#Section_sub1").empty().append('<option value>Select Section</option>');
    //        $.each(data, function (i, c) {
    //            $("#Section_sub1").append($('<option></option>').val(c.SectionId).html(c.Section));
    //        });

    //    });
    //});


    //$("#Section_sub1").change(function () {

    //    var acid = $("#acYear_Sub1").val();
    //    var cid = $("#class_Sub1").val();
    //    var sec_id = $("#Section_sub1").val();
    //    $("#Subjects1").empty().append('<option value >Select Subject</option>');

    //    $.getJSON('/MarkConfiguration/getSubject', { acid: acid, cid: cid, sec_id: sec_id },
    //    function (data) {
    //        $("#Subjects1").empty().append('<option value >Select Section</option>');
    //        $.each(data, function (i, c) {
    //            $("#Subjects1").append($('<option></option>').val(c.sub_id).html(c.sub_name));
    //        });
          
    //    });
    //});

    //$("#acYear_Roll").addClass("col-lg-12");
    //$("#class_Roll").addClass("col-lg-12");
    //$("#Section_Roll").addClass("col-lg-12");
    //$("#acYear_Roll").change(function () {

     
    //    $("#class_Roll").empty().append('<option value >Select Class</option>');
    //    $("#Section_Roll").empty().append('<option value>Select Section</option>');
    //    $("#Subjects").empty().append('<option value>Select Subject</option>');

    //    $.getJSON('/RollNumberManagement/getClass',

    //   function (data) {
    //       $("#class_Roll").empty().append('<option value>Select Class</option>');
    //       $.each(data, function (i, c) {
    //           $("#class_Roll").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //       });

    //   });
    //});

    //$("#class_Roll").change(function () {

    //    var cid = $("#class_Roll").val();

    //    $("#Section_Roll").empty().append('<option value>Select Section</option>');


    //    $.getJSON('/RollNumberManagement/getSection', { cid: cid },

    //    function (data) {
    //        $("#Section_Roll").empty().append('<option value>Select Section</option>');
    //        $.each(data, function (i, c) {
    //            $("#Section_Roll").append($('<option></option>').val(c.SectionId).html(c.Section));
    //        });

    //    });
    //});


    //$("#Section_sub").change(function () {

    //    var acid = $("#acYear_Sub").val();
    //    var cid = $("#class_Sub").val();
    //    var sec_id = $("#Section_sub").val();


    //    $("#Subjects").empty().append('<option value>Select Subject</option>');

    //    $.getJSON('/MarkConfiguration/getSubject', { acid: acid, cid: cid, sec_id: sec_id },

    //    function (data) {
    //        $("#Subjects").empty().append('<option value>Select Subject</option>');
    //        $.each(data, function (i, c) {
    //            $("#Subjects").append($('<option></option>').val(c.sub_id).html(c.sub_name));
    //        });
            
    //    });
    //});


    //$("#AcyearList").change(function () {

    //    $("#txt_rNo").val("").html("");
    //    $("#div-feeDetais1").hide();
    //    $("#div-TermfeeDetais1").hide();
    //    $("#SectionList").empty().append('<option value="">Select Section</option>');
    //    $("#ClassList").empty().append('<option value="">Select Class</option>');
    //    $("#StudentList").empty().append('<option value="">Select Student</option>');

    //    $.getJSON('/FeeManagement/getClass',

    //   function (data) {
    //       $("#ClassList").empty().append('<option value="">Select Class</option>');
    //       $.each(data, function (i, c) {
    //           $("#ClassList").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //       });

    //   });



    //    $.getJSON('/FeeCollection/getClass',

    //   function (data) {
    //       $("#ClassList").empty().append('<option value="">Select Class</option>');
    //       $.each(data, function (i, c) {
    //           $("#ClassList").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //       });

    //   });
    //});

    //$("#FeeCatList").change(function () {
    //    $("#SectionList").empty().append('<option value="">Select Section</option>');
    //    $("#ClassList").empty().append('<option value="">Select Class</option>');
    //    $("#StudentList").empty().append('<option value="">Select Student</option>');
    //    $.getJSON('/FeeManagement/getClass',

    //   function (data) {
    //       $("#ClassList").empty().append('<option value="">Select Class</option>');
    //       $.each(data, function (i, c) {
    //           $("#ClassList").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //       });

    //   });
    //    $.getJSON('/FeeCollection/getClass',

    //  function (data) {
    //      $("#ClassList").empty().append('<option value="">Select Class</option>');
    //      $.each(data, function (i, c) {
    //          $("#ClassList").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //      });

    //  });
    //});

    //$("#ClassList").change(function () {
    //    $("#txt_rNo").val("").html("");
    //    $("#div-feeDetais1").hide();
    //    $("#div-TermfeeDetais1").hide();
    //    var cid = $("#ClassList").val();

    //    $("#SectionList").empty().append('<option value="">Select Section</option>');
    //    $("#StudentList").empty().append('<option value="">Select Student</option>');

    //    $.getJSON('/FeeManagement/getSection', { cid: cid },

    //    function (data) {
    //        $("#SectionList").empty().append('<option value="">Select Section</option>');
    //        $.each(data, function (i, c) {
    //            $("#SectionList").append($('<option></option>').val(c.SectionId).html(c.Section));
    //        });

    //    });

    //    $.getJSON('/FeeCollection/getSection', { cid: cid },

    //   function (data) {
    //       $("#SectionList").empty().append('<option value="">Select Section</option>');
    //       $.each(data, function (i, c) {
    //           $("#SectionList").append($('<option></option>').val(c.SectionId).html(c.Section));
    //       });

    //   });
    //});

    //$("#SectionList").change(function () {
    //    $("#txt_rNo").val("").html("");
    //    $("#div-feeDetais1").hide();
    //    $("#div-TermfeeDetais1").hide();
    //    var cid = $("#ClassList").val();
    //    var secid = $("#SectionList").val();
    //    var acid = $("#AcyearList").val();
    //    $("#StudentList").empty().append('<option value="">Select Student</option>');
    //    $("#StudentList").empty().append('<option value="">Select Student</option>');
    //    $.getJSON('/FeeManagement/getStudent', { acid: acid, sec_id: secid, cid: cid },

    //       function (data) {
    //           $("#StudentList").empty().append('<option value="">Select Student</option>');
    //           $.each(data, function (i, v) {
    //               $("#StudentList").append($('<option></option>').val(v.sid).html(v.sname));
    //           });
    //       });

    //    $.getJSON('/FeeCollection/getStudent', { acid: acid, sec_id: secid, cid: cid },

    //      function (data) {
    //          $("#StudentList").empty().append('<option value="">Select Student</option>');
    //          $.each(data, function (i, v) {
    //              $("#StudentList").append($('<option></option>').val(v.sid).html(v.sname));
    //          });
    //      });

    //});


    //$("#StudentList").change(function () {
    //    $("#txt_rNo").val("").html("");
    //    var acid = $("#AcyearList").val();
    //    var fid = $("#FeeCatList").val();
    //    var cid = $("#ClassList").val();
    //    $("#div-feeDetais1").hide();
    //    $("#div-TermfeeDetais1").hide();
        
    //    $.getJSON('/FeeCollection/getFeeAmount', { acid: acid, fid: fid, cid: cid },

    //       function (data) {
    //           $("#txt-amount").val(data.FeeAmount).html(data.FeeAmount);
    //           $("#txt-totalamount").val(data.FeeAmount).html(data.FeeAmount);
    //       });


    //});

    $(".mode").change(function () {

        var mode = $("input[name=txt-paytype]:checked").val();
        var acid = $("#AcyearList").val();
        var fid = $("#FeeCatList").val();
        var cid = $("#ClassList").val();

        if (mode == "Single") {
            $("#div-fee").hide("slow");
            $.getJSON('/FeeCollection/getFeeAmount', { acid: acid, fid: fid, cid: cid },

               function (data) {
                   $("#txt-amount").val(data.FeeAmount).html(data.FeeAmount);
                   $("#txt-totalamount").val(data.FeeAmount).html(data.FeeAmount);
               });

        }
        else {
            $("#div-fee").show("slow");
            $.getJSON('/FeeCollection/getTerms', { acid: acid },

               function (data) {
                   $('#txt-termAmount').empty().append();

                   $.each(data, function (i, c) {
                       $("#txt-termAmount").append($('<option></option>').val(c.TermID).html(c.TermName));
                   });
               });
        }

    });



    $("#txt-termAmount").change(function () {

        var termid = $("#txt-termAmount").val();
        var acid = $("#AcyearList").val();
        var fid = $("#FeeCatList").val();
        var cid = $("#ClassList").val();
        $.getJSON('/FeeCollection/getTermFeeAmount', { acid: acid, fid: fid, cid: cid, tid: termid },
             function (data) {
                 $("#txt-amount").val(data.FeeAmount).html(data.FeeAmount);
                 $("#txt-totalamount").val(data.FeeAmount).html(data.FeeAmount);
             });

    });

    $("#txt-fineamount").blur(function () {

        var fine = $("#txt-fineamount").val();
        var amt = $("#txt-amount").val();
        var total = parseInt($("#txt-fineamount").val()) + parseInt($("#txt-amount").val());
        $("#txt-totalamount").val(total).html(total);

        var remaining = parseInt($("#txt-totalamount").val()) - parseInt($("#txt-collamount").val());
        $("#txt-ramount").val(remaining).html(remaining);

    });
    $("#txt-collamount").blur(function () {

        var remaining = parseInt($("#txt-totalamount").val()) - parseInt($("#txt-collamount").val());
        $("#txt-ramount").val(remaining).html(remaining);

    });

    $("#txt-paymentMode").change(function () {
        var modeid = $("#txt-paymentMode").val();

        if (modeid == "0") {
            $("#txt-cheque").hide("slow");
            $("#txt-dd").hide("slow");
        }
        if (modeid == "1") {
            $("#txt-cheque").hide("slow");
            $("#txt-dd").hide("slow");
        }
        if (modeid == "2") {
            $("#txt-cheque").show("slow");
            $("#txt-dd").hide("slow");
        }
        if (modeid == "3") {
            $("#txt-cheque").hide("slow");
            $("#txt-dd").show("slow");
        }
    });






    //------------------------------------------------ Fee Collection End--------------------------------------------------------------//






    //----------------------------------------------Fee Management Starts-----------------------------------------------------------------------//


    //$(".StudentList1").change(function () {
    //    var acid = $("#AcyearList").val();
    //    var fid = $("#FeeCatList").val();
    //    var cid = $("#ClassList").val();
    //    var sec_id = $("#SectionList").val();
    //    var stu_id = $("#StudentList").val();

    //    $.getJSON('/FeeManagement/getFeeAmount', { acid: acid, fid: fid, cid: cid, sec_id: sec_id, stu_id: stu_id },

    //       function (data) {
    //           if (data.msg == null && data.msg == "") {
    //               $("#div-feeNoRec").show();
    //               $("#div-feeDetais").hide();
    //           }
    //           else {
    //               $("#div-feeDetais").show();
    //               $("#div-feeNoRec").hide();
    //               $("#tbody-feePaid").empty().append();
    //               $.each(data.ans, function (i, c) {
    //                   var total = c.famt + c.fine;

    //                   row = '<tr><td><input class="col-lg-12" type="text" title="Fee" value=' + data.term + '  readonly /></td> <td><input class="col-lg-12" type="text" value=' + c.Fee + ' title="Amount" readonly /></td><td><input class="col-lg-12" type="text" title="Fine Amount" value="0" placeholder="Enter Fine Amount" value=' + c.Fine + '  readonly /></td> <td><input class="col-lg-12" type="text"  title="Total Amount" value=' + total + ' readonly /></td>   <td><input class="col-lg-12" type="text" value=' + c.AmountCollected + ' title="Amount Collected"  readonly /></td><td><input class="col-lg-12" type="text" value=' + c.ReminingAmount + ' title="Remaining Amount"  readonly /></td><td><textarea class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  title="Remark" style="resize:none;">' + c.Remark + '</textarea></td></tr>';
    //                   $("#tbody-feePaid").append(row);
    //               });
    //           }




    //       });


    //});

   


    //------------------------------------------------Fee Management End----------------------------------------------------------------------------------



    // Student Managene 

    

    //$("#AcyearStdList1").addClass("col-lg-12");

    //$("#AcyearStdList1").change(function () {
    //    $("#ClassStdList1").empty().append('<option value>Select Class</option>');
    //    $("#SectionStdList2").empty().append('<option value>Select Section</option>');
    //    $.getJSON('/StudentManagement/getClassList',
    //       function (data) {


    //           $.each(data, function (i, c) {

    //               $("#ClassStdList1").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //           });

    //       });

    //});

    //$("#AcyearStdList").change(function () {
    //    $("#ClassStdList").empty().append('<option value>Select Class</option>');
    //    $("#SectionStdList").empty().append('<option value>Select Section</option>');
    //    $("#StudentList").empty().append('<option value>Select Student</option>');
    //    $("#ExamList").empty().append('<option value>Select Exam</option>');
    //    $("#Student_List").empty().append('<option value>Select Student</option>');
    //    $("#SectionStdList1").empty().append('<option value>Select Student</option>');
   
    //    var s = 1;
        
    //    $.getJSON('/StudentManagement/ClassList', 
    //       function (data) {


    //           $.each(data, function (i, c) {

    //               $("#ClassStdList").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //           });

    //       });

    //});

    //$("#ClassStdList").change(function () {
        
    //    var cid = $("#ClassStdList").val();
        
    //    $.getJSON('/StudentManagement/getSectionList', { cid: cid },
    //       function (data) {
    //           $("#SectionStdList").empty().append('<option value>Select Section</option>');
    //           $.each(data, function (i, c) {
    //               $("#SectionStdList").append($('<option></option>').val(c.SectionId).html(c.Section));
    //           });
    //           $("#SectionStdList1").empty().append('<option value>Select Section</option>');
    //           $.each(data, function (i, c) {
    //               $("#SectionStdList1").append($('<option></option>').val(c.SectionId).html(c.Section));
    //           });
    //       });

    //});
    //$("#ClassStdList1").change(function () {

    //    var cid = $("#ClassStdList1").val();
    //    $.getJSON('/StudentManagement/getSectionList', { cid: cid },
    //       function (data) {
    //           $("#SectionStdList2").empty().append('<option value>Select Section</option>');
    //           $.each(data, function (i, c) {
    //               $("#SectionStdList2").append($('<option></option>').val(c.SectionId).html(c.Section));
    //           });

    //       });

    //});

    //$("#SectionStdList").change(function () {

    //    var acid = $("#AcyearStdList").val();
    //    var cid = $("#ClassStdList").val();
    //    var sec_id = $("#SectionStdList").val();
    //    $.getJSON('/StudentManagement/getStudentList', { acid:acid, cid: cid, sec_id: sec_id },
    //       function (data) {
    //           $("#StudentList").empty().append('<option value>Select Student</option>');
    //           $.each(data, function (i, c) {
    //               $("#StudentList").append($('<option></option>').val(c.std_id).html(c.std));
    //           });

    //       });

    //    $.getJSON('/StudentManagement/getExam',
    //       function (data) {
    //           $("#ExamList").empty().append('<option value>Select Exam</option>');
    //           $.each(data, function (i, c) {
    //               $("#ExamList").append($('<option></option>').val(c.examId).html(c.exam));
    //           });

    //       });

    //});


    //$("#SectionStdList1").change(function () {

    //    var cid = $("#ClassStdList").val();
    //    var sec_id = $("#SectionStdList1").val();
    //    $.getJSON('/StudentManagement/getStudentList', { cid: cid, sec_id: sec_id },
    //       function (data) {
    //           $("#Student_List").empty().append('<option value>Select Student</option>');
    //           $.each(data, function (i, c) {
    //               $("#Student_List").append($('<option></option>').val(c.std_id).html(c.std));
    //           });

    //       });


    //});
    //$("#ExamList").change(function () {

    //    var acid = $("#AcyearStdList").val();
    //    var cid = $("#ClassStdList").val();
    //    var sec_id = $("#SectionStdList").val();
    //    $.getJSON('/StudentManagement/getStudentList', { acid:acid, cid: cid, sec_id: sec_id },
    //       function (data) {
    //           $("#Student_List").empty().append('<option value>Select Student</option>');
    //           $.each(data, function (i, c) {
    //               $("#Student_List").append($('<option></option>').val(c.std_id).html(c.std));
    //           });

    //       });

    //});



    //Transport Management

    //$("#transportAcyear").addClass("col-lg-12");
    //$("#transportClass").addClass("col-lg-12");
    //$("#transportSection").addClass("col-lg-12");
    //$("#transportStudent").addClass("col-lg-12");

    //$("#transportAcyear1").addClass("col-lg-12");
    //$("#transportClass1").addClass("col-lg-12");
    //$("#transportSection1").addClass("col-lg-12");

    //$("#transportAcyear").change(function () {

    //    $.getJSON('/TransportManagement/getClassList',
    //       function (data) {
    //           $("#transportClass").empty().append('<option value="">Select Class</option>');
    //           $("#transportSection").empty().append('<option value="">Select Section</option>');
    //           $("#transportStudent").empty().append('<option value="">Select Student</option>');
    //           $.each(data, function (i, c) {
    //               $("#transportClass").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //           });

    //       });

    //});
    //$("#transportAcyear1").change(function () {

    //    $.getJSON('/TransportManagement/getClassList',
    //       function (data) {
    //           $("#transportClass1").empty().append('<option value="">Select Class</option>');
    //           $("#transportSection1").empty().append('<option value="">Select Section</option>');

    //           $.each(data, function (i, c) {
    //               $("#transportClass1").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //           });

    //       });

    //});

    //$("#transportClass").change(function () {
    //    var cid = $("#transportClass").val();
    //    $.getJSON('/TransportManagement/getSectionList', { cid: cid },
    //       function (data) {
    //           $("#transportSection").empty().append('<option value="">Select Section</option>');
    //           $("#transportStudent").empty().append('<option value="">Select Student</option>');
    //           $.each(data, function (i, c) {
                  
    //               $("#transportSection").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //           });

    //       });

    //});
    //$("#transportClass1").change(function () {
    //    var cid = $("#transportClass1").val();
    //    $.getJSON('/TransportManagement/getSectionList', { cid: cid },
    //       function (data) {
    //           $("#transportSection1").empty().append('<option value="">Select Section</option>');

    //           $.each(data, function (i, c) {
                  
    //               $("#transportSection1").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //           });

    //       });

    //});

    //$("#transportSection").change(function () {
    //    var cid = $("#transportClass").val();
    //    var sec_id = $("#transportSection").val();
    //    var acid = $("#transportAcyear").val();
    //    $.getJSON('/TransportManagement/getStudent', { acid: acid, cid: cid, sec_id: sec_id },
    //       function (data) {
    //           $("#transportStudent").empty().append('<option value="">Select Student</option>');
    //           $.each(data, function (i, c) {
    //               $("#transportStudent").append($('<option></option>').val(c.sid).html(c.sname));
    //           });

    //       });

    //});



    //Hostel Management


    //$("#hostelAcyear").addClass("col-lg-12");
    //$("#hostelEmpList").addClass("col-lg-12");
    //$("#hostelAcyear1").addClass("col-lg-12");
    //$("#hostelClass").addClass("col-lg-12");
    //$("#hostelSection").addClass("col-lg-12");
    //$("#hostelStudent").addClass("col-lg-12");


    //$("#hostelAcyear").change(function () {
    //    $("#hostelSection").empty().append('<option value="">Select Section</option>');
    //    $("#hostelStudent").empty().append('<option value="">Select Student</option>');
    //    $.getJSON('/HostelManagement/getEmployee',
    //       function (data) {

    //           $("#hostelEmpList").empty().append('<option value>Select Employee</option>');
    //           $.each(data, function (i, c) {

    //               $("#hostelEmpList").append($('<option></option>').val(c.eid).html(c.ename));
    //           });

    //       });

    //    $.getJSON('/HostelManagement/getClassList',
    //       function (data) {

    //           $("#hostelClass").empty().append('<option value="">Select Class</option>');
    //           $.each(data, function (i, c) {

    //               $("#hostelClass").append($('<option></option>').val(c.ClassId).html(c.ClassType));
    //           });

    //       });

    //});



    //$("#hostelClass").change(function () {
    //    var cid = $("#hostelClass").val();
    //    $("#hostelStudent").empty().append('<option value="">Select Student</option>');
    //    $.getJSON('/HostelManagement/getSectionList', { cid: cid },
    //       function (data) {
    //           $("#hostelSection").empty().append('<option value="">Select Section</option>');
    //           $.each(data, function (i, c) {
    //               $("#hostelSection").append($('<option></option>').val(c.SectionId).html(c.Section));
    //           });

    //       });

    //});

    //$("#hostelSection").change(function () {
    //    var cid = $("#hostelClass").val();
    //    var sec_id = $("#hostelSection").val();
    //    var acid = $("#hostelAcyear").val();
    //    $.getJSON('/HostelManagement/getStudent', { acid: acid, cid: cid, sec_id: sec_id },
    //       function (data) {
    //           $("#hostelStudent").empty().append('<option value="">Select Student</option>');
    //           $.each(data, function (i, c) {
    //               $("#hostelStudent").append($('<option></option>').val(c.sid).html(c.sname));
    //           });

    //       });

    //});




    // School Management


    $("#Fac_AcYearId").addClass("col-lg-12");
    $("#FacId").addClass("col-lg-12");
    $("#EmpRegId").addClass("col-lg-12");
    $("#ActivityId").addClass("col-lg-12");
    $("#Act_ClassId").addClass("col-lg-12");
    $("#Act_SectionId").addClass("col-lg-12");
    $("#Act_StudentId").addClass("col-lg-12");
    $("#EmpId").addClass("col-lg-12");
   

    //$("#Act_ClassId").change(function () {
    //    var cid = $("#Act_ClassId").val();
    //    $("#Act_StudentId").empty().append('<option value="">Select Student</option>');
    //    $.getJSON('/SchoolManagement/getSectionList', { cid: cid },
    //       function (data) {
    //           $("#Act_SectionId").empty().append('<option value="">Select Section</option>');

    //           $.each(data, function (i, c) {
    //               $("#Act_SectionId").append($('<option></option>').val(c.SectionId).html(c.Section));
    //           });

    //       });
    //});

    //$("#Act_SectionId").change(function () {
    //    var cid = $("#Act_ClassId").val();
    //    var sec_id = $("#Act_SectionId").val();
    //    var acid = $("#Fac_AcYearId").val();
    //    $.getJSON('/SchoolManagement/getStudent', { acid: acid, cid: cid, sec_id: sec_id },
    //       function (data) {
    //           $("#Act_StudentId").empty().append('<option value="">Select Student</option>');
    //           $.each(data, function (i, c) {
    //               $("#Act_StudentId").append($('<option></option>').val(c.sid).html(c.sname));
    //           });

    //       });

    //});




    $("#RoleId").addClass("col-lg-12");
    $("#FeatureId").addClass("col-lg-12");








    //Grade Configuration


    $("#GradeacYear").addClass("col-lg-12");
    $("#GradeacYear1").addClass("col-lg-12");
    $("#Gradeclass1").addClass("col-lg-12");
    $("#GradeType").addClass("col-lg-12");
    $("#class_Grade").addClass("col-lg-12");
    $("#GradeType").change(function () {
        var gradeTypeId = $("#GradeType").val();
        $("#txt-gradeType").val(gradeTypeId);
        $.getJSON('/GradeConfiguration/getGradeDetails', { gradeTypeId: gradeTypeId },
           function (data) {
               if (data > 0) {
                   $("#div-viewGradeDetails").show();
                   $("#div-addGradeDetails").hide();
                   $("#div-displayGradeDetails").hide();
                   $("#div-editGradeDetails").hide();
                   $("#div-enterGradeDetails").hide();
               }
               else {
                   $("#div-addGradeDetails").show();
                   $("#div-viewGradeDetails").hide();
                   $("#div-displayGradeDetails").hide();
                   $("#div-editGradeDetails").hide();
               }

           });
        $.getJSON('/GradeConfiguration/getGradelist', { gradeTypeId: gradeTypeId },
          function (data) {
              $("#divtable-Grade").show();
              $('#tbody-viewGradeDetails').empty();
              $.each(data, function (i, v) {

                  row = $('<tr><td>' + v.g + '</td><td>' + v.m1 + '</td><td>' + v.m2 + '</td><td>' + v.d + '</td></tr>');
                  $('#tbody-viewGradeDetails').append(row);

              });
          });
    });




    $("#div-addGradeDetails").click(function () {
        var x = $("#GradeType").val();
        if (x == null || x == "") {
         //   alert("Please select grade type");
            return false;
        }
        $("#div-enterGradeDetails").show();
        $('.minMark').filter_input({ regex: '[0-9-.]' });
        $('.maxMark').filter_input({ regex: '[0-9-.]' });

    });


    function updateImageTableRows() {

        var rowIndex = $('#addGradeDetails tbody tr').length ;        
        for (var i = 0; i < rowIndex; i++) {
           
            var gradeName = $('#addGradeDetails tbody tr').eq(i).find("td:first input");
            var minMark = $('#addGradeDetails tbody tr').eq(i).find("td:nth-child(2) input");
            var maxMark = $('#addGradeDetails tbody tr').eq(i).find("td:nth-child(3) input");
            var txgradeDesc = $('#addGradeDetails tbody tr').eq(i).find("td:nth-child(4) textarea");
            
            gradeName.attr('id', 'gradeName' + (i+1));
            minMark.attr('id', 'minMark' + (i+1));
            maxMark.attr('id', 'maxMark' + (i + 1));
            txgradeDesc.attr('id', 'txt-gradeDesc' + (i + 1));
            gradeName.attr('name', 'gradeName' + (i + 1));
            minMark.attr('name', 'minMark' + (i + 1));
            maxMark.attr('name', 'maxMark' + (i + 1));
            txgradeDesc.attr('name', 'txt-gradeDesc' + (i + 1));
           

        }
    }


    $("#div-viewGradeDetails").click(function () {

        $("#div-enterGradeDetails").hide();
        $("#div-displayGradeDetails").show();
        $("#div-editGradeDetails").hide();
        var gradeTypeId = $("#GradeType").val();
        $("#txt-gradeType").val(gradeTypeId);
        $('#tbody-viewGradeDetails').empty().append();
        $.getJSON('/GradeConfiguration/getGradelist', { gradeTypeId: gradeTypeId },
           function (data) {

               $("#div-viewGradeDetails").hide();
               $("#div-editGradeDetails").show();
               var sd = '<a href="/GradeConfiguration/EditGrade/?gid=' + gradeTypeId + '">Edit</a>';
               $("#div-editGradeDetails").append().empty();
               $("#div-editGradeDetails").append(sd);
               $.each(data, function (i, v) {

                   row = $('<tr><td>' + v.g + '</td><td>' + v.m1 + '</td><td>' + v.m2 + '</td><td>' + v.d + '</td></tr>');
                   $('#tbody-viewGradeDetails').append(row);

               });
           });

    });


    //var count = 1;

    //$("#btnUpdateNewRow").click(function () {
    //    count++;
    //    row = $('<tr><td><input class="col-lg-12" type="text" id="gradeName' + count + '" name="gradeName' +
    //        count + '" placeholder="Enter GradeName" required title="Grade Name" /></td> <td><input type="text" class="col-lg-12 minMark" id="minMark' + count + '" name="minMark' +
    //        count + '" placeholder="Enter Min Mark" required title="Min Mark" /></td><td><input type="text" class="col-lg-12 maxMark" id="maxMark' + count + '" name="maxMark' +
    //        count + '" placeholder="Enter Max Mark" required title="Max Mark" /></td><td><textarea class="col-lg-12" id="txt-gradeDesc' + count + '" name="txt-gradeDesc' +
    //        count + '" placeholder="Description" required title="Grade Description" style="resize:none;height:50px;"> </textarea></td><td><a href="#" id="ibtnDel">Delete</a></td></tr>');
    //    $("#tbody-updateGradeDetails").append(row);

    //    $('.minMark').filter_input({ regex: '[0-9-.]' });
    //    $('.maxMark').filter_input({ regex: '[0-9-.]' });
    //    $("#count").val(count).html(count);
    //});


    $("table.updateGrade").on("click", "#ibtnDel", function (event) {
        $(this).closest("tr").remove();

        count--;
        $("#counter").val(count).html(count);

    });


    $("#UserId").addClass("col-lg-12");

    $("#ClassStdList").addClass("col-lg-12");
    $("#SectionStdList").addClass("col-lg-12");




















    $("[data-widget='collapse']").click(function () {
        //Find the box parent        
        var box = $(this).parents(".box").first();
        //Find the body and the footer
        var bf = box.find(".box-body, .box-footer");
        if (!box.hasClass("collapsed-box")) {
            box.addClass("collapsed-box");
            //Convert minus into plus
            $(this).children(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
            bf.slideUp();


        } else {
            box.removeClass("collapsed-box");
            //Convert plus into minus
            $(this).children(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
            bf.slideDown();
            $(".jsonError").hide();
            $(".jsonExceptionMsg").hide();
            $(".jsonSuccessMsg").hide();
            $(".jsonErrorMsg").hide();
            $("#div_Error").hide();
            $("#div_Error1").hide();
            $("#show").hide();
        }
    });


    $('.collapse').on('shown.bs.collapse', function (e) {
        $('.collapse').not(this).removeClass('in');
    });

    $('[data-toggle=collapse]').click(function (e) {
        $('[data-toggle=collapse]').parent('li').removeClass('active');
        $(this).parent('li').toggleClass('active');
    });




    $(".sub_menuactive").click(function () {
        $(this).addClass("dynamicactive");
    });









    //AssignClassToStudent

    //$('#allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadClassDetails();
    //});


    //function LoadClassDetails() {

    //    var ClassId = $('#allClass').val();
    //    $.getJSON('/StudentEnrollment/GetSelectedClassSection', { passClassId: ClassId },
    //        function (orderdetails) {
    //            $("#Section").empty().append('<option value="0">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#Section").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //            });
    //        });
    //}

    //$('#btnAssignClass').click(function () {
    //    var year = $("#allAcademicYears").val();
    //    var classes = $("#allClass").val();
    //    var sec = $("#Section").val();
    //    var Transport = $("#Trasport").val();
    //    var Hostel = $("#Hostel").val();

    //    if (year == "" || classes == "" || sec == "" || Transport == "" || Hostel == "") {
    //        $(".AssignClass-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});


    //Separate Student
    //$('#Section').change(function (event) {
    //    event.preventDefault();
    //    LoadStudentDetails();
    //});


    //function LoadStudentDetails() {
    //    var SectionId = $('#Section').val();
    //    $.getJSON('/StudentEnrollment/GetSectionStudents', { passSectionId: SectionId },

    //        function (orderdetails) {
    //            $("#SectionStudent").empty().append('<option value="0">Select Student</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#SectionStudent").append($('<option></option>').val(c.StudentRegId).html(c.StudentName));
    //            });
    //        });
    //}

    //$('#btnSeparateValid').click(function () {
    //    var year = $("#allAcademicYears").val();
    //    var classes = $("#allClass").val();
    //    var sec = $("#Section").val();
    //    var student = $("#SectionStudent").val();

    //    if (year == "" || classes == "" || sec == "0" || student == "0") {
    //        $(".SeparateStudentValid-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //    else {
    //        $(".SeparateStudentValid-message").hide();
    //        $("#GetReason").show();
    //    }
    //});

    //$("#txt-DateOfIssue").datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    dateFormat: 'dd/mm/yy',
    //});

    //$('#btnSeparateStudent').click(function () {
    //    var date = $("#txt-DateOfIssue").val();
    //    var Reason = $("#txt-Reason").val();
    //    var Description = $("#txt-Description").val();
    //    var tc = $("#Tc").val();
    //    var cc = $("#Cc").val();

    //    if (date == "" || Reason == "" || Description == "" || tc == "" || cc == "") {
    //        $(".SeparateStudent-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});



    //$('#btnApplySeparation').click(function () {
    //    var year = $("#allAcademicYears").val();
    //    if (year == "") {
    //        $(".ApplySeparation-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});



    //// Exam Management

    //$("#allAcademicYears").change(function (event) {

    //    var acid = $("#allAcademicYears").val();
    //    $.getJSON("/ExamManagement/getDate", { acid: acid },

    //        function (data) {

    //            var mdate = data.sd + "------" + data.ed;
    //            $(function () {
    //                // alert("s");
    //                var dates = $("#txt_ExamDate").datepicker({

    //                    dateFormat: 'dd/mm/yy',
    //                    minDate: data.sd,
    //                    maxDate: data.ed,
    //                    changeMonth: true,
    //                    changeYear: true
    //                });
    //            });
    //        });
    //});

    //$("#allAcademicYears").change(function (event) {
    //    var acid = $("#allAcademicYears").val();
    //    $.getJSON("/ExamManagement/getDate", { acid: acid },

    //        function (data) {

    //            var mdate = data.sd + "------" + data.ed;
    //            $(function () {
    //                var dates = $("#txt-ExamDate").datepicker({

    //                    dateFormat: 'dd/mm/yy',
    //                    minDate: data.sd,
    //                    maxDate: data.ed,
    //                    changeMonth: true,
    //                    changeYear: true
    //                });
    //            });
    //        });
    //});

    //$('#allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadClassDetailsEx();
    //});


    //function LoadClassDetailsEx() {
    //    var ClassId = $('#allClass').val();
    //    $.getJSON('/ExamManagement/GetSelectedClassSection', { passClassId: ClassId },
    //        function (orderdetails) {
    //            $("#Section").empty().append('<option value="">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#Section").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //            });
    //        });
    //}

    //$("#allAcademicYears").addClass("col-lg-12");
    //$("#allClass").addClass("col-lg-12");
    //$("#Section").addClass("col-lg-12");
    //$("#allExams").addClass("col-lg-12");
    //$("#allSubjects").addClass("col-lg-12");
    //$("#SectionStudent").addClass("col-lg-12");
    //$("#allFaculties").addClass("col-lg-12");

    //$("#btnExamAttendance").click(function (event) {
    //    event.preventDefault();
    //    $('#StudentList').empty();
    //    var yearId = $("#allAcademicYears").val();
    //    var classId = $("#allClass").val();
    //    var secId = $("#Section").val();
    //    var date = $("#txt-ExamDate").val();
    //    var exam = $("#allExams").val();
    //    var sub = $("#allSubjects").val();

    //    if (yearId == "" || classId == "" || secId == "" || date == "" || exam == "" || sub == "") {
    //        $(".ExamAttendance-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }

    //    $.post('/ExamManagement/ExamAttendance', { passYearId: yearId, passClassId: classId, passSecId: secId, passExamId: exam, passSubId: sub },

    //      function (data) {
    //          var recordExist = data.RecordExist;
    //          if (recordExist != null) {
    //              $("#e").text("Already attendance saved for the selected subject exam").css("color", "#ff0000");
    //          }
    //          $(".ExamAttendance-message").hide();
    //          $("#yearId").val(yearId);
    //          $("#classId").val(classId);
    //          $("#secId").val(secId);
    //          $("#date").val(date);
    //          $("#exam").val(exam);
    //          $("#sub").val(sub);


    //          if (recordExist == null) {
    //              $("#AttendanceList").show();
    //              $("#e").hide();
    //              $.each(data.studentList, function (i, v) {
    //                  var myCheckbox = "myCheckbox" + i;
    //                  var txt_reason = "txt_reason" + i;
    //                  var id = "ID" + i;
    //                  row = $('<tr><td><label name="' + id + '" id="' + id + '"  value="' + v.id + '">' + v.id + '</label></td><td><label id="Name">' + v.name + '</label> </td><td><input type="checkbox" class = "xxx" name="' + myCheckbox + '" /> </td><td><input type="textbox" name="' + txt_reason + '" /><input type="hidden" name="' + id + '" value="' + v.id + '"  /></td></tr>');
    //                  $('#EM_Att_StudentList').append(row);
    //                  $("#ivalue").val(i);
    //              });
    //          }

    //      }, 'json');
    //});



    //$("#EM_Att_allSelect").click(function (event) {
    //    if (this.checked) {
    //        $('.xxx').each(function () {
    //            this.checked = true;
    //        });
    //    }
    //    else {
    //        $('.xxx').each(function () {
    //            this.checked = false;
    //        });
    //    }
    //});


    //$(document).on('click', '.xxx', function () {
    //    if (this.checked) {
    //        $('.xxx').each(function () {
    //            if (this.checked) {
    //                $('#EM_Att_allSelect').prop("checked", true);
    //            }
    //        });
    //    }
    //});

    //$(document).on('click', '.xxx', function () {
    //    if ($("input[class='xxx']").length == $("input[class='xxx']:checked").length) {
    //        $("#EM_Att_allSelect").attr("checked", "checked");
    //    }
    //    else {
    //        $("#EM_Att_allSelect").removeAttr("checked");
    //    }
    //});



    //Exam Class Room

    //$("#btnExamClassRoom").click(function (event) {
    //    var yearId = $("#allAcademicYears").val();
    //    var classId = $("#allClass").val();
    //    var secId = $("#Section").val();
    //    var stuId = $("#allSubjects").val();
    //    var date = $("#txt_ExamDate").val();
    //    var exam = $("#allExams").val();
    //    var sub = $("#allSubjects").val();

    //    if (yearId == "" || classId == "" || secId == "" || stuId == "" || date == "" || exam == "" || sub == "") {
    //        $(".ExamClassRoom-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }

    //    $.getJSON('/ExamManagement/GetRollNumber', { passYearId: yearId, passClassId: classId, passSectionId: secId },
    //    function (data) {
    //        $("#start_RollNumber").empty().append('<option value="">Select RollNumber</option>');
    //        $.each(data, function (i, c) {
    //            $("#start_RollNumber").append($('<option></option>').val(c.RollNumberId).html(c.StudentRollNumber));
    //        });
    //    });

    //    $('#start_RollNumber').change(function (event) {
    //        event.preventDefault();
    //        LoadEndRollNumber();
    //    });


    //    function LoadEndRollNumber() {
    //        var startRollNumberId = $('#start_RollNumber').val();
    //        $.getJSON('/ExamManagement/GetEndRollNumber', { yId: yearId, cId: classId, sId: secId, passStartRollNumberId: startRollNumberId },
    //        function (data) {
    //            $("#end_RollNumber").empty().append('<option value="">Select RollNumber</option>');
    //            $.each(data, function (i, c) {
    //                $("#end_RollNumber").append($('<option></option>').val(c.RollNumberId).html(c.StudentRollNumber));
    //            });
    //        });
    //    }

    //    $(".ExamClassRoom-message").hide();
    //    $("#divAssignClassRoom").show();

    //});

    //$("#Edit_CR_end_RollNumber").addClass("col-lg-12");
    //$("#Edit_CR_end_RollNumber").addClass("col-lg-12");

    //$("#EM_allAcademicYears_List").addClass("col-lg-12");
    //$("#EM_allClass_List").addClass("col-lg-12");
    //$("#EM_Section_List").addClass("col-lg-12");
    //$("#EM_allExams_List").addClass("col-lg-12");
    //$("#EM_allSubjects_List").addClass("col-lg-12");


    //$('#btnEditExamClassRoom').click(function () {
    //    var room = $("#Edit_CR_txt_RoomNumber").val();

    //    if (room == "") {
    //        $(".EditExamClassRoom-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});

    //$("#Edit_CR_start_RollNumber").addClass("col-lg-12");

    //var yearId = $("#Edit_CR_Years").val();
    //var classId = $("#Edit_CR_Class").val();
    //var secId = $("#Edit_CR_Sec").val();
    //var startRegId = $("#Edit_CR_start_RollNumber").val();

    //$('#Edit_CR_start_RollNumber').change(function (event) {
    //    event.preventDefault();
    //    LoadEndRollNumber();
    //});


    //function LoadEndRollNumber() {
    //    var startRegId = $('#Edit_CR_start_RollNumber').val();
    //    $.getJSON('/ExamManagement/GetEndRollNumber', { yId: yearId, cId: classId, sId: secId, passStartRegId: startRegId },
    //    function (data) {
    //        $("#Edit_CR_end_RollNumber").empty().append('<option value="">Select RollNumber</option>');
    //        $.each(data, function (i, c) {
    //            $("#Edit_CR_end_RollNumber").append($('<option></option>').val(c.StudentRegId).html(c.StudentRollNumber));
    //        });
    //    });
    //}

    //$('#Section').change(function (event) {
    //    event.preventDefault();
    //    LoadSubject_EM();
    //});


    //function LoadSubject_EM() {
    //    var yearId = $('#allAcademicYears').val();
    //    var classId = $('#allClass').val();
    //    var secId = $('#Section').val();
    //    $.getJSON('/ExamManagement/GetSectionSubjects_EM', { passYearId: yearId, passClassId: classId, passSecId: secId },
    //    function (data) {
    //        $("#allSubjects").empty().append('<option value="">Select Subject</option>');
    //        $.each(data, function (i, c) {
    //            $("#allSubjects").append($('<option></option>').val(c.SubjectId).html(c.SubjectName));
    //        });
    //    });
    //}
    //$('#EM_Section_List').change(function (event) {
    //    event.preventDefault();
    //    LoadSubject_EM_List();
    //});


    //function LoadSubject_EM_List() {
    //    var yearId = $('#EM_allAcademicYears_List').val();
    //    var classId = $('#EM_allClass_List').val();
    //    var secId = $('#EM_Section_List').val();
    //    $.getJSON('/ExamManagement/GetSectionSubjects_EM', { passYearId: yearId, passClassId: classId, passSecId: secId },
    //    function (data) {
    //        $("#EM_allSubjects_List").empty().append('<option value="">Select Subject</option>');
    //        $.each(data, function (i, c) {
    //            $("#EM_allSubjects_List").append($('<option></option>').val(c.SubjectId).html(c.SubjectName));
    //        });
    //    });
    //}


    //$(".DelClassRoom").click(function (event) {
    //    var r = confirm("Are you sure want to delete?");
    //    if (r == true) {
    //        return true;
    //    } else {
    //        return false;
    //    }
    //});










    //$('#EM_allClass_List').change(function (event) {
    //    event.preventDefault();
    //    Load_EM_ClassRoomList();
    //});


    //function Load_EM_ClassRoomList() {
    //    var ClassId = $('#EM_allClass_List').val();
    //    $.getJSON('/ExamManagement/GetSelectedClassSection', { passClassId: ClassId },
    //        function (orderdetails) {
    //            $("#EM_Section_List").empty().append('<option value="">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#EM_Section_List").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //            });
    //        });
    //}


    //$("#btnExamClassRoomList").click(function (event) {
    //    var yearId = $("#EM_allAcademicYears_List").val();
    //    var classId = $("#EM_allClass_List").val();
    //    var secId = $("#EM_Section_List").val();
    //    var examId = $("#EM_allExams_List").val();
    //    var subId = $("#EM_allSubjects_List").val();


    //    if (yearId == "" || classId == "" || secId == "" || examId == "" || subId == "") {
    //        $(".ListExamClassRoom-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }

    //});




    //var counter = 1;

    //$("#addRoom").click(function (event) {

    //    var secId = $("#Section").val();

    //    function getDropDownList(name, id, optionList) {
    //        var combo = $("<select></select>").attr("id", id).attr("name", name);

    //        $.each(optionList, function (i, el) {
    //            combo.append("<option>" + el + "</option>");
    //        });

    //        return combo;
    //        // OR
    //        $("#SELECTOR").append(combo);
    //    }


    //    $.getJSON('/ExamManagement/GetRollNumber', { passSectionId: secId },
    //      function (data) {
    //          event.preventDefault();
    //          counter++;
    //      });
    //});


    // Exam Remarks

    //$('#Section').change(function (event) {
    //    event.preventDefault();
    //    Load_EM_Remark_StudentDetails();

    //});
    //function Load_EM_Remark_StudentDetails() {
    //    var yearId = $("#allAcademicYears").val();
    //    var classId = $("#allClass").val();
    //    var SectionId = $('#Section').val();

    //    $.getJSON('/ExamManagement/GetSectionStudents_EM', { passYearId: yearId, passClassId: classId, passSectionId: SectionId },

    //        function (orderdetails) {
    //            $("#SectionStudent").empty().append('<option value="">Select Student</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#SectionStudent").append($('<option></option>').val(c.StudentRegId).html(c.StudentName));
    //            });
    //        });
    //}


    //$("#btnExamRemarks").click(function (event) {

    //    var yearId = $("#allAcademicYears").val();
    //    var classId = $("#allClass").val();
    //    var secId = $("#Section").val();
    //    var stuId = $("#SectionStudent").val();
    //    var date = $("#txt-ExamDate").val();
    //    var exam = $("#allExams").val();
    //    var sub = $("#allSubjects").val();

    //    if (yearId == "" || classId == "" || secId == "" || stuId == "" || date == "" || exam == "" || sub == "") {
    //        $(".ExamRemarks-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }

    //    $("#yearId").val(yearId);
    //    $("#classId").val(classId);
    //    $("#secId").val(secId);
    //    $("#stuId").val(stuId);
    //    $("#date").val(date);
    //    $("#exam").val(exam);
    //    $("#sub").val(sub);

    //    $(".ExamRemarks-message").hide();
    //    $("#divGetRemark").show();
    //});

    //$("#btnSaveExamRemarks").click(function (event) {
    //    var remark = $("#txt-RemarkId").val();
    //    if (remark == "") {
    //        $(".Remarks-message").text("Please enter Remark").css("color", "#ff0000");
    //    }
    //});




    // Class Management
    //$("#btnClassList").click(function (event) {
    //    var yearId = $("#CM_allAcademicYears_List").val();
    //    if (yearId == "") {
    //        $(".ClassList-message").text("Please select Academic year").css("color", "#ff0000");
    //        return false;
    //    }
    //    $("#divClassList").show();
    //});

    //$("#CM_allAcademicYears_List").addClass("col-lg-12");

    //$("#btnClassTeacher_Edit").click(function (event) {
    //    var facultyId = $("#ACT_Edit_allFaculties1").val();
    //    if (facultyId == "") {
    //        $(".ClassList_Edit-message").text("Please select Faculty").css("color", "#ff0000");
    //        return false;
    //    }
    //    $("#divClassList").show();
    //});


    //$("#btnSubjectTeacherList").click(function (event) {
    //    var yearId = $("#allAcademicYears_List").val();
    //    var classId = $("#allClass_List").val();
    //    var secId = $("#allSection_List").val();
    //    if (yearId == "" || classId == "" || secId == "") {
    //        $(".SubjectTeacherList-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});

    //$("#allAcademicYears_List").addClass("col-lg-12");
    //$("#allClass_List").addClass("col-lg-12");
    //$("#allSection_List").addClass("col-lg-12");

  

    //$("#btnClassList_Edit").click(function (event) {
    //    var facultyId = $("#allFaculties1").val();
    //    if (facultyId == "") {
    //        $(".ClassList_Edit-message").text("Please select Faculty").css("color", "#ff0000");
    //        return false;
    //    }
    //    $("#divClassList").show();
    //});


    //$("#btnSubjectTeacherList").click(function (event) {
    //    var yearId = $("#allAcademicYears").val();
    //    var classId = $("#allClass").val();
    //    var secId = $("#Section").val();
    //    if (yearId == "" || classId == "" || secId == "0") {
    //        $(".SubjectTeacherList-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});


    //$("#allAcademicYears1").addClass("col-lg-12");
    //$("#allClass1").addClass("col-lg-12");
    //$("#Section1").addClass("col-lg-12");
    //$("#allFaculties1").addClass("col-lg-12");

    

    //$("#AssignFacultyToSubject_allClass").addClass("col-lg-12");
    //$("#AssignFacultyToSubject_Section").addClass("col-lg-12");
    //$("#AssignFacultyToSubjects").addClass("col-lg-12");



    //$("#btnAssignFacultyToSubject").click(function (event) {
    //    var yearId = $("#allAcademicYears").val();
    //    var classId = $("#AssignFacultyToSubject_allClass").val();
    //    var secId = $("#AssignFacultyToSubject_Section").val();
    //    var subId = $("#AssignFacultyToSubjects").val();
    //    var facultyId = $("#allFaculties").val();
    //    if (yearId == "" || classId == "" || secId == "" || subId == "" || facultyId == "") {
    //        $(".AssignFacultyToSubject-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});

    //$('#allClass1').change(function (event) {
    //    event.preventDefault();
    //    LoadClassDetails1();
    //});


    //function LoadClassDetails1() {
    //    var ClassId = $('#allClass1').val();
    //    $.getJSON('/ClassManagement/GetSelectedClassSection', { passClassId: ClassId },
    //        function (orderdetails) {
    //            $("#Section1").empty().append('<option value="0">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#Section1").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //            });
    //        });
    //}

    //$("#EditCI_allClass").addClass("col-lg-12");
    //$("#EditCI_Section").addClass("col-lg-12");

    //$('#EditCI_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadClassDetails_EditCI();
    //});


    //function LoadClassDetails_EditCI() {
    //    var ClassId = $('#EditCI_allClass').val();
    //    $.getJSON('/ClassManagement/GetSelectedClassSection', { passClassId: ClassId },
    //        function (orderdetails) {
    //            $("#EditCI_Section").empty().append('<option value="0">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#EditCI_Section").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //            });
    //        });
    //}

    //Timetable Management

    //$('#allClass').change(function (event) {
    //    event.preventDefault();
    //    Load_TM_ClassDetails();
    //});


    //function Load_TM_ClassDetails() {
    //    var ClassId = $('#allClass').val();
    //    $.getJSON('/TimeTableManagement/GetSelectedClassSection', { passClassId: ClassId },
    //        function (orderdetails) {
    //            $("#Section").empty().append('<option value="0">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#Section").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //            });
    //        });
    //}

    //$('#TM_AssignSubToPeriod_allClass').change(function (event) {
    //    event.preventDefault();
    //    Load_TMAssignSubToPeriod_ClassDetails();
    //});


    //function Load_TMAssignSubToPeriod_ClassDetails() {
    //    var ClassId = $('#TM_AssignSubToPeriod_allClass').val();
    //    $.getJSON('/TimeTableManagement/GetSelectedClassSection', { passClassId: ClassId },
    //        function (orderdetails) {
    //            $("#TM_AssignSubToPeriod_Section").empty().append('<option value="0">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#TM_AssignSubToPeriod_Section").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //            });
    //        });
    //}
    //var count = $("#periodCount").val();
    //for (var i = 0; i < count; i++) {
    //    $("#TM_AssignSubToPeriod_Subject" + i).addClass("SubPeriod");
    //}





    //$('#TM_AssignSubToPeriod_Section').change(function (event) {
    //    event.preventDefault();
    //    TM_LoadSubjectDetails();
    //});


    //function TM_LoadSubjectDetails() {
    //    var YearId = $('#TM_AssignSubToPeriod_allAcademicYears').val();
    //    var ClassId = $('#TM_AssignSubToPeriod_allClass').val();
    //    var secId = $('#TM_AssignSubToPeriod_Section').val();
    //    $.getJSON('/TimeTableManagement/GetSelectedClassSectionSubject', { passYearId: YearId, passClassId: ClassId, passSecId: secId },
    //        function (orderdetails) {
    //            $(".SubPeriod").empty().append('<option value="">Select Subject</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $(".SubPeriod").append($('<option></option>').val(c.SubjectId).html(c.SubjectName));
    //            });
    //        });
    //}


    //$("#dayorder").addClass("col-lg-12");
    //// class Timetable

    //$("#btnClassTimetable").click(function (event) {
    //    var yearId = $("#allAcademicYears").val();
    //    var classId = $("#allClass").val();
    //    var secId = $("#Section").val();


    //    if (yearId == "" || classId == "" || secId == "0") {
    //        $(".ClassTimetable-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});


    //$("#btnAssignSubjectToPeriod").click(function (event) {
    //    var yearId = $("#TM_AssignSubToPeriod_allAcademicYears").val();
    //    var classId = $("#TM_AssignSubToPeriod_allClass").val();
    //    var secId = $("#TM_AssignSubToPeriod_Section").val();
    //    var dayOrder = $("#dayorder").val();
    //    var count = $("#periodCount").val();

    //    if (yearId == "" || classId == "" || secId == "0" || dayOrder == "") {
    //        $(".AssignSubjectToPeriod-message").text("Please select all the mantratory fields1").css("color", "#ff0000");
    //        return false;
    //    }




    //    var sub = "Subject";
    //    for (var i = 0; i < count; i++) {
    //        var ss = $("#TM_AssignSubToPeriod_Subject" + i).val();
    //        if (ss == "0" || ss == null || ss == "") {
    //            $(".AssignSubjectToPeriod-message").text("Please select all the mantratory fields2").css("color", "#ff0000");
    //            return false;
    //        }
    //    }
    //});

    //$("#TM_AssignSubToPeriod_allAcademicYears").addClass("col-lg-12");
    //$("#TM_AssignSubToPeriod_allClass").addClass("col-lg-12");
    //$("#TM_AssignSubToPeriod_Section").addClass("col-lg-12");



    




    // Exam Timetable

    $("#txt_StartTime").timepicker({ timeFormat: 'HH:mm' });
    $("#txt_EndTime").timepicker({ timeFormat: 'HH:mm' });

    $("#txt_schoolST").timepicker({ timeFormat: 'HH:mm' });
    $("#txt_schoolET").timepicker({ timeFormat: 'HH:mm' });

    //$("#btnExamTimetable").click(function (event) {
    //    var yearId = $("#allAcademicYears").val();
    //    var classId = $("#allClass").val();
    //    var secId = $("#Section").val();
    //    var examId = $("#allExams").val();
    //    if (yearId == "" || classId == "" || secId == "0" || secId == "" || examId == "") {
    //        $(".ExamTimetable-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});

    //$('#Section').change(function (event) {
    //    event.preventDefault();
    //    LoadSubject_TM();
    //});


    //function LoadSubject_TM() {
    //    var YearId = $('#allAcademicYears').val();
    //    var ClassId = $('#allClass').val();
    //    var SecId = $('#Section').val();
    //    $.getJSON('/TimeTableManagement/GetSelectedClassSectionSubject', { passYearId: YearId, passClassId: ClassId, passSecId: SecId },
    //        function (orderdetails) {
    //            $("#allSubjects").empty().append('<option value="">Select Subject</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#allSubjects").append($('<option></option>').val(c.SubjectId).html(c.SubjectName));
    //            });
    //        });
    //}



    //$("#allAcademicYears").change(function (event) {

    //    var acid = $("#allAcademicYears").val();
    //    $.getJSON("/TimeTableManagement/getDate", { acid: acid },

    //        function (data) {

    //            var mdate = data.sd + "------" + data.ed;
    //            $(function () {
    //                var dates = $("#txt_ExamDate").datepicker({

    //                    dateFormat: 'dd/mm/yy',
    //                    minDate: data.sd,
    //                    maxDate: data.ed,
    //                    changeMonth: true,
    //                    changeYear: true
    //                });
    //            });
    //        });
    //});


    //$("#btnCreateExamTimetable").click(function (event) {
    //    var yearId = $("#allAcademicYears").val();
    //    var classId = $("#allClass").val();
    //    var secId = $("#Section").val();
    //    var examId = $("#allExams").val();
    //    var stime = $("#txt_StartTime").val();
    //    var etime = $("#txt_EndTime").val();
    //    var date = $("#txt_ExamDate").val();
    //    var subj = $("#allSubjects").val();

    //    if (yearId == "" || classId == "" || secId == "0" || secId == "" || examId == "" || stime == "" || etime == "" || date == "" || subj == "") {
    //        $(".CreateExamTimetable-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }

    //    else if (yearId != "" || classId != "" || secId != "0" || secId != "" || examId != "" || stime != "" || etime != "" || date != "" || subj != "") {
    //        var stime1 = $("#txt_StartTime").timepicker().val();
    //        var etime1 = $("#txt_EndTime").timepicker().val();
    //        if (stime1 >= etime1) {
    //            $(".CreateExamTimetable-message").hide();
    //            $(".TimeCodndiv").text("End Time should be greater than Start Time").css("color", "#ff0000");
    //            return false;
    //        }
    //    }
    //    else {
    //        return true;
    //    }

    //});




    // Attendance Management

    $("#Att_allAcademicYears").addClass("col-lg-12");

    $("#Att_SectionStudent").addClass("col-lg-12");


    $("#txt-AttendaceDate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
    });

    //$("#btnStudentAttendance").click(function (event) {
    //    event.preventDefault();
    //    $('#Att_StudentList').empty();
    //    var yearId = $("#Att_allAcademicYears").val();
    //    var classId = $("#Att_allClass").val();
    //    var secId = $("#Att_Section").val();
    //    var date = $("#txt-AttendaceDate").val();

    //    if (yearId == "" || classId == "" || secId == "" || date == "") {
    //        $(".StudentAttendance-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //    $("#Stu_AttendanceList").show();
    //    $.post('/AttendanceManagement/Studentattendance', { passYearId: yearId, passClassId: classId, passSecId: secId },

    //      function (data) {
    //          $(".StudentAttendance-message").hide();
    //          $.each(data, function (i, v) {
    //              var myCheckbox = "myCheckbox" + i;
    //              var txt_reason = "txt_reason" + i;
    //              var id = "ID" + i;
    //              row = $('<tr><td><label name="' + id + '" id="' + id + '"  value="' + v.id + '">' + v.id + '</label></td><td><label id="Name">' + v.name + '</label> </td><td><input type="checkbox"  name="' + myCheckbox + '"  /> </td><td><input type="textbox" name="' + txt_reason + '" /></td><td><input type="hidden" name="' + id + '" value="' + v.id + '"  /></td></tr>');
    //              $('#Att_StudentList').append(row);
    //              $("#ivalue").val(i);
    //          });
    //      }, 'json');
    //});



   //  faculty attendace
    $("#Att_allAcademicYears").change(function (event) {
        $("#txt_AttendanceDate").datepicker("destroy");
        var acid = $("#Att_allAcademicYears").val();

        var HolidayDate1 = [];
        var HolidayReason1 = [];
        $.getJSON("/Dashboard/CustomeActiveDate", { acid: acid },
            function (data) {
                $.each(data.HolidaysList, function (i, c) {
                    HolidayDate1[i] = c.Date;
                    HolidayReason1[i] = c.Holiday;
                });
                var holidays = HolidayDate1;      //["8/04/2015", "08/20/2015"];

                $(function () {
                    var dates = $("#txt_AttendanceDate").datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: data.sd,
                        maxDate: data.ed,
                        changeMonth: true,
                        changeYear: true,
                        beforeShowDay: function (date) {
                            show = true;
                            for (var i = 0; i < holidays.length; i++) {
                                if (new Date(holidays[i]).toString() == date.toString()) { return [false, 'holiday green', HolidayReason1[i]]; }//No Holidays
                            }
                            if (date.getDay() == 0 || date.getDay() == 6) { return [false, 'holiday red', '']; }//No Weekends
                            var display = [show, '', (show) ? '' : 'Holidays'];//With Fancy hover tooltip!
                            return display;
                        },
                        onSelect: function (selectedDate) {
                            var option = this.id == "txt_AttendanceDate" ? "minDate" : "maxDate",
                            instance = $(this).data("datepicker"),
                            date = $.datepicker.parseDate(
                            instance.settings.dateFormat ||
                            $.datepicker._defaults.dateFormat,
                            selectedDate, instance.settings);
                            dates.not(this).datepicker("option", option, date);
                        },

                    });
                });
            });
    });


    $("#present").click(function (event) {
        if (this.checked) {
            $('.att').val(this.value);
        }
        else {
            $.getJSON('/Dashboard/GetAttendanceStatusList',
                            function (orderdetails) {
                                $(".att").empty().append('<option value="0">Select Status</option>');
                                $.each(orderdetails, function (i, c) {
                                    $(".att").append($('<option></option>').val(c.StatusValue).html(c.StatusName));
                                });
                            });
        }
    });

    $(document).on('change', '.att', function () {

        var n = $(".att").length;

        var j = 0;

        for (var i = 0; i < n; i++) {
            var selVal = $("#atn" + i).val();
            var defVal = "P";

            if (selVal == defVal) {
                j++;
              //  alert("equal");
            }
            else {
              //  alert("un equal");
            }
        }

        if (j == n) {
         //   alert("good");
            $('#present').prop("checked", true);
        }
        else {
          //  alert("bad");
            $("#present").removeAttr("checked");
        }

    });




    // Employee Attendance

 
    //$("#allEmployees").addClass("col-lg-12");

    //$('#Att_allClass1').change(function (event) {
    //    event.preventDefault();
    //    $("#Att_Section1").empty().append('<option value="0">Select Section</option>');
    //    $("#Att_SectionStudent1").empty().append('<option value="">Select Student</option>');
    //    LoadClassDetailsA1();
    //});

    //function LoadClassDetailsA1() {
    //    var ClassId = $('#Att_allClass1').val();
    //    $.getJSON('/AttendanceManagement/GetSelectedClassSection', { passClassId: ClassId },
    //        function (orderdetails) {
    //            $("#Att_Section1").empty().append('<option value="0">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#Att_Section1").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //            });
    //        });
    //}



    //$('#Att_Section1').change(function (event) {
    //    event.preventDefault();
    //    LoadStudentDetailsAS();
    //});

    //function LoadStudentDetailsAS() {
    //    var yearId = $("#Att_allAcademicYears1").val();
    //    var classId = $("#Att_allClass1").val();
    //    var SectionId = $('#Att_Section1').val();
    //    $.getJSON('/AttendanceManagement/GetSectionStudents', {passYearId:yearId, passClassId:classId, passSectionId: SectionId },

    //        function (orderdetails) {
    //            $("#Att_SectionStudent1").empty().append('<option value="">Select Student</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#Att_SectionStudent1").append($('<option></option>').val(c.id).html(c.name));
    //            });
    //        });
    //}



    //// Achievement 

    $("#Ach_allAcademicYears").addClass("col-lg-12");
    $("#Ach_allClass").addClass("col-lg-12");
    $("#Ach_Section").addClass("col-lg-12");
    $("#Ach_SectionStudent").addClass("col-lg-12");
    $("#txt_doa").addClass("col-lg-12");
    $("#txt_achievement").addClass("col-lg-12");
    $("#txt_description").addClass("col-lg-12");

    //$('#Ach_allClass').change(function (event) {
    //    $("#Ach_SectionStudent").val("");
    //    $("#Ach_Section").empty().append('<option value="">-- Select Section --</option>');
    //    $("#Ach_SectionStudent").empty().append('<option value="">-- Select Student --</option>');
    //    event.preventDefault();
    //    LoadClassDetailsAch();
    //});

    //function LoadClassDetailsAch() {
    //    var ClassId = $('#Ach_allClass').val();
    //    $.getJSON('/Achievement/GetSelectedClassSection', { passClassId: ClassId },
    //        function (orderdetails) {
    //            $("#Ach_Section").empty().append('<option value="">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#Ach_Section").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //            });
    //        });
    //}

    //$('#Ach_Section').change(function (event) {
    //    event.preventDefault();
    //    $("#Ach_SectionStudent").empty().append('<option value="">-- Select Student --</option>');
    //    LoadStudentDetailsACS();
    //});

    //function LoadStudentDetailsACS() {
    //    var SectionId = $('#Ach_Section').val();
    //    var ClassId = $('#Ach_allClass').val();
    //    $.getJSON('/Achievement/GetSectionStudents', { passClassId: ClassId, passSectionId: SectionId },

    //        function (orderdetails) {
    //            $("#Ach_SectionStudent").empty().append('<option value="">Select Student</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#Ach_SectionStudent").append($('<option></option>').val(c.StudentRegId).html(c.StudentName));
    //            });
    //        });
    //}










    // Faculty Info

    $("#Fac_allAcademicYears").addClass("col-lg-12");



    // class incharge
    $("#CI_allAcademicYears").addClass("col-lg-12");

    //$("#btnClassAttendance").click(function (event) {
    //    event.preventDefault();
    //    $('#CI_Att_StudentList').empty();
    //    var yearId = $("#CI_allAcademicYears").val();
    //    var date = $("#txt_AttendaceDate").val();

    //    if (yearId == "" || date == "") {
    //        $(".ClassAttendance-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //    $("#Stu_AttendanceList").show();
    //    $.post('/ClassIncharge/ClassAttendance', { passYearId: yearId },

    //      function (data) {
    //          $(".ClassAttendance-message").hide();
    //          $.each(data, function (i, v) {
    //              var myCheckbox = "myCheckbox" + i;
    //              var txt_reason = "txt_reason" + i;
    //              var id = "ID" + i;
    //              var clas = "CI_Checkbox1_ClassAttendance";
    //              row = $('<tr><td><label name="' + id + '" id="' + id + '"  value="' + v.id + '">' + v.id + '</label></td><td><label id="Name">' + v.name + '</label> </td><td><input type="checkbox"  id="' + myCheckbox + '"  class = "x" name="' + myCheckbox + '" /> </td><td><input type="textbox" name="' + txt_reason + '" /><input type="hidden" name="' + id + '" value="' + v.id + '"  /></td></tr>');
    //              $('#CI_Att_StudentList').append(row);
    //              $("#ivalue").val(i);
    //          });
    //      }, 'json');
    //});


    //$("#CI_allStudents").addClass("col-lg-12");

    //$('#CI_allAcademicYears').change(function (event) {
    //    event.preventDefault();
    //    LoadStudentDetailsCIReport();
    //});

    //function LoadStudentDetailsCIReport() {
    //    var yearId = $('#CI_allAcademicYears').val();
    //    $.getJSON('/ClassIncharge/GetStudents', { passYearId: yearId },

    //        function (orderdetails) {
    //            $("#CI_allStudents").empty().append('<option value="">Select Student</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#CI_allStudents").append($('<option></option>').val(c.StudentRegId).html(c.StudentName));
    //            });
    //        });
    //}


    //class incharge - Progress card


    $("#PC_allAcademicYears").addClass("col-lg-12");
    $("#PC_allStudents").addClass("col-lg-12");

    
    //$('#PC_allAcademicYears').change(function (event) {
    //    event.preventDefault();
    //    LoadStudentDetailsPC_Report();
    //});

    //function LoadStudentDetailsPC_Report() {
    //    var yearId = $('#PC_allAcademicYears').val();
    //    $.getJSON('/ClassIncharge/GetStudents', { passYearId: yearId },

    //        function (orderdetails) {
    //            $("#PC_allStudents").empty().append('<option value="">Select Student</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#PC_allStudents").append($('<option></option>').val(c.StudentRegId).html(c.StudentName));
    //            });
    //        });
    //}




    // modules info

    $("#MI_allAcademicYears").addClass("col-lg-12");
    $("#MI_allClass").addClass("col-lg-12");
    $("#MI_Section").addClass("col-lg-12");
    $("#MI_allSubjects").addClass("col-lg-12");
    $("#CI_NotesFile").addClass("col-lg-12");

    
    

    
    //$('#MI_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadSection_MI();
    //});

    //function LoadSection_MI() {
    //    var classId = $('#MI_allClass').val();
    //    $.getJSON('/ModulesInfo/showSection', { passClassId: classId },

    //        function (orderdetails) {
    //            $("#MI_Section").empty().append('<option value="">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_Section").append($('<option></option>').val(c.SecId).html(c.SecName));
    //            });
    //        });
    //}


    //$('#MI_Section').change(function (event) {
    //    event.preventDefault();
    //    LoadSubject_MI();
    //});

    //function LoadSubject_MI() {
    //    var secId = $('#MI_Section').val();
    //    $.getJSON('/ModulesInfo/showSubjects', { passSecId: secId },

    //        function (orderdetails) {
    //            $("#MI_allSubjects").empty().append('<option value="0">Select Subject</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_allSubjects").append($('<option></option>').val(c.SubId).html(c.SubName));
    //            });
    //        });
    //}


    $("#MI_add_allAcademicYears").addClass("col-lg-12");
    //$("#btnAddSubjectNotes").click(function (event) {
    //    var yearId = $("#MI_add_allAcademicYears").val();
    //    var classId = $("#MI_allClass").val();
    //    var secId = $("#MI_Section").val();
    //    var subId = $("#MI_allSubjects").val();
    //    var date = $("#txt-DateOfNotes").datepicker().val();
    //    var topic = $("#txt-Topic").val();
    //    var desc = $("#txt-description").val();

    //    if (yearId == "" || classId == "" || secId == "" || subId == "" || date == "" || topic == "" || desc == "") {
    //        $(".AddSubjectNotes-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});



    //$('#MI_NotesList_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadSection_MI_NotesList();
    //});

    //function LoadSection_MI_NotesList() {
    //    var classId = $('#MI_NotesList_allClass').val();
    //    $.getJSON('/ModulesInfo/showSection', { passClassId: classId },

    //        function (orderdetails) {
    //            $("#MI_NotesList_Section").empty().append('<option value="">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_NotesList_Section").append($('<option></option>').val(c.SecId).html(c.SecName));
    //            });
    //        });
    //}


    //$('#MI_NotesList_Section').change(function (event) {
    //    event.preventDefault();
    //    LoadSubject_MI_NotesList();
    //});

    //function LoadSubject_MI_NotesList() {
    //    var secId = $('#MI_NotesList_Section').val();
    //    $.getJSON('/ModulesInfo/showSubjects', { passSecId: secId },

    //        function (orderdetails) {
    //            $("#MI_NotesList_allSubjects").empty().append('<option value="">Select Subject</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_NotesList_allSubjects").append($('<option></option>').val(c.SubId).html(c.SubName));
    //            });
    //        });
    //}


    //$("#txt-EDateOfNotes").datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    dateFormat: 'dd/mm/yy',
    //});

    $("#MI_NotesList_allClass").addClass("col-lg-12");
    $("#MI_NotesList_Section").addClass("col-lg-12");
    $("#MI_NotesList_allSubjects").addClass("col-lg-12");


    //$("#btnListSubjectNotes").click(function (event) {
    //    var yearId = $("#MI_allAcademicYears").val();
    //    var classId = $("#MI_NotesList_allClass").val();
    //    var secId = $("#MI_NotesList_Section").val();
    //    var subId = $("#MI_NotesList_allSubjects").val();

    //    if (yearId == "" || classId == "" || secId == "" || subId == "") {
    //        $(".ListSubjectNotes-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});


    //$(".DelSubjectNotes").click(function (event) {
    //    var r = confirm("Are you sure want to delete?");
    //    if (r == true) {
    //        return true;
    //    } else {
    //        return false;
    //    }
    //});



    //$('#MI_HW_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadSection_MI_HW();
    //});

    //function LoadSection_MI_HW() {
    //    var classId = $('#MI_HW_allClass').val();
    //    $.getJSON('/ModulesInfo/showSection', { passClassId: classId },

    //        function (orderdetails) {
    //            $("#MI_HW_Section1").empty().append('<option value="">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_HW_Section1").append($('<option></option>').val(c.SecId).html(c.SecName));
    //            });
    //        });
    //}


    //$('#MI_HW_Section1').change(function (event) {
    //    event.preventDefault();
    //    LoadSubject_MI_HW1();
    //});

    //function LoadSubject_MI_HW1() {
    //    var secId = $('#MI_HW_Section1').val();
    //    $.getJSON('/ModulesInfo/showSubjects', { passSecId: secId },

    //        function (orderdetails) {
    //            $("#MI_HW_allSubjects1").empty().append('<option value="">Select Subject</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_HW_allSubjects1").append($('<option></option>').val(c.SubId).html(c.SubName));
    //            });
    //        });
    //}


    //$('#MI_HW_List_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadSection_MI_HW_List();
    //});

    //function LoadSection_MI_HW_List() {
    //    var classId = $('#MI_HW_List_allClass').val();
    //    $.getJSON('/ModulesInfo/showSection', { passClassId: classId },

    //        function (orderdetails) {
    //            $("#MI_HW_List_Section").empty().append('<option value="">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_HW_List_Section").append($('<option></option>').val(c.SecId).html(c.SecName));
    //            });
    //        });
    //}


    //$('#MI_HW_List_Section').change(function (event) {
    //    event.preventDefault();
    //    LoadSubject_MI_HW();
    //});

    //function LoadSubject_MI_HW() {
    //    var secId = $('#MI_HW_List_Section').val();
    //    $.getJSON('/ModulesInfo/showSubjects', { passSecId: secId },

    //        function (orderdetails) {
    //            $("#MI_HW_List_allSubjects").empty().append('<option value="">Select Subject</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_HW_List_allSubjects").append($('<option></option>').val(c.SubId).html(c.SubName));
    //            });
    //        });
    //}

    //$("#MI_HW_List_allAcademicYears").addClass("col-lg-12");
    //$("#MI_HW_List_allClass").addClass("col-lg-12");
    //$("#MI_HW_List_Section").addClass("col-lg-12");
    //$("#MI_HW_List_allSubjects").addClass("col-lg-12");

    //$("#btnListHomework").click(function (event) {
    //    var yearId = $("#MI_HW_List_allAcademicYears").val();
    //    var classId = $("#MI_HW_List_allClass").val();
    //    var secId = $("#MI_HW_List_Section").val();
    //    var subId = $("#MI_HW_List_allSubjects").val();
    //    var fromDate = $("#MI_HW_List_txt_fromDate").val();
    //    var toDate = $("#MI_HW_List_txt_toDate").val();

    //    if (yearId == "" || classId == "" || secId == "" || subId == "" || fromDate == "" || toDate == "") {
    //        $(".ListHomework-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});



    //$(".DelHomework").click(function (event) {
    //    var r = confirm("Are you sure want to delete?");
    //    if (r == true) {
    //        return true;
    //    } else {
    //        return false;
    //    }
    //});


    //$("#MI_HW_allAcademicYears").addClass("col-lg-12");
    //$("#MI_HW_allClass").addClass("col-lg-12");
    //$("#MI_HW_Section1").addClass("col-lg-12");
    //$("#MI_HW_allSubjects1").addClass("col-lg-12");


    //$("#btnAssignmentList").click(function (event) {
    //    var yearId = $("#MI_Assign_List_allAcademicYears").val();
    //    var classId = $("#MI_Assign_List_allClass").val();
    //    var secId = $("#MI_Assign_List_Section").val();
    //    var subId = $("#MI_Assign_List_allSubjects").val();
    //    var fromDate = $("MI_Assign_List_txt-fromDate").val();
    //    var toDate = $("#MI_Assign_List_txt-toDate").val();

    //    if (yearId == "" || classId == "" || secId == "" || subId == "" || fromDate == "" || toDate == "") {
    //        $(".AssignmentList-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});


    //$(function () {
    //    var dates = $("#MI_Assign_List_txt-fromDate, #MI_Assign_List_txt-toDate").datepicker({
    //        dateFormat: 'yy-mm-dd',
    //        changeMonth: true,
    //        changeYear: true,
    //        onSelect: function (selectedDate) {
    //            var option = this.id == "MI_Assign_List_txt-fromDate" ? "minDate" : "maxDate",
    //            instance = $(this).data("datepicker"),
    //            date = $.datepicker.parseDate(
    //            instance.settings.dateFormat ||
    //            $.datepicker._defaults.dateFormat,
    //            selectedDate, instance.settings);
    //            dates.not(this).datepicker("option", option, date);
    //        }
    //    });
    //});


    //$("#MI_Assign_List_allAcademicYears").addClass("col-lg-12");
    //$("#MI_Assign_List_allClass").addClass("col-lg-12");
    //$("#MI_Assign_List_Section").addClass("col-lg-12");
    //$("#MI_Assign_List_allSubjects").addClass("col-lg-12");




    //$('#MI_Assign_List_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadSection_MI_Assign_List();
    //});

    //function LoadSection_MI_Assign_List() {
    //    var classId = $('#MI_Assign_List_allClass').val();
    //    $.getJSON('/ModulesInfo/showSection', { passClassId: classId },

    //        function (orderdetails) {
    //            $("#MI_Assign_List_Section").empty().append('<option value="0">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_Assign_List_Section").append($('<option></option>').val(c.SecId).html(c.SecName));
    //            });
    //        });
    //}

    //$('#MI_Assign_List_Section').change(function (event) {
    //    event.preventDefault();
    //    LoadSubject_MI_Assign_List();
    //});

    //function LoadSubject_MI_Assign_List() {
    //    var secId = $('#MI_Assign_List_Section').val();
    //    $.getJSON('/ModulesInfo/showSubjects', { passSecId: secId },

    //        function (orderdetails) {
    //            $("#MI_Assign_List_allSubjects").empty().append('<option value="0">Select Subject</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_Assign_List_allSubjects").append($('<option></option>').val(c.SubId).html(c.SubName));
    //            });
    //        });
    //}



    //$("#btnAddAssignment").click(function (event) {
    //    var yearId = $("#MI_Assign_allAcademicYears").val();
    //    var classId = $("#MI_Assign_allClass").val();
    //    var secId = $("#MI_Assign_Section1").val();
    //    var subId = $("#MI_Assign_allSubjects1").val();
    //    var assignment = $("#MI_Assign_txt-Assignment").val();
    //    var desc = $("MI_Assign_txt-description").val();
    //    var subDate = $("#MI_Assign_txt-DateToComplete").val();

    //    if (yearId == "" || classId == "" || secId == "" || subId == "" || assignment == "" || desc == "" || subDate == "") {
    //        $(".AddAssignment-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});

    //$("#MI_Assign_allAcademicYears").addClass("col-lg-12");
    //$("#MI_Assign_allClass").addClass("col-lg-12");
    //$("#MI_Assign_Section1").addClass("col-lg-12");
    //$("#MI_Assign_allSubjects1").addClass("col-lg-12");

    //$('#MI_Assign_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadSection_MI_Assign();
    //});

    //function LoadSection_MI_Assign() {
    //    var classId = $('#MI_Assign_allClass').val();
    //    $.getJSON('/ModulesInfo/showSection', { passClassId: classId },

    //        function (orderdetails) {
    //            $("#MI_Assign_Section1").empty().append('<option value="0">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_Assign_Section1").append($('<option></option>').val(c.SecId).html(c.SecName));
    //            });
    //        });
    //}

    //$('#MI_Assign_Section1').change(function (event) {
    //    event.preventDefault();
    //    LoadSubject_MI_Assign();
    //});

    //function LoadSubject_MI_Assign() {
    //    var secId = $('#MI_Assign_Section1').val();
    //    $.getJSON('/ModulesInfo/showSubjects', { passSecId: secId },

    //        function (orderdetails) {
    //            $("#MI_Assign_allSubjects1").empty().append('<option value="0">Select Subject</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_Assign_allSubjects1").append($('<option></option>').val(c.SubId).html(c.SubName));
    //            });
    //        });
    //}


    //$(".DelAssignment").click(function (event) {
    //    var r = confirm("Are you sure want to delete?");
    //    if (r == true) {
    //        return true;
    //    } else {
    //        return false;
    //    }
    //});


    //$('#MI_Remark_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadSection_MI_Remarks();
    //});

    //function LoadSection_MI_Remarks() {
    //    var classId = $('#MI_Remark_allClass').val();
    //    $.getJSON('/ModulesInfo/showSection', { passClassId: classId },

    //        function (orderdetails) {
    //            $("#MI_Remark_Section").empty().append('<option value="0">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_Remark_Section").append($('<option></option>').val(c.SecId).html(c.SecName));
    //            });
    //        });
    //}

    //$('#MI_Remark_Section').change(function (event) {
    //    event.preventDefault();
    //    LoadSubject_MI_Remarks();
    //});

    //function LoadSubject_MI_Remarks() {
    //    var secId = $('#MI_Remark_Section').val();
    //    $.getJSON('/ModulesInfo/showSubjects', { passSecId: secId },

    //        function (orderdetails) {
    //            $("#MI_Remark__allSubjects").empty().append('<option value="0">Select Subject</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_Remark__allSubjects").append($('<option></option>').val(c.SubId).html(c.SubName));
    //            });
    //        });
    //}



    //$("#btnModuleInfoRemarks").click(function (event) {
    //    event.preventDefault();
    //    $('#ModuleInfoStudentList').empty();
    //    var yearId = $("#MI_Remark_allAcademicYears").val();
    //    var classId = $("#MI_Remark_allClass").val();
    //    var secId = $("#MI_Remark_Section").val();
    //    var subId = $("#MI_Remark__allSubjects").val();

    //    if (yearId == "" || classId == "" || secId == "" || subId == "") {
    //        $(".ModuleInfoRemarks-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //    $("#RemarksList").show();
    //    $.post('/ModulesInfo/getStudentList', { passYearId: yearId, passClassId: classId, passSecId: secId },

    //      function (data) {
    //          $(".ModuleInfoRemarks-message").hide();
    //          $.each(data, function (i, v) {
    //              var myCheckbox = "myCheckbox" + i;
    //              var txt_reason = "txt_remark" + i;
    //              var id = "ID" + i;
    //              row = $('<tr><td><input type="checkbox" class="MI_R" name="' + myCheckbox + '" /> </td><td><label name="' + id + '" id="' + id + '"  value="' + v.id + '">' + v.id + '</label></td><td><label id="Name">' + v.name + '</label> </td><td><input type="textbox" name="' + txt_reason + '" /><input type="hidden" name="' + id + '" value="' + v.id + '"  /></td></tr>');
    //              $('#ModuleInfoStudentList').append(row);
    //              $("#ivalue").val(i);
    //          });
    //      }, 'json');
    //});

    $("#allSelect").click(function (event) {
        if (this.checked) {
            $('.MI_R').each(function () {
                this.checked = true;
            });
        }
        else {
            $('.MI_R').each(function () {
                this.checked = false;
            });
        }
    });


    $(document).on('click', '.MI_R', function () {
        if (this.checked) {
            $('.MI_R').each(function () {
                if (this.checked) {
                    $('#allSelect').prop("checked", true);
                }
            });
        }
    });

    $(document).on('click', '.MI_R', function () {
        if ($("input[class='MI_R']").length == $("input[class='MI_R']:checked").length) {
            $("#allSelect").attr("checked", "checked");
        }
        else {
            $("#allSelect").removeAttr("checked");
        }
    });



    //$("#MI_Remark_allAcademicYears").addClass("col-lg-12");
    //$("#MI_Remark_allClass").addClass("col-lg-12");
    //$("#MI_Remark_Section").addClass("col-lg-12");
    //$("#MI_Remark__allSubjects").addClass("col-lg-12");

    //$('#MI_Remark_List_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadSection_MI_List_Remarks();
    //});

    //function LoadSection_MI_List_Remarks() {
    //    var classId = $('#MI_Remark_List_allClass').val();
    //    $.getJSON('/ModulesInfo/showSection', { passClassId: classId },

    //        function (orderdetails) {
    //            $("#MI_Remark_List_Section").empty().append('<option value="0">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_Remark_List_Section").append($('<option></option>').val(c.SecId).html(c.SecName));
    //            });
    //        });
    //}


    //$('#MI_Remark_List_Section').change(function (event) {
    //    event.preventDefault();
    //    LoadSubject_MI_List_Remarks();
    //});

    //function LoadSubject_MI_List_Remarks() {
    //    var secId = $('#MI_Remark_List_Section').val();
    //    $.getJSON('/ModulesInfo/showSubjects', { passSecId: secId },

    //        function (orderdetails) {
    //            $("#MI_Remark_List_allSubjects").empty().append('<option value="0">Select Subject</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_Remark_List_allSubjects").append($('<option></option>').val(c.SubId).html(c.SubName));
    //            });
    //        });
    //}

    //$('#MI_Remark_List_Section').change(function (event) {
    //    event.preventDefault();
    //    LoadStudent_MI_List_Remarks();
    //});

    //function LoadStudent_MI_List_Remarks() {
    //    var yearId = $("#MI_Remark_List_allAcademicYears").val();
    //    var classId = $("#MI_Remark_List_allClass").val();
    //    var secId = $('#MI_Remark_List_Section').val();
    //    $.getJSON('/ModulesInfo/showStudents', { passYearId: yearId, passClassId: classId, passSecId: secId },

    //        function (orderdetails) {
    //            $("#MI_Remark_List_students").empty().append('<option value="0">Select Student</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#MI_Remark_List_students").append($('<option></option>').val(c.StudentId).html(c.StudentName));
    //            });
    //        });
    //}

    //$(function () {
    //    var dates = $("#MI_Remark_List_txt-fromDate, #MI_Remark_List_txt-toDate").datepicker({
    //        dateFormat: 'yy-mm-dd',
    //        changeMonth: true,
    //        changeYear: true,
    //        onSelect: function (selectedDate) {
    //            var option = this.id == "MI_Remark_List_txt-fromDate" ? "minDate" : "maxDate",
    //            instance = $(this).data("datepicker"),
    //            date = $.datepicker.parseDate(
    //            instance.settings.dateFormat ||
    //            $.datepicker._defaults.dateFormat,
    //            selectedDate, instance.settings);
    //            dates.not(this).datepicker("option", option, date);
    //        }
    //    });
    //});


    //$("#btnModuleInfoRemarksList").click(function (event) {
    //    var yearId = $("#MI_Remark_List_allAcademicYears").val();
    //    var classId = $("#MI_Remark_List_allClass").val();
    //    var secId = $("#MI_Remark_List_Section").val();
    //    var subId = $("#MI_Remark_List_allSubjects").val();
    //    var student = $("#MI_Remark_List_students").val();
    //    var fromDate = $("MI_Remark_List_txt-fromDate").val();
    //    var toDate = $("#MI_Remark_List_txt-toDate").val();

    //    if (yearId == "" || classId == "" || secId == "" || subId == "" || student == "" || fromDate == "" || toDate == "") {
    //        $(".ModuleInfoRemarksList-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});

    //$("#MI_Remark_List_allAcademicYears").addClass("col-lg-12");
    //$("#MI_Remark_List_allClass").addClass("col-lg-12");
    //$("#MI_Remark_List_Section").addClass("col-lg-12");
    //$("#MI_Remark_List_allSubjects").addClass("col-lg-12");
    //$("#MI_Remark_List_students").addClass("col-lg-12");


    // Student module

    //$("#SM_HomeworkDate").datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    dateFormat: 'dd/mm/yy',
    //});

    //$("#SM_AllSubjects").addClass("col-lg-12");

    //$("#btnSM_Homework").click(function (event) {
    //    var subId = $("#SM_AllSubjects").val();
    //    var date = $("#SM_HomeworkDate").val();

    //    if (subId == "" || date == "") {
    //        $(".SM_Homework-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});


    //$(function () {
    //    var dates = $("#SM_FromDate, #SM_ToDate").datepicker({
    //        dateFormat: 'yy-mm-dd',
    //        changeMonth: true,
    //        changeYear: true,
    //        onSelect: function (selectedDate) {
    //            var option = this.id == "SM_FromDate" ? "minDate" : "maxDate",
    //            instance = $(this).data("datepicker"),
    //            date = $.datepicker.parseDate(
    //            instance.settings.dateFormat ||
    //            $.datepicker._defaults.dateFormat,
    //            selectedDate, instance.settings);
    //            dates.not(this).datepicker("option", option, date);
    //        }
    //    });
    //});

    //$("#btnSM_Assignment").click(function (event) {
    //    var subId = $("#SM_AllSubjects").val();
    //    var fdate = $("#SM_FromDate").val();
    //    var edate = $("#SM_ToDate").val();

    //    if (subId == "" || fdate == "" || edate == "") {
    //        $(".ModuleInfoRemarksList-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});

    //$("#btnSM_SubjectNotes").click(function (event) {
    //    $("#NoRecords").hide();
    //    $("#MandatoryFieldErrorMessage_List").show();
    //    var subId = $("#SM_AllSubjects").val();
    //    var fdate = $("#SM_FromDate").val();
    //    var edate = $("#SM_ToDate").val();

    //    if (subId == "" || fdate == "" || edate == "") {
    //        $("#MandatoryFieldErrorMessage_List").show();
    //        return false;
    //    }
    //});

    //$(function () {
    //    var dates = $("#SM_FromDate, #SM_ToDate").datepicker({
    //        dateFormat: 'yy-mm-dd',
    //        changeMonth: true,
    //        changeYear: true,
    //        onSelect: function (selectedDate) {
    //            var option = this.id == "SM_FromDate" ? "minDate" : "maxDate",
    //            instance = $(this).data("datepicker"),
    //            date = $.datepicker.parseDate(
    //            instance.settings.dateFormat ||
    //            $.datepicker._defaults.dateFormat,
    //            selectedDate, instance.settings);
    //            dates.not(this).datepicker("option", option, date);
    //        }
    //    });
    //});

    //student - timetble

    //$("#Stu_dayorder").change(function (event) {
    //    event.preventDefault();
    //    $("#NoRecord").hide();
    //    $("#div_classTt").hide();
    //    $('#Stu_TimetableList').empty();
    //    var dayId = $("#Stu_dayorder").val();
      
    //    $.post('/Timetable/ClassTimetable', { passDayId: dayId },
    //      function (data) {
    //          if (data.count > 0)
    //          {
    //              $("#div_classTt").show();
    //              $.each(data.PeriodList, function (i, v) {
    //                  row = $('<tr><td><label id="Period">' + v.Period + ' </td><td><label id="subject">' + v.Subject + '</label> </td></tr>');
    //                  $('#Stu_TimetableList').append(row);
    //              });
    //          }
    //          else {
    //              $("#NoRecord").show();
    //          }          
    //      }, 'json');
    //});



    //$("#allExams").change(function (event) {
    //    event.preventDefault();
    //    $("#NoRecord").hide();
    //    $("#divExamTt").hide();
    //    $('#Stu_ExamTimetableList').empty();
    //    var ExamId = $("#allExams").val();

    //    $("#RemarksList").show();
    //    $.post('/Timetable/ExamTimetable', { passExamId: ExamId },

    //      function (data) {
    //          if (data.count > 0)
    //          {
    //              $("#divExamTt").show();
    //              $("#RemarksList").show();
    //              $.each(data.ExamList, function (i, v) {
    //                  row = $('<tr><td><label id="Period">' + v.Date + ' </td><td><label id="subject">' + v.Subject + '</label> </td></tr>');
    //                  $('#Stu_ExamTimetableList').append(row);
    //              });
    //          }
    //          else {
    //              $("#NoRecord").show();
    //          }
    //      }, 'json');
    //});


    // - achievement

    $("#AcademicYear").addClass("col-lg-12");

    // student - attendance report

    $("SM_AcademicYear").addClass("col-lg-12");

    //$("#FL_Edit_txt_FromDate, #FL_Edit_txt_ToDate").datepicker("destroy");
    //var HolidayDate1 = [];
    //var HolidayReason1 = [];
    //$.getJSON("/FacultyLeaveRequest/getAllDate",

    //    function (data) {
    //        $.each(data.HolidaysList, function (i, c) {
    //            HolidayDate1[i] = c.Date;
    //            HolidayReason1[i] = c.Holiday;
    //        });

    //        var holidays = HolidayDate1;      //["8/04/2015", "08/20/2015"];
    //        $(function () {
    //            var dates = $("#FL_Edit_txt_FromDate, #FL_Edit_txt_ToDate").datepicker({
    //                dateFormat: 'yy-mm-dd',
    //                changeMonth: true,
    //                changeYear: true,
    //                minDate: '@@minDate',
    //                beforeShowDay: function (date) {
    //                    show = true;
    //                    for (var i = 0; i < holidays.length; i++) {
    //                        if (new Date(holidays[i]).toString() == date.toString()) { return [false, 'holiday green', HolidayReason1[i]]; }//No Holidays
    //                    }
    //                    if (date.getDay() == 0 || date.getDay() == 6) { return [false, 'holiday red', '']; }//No Weekends
    //                    var display = [show, '', (show) ? '' : 'Holidays'];//With Fancy hover tooltip!
    //                    return display;
    //                },
    //                onSelect: function (selectedDate) {
    //                    var option = this.id == "FL_Edit_txt_FromDate" ? "minDate" : "maxDate",
    //                    instance = $(this).data("datepicker"),
    //                    date = $.datepicker.parseDate(
    //                    instance.settings.dateFormat ||
    //                    $.datepicker._defaults.dateFormat,
    //                    selectedDate, instance.settings);
    //                    dates.not(this).datepicker("option", option, date);
    //                }
    //            });
    //        });
    //    });


    // student - profile
    $("#BloodGroup").addClass("col-lg-12");
    $("#Email").addClass("col-lg-12");
    $("#Religion").addClass("col-lg-12");
    $("#Community").addClass("col-lg-12");
    $("#Nationality").addClass("col-lg-12");
    $("#Contact").addClass("col-lg-12");
    $("#EmergencyContactPerson").addClass("col-lg-12");
    $("#EmergencyContactNumber").addClass("col-lg-12");
    $("#Address").addClass("col-lg-12");
    $("#City").addClass("col-lg-12");
    $("#State").addClass("col-lg-12");
    $("#Photo").addClass("col-lg-12");


    // Parent Leave Request
    
    // Mark Info
    $("#Mark_allAcademicYears").addClass("col-lg-12");
    $("#Mark_allClass").addClass("col-lg-12");
    $("#Mark_Section").addClass("col-lg-12");
    $("#Mark_allSubjects").addClass("col-lg-12");
    $("#Mark_Exam").addClass("col-lg-12");

    //$('#Mark_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadSection_Marks();
    //});

    //function LoadSection_Marks() {
    //    var classId = $('#Mark_allClass').val();
    //    $.getJSON('/MarksInfo/showSection', { passClassId: classId },

    //        function (orderdetails) {
    //            $("#Mark_Section").empty().append('<option value="0">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#Mark_Section").append($('<option></option>').val(c.SecId).html(c.SecName));
    //            });
    //        });
    //}

    //$('#Mark_Section').change(function (event) {
    //    event.preventDefault();
    //    LoadSubject_Marks();
    //});

    //function LoadSubject_Marks() {
    //    var secId = $('#Mark_Section').val();
    //    $.getJSON('/MarksInfo/showSubjects', { passSecId: secId },

    //        function (orderdetails) {
    //            $("#Mark_allSubjects").empty().append('<option value="0">Select Subject</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#Mark_allSubjects").append($('<option></option>').val(c.SubId).html(c.SubName));
    //            });
    //        });
    //}



    //$("#btnMarksInfoMark").click(function (event) {
    //    event.preventDefault();
    //    $('#MarksInfoStudentList_body').empty();
    //    var yearId = $("#Mark_allAcademicYears").val();
    //    var classId = $("#Mark_allClass").val();
    //    var secId = $("#Mark_Section").val();
    //    var subId = $("#Mark_allSubjects").val();
    //    var examId = $("#Mark_Exam").val();

    //    if (yearId == "" || classId == "" || secId == "" || subId == "" || examId == "") {
    //        $(".MarksInfoMark-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //    $("#MarksInfoStudentList").show();
    //    $.post('/MarksInfo/getStudentListForMark', { passYearId: yearId, passClassId: classId, passSecId: secId, passSubId: subId },
    //      function (data) {
    //          $(".MarksInfoMark-message").hide();
    //          var MaxMark = data.MaxMark;
    //          $('<tr><td><label>' + MaxMark + '</label></td></tr>');
    //          $("#MaxMark").val(MaxMark).html(MaxMark);
    //          $.each(data.studentList, function (i, v) {
    //              var txt_mark = "txt_mark" + i;
    //              var id = "ID" + i;
    //              row = $('<tr><td><label >' + v.id + '</label></td><td><label id="Name">' + v.name + '</label> </td><td><input type="textbox" name="' + txt_mark + '" /><input type="hidden" name="' + id + '" value="' + v.id + '"  /></td></tr>');
    //              $('#MarksInfoStudentList_body').append(row);
    //              $("#ivalue").val(i);
    //          });
    //      }, 'json');
    //});



    $("#PG_allAcademicYears").addClass("col-lg-12");
    $("#PG_allClass").addClass("col-lg-12");
    $("#PG_Section").addClass("col-lg-12");
    $("#PG_allStudents").addClass("col-lg-12");
    $("#PG_Exam").addClass("col-lg-12");

    //$('#PG_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadSection_ProgressCard();
    //});

    //function LoadSection_ProgressCard() {
    //    var classId = $('#PG_allClass').val();
    //    $.getJSON('/MarksInfo/showSection', { passClassId: classId },

    //        function (orderdetails) {
    //            $("#PG_Section").empty().append('<option value="0">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#PG_Section").append($('<option></option>').val(c.SecId).html(c.SecName));
    //            });
    //        });
    //}


    //$('#PG_Section').change(function (event) {
    //    event.preventDefault();
    //    LoadStudents_ProgressCard();
    //});

    //function LoadStudents_ProgressCard() {
    //    var secId = $('#PG_Section').val();
    //    $.getJSON('/MarksInfo/showStudents', { passSecId: secId },

    //        function (orderdetails) {
    //            $("#PG_allStudents").empty().append('<option value="0">Select Student</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#PG_allStudents").append($('<option></option>').val(c.StudentId).html(c.StudentName));
    //            });
    //        });
    //}


    //$("#btnMarksInfoProgressCard").click(function (event) {
    //    var yearId = $("#PG_allAcademicYears").val();
    //    var classId = $("#PG_allClass").val();
    //    var secId = $("#PG_Section").val();
    //    var stuId = $("#PG_allStudents").val();
    //    var examId = $("#PG_Exam").val();

    //    if (yearId == "" || classId == "" || secId == "" || stuId == "" || examId == "") {
    //        $(".MarksInfoProgressCard-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});




    //$("#btnGradeInfoGrade").click(function (event) {
    //    event.preventDefault();
    //    $('#GradeInfoStudentList_body').empty();
    //    var yearId = $("#Mark_allAcademicYears").val();
    //    var classId = $("#Mark_allClass").val();
    //    var secId = $("#Mark_Section").val();
    //    var subId = $("#Mark_allSubjects").val();
    //    var examId = $("#Mark_Exam").val();

    //    if (yearId == "" || classId == "" || secId == "" || subId == "" || examId == "") {
    //        $(".GradeInfoMark-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //    $("#GradeInfoStudentList").show();
    //    $.post('/GradeInfo/getStudentListForGrade', { passYearId: yearId, passClassId: classId, passSecId: secId, passSubId: subId },
    //      function (data) {
    //          $(".MarksInfoMark-message").hide();
    //          var MaxMark = data.MaxMark;
    //          $("#MaxMark").val(MaxMark).html(MaxMark);
    //          $.each(data.studentList, function (i, v) {
    //              var txt_mark = "txt_mark" + i;
    //              var id = "ID" + i;
    //              row = $('<tr><td><label >' + v.id + '</label></td><td><label id="Name">' + v.name + '</label> </td><td><input type="textbox" name="' + txt_mark + '" /><input type="hidden" name="' + id + '" value="' + v.id + '"  /></td></tr>');
    //              $('#GradeInfoStudentList_body').append(row);
    //              $("#ivalue").val(i);
    //          });
    //      }, 'json');
    //});


    // assign subject to section

    //$('#CM_SS_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadSection_AssignSubToSec();
    //});

    //function LoadSection_AssignSubToSec() {
    //    var classId = $('#CM_SS_allClass').val();
    //    $.getJSON('/ClassManagement/GetSelectedClassSection', { passClassId: classId },

    //        function (orderdetails) {
    //            $("#CM_SS_Section").empty().append('<option value="">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#CM_SS_Section").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //            });
    //        });
    //}
    $("#CM_SS_allAcademicYears").addClass("col-lg-12");
    $("#CM_SS_allClass").addClass("col-lg-12");
    $("#CM_SS_Section").addClass("col-lg-12");
    $("#CM_SS_Subjects").addClass("col-lg-12");
    $(".chzn-select").chosen();

    //$("#btnAssignSubjectToSection").click(function (event) {
    //    var yearId = $("#CM_SS_allAcademicYears").val();
    //    var classId = $("#CM_SS_allClass").val();
    //    var secId = $("#CM_SS_Section").val();
    //    var subId = $("#CM_SS_Subjects").val();

    //    if (yearId == "" || classId == "" || secId == "" || subId == "") {
    //        $(".AssignSubjectToSection-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});

    //$("#btnAssignSubjectToSection").click(function (event) {
    //    var yearId = $("#allAcademicYears").val();
    //    var classId = $("#allClass").val();
    //    var secId = $("#Section").val();
    //    var subId = $("#Subjects").val();

    //    if (yearId == "" || classId == "" || secId == "" || subId == "") {
    //        $(".AssignSubjectToSection-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});

    //$("#btnListSubjectToSection").click(function (event) {
    //    var yearId = $("#SS_allAcademicYears").val();
    //    var classId = $("#SS_allClass").val();
    //    var secId = $("#SS_Section").val();

    //    if (yearId == "" || classId == "" || secId == "") {
    //        $(".ListSubjectToSection-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});

    $("#SS_allAcademicYears").addClass("col-lg-12");
    $("#SS_allClass").addClass("col-lg-12");
    $("#SS_Section").addClass("col-lg-12");

    $("ACT_Edit_allFaculties1").addClass("col-lg-12");

    //$('#SS_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadSection_AssignSS();
    //});

    //function LoadSection_AssignSS() {
    //    var classId = $('#SS_allClass').val();
    //    $.getJSON('/ClassManagement/GetSelectedClassSection', { passClassId: classId },

    //        function (orderdetails) {
    //            $("#SS_Section").empty().append('<option value="0">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#SS_Section").append($('<option></option>').val(c.selSectionId).html(c.selSectionName));
    //            });
    //        });
    //}


    //$("#btnCM_AssignClassTeacher").click(function (event) {
    //    var yearId = $("#allAcademicYears").val();
    //    var classId = $("#allClass").val();
    //    var secId = $("#Section").val();
    //    var faculId = $("#allFaculties").val();

    //    if (yearId == "" || classId == "" || secId == "" || faculId == "") {
    //        $(".CM_AssignClassTeacher-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});



    // student

    //$("#btnSM_StudentEditProfile").click(function (event) {
    //    $("#MandatoryFieldErrorMessage_Edit").hide();
    //    var BloodGroup = $("#BloodGroup").val();
    //    var Email = $("#Email").val();
    //    var Religion = $("#Religion").val();
    //    var Community = $("#Community").val();
    //    var Nationality = $("#Nationality").val();
    //    var Contact = $("#Contact").val();
    //    var EmergencyContactPerson = $("#EmergencyContactPerson").val();
    //    var EmergencyContactNumber = $("#EmergencyContactNumber").val();
    //    var Address = $("#Address").val();
    //    var City = $("#City").val();
    //    var State = $("#State").val();

    //    if (BloodGroup == "" || Email == "" || Religion == "" || Community == "" || Nationality == "" || Contact == "" || EmergencyContactPerson == "" || EmergencyContactNumber == "" || Address == "" || City == "" || State == "") {
    //        $("#MandatoryFieldErrorMessage_Edit").show();
    //        return false;
    //    }
    //});


   

    $("#stu_PC_allAcademicYears").addClass("col-lg-12");
    $("#stu_PC_allExams").addClass("col-lg-12");

    //$("#btnStudent_ProgressCard1").click(function (event) {

    //    $("#MandatoryFieldErrorMessage_List").hide();
    //    var yearId = $("#stu_PC_allAcademicYears").val();
    //    var examId = $("#stu_PC_allExams").val();

    //    $("#stu_PC_allAcademicYears").addClass("current_button");
    //    $("#stu_PC_allExams").addClass("current_button");

    //    var a = [];
    //    var field_val = [];
    //    $(".current_button").each(function () {
    //        var valu = $(this).val();
    //        if (valu == '') {
    //            var null_id = $(this).attr('id');
    //            if (null_id == 'stu_PC_allAcademicYears') {

    //                a.push('Please select academic year');
    //            }
    //            if (null_id == 'stu_PC_allExams') {

    //                a.push('Please select exam');
    //            }
    //        }
    //        else {
    //        }
    //    });

    //    if (a.length != 0) {
    //        $("#MandatoryFieldErrorMessage_List").show();
    //        $(".List_jErr").append().empty();
    //        $.each(a, function (i, v) {
    //            if (a[i] != "" && a[i] != null) {
    //                li = $("<li></li>").text(a[i]);
    //                $(".List_jErr").append(li);
    //            }
    //        });
    //        a = [];
    //        return false;
    //    }
    //    else {
    //        $("#MandatoryFieldErrorMessage_List").hide();
    //    }

       
    //});

    // Faculty Info

    //$("#btn_FacultyIfo_ClassAssigned").click(function (event) {
    //    $("#show").hide();
    //    $("#NoRecords").hide();
    //    var yearId = $("#Fac_allAcademicYears").val();

    //    $("#Fac_allAcademicYears").addClass("current_button");

    //    var a = [];
    //    var field_val = [];
    //    $(".current_button").each(function () {
    //        var valu = $(this).val();
    //        if (valu == '') {
    //            var null_id = $(this).attr('id');
    //            if (null_id == 'Fac_allAcademicYears') {

    //                a.push('Please select academic year');
    //            }
    //        }
    //        else {
    //        }
    //    });

    //    if (a.length != 0) {
    //        $("#MandatoryFieldErrorMessage_List").show();
    //        $(".List_jErr").append().empty();
    //        $.each(a, function (i, v) {
    //            if (a[i] != "" && a[i] != null) {
    //                li = $("<li></li>").text(a[i]);
    //                $(".List_jErr").append(li);
    //            }
    //        });
    //        a = [];
    //        return false;
    //    }
    //    else {
    //        $("#show").show();
    //        $("#MandatoryFieldErrorMessage_List").hide();
    //    }

    //    $("#MandatoryFieldErrorMessage_List").hide();
    //});

    //$("#btn_FacultyInfo_ClassTimetable").click(function (event) {
    //    var yearId = $("#Fac_allAcademicYears").val();
    //    var day = $("#dayorder").val();

    //    if (yearId == "" || day == "") {
    //        $("#MandatoryFieldErrorMessage_List").show();
    //        return false;
    //    }
    //    $("#MandatoryFieldErrorMessage_List").hide();
    //});


    // class incharge

    //$("#btn_CI_StudentList").click(function (event) {
    //    var yearId = $("#CI_allAcademicYears").val();

    //    if (yearId == "") {
    //        $(".CI_StudentList-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});

 


    $("#selectAll").click(function (event) {
        if (this.checked) {
            $('.FacultyLeaveApprover').each(function () {
                this.checked = true;
            });
        }
        else {
            $('.FacultyLeaveApprover').each(function () {
                this.checked = false;
            });
        }
    });


    $(document).on('click', '.FacultyLeaveApprover', function () {
        if (this.checked) {
            $('.FacultyLeaveApprover').each(function () {
                if (this.checked) {
                    $('#selectAll').prop("checked", true);
                }
            });
        }
    });

    $(document).on('click', '.FacultyLeaveApprover', function () {
        if ($("input[class='FacultyLeaveApprover']").length == $("input[class='FacultyLeaveApprover']:checked").length) {
            $("#selectAll").attr("checked", "checked");
        }
        else {
            $("#selectAll").removeAttr("checked");
        }
    });


    //-------
    $("#selectAll").click(function (event) {
        if (this.checked) {
            $('.checkbox1').each(function () {
                this.checked = true;
            });
        }
        else {
            $('.checkbox1').each(function () {
                this.checked = false;
            });
        }
    });

    $('.checkbox1').click(function () {
        if (this.checked) {
            $('.checkbox1').each(function () {
                if (this.checked) {
                    $('#selectAll').prop("checked", true);
                }
            });
        }
    });

    $(".checkbox1").click(function () {

        if ($("input[class='checkbox1']").length == $("input[class='checkbox1']:checked").length) {
            $("#selectAll").attr("checked", "checked");
        }
        else {
            $("#selectAll").removeAttr("checked");
        }
    });

    // Employee Management Leave Request

    $("#EM_selectAll").click(function (event) {
        if (this.checked) {
            $('.EM_checkbox1').each(function () {
                this.checked = true;
            });
        }
        else {
            $('.EM_checkbox1').each(function () {
                this.checked = false;
            });
        }
    });

    $('.EM_checkbox1').click(function () {
        if (this.checked) {
            $('.EM_checkbox1').each(function () {
                if (this.checked) {
                    $('#EM_selectAll').prop("checked", true);
                }
            });
        }
    });

    $(".EM_checkbox1").click(function () {

        if ($("input[class='EM_checkbox1']").length == $("input[class='EM_checkbox1']:checked").length) {
            $("#EM_selectAll").attr("checked", "checked");
        }
        else {
            $("#EM_selectAll").removeAttr("checked");
        }
    });


    // --- class attendance

    $("#CI_allSelect_ClassAttendance").click(function (event) {
        if (this.checked) {
            $('.x').each(function () {
                this.checked = true;
            });
        }
        else {
            $('.x').each(function () {
                this.checked = false;
            });
        }
    });


    $(document).on('click', '.x', function () {
        if (this.checked) {
            $('.x').each(function () {
                if (this.checked) {
                    $('#CI_allSelect_ClassAttendance').prop("checked", true);
                }
            });
        }
    });

    $(document).on('click', '.x', function () {
        if ($("input[class='x']").length == $("input[class='x']:checked").length) {
            $("#CI_allSelect_ClassAttendance").attr("checked", "checked");
        }
        else {
            $("#CI_allSelect_ClassAttendance").removeAttr("checked");
        }
    });


    // module Info

    //$("#btnUpdate_HW_File").click(function (event) {
    //    var fileId = $("#CI_HW_File_Update").val();

    //    if (fileId == "") {
    //        $(".Update_HW_File-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});

    //$("#MI_HW_Edit_txt_submissionDate").datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    dateFormat: 'dd/mm/yy',
    //});



    //$("#btnEditHomework").click(function (event) {
    //    var homework = $("#MI_HW_Edit_txt_Homework").val();
    //    var desc = $("#MI_HW_Edit_txt_description").val();

    //    if (homework == "" || desc == "") {
    //        $(".EditHomework-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});



    // 19/12/2014 student Enrollment 


    //$("#txt_StudentDob").datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    dateFormat: 'dd/mm/yy',
    //});
    //$("#txt_StudentDoj").datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    dateFormat: 'dd/mm/yy',
    //});
    //$("#txt_FatherDob").datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    dateFormat: 'dd/mm/yy',
    //});
    //$("#txt_MotherDob").datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    dateFormat: 'dd/mm/yy',
    //});
    //$("#txt_GuardianDob").datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    dateFormat: 'dd/mm/yy',
    //});


    // 20.12.2014

  //  $("#Student_allCountries").addClass("col-lg-12");

    // 22.12.2014

    //$("#btn_ParentEditProfile").click(function (event) {
    //    var email = $("#Email").val();
    //    var Mobile = $("#Mobile").val();
    //    var Qualification = $("#Qualification").val();
    //    var YearlyIncome = $("#YearlyIncome").val();
    //    var Address = $("#Address").val();
    //    var City = $("#City").val();
    //    var State = $("#State").val();
    //    var country = $("#Parent_allCountries").val();


    //    if (email == "" || Mobile == "" || Qualification == "" || YearlyIncome == "" || Address == "" || City == "" || State == "" || country == "") {
    //        $(".ParentEditProfile-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }

    //    var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    //    var valid = emailReg.test(email);

    //    if (!valid) {
    //        $(".ParentEditProfile-message").text("Please enter valid Father Email Id").css("color", "#ff0000");
    //        return false;
    //    }
    //});


    //23.12.2014 ClassIncharge


    //$('#CI_Remark_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadSection_CI_Remark();
    //});

    //function LoadSection_CI_Remark() {
    //    var classId = $('#CI_Remark_allClass').val();

    //    $.getJSON('/ClassIncharge/showSection', { passClassId: classId },

    //        function (orderdetails) {
    //            $("#CI_Remark_Section").empty().append('<option value="">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#CI_Remark_Section").append($('<option></option>').val(c.SecId).html(c.SecName));
    //            });
    //        });
    //}


    //$("#btnCI_Remarks").click(function (event) {
    //    var yearId = $("#CI_Remark_allAcademicYears").val();
    //    var classId = $("#CI_Remark_allClass").val();
    //    var secId = $("#CI_Remark_Section").val();

    //    if (yearId == "" || classId == "" || secId == "") {
    //        $(".CI_Remarks-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }

    //    $("#RemarksList").show();
    //    $.post('/ClassIncharge/getStudentList', { passYearId: yearId, passClassId: classId, passSecId: secId },

    //      function (data) {
    //          $(".CI_Remarks-message").hide();
    //          $.each(data, function (i, v) {
    //              var myCheckbox6120 = "myCheckbox" + i;
    //              var txt_reason = "txt_remark" + i;
    //              var RollNum = "RollNum" + i;
    //              var id = "ID" + i;
    //              row = $('<tr><td><input type="checkbox" class = "xx" name="' + myCheckbox + '" /> </td><td><label name="' + RollNum + '" id="' + RollNum + '"  value="' + v.RolNumber + '">' + v.RolNumber + '</label></td><td><label id="Name">' + v.name + '</label> </td><td><input type="textbox" name="' + txt_reason + '" /><input type="hidden" name="' + id + '" value="' + v.id + '"  /></td></tr>');
    //              $('#CI_StudentList').append(row);
    //              $("#ivalue").val(i);
    //          });
    //      }, 'json');
    //});


    //$("#CI_Remark_allSelect").click(function (event) {
    //    if (this.checked) {
    //        $('.xx').each(function () {
    //            this.checked = true;
    //        });
    //    }
    //    else {
    //        $('.xx').each(function () {
    //            this.checked = false;
    //        });
    //    }
    //});


    //$(document).on('click', '.xx', function () {
    //    if (this.checked) {
    //        $('.xx').each(function () {
    //            if (this.checked) {
    //                $('#CI_Remark_allSelect').prop("checked", true);
    //            }
    //        });
    //    }
    //});

    //$(document).on('click', '.xx', function () {
    //    if ($("input[class='xx']").length == $("input[class='xx']:checked").length) {
    //        $("#CI_Remark_allSelect").attr("checked", "checked");
    //    }
    //    else {
    //        $("#CI_Remark_allSelect").removeAttr("checked");
    //    }
    //});






    //$('#CI_Remark_List_allClass').change(function (event) {
    //    event.preventDefault();
    //    LoadSection_CI_Remark_List();
    //});

    //function LoadSection_CI_Remark_List() {
    //    var classId = $('#CI_Remark_List_allClass').val();
    //    $.getJSON('/ClassIncharge/showSection', { passClassId: classId },

    //        function (orderdetails) {
    //            $("#CI_Remark_List_Section").empty().append('<option value="">Select Section</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#CI_Remark_List_Section").append($('<option></option>').val(c.SecId).html(c.SecName));
    //            });
    //        });
    //}


    //$('#CI_Remark_List_Section').change(function (event) {
    //    event.preventDefault();
    //    LoadStudents_CI_Remark_List();
    //});

    //function LoadStudents_CI_Remark_List() {
    //    var yearId = $('#CI_Remark_List_allAcademicYears').val();
    //    var classId = $('#CI_Remark_List_allClass').val();
    //    var secId = $('#CI_Remark_List_Section').val();
 
    //    $.getJSON('/ClassIncharge/getSectionStudent', { passYearId: yearId, passClassId: classId, passSecId: secId },

    //        function (orderdetails) {
    //            $("#CI_Remark_List_students").empty().append('<option value="">Select Student</option>');
    //            $.each(orderdetails, function (i, c) {
    //                $("#CI_Remark_List_students").append($('<option></option>').val(c.StudentId).html(c.StudentName));
    //            });
    //        });
    //}


    //$(function () {
    //    var dates = $("#CI_Remark_List_txt_fromDate, #CI_Remark_List_txt_toDate").datepicker({
    //        dateFormat: 'yy-mm-dd',
    //        changeMonth: true,
    //        changeYear: true,
    //        onSelect: function (selectedDate) {
    //            var option = this.id == "CI_Remark_List_txt_fromDate" ? "minDate" : "maxDate",
    //            instance = $(this).data("datepicker"),
    //            date = $.datepicker.parseDate(
    //            instance.settings.dateFormat ||
    //            $.datepicker._defaults.dateFormat,
    //            selectedDate, instance.settings);
    //            dates.not(this).datepicker("option", option, date);
    //        }
    //    });
    //});

    //$("#btnCI_RemarksList").click(function (event) {
    //    var yearId = $("#CI_Remark_List_allAcademicYears").val();
    //    var classId = $("#CI_Remark_List_allClass").val();
    //    var secId = $("#CI_Remark_List_Section").val();
    //    var stuId = $("#CI_Remark_List_students").val();
    //    var fdate = $("#CI_Remark_List_txt_fromDate").val();
    //    var edate = $("#CI_Remark_List_txt_toDate").val();


    //    if (yearId == "" || classId == "" || secId == "" || stuId == "" || fdate == "" || edate == "") {
    //        $(".CI_RemarksList-message").text("Please select all the mantratory fields").css("color", "#ff0000");
    //        return false;
    //    }
    //});

    $("#CI_Remark_List_allAcademicYears").addClass("col-lg-12");
    $("#CI_Remark_List_allClass").addClass("col-lg-12");
    $("#CI_Remark_List_Section").addClass("col-lg-12");
    $("#CI_Remark_List_students").addClass("col-lg-12");



    $("#Status").addClass("col-lg-12");


    $("#CI_Remark_allAcademicYears").addClass("col-lg-12");
    $("#CI_Remark_allClass").addClass("col-lg-12");
    $("#CI_Remark_Section").addClass("col-lg-12");




    $('#ParentStudentRegId').change(function (event) {
        event.preventDefault();
        var StudentRegId = $("#ParentStudentRegId").val();
        $.getJSON('/DashboardParent/RefreshStudentRegId', { passStudentRegId: StudentRegId },

            function (orderdetails) {
                location.reload();
            });
    });




    $.getJSON("/Report/Attendance",
    function (data) {

        var month = [];
        var presentCount = [];
        var monthName = [];
        var j = 1;
      
        var Year = data.Year;
        for (var x = 0; x < data.monthCount; x++) {
            var AllMonth = data.month1;



            var singleMonth = AllMonth.slice(x, j);
            month[x] = singleMonth;
       

            var AllMonthPresentCount = data.presentDaysCount;
            var SingleMonthPresentCount = AllMonthPresentCount.slice(x, j);
            presentCount[x] = SingleMonthPresentCount;

            var AllMonthName = data.monthName;
            var singleMonthName = AllMonthName.slice(x, j) ;
            monthName[x] = singleMonthName;

            j++;
        }


        var month1 = [];


        for (var k = 0; k < data.monthCount; k++) {

            var a1 = month[k]; var b1 = presentCount[k]; var c1 = monthName[k];
            var x1 = parseInt(a1), y1 = parseInt(b1);
            month1.push({ label: c1, x: x1, y: y1 });


        }

        var chart = new CanvasJS.Chart("Stu_Attendance_Chart",
        {
            exportEnabled: true,
            title: {
                text: "Attendance"
            },
            axisY: {
                title: "Present Days"
                //suffix: " kPa"
            },
            //  animationEnabled: true,
            axisX: {
                title: "Working Days"
                // suffix: "/2/2015"
                //  prefix: data.month,
                // suffix: Month


            },
            data: [
            {
                markerSize: 0,
                toolTipContent: "<span style='\"'color: {color};'\"'><strong>Month</strong></span> {x}  <br/><span style='\"'color: {color};'\"'><strong>Present days</strong></span> {y}",
                type: "column",
                //     showInLegend: true,
                legendText: "Academic Year: " + Year,
                dataPoints: month1

            }

            ]
        });

        chart.render();
    });


     $.getJSON("/Report/studentAttendanceByMonth",
   function (data) {

       $("#NoRecordsMonthly").hide();
       $("#stChart").hide();

       var WorkingDaysCount = [];
       var presentCount = [];
       var monthName = [];
       var MonthCount;

       $.each(data, function (i, obj) {
           monthName = obj.monthName;
           MonthCount = obj.totalMonthCount;
           WorkingDaysCount = obj.NumberOfWorkingDays;
           presentCount = obj.presentDaysCount;
       });

       var WorkingDaysLength = WorkingDaysCount.length;
       if (WorkingDaysLength >= 1)
       {
           $("#stChart").show();

           var month1WorkingDays = [];
           var month1PresentDays = [];

           for (var k = 0; k < MonthCount; k++) {
               var M1 = monthName[k]; var W1 = WorkingDaysCount[k]; var P1 = presentCount[k];

               var y1 = parseInt(W1); var z1 = parseInt(P1);

               month1WorkingDays.push({ label: M1, y: y1 });
               month1PresentDays.push({ label: M1, y: z1 });
           }

           var chart = new CanvasJS.Chart("Stu_Attendance_ChartWithNumberOfWorkinDays",
           {
               theme: "theme3",
               animationEnabled: true,
               title: {
                   text: "Attendance Details",
                   fontSize: 30
               },
               toolTip: {
                   shared: true
               },
               axisY: {
                   title: "Number of days/Month"
               },
               //axisY2: {
               //    title: "million barrels/day"
               //},
               data: [
               {
                   type: "column",
                   name: "Working Days",
                   legendText: "Working Days",
                   showInLegend: true,
                   dataPoints: month1WorkingDays
               },
               {
                   type: "column",
                   name: "Present Days",
                   legendText: "Present Days",
                   axisYType: "secondary",
                   showInLegend: true,
                   dataPoints: month1PresentDays
               }

               ],
               legend: {
                   cursor: "pointer",
                   itemclick: function (e) {
                       if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                           e.dataSeries.visible = false;
                       }
                       else {
                           e.dataSeries.visible = true;
                       }
                       chart.render();
                   }
               },
           });

           chart.render();
       }
       else
       {
           $("#NoRecordsMonthly").show();
       }
   });



    $("#tbl_studentAchievement").dataTable();


    var order;
    var url = window.location.href;
    if (url == 'http://localhost:55654/DashboardStudent/Dashboard') {
        order = 0;
    }
    else if (url == 'http://localhost:55654/Student/StudentProfile') {
        order = 1;
    }
    else if (url == 'http://localhost:55654/Student/FeeManagement') {
        order = 2;
    }
    else if (url == 'http://localhost:55654/Student/AttendanceReport') {
        order = 3;
    }
    else if (url == 'http://localhost:55654/Student/ProgressCard') {
        order = 4;
    }
    else if (url == 'http://localhost:55654/StudentModuleAssigned/Homework') {
        order = 5;
    }
    else if (url == 'http://localhost:55654/StudentModuleAssigned/Assignment') {
        order = 6;
    }
    else if (url == 'http://localhost:55654/StudentModuleAssigned/ModulesAssigned') {
        order = 7;
    }
    else if (url == 'http://localhost:55654/Timetable/ClassTimetable' || url == 'http://localhost:55654/Timetable/ExamTimetable') {
        order = 8;
    }
    else if (url == 'http://localhost:55654/Achievements/StudentAchievement' || url == 'http://localhost:55654/Achievements/FacultyAchievement' || url == 'http://localhost:55654/Achievements/SchoolAchievement') {
        order = 9;
    }

    var activeEl = order;
    $(function () {
        var items = $('.btn-nav');
        $(items[activeEl]).addClass('active');
        $(".btn-nav").click(function () {
            $(items[activeEl]).removeClass('active');
            $(this).addClass('active');
            activeEl = $(".btn-nav").index(this);
        });
    });
     



});



