﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.ApiModels
{
    public class StudentDetails
    {
        public int studentRegId { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string photo { get; set; }
        public string Class { get; set; }
        public string section { get; set; }
        public string dob { get; set; }
        public string primaryusername { get; set; }       
        public string addressline1 { get; set; }
        public string addressline2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }        
        public long? pincode { get; set; }
        public string email { get; set; }
        public long? phone { get; set; }  

    }
}