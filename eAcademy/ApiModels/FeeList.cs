﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.ApiModels
{
    public class FeeList
    {
        public string FeeCategoryName { get; set; }
        public Decimal? Amount { get; set; }
        public Decimal? Tax { get; set; }
        public string LastDate { get; set; }
        public string category { get; set; }
    }
}