﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eAcademy.DataModel;

namespace eAcademy.ApiModels
{
    public class Notification
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? EventDate { get; set; }
        public string FileName { get; set; }
        public string PostedDate { get; set; }
        public int studentRegId { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string StudentPhoto { get; set; }
        public string NotificationFrom { get; set; }
        public int RowId { get; set; }
        public string FromDate { get; set; }
        public string Reason { get; set; }
        public string ToDate { get; set; }
        public string LeaveRequestStatus { get; set; }
        public string NotificationFromTable { get; set; }
        public int AlbumId { get; set; }
        public List<AlbumImage> AlbumImg { get; set; }
    }
}