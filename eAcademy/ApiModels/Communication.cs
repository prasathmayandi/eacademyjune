﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.ApiModels
{
    public class Communication
    {
        public string token { get; set; }
        public int StudentRegId { get; set; }
        public DateTime date { get; set; }
    }
}