﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.ApiModels
{
    public class PushToken
    {
        public string token { get; set; }
        public string ntoken { get; set; }
    }
}