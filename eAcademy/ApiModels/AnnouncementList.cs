﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.ApiModels
{
    public class AnnouncementList
    {
        public int announcementid { get; set; }
        public string announcementname { get; set; }
        public string announcementdate { get; set; }
        public string announcementdescription { get; set; }
        public string PostedDate { get; set; } 
    }

    public class Dashboard
    {
        public List<AnnouncementList> announcement { get; set; }
        public List<StudentList> student { get; set; }
    }
}