﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.ApiModels
{
    public class StudentList
    {
        public string PrimaryUserFirstName { get; set; }
        public string StudentFirstName { get; set; }
        public string StudentLastName { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        public string Photo { get; set; }
        public int StudentRegId { get; set; }
        public string StudentId { get; set; }
    }
}