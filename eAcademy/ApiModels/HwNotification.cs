﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eAcademy.ApiModels
{
    public class HwNotification
    {
        public string Subject { get; set; }
        public string HomeworkTitle { get; set; }
        public string HomeworkDescription { get; set; }
        public int studentRegId { get; set; }
    }
}