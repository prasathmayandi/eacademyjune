USE [Eacademy1]
GO

/****** Object:  Table [dbo].[ResourceLanguage]    Script Date: 05/19/2016 16:23:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ResourceLanguage](
	[ResourcelanquageId] [int] IDENTITY(1,1) NOT NULL,
	[LanquageDisplayName] [varchar](50) NULL,
	[LanguageCultureName] [varchar](10) NULL,
	[LanguageCultureCode] [varchar](10) NULL,
	[LanguageCountryCode] [varchar](10) NULL,
	[Status] [bit] NULL,
	[DateOfCreated] [datetime] NULL,
	[DateOfUpdated] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ResourcelanquageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [Eacademy1]
GO

/****** Object:  Table [dbo].[ResourceType]    Script Date: 05/19/2016 16:29:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ResourceType](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[Deleted] [bit] NOT NULL,
	[DateCreated] [datetime2](7) NOT NULL,
	[DateUpdated] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_ResourceType_Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ResourceType] ADD  DEFAULT ((0)) FOR [Deleted]
GO

ALTER TABLE [dbo].[ResourceType] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO

ALTER TABLE [dbo].[ResourceType] ADD  DEFAULT (getdate()) FOR [DateUpdated]
GO


USE [Eacademy1]
GO

/****** Object:  Table [dbo].[Resource]    Script Date: 05/19/2016 16:30:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Resource](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[DefaultValue] [nvarchar](4000) NULL,
	[ResourceTypeId] [bigint] NULL,
	[Tag] [nvarchar](100) NULL,
	[Deleted] [bit] NOT NULL,
	[DateCreated] [datetime2](7) NOT NULL,
	[DateUpdated] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Resource_Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Resource]  WITH CHECK ADD  CONSTRAINT [FK_Resource_ResourceTypeId] FOREIGN KEY([ResourceTypeId])
REFERENCES [dbo].[ResourceType] ([Id])
GO

ALTER TABLE [dbo].[Resource] CHECK CONSTRAINT [FK_Resource_ResourceTypeId]
GO

ALTER TABLE [dbo].[Resource] ADD  DEFAULT ((0)) FOR [Deleted]
GO

ALTER TABLE [dbo].[Resource] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO

ALTER TABLE [dbo].[Resource] ADD  DEFAULT (getdate()) FOR [DateUpdated]
GO



USE [Eacademy1]
GO

/****** Object:  Table [dbo].[ResourceValue]    Script Date: 05/19/2016 16:31:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ResourceValue](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Value] [nvarchar](4000) NOT NULL,
	[CultureName] [nvarchar](50) NOT NULL,
	[ResourceId] [bigint] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[DateCreated] [datetime2](7) NOT NULL,
	[DateUpdated] [datetime2](7) NOT NULL,
	[TextOrientation] [varchar](50) NULL,
	[ResourcelanquageId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_ResourceValue] UNIQUE NONCLUSTERED 
(
	[ResourceId] ASC,
	[CultureName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ResourceValue]  WITH CHECK ADD FOREIGN KEY([ResourcelanquageId])
REFERENCES [dbo].[ResourceLanguage] ([ResourcelanquageId])
GO

ALTER TABLE [dbo].[ResourceValue]  WITH CHECK ADD  CONSTRAINT [FK_ResourceValue_ResourceId] FOREIGN KEY([ResourceId])
REFERENCES [dbo].[Resource] ([Id])
GO

ALTER TABLE [dbo].[ResourceValue] CHECK CONSTRAINT [FK_ResourceValue_ResourceId]
GO

ALTER TABLE [dbo].[ResourceValue] ADD  DEFAULT ((0)) FOR [Deleted]
GO

ALTER TABLE [dbo].[ResourceValue] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO

ALTER TABLE [dbo].[ResourceValue] ADD  DEFAULT (getdate()) FOR [DateUpdated]
GO


